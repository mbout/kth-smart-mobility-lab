/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package platooningexpress;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javafx.animation.Interpolator;
import javafx.animation.KeyValue;
import javafx.animation.ParallelTransition;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextBoundsType;
import javafx.scene.transform.Rotate;
import javafx.util.Duration;

/**
 *
 * @author rui
 */
public class FingerSlider {
    
    
    double screenWidth = Globals.screenWidth;
    double screenHeight = Globals.screenHeight;
    
    int numberBlocksX = Globals.numberBlocksX;
    int numberBlocksY = Globals.numberBlocksY;
    
    double blockWidth = Globals.blockWidth;
    double blockHeight = Globals.blockHeight;
    
    
    
    Group gameZone;
    Level currentLevel;
    int[][] map = null;
    
    int[] lastSection;
    int[] truckSection;
    
    public List<Integer[]> arrow0Sections, arrow1Sections;
    Group arrowGroup = new Group();
    
    boolean choosingPath = false;
    int currentArrowIndex = -1;
    
    boolean truck0PathDone = false;
    boolean truck1PathDone = false;
    
    boolean playPressed = false;
    
    Group arrow0LinesGroup = new Group();
    Paint arrow0Color = Paint.valueOf("RED");
    Group arrow1LinesGroup = new Group();
    Paint arrow1Color = Paint.valueOf("YELLOW");
    
    double arrowWidth = blockWidth/2.;
    double arrowOpacity = 0.5;
    double previewTruckOpacity = 0.5;
    
    int[] truckPosition0 = new int[2];
    int[] truckPosition1 = new int[2];
    
    double animationPathsDuration = 0;
    
    ImageView truck0ImageView, truck1ImageView;
    
    Image truckRedImg = new Image("file:frontTruckRed.png");
    Image truckYellowImg = new Image("file:frontTruckYellow.png");
    
    Image truckYellowFrontImage = new Image("file:truckYellowFront.png");
    Image truckYellowBackImage = new Image("file:truckYellowBack.png");
    Image truckYellowLeftImage = new Image("file:truckYellowLeft.png");
    Image truckYellowRightImage = new Image("file:truckYellowRight.png");
    
    Image truckRedFrontImage = new Image("file:truckRedFront.png");
    Image truckRedBackImage = new Image("file:truckRedBack.png");
    Image truckRedLeftImage = new Image("file:truckRedLeft.png");
    Image truckRedRightImage = new Image("file:truckRedRight.png");
    
    Image houseRedImage = new Image("file:houseRed.png");
    Image houseYellowImage = new Image("file:houseYellow.png");
    
    double truckScalingRatio = 0.4 * ( blockWidth/100 );
    double houseScalingRatio = 0.3 * ( blockWidth/100 );
    
    Image packageRedImg = new Image("file:packageRed.png");
    Image packageYellowImg = new Image("file:packageYellow.png");
    
    ImageView otherTruckPreview = new ImageView();
    
    Group truck0Packages = new Group();
    Group truck1Packages = new Group();
    
    Group truck0Text = new Group();
    Group truck1Text = new Group();
    
    Group fuelBar0 = new Group();
    Group fuelBar1 = new Group();
    
    double fuelUnitWidth;
    
    boolean interpolatorFirstTime = true;
    double prevInterpolatorTruck0X = 0.;
    double prevInterpolatorTruck0Y = 0.;
    double prevInterpolatorTruck1X = 0.;
    double prevInterpolatorTruck1Y = 0.;
    
    public int[] fuelSlotsTruck0Width;
    public int[] fuelSlotsTruck1Width;
    
    int truck0Delay = 0;
    int truck1Delay = 0;
    
    int maxHourGlasses = 5;
    
    Group timerGroup0, timerGroup1;
    
    public FingerSlider(Group argRoot, Level argCurrentLevel){
        
        
        
        gameZone = argRoot;
        
        currentLevel = argCurrentLevel;
        
        map = currentLevel.getMap();
        truckPosition0 = currentLevel.getTruckPos(0);
        truckPosition1 = currentLevel.getTruckPos(1);
        
        gameZone.getChildren().add(arrow0LinesGroup);
        gameZone.getChildren().add(arrow1LinesGroup);
        
        lastSection = new int[2];
        lastSection[0] = lastSection[1] = -1;
        
        truckSection = new int[2];
        truckSection[0] = 0;
        truckSection[1] = 8;
        
        arrow0Sections = new ArrayList<>();        
        arrow1Sections = new ArrayList<>();     
        
        // Adding other truck preview
        gameZone.getChildren().add(otherTruckPreview);
        
        // Adding first truck
        int firstTruckRoadType = map[truckPosition0[0]][truckPosition0[1]];
        
        double imageWidth = 0;
        
        if ( firstTruckRoadType == 8){
        
            truck0ImageView = new ImageView(truckRedBackImage);       
            imageWidth = truckRedBackImage.getWidth();
            
        }
        if ( firstTruckRoadType == 9){
        
            truck0ImageView = new ImageView(truckRedRightImage);       
            imageWidth = truckRedRightImage.getWidth();
            
        }
        if ( firstTruckRoadType == 10){
        
            truck0ImageView = new ImageView(truckRedFrontImage);   
            imageWidth = truckRedFrontImage.getWidth();
            
        }
        if ( firstTruckRoadType == 11){
        
            truck0ImageView = new ImageView(truckRedLeftImage); 
            imageWidth = truckRedLeftImage.getWidth();
            
        }
        
        gameZone.getChildren().add(truck0ImageView);
        
        truck0ImageView.setPreserveRatio(true);
        truck0ImageView.setFitWidth(imageWidth*truckScalingRatio);
                
        int[] truck0Pos = currentLevel.getTruckPos(0);
        
        double positionX = ( (double)truck0Pos[0] + 0.5)*blockWidth;
        double positionY = ( (double)truck0Pos[1] + 0.5)*blockHeight;
        truck0ImageView.setLayoutX(positionX - truck0ImageView.getBoundsInLocal().getWidth()/2.);
        truck0ImageView.setLayoutY(positionY - truck0ImageView.getBoundsInLocal().getHeight()/2.);
        
        // Adding second truck
        int secondTruckRoadType = map[truckPosition1[0]][truckPosition1[1]];
        
        if ( secondTruckRoadType == 8){
        
            truck1ImageView = new ImageView(truckYellowBackImage);     
            imageWidth = truckYellowBackImage.getWidth();
            
        }
        if ( secondTruckRoadType == 9){
        
            truck1ImageView = new ImageView(truckYellowRightImage);      
            imageWidth = truckYellowRightImage.getWidth();
            
        }
        if ( secondTruckRoadType == 10){
        
            truck1ImageView = new ImageView(truckYellowFrontImage);       
            imageWidth = truckYellowFrontImage.getWidth();
            
        }
        if ( secondTruckRoadType == 11){
        
            truck1ImageView = new ImageView(truckYellowLeftImage);       
            imageWidth = truckYellowLeftImage.getWidth();
            
        }
        
        gameZone.getChildren().add(truck1ImageView);
        
        truck1ImageView.setPreserveRatio(true);
        truck1ImageView.setFitWidth(imageWidth*truckScalingRatio);
                
        int[] truck1Pos = currentLevel.getTruckPos(1);
        
        positionX = ( (double)truck1Pos[0] + 0.5)*blockWidth;
        positionY = ( (double)truck1Pos[1] + 0.5)*blockHeight;
        truck1ImageView.setLayoutX(positionX - truck1ImageView.getBoundsInLocal().getWidth()/2.);
        truck1ImageView.setLayoutY(positionY - truck1ImageView.getBoundsInLocal().getHeight()/2.);
        
        // Adding houses
        ImageView tempHouseImageView = new ImageView(houseRedImage);
        
        tempHouseImageView.setPreserveRatio(true);
        tempHouseImageView.setFitWidth(houseRedImage.getWidth()*houseScalingRatio);
        
        int[] goal0Pos = currentLevel.getGoalPos(0);
        
        tempHouseImageView.setLayoutX( (goal0Pos[0] + .5)*blockWidth - tempHouseImageView.getBoundsInLocal().getWidth()/2 );
        tempHouseImageView.setLayoutY( (goal0Pos[1] + .5)*blockHeight - tempHouseImageView.getBoundsInLocal().getHeight()/2 );
        
        gameZone.getChildren().add(tempHouseImageView);
        
        tempHouseImageView = new ImageView(houseYellowImage);
        
        tempHouseImageView.setPreserveRatio(true);
        tempHouseImageView.setFitWidth(houseYellowImage.getWidth()*houseScalingRatio);
        
        int[] goal1Pos = currentLevel.getGoalPos(1);
        
        tempHouseImageView.setLayoutX( (goal1Pos[0] + .5)*blockWidth - tempHouseImageView.getBoundsInLocal().getWidth()/2 );
        tempHouseImageView.setLayoutY( (goal1Pos[1] + .5)*blockHeight - tempHouseImageView.getBoundsInLocal().getHeight()/2 );
        
        gameZone.getChildren().add(tempHouseImageView);
        
        createFuelBars();
        
        gameZone.getChildren().addAll(fuelBar0, fuelBar1);
        
        fuelBar0.setLayoutX( 0.98*numberBlocksX*blockWidth - fuelBar0.getBoundsInParent().getWidth() );
        fuelBar0.setLayoutY( 0.02*numberBlocksY*blockHeight );
        
        fuelBar1.setLayoutX( 0.98*numberBlocksX*blockWidth - fuelBar1.getBoundsInParent().getWidth() );
        fuelBar1.setLayoutY( 0.02*numberBlocksY*blockHeight + fuelBar0.getBoundsInParent().getMaxY() );
        
        System.out.println("fuelBar0.getBoundsInLocal().getMinY() = " + fuelBar0.getBoundsInParent().getMinY());
        System.out.println("fuelBar0.getBoundsInLocal().getMaxY() = " + fuelBar0.getBoundsInParent().getMaxY());
        
        
        timerGroup0 = createTimerGroup(blockWidth, 0);
        timerGroup1 = createTimerGroup(blockWidth, 1);
        
        gameZone.getChildren().addAll(timerGroup0);
        gameZone.getChildren().addAll(timerGroup1);
        
        truck0Pos = currentLevel.getTruckPos(0);
        truck1Pos = currentLevel.getTruckPos(1);
        
        timerGroup0.setLayoutX( ( 3 )*blockWidth );
        timerGroup0.setLayoutY( ( 9 )*blockHeight );
        
        timerGroup1.setLayoutX( ( 6 )*blockWidth );
        timerGroup1.setLayoutY( ( 9 )*blockHeight );
        
        
        trackFinger();
        
    }
    
    private void updateFuelBars(int index, int argCurrentFuelUnits){
        
        Rectangle currentRect;
                
        if ( index == 0){
        
            currentRect = (Rectangle) fuelBar0.lookup("#"+"rectFuel");
        
        }else{
            
            currentRect = (Rectangle) fuelBar1.lookup("#"+"rectFuel");
                
        }
        
//        double newRectWidth = currentRect.getBoundsInLocal().getWidth() - ratioBlockLost*fuelUnitWidth;
        
        
//        System.out.println("Updating fuel bar number "+index+" with value of argCurrentFuelUnits: "+argCurrentFuelUnits );
        
        
        double newRectWidth = fuelUnitWidth*argCurrentFuelUnits;
        
//        System.out.println("newRectWidth = " + newRectWidth);
        
        currentRect.setWidth(newRectWidth);
        
    }
    
    private void createFuelBars(){
        
        double arcWidth = 10;
        double arcHeight = 10;
        
        double rectangleWidth = 0.2*numberBlocksX*blockWidth;
        double rectangleHeight = 0.03*numberBlocksY*blockHeight;
        
        Rectangle rectBG = new Rectangle(rectangleWidth, rectangleHeight);
        rectBG.setFill(Color.LIGHTSLATEGREY);
        rectBG.setId("rectBG");
        rectBG.setArcWidth(arcWidth);
        rectBG.setArcHeight(arcHeight);
        
        double borderLength = 0.02*rectangleWidth;
        
                
        double emptyFuelWidth = rectangleWidth - 2*borderLength;
        double emptyFuelHeight = rectangleHeight - 2*borderLength;
        
        Rectangle rectEmptyFuel = new Rectangle(emptyFuelWidth, emptyFuelHeight);
        rectEmptyFuel.setLayoutX( (rectangleWidth-emptyFuelWidth)/2. );
        rectEmptyFuel.setLayoutY( (rectangleHeight-emptyFuelHeight)/2. );
        
        rectEmptyFuel.setFill(Color.BLACK);
        rectEmptyFuel.setId("rectEmptyFuel");
        rectEmptyFuel.setArcWidth(arcWidth);
        rectEmptyFuel.setArcHeight(arcHeight);
        
        double maxFuel = (double) Math.max(currentLevel.getTruckFuel(0), currentLevel.getTruckFuel(1));
        
        double fuelBarWidth = emptyFuelWidth*( ((double)currentLevel.getTruckFuel(0))/maxFuel);
        
        fuelUnitWidth = emptyFuelWidth/maxFuel;
        
        System.out.println("fuelBarWidth = " + fuelBarWidth);
               
        Rectangle rectCurrentFuel = new Rectangle(fuelBarWidth, emptyFuelHeight);
        rectCurrentFuel.setLayoutX( (rectangleWidth-emptyFuelWidth)/2. );
        rectCurrentFuel.setLayoutY( (rectangleHeight-emptyFuelHeight)/2. );
        rectCurrentFuel.setArcWidth(arcWidth);
        rectCurrentFuel.setArcHeight(arcHeight);
        
        rectCurrentFuel.setFill(Color.RED);
        rectCurrentFuel.setId("rectFuel");        
        
        fuelBar0.getChildren().addAll(rectBG, rectEmptyFuel, rectCurrentFuel);
        
        rectBG = new Rectangle(rectangleWidth, rectangleHeight);
        rectBG.setFill(Color.LIGHTSLATEGREY);
        rectBG.setId("rectBG");
        rectBG.setArcWidth(arcWidth);
        rectBG.setArcHeight(arcHeight);
        
        rectEmptyFuel = new Rectangle(emptyFuelWidth, emptyFuelHeight);
        rectEmptyFuel.setLayoutX( (rectangleWidth-emptyFuelWidth)/2. );
        rectEmptyFuel.setLayoutY( (rectangleHeight-emptyFuelHeight)/2. );
        
        rectEmptyFuel.setFill(Color.BLACK);
        rectEmptyFuel.setId("rectEmptyFuel");
        rectEmptyFuel.setArcWidth(arcWidth);
        rectEmptyFuel.setArcHeight(arcHeight);
                
        System.out.println("currentLevel.getTruckFuel(1) = " + currentLevel.getTruckFuel(1));
        
        fuelBarWidth = emptyFuelWidth*( ((double)currentLevel.getTruckFuel(1))/maxFuel);
        
        System.out.println("fuelBarWidth = " + fuelBarWidth);
        
        rectCurrentFuel = new Rectangle(fuelBarWidth, emptyFuelHeight);
        rectCurrentFuel.setLayoutX( (rectangleWidth-emptyFuelWidth)/2. );
        rectCurrentFuel.setLayoutY( (rectangleHeight-emptyFuelHeight)/2. );
        
        rectCurrentFuel.setFill(Color.YELLOW);
        rectCurrentFuel.setId("rectFuel");   
        rectCurrentFuel.setArcWidth(arcWidth);
        rectCurrentFuel.setArcHeight(arcHeight);
        
        fuelBar1.getChildren().addAll(rectBG, rectEmptyFuel, rectCurrentFuel);
        
    }
    
    private void clearArrowDraw(int arrowIndex){
        
        if ( arrowIndex == 0){
        
            int numElements = arrow0LinesGroup.getChildren().size();
            for ( int i = 0 ; i < numElements ; i++ ){

                arrow0LinesGroup.getChildren().get(i).setOpacity(0.);

            }
            
        }else{
            
            int numElements = arrow1LinesGroup.getChildren().size();
            for ( int i = 0 ; i < numElements ; i++ ){

                arrow1LinesGroup.getChildren().get(i).setOpacity(0.);

            }
            
        }
                    
    }
        
    private void updateArrow(int[] argLastSection, int[] argMapSection, int arrowIndex){
          
        if ( !choosingPath ){
            
            return;
            
        }
        
        eraseAndDrawNewArrow(arrowIndex);
 
   
    }
    
    private void eraseAndDrawNewArrow(int arrowIndex){
          
        
//        arrowSections.clear();
//        choosingPath = false;
        clearArrowDraw(arrowIndex);
        
        Image shadeStraightImage;
        Image shadeCurveImage;
        Image shadeEndImage;
        
        if (arrowIndex == 0){
        
            shadeStraightImage = new Image("file:shadeStraightRed.png");
            shadeCurveImage = new Image("file:shadeCurveRed.png");
            shadeEndImage = new Image("file:shadeEndRed.png");
        
        }else{
            
            shadeStraightImage = new Image("file:shadeStraightYellow.png");
            shadeCurveImage = new Image("file:shadeCurveYellow.png");
            shadeEndImage = new Image("file:shadeEndYellow.png");
            
        }
        
        List<Integer[]> arrowSections;
        if ( arrowIndex == 0 ){
            arrowSections = arrow0Sections;
        }else{
            arrowSections = arrow1Sections;
        }
        
        for ( int i = 0; i < arrowSections.size() ; i++){
        
            int startSectionX = (int) arrowSections.get(i)[0];
            int startSectionY = (int) arrowSections.get(i)[1];
            
            int prevSectionX, prevSectionY;
            
            if ( i - 1 >= 0){
                
                prevSectionX = (int) arrowSections.get(i - 1)[0];
                prevSectionY = (int) arrowSections.get(i - 1)[1];
                
            }else{
                
                prevSectionX = -1;
                prevSectionY = -1;
                
            }
            
            int nextSectionX, nextSectionY;
            
//            System.out.println("i = " + i);
//            System.out.println("arrowSections.size()-1 = " + (arrowSections.size()-1) );
            
            if ( i < arrowSections.size()-1 ){
                
                nextSectionX = (int) arrowSections.get(i + 1)[0];
                nextSectionY = (int) arrowSections.get(i + 1)[1];
                
            }else{
                
                nextSectionX = -1;
                nextSectionY = -1;
                
            }
//            
//            int endSectionX = (int) arrowSections.get(i+1)[0];
//            int endSectionY = (int) arrowSections.get(i+1)[1];
            
//            Line tempLine = new Line((double) ( ( (double)startSectionX + .5 )*blockWidth ), (double) ( ( (double)startSectionY + .5 )*blockHeight ),
//                    (double) ( ( (double)endSectionX + .5 )*blockWidth ), (double) ( ( (double)endSectionY + .5 )*blockHeight ) );
            Circle tempCircle = new Circle( ((double)startSectionX + .5 )*blockWidth , ((double)startSectionY + .5 )*blockWidth , blockWidth/4.);
            
            ImageView tempImageView = new ImageView();
            
            int typeRoad = map[startSectionX][startSectionY];
        
            double toRotate = 0;
            
            if ( typeRoad == 1 ){

                tempImageView.setImage(shadeStraightImage);

            }
            if ( typeRoad == 2 ){

                tempImageView.setImage(shadeStraightImage);
                toRotate = 90;

            }
            if ( typeRoad == 3 ){

                tempImageView.setImage(shadeCurveImage);
                toRotate = 0;

            }
            if ( typeRoad == 4 ){

                tempImageView.setImage(shadeCurveImage);
                toRotate = 90;

            }
            if ( typeRoad == 5 ){

                tempImageView.setImage(shadeCurveImage);
                toRotate = 180;

            }
            if ( typeRoad == 6 ){

                tempImageView.setImage(shadeCurveImage);
                toRotate = -90;

            }
            if ( typeRoad == 7 || typeRoad == 12 || typeRoad == 13 || typeRoad == 14 || typeRoad == 15 ){
                // JUNCTION NEED ADDITIONAL PROCESSING
                
//                System.out.println("nextSectionX = " + nextSectionX);
                
//                System.out.println("typeRoad = " + typeRoad);
                
                if (nextSectionX == -1){
                    tempImageView.setImage(shadeEndImage);
                    
                    if ( startSectionY - prevSectionY > 0 ){
                        toRotate = 0;
                    }
                    if ( startSectionY - prevSectionY < 0 ){
                        toRotate = 180;
                    }
                    if ( startSectionX - prevSectionX < 0 ){
                        toRotate = 90;
                    }
                    if ( startSectionX - prevSectionX > 0 ){
                        toRotate = -90;
                    }
                }
                else{
                    tempImageView.setImage(shadeCurveImage);
                    
                    boolean fromUp = false, fromLeft = false;
                    
                    String origin = "";
                    
                    if ( startSectionX - prevSectionX > 0){
                        
                        origin = "left";
                        
                    }
                    if ( startSectionX - prevSectionX < 0){
                        
                        origin = "right";
                        
                    }
                    if ( startSectionY - prevSectionY > 0){
                        
                        origin = "up";
                        
                    }
                    if ( startSectionY - prevSectionY < 0){
                        
                        origin = "down";
                        
                    }
                    
                    boolean toUp = false, toLeft = false;
                    String destination = "";
                    
                    if ( nextSectionX - startSectionX > 0){
                        
                        destination = "right";
                        
                    }
                    if ( nextSectionX - startSectionX < 0){
                        
                        destination = "left";
                        
                    }
                    if ( nextSectionY - startSectionY > 0){
                        
                        destination = "down";
                        
                    }
                    if ( nextSectionY - startSectionY < 0){
                        
                        destination = "up";
                        
                    }
                    
                    
                    
                    if ( nextSectionY - startSectionY > 0 ){
                        toRotate = 180;
                    }
                    if ( nextSectionY - startSectionY < 0 ){
                        toRotate = 0;
                    }
                    if ( nextSectionX - startSectionX < 0 ){
                        toRotate = -90;
                    }
                    if ( nextSectionX - startSectionX > 0 ){
                        toRotate = 90;
                    }
                    
                    if ( (origin.equals("up") && destination.equals("right")) || (origin.equals("right") && destination.equals("up")) ){
                        toRotate = 90;                        
                    }
                    if ( (origin.equals("down") && destination.equals("right")) || (origin.equals("right") && destination.equals("down")) ){
                        toRotate = 180;                        
                    }
                    if (  (origin.equals("down") && destination.equals("left")) || (origin.equals("left") && destination.equals("down")) ){
                        toRotate = -90;                        
                    }
                    if ( (origin.equals("up") && destination.equals("left")) || (origin.equals("left") && destination.equals("up")) ){
                        toRotate = 0;                        
                    }
                    if ( (origin.equals("up") && destination.equals("down")) || (origin.equals("down") && destination.equals("up")) ){
                        tempImageView.setImage(shadeStraightImage);
                        toRotate = 0;                    
                    }
                    if ( (origin.equals("left") && destination.equals("right")) || (origin.equals("right") && destination.equals("left")) ){
                        tempImageView.setImage(shadeStraightImage);
                        toRotate = 90;                      
                    }
                    
                }
                
            }
            if ( typeRoad == 8 ){

                tempImageView.setImage(shadeEndImage);
                toRotate = 0;

            }
            if ( typeRoad == 9 ){

                tempImageView.setImage(shadeEndImage);
                toRotate = 90;

            }
            if ( typeRoad == 10 ){

                tempImageView.setImage(shadeEndImage);
                toRotate = 180;

            }
            if ( typeRoad == 11 ){

                tempImageView.setImage(shadeEndImage);
                toRotate = -90;

            }

            
            tempImageView.setPreserveRatio(true);
            tempImageView.setFitWidth(blockWidth);
            
            tempImageView.setOpacity(arrowOpacity);

            tempImageView.getTransforms().add(new Rotate(toRotate, blockWidth/2., blockWidth/2.));

            tempImageView.setLayoutX( (double)startSectionX*blockWidth );
            tempImageView.setLayoutY( (double)startSectionY*blockHeight );
                    
            
//            tempCircle.setStrokeWidth(arrowWidth);
//            tempCircle.setOpacity(arrowOpacity);
//            tempCircle.setStrokeLineCap(StrokeLineCap.BUTT);

            if ( arrowIndex == 0){
                
//                tempCircle.setStroke(arrow0Color);
                arrow0LinesGroup.getChildren().add( tempImageView );
                
            }else{
                
//                tempCircle.setStroke(arrow1Color);
                arrow1LinesGroup.getChildren().add( tempImageView );
                
            }
            
        }
   
    }
    
    private boolean repeatingSection(int[] argMapSection, int arrowIndex){
        
        boolean repeating = false;
        
        List<Integer[]> arrowSections;
        if ( arrowIndex == 0 ){
            arrowSections = arrow0Sections;
        }else{
            arrowSections = arrow1Sections;
        }
        
        
        Integer[] tempSection = new Integer[2];
        
        for ( int i = 0 ; i < arrowSections.size() ; i++ ){
            
            tempSection = arrowSections.get(i);
            
            if ( Arrays.equals( convertArrayToInt(tempSection) , argMapSection) ){
                repeating = true;
                return repeating;
                
            }
            
        }      
        
        return repeating;
        
    }
    
    private void deleteArrowUntil(int[] argMapSection, int arrowIndex){
        
        List<Integer[]> arrowSections;
        if ( arrowIndex == 0 ){
            arrowSections = arrow0Sections;
        }else{
            arrowSections = arrow1Sections;
        }
        
        int removeFromIndex = 0;
        
        Integer[] tempSection = new Integer[2];
        
        for ( int i = 0 ; i < arrowSections.size() ; i++ ){
            
            tempSection = arrowSections.get(i);
            if ( Arrays.equals( convertArrayToInt(tempSection) , argMapSection) ){    
                removeFromIndex = i;
                break;
                
            }
            
        }      
        
        int numElementsToRemove = arrowSections.size() - ( removeFromIndex + 1) ;
        
        
        for ( int i = 0 ; i < numElementsToRemove ; i++){
            
            arrowSections.remove(arrowSections.size()-1);
//            arrowSections.remove(0);
            
        }
        
//        System.out.println("arrowSections.size() = " + arrowSections.size());
        
        eraseAndDrawNewArrow(currentArrowIndex);
        
        
    }
    
    private boolean isAdjacentToLastPoint(int[] argMapSection, int arrowIndex){
        
        List<Integer[]> arrowSections;
        if ( arrowIndex == 0 ){
            arrowSections = arrow0Sections;
        }else{
            arrowSections = arrow1Sections;
        }
        
        boolean adjacentToLastPoint = true;
        
        
        
        Integer[] lastSection = arrowSections.get( arrowSections.size() - 1 );
        
        double manhattanDistance = ( Math.abs( (int)lastSection[0] - argMapSection[0] ) ) + ( Math.abs( (int)lastSection[1] - argMapSection[1] ) );
        
//        System.out.println("manhattanDistance = " + manhattanDistance);
        
        if (  manhattanDistance > 1 ){
            
            adjacentToLastPoint = false;
            
        }
        
        return adjacentToLastPoint;
        
    }
        
    private int isTruckClicked(int[] argMapSection){
        
        int truckClicked = -1;
        
        if ( Arrays.equals( argMapSection , truckPosition0) ){        
            truckClicked = 0;
            return truckClicked;
            
        }
        
        if ( Arrays.equals( argMapSection , truckPosition1) ){
            
            truckClicked = 1;
            return truckClicked;
            
        }
        
        
        return truckClicked;
        
    }
    
    private Integer[] convertArrayToInteger(int[] arrayToConvert){
        
        Integer[] convertedArray = new Integer[arrayToConvert.length];
        
        for ( int i = 0 ; i < arrayToConvert.length ; i++ ){
            
            convertedArray[i] = (Integer) arrayToConvert[i];
            
        }        
        
        return convertedArray;
        
    }
    
    private int[] convertArrayToInt(Integer[] arrayToConvert){
        
        int[] convertedArray = new int[arrayToConvert.length];
        
        for ( int i = 0 ; i < arrayToConvert.length ; i++ ){
            
            convertedArray[i] = (int) arrayToConvert[i];
            
        }        
        
        return convertedArray;
        
    }
    
    private void updatePath(int[] argMapSection, int arrowIndex){
        

        List<Integer[]> arrowSections;
        if ( arrowIndex == 0 ){
            arrowSections = arrow0Sections;
        }else{
            arrowSections = arrow1Sections;
        }
        
        if ( repeatingSection(argMapSection, arrowIndex) ){
            
            deleteArrowUntil(argMapSection, arrowIndex);
            
            return;
            
            
        }
        
        
        if ( validMovement(lastSection, argMapSection) && isAdjacentToLastPoint(argMapSection, arrowIndex) ){
            
            
            if ( arrowSections.size() == 0 ){
                
                System.out.println("EMPTY");
                arrowSections.add( convertArrayToInteger(argMapSection) );
                updateArrow(lastSection, argMapSection, currentArrowIndex);
                
            }
            

            arrowSections.add( convertArrayToInteger(argMapSection) );
            updateArrow(lastSection, argMapSection, currentArrowIndex);
            
        }else{
            
        }
        
        
        
        
        
        
        
        
    }
        
    private void computePathsAndFuels(int[][] animationPathTruck0, int[][] animationPathTruck1){
        
        animationPathTruck0[0] = convertArrayToInt( arrow0Sections.get( 0 ) );
        animationPathTruck1[0] = convertArrayToInt( arrow1Sections.get( 0 ) );
        
        for ( int i = 0 ; i < truck0Delay ; i++ ){
            animationPathTruck0[i+1] = animationPathTruck0[i];
        }
        
        for ( int i = truck0Delay ; i < animationPathTruck0.length ; i++ ){
            animationPathTruck0[i] = convertArrayToInt( arrow0Sections.get( i - truck0Delay) );
        }
        
        for ( int i = 0 ; i < truck1Delay ; i++ ){
            animationPathTruck1[i+1] = animationPathTruck1[i];
        }
        
        for ( int i = truck1Delay ; i < animationPathTruck1.length ; i++ ){
            animationPathTruck1[i] = convertArrayToInt( arrow1Sections.get( i - truck1Delay) );
        }
        
//        fuelSlotsTruck0Width = new int[arrow0Sections.size() + truck0Delay];
//        fuelSlotsTruck1Width = new int[arrow1Sections.size() + truck1Delay];
        fuelSlotsTruck0Width = new int[animationPathTruck0.length];
        fuelSlotsTruck1Width = new int[animationPathTruck1.length];
        
        for (int i = 0;i<fuelSlotsTruck0Width.length;i++){
            fuelSlotsTruck0Width[i]=-1;
        }
        for (int i = 0;i<fuelSlotsTruck1Width.length;i++){
            fuelSlotsTruck1Width[i]=-1;
        }
        
        fuelSlotsTruck0Width[0] = currentLevel.getTruckFuel(0);
        fuelSlotsTruck1Width[0] = currentLevel.getTruckFuel(1);
        
        for ( int i = 1 ; i < Math.min(animationPathTruck0.length,animationPathTruck1.length) - 1 ; i++ ){
            
            int[] posTruck0 = animationPathTruck0[i];
            int[] posTruck1 = animationPathTruck1[i-1];
            
            if ( Arrays.equals(posTruck0, posTruck1) ){
//                System.out.println("platoon happening up");
                fuelSlotsTruck0Width[i] = fuelSlotsTruck0Width[i-1];
                fuelSlotsTruck1Width[i] = fuelSlotsTruck1Width[i-1] - 1;
                continue;
            }
            
            posTruck0 = animationPathTruck0[i-1];
            posTruck1 = animationPathTruck1[i];
            
            if ( Arrays.equals(posTruck0, posTruck1) ){
//                System.out.println("platoon happening down");
                fuelSlotsTruck0Width[i] = Math.max(fuelSlotsTruck0Width[i-1] - 1, 0);
                fuelSlotsTruck1Width[i] = fuelSlotsTruck1Width[i-1];
                continue;
            }
            
            if ( i < truck0Delay){
                fuelSlotsTruck0Width[i] = fuelSlotsTruck0Width[i-1];
            }else{
                fuelSlotsTruck0Width[i] = Math.max(fuelSlotsTruck0Width[i-1] - 1, 0);
            }
            
            if ( i < truck1Delay){
                fuelSlotsTruck1Width[i] = fuelSlotsTruck1Width[i-1];
            }else{
                fuelSlotsTruck1Width[i] = Math.max(fuelSlotsTruck1Width[i-1] - 1, 0);
            }
                       
            
        }
        
        for (int i = 0;i<fuelSlotsTruck0Width.length;i++){
            if ( fuelSlotsTruck0Width[i]==-1){
                fuelSlotsTruck0Width[i]=Math.max(fuelSlotsTruck0Width[i-1]-1, 0);
            }
        }
        
        for (int i = 0;i<fuelSlotsTruck1Width.length;i++){
            if ( fuelSlotsTruck1Width[i]==-1){
                fuelSlotsTruck1Width[i]=Math.max(fuelSlotsTruck1Width[i-1]-1, 0);
            }
        }
        
        
        for ( int i = 1 ; i < fuelSlotsTruck0Width.length ; i++ ){
            if ( fuelSlotsTruck0Width[i-1] == 0 ){
                animationPathTruck0[i] = animationPathTruck0[i-1];
            }
        }
        for ( int i = 1 ; i < fuelSlotsTruck1Width.length ; i++ ){
            if ( fuelSlotsTruck1Width[i-1] == 0 ){
                animationPathTruck1[i] = animationPathTruck1[i-1];
            }
        }
        
        
        
    }
    
    
    private void playSelectedPaths(){
         
//        createTruckScoreText();
        
        double delayToStart = Math.min(truck0Delay, truck1Delay);
        
        arrow0LinesGroup.setOpacity(0.);
        arrow1LinesGroup.setOpacity(0.);
        otherTruckPreview.setImage(null);
        
        int[][] animationPathTruck0 = new int[truck0Delay+arrow0Sections.size()][2];
        int[][] animationPathTruck1 = new int[truck1Delay+arrow1Sections.size()][2];
        
        computePathsAndFuels(animationPathTruck0, animationPathTruck1);
        
        
        System.out.println("Arrays.toString(fuelSlotsTruck0Width) = " + Arrays.toString(fuelSlotsTruck0Width));
        System.out.println("Arrays.toString(fuelSlotsTruck1Width) = " + Arrays.toString(fuelSlotsTruck1Width));
        
//        animationPathsDuration = Math.max( arrow0Sections.size() , arrow1Sections.size() )
        animationPathsDuration = Math.max( animationPathTruck0.length , animationPathTruck1.length );
        
        // Dummy transition
        
        Rectangle rect = new Rectangle (100, 40, 100, 100);
        rect.setOpacity(0.);
 
        TranslateTransition tt = new TranslateTransition(Duration.millis(animationPathsDuration*500), rect);
        tt.setCycleCount(1);
        tt.setAutoReverse(false);
        
        
        class AnimationPathsInterpolator extends Interpolator {
            @Override
            protected double curve(double t) {
                
//                double percentageTruck0Path = ( t*animationPathsDuration )/arrow0Sections.size();
                double percentageTruck0Path = ( t*animationPathsDuration )/animationPathTruck0.length;
//                System.out.println("percentageTruck0Path = " + percentageTruck0Path);
                
                if ( percentageTruck0Path > 1.0 ){
                    
                }else{
                    
                    
//                    int prevIndex = (int) Math.floor ( percentageTruck0Path*arrow0Sections.size() );
//                    int nextIndex = (int) Math.ceil ( percentageTruck0Path*arrow0Sections.size() );
                    int prevIndex = (int) Math.floor ( percentageTruck0Path*animationPathTruck0.length );
                    int nextIndex = (int) Math.ceil ( percentageTruck0Path*animationPathTruck0.length );
                    
//                    if ( nextIndex > arrow0Sections.size() - 1 ){
//                        
//                        nextIndex = arrow0Sections.size() - 1;
//                        
//                    }
                    
                    if ( prevIndex > animationPathTruck0.length - 1 ){
                        
                        prevIndex = animationPathTruck0.length - 1;
                        
                    }
                    
                    if ( nextIndex > animationPathTruck0.length - 1 ){
                        
                        nextIndex = animationPathTruck0.length - 1;
                        
                    }

                    
//                    int[] prevPos = convertArrayToInt( arrow0Sections.get(prevIndex) );
//                    int[] nextPos = convertArrayToInt( arrow0Sections.get(nextIndex) );
                    int[] prevPos = animationPathTruck0[prevIndex];
                    int[] nextPos = animationPathTruck0[nextIndex];
                            
//                    double prevDistance = percentageTruck0Path*arrow0Sections.size() - (double)prevIndex;
//                    double nextDistance = (double)nextIndex - percentageTruck0Path*arrow0Sections.size();
                    double prevDistance = percentageTruck0Path*animationPathTruck0.length - (double)prevIndex;
                    double nextDistance = (double)nextIndex - percentageTruck0Path*animationPathTruck0.length;
                    
                    double currentX;
                    double currentY;
                    
                    if ( Arrays.equals(prevPos, nextPos) ){
                        
                        currentX = prevPos[0];
                        currentY = prevPos[1];
                        
                    }else{
                        
                        currentX = (1-prevDistance)*(double)prevPos[0] + (1-nextDistance)*(double)nextPos[0];
                        currentY = (1-prevDistance)*(double)prevPos[1] + (1-nextDistance)*(double)nextPos[1];
                        
                    }
                    
                    double deltaX = nextPos[0] - prevPos[0];
                    double deltaY = nextPos[1] - prevPos[1];
                    
                    double imageWidth = 0;
                    
//                    int currentFuelUnits = fuelSlotsTruck0Width[ (int) Math.round ( percentageTruck0Path* (arrow0Sections.size() - 1) )];
                    int currentFuelUnits = fuelSlotsTruck0Width[ (int) Math.round ( percentageTruck0Path* (animationPathTruck0.length - 1) )];
                    
                    updateFuelBars(0, currentFuelUnits);
                    
                    if ( currentFuelUnits != 0 ){
                        
                        if ( deltaX > 0 ){

                            truck0ImageView.setImage(truckRedRightImage);
                            imageWidth = truckRedRightImage.getWidth();

                        } 
                        if ( deltaX < 0 ){

                            truck0ImageView.setImage(truckRedLeftImage);
                            imageWidth = truckRedLeftImage.getWidth();

                        } 
                        if ( deltaY > 0 ){

                            truck0ImageView.setImage(truckRedFrontImage);
                            imageWidth = truckRedFrontImage.getWidth();

                        }
                        if ( deltaY < 0 ){

                            truck0ImageView.setImage(truckRedBackImage);
                            imageWidth = truckRedBackImage.getWidth();

                        }

                        if (imageWidth != 0){

                            truck0ImageView.setPreserveRatio(true);
                            truck0ImageView.setFitWidth(imageWidth*truckScalingRatio);

                        }
                        
                        truck0ImageView.setLayoutX( (currentX + 0.5)*blockWidth - truck0ImageView.getBoundsInLocal().getWidth()/2. );
                        truck0ImageView.setLayoutY( (currentY + 0.5)*blockHeight - truck0ImageView.getBoundsInLocal().getHeight()/2. );
                        
                    }
                    
                    
                    
                    
                 }
                
                double percentageTruck1Path = ( t*animationPathsDuration )/animationPathTruck1.length;
//                System.out.println("percentageTruck1Path = " + percentageTruck1Path);
                
                if ( percentageTruck1Path > 1.0 ){
                    
                }else{
                                       
                    
                    int prevIndex = (int) Math.floor ( percentageTruck1Path*animationPathTruck1.length );
                    int nextIndex = (int) Math.ceil ( percentageTruck1Path*animationPathTruck1.length );
                    
                    if ( prevIndex > animationPathTruck1.length - 1 ){
                        
                        prevIndex = animationPathTruck1.length - 1;
                        
                    }
                    
                    if ( nextIndex > animationPathTruck1.length - 1 ){
                        
                        nextIndex = animationPathTruck1.length - 1;
                        
                    }
                    
                    
                    
                    if ( prevIndex == nextIndex ){
                        
                        
                    }

//                    int[] prevPos = convertArrayToInt( arrow1Sections.get(prevIndex) );
//                    int[] nextPos = convertArrayToInt( arrow1Sections.get(nextIndex) );
                    int[] prevPos = animationPathTruck1[prevIndex];
                    int[] nextPos = animationPathTruck1[nextIndex];
                    
                    double prevDistance = percentageTruck1Path*animationPathTruck1.length - (double)prevIndex;
                    double nextDistance = (double)nextIndex - percentageTruck1Path*animationPathTruck1.length;
                    
                    double currentX;
                    double currentY;
                    
                    if ( Arrays.equals(prevPos, nextPos) ){
                        
                        currentX = prevPos[0];
                        currentY = prevPos[1];
                        
                    }else{
                        
                        currentX = (1-prevDistance)*(double)prevPos[0] + (1-nextDistance)*(double)nextPos[0];
                        currentY = (1-prevDistance)*(double)prevPos[1] + (1-nextDistance)*(double)nextPos[1];
                        
                    }
                    
                    double deltaX = nextPos[0] - prevPos[0];
                    double deltaY = nextPos[1] - prevPos[1];
                    
                    
                    double imageWidth = 0;
                    
                    int currentFuelUnits = fuelSlotsTruck1Width[ (int) Math.round ( percentageTruck1Path* (animationPathTruck1.length - 1) )];
                    
                    updateFuelBars(1, currentFuelUnits);
                    
                    if ( currentFuelUnits != 0 ){
                        
                        
                        if ( deltaX > 0 ){
                        
                            truck1ImageView.setImage(truckYellowRightImage);
                            imageWidth = truckYellowRightImage.getWidth();

                        } 
                        if ( deltaX < 0 ){

                            truck1ImageView.setImage(truckYellowLeftImage);
                            imageWidth = truckYellowLeftImage.getWidth();

                        } 
                        if ( deltaY > 0 ){

                            truck1ImageView.setImage(truckYellowFrontImage);
                            imageWidth = truckYellowFrontImage.getWidth();

                        }
                        if ( deltaY < 0 ){

                            truck1ImageView.setImage(truckYellowBackImage);
                            imageWidth = truckYellowBackImage.getWidth();

                        }

                        if (imageWidth != 0){

                            truck1ImageView.setPreserveRatio(true);
                            truck1ImageView.setFitWidth(imageWidth*truckScalingRatio);

                        }
                        
                        
                        truck1ImageView.setLayoutX( (currentX + 0.5)*blockWidth - truck1ImageView.getBoundsInLocal().getWidth()/2. );
                        truck1ImageView.setLayoutY( (currentY + 0.5)*blockHeight - truck1ImageView.getBoundsInLocal().getHeight()/2. );
                                           
                    
                    }
                    
                    if (nextDistance < 0 ){
                        
                        currentX = (double)nextPos[0];
                        currentY = (double)nextPos[1];
                        
                    }
                    
                    
                    
                    double movedPixels;
                    
                    movedPixels = Math.max( currentX - prevInterpolatorTruck1X, currentY - prevInterpolatorTruck1Y) ;
                    prevInterpolatorTruck1X = currentX;
                    prevInterpolatorTruck1Y = currentY;
                    
                    if ( interpolatorFirstTime ){
                    
                        movedPixels = 0;
                        
                        prevInterpolatorTruck1X = currentX;
                        prevInterpolatorTruck1Y = currentY;
                        
                        
                        
                        interpolatorFirstTime = false;
                    
                    }
                    
                    
                    
//                    updateFuelBars(percentageTruck1Path, 1, movedPixels);
                    
                    
                 }
                
                
                
                return t ;
                
            }
        }
        
        tt.setInterpolator(new AnimationPathsInterpolator() );
        
        EventHandler onAnimationFinished = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                
                boolean goalAchieved = true;
                
//                double currentX = ( truck0ImageView.getBoundsInParent().getMinX() + truck0ImageView.getBoundsInParent().getMaxX() )/2. ;
                double currentX = truck0ImageView.getBoundsInParent().getMinX() ;
                currentX = currentX/blockWidth;
                
//                double currentY = ( truck0ImageView.getBoundsInParent().getMinY() + truck0ImageView.getBoundsInParent().getMaxY() )/2. ;
                double currentY = truck0ImageView.getBoundsInParent().getMinY() ;
                currentY = currentY/blockHeight;
                
                int[] goal0Pos = currentLevel.getGoalPos(0);
                double goalX = (double) goal0Pos[0] ;
                double goalY = (double) goal0Pos[1] ;
                
                if ( Math.abs(currentX-goalX) > 0.5 || Math.abs(currentY-goalY) > 0.5 ){
                    goalAchieved = false;
                }
                
                System.out.println("currentX = " + currentX + " currentY = " + currentY);
                System.out.println("goalX = " + goalX + " goalY = " + goalY);
                
                
//                currentX = ( truck1ImageView.getBoundsInParent().getMinX() + truck1ImageView.getBoundsInParent().getMaxX() )/2. ;
                currentX = truck1ImageView.getBoundsInParent().getMinX();
                currentX = currentX/blockWidth;
                
//                currentY = ( truck1ImageView.getBoundsInParent().getMinY() + truck1ImageView.getBoundsInParent().getMaxY() )/2. ;
                currentY = truck1ImageView.getBoundsInParent().getMinY();
                currentY = currentY/blockHeight;
                
                int[] goal1Pos = currentLevel.getGoalPos(1);
                goalX = (double) goal1Pos[0] ;
                goalY = (double) goal1Pos[1] ;
                
                if ( Math.abs(currentX-goalX) > 0.5 || Math.abs(currentY-goalY) > 0.5 ){
                    goalAchieved = false;
                }
                
                System.out.println("goalAchieved = " + goalAchieved);
                
                Group resultGroup = new Group();
                
                if ( goalAchieved ){
                    
                    Rectangle rect = new Rectangle(100, 100, 200, 200);
                    rect.setFill(Color.GREEN);
                    resultGroup.getChildren().add(rect);
                    
                }else{
                    
                    Rectangle rect = new Rectangle(100, 100, 200, 200);
                    rect.setFill(Color.RED);
                    resultGroup.getChildren().add(rect);
                    
                }
                
                System.out.println(" ");
                System.out.println("currentX = " + currentX + " currentY = " + currentY);
                System.out.println("goalX = " + goalX + " goalY = " + goalY);
                
                gameZone.getChildren().add(resultGroup);
                
            }
        };
        
        
        tt.setOnFinished(onAnimationFinished);
        
        tt.play();
        
    }
    
    private void timerButtonClicked(int argIndexTruck, boolean isPlusButton){
        
        if (isPlusButton){
        
            if (argIndexTruck == 0){
                
                if ( truck0Delay == maxHourGlasses ){
                    return;
                }                
                truck0Delay ++;
                
            }else{
                
                if ( truck1Delay == maxHourGlasses ){
                    return;
                } 
                truck1Delay ++;
            }
                        
            System.out.println("Plus Button clicked, on group: "+ argIndexTruck);
            
        }else{
            
            if (argIndexTruck == 0){
                
                if ( truck0Delay == 0 ){
                    return;
                } 
                truck0Delay --;
                
            }else{
                
                if ( truck1Delay == 0 ){
                    return;
                } 
                truck1Delay --;
                
            }
                        
            System.out.println("Minus Button clicked, on group: "+ argIndexTruck);
                    
        }
        
        ImageView tempImageView;
        
        if ( argIndexTruck == 0 ){
            
            int indexToGet = 0;
            
            if (isPlusButton){
                indexToGet = truck0Delay-1;
            }else{
                indexToGet = truck0Delay;
            }
            
            tempImageView = (ImageView) timerGroup0.lookup("#"+indexToGet);
            
        }else{
            
            int indexToGet = 0;
            
            if (isPlusButton){
                indexToGet = truck1Delay-1;
            }else{
                indexToGet = truck1Delay;
            }
            
            tempImageView = (ImageView) timerGroup1.lookup("#"+indexToGet);
            
        }
        
        double currentWidth = tempImageView.getFitWidth();
        
        if (isPlusButton){
            tempImageView.setImage(new Image("file:hourglassFull.png"));
        }else{
            tempImageView.setImage(new Image("file:hourglassEmpty.png"));
        }
        
        
        tempImageView.setFitWidth(currentWidth);
        
        showOtherTruckPath();
        
    }
    
    
    
    private Group createTimerGroup(double argGroupWidth, int argIndexTruck){
        
        Group timerGroup = new Group();
        
        Rectangle rectBG = new Rectangle();
        rectBG.setWidth(argGroupWidth);
        rectBG.setHeight(argGroupWidth);
        double borderArc = 25;
        rectBG.setArcWidth(borderArc);
        rectBG.setArcHeight(borderArc);
        if (argIndexTruck == 0){
            rectBG.setFill(Color.RED);
        }else{
            rectBG.setFill(Color.YELLOW);
        }
        timerGroup.getChildren().add(rectBG);
        
        Image hourglassImage = new Image("file:hourglassEmpty.png");
        Image minusSignImage = new Image("file:minusSign.png");
        Image plusSignImage = new Image("file:plusSign.png");
        
        ImageView hourglassImageView = new ImageView(hourglassImage);;
        ImageView minusSignImageView = new ImageView(minusSignImage);
        ImageView plusSignImageView = new ImageView(plusSignImage);
        
        int numHourglasses = maxHourGlasses;
        double hourglassWidth = argGroupWidth / ((double) numHourglasses);
        
        for ( int i = 0 ; i < numHourglasses ; i++ ){
            
            hourglassImageView = new ImageView(hourglassImage);
            hourglassImageView.setPreserveRatio(true);
            hourglassImageView.setFitWidth( hourglassWidth );
            hourglassImageView.setLayoutX( ((double)i)*hourglassWidth );
            hourglassImageView.setId(""+i);
            timerGroup.getChildren().add(hourglassImageView);
            
        }
        
        
        
        plusSignImageView.setPreserveRatio(true);
        plusSignImageView.setFitWidth(argGroupWidth/2.);
        plusSignImageView.setLayoutY( hourglassImageView.getBoundsInLocal().getHeight() );
        
        plusSignImageView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {

                timerButtonClicked(argIndexTruck, true);

            }
        });
        
        minusSignImageView.setPreserveRatio(true);
        minusSignImageView.setFitWidth(argGroupWidth/2.);
        minusSignImageView.setLayoutX(argGroupWidth/2.);
        minusSignImageView.setLayoutY( hourglassImageView.getBoundsInLocal().getHeight() );
        
        minusSignImageView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {

                timerButtonClicked(argIndexTruck, false);

            }
        });
        
        timerGroup.getChildren().addAll(plusSignImageView, minusSignImageView);
        
        return timerGroup;
        
    }
    
    private void createTruckScoreText(){
        
        double fontSize = 20;
        
        Text truck0Distance = new Text();
        truck0Distance.setText("Distance travelled: "+ 0 + "m ");
        truck0Distance.setFont(new Font(fontSize) );
        truck0Distance.setId("truck0Distance");
        truck0Distance.setBoundsType(TextBoundsType.VISUAL);        
        truck0Distance.setLayoutX( 3*blockWidth );
        truck0Distance.setLayoutY( 0.5*blockHeight + truck0Distance.getBoundsInLocal().getHeight() );
        
        Text truck0PlatoonDistance = new Text();
        truck0PlatoonDistance.setText("Distance platooned: "+ 0 + "m ");
        truck0PlatoonDistance.setFont(new Font(fontSize) );
        truck0PlatoonDistance.setId("truck0PlatoonedDistance");
        truck0PlatoonDistance.setBoundsType(TextBoundsType.VISUAL);        
        truck0PlatoonDistance.setLayoutX( 6*blockWidth );
        truck0PlatoonDistance.setLayoutY( 0.5*blockHeight + truck0PlatoonDistance.getBoundsInLocal().getHeight() );
        
        Text truck1Distance = new Text();
        truck1Distance.setText("Distance travelled: "+ 0 + "m ");
        truck1Distance.setFont(new Font(fontSize) );
        truck1Distance.setId("truck1Distance");
        truck1Distance.setBoundsType(TextBoundsType.VISUAL);        
        truck1Distance.setLayoutX( 3*blockWidth );
        truck1Distance.setLayoutY( 9.5*blockHeight + truck1Distance.getBoundsInLocal().getHeight() );
        
        Text truck1PlatoonDistance = new Text();
        truck1PlatoonDistance.setText("Distance platooned: "+ 0 + "m ");
        truck1PlatoonDistance.setFont(new Font(fontSize) );
        truck1PlatoonDistance.setId("truck1PlatoonedDistance");
        truck1PlatoonDistance.setBoundsType(TextBoundsType.VISUAL);        
        truck1PlatoonDistance.setLayoutX( 6*blockWidth );
        truck1PlatoonDistance.setLayoutY( 9.5*blockHeight + truck1PlatoonDistance.getBoundsInLocal().getHeight() );
        
        gameZone.getChildren().addAll(truck0Distance, truck0PlatoonDistance, truck1Distance, truck1PlatoonDistance);
        
    }
    
    private void updateTruckScoreText(double percentageTruckPath, int truckIndex){
        
        double meters;
        
        DecimalFormat dfDistance = new DecimalFormat("00");  
        int startingPlatoonIndex = 1000;
        int endingPlatoonIndex = -1;
        
        for ( int i = 0 ; i < Math.min(arrow0Sections.size()-1, arrow1Sections.size()-1 )  ; i++ ){
            
            if ( i-1 < arrow1Sections.size() && i-1 >= 0 ){
                
            }else{
                continue;
            }
            
            if ( Arrays.equals( arrow0Sections.get(i), arrow1Sections.get(i-1) ) ){
                
                if ( i > endingPlatoonIndex){
                    
                    endingPlatoonIndex = i;
                    
                }
                
                if ( i < startingPlatoonIndex){
                    
                    startingPlatoonIndex = i;
                    
                }
                
            }
            
        }
        
        if ( endingPlatoonIndex == -1){
            
            for ( int i = 0 ; i < Math.min(arrow1Sections.size()-1, arrow0Sections.size()-1 )  ; i++ ){
            
                if ( i-1 < arrow0Sections.size()  && i-1 >= 0 ){

                }else{
                    continue;
                }

                if ( Arrays.equals( arrow1Sections.get(i), arrow0Sections.get(i-1) ) ){

                    if ( i > endingPlatoonIndex){

                        endingPlatoonIndex = i;

                    }

                    if ( i < startingPlatoonIndex){

                        startingPlatoonIndex = i;

                    }

                }

            }
            
        }
        
        
        if ( truckIndex == 0 ){
        
            meters = percentageTruckPath*( (double) arrow0Sections.size() );
            
            
            
            Text distanceText = (Text) gameZone.lookup("#truck0Distance");
            distanceText.setText("Distance travelled: "+ dfDistance.format(meters) + "m ");

            if ( meters - startingPlatoonIndex > 0 && meters < endingPlatoonIndex ){
            
                Text distancePlatoonedText = (Text) gameZone.lookup("#truck0PlatoonedDistance");
                distancePlatoonedText.setText("Distance platooned: "+ dfDistance.format(meters - startingPlatoonIndex) + "m ");
            
            }
        
        }else{
            
            meters = percentageTruckPath*( (double) arrow1Sections.size() );
            
            Text distanceText = (Text) gameZone.lookup("#truck1Distance");
            distanceText.setText("Distance travelled: "+ dfDistance.format(meters) + "m ");

            if ( meters - startingPlatoonIndex > 0 && meters < endingPlatoonIndex ){
            
                Text distancePlatoonedText = (Text) gameZone.lookup("#truck1PlatoonedDistance");
                distancePlatoonedText.setText("Distance platooned: "+ dfDistance.format(meters - startingPlatoonIndex) + "m ");
            
            }
            
            
        }
        
        
    }
    
    private void newFingerSection(int[] argMapSection, boolean clicked){
        
//        System.out.println("---newFingerSection---");
        
        int[] playButton = {0, 0};
        
        if ( playPressed ){
                
                return;
                
        }
        
        if ( Arrays.equals(argMapSection, playButton) && clicked){
            
            System.out.println("Play button pressed");
            
            if ( playPressed ){
                
                return;
                
            }
            
            if ( arrow0Sections.size()>1 && arrow1Sections.size()>1 ){
                
                playSelectedPaths();
                System.out.println("clicked Play and played");
                
                playPressed = true;
                
            }else{
                
                System.out.println("clicked Play and did nothing");
                
            }
            
            return;
        
        }
        
        
        int truckClicked = isTruckClicked(argMapSection);
        
        if ( clicked && !choosingPath ){
            
            System.out.println("Something is clicked withle path is being chosen");
            
            if (truckClicked == -1){
                // No truck clicked
                System.out.println("Not choosing a path and no truck clicked, do nothing");
                return;
                
            }   else if ( truckClicked == 0) {
                // Clicked first truck
                System.out.println("Not choosing a path and clicked truck #0, start a path");
                choosingPath = true;
                
                currentArrowIndex = 0;
                
                arrow0Sections.clear();
                arrow0Sections.add( convertArrayToInteger(argMapSection) );
                eraseAndDrawNewArrow(currentArrowIndex);
                
                return;
                
            } else {
                // Clicked second truck
                System.out.println("Not choosing a path and clicked truck #1, code not done for this situation");
                choosingPath = true;
                
                currentArrowIndex = 1;
                
                arrow1Sections.clear();
                arrow1Sections.add( convertArrayToInteger(argMapSection) );
                eraseAndDrawNewArrow(currentArrowIndex);
                
                return;
            }
            
        }
        
        if ( clicked && choosingPath && truckClicked != -1 ){
            
            System.out.println("Clicked a truck while choosing a path");
            
            if (truckClicked == 0 && currentArrowIndex == 1){
            
                arrow0Sections.clear();
                arrow0Sections.add( convertArrayToInteger(argMapSection) );
                currentArrowIndex = 0;
                eraseAndDrawNewArrow(currentArrowIndex);
                
                return;
                
            }
            
            if (truckClicked == 1 && currentArrowIndex == 0){
            
                arrow1Sections.clear();
                arrow1Sections.add( convertArrayToInteger(argMapSection) );
                currentArrowIndex = 1;
                eraseAndDrawNewArrow(currentArrowIndex);
                
                return;
            
            }
            
            updatePath(argMapSection, currentArrowIndex);
            
//            choosingPath = false;
//            System.out.println("Path is finished choosing");
            return;
        }
        
        
        if ( !clicked && choosingPath ){
            
//            System.out.println("Choosing path and not clicking");
            
            updatePath(argMapSection, currentArrowIndex);
            showOtherTruckPath();
            return;
//            System.out.println("Path is being chosen");
        }
        if ( !clicked && !choosingPath ){
//            System.out.println("Not doing anything");
            return;
        }
        
        
        
        
        
    }
    
    private void showOtherTruckPath(){
                
        int currentLength;
        int[] currentOtherTruckPos = new int[2];
        
        List<Integer[]> tempArrowSectionsA, tempArrowSectionsB;
        int truckDelayA, truckDelayB;
        Image truckLeftImage, truckRightImage, truckFrontImage, truckBackImage;
                
        if ( currentArrowIndex == 0 ){
            
            tempArrowSectionsA = arrow0Sections;
            tempArrowSectionsB = arrow1Sections;
            truckDelayA = truck0Delay;
            truckDelayB = truck1Delay;
            truckLeftImage = truckYellowLeftImage;
            truckRightImage = truckYellowRightImage;
            truckFrontImage = truckYellowFrontImage;
            truckBackImage = truckYellowBackImage;
            
        }else{
            
            tempArrowSectionsA = arrow1Sections;
            tempArrowSectionsB = arrow0Sections;
            truckDelayA = truck1Delay;
            truckDelayB = truck0Delay;
            truckLeftImage = truckRedLeftImage;
            truckRightImage = truckRedRightImage;
            truckFrontImage = truckRedFrontImage;
            truckBackImage = truckRedBackImage;
            
        }
        
        currentLength = tempArrowSectionsA.size() + truckDelayA - truckDelayB;
            
            if ( currentLength == 0 || tempArrowSectionsB.isEmpty()  ){
                
                return;
                
            }
            
            
            if (tempArrowSectionsB.size() + truckDelayB < tempArrowSectionsA.size() + truckDelayA ){
                return;                
            }
            
        
            int[] prevPos = new int[2];
            int[] currentPos = new int[2];
            
            
            if ( tempArrowSectionsA.size() + truckDelayA - truckDelayB <= 0 ){
                
//                Truck preview is in past time
                
                prevPos = convertArrayToInt( tempArrowSectionsB.get( 0 ) );
                currentPos = convertArrayToInt( tempArrowSectionsB.get( 0 ) );
                
            }else{
                
                
                if ( tempArrowSectionsB.size() > tempArrowSectionsA.size() + truckDelayA - truckDelayB){

                    prevPos = convertArrayToInt( tempArrowSectionsB.get( tempArrowSectionsA.size()-1 + truckDelayA - truckDelayB ) );
                    currentPos = convertArrayToInt( tempArrowSectionsB.get( tempArrowSectionsA.size() + truckDelayA - truckDelayB ) );

                }else{

                    prevPos = convertArrayToInt( tempArrowSectionsB.get( tempArrowSectionsA.size()-2 + truckDelayA - truckDelayB ) );
                    currentPos = convertArrayToInt( tempArrowSectionsB.get( tempArrowSectionsA.size()-1 + truckDelayA - truckDelayB) );

                }
                
                
            
            }
            
            currentOtherTruckPos = convertArrayToInt( tempArrowSectionsB.get ( Math.max( Math.min(currentLength - 1, tempArrowSectionsB.size() - 1 ) , 0 ) ) );
            
            double deltaX = currentPos[0] - prevPos[0];
            double deltaY = currentPos[1] - prevPos[1];
            
            double imageWidth = 0;
            
            if ( deltaX > 0 ){
                        
                otherTruckPreview.setImage(truckRightImage);
                imageWidth = truckRightImage.getWidth();

            } 
            if ( deltaX < 0 ){

                otherTruckPreview.setImage(truckLeftImage);
                imageWidth = truckLeftImage.getWidth();

            } 
            if ( deltaY > 0 ){

                otherTruckPreview.setImage(truckFrontImage);
                imageWidth = truckFrontImage.getWidth();

            }
            if ( deltaY < 0 ){

                otherTruckPreview.setImage(truckBackImage);
                imageWidth = truckBackImage.getWidth();

            }

//            System.out.println("imageWidth = " + imageWidth);
            
            if (imageWidth != 0){

                otherTruckPreview.setPreserveRatio(true);
                otherTruckPreview.setFitWidth(imageWidth*truckScalingRatio);

            }
        
        
        
        
              
        otherTruckPreview.setOpacity(previewTruckOpacity);
        
        double positionX = ( (double)currentOtherTruckPos[0] + 0.5)*blockWidth;
        double positionY = ( (double)currentOtherTruckPos[1] + 0.5)*blockHeight;
        otherTruckPreview.setLayoutX(positionX - otherTruckPreview.getBoundsInLocal().getWidth()/2.);
        otherTruckPreview.setLayoutY(positionY - otherTruckPreview.getBoundsInLocal().getHeight()/2.);
        
//        Circle tempCircle = new Circle( ((double)startSectionX + .5 )*blockWidth , ((double)startSectionY + .5 )*blockWidth , blockWidth/4.);
        
        otherTruckPreview.toFront();
        
    }
    
    private boolean validMovement(int[] argLastSection, int[] argMapSection){
        
        
        if ( map == null ){
            
            System.out.println("Map not defined in validMovement");
            return false;
            
        }
        
        int movement = -1;
        
        if ( Arrays.equals( argMapSection , argLastSection)   ){
        
            System.out.println("Moving diagonally, invalid movement.");
            return false;
            
        }
        
        if ( ( Math.abs( argMapSection[0] - argLastSection[0] ) > 1 ) || ( Math.abs( argMapSection[1] - argLastSection[1] ) > 1 ) ){
            
            System.out.println("Moving not adjacent");
            return false;
            
        }        
        
        if ( argMapSection[0] > argLastSection[0] ){
            movement = 1; // right
        }
        if ( argMapSection[0] < argLastSection[0] ){
            movement = 3; // left
        }
        if ( argMapSection[1] > argLastSection[1] ){
            movement = 4; // down
        }
        if ( argMapSection[1] < argLastSection[1] ){
            movement = 2; // up
        }
        
        boolean validMovement = false;
        
        switch (movement) {
                
            case 1: // right
                validMovement = checkValidMovementHorizontal(argLastSection, argMapSection);
                
                break;
            case 2: // up
                validMovement = checkValidMovementVertical(argLastSection, argMapSection);
                break;
            case 3: // left
                validMovement = checkValidMovementHorizontal(argMapSection, argLastSection);
                break;
            case 4: // down
                validMovement = checkValidMovementVertical(argMapSection, argLastSection);
                break;
            default: 
                validMovement = false;
                break;
        }
                
        return validMovement;
        
    }
    
    private boolean checkValidMovementHorizontal(int[] argLastSection, int[] argMapSection){
        
        // Redundant functions, Right and Left can be turned into lateral
        
        boolean valid;
        
        int roadId;
        
        // To check if departure is correct
        
        boolean validDeparture = false;
        
        roadId = map[ argLastSection[0] ][ argLastSection[1] ];
        
        if ( roadId == 4 || roadId == 5 || roadId == 7 || roadId == 9 || roadId == 2 || roadId == 12 || roadId == 13 || roadId == 14 ){
            
            validDeparture = true;
            
        }
        
        // To check if arrival is correct
        
        boolean validArrival = false;
        
        roadId = map[ argMapSection[0] ][ argMapSection[1] ];
        
        if ( roadId == 3 || roadId == 6 ||roadId == 7 || roadId == 11 || roadId == 2 || roadId == 12 || roadId == 14 || roadId == 15 ){
            
            validArrival = true;
            
        }
        
        valid = validDeparture && validArrival;
        
        return valid;
    
    }
    
    private boolean checkValidMovementVertical(int[] argLastSection, int[] argMapSection){
        
        boolean valid;
        
        int roadId;
        
        // To check if departure is correct
        
        boolean validDeparture = false;
        
        roadId = map[ argLastSection[0] ][ argLastSection[1] ];
        
        if ( roadId == 3 || roadId == 4 || roadId == 7 || roadId == 8 || roadId == 1 || roadId == 12 || roadId == 13 || roadId == 15 ){
            
            validDeparture = true;
            
        }
        
        // To check if arrival is correct
        
        boolean validArrival = false;
        
        roadId = map[ argMapSection[0] ][ argMapSection[1] ];
        
        if ( roadId == 5 || roadId == 6 ||roadId == 7 || roadId == 10 || roadId == 1 || roadId == 13 || roadId == 14 || roadId == 15 ){
            
            validArrival = true;
            
        }
        
        valid = validDeparture && validArrival;
                
        return valid;
    
    }
    
    public void trackFinger(){
        
        gameZone.addEventFilter(
            MouseEvent.ANY,
            new EventHandler<MouseEvent>() {
                public void handle(final MouseEvent mouseEvent) {
                    
//                      System.out.println("mouseEvent.getSceneX() = " + mouseEvent.getSceneX() + "mouseEvent.getSceneY() = " + mouseEvent.getSceneY());
                    double currentCursorX = mouseEvent.getSceneX();
                    double currentCursorY = mouseEvent.getSceneY();
                    
                    int[] currentMapSection = getCurrentMapSection(currentCursorX, currentCursorY);
                    
                    if ( currentMapSection == null ){
                        // The mouse is outside the playing space    
                        return;
                    }

                              
                    boolean clicked;
                        
                    if ( mouseEvent.getButton() == MouseButton.PRIMARY && mouseEvent.getEventType() == MouseEvent.MOUSE_PRESSED) {
                        
                        clicked = true;
                        
                        lastSection[0] = currentMapSection[0];
                        lastSection[1] = currentMapSection[1];
                        newFingerSection(currentMapSection, clicked);
                        
                        return;

                    }else{

                        clicked = false;                            

                    }
                    
                    if ( Arrays.equals(currentMapSection, lastSection) ){
                    
//                        System.out.println("currentMapSection: [" + currentMapSection[0] + "," + currentMapSection[1] + "]");
//                        newFingerSection(currentMapSection, clicked);
                                                
                    }else{
                                    
                        
                        newFingerSection(currentMapSection, clicked);
//
//                        System.out.println("Left button clicked");
//
//                        startArrow();

                        
                        
                        
                        lastSection[0] = currentMapSection[0];
                        lastSection[1] = currentMapSection[1];
                        
                    }
                    
                    
                    
                }
            });

        
    }
    
    
    private int[] getCurrentMapSection(double xCursor, double yCursor){
        
        int[] mapSection = new int[2];
        
        int mapSectionX, mapSectionY;
        
        mapSectionX = (int) (xCursor/blockWidth) ;
        mapSectionY = (int) (yCursor/blockHeight) ;
        
        if ( mapSectionX >= numberBlocksX || mapSectionY >= numberBlocksY || mapSectionX < 0 || mapSectionY < 0 ){
            
//            System.out.println("Error: Invalid map section, returning NULL");
            return null;
            
        }
        
        mapSection[0] = mapSectionX;
        mapSection[1] = mapSectionY;
        
        return mapSection;
        
    }
    
    
    
    
    
    
    
}
