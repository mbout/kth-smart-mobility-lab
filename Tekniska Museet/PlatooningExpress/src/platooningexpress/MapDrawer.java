/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package platooningexpress;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.transform.Rotate;

/**
 *
 * @author rui
 */
public class MapDrawer {
        
    double screenWidth = Globals.screenWidth;
    double screenHeight = Globals.screenHeight;
    
    int numberBlocksX = Globals.numberBlocksX;
    int numberBlocksY = Globals.numberBlocksY;
    
    double blockWidth = Globals.blockWidth;
    double blockHeight = Globals.blockHeight;
    
    boolean mapLoaded = false;
    
//    int[][] map;
    Level currentLevel;
    
    Image crossRoadImg = new Image("file:crossRoad.png");
    Image curvedRoadImg = new Image("file:curveRoad.png");
    Image straightRoadImg = new Image("file:straightRoad.png");
    Image endRoadImg = new Image("file:endRoad.png");
    Image junctionRoadImg = new Image("file:junctionRoad.png");
    
    Image frontTruckImg = new Image("file:frontTruckRed.png");
    Image sideRightTruckImg = new Image("file:frontTruckYellow.png");
    
    public Level getLevel(){
        
        if ( !mapLoaded ){
            
            return null;
            
        }
        
        return currentLevel;
        
    }
    
    private void createMap(int mapNumber){
        
        
        
        switch (mapNumber) {
                
            case 0: loadMap0();
                    mapLoaded = true;
                    break;
            case 1: loadMap1();
                    mapLoaded = true;
                    break;
            case 2: loadMap2();
                mapLoaded = true;
                break;
//            case 12: monthString = "December";
//                     break;
            default: mapLoaded = false;
                    break;
        }
        
        
    }
    
    private void loadMap0(){
        
        int[][] map = new int[numberBlocksX][numberBlocksY];
        
        for( int i = 0 ; i < 10 ; i++ ){
            
            for( int j = 0 ; j < 10 ; j++ ){
            
                map[i][j] = 0;
            
            }
                        
        }
        
        map[8][0] = 10;
        
        map[8][1] = 3;
        map[7][1] = 2;
        map[6][1] = 2;
        map[5][1] = 2;
        map[4][1] = 5;
        map[4][2] = 1;
        map[4][3] = 1;
        map[4][4] = 1;
        
        map[2][5] = 5;
        map[3][5] = 2;
        map[4][5] = 7;
        map[5][5] = 2;
        map[6][5] = 2;
        map[7][5] = 6;
        
        map[2][6] = 1;
        map[4][6] = 1;
        map[7][6] = 1;
        
        map[2][7] = 1;
        map[4][7] = 1;
        map[7][7] = 1;
        
        map[0][8] = 9;
        map[1][8] = 2;
        map[2][8] = 3;
        map[4][8] = 13;
        map[5][8] = 2;
        map[6][8] = 2;
        map[7][8] = 12;
        map[8][8] = 2;
        map[9][8] = 11;
        
        map[4][9] = 8;
        
        int[] truck0Pos = {8, 0};
        int[] truck1Pos = {0, 8};
        
        int[] goal0Pos = {9, 3};
        int[] goal1Pos = {9, 7};
        
        int truck0Fuel = 12;
        int truck1Fuel = 19;
        
        currentLevel = new Level(map, truck0Pos, truck1Pos, goal0Pos, goal1Pos, null, null, null, null, truck0Fuel, truck1Fuel);
        
        
    }
    
    private void loadMap1(){
        
        int[][] map = new int[numberBlocksX][numberBlocksY];
        
        for( int i = 0 ; i < 10 ; i++ ){
            
            for( int j = 0 ; j < 10 ; j++ ){
            
                map[i][j] = 0;
            
            }
                        
        }
        
        map[9][3] = 11; // Red house
        map[9][7] = 11; // Yellow house
        
        map[2][1] = 5;
        map[3][1] = 2;
        map[4][1] = 2;
        map[5][1] = 2;
        map[6][1] = 2;
        map[7][1] = 11;
        
        map[2][2] = 1;
        
        map[2][3] = 1;
        map[8][3] = 5;
        
        map[2][4] = 1;
        map[8][4] = 1;
        
        map[2][5] = 13;
        map[3][5] = 2;
        map[4][5] = 2;
        map[5][5] = 2;
        map[6][5] = 2;
        map[7][5] = 2;
        map[8][5] = 15;
        
        map[2][6] = 13;
        map[3][6] = 2;
        map[4][6] = 6;
        map[8][6] = 1;
        
        map[2][7] = 1;
        map[4][7] = 1;
        map[8][7] = 4;
        
        map[0][8] = 5;
        map[1][8] = 2;
        map[2][8] = 12;
        map[3][8] = 2;
        map[4][8] = 3;        
        
        map[0][9] = 8;
        
        int[] truck0Pos = {7, 1};
        int[] truck1Pos = {0, 9};
        
        System.out.println("Arrays.toString(map[2]) = " + Arrays.toString(map[2]));
        
        List<Integer[]> truck0Packages = new ArrayList<>();   
        
        Integer[] tempPackage = {2, 4};
        
        truck0Packages.add(tempPackage);
        
        List<Integer[]> truck1Packages = new ArrayList<>();   
        
        tempPackage[0] = 2; tempPackage[1] = 6;
        
        truck1Packages.add(tempPackage);
        
        List<Integer[]> truck0Home = new ArrayList<>();   
        
        Integer[] tempHome = {8, 3};
        
        truck0Home.add(tempHome);
        
        List<Integer[]> truck1Home = new ArrayList<>();   
        
        tempHome[0] = 8; tempHome[1] = 7;
        
        truck1Home.add(tempHome);
        
        int[] goal0Pos = {9, 3};
        int[] goal1Pos = {9, 7};
        
        int truck0Fuel = 19;
        int truck1Fuel = 12;
        
        currentLevel = new Level(map, truck0Pos, truck1Pos, goal0Pos, goal1Pos, truck0Packages, truck1Packages, truck0Home, truck1Home, truck0Fuel, truck1Fuel);
        
    }
    
    private void loadMap2(){
        
        int[][] map = new int[numberBlocksX][numberBlocksY];
        
        for( int i = 0 ; i < 10 ; i++ ){
            
            for( int j = 0 ; j < 10 ; j++ ){
            
                map[i][j] = 0;
            
            }
                        
        }
        
        
//        map[0][2] = 9; // Red house
        map[2][4] = 3; // Yellow house
        
        
        
//        map[1][2] = 2;
//        map[2][2] = 2;
//        map[3][2] = 2;
        map[4][2] = 5;
        map[5][2] = 2;
        map[6][2] = 2;
        map[7][2] = 14;
        map[8][2] = 2;
        map[9][2] = 6;
        
        map[0][3] = 9;
        map[1][3] = 14;
        map[2][3] = 14;
        map[3][3] = 2;
        map[4][3] = 7;
        map[5][3] = 2;
        map[6][3] = 2;
        map[7][3] = 15;
        map[9][3] = 1;
        
        map[1][4] = 13;
        map[4][4] = 1;
        map[7][4] = 1;
        map[9][4] = 8;
        
        
        map[1][5] = 1;
        map[4][5] = 1;
        map[7][5] = 1;
        
        map[1][6] = 4;
        map[2][6] = 2;
        map[3][6] = 2;
        map[4][6] = 12;
        map[5][6] = 2;
        map[6][6] = 2;
        map[7][6] = 12;
        map[8][6] = 2;
        map[9][6] = 6;
        
        map[9][7] = 8;
        
        int[] truck0Pos = {9,4};
        int[] truck1Pos = {9,7};
        
        System.out.println("Arrays.toString(map[2]) = " + Arrays.toString(map[2]));
        
        List<Integer[]> truck0Packages = new ArrayList<>();   
        
        Integer[] tempPackage = {2, 4};
        
        truck0Packages.add(tempPackage);
        
        List<Integer[]> truck1Packages = new ArrayList<>();   
        
        tempPackage[0] = 2; tempPackage[1] = 6;
        
        truck1Packages.add(tempPackage);
        
        List<Integer[]> truck0Home = new ArrayList<>();   
        
        Integer[] tempHome = {8, 3};
        
        truck0Home.add(tempHome);
        
        List<Integer[]> truck1Home = new ArrayList<>();   
        
        tempHome[0] = 8; tempHome[1] = 7;
        
        truck1Home.add(tempHome);
        
        int[] goal0Pos = {0, 3};
        int[] goal1Pos = {2, 4};
        
        int truck0Fuel = 14;
        int truck1Fuel = 8;
        
        currentLevel = new Level(map, truck0Pos, truck1Pos, goal0Pos, goal1Pos, truck0Packages, truck1Packages, truck0Home, truck1Home, truck0Fuel, truck1Fuel);
        
    }
    
    public Group getMapBackground(){
        
        Group backgroundGroup = new Group();
        
        Rectangle backgroundColor = new Rectangle(0, 0, numberBlocksX*blockWidth, numberBlocksY*blockHeight);
        backgroundColor.setFill(Color.GREENYELLOW);
        
        backgroundGroup.getChildren().add(backgroundColor);
        
        return backgroundGroup;
        
        
    }
    
    public Group getMapDrawing(int mapNumber){

        createMap(mapNumber);
        
        int[][] map = currentLevel.getMap();
        
        Group mapGroup = new Group();
        
        if (!mapLoaded){
            
            System.out.println("Asking for a map, when no map was loaded!");
            return null;
            
        }
        
        int typeRoad;
        ImageView tempRoadImageView;
        
        
        for( int i = 0 ; i < 10 ; i++ ){
            
            for( int j = 0 ; j < 10 ; j++ ){
            
                typeRoad = map[i][j];
//                System.out.println("map[i][j] = " + map[i][j]);
                tempRoadImageView = getRoadBlock(typeRoad, i, j);
                
                if (tempRoadImageView != null){
//                    System.out.println("Added road");
                    mapGroup.getChildren().add(tempRoadImageView);
                                        
                }
                            
            }
                        
        }
        
        double positionX, positionY;
        
        Text OKRed = new Text();
        
        double fontSize = 30;
        
        OKRed.setText("OK");
        OKRed.setFill(Color.RED);   
        OKRed.setFont(new Font(fontSize));
        positionX = 0.5*blockWidth;
        positionY = 0.5*blockHeight;
        OKRed.setLayoutX(positionX - OKRed.getBoundsInLocal().getWidth()/2.);
        OKRed.setLayoutY(positionY - OKRed.getBoundsInLocal().getHeight()/2.);
        
        Text restartRed = new Text();
        
        restartRed.setText("CLEAR");
        restartRed.setFill(Color.RED);
        restartRed.setFont(new Font(fontSize));
        positionX = 1.5*blockWidth;
        positionY = 0.5*blockHeight;
        restartRed.setLayoutX(positionX - restartRed.getBoundsInLocal().getWidth()/2.);
        restartRed.setLayoutY(positionY - restartRed.getBoundsInLocal().getHeight()/2.);
        
        Text OKYellow = new Text();
        
        OKYellow.setText("OK");
        OKYellow.setFill(Color.YELLOW);
        OKYellow.setFont(new Font(fontSize));
        positionX = 0.5*blockWidth;
        positionY = 1.5*blockHeight;
        OKYellow.setLayoutX(positionX - OKYellow.getBoundsInLocal().getWidth()/2.);
        OKYellow.setLayoutY(positionY - OKYellow.getBoundsInLocal().getHeight()/2.);
        
        Text restartYellow = new Text();
        
        restartYellow.setText("CLEAR");
        restartYellow.setFill(Color.YELLOW); 
        restartYellow.setFont(new Font(fontSize));
        positionX = 1.5*blockWidth;
        positionY = 1.5*blockHeight;
        restartYellow.setLayoutX(positionX - restartYellow.getBoundsInLocal().getWidth()/2.);
        restartYellow.setLayoutY(positionY - restartYellow.getBoundsInLocal().getHeight()/2.);
        
        Text playText = new Text();
        
        playText.setText("PLAY");
        playText.setFill(Color.BLUE);   
        playText.setFont(new Font(fontSize));
        positionX = 0.5*blockWidth;
        positionY = 0.5*blockHeight;
        playText.setLayoutX(positionX - playText.getBoundsInLocal().getWidth()/2.);
        playText.setLayoutY(positionY - playText.getBoundsInLocal().getHeight()/2.);
        
        mapGroup.getChildren().add(playText);
//        mapGroup.getChildren().addAll(OKRed, restartRed, OKYellow, restartYellow, playText);
        
        return mapGroup;
        
    }
    
    private ImageView getRoadBlock(int typeRoad, int currentIndexX, int currentIndexY){
        
        ImageView currentRoadSection = null;
        
        double toRotate = 0;
        
        switch (typeRoad) {
                
            case 0:
                currentRoadSection = null;
                break;
            case 1:
                currentRoadSection = new ImageView(straightRoadImg);
                break;
            case 2:
                currentRoadSection = new ImageView(straightRoadImg);
//                currentRoadSection.getTransforms().add(new Rotate(90));
                toRotate = -90;
                break;
            case 3:
                currentRoadSection = new ImageView(curvedRoadImg);
                break;
            case 4:
                currentRoadSection = new ImageView(curvedRoadImg);
//                currentRoadSection.getTransforms().add(new Rotate(-90));
                toRotate = 90;
                break;
            case 5:
                currentRoadSection = new ImageView(curvedRoadImg);
//                currentRoadSection.getTransforms().add(new Rotate(180));
                toRotate = 180;
                break;
            case 6:
                currentRoadSection = new ImageView(curvedRoadImg);
//                currentRoadSection.getTransforms().add(new Rotate(90));
                toRotate = -90;
                break;
            case 7:
                currentRoadSection = new ImageView(crossRoadImg);
                break;
            case 8:
                currentRoadSection = new ImageView(endRoadImg);
                break;
            case 9:
                currentRoadSection = new ImageView(endRoadImg);
//                currentRoadSection.getTransforms().add(new Rotate(-90));
                toRotate = 90;
                break;
            case 10:
                currentRoadSection = new ImageView(endRoadImg);
//                currentRoadSection.getTransforms().add(new Rotate(180));
                toRotate = 180;
                break;
            case 11:
                currentRoadSection = new ImageView(endRoadImg);
//                currentRoadSection.getTransforms().add(new Rotate(90));
                toRotate = -90;
                break;
            case 12:
                currentRoadSection = new ImageView(junctionRoadImg);
                break;                
            case 13:
                currentRoadSection = new ImageView(junctionRoadImg);
//                currentRoadSection.getTransforms().add(new Rotate(-90));
                toRotate = 90;
                break;                      
            case 14:
                currentRoadSection = new ImageView(junctionRoadImg);
//                currentRoadSection.getTransforms().add(new Rotate(180));
                toRotate = 180;
                break;                      
            case 15:
                currentRoadSection = new ImageView(junctionRoadImg);
//                currentRoadSection.getTransforms().add(new Rotate(90));
                toRotate = -90;
                break;                      
            default: mapLoaded = false;
                break;
        }

        if (currentRoadSection == null ){
            
            return currentRoadSection;
            
        }
        
        currentRoadSection.setPreserveRatio(true);
        currentRoadSection.setFitWidth(blockWidth);
        
        currentRoadSection.getTransforms().add(new Rotate(toRotate, blockWidth/2., blockWidth/2.));
        
        double positionX = currentIndexX*blockWidth;
        double positionY = currentIndexY*blockHeight;
        
        currentRoadSection.setLayoutX(positionX);
        currentRoadSection.setLayoutY(positionY);
        
        return currentRoadSection;
        
    }
    
    
    
}
