function varargout = start(varargin)
% START MATLAB code for start.fig
%      START, by itself, creates a new START or raises the existing
%      singleton*.
%
%      H = START returns the handle to a new START or the handle to
%      the existing singleton*.
%
%      START('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in START.M with the given input arguments.
%
%      START('Property','Value',...) creates a new START or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before start_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to start_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help start

% Last Modified by GUIDE v2.5 12-Nov-2014 18:44:58

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @start_OpeningFcn, ...
                   'gui_OutputFcn',  @start_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before start is made visible.
function start_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to start (see VARARGIN)

% Choose default command line output for start
handles.output = hObject;
%set(gcf,'units','normalized','outerposition',[0 0 1 1]);

% Update handles structure

global handles_v temp stop_video count stop_r
stop_video = false;
handles_v=handles;
count=0;
stop_r=0;
temp=get(handles.start,'WindowButtonDownFcn');
set(handles.start,'WindowButtonDownFcn','');

addpath winontop;
set(handles.start_button,'Visible','on');
set(handles.touch,'Visible','off');
set(handles.start,'Visible','on');
set(handles.button,'Visible','off');
set(handles.message,'Visible','off');
set(handles.im1,'Visible','off');
set(handles.im2,'Visible','off');
set(handles.im3,'Visible','off');
set(handles.im4,'Visible','off');
set(handles.sml_text,'Visible','off');
set(handles.kth_logo,'Visible','off');
set(handles.first,'Visible','off');
set(handles.exp2,'Visible','off');
set(handles.exp3,'Visible','off');
set(handles.exp4,'Visible','off');
set(handles.left,'Visible','off');
set(handles.right,'Visible','off');
WinOnTop(handles.start);
%set(handles.video,'Position',[1950 120 500 500])
guidata(hObject, handles);


%close start


%close start

%videoFReader = vision.VideoFileReader('sml.mp4');
%imshow(imread('sml.jpg'),'Parent',handles.video)


% UIWAIT makes start wait for user response (see UIRESUME)
% uiwait(handles.start);


% --- Outputs from this function are returned to the command line.
function varargout = start_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
%varargout{1} = handles.output;


% --- Executes on mouse press over figure background, over a disabled or
% --- inactive control, or over an axes background.
function start_WindowButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global stop_video
stop_video=true;
%control;


 

% --- Executes on button press in button.
function button_Callback(hObject, eventdata, handles)
% hObject    handle to button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global count
set(handles.video,'Visible','off');
set(handles.message,'Visible','off');
set(handles.kth_logo,'Visible','off');
set(handles.sml_text,'Visible','off');
set(handles.im1,'Visible','off');
set(handles.im2,'Visible','off');
set(handles.im3,'Visible','off');
set(handles.im4,'Visible','off');
cla(handles.kth_logo);
switch count
    case 0
        set(handles.exp3,'Visible','on');
        set(handles.exp3,'String','In order to platoon both trucks need to reach the intersection at the same time so the driver''s truck can join the platoon');
        cla(handles.im1);
        imshow(imread('cap4.jpg'),'Parent',handles.im4);
        count=1;
        drawnow
       
    case 1
        set(handles.exp3,'Visible','off');
        set(handles.first,'Visible','on');
        set(handles.first,'String','This is the point of view of the driver''s truck')
        cla(handles.im4);
        set(handles.im2,'Visible','on');
        imshow(imread('cap2.jpg'),'Parent',handles.im2);
        count=2;
        drawnow
    case 2
        set(handles.first,'Visible','off')
        set(handles.exp4,'Visible','on');
        set(handles.exp4,'String','This is the sideview of the platoon')
        cla(handles.im2);
        set(handles.im3,'Visible','on');
        imshow(imread('cap3.jpg'),'Parent',handles.im3);
        count=3;
        drawnow
    case 3
        set(handles.exp4,'Visible','off');
        set(handles.exp2,'Visible','on');
        set(handles.exp2,'String','During the simulation it is possible to track the speeds of the trucks both in km/h and m/s. There is also information of the distance of the trucks to the intersection that can be measured both in time and physical distance. ')
        cla(handles.im3);
        imshow(imread('cap5.jpg'),'Parent',handles.im1);
        count=4;
        drawnow
    case 4
        cla(handles.im1);
        set(handles.exp2,'String','In the auto mode the simulation achieves automatically a platoon between the trucks. In the manual mode the user controls the driver''s truck')
        imshow(imread('cap1.jpg'),'Parent',handles.im1);
        count=5;
        drawnow
    case 5 
        cla(handles.im1);
        cla(handles.video);
        set(handles.exp2,'Visible','off');
        set(handles.start,'Color',[1 1 1])
        set(handles.video,'Color',[1 1 1])
        set(handles.button,'Visible','off');
        
        set(handles.touch,'Visible','on');
        set(handles.touch,'BackgroundColor',[1 1 1]);
        set(handles.touch,'ForegroundColor',[0 0 0]);
        
        set(handles.touch,'String','In auto mode to control the truck use the steering wheel, the right paddle to increase your speed and the left one to brake. To continue press both the left and right paddles!')
        set(handles.touch,'FontSize',30);
        wheel=imread('wheel.jpg');
        dw=double(wheel);
        dw(dw==0)=50;
        wheel=uint8(dw);
        imshow(wheel,'Parent',handles.video);
        rotate_wheel(handles.video,wheel,handles.left,handles.right);
        drawnow
        control
        close start     
end


% --- Executes on button press in start_button.
function start_button_Callback(hObject, eventdata, handles)
% hObject    handle to start_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global stop_video temp
set(handles.start,'WindowButtonDownFcn',temp);

set(handles.start,'Position',[0 10 1920 1080]);
set(handles.start_button,'Visible','off');
set(handles.touch,'Visible','on');
set(handles.video,'xtick',[],'ytick',[])
videoFReader = vision.VideoFileReader('sml.mp4');
k=1;
while ~isDone(videoFReader) && ~stop_video
    reset(handles.video)
    set(handles.video,'xtick',[],'ytick',[])
    videoFrame = step(videoFReader);
    imagesc(videoFrame,'Parent',handles.video);
    drawnow
    if rem(k,10)==0
       status=get(handles.touch,'Visible');
       if strcmp(status,'on')
          set(handles.touch,'Visible','off');
       elseif strcmp(status,'off')
          set(handles.touch,'Visible','on');
       end
    end
    k=k+1;
end
%plot(handles.video,0,0);

release(videoFReader);
cla(handles.video)
set(handles.video,'Visible','off');
set(handles.sml_text,'Visible','on');
set(handles.kth_logo,'Visible','off');

imagesc(imread('kth_rgb.jpg'),'Parent',handles.kth_logo);
set(handles.kth_logo,'xtick',[],'ytick',[])
drawnow

set(handles.message,'FontSize',50);
set(handles.message,'Visible','on');
set(handles.message,'String','Welcome to the platooning simulation')

set(handles.touch,'Visible','off');
pause(1)
set(handles.button,'Visible','on');
