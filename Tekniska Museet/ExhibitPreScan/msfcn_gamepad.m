function msfcn_gamepad(block)
% Handles input to the gui panel that displays the map and updates it.

setup(block);
%endfunction

function setup( block )


%% define number of input and output ports
block.NumInputPorts  = 1;
block.NumOutputPorts = 2;


%% port properties
block.SetPreCompInpPortInfoToDynamic;
block.SetPreCompOutPortInfoToDynamic;

block.InputPort(1).Complexity = 'real'; % x
block.InputPort(1).DataTypeId = 0; %real
block.InputPort(1).SamplingMode = 'Sample';
block.InputPort(1).Dimensions = 1;

block.OutputPort(1).Complexity = 'real'; % x
block.OutputPort(1).DataTypeId = 0; %real
block.OutputPort(1).SamplingMode = 'Sample';
block.OutputPort(1).Dimensions = 1;

block.OutputPort(2).Complexity = 'real'; % y
block.OutputPort(2).DataTypeId = 0; %real
block.OutputPort(2).SamplingMode = 'Sample';
block.OutputPort(2).Dimensions = 1;


%% Register parameters
block.NumDialogPrms     = 1;
dfreq = block.DialogPrm(1).Data;

%% block sample time
block.SampleTimes = [1/dfreq 0];
%% register methods
block.RegBlockMethod('Outputs', @Outputs);

%% Block runs on TLC in accelerator mode.
block.SetAccelRunOnTLC(true);
%endfunction


function Outputs(block)

global handles_v stop first

%movegui(gcf,[2888 132]);

%set(handles_v.text,'String',control'Simulation Started')

speed = block.InputPort(1).Data;

if ~stop
    
    [a ab] = JoyMEX(0);
    
    limit = 90;
    
    steering = a(1)*limit;
    left = ab(6);
    right = ab(5);
    
    if right
        speed=rem(speed+1,20);
    end
    if left
        speed=rem(speed-1,20);
    end
    
    block.OutputPort(1).Data=steering;
    block.OutputPort(2).Data=speed;
    
end




%endfunction Outputs
