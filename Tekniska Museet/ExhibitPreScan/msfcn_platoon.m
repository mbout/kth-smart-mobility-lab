function msfcn_platoon(block)
% Handles input to the gui panel that displays the map and updates it.

setup(block);
%endfunction

function setup( block )


%% define number of input and output ports
block.NumInputPorts  = 3;
block.NumOutputPorts = 0;


%% port properties
block.SetPreCompInpPortInfoToDynamic;
block.SetPreCompOutPortInfoToDynamic;

block.InputPort(1).Complexity = 'real'; % x
block.InputPort(1).DataTypeId = 0; %real
block.InputPort(1).SamplingMode = 'Sample';
block.InputPort(1).Dimensions = 1;

block.InputPort(2).Complexity = 'real'; % y
block.InputPort(2).DataTypeId = 0; %real
block.InputPort(2).SamplingMode = 'Sample';
block.InputPort(2).Dimensions = 1;

block.InputPort(3).Complexity = 'real'; % speed
block.InputPort(3).DataTypeId = 0; %real
block.InputPort(3).SamplingMode = 'Sample';
block.InputPort(3).Dimensions = 1;


%% Register parameters
block.NumDialogPrms     = 1;
dfreq = block.DialogPrm(1).Data;

%% block sample time
block.SampleTimes = [1/dfreq 0];
%% register methods
block.RegBlockMethod('Outputs', @Outputs);

%% Block runs on TLC in accelerator mode.
block.SetAccelRunOnTLC(true);
%endfunction

function Outputs(block)

global handles_v stop first time_p time_d

%movegui(gcf,[2888 132]);

stop=false;

if ~stop
    set(handles_v.manual_button,'Visible','off');
    set(handles_v.start,'Visible','off');
    set(handles_v.stop,'Visible','on');
    set(handles_v.platoon_v,'Visible','off');
    set(handles_v.platoon_text,'Visible','on');
    set(handles_v.speed_text,'Visible','on');
    set(handles_v.distance_text,'Visible','on');
    set(handles_v.platoon_d,'Visible','on');
    set(handles_v.time,'Visible','on');
    set(handles_v.meters,'Visible','on');
    set(handles_v.text,'Visible','on');
    set(handles_v.platoon_axis,'Visible','on');    
end

%set(handles_v.text,'String',control'Simulation Started')

x=block.InputPort(1).Data;
y=block.InputPort(2).Data;
platoon_v=block.InputPort(3).Data;

x_inter=74.69;
y_inter=157.49;

d_inter=sqrt((x-x_inter)^2+(y-y_inter)^2);

time_p = d_inter/platoon_v; 

if get(handles_v.time,'Value')
    d_inter=d_inter/platoon_v;
end

if get(handles_v.speed_text,'Value')
    velocity = platoon_v*3.6;
    set(handles_v.platoon_v,'String',velocity);
    drawGauge(velocity,handles_v.platoon_axis,'kmh')
else
    velocity = platoon_v;
    set(handles_v.platoon_v,'String',num2str(platoon_v));
    drawGauge(velocity,handles_v.platoon_axis,'mps')
end

set(handles_v.text,'String',['Prediction: ','Platoon'])

red = [0.847 0.161 0];
green = [0 0.498 0];

set(handles_v.text,'BackgroundColor',green)

if time_p > time_d
    set(handles_v.text,'String',['Prediction: ','Fail'])
    set(handles_v.text,'BackgroundColor',red)
elseif time_d > time_p + 3; 
    set(handles_v.text,'String',['Prediction: ','Fail'])
    set(handles_v.text,'BackgroundColor',red)
end

if (x-x_inter)>0
    set(handles_v.platoon_d,'String','')
    if time_d > time_p + 3
       set(handles_v.text,'String','Failed to Platoon')
       set(handles_v.text,'BackgroundColor',red)
    else
       %set(handles_v.text,'HorizontalAlignment','left');
       %set(handles_v.text,'String',['   Platooning with ',sprintf('%.0f',abs(time_p-time_d)*platoon_v),'m apart'])
       set(handles_v.text,'String','Platooning')
       set(handles_v.text,'BackgroundColor',green)
    end
    
else
    if get(handles_v.time,'Value')
        d = [sprintf('%.1f',d_inter),' s'];
    else
        d = [sprintf('%.0f',d_inter),' m'];
    end    
    set(handles_v.platoon_d,'String',d)
end

if platoon_v == 0
   stop = true; 
   clear JoyMEX
   set_param('tekniska_cs','SimulationCommand','Stop')
end


drawnow

if stop && ~first
    set(handles_v.text,'HorizontalAlignment','center');
    if (x-x_inter)>0
        if time_d > time_p + 3
            set(handles_v.text,'String','Platooning')
        end
    end
    set(handles_v.manual_button,'Visible','on');
    set(handles_v.start,'Visible','on');
    set(handles_v.stop,'Visible','off');
    set(handles_v.platoon_v,'Visible','off');
    set(handles_v.platoon_text,'Visible','off');
    set(handles_v.driver_v,'Visible','off');
    set(handles_v.driver_text,'Visible','off');
    set(handles_v.speed_text,'Visible','off');
    set(handles_v.speed_title,'Visible','off');
    set(handles_v.distance_text,'Visible','off');
    set(handles_v.speed_title,'Visible','off');
    set(handles_v.driver_d,'Visible','off');
    set(handles_v.platoon_d,'Visible','off');
    set(handles_v.speed,'Visible','off');
    set(handles_v.time,'Visible','off');
    set(handles_v.meters,'Visible','off');
    set(handles_v.plusone,'Visible','off');
    set(handles_v.plusten,'Visible','off');
    set(handles_v.minusten,'Visible','off');
    set(handles_v.minusone,'Visible','off');
    set(handles_v.driver_axis,'Visible','off');
    set(handles_v.platoon_axis,'Visible','off');
    cla(handles_v.platoon_axis)
    cla(handles_v.driver_axis)
    
    set(handles_v.driver_d,'String','1000');
    set(handles_v.platoon_d,'String','1000');
end

first=false;

%endfunction Outputs
