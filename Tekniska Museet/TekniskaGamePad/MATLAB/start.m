function varargout = start(varargin)
% START_BUTTON MATLAB code for start_button.fig
%      START_BUTTON, by itself, creates a new START_BUTTON or raises the existing
%      singleton*.
%
%      H = START_BUTTON returns the handle to a new START_BUTTON or the handle to
%      the existing singleton*.
%
%      START_BUTTON('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in START_BUTTON.M with the given input arguments.
%
%      START_BUTTON('Property','Value',...) creates a new START_BUTTON or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before start_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to start_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help start_button

% Last Modified by GUIDE v2.5 15-Nov-2014 14:46:07

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @start_OpeningFcn, ...
                   'gui_OutputFcn',  @start_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before start_button is made visible.
function start_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to start_button (see VARARGIN)

% Choose default command line output for start_button
handles.output = hObject;

global chosen gamepads stop running

running = false;
stop = false;

set(handles.start_button,'String','Start')

set(handles.start_button,'Visible','on')
set(handles.stop_button,'Visible','off')

set(handles.axes1,'xtick',[],'ytick',[])
set(handles.axes2,'xtick',[],'ytick',[])
set(handles.axes3,'xtick',[],'ytick',[])
set(handles.axes4,'xtick',[],'ytick',[])

gamepad = imread('gamepad.jpg');
bw_gamepad = ~im2bw(gamepad,.99);

% wheel = imread('wheel.jpg');
% bw_wheel = ~im2bw(wheel,.99);


h=imshow(gamepad,'Parent',handles.axes1);
set(h,'AlphaData',bw_gamepad);

h=imshow(gamepad,'Parent',handles.axes2);
set(h,'AlphaData',bw_gamepad);

h=imshow(gamepad,'Parent',handles.axes3);
set(h,'AlphaData',bw_gamepad);

% h=imshow(wheel,'Parent',handles.axes4);
% set(h,'AlphaData',bw_wheel);

set(handles.left1,'Visible','off');
set(handles.left2,'Visible','off');
set(handles.left3,'Visible','off');
set(handles.left4,'Visible','off');
set(handles.right1,'Visible','off');
set(handles.right2,'Visible','off');
set(handles.right3,'Visible','off');
set(handles.right4,'Visible','off');
set(handles.up1,'Visible','off');
set(handles.up2,'Visible','off');
set(handles.up3,'Visible','off');
set(handles.up4,'Visible','off');
set(handles.down1,'Visible','off');
set(handles.down2,'Visible','off');
set(handles.down3,'Visible','off');
set(handles.down4,'Visible','off');
set(handles.steer1,'Visible','off');
set(handles.steer2,'Visible','off');
set(handles.steer3,'Visible','off');
set(handles.steer4,'Visible','off');
set(handles.speed1,'Visible','off');
set(handles.speed2,'Visible','off');
set(handles.speed3,'Visible','off');
set(handles.speed4,'Visible','off');




gamepads = false(1,4); 
chosen = true(1,4);

clear JoyMex

detect_gamepads(handles)
    

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes start_button wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = start_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in start_button.
function start_button_Callback(hObject, eventdata, handles)
% hObject    handle to start_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global gamepads chosen playwith

playwith = gamepads & chosen

set(handles.start_button,'Visible','off')
set(handles.stop_button,'Visible','on')
guidata(hObject, handles);
drawnow
disp ('start')

MakingTrucksRun(handles)
    


% --------------------------------------------------------------------
function uipanel3_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to uipanel3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global chosen
if chosen(3)
    set (handles.uipanel3,'HighlightColor',[1 0 0]);
    chosen(3) = false;
else
    set (handles.uipanel3,'HighlightColor',[0 1 0 ]);
    chosen(3) = true;
end

% --------------------------------------------------------------------
function uipanel4_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to uipanel4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global chosen
if chosen(4)
    set (handles.uipanel4,'HighlightColor',[1 0 0]);
    chosen(4) = false;
else
    set (handles.uipanel4,'HighlightColor',[0 1 0 ]);
    chosen(4) = true;
end

% --------------------------------------------------------------------
function uipanel2_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to uipanel2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global chosen
if chosen(2)
    set (handles.uipanel2,'HighlightColor',[1 0 0]);
    chosen(2) = false;
else
    set (handles.uipanel2,'HighlightColor',[0 1 0 ]);
    chosen(2) = true;
end

% --------------------------------------------------------------------
function uipanel1_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to uipanel1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global chosen
if chosen(1)
    set (handles.uipanel1,'HighlightColor',[1 0 0]);
    chosen(1) = false;
else
    set (handles.uipanel1,'HighlightColor',[0 1 0 ]);
    chosen(1) = true;
end


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
global stop
clear JoyMex
stop = true;
delete(hObject);

function detect_gamepads(handles)

global chosen gamepads

if chosen(1)
    set (handles.uipanel1,'HighlightColor',[0 1 0 ]);
else
    set (handles.uipanel1,'HighlightColor',[1 0 0]);
end

if chosen(2)
    set (handles.uipanel2,'HighlightColor',[0 1 0 ]);
else
    set (handles.uipanel2,'HighlightColor',[1 0 0]);
end

if chosen(3)
    set (handles.uipanel3,'HighlightColor',[0 1 0 ]);
else
    set (handles.uipanel3,'HighlightColor',[1 0 0]);
end

if chosen(4)
    set (handles.uipanel4,'HighlightColor',[0 1 0 ]);
else
    set (handles.uipanel4,'HighlightColor',[1 0 0]);
end

try
    JoyMEX('init',0);
    set(handles.uipanel1,'BackgroundColor',[0 0.498 0]);
    gamepads(1) = true; 
catch err
    if ~strcmp(err.identifier,'JoyMEX:AlreadyInitialized')
        set(handles.uipanel1,'BackgroundColor',[0.8 0 0]);
    end
end

try
    JoyMEX('init',1);
    set(handles.uipanel2,'BackgroundColor',[0 0.498 0]);
    gamepads(2) = true; 
catch err
    if ~strcmp(err.identifier,'JoyMEX:AlreadyInitialized')
        set(handles.uipanel2,'BackgroundColor',[0.8 0 0]);
    end
end

try
    JoyMEX('init',2);
    set(handles.uipanel3,'BackgroundColor',[0 0.498 0]);
    gamepads(3) = true; 
catch err
    if ~strcmp(err.identifier,'JoyMEX:AlreadyInitialized')
        set(handles.uipanel3,'BackgroundColor',[0.8 0 0]);
    end
end


try
    JoyMEX('init',3);
    set(handles.uipanel4,'BackgroundColor',[0 0.498 0]);
    gamepads(4) = true; 
catch err
    if ~strcmp(err.identifier,'JoyMEX:AlreadyInitialized')
        set(handles.uipanel4,'BackgroundColor',[0.8 0 0]);
    end
end


% --- Executes on button press in refresh.
function refresh_Callback(hObject, eventdata, handles)
% hObject    handle to refresh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
detect_gamepads(handles)


% --- Executes on button press in stop_button.
function stop_button_Callback(hObject, eventdata, handles)
% hObject    handle to stop_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global stop
disp('try_stop')
set(handles.start_button,'Visible','on');
set(handles.stop_button,'Visible','off');
stop = true;
guidata(hObject, handles);
drawnow



function kspeed_Callback(hObject, eventdata, handles)
% hObject    handle to kspeed (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of kspeed as text
%        str2double(get(hObject,'String')) returns contents of kspeed as a double


% --- Executes during object creation, after setting all properties.
function kspeed_CreateFcn(hObject, eventdata, handles)
% hObject    handle to kspeed (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ksteer_Callback(hObject, eventdata, handles)
% hObject    handle to ksteer (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ksteer as text
%        str2double(get(hObject,'String')) returns contents of ksteer as a double


% --- Executes during object creation, after setting all properties.
function ksteer_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ksteer (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
