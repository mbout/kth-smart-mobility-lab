function [s] = NI_initialization2(handles)
%NI_INITIALIZATION Summary of this function goes here
%   Detailed explanation goes here

devices=daq.getDevices;
% Create the session
s=daq.createSession('ni');
% Adding output channels


s.addAnalogOutputChannel(devices(1).ID,0,'Voltage');
s.addAnalogOutputChannel(devices(1).ID,1,'Voltage');
% s.addAnalogOutputChannel(devices(1).ID,6,'Voltage');
% s.addAnalogOutputChannel(devices(1).ID,7,'Voltage');
% s.addAnalogOutputChannel(devices(1).ID,2,'Voltage');
% s.addAnalogOutputChannel(devices(1).ID,3,'Voltage');
% s.addAnalogOutputChannel(devices(1).ID,10,'Voltage');
% s.addAnalogOutputChannel(devices(1).ID,11,'Voltage');
% s.addAnalogOutputChannel(devices(1).ID,6,'Voltage');
% s.addAnalogOutputChannel(devices(1).ID,7,'Voltage');
% s.addAnalogOutputChannel(devices(1).ID,12,'Voltage');

s.addAnalogOutputChannel(devices(1).ID,str2num(get(handles.port1,'String')),'Voltage');
s.addAnalogOutputChannel(devices(1).ID,str2num(get(handles.port2,'String')),'Voltage');
s.addAnalogOutputChannel(devices(1).ID,str2num(get(handles.port3,'String')),'Voltage');
s.addAnalogOutputChannel(devices(1).ID,str2num(get(handles.port4,'String')),'Voltage');
s.addAnalogOutputChannel(devices(1).ID,str2num(get(handles.port5,'String')),'Voltage');
s.addAnalogOutputChannel(devices(1).ID,str2num(get(handles.port6,'String')),'Voltage');
s.addAnalogOutputChannel(devices(1).ID,str2num(get(handles.port7,'String')),'Voltage');
s.addAnalogOutputChannel(devices(1).ID,str2num(get(handles.port8,'String')),'Voltage');

end

