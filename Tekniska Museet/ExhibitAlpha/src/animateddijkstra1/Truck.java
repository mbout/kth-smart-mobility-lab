/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
// Rui like truck

// me too
package animateddijkstra1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 *
 * @author rui
 */
class Truck{
    
    public int id;
    public int[] path;
    public int source;
    public int destination;
    public double departureTime;
    public double[] times;
    public double fuelConsumption;
    double litresPerMeter = 30./(100.*1000.); // 30 litres per 100 km 
    public double truckSpeed = 75000./3600.; //75 km/h
    public int company;
    
    
    
    public List<Integer[]> dijkstraIterations = new ArrayList<>();
    
    public Truck(int argId, int argSource, int argDestination, double argDepartureTime, DijkstraSolver argPathSolver){
        
        id = argId;
        source = argSource;
        destination = argDestination;
        departureTime = argDepartureTime;
        
        Random randGen = new Random(); 
        company = randGen.nextInt(4);
        
        
        List<Integer> shortestPath = new ArrayList<>();
        List<Double> costs = new ArrayList<>();
       
        argPathSolver.findShortestPath(source, destination, shortestPath, costs);
        // Costs are distances in meters!
        
//        System.out.println("From: " + argSource + " to: " + argDestination);
//        
//        System.out.println("Shortest path size: " + shortestPath.size());
        
        path = new int[shortestPath.size()];
        times = new double[shortestPath.size()];
        for ( int i = 0 ; i < shortestPath.size() ; i++ ) {
            path[i] = shortestPath.get(i);
            
//            System.out.println(path[i]);
            
            if ( i == 0) {
                times[i] = departureTime;
            }else{
                //System.out.println(costs.get(i-1));
                //System.out.println(truckSpeed);
                //System.out.println(costs.get(i-1)/truckSpeed);
                fuelConsumption += costs.get(i-1)*litresPerMeter;
                times[i] = times[i-1] + (costs.get(i-1)/truckSpeed);
            }
        }
        
        
        
        //System.out.println(Arrays.toString(path));
        
    }
    
    public void getTruckDijkstraHistory(DijkstraSolver argPathSolver){
        
        List<Integer> shortestPath = new ArrayList<>();
        List<Double> costs = new ArrayList<>();
        
        argPathSolver.resetDijkstraIterations();
        argPathSolver.findShortestPath(source, destination, shortestPath, costs);
        
        
    }
    
    public int[][] getTruckDijkstra(DijkstraSolver argPathSolver){
        
        List<Integer> shortestPath = new ArrayList<>();
        List<Double> costs = new ArrayList<>();
       
        argPathSolver.resetDijkstraIterations();
        argPathSolver.findShortestPath(source, destination, shortestPath, costs);
        
        
        int[][] shortestPaths;
        
        
        
        shortestPaths = argPathSolver.getAllShortestPath();
        
//        System.out.println("Will show Dijkstra steps");
        
        for ( int i = 0 ; i < shortestPaths.length ; i++ ){
            
            for ( int j = 0 ; j < shortestPaths[i].length ; j++ ){
                
//                System.out.println( shortestPaths[i][j] );
                
            }
            
//            System.out.println( "" );
            
//            System.out.println( shortestPaths[i].toString() );
            
        }
        
        dijkstraIterations = argPathSolver.dijkstraIterations;        
        
        return shortestPaths;
                
        // Costs are distances in meters!
              
        
    }
    
    public void printPath(){
        System.out.println(Arrays.toString(path));
    }
    
//    public void displayClockTimes(){
//        
//        int[] hourTimes = getHourTimes();
//        int[] minuteTimes = getMinuteTimes();
//        
//        for ( int i = 0 ; i < hourTimes.length ; i++ ){
//            if ( hourTimes[i] < 10 && minuteTimes[i] < 10){
//                System.out.println("Time: 0"+hourTimes[i]+":0"+minuteTimes[i]);
//            }
//            if ( minuteTimes[i] < 10  && hourTimes[i] >= 10){
//                System.out.println("Time: "+hourTimes[i]+":0"+minuteTimes[i]);
//            }
//            if ( minuteTimes[i] >= 10  && hourTimes[i] < 10){
//                System.out.println("Time: 0"+hourTimes[i]+":"+minuteTimes[i]);
//            }
//            if ( hourTimes[i] >= 10 && minuteTimes[i] >= 10){
//                System.out.println("Time: "+hourTimes[i]+":"+minuteTimes[i]);
//            }
//        }
//        
//    }
        
    public int[] getPath(){
        return path;
    }
    public double[] getTimes(){
        return times;
    }
    public double getDepartureTime(){
        return times[0];
    }
    public double getFuelConsumption(){
        return fuelConsumption;
    }
    
    
//    public int[] getHourTimes(){
//        int[] hourTimes = new int[times.length];
//        for ( int i = 0 ; i < times.length ; i++ ){
//            //System.out.println("getHourTimes");
//            //System.out.println("times[i]");
//            //System.out.println(times[i]);
//            //System.out.println((int)times[i]/(60*60));
//            hourTimes[i] = (int)times[i]/(60*60);
//        }               
//        return hourTimes;
//    }
//    
//    public int[] getMinuteTimes(){
//        int[] minuteTimes = new int[times.length];
//        for ( int i = 0 ; i < times.length ; i++ ){
//            //System.out.println("getMinuteTimes");
//            //System.out.println("times[i]");
//            //System.out.println(times[i]);
//            //System.out.println((int)times[i]%(3600)/60);
//            minuteTimes[i] = (int)times[i]%(3600)/60;
//        }               
//        return minuteTimes;
//    }
    
    public int getId(){
        return id;
    }
    
}