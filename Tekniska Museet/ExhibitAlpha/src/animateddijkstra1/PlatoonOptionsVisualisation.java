/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package animateddijkstra1;

import static animateddijkstra1.AnimatedDijkstra1.getHourTime;
import static animateddijkstra1.AnimatedDijkstra1.getMinuteTime;
import static animateddijkstra1.AnimatedDijkstra1.getSecondTime;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.animation.KeyFrame;
import javafx.animation.PathTransition;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.CubicCurveTo;
import javafx.scene.shape.Line;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeType;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.util.Duration;
import javafx.util.converter.IntegerStringConverter;

/**
 *
 * @author rui
 */
public class PlatoonOptionsVisualisation {
    
    Scene mainScene;
    Group mainRoot;
    
    SwedenPlatoon swedenPlatooning;
    
    Group platoonOptionsRoot;
    Group truckPlatooningLines;
    Group truckPlatooningLinesSearch;
    Group platooningOptions;
    
//        Group platoonOptionsRoot = new Group();
//    Group truckPlatooningLines = new Group();
//    Group truckPlatooningLinesSearch = new Group();
//    Group platooningOptions = new Group();
    
    Group textPress;
    
//    Group legendLines = new Group();
    
    HBox buttonsHB;
    ButtonsBar BB;
    
    Text tSkip;
    
    boolean bigMode = Globals.bigMode;
    double fontStretch = Globals.fontStretch;
    double horizontalStretch = Globals.horizontalStretch;
    double verticalStretch = Globals.verticalStretch;
    double inactivityTimeSeconds = Globals.inactivityTimeSeconds;
    
    int numFastestPlatoons = 3;
    int textPositionX = 1000;
    int textPositionY = 150;
    
    int currentNodeHovered;
    
    int numPlatoonTruck;
    int maxNumPlatoonTruck = 50;
    
    double hoveredOpacity = 1.;
    double imageHoveredOpacity = 1.;
    double notHoveredOpacity = .5;
    double imageNotHoveredOpacity = .3;
    
    FadeTransition ftChoiceChanger;
    
    
    
    boolean searchDone = false;
    Text platoonSearchText;
    
    boolean optionSelected = false;
    
    double startingNodeRadius = 10.0;
    double startingNodeOpacity = 1.0;
    double endingNodeRadius = 10.0;
    double endingNodeOpacity = 1.0;

    PlatoonPlan currentPlatoonPlan = new PlatoonPlan();
    
    private ArrayList<SwitchListener> switchListenerList = new ArrayList<SwitchListener>();
    
    EventHandler handler;
    final Languages language;
    
    double screenWidth = Globals.screenWidth;
    double screenHeight = Globals.screenHeight;
    
    Timeline inactivityTimeline;
    
    Group helpMenuGroup;
    HelpMenu helpMenu;
    
    PlatoonOptionsVisualisation(Scene argMainScene, Group argMainRoot, SwedenPlatoon argSwedenPlatooning, Languages inheritLang){
        
        mainScene = argMainScene;
        mainRoot = argMainRoot;
        swedenPlatooning = argSwedenPlatooning;
        language = inheritLang;
        
        if ( Globals.isSwedishCurrentLanguage ){
    
            language.setSwedish();
    
        }else{
            
            language.setEnglish();
            
        }
        
    }
    
    private Group createGraphLinks(){
        
        
        
//        System.out.println("PlatoonOptionsVisualisation -> createGraphLinks");
        
        Group graphLinks = new Group();
        
        for (int i = 0 ; i < swedenPlatooning.cityLinks.length ; i++) {
        
            for (int j = 0 ; j < swedenPlatooning.cityLinks[i].length ; j++) {
        
                Line line = new Line();
                line.setStartX(swedenPlatooning.cityPositions[i][0]);
                line.setStartY(swedenPlatooning.cityPositions[i][1]);
                line.setEndX(swedenPlatooning.cityPositions[ swedenPlatooning.cityLinks[i][j] ][0]);
                line.setEndY(swedenPlatooning.cityPositions[ swedenPlatooning.cityLinks[i][j] ][1]);
                
                //line.setFill(Color.RED);
                line.setStroke(Color.RED);
                
                graphLinks.getChildren().add(line);
            
            }
            
        }
        
        return graphLinks;
        
    }
    
    
    private void showPlatoonSearch(){
        
        numPlatoonTruck ++;
        showPlatoonSearchIteration(numPlatoonTruck);
        
    }
    
    
    private Group showStartingAndEndingNode(){
        
        Group startEndNodes = new Group();
        
        Truck myTruck = swedenPlatooning.getMyTruck();   
        
        Circle startingCircle = new Circle(swedenPlatooning.cityPositions[ myTruck.path[0] ][0],
                swedenPlatooning.cityPositions[ myTruck.path[0] ][1], startingNodeRadius, Color.web("green", startingNodeOpacity));

       
        startEndNodes.getChildren().add(startingCircle);
        
        Circle endingCircle = new Circle(swedenPlatooning.cityPositions[ myTruck.path[ myTruck.path.length-1 ] ][0],
                swedenPlatooning.cityPositions[ myTruck.path[ myTruck.path.length-1 ] ][1], endingNodeRadius, Color.web("green", endingNodeOpacity));

       
        startEndNodes.getChildren().add(endingCircle);
        
        return startEndNodes;
        
    }
    
    
    private void initializeInactivityAnimation(){
        
        //create a timeline for moving the circle
        inactivityTimeline = new Timeline();
        inactivityTimeline.setCycleCount(1);
        inactivityTimeline.setAutoReverse(false);
        
        //create a keyFrame, the keyValue is reached at time 2s
        Duration duration = Duration.millis(inactivityTimeSeconds*1000);
        //one can add a specific action when the keyframe is reached
        EventHandler onFinished = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                
                System.out.println("-----------Inactivity from PlatoonOptionsVisualisation");
                inactivityTimeline.setOnFinished(null);
                inactivityTimeline.stop();
                processSwitchEvent(new SwitchEvent(this, 3, 1));
                // go to Original Menu
                
            }
        };
 
        KeyFrame keyFrame = new KeyFrame(duration, onFinished);
 
        //add the keyframe to the timeline
        inactivityTimeline.getKeyFrames().add(keyFrame);
 
        inactivityTimeline.play();
        
        System.out.println("PlatoonOptionsVisualisation: Start Inactivity");
        
    }
    
    private void restartInactivityAnimation(){
        
        inactivityTimeline.jumpTo(Duration.ZERO);
        inactivityTimeline.play();
        
        System.out.println("PlatoonOptionsVisualisation: Restart Inactivity");
                
    }
        
    public void stopInactivityAnimation(){
        
        inactivityTimeline.setOnFinished(null);
        inactivityTimeline.stop();
         
        System.out.println("PlatoonOptionsVisualisation: Stop Inactivity");
        
    }
    
    
    private void setUpHelpStuff(int argStageOfProgram){
        
        helpMenuGroup = new Group();
        helpMenu = new HelpMenu(inactivityTimeline, argStageOfProgram, language.isSwedish());
        
        // Help menustuff
        helpMenuGroup = helpMenu.createHelpMenu();
        
        helpMenuGroup.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                
                showPlatoonSearch();
                
            }

        });
        
        mainRoot.getChildren().add(helpMenuGroup);
        
        helpMenuGroup.setLayoutX( screenWidth/2. - helpMenuGroup.getBoundsInParent().getWidth()/2. );
        helpMenuGroup.setLayoutY( screenHeight/2. - helpMenuGroup.getBoundsInParent().getHeight()/2. );
        
        ImageView helpButtonImageView = helpMenu.createHelpButton();
        
        helpButtonImageView.setId("helpButton");
        
        ImageView toRemoveImageView = (ImageView) mainRoot.lookup("#helpButton");
        
        System.out.println("-------------------setUpHelpStuff");
        
        if ( toRemoveImageView != null ){
            System.out.println("-------------------found IMAGE");
            toRemoveImageView.setLayoutX(-200);
            mainRoot.getChildren().remove(toRemoveImageView);
            
        }        
        
        mainRoot.getChildren().add(helpButtonImageView);
        
        helpButtonImageView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {

                System.out.println("Restarted counter because I clicked on the helpLogo");
                
                
            }

        });
        
        
    }
    
    
    
    public void showPlatoonOptions(){
        
        // THIS IS THE MAIN OF THIS CLASS
        System.out.println("PlatoonOptionsVIsualisation->main");
        
        if ( searchDone == false ){
            
            initializeInactivityAnimation();
            
            // Adding button stuff
            String[] types = {"home","skip"};
            BB = new ButtonsBar(mainScene, types);
            buttonsHB = BB.getHB();
            buttonsEvents(buttonsHB);

            mainRoot.getChildren().add(buttonsHB);
            
            // Initializing groups
            platoonOptionsRoot = new Group();
            truckPlatooningLines = new Group();
            truckPlatooningLinesSearch = new Group();
            platooningOptions = new Group();
            
            // Reseting search iteration
            numPlatoonTruck = 0;
            
            // Reseting auto change animation
            ftChoiceChanger = new FadeTransition(Duration.millis(1000), truckPlatooningLines);
            
            if (bigMode){
                tSkip = new Text(horizontalStretch*880, verticalStretch*225, "");
                tSkip.setFont(new Font(30*fontStretch));
            }else{
                tSkip = new Text(880, 225, "");
                tSkip.setFont(new Font(30));
            }
            
            if (language.isEnglish())
                tSkip.setText("Press here to skip");
            else
                tSkip.setText("Tryck här för att komma vidare");
                
            tSkip.setFill(Color.WHITE);

            textPress = new Group(tSkip);
            textPress.setOnMousePressed((MouseEvent me)->{
                
                    System.out.println("Skipped by Text");
                
                    searchDone = true;
                    mainRoot.getChildren().remove(textPress);
                    BB.removeButton("skip");
                    showPlatoonOptions();
                    
            });

            mainRoot.getChildren().add(textPress);


            platoonSearchText = new Text();
            if (bigMode){
                platoonSearchText = new Text(horizontalStretch*880, verticalStretch*100, "");
                platoonSearchText.setFont(new Font(30*fontStretch));
            }else{
                platoonSearchText = new Text(880, 100, "");
                platoonSearchText.setFont(new Font(30));
            }  
            
            if (language.isEnglish())
                platoonSearchText.setText("Searching platooning\nopportunities...");
            else
                platoonSearchText.setText("Söker efter andra fordon\natt ansluta till...");

            
            platoonSearchText.setFill(Color.WHITE);
            mainRoot.getChildren().add(platoonSearchText);
            
            
            platoonOptionsRoot = new Group();

            Group lines;
            lines = createGraphLinks();
            mainRoot.getChildren().add(lines);

            Group circles;
            circles = createGraphPositions();
            mainRoot.getChildren().add(circles);

            
            mainRoot.getChildren().add( showStartingAndEndingNode() );
            
            

            Line line;        
            Truck myTruck = swedenPlatooning.getMyTruck();   

            for ( int i = 1 ; i < myTruck.path.length ; i++ ){

                line = new Line();
                line.setStartX(swedenPlatooning.cityPositions[myTruck.path[i]][0]);
                line.setStartY(swedenPlatooning.cityPositions[myTruck.path[i]][1]);
                line.setEndX(swedenPlatooning.cityPositions[myTruck.path[i-1]][0]);
                line.setEndY(swedenPlatooning.cityPositions[myTruck.path[i-1]][1]);

                line.setStroke(Color.GREEN);
                line.setStrokeWidth(8.0);

                mainRoot.getChildren().add(line);
    //            linesDisplayed.add(line);

            } 

            mainRoot.getChildren().add(truckPlatooningLinesSearch);
            
//            showPlatoonSearch();

//            mainRoot.getChildren().add(truckPlatooningLines);
            int currentStageOfProgram = 2;
            setUpHelpStuff(currentStageOfProgram);
            
        
        }else{
        
            restartInactivityAnimation();
            
            currentNodeHovered = 0;
            
            BB.addButton("replay");
            buttonsEvents(buttonsHB);
            
            mainRoot.getChildren().remove(platoonSearchText);
            
            mainRoot.getChildren().add(truckPlatooningLines);
//            mainRoot.getChildren().add(truckPlatooningLines);
            
            Group legendLines;
            legendLines = showLegend();
            mainRoot.getChildren().add(legendLines);

            showTruckSharedPaths();

            mainRoot.getChildren().add(platoonOptionsRoot);
          
            int currentStageOfProgram = 3;
            setUpHelpStuff(currentStageOfProgram);
            
        }
        
        
        

    }
    
    
    
    
    private void showTruckSharedPaths(){
        
        
        showBestTruckSharedPaths(0);
                                
    }
     
     
    private void showBestTruckSharedPaths(int numPlatoon){
        
//        System.out.println("PlatoonOptionsVisualisation -> showBestTruckSharedPaths");
           
        List<PlatoonPlan> fastestPlatoons = new ArrayList<>();
        
        swedenPlatooning.getFastestPossiblePlatoonPlans(fastestPlatoons, numFastestPlatoons);
        
                    
        SharedPath sharedPath = new SharedPath();
        PlatoonPlan platoonPlan = new PlatoonPlan();
        
        int tempNumPlatoon = 0;
        
        for ( int i = 0 ; i < fastestPlatoons.size() ; i++ ){
            
            platoonPlan = fastestPlatoons.get(i);
            
            if ( platoonPlan.possible ){
                
                if ( tempNumPlatoon == numPlatoon ){
                
                    break;            
                
                }else{
                    
                    tempNumPlatoon++;
                    
                }
            }
            
        }
        
        DecimalFormat df = new DecimalFormat("#00");
        
        
        
//        Text t = new Text(textPositionX, textPositionY-50, "Finding the shortest path\nbetween selected areas");
        Text tMenu = new Text();
        if (bigMode){
            tMenu = new Text(horizontalStretch*(textPositionX-125), verticalStretch*(textPositionY-50), "");
            tMenu.setFont(new Font(30*fontStretch));
        }else{
            tMenu = new Text(textPositionX-125, textPositionY-50, "");
            tMenu.setFont(new Font(30));
        }  
        
        if (language.isEnglish()){
            tMenu.setText("Choose one of the platoon\nopportunities below");
        }else{
            tMenu.setText("Välj ett av alternativen");
        }
        
        tMenu.setFill(Color.WHITE);
        mainRoot.getChildren().add(tMenu);
        
        
        for ( int i = 0 ; i < numFastestPlatoons ; i++ ){
            
            int listOffsetX = 50;
            
            Group tempGroup = new Group();         
            
            platoonPlan = fastestPlatoons.get(i);
            
            double departureDelay = platoonPlan.getDepartureDelay( swedenPlatooning.getMyTruck() );

               
            df = new DecimalFormat("#00");
            
            String secondsString;
            
            if (language.isEnglish()){
                secondsString = " sec";
            }else{
                secondsString = " sek";
            }
            
            String departureDelayString = df.format(getMinuteTime(departureDelay))+" min "+df.format(getSecondTime(departureDelay))+secondsString;
        

            df = new DecimalFormat("0.00");
            String fuelFormated = df.format(platoonPlan.savedFuel);

            Text t;
            if (bigMode){
                t = new Text( horizontalStretch*(textPositionX-listOffsetX), verticalStretch*(textPositionY + 100 + 125*i), "");
                t.setFont(new Font(20*fontStretch));
            }else{
                t = new Text(textPositionX-listOffsetX, textPositionY + 100 + 125*i, "");
                t.setFont(new Font(20));
            }
            
            if (language.isEnglish())
                t.setText("Platooning opportunity #"+(i+1) + "\nSaved fuel: "+ fuelFormated + " litres"+
                    "\nDeparture delay: "+departureDelayString);
            else
                t.setText("Platooning möjlighet #"+(i+1) + "\nSparat bränsle: "+ fuelFormated + " liter"+
                    "\nFörsenad avgång: "+departureDelayString);
            
            
            t.setFill(Color.WHITE);
            t.setId("text");
            t.setOpacity(notHoveredOpacity);
            
            Truck tempTruck = platoonPlan.getFollowedTruck();
            
            Image companyImg = Globals.truckIkeaImage;
        
            if ( tempTruck.company == 0){
                companyImg = Globals.truckIkeaImage;
            }
            if ( tempTruck.company == 1){
                companyImg = Globals.truckIcaImage;
            }
            if ( tempTruck.company == 2){
                companyImg = Globals.truckHmImage;
            }
            if ( tempTruck.company == 3){
                companyImg = Globals.truckScaniaImage;
            }
            
            int imgOffsetX = 100;
            
            ImageView companyImgView = new ImageView(companyImg);
        
            int companyHeight;
            if (bigMode){
                companyHeight = (int) (50*verticalStretch);
            }else{
                companyHeight = 50;
            }
            companyImgView.setFitHeight(companyHeight);
            companyImgView.setPreserveRatio(true);

            
            if (bigMode){
                companyImgView.setX( horizontalStretch*(textPositionX-listOffsetX-imgOffsetX) );
                companyImgView.setY( verticalStretch*(textPositionY + 100 + 125*i) );
            }else{
                companyImgView.setX(textPositionX-listOffsetX-imgOffsetX);
                companyImgView.setY(textPositionY + 100 + 125*i);
            }
            
            companyImgView.setId("image");
            
            companyImgView.setOpacity(imageNotHoveredOpacity);
            
            tempGroup.getChildren().add(companyImgView);
            
            
            
            if ( i == 0){
                // By default will show the first platoon
                showFastestPlatoon(fastestPlatoons, i);     
                t.setOpacity(hoveredOpacity);
                companyImgView.setOpacity(imageHoveredOpacity);
                
            }
            
            
            tempGroup.getChildren().add(t);
            
            tempGroup.setId(""+i);
            
            IntegerStringConverter integerStringConverter = new IntegerStringConverter(); 
            
            
            tempGroup.setOnMouseClicked(new EventHandler<MouseEvent>() {
                public void handle(MouseEvent me) {
//                    System.out.println("Mouse clicked platoon box #"+tempGroup.getId()); 
                    int nodeHovered = integerStringConverter.fromString( tempGroup.getId() );
                    setPlatoonOpportunity(fastestPlatoons, nodeHovered);
                    showFastestPlatoon(fastestPlatoons, nodeHovered);
//                    Node temp = tempGroup.lookup("#text");
//                    temp.setOpacity(hoveredOpacity);
//                    tempGroup.setOpacity(hoveredOpacity);
                    
                    if ( optionSelected == true ){
                        
                        return;
                        
                    }
                    
                    optionSelected = true;
                    ftChoiceChanger.stop();
                    
                    for ( int i = 0 ; i < platooningOptions.getChildren().size() ; i++ ){
                            
                            
                            
                            Node temp = platooningOptions.lookup("#"+i);
                            Node textTemp = temp.lookup("#text");
                            textTemp.setOpacity(notHoveredOpacity);          
                            Node imageTemp = temp.lookup("#image");
                            imageTemp.setOpacity(imageNotHoveredOpacity);         

              
                    }
                    
                    
                    
                    Node tempText = tempGroup.lookup("#text");
                    tempText.setOpacity(hoveredOpacity);
                    Node tempImage = tempGroup.lookup("#image");
                    tempImage.setOpacity(imageHoveredOpacity);
                    
                    
                    PlatoonPlan platoonPlan = fastestPlatoons.get(nodeHovered);
        
                    currentPlatoonPlan = platoonPlan;
                    
                    ftChoiceChanger.stop();
                    stopInactivityAnimation();
                    cleanScreen();
                                        
                    
                    
                }
            });
            
            
            
                        
            platooningOptions.getChildren().add(tempGroup);
            
        }
        
        mainRoot.getChildren().add(platooningOptions);
        
        
    }
    
    public void groupVacuumCleaner(Group dirtyGroup){
        
        int numElements = dirtyGroup.getChildren().size();

        for ( int i = 0 ; i < numElements ; i++ ){

            dirtyGroup.getChildren().remove(0);

        }
        
    }
    
    public void hardCleanScreen(){
        
        System.out.println("PlatoonOptionsVisualisation->hardCleanScreen!");
        
        ftChoiceChanger.stop();
        
        groupVacuumCleaner(platoonOptionsRoot);
        groupVacuumCleaner(truckPlatooningLines);
        groupVacuumCleaner(truckPlatooningLinesSearch);
        groupVacuumCleaner(platooningOptions);
        groupVacuumCleaner(mainRoot);
        
        
    }
        
    public void cleanScreen(){
        
        System.out.println("PlatoonOptionsVisualisation->cleanScreen!");
        
//        inactivityTimeline.setOnFinished(null);
        inactivityTimeline.stop();
        
        FadeTransition ft = new FadeTransition(Duration.millis(1000), mainRoot);
        ft.setFromValue(1);
        ft.setToValue(0);
        ft.setCycleCount(1);
        
        EventHandler onFinishedFading = new EventHandler<ActionEvent>() {
                public void handle(ActionEvent t) {

                    System.out.println("Calling hardCleanScreen() from cleanScreen()");
                    
                    hardCleanScreen();
                    
                    ftChoiceChanger.stop();
                    
                    
                    inactivityTimeline.setOnFinished(null);
                    inactivityTimeline.stop();
                    processSwitchEvent(new SwitchEvent(this, 3));
                    
                }
                

            };
        
        // After smooth fade, I will go to the next stage of the program
        ft.setOnFinished(onFinishedFading);
        ft.play();
        
        
    }
    
    
    private void setPlatoonOpportunity(List<PlatoonPlan> fastestPlatoons, int numPlatoon){
        
        swedenPlatooning.setDepartureTimeDelayMyTruck( currentPlatoonPlan.getDepartureDelay( swedenPlatooning.getMyTruck() ) );
        swedenPlatooning.setPlatoonTruckId(currentPlatoonPlan.truck.id);
        
        
        System.out.println("SETTINGPLATOONOPPORTUNITY");
        System.out.println("currentPlatoonPlan.truck.id = " + currentPlatoonPlan.truck.id);
        
//        cleanScreen();
        
    }
    
    
    public Group createGraphPositions(){
        
//        System.out.println("PlatoonOptionsVisualisation -> createGraphPositions");
        
        Group graphNodes = new Group();
        
        double nodeOpacity = 0.5;
        double nodeRadius = 10;
        IntegerStringConverter integerStringConverter = new IntegerStringConverter(); 
        
        for (int i = 0 ; i < swedenPlatooning.numberCities ; i++) {           
            
            Circle circle = new Circle(swedenPlatooning.cityPositions[i][0],
                swedenPlatooning.cityPositions[i][1], nodeRadius, Color.web("red", nodeOpacity));
            circle.setStrokeType(StrokeType.OUTSIDE);
            circle.setStroke(Color.web("red", 0.16));
            circle.setStrokeWidth(4);
            circle.setId(integerStringConverter.toString(i));
           
            graphNodes.getChildren().add(circle);
           
        }
        
        return graphNodes;
        
    }
    
    
    private void showFastestPlatoon(List<PlatoonPlan> fastestPlatoons, int numPlatoon){
        
        numPlatoon = numPlatoon ;
        if (numPlatoon == -1){
            numPlatoon = 2;
        }
        if (numPlatoon == 3){
            numPlatoon = 0;
        }
        
//        System.out.println("PlatoonOptionsVisualisation -> showFastestPlatoon");
        
        for ( int i = 0 ; i < platooningOptions.getChildren().size() ; i++ ){
            
            if ( i != numPlatoon){
                
                Node temp = platooningOptions.lookup("#"+i);
                Node textTemp = temp.lookup("#text");
                textTemp.setOpacity(notHoveredOpacity);          
                Node imageTemp = temp.lookup("#image");
                imageTemp.setOpacity(imageNotHoveredOpacity);         
                
            }
            
        }
        
        
        int numVisibleElements = truckPlatooningLines.getChildren().size();
        
        for ( int i = 0 ; i < numVisibleElements ; i++ ){
            
            truckPlatooningLines.getChildren().remove(0);
            
        }
        
        Line line;
                
        Truck myTruck = swedenPlatooning.getMyTruck();   
        
        for ( int i = 1 ; i < myTruck.path.length ; i++ ){
            
            line = new Line();
            line.setStartX(swedenPlatooning.cityPositions[myTruck.path[i]][0]);
            line.setStartY(swedenPlatooning.cityPositions[myTruck.path[i]][1]);
            line.setEndX(swedenPlatooning.cityPositions[myTruck.path[i-1]][0]);
            line.setEndY(swedenPlatooning.cityPositions[myTruck.path[i-1]][1]);
            
            line.setStroke(Color.GREEN);
            line.setStrokeWidth(8.0);
            
            truckPlatooningLines.getChildren().add(line);
//            linesDisplayed.add(line);
            
        } 
        
        
        truckPlatooningLines.getChildren().add(showStartingAndEndingNode() );
        
        
        PlatoonPlan platoonPlan = fastestPlatoons.get(numPlatoon);
        
        currentPlatoonPlan = platoonPlan;
        
        System.out.println("SETTING currentPlatoonPlan");
        System.out.println("currentPlatoonPlan.truck.id = " + currentPlatoonPlan.truck.id);
        
        Truck otherTruck = platoonPlan.getFollowedTruck();  
        
        double truckPathOffsetX = 0;
        double truckPathOffsetY = - truckPathOffsetX * 0;
        for ( int i = 1 ; i < otherTruck.path.length ; i++ ){
            
            line = new Line();
            line.setStartX(swedenPlatooning.cityPositions[otherTruck.path[i]][0] + truckPathOffsetX);
            line.setStartY(swedenPlatooning.cityPositions[otherTruck.path[i]][1] + truckPathOffsetY);
            line.setEndX(swedenPlatooning.cityPositions[otherTruck.path[i-1]][0] + truckPathOffsetX);
            line.setEndY(swedenPlatooning.cityPositions[otherTruck.path[i-1]][1] + truckPathOffsetY);
            
            line.setOpacity(1.0);
            line.setStroke(Color.BLUE);
            line.setStrokeWidth(4.0);
            
            truckPlatooningLines.getChildren().add(line);
//            linesDisplayed.add(line);
            
        }
        
        
        
        double sharedPathOffsetX = 0;
        double sharedPathOffsetY = -sharedPathOffsetX * 0;
        for ( int i = 1 ; i < platoonPlan.sharedPath.length ; i++ ){
            
            line = new Line();
            line.setStartX(swedenPlatooning.cityPositions[platoonPlan.sharedPath[i]][0] + sharedPathOffsetX);
            line.setStartY(swedenPlatooning.cityPositions[platoonPlan.sharedPath[i]][1] + sharedPathOffsetY);
            line.setEndX(swedenPlatooning.cityPositions[platoonPlan.sharedPath[i-1]][0] + sharedPathOffsetX);
            line.setEndY(swedenPlatooning.cityPositions[platoonPlan.sharedPath[i-1]][1] + sharedPathOffsetY);
            
//            line.setOpacity(0.5);
            line.setStroke(Color.YELLOW);
//            line.setStrokeDashOffset(10.0);
            line.getStrokeDashArray().addAll(2.5d, 15d);
            line.setStrokeWidth(4.0);
            
            truckPlatooningLines.getChildren().add(line);
//            linesDisplayed.add(line);
            
        }
        
//        System.out.println("Added several lines to truckPlatooningLines");
        
        
        
        
//        ftChoiceChanger = new FadeTransition(Duration.millis(1000), truckPlatooningLines);
//        ft.setDuration(Duration.millis(5000));
        ftChoiceChanger.setFromValue(1.0);
        ftChoiceChanger.setFromValue(1.0);
        
        EventHandler onFinishedTime = new EventHandler<ActionEvent>() {
                public void handle(ActionEvent t) {

                    ftChoiceChanger.setOnFinished(null);
                    
                    if ( optionSelected == true ){
                        return;
                    }
                    
                    System.out.println("ftChoiceChanger.onFinishedTime just ran");
                    
                    currentNodeHovered ++;
                    currentNodeHovered = currentNodeHovered % 3;
//                    System.out.println("currentNodeHovered: "+currentNodeHovered);
                    showFastestPlatoon(fastestPlatoons, currentNodeHovered);
                    
                    
                    for ( int i = 0 ; i < platooningOptions.getChildren().size() ; i++ ){

                        if ( i != currentNodeHovered){

                            Node temp = platooningOptions.lookup("#"+i);
                            Node textTemp = temp.lookup("#text");
                            textTemp.setOpacity(notHoveredOpacity);          
                            Node imageTemp = temp.lookup("#image");
                            imageTemp.setOpacity(imageNotHoveredOpacity);         

                        }else{
                            
                            Node temp = platooningOptions.lookup("#"+i);
                            Node textTemp = temp.lookup("#text");
                            textTemp.setOpacity(hoveredOpacity);          
                            Node imageTemp = temp.lookup("#image");
                            imageTemp.setOpacity(imageHoveredOpacity);     
                            
                            
                        }

                    }
                    
                }
                

            };
        
        
        // After showing current selection for some time, I will change to the 
        // following selection
        ftChoiceChanger.setOnFinished(onFinishedTime);
        ftChoiceChanger.play();        
        
    }
    
    
    
    private void showPlatoonSearchIteration(int numPlatoonTruck){
        
//        System.out.println("PlatoonOptionsVisualisation -> showFastestPlatoon");
        
        Truck platoonTruck = swedenPlatooning.truckList[numPlatoonTruck];
        
        Truck myTruck = swedenPlatooning.getMyTruck();
        
        int numVisibleElements = truckPlatooningLinesSearch.getChildren().size();
        
        for ( int i = 0 ; i < numVisibleElements ; i++ ){
            
            truckPlatooningLinesSearch.getChildren().remove(0);
            
        }
        
        
        if ( numPlatoonTruck > maxNumPlatoonTruck){
            
            searchDone = true;
            mainRoot.getChildren().remove(textPress);
            BB.removeButton("skip");
            showPlatoonOptions();
            return;
            
        }
        
        SharedPath sharedPath = swedenPlatooning.getSharedPath(numPlatoonTruck);
        
        int sharedPoints = 0;
        
        for ( int i = 0 ; i < platoonTruck.path.length ; i++ ){
            
            for ( int j = 0 ; j < myTruck.path.length ; j++ ){
                
                if ( platoonTruck.path[i] == myTruck.path[j] ){
                    
                    sharedPoints ++;
                    
                }
                
            }
            
        }
        
        if ( sharedPoints > 1 && sharedPath.sharedPath.length == 0 ){
            
            numPlatoonTruck--;
            showPlatoonSearch();
            return;
            
        }
        
        
        
        Line line;
        
        for ( int i = 1 ; i < platoonTruck.path.length ; i++ ){
            
            line = new Line();
            line.setStartX(swedenPlatooning.cityPositions[platoonTruck.path[i]][0]);
            line.setStartY(swedenPlatooning.cityPositions[platoonTruck.path[i]][1]);
            line.setEndX(swedenPlatooning.cityPositions[platoonTruck.path[i-1]][0]);
            line.setEndY(swedenPlatooning.cityPositions[platoonTruck.path[i-1]][1]);
            
            line.setStroke(Color.BLUE);
            line.setStrokeWidth(4.0);
            
            truckPlatooningLinesSearch.getChildren().add(line);
//            linesDisplayed.add(line);
            
        } 
        
                
        double sharedPathOffsetX = 0;
        double sharedPathOffsetY = -sharedPathOffsetX * 0;
        for ( int i = 1 ; i < sharedPath.sharedPath.length ; i++ ){
            
            line = new Line();
            line.setStartX(swedenPlatooning.cityPositions[sharedPath.sharedPath[i]][0] + sharedPathOffsetX);
            line.setStartY(swedenPlatooning.cityPositions[sharedPath.sharedPath[i]][1] + sharedPathOffsetY);
            line.setEndX(swedenPlatooning.cityPositions[sharedPath.sharedPath[i-1]][0] + sharedPathOffsetX);
            line.setEndY(swedenPlatooning.cityPositions[sharedPath.sharedPath[i-1]][1] + sharedPathOffsetY);
            
//            line.setOpacity(0.5);
            line.setStroke(Color.YELLOW);
//            line.setStrokeDashOffset(10.0);
            line.getStrokeDashArray().addAll(2.5d, 15d);
            line.setStrokeWidth(4.0);
            
            truckPlatooningLinesSearch.getChildren().add(line);
//            linesDisplayed.add(line);
            
            
        }
        
        
        
        FadeTransition ft;
        if ( sharedPath.sharedPath.length == 0){
            ft = new FadeTransition(Duration.millis(200), truckPlatooningLinesSearch);
        }else{
            ft = new FadeTransition(Duration.millis(350), truckPlatooningLinesSearch);    
        }
        ft.setFromValue(1.0);
        ft.setToValue(0.0);
        ft.setCycleCount(1);
        ft.setAutoReverse(true);

        ft.play();
        
        EventHandler onFinished = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                
                if ( !searchDone ){
                    
                    showPlatoonSearch();

                }
                
            }
        };
        
        
        ft.setOnFinished(onFinished);
        
    }
    
    
    public synchronized void addSwitchListener(SwitchListener sl)
    {
        if (!switchListenerList.contains(sl))
        {
            switchListenerList.add(sl);
        }
    }
    
    
    private void processSwitchEvent(SwitchEvent switchEvent) {
        ArrayList<SwitchListener> tempSwitchListenerList;

        synchronized (this) {
                if (switchListenerList.size() == 0)
                        return;
                tempSwitchListenerList = (ArrayList<SwitchListener>) switchListenerList.clone();
        }

        for (SwitchListener listener : tempSwitchListenerList) {
                listener.switchRequested(switchEvent);
        }
    }
    
    
    
    private Group showLegend(){
        
        Group tempLegendLines = new Group();
        
        int startX = 850;
        int endX = 1015;
        int startY = 800;
        int offsetY = 50;
        int textOffsetX = 35;
        int textOffsetY = 7;
        int fontSize = 20;
        
        if (bigMode){
            
            startX = (int) (startX * horizontalStretch);
            endX = (int) (endX * horizontalStretch);
            startY = (int) (startY * verticalStretch);
            offsetY = (int) (offsetY * verticalStretch);
            textOffsetX = (int) (textOffsetX * horizontalStretch);
            textOffsetY = (int) (textOffsetY * verticalStretch);
            fontSize = (int) (fontSize * fontStretch);
            
        }
        
        Line line = new Line();
        line.setStartX(startX);
        line.setEndX(endX);
        line.setStartY(startY);
        line.setEndY(startY);
        line.setStroke(Color.GREEN);
        line.setStrokeWidth(8.0);
            
        tempLegendLines.getChildren().add(line);
        
        Text text = (language.isEnglish())?new Text("Own path"):new Text("Din färdväg");
        text.setX(endX + textOffsetX);
        text.setY(startY + textOffsetY);
        text.setFont(new Font(fontSize));
        text.setFill(Color.WHITE);
        
        tempLegendLines.getChildren().add(text);
        
        line = new Line();
        line.setStartX(startX);
        line.setEndX(endX);
        line.setStartY(startY + offsetY);
        line.setEndY(startY + offsetY);
        line.setStroke(Color.BLUE);
        line.setStrokeWidth(4.0);
        
        tempLegendLines.getChildren().add(line);
        
        text = (language.isEnglish())?new Text("Other truck's path"):new Text("Den andra bilens färdväg");
        text.setX(endX + textOffsetX);
        text.setY(startY + offsetY + textOffsetY);
        text.setFont(new Font(fontSize));
        text.setFill(Color.WHITE);
        
        tempLegendLines.getChildren().add(text);
        
        line = new Line();
        line.setStartX(startX);
        line.setEndX(endX);
        line.setStartY(startY + 2*offsetY);
        line.setEndY(startY + 2*offsetY);
        line.setStroke(Color.YELLOW);
        line.getStrokeDashArray().addAll(2.5d, 15d);
        line.setStrokeWidth(4.0);
        
        tempLegendLines.getChildren().add(line);
        
        text = (language.isEnglish())?new Text("Shared path"):new Text("Gemensam färdväg");
        text.setX(endX + textOffsetX);
        text.setY(startY + 2*offsetY + textOffsetY);
        text.setFont(new Font(fontSize));
        text.setFill(Color.WHITE);
        
        tempLegendLines.getChildren().add(text);
        
        return tempLegendLines;
        
        
    }

    private void buttonsEvents(HBox buttonsHB) {
        Object[] bArr = buttonsHB.getChildren().toArray();
        
        for (Object obj : bArr)
        {
            assert (obj instanceof Button);
            Button b = (Button)obj;
            if (b.getText().equals("")) // home button
            {
//                b.setDefaultButton(true);
                b.setOnMousePressed((MouseEvent me)->{
                    
                    stopInactivityAnimation();
                    inactivityTimeline.setOnFinished(null);
                    inactivityTimeline.stop();
                    processSwitchEvent(new SwitchEvent(this, 3, 1));
                });
            }
            if (b.getText().equalsIgnoreCase("skip"))
            {
                b.setOnMousePressed((MouseEvent me)->{
                    
                    stopInactivityAnimation();
                                        
                    restartInactivityAnimation();
                    
                    System.out.println("Skipped by Button");
                
                    searchDone = true;
                    mainRoot.getChildren().remove(textPress);
                    BB.removeButton("skip");
                    showPlatoonOptions();
                    
                });

            }
            if (b.getText().equalsIgnoreCase("replay"))
            {
                b.setOnMousePressed((MouseEvent me)->{
                    // CHANGE HERE
                    System.out.println("REPLAY PRESSED");
                    searchDone = false;
                    BB.removeButton("replay");
                    
                    restartInactivityAnimation();
                    hardCleanScreen();
                    showPlatoonOptions();
                });

            }
            
        }
    }
    
}
