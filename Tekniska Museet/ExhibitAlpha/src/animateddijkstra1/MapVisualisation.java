/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package animateddijkstra1;

import static animateddijkstra1.AnimatedDijkstra1.getHourTime;
import static animateddijkstra1.AnimatedDijkstra1.getMinuteTime;
import static animateddijkstra1.AnimatedDijkstra1.swedenPlatooning;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.ParallelTransition;
import javafx.animation.RotateTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Point3D;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeType;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextBoundsType;
import javafx.util.Duration;
import javafx.util.converter.IntegerStringConverter;

/**
 *
 * @author rui
 */

public class MapVisualisation {
    
    Languages language = new Languages(); //default constructor sets Swedish as startup language
//    Languages language = new Languages("English"); //set English as startup language
    
    boolean canPressRed;
    int sourceNode;
    static int destNode;
    Scene mainScene;
    Group mainRoot;
    Group mainFadeRoot;
    
    FadeTransition fadeToMorph;
    
    Group LinesCustomInterpolatorGroup;
    
    Group cityLinks;
    Group cityNodesNoEdges;
    Group cityNodesEdges;
    Group flagButtons;
    
    HBox buttonsHB;
    ButtonsBar BB;
    
    boolean destinationClicked;
    
    double graphNodeOpacity = 0.6;
    double graphNodeRadius = 12;
    
    DragMe[] dragMes;
        
    boolean bigMode = Globals.bigMode;
    double fontStretch = Globals.fontStretch;
    double horizontalStretch = Globals.horizontalStretch;
    double verticalStretch = Globals.verticalStretch;
    double inactivityTimeSeconds = Globals.inactivityTimeSeconds;
    
    double screenWidth = 1920;
    double screenHeight = 1080;
    
    static int textPositionX = 900;
    static int textPositionY = 100;
    
    
    
    static Text menuText1;
    static Rectangle rectText1;
    double rectArcHeight = 50;
    double rectArcWidth = 50;
    static final String menuText1EN = "Drag and drop the truck\non one of the red\nareas to select the start";
    static final String menuText1SV = "Välj startplats genom\natt dra lastbilen till\nen av de röda punkterna.";
    
    static Text tDest;
    static final String tDestEN = "Choose a destination from\nthe nodes in the map, or\nfrom the named regions";
    static final String tDestSV = "Välj resans mål genom att\nklicka på någon av punkterna\neller på de namngivna orterna.";
    
    int sliderVariable0, sliderVariable1, sliderVariable2, sliderVariable3, sliderVariable4;
    
    boolean showingMenuText1;
    
    ImageView imgView;
    
    SwedenPlatoon swedenPlatooning;
    
    Group destinationMenu;
    
    SequentialTransition mapIntoGraphAnimation;
    TranslateTransition dummyTransition;
    
    private ArrayList<SwitchListener> switchListenerList;
    
    Timeline inactivityTimeline;
    
    Group helpMenuGroup;
    HelpMenu helpMenu;
    
    
    
    
    MapVisualisation(Scene argMainScene, Group argMainRoot, SwedenPlatoon argSwedenPlatooning){
        
        System.out.println("--MapVisualisationConstructor");
        
        mainScene = argMainScene;
        mainRoot = argMainRoot;
        swedenPlatooning = argSwedenPlatooning;
        
        if ( Globals.isSwedishCurrentLanguage ){
    
            language.setSwedish();
    
        }else{
            
            language.setEnglish();
            
        }
        
    }
    
    
    
    private void darkenDestinationMenu(int argNodeHovered){
        
        
        destinationMenu.setOpacity(1.0);
        
        for ( int i = 0 ; i < destinationMenu.getChildren().size() ; i++ ){
            
            Node tempNode = destinationMenu.getChildren().get(i);

            
            if ( argNodeHovered != Integer.parseInt( tempNode.getId()) ){
                tempNode.setOpacity(1.0);
            }else{
                tempNode.setOpacity(0.5);
                
            }
            
        }
        
        
        
    }
    
    public void startDestinationMenu(){
                
        mainFadeRoot.getChildren().remove(menuText1);
        showingMenuText1 = false;
        
        destinationMenu = new Group();         
        
        if (bigMode){
            
            tDest = new Text(horizontalStretch*textPositionX, verticalStretch*textPositionY,"");
            tDest.setFont(new Font(fontStretch*30));
            
        }else{
            
            tDest = new Text(textPositionX, textPositionY,"");
            tDest.setFont(new Font(30));
            
        }       
        
        tDest.setX( rectText1.getX() + 30 );
        tDest.setY( rectText1.getY() + 30 + 35 );
        
        String s = (language.isEnglish())?tDestEN:tDestSV;
        tDest.setText(s);
        tDest.setFill(Color.BLUE);
        tDest.setId("-1");

        destinationMenu.getChildren().add(tDest);
        
        rectText1.setWidth( tDest.getBoundsInLocal().getWidth() + 2*30 );
        
        String[] extraNodes = {"Uppsala","Elfvik","Värmdö","Nynäshamn","Södertälje","Hässelby","Västerås","Mariehamn"};
        
        int tempTextPositionX = 0;
        int tempTextPositionY = 0;
        
        for ( int i = 0 ; i < extraNodes.length ; i++ ){
            
            Group tempGroup = new Group();
            
            double tempHorizontalStretch = 1;
            double tempVerticalStretch = 1;
            
            if (bigMode){
            
                tempHorizontalStretch = horizontalStretch;
                tempVerticalStretch = verticalStretch;

            }
            
            if ( i == 0 ){ // "Uppsala"
                tempTextPositionX = swedenPlatooning.cityPositions[0][0]+ (int) (tempHorizontalStretch*10);
                tempTextPositionY = swedenPlatooning.cityPositions[0][1]+ (int) (tempVerticalStretch*5);
            }
            if ( i == 1 ){ // "Elfvik"
//                destNode = 10;
                tempTextPositionX = swedenPlatooning.cityPositions[10][0]+ (int) (tempHorizontalStretch*10);
                tempTextPositionY = swedenPlatooning.cityPositions[10][1]+ (int) (tempVerticalStretch*35);
            }
            if ( i == 2 ){ // "Värmdö"
//                destNode = 16;
                tempTextPositionX = swedenPlatooning.cityPositions[16][0]- (int) (tempHorizontalStretch*25);
                tempTextPositionY = swedenPlatooning.cityPositions[16][1]- (int) (tempVerticalStretch*10);
            }
            if ( i == 3 ){ // "Nynäshamn"
//                destNode = 15;
                tempTextPositionX = swedenPlatooning.cityPositions[15][0]- (int) (tempHorizontalStretch*50);
                tempTextPositionY = swedenPlatooning.cityPositions[15][1]+ (int) (tempVerticalStretch*50);
            }
            if ( i == 4 ){ // "Södertälje"
//                destNode = 13;
                tempTextPositionX = swedenPlatooning.cityPositions[13][0];
                tempTextPositionY = swedenPlatooning.cityPositions[13][1]+ (int) (tempVerticalStretch*50);
            }
            if ( i == 5 ){ // "Hässelby"
//                destNode = 4;
                tempTextPositionX = swedenPlatooning.cityPositions[4][0]- (int) (tempHorizontalStretch*100);
                tempTextPositionY = swedenPlatooning.cityPositions[4][1]- (int) (tempVerticalStretch*25);
            }
            if ( i == 6 ){ // "Västerås"
//                destNode = 1;
                tempTextPositionX = swedenPlatooning.cityPositions[1][0]- (int) (tempHorizontalStretch*30);
                tempTextPositionY = swedenPlatooning.cityPositions[1][1]- (int) (tempVerticalStretch*10);
            }
            if ( i == 7 ){ // "Mariehamn"
//                destNode = 18;
                tempTextPositionX = swedenPlatooning.cityPositions[18][0]+ (int) (tempVerticalStretch*10);
                tempTextPositionY = swedenPlatooning.cityPositions[18][1];
            }
            
            
            Text t = new Text(tempTextPositionX, tempTextPositionY, extraNodes[i]);

            if (bigMode){
            
                t.setFont(new Font(fontStretch*30));

            }else{
                
                t.setFont(new Font(30));

            }  
            
            t.setFill(Color.RED);
            t.setId("text");
            
            
            
            tempGroup.setId(""+i);
            t.setId(""+i);
            
            IntegerStringConverter integerStringConverter = new IntegerStringConverter(); 

            
            t.setOnMouseClicked(new EventHandler<MouseEvent>() {
                public void handle(MouseEvent me) {
                    
                    restartInactivityAnimation();
                    
                    if ( destinationClicked == true){
                        return;
                    }
                    destinationClicked = true;
                    int nodeHovered = integerStringConverter.fromString( tempGroup.getId() );
                    Node temp = tempGroup.lookup("#text");

                    destNode = 0;
                    if ( nodeHovered == 0 ){ // "Uppsala"
                        destNode = 0;
                    }
                    if ( nodeHovered == 1 ){ // "Elfvik"
                        destNode = 10;
                    }
                    if ( nodeHovered == 2 ){ // "Värmdö"
                        destNode = 16;
                    }
                    if ( nodeHovered == 3 ){ // "Nynäshamn"
                        destNode = 15;
                    }
                    if ( nodeHovered == 4 ){ // "Södertälje"
                        destNode = 13;
                    }
                    if ( nodeHovered == 5 ){ // "Hässelby"
                        destNode = 4;
                    }
                    if ( nodeHovered == 6 ){ // "Västerås"
                        destNode = 1;
                    }
                    if ( nodeHovered == 7 ){ // "Västerås"
                        destNode = 18;
                    }
                    
//                    System.out.println(destNode);
                    swedenPlatooning.definePlatoons(sourceNode, destNode, 0.0);
                                       
                    
                    fadeToMorph.play();
                    
                    cityNodesEdges.setOpacity(1.);
                    
                    
                    
                }
            });
            t.setOnMouseExited(new EventHandler<MouseEvent>() {
                public void handle(MouseEvent me) {
//                    System.out.println("Mouse exited destination box #"+tempGroup.getId()); 
                    int nodeHovered = integerStringConverter.fromString( tempGroup.getId() );
                    Node temp = tempGroup.lookup("#text");
//                    temp.setOpacity(0.5);
                    // Will darken all!
                    darkenDestinationMenu(-99);
                    
                }
            });
            
        
            tempGroup.getChildren().add(t);
//            destinationMenu.getChildren().add(tempGroup);
            destinationMenu.getChildren().add(t);
        
        }
        
        mainFadeRoot.getChildren().add(destinationMenu);
        
        String[] types = {"home"};
        BB = new ButtonsBar(mainScene,types);
        buttonsHB = BB.getHB();
        buttonsEvents(buttonsHB);
        
        
        mainRoot.getChildren().add(buttonsHB);
        
    }
    

    private Group createGraphLinks(){
        
        Group graphLinks = new Group();
        
        for (int i = 0 ; i < swedenPlatooning.cityLinks.length ; i++) {
        
            for (int j = 0 ; j < swedenPlatooning.cityLinks[i].length ; j++) {
        
                Line line = new Line();
                line.setStartX(swedenPlatooning.cityPositions[i][0]);
                line.setStartY(swedenPlatooning.cityPositions[i][1]);
                line.setEndX(swedenPlatooning.cityPositions[ swedenPlatooning.cityLinks[i][j] ][0]);
                line.setEndY(swedenPlatooning.cityPositions[ swedenPlatooning.cityLinks[i][j] ][1]);
                
                line.setStroke(Color.RED);
                
                graphLinks.getChildren().add(line);
            
            }
            
        }
        
        return graphLinks;
        
    }
    
    private Group createGraphLinksWithoutBorders(){
        
        Group graphLinks = new Group();
        
        for (int i = 0 ; i < swedenPlatooning.cityLinks.length ; i++) {
        
            for (int j = 0 ; j < swedenPlatooning.cityLinks[i].length ; j++) {
        
                
                
                if ( i == 0 || j == 0 ){ // "Uppsala"
                    continue;
                }
                if ( i == 10 || j == 10 ){ // "Elfvik"
                    continue;
                }
                if ( i == 16 || j == 16 ){ // "Värmdö"
                    continue;
                }
                if ( i == 15 || j == 15 ){ // "Nynäshamn"
                    continue;
                }
                if ( i == 13 || j == 13 ){ // "Södertälje"
                    continue;
                }
                if ( i == 4 || j == 4 ){ // "Hässelby"
                    continue;
                }
                if ( i == 1 || j == 1 ){ // "Västerås"
                    continue;
                }
                if ( i == 18 || j == 18 ){ // "Mariehamn"
                    continue;
                }
                
                
                
                Line line = new Line();
                line.setStartX(swedenPlatooning.cityPositions[i][0]);
                line.setStartY(swedenPlatooning.cityPositions[i][1]);
                line.setEndX(swedenPlatooning.cityPositions[ swedenPlatooning.cityLinks[i][j] ][0]);
                line.setEndY(swedenPlatooning.cityPositions[ swedenPlatooning.cityLinks[i][j] ][1]);
                
                line.setStroke(Color.RED);
                
                graphLinks.getChildren().add(line);
            
            }
            
        }
        
        return graphLinks;
        
    }
    
    
    private Group createMapLinks(){
        // Creates the map roads
        
        Group mapLinks = new Group();
        List<RoadWaypoints> mapRoadWaypoints;
                
        mapRoadWaypoints = swedenPlatooning.getRoadWayPoints();
        
        RoadWaypoints tempWaypoints;
        double[] polylineWaypoints;
        
        for ( int i = 0 ; i < mapRoadWaypoints.size() ; i++ ){
            
            tempWaypoints = mapRoadWaypoints.get(i);
            
            polylineWaypoints = new double[2 * tempWaypoints.waypoints.length ];
            
            for ( int j = 0 ; j < tempWaypoints.waypoints.length ; j++ ){
                
                polylineWaypoints[2*j] = tempWaypoints.waypoints[j][0];
                polylineWaypoints[2*j+1] = tempWaypoints.waypoints[j][1];
                
            }
            
            Polyline polyline = new Polyline(polylineWaypoints);
            polyline.setStroke(Color.RED);
            polyline.setStrokeWidth(3.0);
            
            mapLinks.getChildren().add(polyline);
                        
        }
        
        return mapLinks;
        
    }
    
    
    public Group createGraphPositions(){
        
        Group graphPositions = new Group();
        
        IntegerStringConverter integerStringConverter = new IntegerStringConverter(); 
        
        for (int i = 0 ; i < swedenPlatooning.numberCities ; i++) {           
            
            Circle circle = new Circle(swedenPlatooning.cityPositions[i][0],
                swedenPlatooning.cityPositions[i][1], graphNodeRadius, Color.web("red", graphNodeOpacity));
            circle.setStrokeType(StrokeType.OUTSIDE);
            circle.setStroke(Color.web("red", 0.16));
            circle.setStrokeWidth(4);
            circle.setId(integerStringConverter.toString(i));
           
            circle.setOnMousePressed((MouseEvent me) -> {
                
                restartInactivityAnimation();
                
                if ( destinationClicked ){
                    
                    
                    return;
                    
                }
                
                destinationClicked = true;
                
                if (canPressRed)
                {
                    
                    
                    
                    circle.setFill(Color.web("yellow", 1));
                    circle.setStroke(Color.web("yellow", 0.16));
                    canPressRed = false;
                    destNode = Integer.parseInt(circle.getId());
                    swedenPlatooning.definePlatoons(sourceNode, destNode, 0.0);
                    
                    fadeToMorph.play();
                    

                    cityNodesEdges.setOpacity(1.);
                }
            });
           
            graphPositions.getChildren().add(circle);
           
        }
        
        return graphPositions;
        
    }
    
    public Group createGraphPositionsWithoutBorders(){
        // Creates the nodes without the edges, for selection of starting point
        Group graphPositions = new Group();        

        IntegerStringConverter integerStringConverter = new IntegerStringConverter(); 
        
        for (int i = 0 ; i < swedenPlatooning.numberCities ; i++) {           
            
                        
            if ( i == 0 ){ // "Uppsala"
                continue;
            }
            if ( i == 10 ){ // "Elfvik"
                continue;
            }
            if ( i == 16 ){ // "Värmdö"
                continue;
            }
            if ( i == 15 ){ // "Nynäshamn"
                continue;
            }
            if ( i == 13 ){ // "Södertälje"
                continue;
            }
            if ( i == 4 ){ // "Hässelby"
                continue;
            }
            if ( i == 1 ){ // "Västerås"
                continue;
            }
            if ( i == 18 ){ // "Mariehamn"
                continue;
            }
            
            Circle circle = new Circle(swedenPlatooning.cityPositions[i][0],
                swedenPlatooning.cityPositions[i][1], graphNodeRadius, Color.web("red", graphNodeOpacity));
            circle.setStrokeType(StrokeType.OUTSIDE);
            circle.setStroke(Color.web("red", 0.05));
            circle.setStrokeWidth(19);
            circle.setId(integerStringConverter.toString(i));
           
            circle.setOnMousePressed((MouseEvent me) -> {
                
                System.out.println("---------CIRCLE PRESSED");
                
                restartInactivityAnimation();
                if ( destinationClicked ){
                    System.out.println("---------IF DESTINATION CLICKED TRUE");
                    return;
                    
                }
                
                
                
                if (canPressRed)
                {
                    System.out.println("---------IF DESTINATION CANPRESSRED TRUE");
                    circle.setFill(Color.web("yellow", 1));
                    circle.setStroke(Color.web("yellow", 0.16));
                    canPressRed = false;
                    destNode = Integer.parseInt(circle.getId());
                    swedenPlatooning.definePlatoons(sourceNode, destNode, 0.0);
                    
                    fadeToMorph.play();
                    
                    cityNodesEdges.setOpacity(1.);
                
                    destinationClicked = true;
                    
                }
                
            });
           
            graphPositions.getChildren().add(circle);
           
        }
        
        return graphPositions;
        
    }
    
    
    public Group showFlagButtons(){
        
        // Flag truck images
        
        Group flagButtons = new Group();
        
        double truckWidth = screenWidth*0.05;
        double trucksCenterX = screenWidth*0.9;
        double trucksCenterY = screenHeight*0.95;
        
        Image truckImgSV = Globals.truckImgSV;
        Image truckImgEN = Globals.truckImgEN;
        ImageView truckImgViewSV = new ImageView(truckImgSV);
        ImageView truckImgViewEN = new ImageView(truckImgEN);
        
        truckImgViewSV.setPreserveRatio(true);
        truckImgViewSV.setFitWidth(truckWidth);
        
        truckImgViewEN.setPreserveRatio(true);
        truckImgViewEN.setFitWidth(truckWidth);
        
        truckImgViewSV.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                switchPageToSwedish();
            }
        });
        
        truckImgViewEN.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                switchPageToEnglish();
            }
        });
        
        truckImgViewEN.setLayoutX(truckImgViewSV.getBoundsInLocal().getMinX() + 1.25*truckWidth);
        
        flagButtons.getChildren().addAll(truckImgViewSV, truckImgViewEN);
        
        flagButtons.setLayoutX( trucksCenterX - flagButtons.getBoundsInLocal().getWidth()/2. );
        flagButtons.setLayoutY( trucksCenterY - flagButtons.getBoundsInLocal().getHeight()/2. );
        
        return flagButtons;
        
    }
    
    
    public void setInitialText(){
        
//        System.out.println("MapVisualisation->setInitialText");
        
        menuText1 = new Text();
        
        if (bigMode){
            
            menuText1.setFont(new Font(fontStretch*30));
            
        }else{
            
            menuText1.setFont(new Font(30));
            
        }
        
        String s = (language.isEnglish())?menuText1EN:menuText1SV;
        menuText1.setText(s);
        
        menuText1.setFill(Color.BLUE);
//        menuText1.setId("-1");
        
        rectText1 = new Rectangle();
        
        double menuWidth =  menuText1.getBoundsInLocal().getWidth() + 2*30;
        double menuHeight =  menuText1.getBoundsInLocal().getHeight() + 2*30;

        rectText1.setX( screenWidth - menuWidth - 100  );
        rectText1.setY( 50 );
        
        menuText1.setX( rectText1.getX() + 30 );
        menuText1.setY( rectText1.getY() + 30 + 35 );
        menuText1.setOpacity(1.);
      
        rectText1.setFill(Color.WHITE);
        rectText1.setOpacity(.75);
        rectText1.setWidth( menuWidth );
        rectText1.setHeight( menuHeight );
            
        rectText1.setArcHeight(rectArcHeight);
        rectText1.setArcWidth(rectArcWidth);
        
        mainFadeRoot.getChildren().add(rectText1);
        mainFadeRoot.getChildren().add(menuText1);
        showingMenuText1 = true;
        
    }
    
    
    private void initializeInactivityAnimation(){
        
        //create a timeline for moving the circle
        inactivityTimeline = new Timeline();
        inactivityTimeline.setCycleCount(1);
        inactivityTimeline.setAutoReverse(false);
        
        //create a keyFrame, the keyValue is reached at time 2s
        Duration duration = Duration.millis(inactivityTimeSeconds*1000);
        //one can add a specific action when the keyframe is reached
        EventHandler onFinished = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                
                System.out.println("-----------Inactivity from MapVisualization");
                inactivityTimeline.setOnFinished(null);
                inactivityTimeline.stop();
                processSwitchEvent(new SwitchEvent(MapVisualisation.this, 1,1));
                // go to Original Menu
                
            }
        };
 
        KeyFrame keyFrame = new KeyFrame(duration, onFinished);
 
        //add the keyframe to the timeline
        inactivityTimeline.getKeyFrames().add(keyFrame);
 
        inactivityTimeline.play();
        
        System.out.println("--MapVisualisation: Start Inactivity");
        
    }
    
    public void restartInactivityAnimation(){
        
        inactivityTimeline.jumpTo(Duration.ZERO);
        inactivityTimeline.play();
                
//        System.out.println("MapVisualisation: Restart Inactivity");
        
    }
    
    public void stopInactivityAnimation(){
        
        inactivityTimeline.setOnFinished(null);
        inactivityTimeline.stop();
        
        System.out.println("--MapVisualisation: Stop Inactivity");
                
    }
        
    public void showMapIntroGraph() throws Exception {
        
        System.out.println("--MapVisualisation->main showMapIntroGraph()");
        
        
        LinesCustomInterpolatorGroup = new Group(); // For memory management
        
        
        initializeInactivityAnimation();
        
        canPressRed = false;
        sourceNode = -1;
        destNode = -1;
        destinationClicked = false;
        showingMenuText1 = false;
        mapIntoGraphAnimation = new SequentialTransition();
        
        // This is the main of this function
        
        mainFadeRoot = new Group();
        
        // 
        Group mapGroup = new Group();
        Image img;
        img = Globals.stockholmMapImage;
        
        imgView = new ImageView(img);
        mapGroup.getChildren().add(imgView);
        mainFadeRoot.getChildren().add(mapGroup);

        //fadeToMorph;
        fadeToMorph = new FadeTransition(Duration.millis(2500), mainFadeRoot);
        fadeToMorph.setFromValue(1.0);
        fadeToMorph.setToValue(0.0);
        fadeToMorph.setCycleCount(1);
        EventHandler onfadeToMorphFinished = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {

                  System.out.println("Triggered playMapIntoGraphAnimation by onfadeToMorphFinished");
                  playMapIntoGraphAnimation();

            }
        };
        fadeToMorph.setOnFinished(onfadeToMorphFinished);
        
        // Creates group and the animation where the map transforms into a graph
        mainRoot.getChildren().add( MapIntoGraphAnimation() );
        mainRoot.getChildren().add( LinesCustomInterpolatorGroup );

        
        // Creates the map roads
        Group cityMapLinks = createMapLinks();
        mainFadeRoot.getChildren().add(cityMapLinks);
        
        // Creates the nodes without the edges, for selection of starting point
        cityNodesNoEdges = createGraphPositionsWithoutBorders();
        mainFadeRoot.getChildren().add(cityNodesNoEdges);
        
        // Creates all the nodes, but make them transparent, only show for the selection of a destination point
        cityNodesEdges = createGraphPositions();
        cityNodesEdges.setOpacity(0.);
        mainRoot.getChildren().add(cityNodesEdges);
        
        // Flag truck images
        flagButtons = showFlagButtons();
        mainFadeRoot.getChildren().add( flagButtons );
        
        // Sets initial text
        setInitialText();
        
        // Set draggable trucks
        Group blues = new Group();
        dragMes = createDraggableTrucks(cityNodesNoEdges);
        
        for (DragMe dm : dragMes)
        {
            blues.getChildren().add(dm.getImageView());
        }
        mainFadeRoot.getChildren().addAll(blues);
        
        mainRoot.getChildren().addAll(mainFadeRoot);
        
        setUpHelpStuff();
        
        flagButtons.toFront();
        
        switchPageToSwedish();
                                
    }
    
            

    private void setUpHelpStuff(){
        
        helpMenuGroup = new Group();
        int stageOfProgram = 0;
        helpMenu = new HelpMenu(inactivityTimeline, stageOfProgram, language.isSwedish() );
        
        // Help menustuff
        helpMenuGroup = helpMenu.createHelpMenu();
        mainFadeRoot.getChildren().add(helpMenuGroup);
        
        helpMenuGroup.setLayoutX( screenWidth/2. - helpMenuGroup.getBoundsInParent().getWidth()/2. );
        helpMenuGroup.setLayoutY( screenHeight/2. - helpMenuGroup.getBoundsInParent().getHeight()/2. );
        
        ImageView helpButtonImageView = helpMenu.createHelpButton();
        mainFadeRoot.getChildren().add(helpButtonImageView);
        
        helpButtonImageView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {

                System.out.println("Restarted counter because I clicked on the helpLogo");
                restartInactivityAnimation();
                
            }

        });
        
    }
    
        
    private DragMe[] createDraggables(Group circles)
    {
        int i = 0;
        double blueRadius = 30.0;
        double blueOpacity = 0.5;
        
        Circle[] arrC  = new Circle[circles.getChildren().size()];
        for (Node c : circles.getChildren())
        {
            Circle cc = (Circle)c;
            arrC[i] = cc;
            i++;
        }
        
        Circle bd1 = new Circle(830,135,blueRadius,Color.web("blue", blueOpacity));
        Group bdots = new Group(bd1);

       
        for (Node bd : bdots.getChildren())
        {
            if (bd instanceof Circle)
            {
                Circle bdc = (Circle)bd;
                bdc.setStrokeType(StrokeType.OUTSIDE);
                bdc.setStroke(Color.web("blue", 0.16));
                bdc.setStrokeWidth(4);
            }
        }
        DragMe d1 = new DragMe(bd1,0,arrC, this);


        DragMe[] dmList = {d1};
                
        return dmList;
        
    }
    
    private DragMe[] createDraggableTrucks(Group circles)
    {
        int truckWidth;
        if (bigMode){
            truckWidth = (int) (horizontalStretch*75);
        }else{
            truckWidth = 75;
        }
        
        
        int i = 0;
        
        
        Circle[] arrC  = new Circle[circles.getChildren().size()];
        for (Node c : circles.getChildren())
        {
            Circle cc = (Circle)c;
            arrC[i] = cc;
            i++;
        }
        
        Image truckImg = Globals.kthTruckImage;
        ImageView truckImgView = new ImageView(truckImg);
        
        truckImgView.setFitWidth(truckWidth);
        truckImgView.setPreserveRatio(true);
        truckImgView.setFitWidth(truckWidth);

        if (bigMode){
            truckImgView.setX( horizontalStretch*(830 - truckWidth/2) );
            truckImgView.setY( verticalStretch*110 );
        }else{
            truckImgView.setX(830 - truckWidth/2);
            truckImgView.setY(110);
        }
        
        
//        truckImgView.setX(830);
//        truckImgView.setY(135);
        
        Group trucks = new Group(truckImgView);
        
        DragMe d1 = new DragMe(truckImgView,0,arrC, this);

        
//        DragMe d2 = new DragMe(bd2,1,arrC, this);
//        DragMe d3 = new DragMe(bd3,2,arrC, this);
//        DragMe d4 = new DragMe(bd4,3,arrC, this);
        
//        DragMe[] dmList = {d1,d2,d3,d4};
        DragMe[] dmList = {d1};
                
        return dmList;
        
    }


    public synchronized void addSwitchListener(SwitchListener sl)
    {
        
        switchListenerList = new ArrayList<SwitchListener>(); // For memory management
        
        if (!switchListenerList.contains(sl))
        {   
            
            switchListenerList.add(sl);
        }
    }

    private void processSwitchEvent(SwitchEvent switchEvent) {
        ArrayList<SwitchListener> tempSwitchListenerList;

        synchronized (this) {
                if (switchListenerList.size() == 0)
                        return;
                tempSwitchListenerList = (ArrayList<SwitchListener>) switchListenerList.clone();
        }

        for (SwitchListener listener : tempSwitchListenerList) {
                listener.switchRequested(switchEvent);
        }
    }
    
    public void cleanScreen(){

//        inactivityTimeline.setOnFinished(null);
        inactivityTimeline.stop();
        
        System.out.println("--MapVisualisation->cleanScreen!");
        
        int numElements = mainRoot.getChildren().size();

        for ( int i = 0 ; i < numElements ; i++ ){

            mainRoot.getChildren().remove(0);

        }
        
        switchListenerList.clear(); // For memory management
    
    }
        
    private Group createAllLines(double frac){
        
        
        
        Group linesGroup = new Group();

        
        List<RoadWaypoints> mapRoadWaypoints;
        
        mapRoadWaypoints = swedenPlatooning.getRoadWayPoints();
        
        RoadWaypoints tempLinearisedWaypoints, tempGraphWaypoints;
        
        int lineId = 0;
        
        for ( int k = 0 ; k < mapRoadWaypoints.size() ; k++ ){
        
            WaypointsHandler waypointsHandler = new WaypointsHandler();

            List<RoadWaypoints> linearisedRoadWaypoints = waypointsHandler.getRoadWaypointsLinearised();

            tempLinearisedWaypoints = linearisedRoadWaypoints.get(k);
            
            List<RoadWaypoints> straightRoadWaypoints = waypointsHandler.getRoadWaypointsStraight();

            tempGraphWaypoints = straightRoadWaypoints.get(k);
            
            for ( int i = 0 ; i < tempLinearisedWaypoints.waypoints.length - 1 ; i++ ){

                Line tempLine = new Line(100, 100, 200, 200);
                
                tempLine.setId(""+lineId);
                
                tempLine.setStartX( tempLinearisedWaypoints.waypoints[i][0] );
                tempLine.setEndX( tempLinearisedWaypoints.waypoints[i+1][0] );
                tempLine.setStartY( tempLinearisedWaypoints.waypoints[i][1] );
                tempLine.setEndY( tempLinearisedWaypoints.waypoints[i+1][1] );
                tempLine.setStrokeWidth(2.);
                tempLine.setStroke(Color.RED);
                
                LinesCustomInterpolatorGroup.getChildren().add(tempLine);
                
                lineId ++;
             
            }

        }
        
        return linesGroup;
        
    }
    
    private void setAllLines(double frac){
        
      
        
        List<RoadWaypoints> mapRoadWaypoints;
        
        mapRoadWaypoints = swedenPlatooning.getRoadWayPoints();
        
        RoadWaypoints tempLinearisedWaypoints, tempGraphWaypoints;
        
        int lineId = 0;
        
        for ( int k = 0 ; k < mapRoadWaypoints.size() ; k++ ){
        
            WaypointsHandler waypointsHandler = new WaypointsHandler();

            List<RoadWaypoints> linearisedRoadWaypoints = waypointsHandler.getRoadWaypointsLinearised();

            tempLinearisedWaypoints = linearisedRoadWaypoints.get(k);
            
            List<RoadWaypoints> straightRoadWaypoints = waypointsHandler.getRoadWaypointsStraight();

            tempGraphWaypoints = straightRoadWaypoints.get(k);
            
            for ( int i = 0 ; i < tempLinearisedWaypoints.waypoints.length - 1 ; i++ ){

                Line tempLine = (Line) LinesCustomInterpolatorGroup.lookup("#"+lineId);

                tempLine.setStartX( ((frac)/1.)*tempGraphWaypoints.waypoints[i][0] + ((1-frac)/1.)*tempLinearisedWaypoints.waypoints[i][0]);
                tempLine.setEndX( ((frac)/1.)*tempGraphWaypoints.waypoints[i+1][0] + ((1-frac)/1.)*tempLinearisedWaypoints.waypoints[i+1][0]);
                tempLine.setStartY( ((frac)/1.)*tempGraphWaypoints.waypoints[i][1] + ((1-frac)/1.)*tempLinearisedWaypoints.waypoints[i][1]);
                tempLine.setEndY( ((frac)/1.)*tempGraphWaypoints.waypoints[i+1][1] + ((1-frac)/1.)*tempLinearisedWaypoints.waypoints[i+1][1]);
                
                lineId ++;
             
            }

        }
        
        
        
    }
    
    
    private Group MapIntoGraphAnimation(){
        // Creates group and the animation where the map transforms into a graph
        Group mapLinks = new Group();
//        Group mapLinks = createAllLines(1.);
        
        createAllLines(1.);
        
        // CUSTOM INTERPOLATOR STUFF
        class LineMorpherInterpolator extends Interpolator {
            @Override
            protected double curve(double t) {
                setAllLines(t);
                return t ;
            }
        }
        Rectangle dummyRectangle = new Rectangle();
        dummyRectangle.setOpacity(0.);
        dummyTransition = new TranslateTransition(Duration.millis(4000), dummyRectangle);
        dummyTransition.setInterpolator( new LineMorpherInterpolator() );

        
        EventHandler onMapIntoGraphAnimationFinished = new EventHandler<ActionEvent>() {
                        public void handle(ActionEvent t) {
//                            System.out.println("mapIntoGraphAnimation.setOnFinished(onFinished);");
                            mainRoot.getChildren().removeAll(buttonsHB);
                            
                            inactivityTimeline.setOnFinished(null);
                            inactivityTimeline.stop();
                            processSwitchEvent(new SwitchEvent(this, 1));
                        }
                    };
        
        
        
        EventHandler onFinishedMorphing = new EventHandler<ActionEvent>() {
                        public void handle(ActionEvent t) {
//                            mainRoot.getChildren().addAll( createGraphPositions() );
                            
                            System.out.println("onFinishedMorphing event handler");
                            
                            Group graphLinks = createGraphLinks();
                            graphLinks.setOpacity(0.0);
                            
                            FadeTransition fadeTransition = new FadeTransition(Duration.millis(2500), graphLinks);
                            fadeTransition.setFromValue(0.0);
                            fadeTransition.setToValue(1.0);
                            fadeTransition.setCycleCount(1);
                            
                            mainRoot.getChildren().addAll( graphLinks );
                            
                            fadeTransition.play();
                            
                        }
                    };
        
        
        dummyTransition.setOnFinished(onFinishedMorphing);
        mapIntoGraphAnimation.getChildren().add(dummyTransition);
        
//        parallelMapIntoGraphAnimation.setOnFinished(onFinishedMorphing);
//        
//        mapIntoGraphAnimation.getChildren().add(parallelMapIntoGraphAnimation);
        
        
        FadeTransition fadeTransition = new FadeTransition(Duration.millis(2500), mapLinks);
        fadeTransition.setFromValue(1.0);
        fadeTransition.setToValue(0.0);
        fadeTransition.setCycleCount(1);
        mapIntoGraphAnimation.getChildren().add( fadeTransition );
                
        mapIntoGraphAnimation.setOnFinished(onMapIntoGraphAnimationFinished);
        
        return mapLinks;

        
    }
    
    private void playMapIntoGraphAnimation(){
        
        mainRoot.getChildren().removeAll(buttonsHB);
//        buttonsHB = buttonUtils.getButtons(mainScene, "home", "skip");
//        String[] types = {"home", "skip"};
//        ButtonsBar BB = new ButtonsBar(mainScene, types);
        BB.addButton("skip");
        buttonsHB = BB.getHB();
        buttonsEvents(buttonsHB);
        mainRoot.getChildren().addAll(buttonsHB);
        
        Text t;
        if (bigMode){
            
            t = new Text(horizontalStretch*880, verticalStretch*225, "");
            t.setFont(new Font(fontStretch*30));
            
        }else{
            
            t = new Text(880, 225, "");
            t.setFont(new Font(30));
        }
        
        
        if (language.isEnglish())
            t.setText("Press here to skip");
        else
            t.setText("Tryck här för att komma vidare");
        
        t.setFill(Color.WHITE);
        
        Group textPress = new Group(t);
        textPress.setOnMousePressed((MouseEvent me)->{
            mainRoot.getChildren().removeAll(buttonsHB);
//            dummyTransition.jumpTo("end");
            mapIntoGraphAnimation.jumpTo("end");
//            processSwitchEvent(new SwitchEvent(this, 1));  
        });
        
        mainRoot.getChildren().add(textPress);
        
//        dummyTransition.play();
        mapIntoGraphAnimation.play();
        
    }
    
    private void buttonsEvents(HBox buttonsHB)
    {
        
        Object[] bArr = buttonsHB.getChildren().toArray();
        
        for (Object obj : bArr)
        {
            assert (obj instanceof Button);
            Button b = (Button)obj;
            if (b.getText().equals("")) // home button
            {
                b.setOnMousePressed((MouseEvent me)->{
                    
                    System.out.println("going home");
                    
                    fadeToMorph.stop();
//                    dummyTransition.stop();
                    mapIntoGraphAnimation.stop();
//                    cleanScreen();
                    inactivityTimeline.setOnFinished(null);
                    inactivityTimeline.stop();
                    processSwitchEvent(new SwitchEvent(MapVisualisation.this, 1,1));
                    for (DragMe dm : dragMes)
                    {
                        dm.reset();
                    }
                    mainFadeRoot.getChildren().removeAll(destinationMenu,buttonsHB);
                    
                    
                });
            }
            else if (b.getText().equalsIgnoreCase("skip"))
            {
                b.setOnMousePressed((MouseEvent e)->{
                    System.out.println("skipped");
                    mainRoot.getChildren().removeAll(buttonsHB);
//                    dummyTransition.jumpTo("end");
                    mapIntoGraphAnimation.jumpTo("end");
//                    processSwitchEvent(new SwitchEvent(this, 1));                
                });
            }

        }
        
    }

    private void switchPageToSwedish() {

        System.out.println("switchPageToSwedish()");
        
        if ( language.isSwedish() ){
            return;
        }else{
            language.setSwedish();
            Globals.isSwedishCurrentLanguage = true;
            System.out.println("CHANGING GLOBAL TO SWEDISH");
        }
        
        
        if (showingMenuText1)
        {
            menuText1.setText(menuText1SV);
            rectText1.setWidth( menuText1.getBoundsInLocal().getWidth() + 2*30 );
        }
        else
        {
            tDest.setText(tDestSV);
            rectText1.setWidth( tDest.getBoundsInLocal().getWidth() + 2*30 );
        }
        
        Text helpMenuText = (Text) helpMenuGroup.lookup("#text");
//        helpMenuText.setText("VÄLKOMMEN\n\n" +
//            "Här är du ägare till en lastbil som har en leverans att utföra. Vi ska visa hur du kan göra det billigare och grönare genom att hitta andra fordon att köra i kolonn med.\n\n" +
//            "Du börjar genom att välja start och mål för din leverans.");
//        helpMenuText.setText(helpMenu.infoTextString0);
        
        String tempInfoTextString0 = Globals.initialHelpSV;
        helpMenuText.setText(tempInfoTextString0);
        
//        Rectangle tempRectangle = (Rectangle) helpMenuGroup.lookup("#infoBox");
//        
//        helpMenuText.setLayoutY( tempRectangle.getBoundsInParent().getMinY() + tempRectangle.getBoundsInParent().getHeight()/2. - helpMenuText.getBoundsInParent().getHeight()/2. + 30);
        
        
    }

    private void switchPageToEnglish() {
        
        System.out.println("switchPageToEnglish()");
        
        if ( language.isEnglish() ){
            return;
        }else{
            language.setEnglish();
            Globals.isSwedishCurrentLanguage = false;
            System.out.println("CHANGING GLOBAL TO ENGLISH");
        }
        
        if (showingMenuText1)
        {
            menuText1.setText(menuText1EN);
            rectText1.setWidth( menuText1.getBoundsInLocal().getWidth() + 2*30 );
            
        }
        else
        {
            tDest.setText(tDestEN);
            rectText1.setWidth( tDest.getBoundsInLocal().getWidth() + 2*30 );
        }
        
        Text helpMenuText = (Text) helpMenuGroup.lookup("#text");
//        helpMenuText.setText("WELCOME TO THE WORLD OF PLATOONING\n\n" +
//            "Here you will be the owner of a truck that has a delivery to do. We will demonstrate how you can do it cheaper and greener by finding other vehicles to platoon with.\n\n" +
//            "You must first define the origin and destination of your delivery.");
        
        String tempInfoTextString0 = Globals.initialHelpEN;
        helpMenuText.setText(tempInfoTextString0);
        
//        Rectangle tempRectangle = (Rectangle) helpMenuGroup.lookup("#infoBox");
        
//        helpMenuText.setLayoutY( tempRectangle.getBoundsInParent().getMinY() + tempRectangle.getBoundsInParent().getHeight()/2. - helpMenuText.getBoundsInParent().getHeight()/2. + 50);
//        tempHelpMenu = new HelpMenu(inactivityTimeline, stageOfProgram, language.isSwedish() );
        
    }
    
    
}