/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package animateddijkstra1;


import java.util.Arrays;
import javafx.collections.ObservableList;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 *
 * @author Matteo
 */
public class ButtonsBar {
    
    
    private static final String[] names = {"replay","cancel","home","continue","skip"};
    private static final double INF = Double.MAX_VALUE;
    private static final Image homeImg = Globals.homeButtonImage;
    
    private HBox hb;
    private VBox vb;
    private static Scene scene;
    private static double offX;
    private static double offY;
    
    boolean bigMode = Globals.bigMode;
    double fontStretch = Globals.fontStretch;
    double horizontalStretch = Globals.horizontalStretch;
    double verticalStretch = Globals.verticalStretch;
    
    public ButtonsBar(Scene argScene, String[] args, double[] pos, double HBoff, String boxType) throws IllegalArgumentException
    {
        if (pos.length!=2)
            throw new IllegalArgumentException("The position array must contain exactly two elements, posX and posY");
        
        offX = pos[0];
        offY = pos[1];
        
        if (boxType.equalsIgnoreCase("h"))
        {
            hb = new HBox(HBoff);
            hb.setLayoutX(offX);
            hb.setLayoutY(offY);
            hb.setMinHeight(50);
            hb.setMaxHeight(50);        
        }
        else if (boxType.equalsIgnoreCase("v"))
        {
            vb = new VBox(HBoff);
            vb.setLayoutX(offX);
            vb.setLayoutY(offY);
            vb.setMinHeight(50);
            vb.setMaxHeight(50);             
        }
        else
            throw new IllegalArgumentException("type can only be either h (horizontal bar) or v (vertical bar)");
            
        scene = argScene;
        
        for (String s : args)
        {
            if (!Arrays.asList(names).contains(s))
            {
                throw new IllegalArgumentException(s+" is not a valid parameter");
            }
            
            else
            {
                addButton(s);
            }
        }
        

    }
    
    public ButtonsBar(Scene argScene, String[] args) throws IllegalArgumentException
    {
        
        
        
        offX = 70;
        offY = 970;
        
        if(bigMode){
            
            offX = horizontalStretch*offX;
            offY = verticalStretch*offY;
            
        }
        
        
        
        if(bigMode){
            
            hb = new HBox(horizontalStretch*50);
            
        }else{
            
            hb = new HBox(50);
            
        }
        
        scene = argScene;
        
        for (String s : args)
        {
            if (!Arrays.asList(names).contains(s))
            {
                throw new IllegalArgumentException(s+" is not a valid parameter");
            }
            
            else
            {
                addButton(s);
            }
        }
        
        hb.setLayoutX(offX);
        hb.setLayoutY(offY);
        
        if(bigMode){
            
            hb.setMinHeight(verticalStretch*50);
            hb.setMaxHeight(verticalStretch*50);
            
        }else{
            
            hb.setMinHeight(50);
            hb.setMaxHeight(50);
            
        }
        
    }
    
    public HBox getHB()
    {
        if (hb != null)
            return hb;
        else
            throw new IllegalArgumentException("hb is null, try with getVB()");
    }
    
    public VBox getVB()
    {
        if (vb != null)
            return vb;
        else
            throw new IllegalArgumentException("vb is null, try with getHB()");
    }
    
    private static void setCursor(Button b)
    {   
            b.setOnMouseEntered((MouseEvent e)->{
                 scene.setCursor(Cursor.DEFAULT);
            });
             
            b.setOnMouseExited((MouseEvent e)->{
                 scene.setCursor(Cursor.DEFAULT);
            });         
    }
    
    public void addButton(String name)
    {   
        for (Button bb : getButtons())
        {
            if (bb.getText().equalsIgnoreCase(name))
                return;
        }
        
        Button b = new Button();
        b.setId(name);
        
        if (name.equals("home"))
        {
            b.setGraphic(new ImageView(homeImg));
        }
        else if (Arrays.asList(names).contains(name))
        {
            b.setTextFill(Color.web("4a4aff", 1));
            b.setText(name.toUpperCase());
            
            if (bigMode){
            
                b.setFont(new Font(fontStretch*20));
                
            }else{
                
                b.setFont(new Font(20));
                
            }
        }
        else
            throw new IllegalArgumentException();
        
        b.setBackground(Background.EMPTY);
        b.setMaxWidth(INF);
        b.setMaxHeight(INF);
        
        if(bigMode){
            
            b.setMinWidth(horizontalStretch*50);
            b.setMinHeight(verticalStretch*50);
            
        }else{
            
            b.setMinWidth(50);
            b.setMinHeight(50);
            
        }
        
        
        setCursor(b);
        
        if (hb!=null)
            hb.getChildren().add(b);
        else
            vb.getChildren().add(b);
        
    }
    
    public void removeButton(String name)
    {        
        Boolean found = false;
        
        if (hb != null)
        {
            for (Node n : hb.getChildren())
            {
                assert n instanceof Button;
                Button b = (Button)n;
                if (b.getText().equalsIgnoreCase(name))
                {
                    hb.getChildren().remove(n);
                    found = true;
                    break;
                }
            }
        }
        else
        {
            for (Node n : vb.getChildren())
            {
                assert n instanceof Button;
                Button b = (Button)n;
                if (b.getText().equalsIgnoreCase(name))
                {
                    vb.getChildren().remove(n);
                    found = true;
                    break;
                }
            }
        
        }
        
        if (found == false)
            throw new IllegalArgumentException("the buttons bar doesn't contain button "+name);
    }
    
    public void changeLabel(String oldS, String newS)
    {
        Object[] arr;
        
        if (hb!=null)  
            arr = hb.getChildren().toArray();
        else
            arr = vb.getChildren().toArray();
     
        for ( Object a : arr)
        {
            Button b = (Button)a;
            if (b.getText().equalsIgnoreCase(oldS) && Arrays.asList(names).contains(newS))
            {
                b.setId(newS);
                b.setText(newS.toUpperCase());
                break;
            }
        }       
        
    }
    
    public Button[] getButtons()
    {
        ObservableList<Node> bList = (hb!=null)? hb.getChildren() : vb.getChildren();
        Button[] bArr = new Button[bList.size()];
        int i  = 0;
        
        for (Node n : bList)
        {
            assert n instanceof Button;
            Button b = (Button)n;
            bArr[i] = b;
            i++;
        }
        
        return bArr;
    }
    
    public Button getButton(String name)
    {
        Button[] bArr = getButtons();
        for (Button b:bArr)
        {
            if (b.getId().equalsIgnoreCase(name))
                return b;
        }
        
        return null;
    }
    
}
