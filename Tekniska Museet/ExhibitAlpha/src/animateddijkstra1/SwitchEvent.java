

package animateddijkstra1;

/**
 *
 * @author Matteo
 */
class SwitchEvent extends java.util.EventObject
{
    private int currentStep;
    private int nextStep;

    public SwitchEvent(Object source, int cs,int ns) {
        super(source);
        this.currentStep = cs;
        this.nextStep = ns;
    }
    
    public SwitchEvent(Object source, int cs) {
        super(source);
        this.currentStep = cs;
        this.nextStep = -1;
    }
    
    public int getCurrStep()
    {
        return this.currentStep;
    }
    
    public int getNextStep()
    {
        return this.nextStep;
    }
    
}

