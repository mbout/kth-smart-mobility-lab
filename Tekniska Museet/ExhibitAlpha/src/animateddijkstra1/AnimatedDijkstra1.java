/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package animateddijkstra1;

import static animateddijkstra1.AnimatedDijkstra1.swedenPlatooning;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level; 
import java.util.logging.Logger;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author rui
 */

public class AnimatedDijkstra1 extends Application {
        
    static public SwedenPlatoon swedenPlatooning = Globals.swedenPlatooning;
    
    Stage stage;
    
    boolean bigMode = Globals.bigMode;

    Group root;
    
    MapVisualisation mapVisualisation;
    static DijkstraVisualisation dijsktraVisualisation;
    PlatoonOptionsVisualisation platoonOptionsVisualisation;
    
    int screenWidth = Globals.screenWidth;
    int screenHeight = Globals.screenHeight;
    
    /////////////////////////////////////////////
    ArrayList<SwitchListener> switchListenerList;
    /////////////////////////////////////////////
    
    static public int getHourTime(double time){
        
        int hourTime;
        
        hourTime = (int)time/(60*60);
     
        return hourTime;
        
    }
    
    static public int getMinuteTime(double time){
        
        int minuteTime;
        
        minuteTime = (int)time%(3600)/60;
                      
        return minuteTime;
        
    }
    
    static public int getSecondTime(double time){
        
        int minuteTime;
        
        minuteTime = (int) (time%(60));
                      
        return minuteTime;
        
    }
    


    
    @Override
    public void start(Stage primaryStage) throws InterruptedException, Exception {
        
//        root = new Group();
        root = Globals.root;

        Scene scene;
        scene = Globals.scene;

        scene.getStylesheets().add("file:style.css");
         
//        mapVisualisation = new MapVisualisation(scene, root, swedenPlatooning);
        Globals.swedenPlatooning = new SwedenPlatoon();
        Globals.language = new Languages();
        mapVisualisation = new MapVisualisation(Globals.scene, Globals.root, Globals.swedenPlatooning);
        
        switchListenerList = new ArrayList<SwitchListener>();
        SwitchListener listener = new MySwitchListener(mapVisualisation);
        mapVisualisation.addSwitchListener(listener);
        
        stage = primaryStage;
        
        if (bigMode){
            stage.initStyle(StageStyle.UNDECORATED);
        }
        
        mapVisualisation.showMapIntroGraph();
                
        primaryStage.setScene(scene);
        primaryStage.setTitle("Animated Dijkstra v1.0");
        primaryStage.show();

    }

    // NEEDS THE STATIC KEYWORD!!!!
    public static void main(String[] args) throws IOException {
          
        launch(args);
    
    }
    
}

class ButtonToExit
{
    
    static public Group addButtonToExit(){
        
        Group buttonGroup = new Group();
        
        Image exitButtonImg = Globals.homeButtonImage;
        ImageView exitButtonImgView = new ImageView(exitButtonImg);
        
        buttonGroup.getChildren().add(exitButtonImgView);
        
        buttonGroup.setOnMousePressed((MouseEvent me)->{
            
            System.exit(0);
            
        });        
        
        return buttonGroup;
        
    }
    
    
    
}

class MySwitchListener implements SwitchListener
{
    private MapVisualisation mv;
    private DijkstraVisualisation dv;
    private PlatoonOptionsVisualisation pov;
    private RealTimeVisualisation rtv;
    
    public MySwitchListener(MapVisualisation argmv)
    {
        mv = argmv;
    }
    
    public MySwitchListener(DijkstraVisualisation argdv)
    {
        dv = argdv;
    }
    
    public MySwitchListener(PlatoonOptionsVisualisation argpov)
    {
        pov = argpov;
    }
    
    public MySwitchListener(RealTimeVisualisation argrtv)
    {
        rtv = argrtv;
    }

    private void StopAllInactivities(){
        
        System.out.println("----Stoping everything");
        
        if ( mv != null){
            mv.stopInactivityAnimation();
        }
        if ( dv != null){
            dv.stopInactivityAnimation();
        }
        if ( pov != null){
            pov.stopInactivityAnimation();
        }
        if ( rtv != null){
            rtv.stopInactivityAnimation();
        }
        
        System.out.println("mv = " + mv);
        System.out.println("dv = " + dv);
        System.out.println("pov = " + pov);
        System.out.println("rtv = " + rtv);
        
    }
    
    
    @Override
    public void switchRequested(SwitchEvent se)
    {
        
        System.out.println("+++++++++++++++++++++++++++++SWITCH REQUESTED+++++++++++++++++++++++++++++");
        System.out.println("Globals.swedenPlatooning.getPlatoonTruckId() = " + Globals.swedenPlatooning.getPlatoonTruckId());
        
        int currStep = se.getCurrStep();
        int nextStep = se.getNextStep();
        
        if (nextStep == -1)
        {
            // Progressing in normal order
            if (currStep == 1)
            {
                mv.stopInactivityAnimation();
                StopAllInactivities();
                
//                System.out.println("Normal progression: switch request to DijkstraVisualisation");
//                System.out.println("mv language "+mv.language.getLang());
//                dv = new DijkstraVisualisation(mv.mainScene, mv.mainRoot, mv.swedenPlatooning, mv.language);
                dv = new DijkstraVisualisation(Globals.scene, Globals.root, Globals.swedenPlatooning, Globals.language);
                
                SwitchListener listener = new MySwitchListener(dv);
                dv.addSwitchListener(listener);
                mv.cleanScreen();
                dv.showDijkstraSearch();
                
//                mv.mainRoot.getChildren().add( ButtonToExit.addButtonToExit() );
                
            }
            else if (currStep == 2)
            {
//                System.out.println("Normal progression: switch request to PlatoonOptionsVisualisation");
                dv.cleanScreen();
                dv.stopInactivityAnimation();
                StopAllInactivities();
//                pov = new PlatoonOptionsVisualisation(dv.mainScene, dv.mainRoot, dv.swedenPlatooning, dv.language);
                pov = new PlatoonOptionsVisualisation(Globals.scene, Globals.root, Globals.swedenPlatooning, Globals.language);
                pov.showPlatoonOptions();
                SwitchListener listener = new MySwitchListener(pov);
                pov.addSwitchListener(listener);
                
//                dv.mainRoot.getChildren().add( ButtonToExit.addButtonToExit() );
            }
            else if (currStep == 3)
            {
                pov.stopInactivityAnimation();
                StopAllInactivities();
//                System.out.println("Normal progression: switch request to RealTimeVisualisation");
//                pov.cleanScreen();
//                rtv = new RealTimeVisualisation(pov.mainScene, pov.mainRoot, pov.swedenPlatooning, pov.language);
                rtv = new RealTimeVisualisation(Globals.scene, Globals.root, Globals.swedenPlatooning, Globals.language);
                rtv.showTrucksMoving();
                SwitchListener listener = new MySwitchListener(rtv);
                rtv.addSwitchListener(listener);
                
//                pov.mainRoot.getChildren().add( ButtonToExit.addButtonToExit() );
                
            }
            else if (currStep == 4)
            {
//                System.out.println("Normal progression: switch request to MapVisualisation");
                rtv.stopInactivityAnimation();
                StopAllInactivities();
                rtv.cleanScreen();
                rtv.setBooleanToFinished();
                
//                try
//                {
//                    mv = new MapVisualisation(rtv.mainScene, rtv.mainRoot, new SwedenPlatoon());
                    mv = new MapVisualisation(Globals.scene, Globals.root, Globals.swedenPlatooning);
//                }
//                catch (IOException ex)
//                {
//                    Logger.getLogger(MySwitchListener.class.getName()).log(Level.SEVERE, null, ex);
//                }
                try {
                    mv.showMapIntroGraph();
                } catch (Exception ex) {
                    Logger.getLogger(MySwitchListener.class.getName()).log(Level.SEVERE, null, ex);
                }
                SwitchListener listener = new MySwitchListener(mv);
                mv.addSwitchListener(listener);
                
//                rtv.mainRoot.getChildren().add( ButtonToExit.addButtonToExit() );
            }
            
            
            
        }
        else if (nextStep == 1)
        {
//            System.out.println("Home shortcut: returning from MapVisualisation");
            
            // This is when home button is pressed
            
            if (currStep == 1) // this is handled internally in MapVisualisation (faster)
            {
//                System.out.println("Home shortcut: returning from MapVisualisation");
//                mv.stopInactivityAnimation();
                mv.cleanScreen();
                mv.stopInactivityAnimation();
                StopAllInactivities();
//                mapVisualisation = new MapVisualisation(scene, root, swedenPlatooning);
                
                
//                RUI STUFF:
//                try
//                {
//                    mv = new MapVisualisation(mv.mainScene, mv.mainRoot, new SwedenPlatoon());
//                    mv = new MapVisualisation(Globals.scene, Globals.root, Globals.swedenPlatooning);
//                }
//                catch (IOException ex)
//                {
//                    Logger.getLogger(MySwitchListener.class.getName()).log(Level.SEVERE, null, ex);
//                }


            }
            else if (currStep == 2)
            {
                // I'm in dijkstraVisualization and want to reset
//                System.out.println("Home shortcut: returning from DijkstraVisualisation");
                dv.cleanScreen();    
                dv.stopInactivityAnimation();
                StopAllInactivities();
//                try
//                {
//                    mv = new MapVisualisation(dv.mainScene, dv.mainRoot, new SwedenPlatoon());
                    mv = new MapVisualisation(Globals.scene, Globals.root, Globals.swedenPlatooning );
//                }
//                catch (IOException ex)
//                {
//                    Logger.getLogger(MySwitchListener.class.getName()).log(Level.SEVERE, null, ex);
//                }
                
//                dv.mainRoot.getChildren().add( ButtonToExit.addButtonToExit() );

            }
            else if (currStep == 3)
            {
                // I'm in platoonoptionsVisualisation and want to reset
//                System.out.println("Home shortcut: returning from PlatoonOptionsVisualisation");
                pov.hardCleanScreen();
                pov.stopInactivityAnimation();
                StopAllInactivities();
//                try
//                {
//                    mv = new MapVisualisation(pov.mainScene, pov.mainRoot, new SwedenPlatoon());
                    mv = new MapVisualisation(Globals.scene, Globals.root, Globals.swedenPlatooning );
//                }
//                catch (IOException ex)
//                {
//                    Logger.getLogger(MySwitchListener.class.getName()).log(Level.SEVERE, null, ex);
//                }
                
//                pov.mainRoot.getChildren().add( ButtonToExit.addButtonToExit() );

            }
            else if (currStep == 4)
            {
                // I'm in realTimeVisualisation and want to reset
//                System.out.println("Home shortcut: returning from RealTimeVisualisation");
                rtv.cleanScreen();
                rtv.setBooleanToFinished();
                rtv.stopInactivityAnimation();
                StopAllInactivities();
                
//                try
//                {
//                    mv = new MapVisualisation(rtv.mainScene, rtv.mainRoot, new SwedenPlatoon());
                    mv = new MapVisualisation(Globals.scene, Globals.root, Globals.swedenPlatooning );
//                }
//                catch (IOException ex)
//                {
//                    Logger.getLogger(MySwitchListener.class.getName()).log(Level.SEVERE, null, ex);
//                }
                
                SwitchListener listener = new MySwitchListener(mv);
                mv.addSwitchListener(listener);
                
//                rtv.mainRoot.getChildren().add( ButtonToExit.addButtonToExit() );
            }

            try {
                mv.showMapIntroGraph();
            } catch (Exception ex) {
                Logger.getLogger(MySwitchListener.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            SwitchListener listener = new MySwitchListener(mv);
            mv.addSwitchListener(listener);  
            
//            mv.mainRoot.getChildren().add( ButtonToExit.addButtonToExit() );
        }
        


    }

}



