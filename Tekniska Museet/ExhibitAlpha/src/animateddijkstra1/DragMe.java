/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package animateddijkstra1;

import javafx.geometry.Point2D;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.util.converter.IntegerStringConverter;

/**
 *
 * @author Matteo
 */
public class DragMe
    { 
        private static final double REL_RADIUS = 75;
        private final int id;
        private Circle c;
        private ImageView iv;
        private double origRad;
        private final double startX;
        private final double startY;
        private double initX;
        private double initY;
        private Point2D dragAnchor;
        private int placedToId;
        private String destId;
        private int goal;
        private static int selected = -1; //keeps track of which truck is selected
        private Label lab;
        private static int progrTruck = 1;
        private Circle[] destList;
        private MapVisualisation mapVisualisation;
        
        public DragMe(Circle mycirc, int n, Circle[] argDestList, MapVisualisation argMapVisualisation)
        {
            
            mapVisualisation = argMapVisualisation;
            c = mycirc;
            origRad = c.getRadius();
            id = n;
            c.setId("bluenode"+id);
            startX = c.getCenterX();
            startY = c.getCenterY();
            destList = argDestList;
            placedToId = -1;
            destId = "";
            lab = new Label();

            c.setOnMouseDragged((MouseEvent me) ->
            {
                mapVisualisation.restartInactivityAnimation();
                
                if (placedToId == -1)
                {
                    double dragX = me.getSceneX() - dragAnchor.getX();
                    double dragY = me.getSceneY() - dragAnchor.getY();
                    //calculate new position of the circle
                    double newXPosition = initX + dragX ;
                    double newYPosition = initY + dragY ;

                    c.setTranslateX(newXPosition);
                    c.setTranslateY(newYPosition);
                    //System.out.println(id + " was dragged (x:" + dragX + ", y:" + dragY +")");                    
                }
            });

            // this works
//            c.setOnMousePressed((MouseEvent me) ->
            c.setOnDragDetected((MouseEvent me) ->
            {
                mapVisualisation.restartInactivityAnimation();
                
                if (placedToId == -1)
                {
                    dragAnchor = new Point2D(me.getSceneX(), me.getSceneY());
                    initX = c.getTranslateX();
                    initY = c.getTranslateY();
                    //System.out.println("Mouse pressed above " + id);
                    //System.out.println("dragAnchor: "+me.getSceneX()+" , "+me.getSceneY());
                    //System.out.println("initX: "+initX+" ,initY "+initY); //always = 0 the first time
                }
            });

            c.setOnMouseReleased((MouseEvent me) ->
            {
                mapVisualisation.restartInactivityAnimation();
                
                if (placedToId == -1)
                {
                    Point2D relPos = new Point2D(me.getSceneX(), me.getSceneY());
                    //System.out.println("relPos is "+relPos.getX()+" , "+relPos.getY()); // OK
                    double mindist = Double.MAX_VALUE;

                    for (int i=0;i<destList.length;i++)
                    {
                        Point2D redDPos = new Point2D(destList[i].getCenterX(),destList[i].getCenterY());
                        double this_d = relPos.distance(redDPos);
                        if (this_d < mindist)
                        {
                            mindist = this_d;
                            placedToId = i;
                        }

                    }

                    //System.out.println("released at distance "+mindist+" from red dot nr "+themin+" at x "+dests[themin].posX+" ,y "+dests[themin].posY);


                    if (mindist <= REL_RADIUS)
                    {
                        
                          System.out.println("attach to red dot node!");
                        c.setTranslateX(initX+destList[placedToId].getCenterX()-startX);
                        c.setTranslateY(initY+destList[placedToId].getCenterY()-startY);
                        //System.out.println(destList[placedToId]);
                        //System.out.println("released to "+(initX+dests[themin].posX-startX)+", "+(initY+dests[themin].posY-startY));
                        this.clip();
                        destId = destList[placedToId].getId();
                        //IntegerStringConverter isc = new IntegerStringConverter();
                        //int nodeClicked = isc.fromString(destId);
                        int nodeClicked = Integer.parseInt(destId);
                        mapVisualisation.canPressRed = true;
                        mapVisualisation.sourceNode = nodeClicked;
                        mapVisualisation.startDestinationMenu();
//                        AnimatedDijkstra1.setSrcNodstartDestinationMenueDijkstra(nodeClicked);
                        //howTo.setText("Click on the destination for truck number "+(progrTruck-1));
                    }
                    else
                    {
                        reset();
//                        System.out.println("released to starting position "+0+", "+0);
                    }                    
                }

            });

        }
        
        public DragMe(ImageView truckIV, int n, Circle[] argDestList, MapVisualisation argMapVisualisation)
        {   
            mapVisualisation = argMapVisualisation;
            
            iv = truckIV;
//            origRad = c.getRadius();
            id = n;
            iv.setId("bluenode"+id);
            startX = iv.getX();
            startY = iv.getY();
            destList = argDestList;
            placedToId = -1;
            destId = "";
            lab = new Label();

            iv.setOnMouseDragged((MouseEvent me) ->
            {
                mapVisualisation.restartInactivityAnimation();
                
                if (placedToId == -1)
                {
                    double dragX = me.getSceneX() - dragAnchor.getX();
                    double dragY = me.getSceneY() - dragAnchor.getY();
                    //calculate new position of the circle
                    double newXPosition = initX + dragX ;
                    double newYPosition = initY + dragY ;

                    iv.setTranslateX(newXPosition);
                    iv.setTranslateY(newYPosition);
                    //System.out.println(id + " was dragged (x:" + dragX + ", y:" + dragY +")");                    
                }
            });

            // this works
            iv.setOnMousePressed((MouseEvent me) ->
            {
                mapVisualisation.restartInactivityAnimation();
                
                if (placedToId == -1)
                {
                    dragAnchor = new Point2D(me.getSceneX(), me.getSceneY());
                    initX = iv.getTranslateX();
                    initY = iv.getTranslateY();
                    //System.out.println("Mouse pressed above " + id);
                    //System.out.println("dragAnchor: "+me.getSceneX()+" , "+me.getSceneY());
                    //System.out.println("initX: "+initX+" ,initY "+initY); //always = 0 the first time
                }
            });

            iv.setOnMouseReleased((MouseEvent me) ->
            {
                mapVisualisation.restartInactivityAnimation();
                
                if (placedToId == -1)
                {
                    Point2D relPos = new Point2D(me.getSceneX(), me.getSceneY());
                    //System.out.println("relPos is "+relPos.getX()+" , "+relPos.getY()); // OK
                    double mindist = Double.MAX_VALUE;

                    for (int i=0;i<destList.length;i++)
                    {
                        Point2D redDPos = new Point2D(destList[i].getCenterX(),destList[i].getCenterY());
                        double this_d = relPos.distance(redDPos);
                        if (this_d < mindist)
                        {
                            mindist = this_d;
                            placedToId = i;
                        }

                    }

                    //System.out.println("released at distance "+mindist+" from red dot nr "+themin+" at x "+dests[themin].posX+" ,y "+dests[themin].posY);


                    if (mindist <= REL_RADIUS)
                    {
                        
//                        System.out.println("attach to red dot node!");
                        
                        iv.setTranslateX(initX+destList[placedToId].getCenterX()-startX-iv.getFitWidth()/2);
                        iv.setTranslateY(initY+destList[placedToId].getCenterY()-startY-iv.getFitWidth()/2);
                        //System.out.println(destList[placedToId]);
                        //System.out.println("released to "+(initX+dests[themin].posX-startX)+", "+(initY+dests[themin].posY-startY));
                        this.clip();
                        destId = destList[placedToId].getId();
                        //IntegerStringConverter isc = new IntegerStringConverter();
                        //int nodeClicked = isc.fromString(destId);
                        int nodeClicked = Integer.parseInt(destId);
                        mapVisualisation.canPressRed = true;
                        mapVisualisation.sourceNode = nodeClicked;
                        
                        mapVisualisation.startDestinationMenu();
//                        AnimatedDijkstra1.setSrcNodstartDestinationMenueDijkstra(nodeClicked);
                        //howTo.setText("Click on the destination for truck number "+(progrTruck-1));
                    }
                    else
                    {
                        reset();
//                        System.out.println("released to starting position "+0+", "+0);
                    }                    
                }

            });

        }

        public Circle getCircle() throws Exception
        {
            if (c!=null)
                return c;
            else
                throw new Exception("The draggable is an ImageView, not a Circle");
        }
        
        public ImageView getImageView() throws Exception
        {
            if (iv!=null)
                return iv;
            else
                throw new Exception("The draggable is an ImageView, not a Circle");
        }

        public void modCircle(double rad, Color col) throws Exception
        {
            if (c!=null)
            {
                c.setRadius(rad);
                c.setFill(col);
            }
            else
                throw new Exception("The draggable is an ImageView, not a Circle");

        }

        public final void reset()
        {
            if (c!=null)
            {
                c.setFill(Color.BLUE);
                //c.setStroke(null);
                c.setTranslateX(0);
                c.setTranslateY(0);
                c.setOpacity(0.5);
                c.setRadius(origRad);
                c.setVisible(true);
            }
            else
            {
                iv.setTranslateX(0);
                iv.setTranslateY(0);
                iv.setVisible(true);
            }
            
            placedToId = -1;
            lab.setVisible(false);
            progrTruck = 1;

        }

        public void disable()
        {
            if (c!=null)
                c.setVisible(false);
            else
                iv.setVisible(false);
        }

        public final void clip()
        {
            if (c!=null)
            {
                //c.setOpacity(1);
                c.toFront();
                c.setRadius(10);
            }

            selected = this.id;
            setLabel(""+progrTruck);
            progrTruck++;
            
        }
        
        public void setLabel(String labText)
        {
            lab.setText(labText);
            lab.setOpacity(1);
            lab.setTextFill(Color.BLUE);
            lab.setFont(new Font("Arial", 30));
            lab.setLayoutX(destList[placedToId].getCenterX()-40);
            lab.setLayoutY(destList[placedToId].getCenterY()-00);
            lab.setVisible(true);
        }
        
        public Label getLabel()
        {
            return this.lab;
        }
        
        public int getDest()
        {
            return this.placedToId;
        }

    }