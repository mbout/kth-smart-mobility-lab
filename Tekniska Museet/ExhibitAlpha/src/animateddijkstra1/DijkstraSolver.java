/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package animateddijkstra1;

/**
 *
 * @author rui
 */

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;

class Vertex implements Comparable<Vertex>{

    public final String name;
    public Edge[] adjacencies;
    public double minDistance = Double.POSITIVE_INFINITY;
    public Vertex previous;
    public double[] position;
    
    public Vertex(String argName) { 
        name = argName; 
        
    }
    
    public String toString() { 
        return name; 
    }
    
    public int compareTo(Vertex other){
        return Double.compare(minDistance, other.minDistance);
    }    

}

class Edge {
    
    public final Vertex target;
    public final double weight;
    
    public Edge(Vertex argTarget, double argWeight) {
        target = argTarget;
        weight = argWeight;
    }
    
}


public class DijkstraSolver {
    
    /*
    int numberCities = 6;
    String[] cityNames = {"Stockholm" , "Sundsvall" , "Umea" , "Kiruna" , "Gotemburg" , "Malmo"};
    double[][] cityPositions = {{164.6353, 373.7044}, {151.9674, 281.5739}, {196.8810, 228.5988}, {194.5777, 70.8253}, {67.8983, 426.6795}, {88.6276, 491.1708}};
    int[][] cityLinks = {{1, 4, 5}, {0, 2}, {1, 3}, {2, 4}, {0, 3, 5}, {0, 4}};
    */
    
    public List<Integer[]> dijkstraIterations = new ArrayList<>();
    
    private Vertex[] verticesList;
    
    private int numberCities;
    private String[] cityNames;
    private double[][] cityPositions;
    private int[][] cityLinks;
    private double[][] adjMatrix;
    
    private List<int []> graphHistory = new ArrayList<>();
    private int[] currentGraph;
    private boolean isRecording = false;
    
    public void setGraph(int argNumberCities, String[] argCityNames, double[][] argAdjMatrix, int[][] argCityLinks) {
        
        numberCities = argNumberCities;
        cityNames = argCityNames;
        adjMatrix = argAdjMatrix; 
        cityLinks = argCityLinks;
                
    }
    
    List<String> readSmallTextFile(String aFileName) throws IOException {

        Charset ENCODING = StandardCharsets.UTF_8;
        
        Path path = Paths.get(aFileName);
        return Files.readAllLines(path, ENCODING);
        
    }
    
    public void resetDijkstraIterations(){
        
        dijkstraIterations = new ArrayList<>();
        
    }
    
    
    public void getGraphFromFile(String argFilename) throws IOException{
        
        //treat as a small file
        List<String> lines = readSmallTextFile(argFilename);
        log(lines);
        
    }
    
    public int getIndexFromName(String argNodeName){
        
        int index = 0;
        
        for ( String cityName : cityNames ){
            
            if ( cityName.equals(argNodeName) ){
                
                return index;
                
            }
            index ++;
            
        }
       
        System.out.println("ERROR: This line should not be achieved. Index must be found during loop!");
        return index;
        
    }
     
    private static void log(Object aMsg){
        System.out.println(String.valueOf(aMsg));
    }
    
    public int getNumberCities(){
        
        return numberCities;
        
    }
    
    public double[][] getAdjMatrix(){
        
        return adjMatrix;
        
    }  
    
    private void storeCurrentDijkstraIteration( Vertex to, Vertex from){
        
        Integer[] currentShortestPath = new Integer[2];
        
        currentShortestPath[0] = getIndexFromName(to.name);
        currentShortestPath[1] = getIndexFromName(from.name);
        
        dijkstraIterations.add(currentShortestPath);
        
    }
    
    public void setRecordingState(boolean state){
        
        isRecording = state;
        
        if (state){
            
            currentGraph = new int[numberCities];
            
        }
        
    }
    
    public List<int []> getGraphHistory(){
        
        return graphHistory;
                
    }
    
    private void recordCurrentGraph(String node, String prevNode){
        
//        private List<int [][]> graphHistory = new ArrayList<>();
        int[] tempGraph = new int[numberCities];
        
        currentGraph[getIndexFromName(node)] = getIndexFromName(prevNode);
        
        for ( int i = 0 ; i < tempGraph.length ; i++ ){
            tempGraph[i] = currentGraph[i];
        }
        
        graphHistory.add(tempGraph);        
        
    }
    
    
    public void computePaths(Vertex source) {
        source.minDistance = 0.;
        PriorityQueue<Vertex> vertexQueue = new PriorityQueue<Vertex>();
        vertexQueue.add(source);
        
        while (!vertexQueue.isEmpty()) {
            Vertex u = vertexQueue.poll();
            
            // Visit each edge exiting u
            for (Edge e : u.adjacencies) {
                Vertex v = e.target;
                double weight = e.weight;
                double distanceThroughU = u.minDistance + weight;
                if (distanceThroughU < v.minDistance) {
                    vertexQueue.remove(v);
                    v.minDistance = distanceThroughU;
                    v.previous = u;
                    
                    if (isRecording){
                        
                        recordCurrentGraph(v.name, u.name);
                        
                        
                    }
                    
                    
                    storeCurrentDijkstraIteration(v, u);
                    
                    
                    vertexQueue.add(v);
                    
                }
            }
                        
        }
    }
    
    public static List<Vertex> getShortestPathTo(Vertex target) {
        List<Vertex> path = new ArrayList<>();
        for (Vertex vertex = target; vertex != null; vertex = vertex.previous){
            path.add(vertex);
        }
        Collections.reverse(path);
        return path;

    }
    
    public int[][] getAllShortestPath() {
        
//        List<Vertex> path = new ArrayList<>();
        
//        for (Vertex vertex = target; vertex != null; vertex = vertex.previous){
//            path.add(vertex);
//        }
//        Collections.reverse(path);
        
        int[][] paths = new int[numberCities][];
        
        int i = 0;
        
        for (Vertex vertex : verticesList){
//            System.out.println("Node: "+i);
            
            List<Integer> shortestPath = new ArrayList<>();
            
            for (Vertex vertexPath = vertex; vertexPath != null; vertexPath = vertexPath.previous){
//                System.out.println(vertexPath.name);
                shortestPath.add( getIndexFromName(vertexPath.name) );
                
            }
            
            paths[i] = new int[shortestPath.size()];
            
            for ( int j = 0 ; j < shortestPath.size() ; j++ ){
                
                paths[i][j] = shortestPath.get(j);
                
            }
            
            i++;
                    
        }
        
        
        return paths;

    }
    
    //public static double getEdgeWeight(Vertex source, Vertex destination){
    public double getEdgeWeight(int source, int destination){
    
        double weight;
        weight = adjMatrix[source][destination];
        return weight;
        
    }
    
    public void testing() {
        
        System.out.println("Worked!");
        
    }

    public void findShortestPath(int sourceIndex, int destinationIndex, List<Integer> shortest_path, List<Double> costs) {
        
        
        Vertex[] vertices = new Vertex[numberCities];
        
        Vertex v_temp;
        for (int i = 0; i < numberCities; i++) {
            
            //v_temp = new Vertex(cityNames[i], cityPositions[i]);
            v_temp = new Vertex(cityNames[i]);
            v_temp.adjacencies = new Edge[ cityLinks[i].length ];
            
            vertices[i] = v_temp; 
            
        }
        
        for (int i = 0; i < numberCities; i++) {
            
            for (int j = 0; j < cityLinks[i].length; j++){
                //double weight = getEdgeWeight(vertices[i], vertices[cityLinks[i][j]]);
                double weight = getEdgeWeight( i, cityLinks[i][j]);
                vertices[i].adjacencies[j] = new Edge(vertices[cityLinks[i][j]], weight);
            }
            
            
        }

        computePaths(vertices[sourceIndex]);
        
        verticesList = vertices;
        
        List<Vertex> path = getShortestPathTo(vertices[destinationIndex]);
        
        //System.out.println(path.size());
        int pathIndexes[] = new int[path.size()];
        
        for (int i = 0 ; i < pathIndexes.length ; i++){
            
            for (int j = 0; j < cityNames.length ; j++){
                
                String tempName;
                tempName = path.get(i).toString();
                
                if ( tempName.equals(cityNames[j]) ) {
                    
                    pathIndexes[i] = j;
                    shortest_path.add(j);
                    
                }
                
            }
                        
        }
        
        for (int i = 0; i < pathIndexes.length-1 ; i++){
            
            //costs.add( getEdgeWeight( vertices[pathIndexes[i]], vertices[pathIndexes[i+1]] ) );
            costs.add( getEdgeWeight( pathIndexes[i] , pathIndexes[i+1] ) );
            
        }
        
    }    
    
}
