/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app;

import javafx.scene.Scene;
import javafx.scene.control.Button;

/**
 *
 * @author Matteo
 */
class MenuButton
{

    private static int order;
    private int myOrder;
    private Button b;
    
    MenuButton(Scene scene, String text)
    {
        b = new Button(text);
        myOrder = order++;
        b.setLayoutX(scene.getWidth()/2);
        //double yPos = scene.getHeight()/2
    }
    
    double getOrder()
    {
        return myOrder;
    }
    
    Button getButton()
    {
        return b;
    }
        
}
