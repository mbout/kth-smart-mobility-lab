
package app;

import javafx.scene.layout.Pane;

public interface ControlledScreen
{
    public void setScreenParent(PageSwitcher screenPage);
    public PageSwitcher getController();
    public Pane getPane();
}
