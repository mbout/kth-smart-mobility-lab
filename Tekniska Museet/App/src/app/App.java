/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app;

import java.util.Timer;
import java.util.TimerTask;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

/**
 *
 * @author Matteo
 */

public class App extends Application
{    
    @Override
    public void start(Stage primaryStage)
    {
        primaryStage.setWidth(Globals.screenWidth);
        primaryStage.setHeight(Globals.screenHeight);
        
        primaryStage.initStyle(StageStyle.UNDECORATED);
        PageSwitcher mainContainer = new PageSwitcher();
        
//        long delay = 5*1000;
//        Timer restartTimer = new Timer();
//        TimerTask restart = new TimerTask()
//        {
//            @Override
//            public void run()
//            {
//                Platform.runLater(() -> {
//                    System.out.println("restarting...");
//                    mainContainer.setPage("videoMenu");
////                    restartTimer.cancel();
////                    Timer newTimer = new Timer();
////                    newTimer.schedule(this, delay);
//                });
//                restartTimer.cancel();
//                restartTimer = new Timer();
//                restartTimer.schedule(this, delay);
//            }
//        };
//        restartTimer.schedule(restart, delay);
        
//        Rectangle dummy = new Rectangle();

        double timeout = Globals.timeOutSeconds*1000;
        Timeline timer = new Timeline();
        timer.setCycleCount(Timeline.INDEFINITE);
        timer.setAutoReverse(false);
        EventHandler onFinished = new EventHandler<ActionEvent>()
        {
            public void handle (ActionEvent t)
            {
                System.out.println("restarting from inactivity");
                mainContainer.setPage("videoMenu");
            }
        };
        final KeyFrame kf = new KeyFrame(Duration.millis(timeout), onFinished);
        timer.getKeyFrames().add(kf);
        timer.play();
        
        mainContainer.loadPage("levelsMenu", new wheelGameMenu());
//        mainContainer.loadPage("levelsMenu", new WheelGameManager());
        mainContainer.loadPage("tutorialLevel", new playLevel(0));
        mainContainer.loadPage("level1", new playLevel(1));
        mainContainer.loadPage("level2", new playLevel(2));
        mainContainer.loadPage("level3", new playLevel(3));
        mainContainer.loadPage("level4", new playLevel(4));
        mainContainer.loadPage("level5", new playLevel(5));
        mainContainer.loadPage("videoMenu", new videoMenu());
        
        mainContainer.setPage("videoMenu");
                
        Group root = new Group();
        root.getChildren().addAll(mainContainer);
        
        Scene scene  = new Scene(root);

        scene.setOnMousePressed((MouseEvent me)->
        {
            System.out.println("Timer reset");
            timer.jumpTo(Duration.ZERO);
        });
        
        primaryStage.setScene(scene);
        primaryStage.show();
        
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        launch(args); 
    }
    
}
