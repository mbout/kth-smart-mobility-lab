/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app;

import java.io.File;
import java.text.DecimalFormat;
import java.util.Random;
import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.RotateTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaMarkerEvent;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextBoundsType;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 *
 * @author rui
 */





public class WhatIsPlatooning {
    
    Group root;
    
    Stage stage;
    
    private Point2D dragAnchor;
    private double initX;
    private double initY;
    static private double newXPosition, newYPosition;
    
    static private Text infoText;
    static private Text infoTextNumber;
    static private Text fuelSavingsText;
    static private Text fuelSavingsNumberText;
    static private Text safeText;
    
    static boolean fun = false;
    
    static Random rand = new Random();
    
    static boolean textShowing = true;
//    String fontString = "Century Gothic";
    String fontString = "";
    
    static long startTime = System.nanoTime();
    static long endTime = System.nanoTime();

    static long duration = (endTime - startTime)/1000000;
    
    static private double truckLengthMeters = 7.92;
    static private double truckLengthPixels = 600;
    static private double meterPerPixel = truckLengthMeters/truckLengthPixels;
    
    static private double frontTruckFraction = 0;
    static private double truckFraction = 0;
    static private double frontTruckTransitionOffsetX = 20;
    static private double truckTransitionOffsetX = -30;
    static private double frontTruckTransitionOffsetY = 15;
    static private double truckTransitionOffsetY = -20;
    static private double frontWheelOffsetX = 62;
    static private double frontWheelOffsetY = 210;
    static private double backWheelOffsetX = 385;
    static private double backWheelOffsetY = 215;
    static private double frontTruckBackWheelOffsetX = 385;
    static private double frontTruckBackWheelOffsetY = 215;
    
    static double inactivityTimeBeforeTextReappears = 7500;
    
    static private double screenWidth = 1920;
//    static private double screenHeight = 1200;
    static private double screenHeight = 1080;
    
    static private double backgroundOffsetY;
//    static private double backgroundOffsetY = 0;
    
    private boolean menuAtBottom = false;
    private boolean noFuelGains = true;
    
    int animationTime = 5000;
    
    static DecimalFormat dfMeters = new DecimalFormat("#0.0");
    static DecimalFormat dfFuelSavings = new DecimalFormat("#00");
    
    static Text slideTruckText;
    
    static ImageView backgroundImageView;
    static ImageView truckImageView;
    static ImageView frontWheelImageView;
    static ImageView backWheelImageView;
    static ImageView frontTruckBackWheelImageView;
    static Group truckGroup;
    static Group frontTruckGroup;
    
    // Truck text Fadings
//    FadeTransition ftAppear, ftDisappear, ftBlink;
    static SequentialTransition ftAppear, ftDisappear;
            
    
    static ImageView frontTruckImageView;
    
//    static Image snowflakeImage = new Image("file:snowflake50.png");
    
//    static Image snowflakeImage = new Image("file:snowflake50White.png");
//    static Image snowflakeImage = new Image("file:cloud50.png");
    static Image snowflakeImage = new Image("file:cloud100.png");
   
    double numSnowFlakes = 400; // 400
    double numTurbulenceSnowFlakes = 100;
    double snowFlakeOpacity = 0.4;
    static ImageView snowflakeImageView;
    static Timeline airDragAnimation;
    
    static ImageView fuelGaugeImageView;
    static ImageView fuelNeedleImageView;
    static Group fuelGroup; 
        
    static boolean firstTime = true;
    
    Group demoGroup = new Group();
    static HBox hb,hbDemo;
    
    static Image replayLogoImg = new Image("file:replayRound.png");
    static ImageView replayLogo = new ImageView(replayLogoImg);
    
    static MediaPlayer mediaPlayer;
    MediaView mediaView;
//    Group moviePane;
    static Group moviePane = new Group();
    
    double videoWidth, videoHeight;
    
    Text repeatText, goToGameText;
    static Group goToGameButton, goToPlatExpress, backToVideo, demoGoToPlatExpress; //repeatButton
    
    static Timeline subtitlesTimeline;
    double videoDuration;
    
    static Timeline restartingAnimation;
    
    String subtitle1Id = "1";
    String subtitle2Id = "2";
    String subtitle3Id = "3";
    String subtitle4Id = "4";
    String subtitle5Id = "5";
    String subtitle6Id = "6";
    String subtitle7Id = "7";
    
    double subtitleStart1, subtitleStart2, subtitleStart3, subtitleStart4, subtitleStart5, subtitleStart6, subtitleStart7; 
    double subtitleEnd1, subtitleEnd2, subtitleEnd3, subtitleEnd4, subtitleEnd5, subtitleEnd6, subtitleEnd7;
    
    Group subtitle1, subtitle2, subtitle3, subtitle4, subtitle5, subtitle6, subtitle7;
    
    double subtitleFadeIn = 250;
    double subtitleFadeOut = 250;
    
    static boolean languageIsSwedish = true;
    
    String subtitle1StringEN = "TRUCKS DRIVE CLOSE BEHIND ONE ANOTHER TO UTILIZE THE ROAD BETTER AND SAVE TIME, FUEL AND EMISSIONS.\nTHE EMISSIONS CAN BE REDUCED BY 20%";
    String subtitle1StringSV = "LASTBILARNA KÖR TÄTT EFTER VARANDRA FÖR ATT UTNYTTJA VÄGEN BÄTTRE, OCH FÖR ATT SPARA TID, BRÄNSLE OCH UTSLÄPP. UTSLÄPPEN KAN MINSKAS MED 20%";
       
    String subtitle2StringEN = "THE DISTANCE BETWEEN THE TRUCKS IS OPTIMIZED TO REDUCE THE AIR DRAG";
    String subtitle2StringSV = "AVSTÅNDET MELLAN LASTBILARA ÄR OPTIMERAT FÖR ATT REDUCERA LUFTMOTSTÅNDET";
    
    String subtitle3StringEN = "THE DISTANCE IS ADJUSTED WHEN A CAR DRIVES IN BETWEEN";
    String subtitle3StringSV = "AVSTÅNDET JUSTERAS NÄR EN BIL KÖR IN EMELLAN";
    
    String subtitle4StringEN = "THE TRUCKS COMMUNICATE WITH RADAR, GPS AND WI-FI";
    String subtitle4StringSV = "LASTBILARNA KOMMUNICERAR MED RADAR, GPS OCH WI-FI";
    
    String subtitle5StringEN = "THE LEAD TRUCK DETERMINES THE ROUTE, SPEED AND POSITION";
    String subtitle5StringSV = "DEN FÖRSTA LASTBILEN BESTÄMMER FÄRDVÄG, HASTIGHET OCH POSITION";
    
    String subtitle6StringEN = "IF THE LEAD TRUCK MAKES AN EVASIVE MANEUVER, THE TRUCKS BEHIND REACT AT THE SAME TIME";
    String subtitle6StringSV = "OM DEN FÖRSTA BILEN GÖR EN UNDANMANÖVER KOMMER LASTBILARNA BAKOM REAGERA SAMTIDIGT";
    
    String subtitle7StringEN = "SAFETY IS IMPORTANT AND THE DRIVER IS ALWAYS STEERING AND CONTROLLING THE TRUCK";
    String subtitle7StringSV = "SÄKERHET ÄR VIKTIGT OCH FÖRAREN ÄR ALLTID MED OCH STYR OCH KONTROLLERAR LASTBILEN";
        
    String slideTextEN = "  Slide here to \nmove the truck";
    String slideTextSV = " Dra med fingret för\n att flytta lastbilen";
    
    static Group truckFlags;
    
    Group returnGroup;
    
    public WhatIsPlatooning()
    {
        returnGroup = getGroup();
        showVideo();
    }
    
    public Group requestGroup()
    {
        return returnGroup;
    }
    
    public Group getGroup() {
        root = new Group();
        
        

//        scene = new Scene(root, 1200, 899, Color.BLACK);
        
//        playGame();
        
        setUpGame();
        setUpVideo();
        setUpScaniaLogo();
        setUpRestartingAnimation();
        setUpReplayButton();
        playVideo();
        switchLanguages(languageIsSwedish);
        
        truckFlags = setUpFlagButtons();
        root.getChildren().add(truckFlags);
        root.getChildren().addAll(replayLogo);
        
        //showVideo();
        return root;
        
    }
    
    private void setUpScaniaLogo(){
        
        Group scaniaLogoGroup = new Group();
                
        double scaniaLogoWidth = 250;
        double textFont = 30;
//        String scaniaString = "Courtesy of Scania";
        String scaniaString = "Courtesy of ";
        
        Image scaniaLogoImage = new Image("file:logoScaniaBigTransp.png");
        ImageView scaniaLogoImageView = new ImageView(scaniaLogoImage);
        
        scaniaLogoImageView.setPreserveRatio(true);
        scaniaLogoImageView.setFitWidth(scaniaLogoWidth);
        
        Text scaniaText = new Text();
        scaniaText.setText(scaniaString);
        scaniaText.setFont( new Font(textFont) );
        scaniaText.setBoundsType(TextBoundsType.VISUAL);
        scaniaText.setFill( new Color(53/255.,240/255.,217/255.,1.0) );
        
        scaniaText.setLayoutX( - 1.1*scaniaText.getBoundsInLocal().getWidth() );
        scaniaText.setLayoutY( scaniaLogoImageView.getBoundsInLocal().getHeight() - scaniaText.getBoundsInLocal().getHeight()/2. );
                
        
        scaniaLogoGroup.getChildren().addAll(scaniaLogoImageView);
//        scaniaLogoGroup.getChildren().add(scaniaText);
        
        moviePane.getChildren().add(scaniaLogoGroup);
        
//        mediaView
        scaniaLogoGroup.setLayoutX( mediaView.getBoundsInParent().getMaxX() - 1.2*scaniaLogoGroup.getBoundsInParent().getWidth() );
        scaniaLogoGroup.setLayoutY( mediaView.getBoundsInParent().getMaxY() - scaniaLogoGroup.getBoundsInParent().getHeight() );
        
    }
    
    private void setUpRestartingAnimation(){
        
        restartingAnimation = new Timeline();
        
        KeyValue kv;
        KeyFrame kf;
         
        Rectangle rectTemp = new Rectangle();
        root.getChildren().add(rectTemp);
        rectTemp.setOpacity(0.);
                
        kv = new KeyValue(rectTemp.opacityProperty(), 0.0);
        kf = new KeyFrame(Duration.millis(videoDuration), kv);
        restartingAnimation.getKeyFrames().add(kf);
                
        EventHandler onFinishedVideo = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {

                
                
                mediaPlayer.play();
                mediaPlayer.seek(Duration.millis(24000));
                subtitlesTimeline.jumpTo("start");
                restartingAnimation.jumpTo("start");
                
                System.out.println("RESTARTING AUTOMATICALLY FROM ON FINISHED");
                
                
                
            }


        };
        
        // After smooth fade, I will go to the next stage of the program
        restartingAnimation.setOnFinished(onFinishedVideo);
        
        
        
        
    }
    
    private void setUpVideo(){
        
        
                // video.mp4
        
//        String trailer = getHostServices().getDocumentBase()+"trailers/sintel.mp4";
//        Media media = new Media(trailer);
        
//        file:snowflake50White.png
        
//        Media media = new Media("file:///home/paul/MoviePlayer/trailers/sintel.mp4");
        
        
//        Media media = new Media("file:video.mp4");
//        Media media = new Media("file:video_H264_AAC.mp4");
//        Media media = new Media("file:video_VP6_MP3.flv");
        
//        Media media = new Media("file:///C:"+File.separator+"Users"+File.separator+"rui"+File.separator+"Documents"+File.separator+"kth-smart-mobility-lab"
//                +File.separator+"TekniskaMuseet"+File.separator+"WhatIsPlatooning"+File.separator+"video_VP6_MP3.flv");
        
//        File file = new File("C:\\Video menu\\touchMV\\03.mp4");
//        File file = new File("C:\\Users\\rui\\Documents\\kth-smart-mobility-lab\\Tekniska Museet\\WhatIsPlatooning\\video.mp4");
        File file = new File("video.mp4");
//        File file = new File("file:video.mp4");
        String path = file.toURI().toASCIIString();
        Media media = new Media(path);
        
//        Media media = new Media("file:///C:/Users/SML/Documents/%20kth-smart-mobility-lab/Tekniska%20Museet/WhatIsPlatooning/video.mp4");
//        Media media = new Media("file:///C:/video.mp4");
                
//        Media media = new Media("file:sound.mp3");
//        System.out.println("Media media = new Media(\"file:sound.mp3\");");
        

        subtitleStart1 = 24000 - 23000;
        subtitleStart2 = 45000 - 23000;// down 
        subtitleStart3 = 56000 - 23000;// down
        subtitleStart4 = (12.9+60)*1000 - 23000;// down
        subtitleStart5 = (24.5+60.)*1000 - 23000; 
        subtitleStart6 = (32.0+60.)*1000 - 23000;
        subtitleStart7 = (49.5+60.)*1000 - 23000;
        
//        media.getMarkers().put(subtitle1Id, Duration.millis(subtitleStart1-subtitleFadeIn + 23000 ));
        media.getMarkers().put(subtitle1Id, Duration.millis(1000 + 23000 ));
        media.getMarkers().put(subtitle2Id, Duration.millis(subtitleStart2-subtitleFadeIn + 23000 ));
        media.getMarkers().put(subtitle3Id, Duration.millis(subtitleStart3-subtitleFadeIn + 23000 ));
        media.getMarkers().put(subtitle4Id, Duration.millis(subtitleStart4-subtitleFadeIn + 23000 ));
        media.getMarkers().put(subtitle5Id, Duration.millis(subtitleStart5-subtitleFadeIn + 23000 ));
        media.getMarkers().put(subtitle6Id, Duration.millis(subtitleStart6-subtitleFadeIn + 23000 ));
        media.getMarkers().put(subtitle7Id, Duration.millis(subtitleStart7-subtitleFadeIn + 23000 ));
              
        
        videoWidth = screenWidth/1.2;
        videoHeight = screenHeight*(0.835);
        
        double videoX = (screenWidth-videoWidth)/2;
        double videoY = (screenHeight-videoHeight)/8;        


        subtitle1 = createVideoSubtitle( subtitle1StringEN,
                videoWidth*0.65, videoHeight*0.225);
        
        subtitle1.setOpacity(0.);
        
        subtitle1.setLayoutX(videoX + videoWidth*0.05);
        subtitle1.setLayoutY(videoY + videoHeight*0.05);
        
        moviePane.getChildren().add(subtitle1);
        
        subtitle2 = createVideoSubtitle( subtitle2StringEN,
            videoWidth * 0.65, videoHeight * 0.13);
        
        subtitle2.setOpacity(0.);
        
        subtitle2.setLayoutX(videoX + videoWidth*0.05);
        subtitle2.setLayoutY(videoY + videoHeight*0.77);
        
        moviePane.getChildren().add(subtitle2);
        
        
        
        subtitle3 = createVideoSubtitle( subtitle3StringEN,
            videoWidth*0.65, videoHeight*0.14);
        
        subtitle3.setOpacity(0.);
        
        subtitle3.setLayoutX(videoX + videoWidth*0.05);
        subtitle3.setLayoutY(videoY + videoHeight*0.76);
        
        moviePane.getChildren().add(subtitle3);
        
        
        subtitle4 = createVideoSubtitle( subtitle4StringEN,
            videoWidth*0.65, videoHeight*0.14);
        
        subtitle4.setOpacity(0.);
        
        subtitle4.setLayoutX(videoX + videoWidth*0.05);
        subtitle4.setLayoutY(videoY + videoHeight*0.81);
        
        moviePane.getChildren().add(subtitle4);
        
        
        
        subtitle5 = createVideoSubtitle( subtitle5StringEN,
            videoWidth*0.65, videoHeight*0.11);
        
        subtitle5.setOpacity(0.);
        
        subtitle5.setLayoutX(videoX + videoWidth*0.05);
        subtitle5.setLayoutY(videoY + videoHeight*0.08);
        
        moviePane.getChildren().add(subtitle5);
        
        
        
        subtitle6 = createVideoSubtitle( subtitle6StringEN,
            videoWidth*0.65, videoHeight*0.11);
        
        subtitle6.setOpacity(0.);
        
        subtitle6.setLayoutX(videoX + videoWidth*0.05);
        subtitle6.setLayoutY(videoY + videoHeight*0.08);
        
        moviePane.getChildren().add(subtitle6);
        
        
        subtitle7 = createVideoSubtitle( subtitle7StringEN,
            videoWidth*0.65, videoHeight*0.11);
        
        subtitle7.setOpacity(0.);
        
        subtitle7.setLayoutX(videoX + videoWidth*0.05);
        subtitle7.setLayoutY(videoY + videoHeight*0.7);
        
        moviePane.getChildren().add(subtitle7);
        

        
        double fadeInSafetyTime = 500;
        double fadeOutSafetyTime = 500;

        

        subtitleStart1 = 24000 - 23000 - fadeInSafetyTime;
        subtitleStart2 = 45000 - 23000 - fadeInSafetyTime;// down 
        subtitleStart3 = 56000 - 23000 - fadeInSafetyTime;// down
        subtitleStart4 = (12.9+60)*1000 - 23000 - fadeInSafetyTime;// down
        subtitleStart5 = (24.5+60.)*1000 - 23000 - fadeInSafetyTime; 
        subtitleStart6 = (32.0+60.)*1000 - 23000 - fadeInSafetyTime;
        subtitleStart7 = (49.5+60.)*1000 - 23000 - fadeInSafetyTime;

        

        subtitleEnd1 = 42200 - 23000 + fadeOutSafetyTime;
        subtitleEnd2 = 54200 - 23000 + fadeOutSafetyTime;
        subtitleEnd3 = (10.5+60)*1000 - 23000 + fadeOutSafetyTime;
        subtitleEnd4 = (24.1+60)*1000 - 23000 + fadeOutSafetyTime;
        subtitleEnd5 = (29.3+60.)*1000 - 23000 + fadeOutSafetyTime; 
        subtitleEnd6 = (48.1+60.)*1000 - 23000 + fadeOutSafetyTime;
        subtitleEnd7 = (2.3+120.)*1000 - 23000 + fadeOutSafetyTime;
        

        subtitlesTimeline = new Timeline();
        subtitlesTimeline.setCycleCount(Timeline.INDEFINITE);
        subtitlesTimeline.setAutoReverse(false);      
        
        mediaPlayer = new MediaPlayer(media);
//        mediaPlayer.setRate(5.0);
        
        
        double videoStart = 24*1000.;
        double videoEnd = (2*60 + 5)*1000.;
        
        mediaPlayer.setStartTime(Duration.millis(videoStart));
        mediaPlayer.setStopTime(Duration.millis(videoEnd));
        
        mediaPlayer.setCycleCount(Timeline.INDEFINITE);
//        mediaPlayer.setRate(10.);
        
        
        
        mediaPlayer.setOnMarker((MediaMarkerEvent event) ->
        {        
            
            System.out.println("Media marker reached:");
            
            if ( event.getMarker().getKey().equals(subtitle1Id)  ){
                
                startSubtitle(subtitle1, subtitleStart1, subtitleEnd1);
                System.out.println("Subtitle 1");                
                
            }
            
            if ( event.getMarker().getKey().equals(subtitle2Id)  ){
                
                startSubtitle(subtitle2, subtitleStart2, subtitleEnd2);
                System.out.println("Subtitle 2");                
                
            }
            
            if ( event.getMarker().getKey().equals(subtitle3Id)  ){
                
                startSubtitle(subtitle3, subtitleStart3, subtitleEnd3);
                System.out.println("Subtitle 3");                
                
            }
            
            if ( event.getMarker().getKey().equals(subtitle4Id)  ){
                
                startSubtitle(subtitle4, subtitleStart4, subtitleEnd4);
                System.out.println("Subtitle 4");                
                
            }
            
            if ( event.getMarker().getKey().equals(subtitle5Id)  ){
                
                startSubtitle(subtitle5, subtitleStart5, subtitleEnd5);
                System.out.println("Subtitle 5");                
                
            }
            
            if ( event.getMarker().getKey().equals(subtitle6Id)  ){
                
                startSubtitle(subtitle6, subtitleStart6, subtitleEnd6);
                System.out.println("Subtitle 6");                
                
            }
            
            if ( event.getMarker().getKey().equals(subtitle7Id)  ){
                
                startSubtitle(subtitle7, subtitleStart7, subtitleEnd7);
                System.out.println("Subtitle 7");                
                
            }
        
        
        
        }); 
        
        
        
        
        videoDuration = videoEnd - videoStart;
                
        mediaView = new MediaView(mediaPlayer);
//        mediaView.setPreserveRatio(false);
        
        
        
        mediaView.setPreserveRatio(false);
        mediaView.setFitWidth(videoWidth);
        mediaView.setFitHeight(videoHeight); 
        
 
                
        mediaView.setX( videoX );
        mediaView.setY( videoY );
        
//        moviePane = new Group();
        
        Rectangle blackBackground = new Rectangle();
        blackBackground.setWidth(screenWidth);
        blackBackground.setHeight(screenHeight);
                
        moviePane.getChildren().add(blackBackground);
        moviePane.getChildren().add(mediaView);
        
        subtitle1.toFront();
        subtitle2.toFront();
        subtitle3.toFront();
        subtitle4.toFront();
        subtitle5.toFront();
        subtitle6.toFront();
        subtitle7.toFront();
        
        root.getChildren().add(moviePane);
                
    }
    
    public Group setUpFlagButtons(){
        
        // Flag truck images
        
        Group flagButtons = new Group();
        
        double truckWidth = 100;
        double truckOffsetX = 125;
        double centerX = screenWidth*(17.5/20.);
        double centerY = screenHeight*(18/20.);
        
        Image truckImgSV;
        Image truckImgEN;
        ImageView truckImgViewSV;
        ImageView truckImgViewEN;
        
        truckImgSV = new Image("file:flagTruckSweden.png");
        truckImgViewSV = new ImageView(truckImgSV);
        truckImgViewSV.setFitWidth(truckWidth);
        truckImgViewSV.setPreserveRatio(true);
        truckImgViewSV.setX(centerX - truckImgViewSV.getFitWidth()/2);
        truckImgViewSV.setY(centerY - truckImgViewSV.getFitHeight()/2);
        truckImgViewSV.setOnMousePressed((MouseEvent me)->{
            if (!languageIsSwedish){
                
                languageIsSwedish = true;
                switchLanguages(languageIsSwedish);
            
            }
        });
     
        flagButtons.getChildren().add(truckImgViewSV);
        
        truckImgEN = new Image("file:flagTruckEngland.png");
        truckImgViewEN = new ImageView(truckImgEN);
        truckImgViewEN.setFitWidth(truckWidth);
        truckImgViewEN.setPreserveRatio(true);
        truckImgViewEN.setX(centerX - truckImgViewEN.getFitWidth()/2 + truckOffsetX);
        truckImgViewEN.setY(centerY - truckImgViewEN.getFitHeight()/2);
        truckImgViewEN.setOnMousePressed((MouseEvent me)->{
            if (languageIsSwedish){
                
                languageIsSwedish = false;
                switchLanguages(languageIsSwedish);
            
            }
            
        });
 
        flagButtons.getChildren().add(truckImgViewEN);
        
        return flagButtons;
    }
    
    private void switchLanguages(boolean argLanguageIsSwedish){
        
        System.out.println("Changing to:");
        
        if (argLanguageIsSwedish){
            
            System.out.println("Swedish");
            
            Text subtitle1Text = (Text) subtitle1.lookup("#text");
            subtitle1Text.setText(subtitle1StringSV);
            
            Text subtitle2Text = (Text) subtitle2.lookup("#text");
            subtitle2Text.setText(subtitle2StringSV);
            subtitle2Text.setFont(new Font("Calibri",48) );
            
            Text subtitle3Text = (Text) subtitle3.lookup("#text");
            subtitle3Text.setText(subtitle3StringSV);
            
            Text subtitle4Text = (Text) subtitle4.lookup("#text");
            subtitle4Text.setText(subtitle4StringSV);
            
            Text subtitle5Text = (Text) subtitle5.lookup("#text");
            subtitle5Text.setText(subtitle5StringSV);
            
            Text subtitle6Text = (Text) subtitle6.lookup("#text");
            subtitle6Text.setText(subtitle6StringSV);
            
            Text subtitle7Text = (Text) subtitle7.lookup("#text");
            subtitle7Text.setText(subtitle7StringSV);
            subtitle7Text.setFont(new Font("Calibri",52) );
            
            Text gameText = (Text) goToGameButton.lookup("#text");
            gameText.setText("DEMO");
            
//            Text platText = (Text) goToPlatExpress.lookup("#text");
            Text newText = Globals.setText("SPEL", (Rectangle)goToPlatExpress.lookup("#rect"));
            goToPlatExpress.getChildren().remove(goToPlatExpress.lookup("#text"));
            newText.setId("text");
            goToPlatExpress.getChildren().addAll(newText);
            
            Text newText2 = Globals.setText("SPEL", (Rectangle)demoGoToPlatExpress.lookup("#rect"));
            demoGoToPlatExpress.getChildren().remove(demoGoToPlatExpress.lookup("#text"));
            newText2.setId("text");
            demoGoToPlatExpress.getChildren().addAll(newText2);
            
//            node.setId(string)
//                    .lookup("#"+string)
            
//            Text repeatTempText = (Text) repeatButton.lookup("#text");
//            repeatTempText.setText("ÅTERSTART VIDEO"); 
//            repeatTempText.setLayoutX(30);
                        
            slideTruckText.setText(slideTextSV);
            slideTruckText.setLayoutX(10);
            
            fuelSavingsText.setLayoutX(-75);
//            fuelSavingsNumberText.setLayoutX(-75);
//            infoText.setLayoutX(-50);
            safeText.setLayoutX( screenWidth/2. - safeText.getBoundsInLocal().getWidth()/2. );
            
//            changing here!!!!!
            fuelSavingsNumberText.setLayoutX( fuelSavingsText.getBoundsInParent().getMinX() + fuelSavingsText.getBoundsInLocal().getWidth()/2. - fuelSavingsNumberText.getBoundsInLocal().getWidth()/2.);
            
        }else{
            
            System.out.println("English");
            
            Text subtitle1Text = (Text) subtitle1.lookup("#text");
            subtitle1Text.setText(subtitle1StringEN);
            
            Text subtitle2Text = (Text) subtitle2.lookup("#text");
            subtitle2Text.setText(subtitle2StringEN);
            subtitle2Text.setFont(new Font("Calibri",50) );
            
            Text subtitle3Text = (Text) subtitle3.lookup("#text");
            subtitle3Text.setText(subtitle3StringEN);
            
            Text subtitle4Text = (Text) subtitle4.lookup("#text");
            subtitle4Text.setText(subtitle4StringEN);
            
            Text subtitle5Text = (Text) subtitle5.lookup("#text");
            subtitle5Text.setText(subtitle5StringEN);
            
            Text subtitle6Text = (Text) subtitle6.lookup("#text");
            subtitle6Text.setText(subtitle6StringEN);
            
            Text subtitle7Text = (Text) subtitle7.lookup("#text");
            subtitle7Text.setText(subtitle7StringEN);
            subtitle7Text.setFont(new Font("Calibri",50) );
            
            Text gameText = (Text) goToGameButton.lookup("#text");
            gameText.setText("DEMO");
            
            Text newText = Globals.setText("GAME", (Rectangle)goToPlatExpress.lookup("#rect"));
            goToPlatExpress.getChildren().remove(goToPlatExpress.lookup("#text"));
            newText.setId("text");
            goToPlatExpress.getChildren().addAll(newText);
            
            Text newText2 = Globals.setText("GAME", (Rectangle)demoGoToPlatExpress.lookup("#rect"));
            demoGoToPlatExpress.getChildren().remove(demoGoToPlatExpress.lookup("#text"));
            newText2.setId("text");
            demoGoToPlatExpress.getChildren().addAll(newText2);
            
//            Text repeatTempText = (Text) repeatButton.lookup("#text");
//            repeatTempText.setText("RESTART VIDEO");
//            repeatTempText.setLayoutX(55);
            
            slideTruckText.setText(slideTextEN);
            slideTruckText.setLayoutX(0);
            
        }
        
    }
    
    private void startSubtitle(Group argSubtitle, double argSubtitleStart, double argSubtitleEnd){
        
        int numKeyFrames = subtitlesTimeline.getKeyFrames().size();
        
        subtitlesTimeline.jumpTo("end");
        subtitlesTimeline.stop();
        
        for ( int i = 0 ; i < numKeyFrames ; i++){
            
            subtitlesTimeline.getKeyFrames().remove(0);
            
        }
        
        KeyValue kv;
        KeyFrame kf;
         
        kv = new KeyValue(argSubtitle.opacityProperty(), 0.0);
//        kf = new KeyFrame(Duration.millis(argSubtitleStart-argSubtitleFadeIn), kv);
        kf = new KeyFrame(Duration.millis(0), kv);
        subtitlesTimeline.getKeyFrames().add(kf);
        
        kv = new KeyValue(argSubtitle.opacityProperty(), 1.0);
        kf = new KeyFrame(Duration.millis(subtitleFadeIn), kv);
        subtitlesTimeline.getKeyFrames().add(kf);
        
        kv = new KeyValue(argSubtitle.opacityProperty(), 1.0);
        kf = new KeyFrame(Duration.millis(argSubtitleEnd - argSubtitleStart), kv);
        subtitlesTimeline.getKeyFrames().add(kf);
        
        kv = new KeyValue(argSubtitle.opacityProperty(), 0.0);
        kf = new KeyFrame(Duration.millis(argSubtitleEnd - argSubtitleStart + subtitleFadeOut), kv);
        subtitlesTimeline.getKeyFrames().add(kf);
        
        subtitlesTimeline.setCycleCount(1);
        subtitlesTimeline.setAutoReverse(false);
        
        
        subtitlesTimeline.jumpTo("start");
        subtitlesTimeline.play();
        
    }
    
    private Group createVideoSubtitle(String argText, double argWidth, double argHeight){
        
        Group videoSubtitle = new Group();
        
        double borderline = argWidth*(1/50.);
        
        Text text = new Text();
        text.setBoundsType(TextBoundsType.VISUAL);
        
        text.setWrappingWidth( argWidth*(19/20.) );
        
//        text.setFont(new Font(50));
        
        System.out.println("subtitle2StringSV = " + subtitle2StringSV);
        System.out.println("argText = " + argText);
        System.out.println("subtitle2StringSV.equals(argText) = " + subtitle2StringSV.equals(argText));
        
//        if ( subtitle2StringSV.equals(argText) ){
//            text.setFont(new Font("Calibri",40) );
//        }else{
//            text.setFont(new Font("Calibri",50) );
//        }
       
        text.setFont(new Font("Calibri",50) );
        
        text.setId("text");
        
        text.setText("A"); 
        double lineHeight = text.getBoundsInLocal().getHeight();
        
        text.setText(argText);
        text.setFill( new Color(53/255.,240/255.,217/255.,1.0) );
       
        
        Rectangle rect = new Rectangle();
        
        rect.setWidth(argWidth);
        double insideRectHeight = ( text.getBoundsInLocal().getHeight() + 2*borderline );
        rect.setHeight( insideRectHeight );
//        rect.setLayoutX();
//        rect.setLayoutY(argX);
//        rect.setFill(Color.AZURE);
        rect.setFill(new Color(35/255.,53/255.,61/255.,1.0));
        
        rect.setArcWidth(20);
        rect.setArcHeight(20);
        
        text.setLayoutX( borderline );
        text.setLayoutY( borderline + lineHeight );
        
        Rectangle rectBorder = new Rectangle();
        
        double borderRatioX = 1.015;
        double borderRatioY = 1.05;
        
        rectBorder.setWidth( argWidth*borderRatioX );
        rectBorder.setHeight( insideRectHeight * borderRatioY );
        rectBorder.setLayoutX( -(argWidth*(borderRatioX-1.)/2.) );
        rectBorder.setLayoutY( -(insideRectHeight*(borderRatioY-1.)/2.) );
//        rect.setLayoutY(argX);
//        rect.setFill(Color.AZURE);
        rectBorder.setFill( new Color(217/255.,217/255.,219/255.,1.0) );
        
        rectBorder.setArcWidth(20);
        rectBorder.setArcHeight(20);
        
        text.setLayoutX( borderline );
        text.setLayoutY( borderline + lineHeight );
        
        videoSubtitle.getChildren().add(rectBorder);
        videoSubtitle.getChildren().add(rect);
        videoSubtitle.getChildren().add(text);
        
//        mediaView.getBoundsInLocal().getHeight();   
        
//        videoWidth
//        System.out.println("mediaView.getBoundsInLocal().getHeight(): "+mediaView.getBoundsInLocal().getHeight());
//        mediaView.getBoundsInLocal().getHeight()
//        
//        text.setWrappingWidth(double)
        
        
        
        return videoSubtitle;
        
    }
    
    private void playVideo(){
        
        mediaPlayer.play();
        mediaPlayer.seek(Duration.millis(24000));
        subtitlesTimeline.play();
        subtitlesTimeline.jumpTo("start");
        restartingAnimation.play();
       
        // ADD VIDEO TEXT METHOD
        
        setVideoText();
        
    }
    
    private void setVideoText(){
        
//        String restartString = "RESTART VIDEO";
//        String playString = "PLAY GAME";
        
//        String restartString = "ÅTERSTART VIDEO";
        String playString = "DEMO";
        String platString = "GAME";
        String platDString = "GAME";
        String btvString = "VIDEO";
        
        double buttonDistance = 100;
//        double widthFactor = (mediaView.getBoundsInLocal().getWidth()-2*buttonDistance)/3.0;
        
//        Rectangle repeat = Globals.getButtonRect();
//        Text repeatT = Globals.setText(restartString, repeat);
//        repeatT.setId("text");
        
//        repeatButton = new Group(repeat,repeatT);
        
//        repeatButton = createEqualButtons(restartString, playString); 
//        root.getChildren().add(repeatButton);
        
//        double buttonOffsetY = -15;
//        
//        repeatButton.setLayoutX( screenWidth*(1./3) - repeatButton.getBoundsInLocal().getWidth()/2 );
//        repeatButton.setLayoutY( screenHeight - repeatButton.getBoundsInLocal().getHeight() + buttonOffsetY );
        
//        repeatButton.setOnMousePressed((me) ->
//        {   
//            
//            System.out.println("RESTART PRESSED");
//            
////            repeatButton.setLayoutX( screenWidth*(1./3) - repeatButton.getBoundsInLocal().getWidth()/2 );
////            repeatButton.setLayoutY( screenHeight - repeatButton.getBoundsInLocal().getHeight() + buttonOffsetY );
//            goToGameButton.setOpacity(1.);
//            
//            mediaPlayer.play();
//            mediaPlayer.seek(Duration.millis(24000));
//            subtitlesTimeline.jumpTo("start");
//            restartingAnimation.jumpTo("start");
//            restartingAnimation.play();
//            
//            
//            moviePane.setOpacity(1.);
//            moviePane.toFront();
//            repeatButton.toFront();
//            goToGameButton.toFront();
//            truckFlags.toFront();
//            
//        });
        
        Rectangle goGame = Globals.getButtonRect();
        goGame.setId("rect");
        Text goGameT = Globals.setText(playString, goGame);
        goGameT.setId("text");
                
        goToGameButton = new Group(goGame,goGameT);
        
//        goToGameButton = createEqualButtons(playString, restartString);
//        root.getChildren().add(goToGameButton);
        
//        goToGameButton.setLayoutX( screenWidth*(2./3) - goToGameButton.getBoundsInLocal().getWidth()/2 );
//        goToGameButton.setLayoutY( screenHeight - goToGameButton.getBoundsInLocal().getHeight() + buttonOffsetY );
        
        goToGameButton.setOnMousePressed((me) ->
        {   
            
            System.out.println("TO DEMO PRESSED");
            
//            repeatButton.setLayoutX( screenWidth - 500 );
//            repeatButton.setLayoutY( 100 );
            showDemo();
        
        });
        
        Rectangle btv = Globals.getButtonRect();
        btv.setId("rect");
        Text btvT = Globals.setText(btvString, btv);
        btvT.setId("text");
        
        backToVideo = new Group(btv,btvT);
        
        backToVideo.setOnMousePressed((me) ->
        {   
            
            System.out.println("BACK TO VIDEO PRESSED");
            
//            repeatButton.setLayoutX( screenWidth - 500 );
//            repeatButton.setLayoutY( 100 );
            showVideo();
        
        });
        
        Rectangle goPlat = Globals.getButtonRect();
        goPlat.setId("rect");
        Text goPlatT = Globals.setText(platString, goPlat);
        goPlatT.setId("text");
        
        goToPlatExpress = new Group(goPlat,goPlatT);
        goToPlatExpress.setId("gameButton");
        //pressing managed in the wrapper (videoMenu)
        
        Rectangle goPlatD = Globals.getButtonRect();
        goPlatD.setId("rect");
        Text goPlatDT = Globals.setText(platDString, goPlatD);
        goPlatDT.setId("text");
        
        demoGoToPlatExpress = new Group(goPlatD,goPlatDT);
        demoGoToPlatExpress.setId("gameButton");
        
        
        hb = new HBox(buttonDistance);
        hb.getChildren().addAll(goToGameButton,goToPlatExpress);
        hb.setLayoutX((screenWidth-2*Globals.buttonWidth-buttonDistance)/2.0);
        hb.setLayoutY(screenHeight*18./20.-Globals.buttonHeight/2.0+20.0);
        hb.setId("buttonBar");
        
        hbDemo = new HBox(buttonDistance);
        hbDemo.getChildren().addAll(backToVideo,demoGoToPlatExpress);
        hbDemo.setLayoutX((screenWidth-2*Globals.buttonWidth-buttonDistance)/2.0);
        hbDemo.setLayoutY(screenHeight*18./20.-Globals.buttonHeight/2.0+20.0);
        hbDemo.setId("buttonBarDemo");
        
        hbDemo.setVisible(false);
        
        root.getChildren().addAll(hb,hbDemo);
        
    }
    
    private void setUpGame(){
        
        
        settingUpImageViews();
        
        setWheelRotations();
        
        setTruckTextFadeTransitions();
        
        setTruckRandomMovements();
        
        initializeGameMenu();
        
        initializeText();
        
        root.getChildren().add(backgroundImageView);
        root.getChildren().add(truckGroup);
        root.getChildren().add(frontTruckGroup);
        root.getChildren().add(fuelGroup);
        
//        demoGroup.getChildren().addAll(backgroundImageView,truckGroup,frontTruckGroup,fuelGroup);
        
//        root.getChildren().addAll(demoGroup);

        initX = newXPosition;
        initY = newYPosition;

        dragAnchor = new Point2D(400, 0);
        
        setUpTruckEvents();
        
        setUpAirDragAnimation();
        
        
        
        
    }
    
    private void setUpAirDragAnimation(){
        
        
        double offsetY;
        for ( int i = 0; i < numSnowFlakes ; i++){
        
            offsetY = 50*rand.nextDouble() - 25;
            
            ImageView tempSnowflakeImageView = new ImageView(snowflakeImage);
            tempSnowflakeImageView.setMouseTransparent(true);
            root.getChildren().add(tempSnowflakeImageView);
            createAirDragAnimation(tempSnowflakeImageView, i, offsetY);
        
        }
        
        
    }
    
    private void setUpTruckEvents(){
        
        
        truckGroup.setOnMousePressed((me) ->
//        truckGroup.setOnDragDetected((me) ->

        {   
            
            dragAnchor = new Point2D(me.getSceneX(), me.getSceneY());
            
//            System.out.println("me.getSceneX(): "+ me.getSceneX() );
            
            startTime = System.nanoTime();
            
            if (firstTime){
            
                
                firstTime = false;
                
                textShowing = false;
                startTime = System.nanoTime();
                
                ftAppear.stop();
                
                ftDisappear.play();
                
            
            }
            

        });
        
        
        truckGroup.setOnMouseDragged((MouseEvent me) ->
        {
            
            double dragX = me.getSceneX() - dragAnchor.getX();
            double dragY = me.getSceneY() - dragAnchor.getY();
            
            //calculate new position of the circle
            newXPosition = initX + dragX ;
            newYPosition = initY + dragY ;
            
            truckImageView.setX(newXPosition);
            frontWheelImageView.setX(newXPosition + frontWheelOffsetX);
            backWheelImageView.setX(newXPosition + backWheelOffsetX);
            slideTruckText.setX(newXPosition + 130);
            
            
            
            updateText();
            
            
            
        });
        
        truckGroup.setOnMouseReleased((MouseEvent me) ->
        {
            
            initX = newXPosition;
            initY = newYPosition;
            
            dragAnchor = new Point2D(me.getSceneX(), me.getSceneY());

        });

        
        
    }
    
    private void setTruckTextFadeTransitions(){
        
        FadeTransition ftAppearTemp = new FadeTransition(Duration.millis(500), slideTruckText);
        ftAppearTemp.setFromValue(0.0);
        ftAppearTemp.setToValue(1);
        ftAppearTemp.setCycleCount(1);
        ftAppearTemp.setAutoReverse(false);
        
        FadeTransition ftBlink = new FadeTransition(Duration.millis(400), slideTruckText);
        ftBlink.setFromValue(1.0);
        ftBlink.setToValue(0.7);
        ftBlink.setCycleCount(Timeline.INDEFINITE);
        ftBlink.setAutoReverse(true);
        
        ftAppear = new SequentialTransition(ftAppearTemp, ftBlink);
        
        FadeTransition ftDisappearTemp = new FadeTransition(Duration.millis(500), slideTruckText);
        ftDisappearTemp.setFromValue(1.0);
        ftDisappearTemp.setToValue(0.0);
        ftDisappearTemp.setCycleCount(1);
        ftDisappearTemp.setAutoReverse(false);
        
        ftDisappear = new SequentialTransition(ftDisappearTemp);
        
    }
    
    private void setWheelRotations(){
        
        double rotationDuration = 500;
        
        RotateTransition rt = new RotateTransition(Duration.millis(rotationDuration), frontWheelImageView);
        rt.setByAngle(-360);
        rt.setCycleCount(Timeline.INDEFINITE);
        rt.setAutoReverse(false);
        rt.setInterpolator(Interpolator.LINEAR);

        rt.play();
        
        rt = new RotateTransition(Duration.millis(rotationDuration), backWheelImageView);
        rt.setByAngle(-360);
        rt.setCycleCount(Timeline.INDEFINITE);
        rt.setAutoReverse(false);
        rt.setInterpolator(Interpolator.LINEAR);

        rt.play();
        
        rt = new RotateTransition(Duration.millis(rotationDuration), frontTruckBackWheelImageView);
        rt.setByAngle(-360);
        rt.setCycleCount(Timeline.INDEFINITE);
        rt.setAutoReverse(false);
        rt.setInterpolator(Interpolator.LINEAR);
        
        rt.play();
        
        
    }
            
    
    private void settingUpImageViews(){
        
        truckGroup = new Group();
        frontTruckGroup = new Group();
        
        Image backgroundImage;
        if (!fun){
//            backgroundImage = new Image("file:roadSideviewExtendedTwiceSignTouchScreen.jpg");
            backgroundImage = new Image("file:roadSideviewExtendedTwiceSignTouchScreen.png");
//            backgroundImage = new Image("file:roadSideviewExtendedTwiceSignTouchScreen.png");
//            backgroundImage = new Image("file:roadSideviewExtendedTwiceSignTouchScreen.png");
        }else{
            backgroundImage = new Image("file:drawingBackground.png");
        }
        if (fun){
    
            snowflakeImage = new Image("file:drawingSnowflake50.png");
        
        }
        
        Image truckImage;
        Image frontTruckImage;
                
        if (!fun){
            truckImage = new Image("file:truckSideNoWheels1Blue.png");
            frontTruckImage = new Image("file:truckSideNoWheels1Blue.png");
        }else{
            truckImage = new Image("file:drawingTruck.png");
            frontTruckImage = new Image("file:drawingTruck.png");
        }
        
        if (menuAtBottom){
            
            backgroundOffsetY = -250;

        }else{
            
            backgroundOffsetY = 0;
            
        }

        Image frontWheelImage = new Image("file:frontWheel1.png");
                
        backgroundImageView = new ImageView(backgroundImage);
        backgroundImageView.setX( -backgroundImage.getWidth()*(2/3.) );
        backgroundImageView.setY(backgroundOffsetY);
        
        TranslateTransition translateTransition = new TranslateTransition(Duration.millis(animationTime), backgroundImageView);
        translateTransition.setFromX( 0 );
        translateTransition.setToX( backgroundImageView.getBoundsInLocal().getWidth()*(2./3) );
        translateTransition.setCycleCount(Timeline.INDEFINITE);
        translateTransition.setAutoReverse(false);
        translateTransition.setInterpolator(Interpolator.LINEAR);
        
        translateTransition.play();
        
        
        truckImageView = new ImageView(truckImage);
        frontWheelImageView  = new ImageView(frontWheelImage);
        backWheelImageView  = new ImageView(frontWheelImage);
        frontTruckBackWheelImageView  = new ImageView(frontWheelImage);
        
        double truckYPosition;
        
        if (menuAtBottom){
            
            truckYPosition = 800;
            
        }else{
            
            truckYPosition = 700;
            
        }
        
        truckImageView.setPreserveRatio(true);
        truckImageView.setFitWidth(truckLengthPixels);
        
        
        newXPosition = truckLengthPixels;
        truckImageView.setX(newXPosition);
        truckImageView.setY(truckYPosition + 1.2*backgroundOffsetY);
        
        frontWheelImageView.setPreserveRatio(true);
        frontWheelImageView.setFitWidth(70);
        frontWheelImageView.setX(newXPosition + frontWheelOffsetX);
        frontWheelImageView.setY(truckYPosition + frontWheelOffsetY + 1.2*backgroundOffsetY);
        
        backWheelImageView.setPreserveRatio(true);
        backWheelImageView.setFitWidth(70);
        backWheelImageView.setX(newXPosition + backWheelOffsetX);
        backWheelImageView.setY(truckYPosition + backWheelOffsetY + 1.2*backgroundOffsetY);
        
        frontTruckBackWheelImageView.setPreserveRatio(true);
        frontTruckBackWheelImageView.setFitWidth(70);
        frontTruckBackWheelImageView.setX(-200 + frontTruckBackWheelOffsetX);
        frontTruckBackWheelImageView.setY(truckYPosition + frontTruckBackWheelOffsetY + 1.2*backgroundOffsetY);
        
        
        slideTruckText = new Text(newXPosition + 200, truckYPosition + 85 + 1.2*backgroundOffsetY, slideTextEN);
        
        if (languageIsSwedish){
            
            slideTruckText.setText(slideTextSV);
            
        }
        
        slideTruckText.setFont( new Font( fontString , 50) );
        slideTruckText.setFill(Color.WHITE);
        
        
        truckGroup.getChildren().add(truckImageView);
        truckGroup.getChildren().add(frontWheelImageView);
        truckGroup.getChildren().add(backWheelImageView);
        truckGroup.getChildren().add(slideTruckText);
        
        
        
        dragAnchor = new Point2D(newXPosition, truckYPosition);
        
        frontTruckImageView = new ImageView(frontTruckImage);
        
        frontTruckGroup.getChildren().add(frontTruckImageView);
        frontTruckGroup.getChildren().add(frontTruckBackWheelImageView);
        
        
        frontTruckImageView.setPreserveRatio(true);
        frontTruckImageView.setFitWidth(truckLengthPixels);
        frontTruckImageView.setX(-200);
        frontTruckImageView.setY(truckYPosition + 1.2*backgroundOffsetY);
        
        
    }
    
    private void initializeGameMenu(){
        
        Rectangle blackRectangle = new Rectangle();
        fuelSavingsText = new Text();
        fuelSavingsNumberText = new Text();
        infoText = new Text();
        infoTextNumber = new Text();
        safeText = new Text();
        
        fuelGroup = new Group();
        
        fuelGaugeImageView = new ImageView( new Image("file:fuelBar.jpg") );
        fuelNeedleImageView = new ImageView( new Image("file:fuelNeedleCenter3.png") );
        fuelGroup = new Group();
        
        if (noFuelGains){
            
            fuelGroup.getChildren().addAll(blackRectangle, fuelSavingsText, fuelSavingsNumberText, infoText, infoTextNumber, safeText);     
                        
        }else{
        
            fuelGroup.getChildren().addAll(blackRectangle, fuelGaugeImageView, fuelNeedleImageView, fuelSavingsText, fuelSavingsNumberText, infoText, infoTextNumber, safeText);     
            
        }
           
        
        double menuHeight = 300;
        
        
        double fuelGaugeX;
        double fuelGaugeY;
        double fuelGaugeHeight;
        double fuelNeedleOffsetY = -10;
        double fuelNeedleWidth = 400;
        
        blackRectangle.setX(0);
        blackRectangle.setOpacity(0.);
        
        double fuelTextOffsetX;
        double fuelTextOffsetY;
        
        double infoTextOffsetX;
        double infoTextOffsetY;
        
        double safeTextOffsetX;
        double safeTextOffsetY;
        
        
        if (noFuelGains){
            
            fuelGaugeX = screenWidth - 500;
            
        }else{
        
            fuelGaugeX = screenWidth - 400;
            
        }
        
        
        if (menuAtBottom){
            
            fuelGaugeHeight = - backgroundOffsetY/1.2;
            blackRectangle.setY(screenHeight - fuelGaugeHeight);
            fuelGaugeY = screenHeight - fuelGaugeHeight - 30;
            
//            fuelTextOffsetX = -325;
            fuelTextOffsetX = screenWidth*0.95;
            fuelTextOffsetY = -backgroundOffsetY*(1/4.) + 25;
            
            fuelSavingsText.setX( fuelGaugeX + fuelTextOffsetX );
            fuelSavingsText.setY( fuelGaugeY + fuelTextOffsetY );
            
            infoTextOffsetX = -screenWidth*(7./10);
            infoTextOffsetY = -backgroundOffsetY*(1/4.) + 25;

            infoText.setLayoutX( 0.1*screenWidth );
            infoText.setY( fuelGaugeY + infoTextOffsetY );
            
            safeTextOffsetX = -screenWidth*(2.3/5);
            safeTextOffsetY = -backgroundOffsetY*(13/20.);
            
        }else{
            
            fuelGaugeHeight = 250/1.2;
            blackRectangle.setY(0);
            fuelGaugeY =  + 30;
            
            fuelTextOffsetX = screenWidth*0.75;
            fuelTextOffsetY = 250*(1/4.) + 25;
            
            fuelSavingsText.setX( fuelTextOffsetX );
            fuelSavingsText.setY( fuelGaugeY + fuelTextOffsetY );
            
            infoText.setLayoutX( 0.075*screenWidth );
            infoTextOffsetY = 250*(1/4.) + 25;

//            infoText.setX( fuelGaugeX + infoTextOffsetX );
            infoText.setY( fuelGaugeY + infoTextOffsetY );
            
            safeTextOffsetX = -screenWidth*(2.3/5);
            safeTextOffsetY = 250*(13/20.);
            
        }
                
        
        
        
        blackRectangle.setWidth(screenWidth);
        blackRectangle.setHeight(menuHeight);
        
        fuelGaugeImageView.setPreserveRatio(true);
        fuelGaugeImageView.setFitHeight(fuelGaugeHeight);
        fuelGaugeImageView.setX(fuelGaugeX);
        fuelGaugeImageView.setY(fuelGaugeY);
                                
        fuelNeedleImageView.setPreserveRatio(true);
        fuelNeedleImageView.setFitWidth(fuelNeedleWidth);
//        fuelNeedleImageView.setX(fuelGaugeX + fuelGaugeImageView.getFitWidth()/2 - fuelNeedleImageView.getFitWidth()/2 );
        fuelNeedleImageView.setX(fuelGaugeX + fuelGaugeImageView.getBoundsInParent().getWidth()/2 - fuelNeedleImageView.getBoundsInParent().getWidth()/2  );
//        double fuelNeedleOffsetY = fuelNeedleImageView.getFitWidth()/2;
        fuelNeedleImageView.setY(fuelGaugeY  + fuelNeedleOffsetY - fuelNeedleImageView.getFitHeight()/2 );
        
//        double fuelTextOffsetX = -325;
//        double fuelTextOffsetY = -backgroundOffsetY*(1/4.) + 25;
        
        fuelSavingsText.setText("");
//        fuelSavingsText.setX( fuelGaugeX + fuelTextOffsetX );
//        fuelSavingsText.setY( fuelGaugeY + fuelTextOffsetY );
        fuelSavingsText.setFont(new Font( fontString , 50));
        fuelSavingsText.setFill(Color.WHITE);
        
        if (languageIsSwedish){
            fuelSavingsText.setText("Bränslebesparing:" );
        }else{
            fuelSavingsText.setText( "Fuel Saving:" );
        }
        
        double fuelTextNumberOffsetX = screenWidth*0.75;
        double fuelTextNumberOffsetY = fuelTextOffsetY + 100;
        
        fuelSavingsNumberText.setText("Rui");
        fuelSavingsNumberText.setFont(new Font( fontString , 80));
        fuelSavingsNumberText.setFill(Color.WHITE);
        fuelSavingsNumberText.setLayoutX( fuelTextNumberOffsetX + fuelSavingsText.getBoundsInLocal().getWidth()/2. - fuelSavingsNumberText.getBoundsInLocal().getWidth()/2.);
        System.out.println("fuelTextNumberOffsetX = " + fuelTextNumberOffsetX);
        System.out.println("fuelSavingsText.getBoundsInLocal().getWidth() = " + fuelSavingsText.getBoundsInLocal().getWidth());
        System.out.println("fuelSavingsNumberText.getBoundsInLocal().getWidth()/2. = " + fuelSavingsNumberText.getBoundsInLocal().getWidth()/2.);
        fuelSavingsNumberText.setY( fuelGaugeY + fuelTextNumberOffsetY );
        
        
        infoText.setFont(new Font( fontString , 50));
        infoText.setFill(Color.WHITE);
        
//        double infoTextNumberOffsetX = infoTextOffsetX + 20;
        double infoTextNumberOffsetY = infoTextOffsetY + 100;
        
//        ("Avstånd")
//        ("Distance")
        
        if (languageIsSwedish){
        
            infoTextNumber.setText("Avstånd");
        
        }else{
            
            infoTextNumber.setText("Distance");
            
        }
        
//        infoTextNumber.setX( fuelGaugeX + infoTextNumberOffsetX );
        infoTextNumber.setY( fuelGaugeY + infoTextNumberOffsetY );
        infoTextNumber.setFont(new Font( fontString , 70));
        infoTextNumber.setFill(Color.WHITE);
        
        
        if (languageIsSwedish){
        
            safeText.setText("  SÄKER  ");
                    
        }else{
       
            safeText.setText("  SAFE  ");
            
        }
        
//        safeText.setX( fuelGaugeX + safeTextOffsetX );
        safeText.setY( fuelGaugeY + safeTextOffsetY );
        safeText.setFont(new Font( fontString , 150));
        safeText.setFill(Color.GREEN);
        
    }
    
    private void setTruckRandomMovements(){
        
        TranslateTransition truckTranslateTransition = new TranslateTransition(Duration.millis(animationTime/4), truckGroup);
        truckTranslateTransition.setFromX( 0 );
        truckTranslateTransition.setToX( truckTransitionOffsetX );
        truckTranslateTransition.setCycleCount(Timeline.INDEFINITE);
        truckTranslateTransition.setAutoReverse(true);
//        truckTranslateTransition.setInterpolator(Interpolator.LINEAR);
        
        class TruckInterpolator extends Interpolator {
            @Override
            protected double curve(double t) {

                setInterpolatorTruck(t);
                return t;
            }
        }
        
        class FrontTruckInterpolator extends Interpolator {
            @Override
            protected double curve(double t) {

                setInterpolatorFrontTruck(t);
                return t;
            }
        }
        
//        AnimationBooleanInterpolator yInterp = new AnimationBooleanInterpolator();
        TruckInterpolator truckInterpolator = new TruckInterpolator();
        truckTranslateTransition.setInterpolator( truckInterpolator );
        
        truckTranslateTransition.play();
        

        
        TranslateTransition frontTruckTranslateTransition = new TranslateTransition(Duration.millis(animationTime/3), frontTruckGroup);
        frontTruckTranslateTransition.setFromX( 0 );
        frontTruckTranslateTransition.setToX( frontTruckTransitionOffsetX );
        frontTruckTranslateTransition.setCycleCount(Timeline.INDEFINITE);
        frontTruckTranslateTransition.setAutoReverse(true);
//        truckTranslateTransition.setInterpolator(Interpolator.LINEAR);
        
        FrontTruckInterpolator frontTruckInterpolator = new FrontTruckInterpolator();
        frontTruckTranslateTransition.setInterpolator( frontTruckInterpolator );
        
        frontTruckTranslateTransition.play();
        
        
        
        
        TranslateTransition truckTranslateTransitionVertical = new TranslateTransition(Duration.millis(animationTime/3), truckGroup);
        truckTranslateTransitionVertical.setFromY( 0 );
        truckTranslateTransitionVertical.setToY( truckTransitionOffsetY );
        truckTranslateTransitionVertical.setCycleCount(Timeline.INDEFINITE);
        truckTranslateTransitionVertical.setAutoReverse(true);
        
        truckTranslateTransitionVertical.play();
        
        TranslateTransition frontTruckTranslateTransitionVertical = new TranslateTransition(Duration.millis(animationTime/2), frontTruckGroup);
        frontTruckTranslateTransitionVertical.setFromY( 0 );
        frontTruckTranslateTransitionVertical.setToY( frontTruckTransitionOffsetY );
        frontTruckTranslateTransitionVertical.setCycleCount(Timeline.INDEFINITE);
        frontTruckTranslateTransitionVertical.setAutoReverse(true);
        
        frontTruckTranslateTransitionVertical.play();
        
    }
    
    private void initializeText(){
        
        double distance = newXPosition - ( -200 + truckLengthPixels);
        distance = distance * meterPerPixel;
        
        
    }
    
    static public void setInterpolatorFrontTruck(double fraction){
        
        endTime = System.nanoTime();

        duration = (endTime - startTime)/1000000;
        
        
        if (duration > inactivityTimeBeforeTextReappears){
            
            if ( !textShowing ){
                
                ftAppear.play();
                textShowing = true;
            
            }
            firstTime = true;
            
            
        }
        
        frontTruckFraction = fraction;
        updateText();
        
    }
    
    static public void setInterpolatorTruck(double fraction){
        
        truckFraction = fraction;
//        System.out.println("fraction: "+fraction);
        updateText();
        
    }
    
    public void createAirDragAnimation(ImageView argSnowflakeImageView, double offsetTime, double offsetY){
        
        Timeline tempAirDragAnimation = new Timeline();
//        airDragAnimation = new Timeline();
        
        double truckY = frontTruckImageView.getY() - 25;
        double frontTruckEndX = frontTruckImageView.getBoundsInLocal().getMaxX();
        
        double truckX = truckImageView.getX();
        truckX = truckX-50;
        double truckEndX = truckImageView.getBoundsInLocal().getMaxX();
               

        
        double startingX = -100;
        double endingX = screenWidth + 100;
        double stepX = endingX - startingX;
        
        double airDragResolution = 100;
        double airDragTime = 500;
        
        
        double timeOffset = (int) ( airDragTime*(offsetTime/numSnowFlakes) + .5 );
        timeOffset = timeOffset + 50.*rand.nextDouble();
//        double timeOffset = 0;
        
        double currentX = 0, currentY = 0, currentTime = 0;
        double prevY = 0;
        double truckImpactIncrementY = 25;
        double airFallingIncrementY = 10;
        
        double maxAirDragHole = 200 ;
        double airDragHole = 0.5*maxAirDragHole * ( ((truckX - frontTruckEndX)* meterPerPixel) /5.);
        double maxAirDragWidth = 800;

        
        double currentDistance = frontTruckImageView.getBoundsInLocal().getMaxX() - truckImageView.getBoundsInLocal().getMinX();
        currentDistance = -currentDistance * meterPerPixel/2.5;
//        System.out.println("currentDistance"+currentDistance);
        double currentOpacityRatio = 1.0;
        
//        System.out.println("CREATING ANIMATION FOR 1 SNOWFLAKE");
        
        double currentMinBeforeOurTruck = 0;
        
        double groundLimit;
                
        if (menuAtBottom){

            groundLimit = 650;

        }else{

            groundLimit = 910;

        }
        
        for ( int i = 0 ; i < airDragResolution ; i++ ){
            
            currentTime = airDragTime*(i/airDragResolution);
            currentX = (i/airDragResolution)*stepX + startingX;
            
            // I CAN REMOVE THIS IF IF I CREATE INSTEAD THREE LOOPS!!!!!!
            if ( currentX < frontTruckEndX ){
                
                // Other truck zone
                
                currentY = truckY;

                prevY = currentY;
                
                currentMinBeforeOurTruck = currentY;
                
            }else if ( currentX < frontTruckEndX + maxAirDragWidth && currentX < truckX ){

                
                // Linear fall
//                currentY = prevY + airFallingIncrementY;
                
                // Arctangent, forms an S
                double maxDistance = maxAirDragWidth;
                double distanceToFrontTruck = currentX - frontTruckEndX;
                double maxFallingHeight = 300;
                
                
                
                double angle = Math.PI * ( (distanceToFrontTruck - maxAirDragWidth/2.) / (maxAirDragWidth/2.) );
                
//                System.out.println("angle = " + angle);
                
                double yDisplacement = ( -Math.atan(angle) -  Math.atan(Math.PI) ) ;
                
//                System.out.println("yDisplacement = " + yDisplacement);
                
                currentY = truckY - maxFallingHeight*(yDisplacement/Math.PI);
                
                
//                System.out.println("displacement = " + displacement);
                
                prevY = currentY;                
                
                currentMinBeforeOurTruck = currentY;

                
            }else if ( currentX < truckX){
                
                // Remaining zone, before our truck
                
                currentY = prevY + airFallingIncrementY;
                
                if ( currentY > groundLimit){
                
                    currentY = groundLimit;
                    prevY = groundLimit;
                
                }
                
                currentMinBeforeOurTruck = currentY;
                
                
            }else if ( currentX < truckEndX){
                
                // Our truck zone
//                currentY = prevY - truckImpactIncrementY;
                
                
                // New logarithm airdrag
                double distanceFromAirStartToGround = groundLimit - currentMinBeforeOurTruck;
                
                
                double distanceInsideTruck = currentX - truckX;
                double scalingOfHeight = 50;
                double scalingOfWidth = 0.5;

                double upwardsDisplacement = scalingOfHeight*Math.log(scalingOfWidth*distanceInsideTruck);

                currentY = currentMinBeforeOurTruck - upwardsDisplacement;
                    
                if ( currentY < truckY - 0){
                
                    currentY = truckY - 0;
                
                }
                
                prevY = currentY;

                
            }else if ( currentX < truckEndX + maxAirDragWidth){ 
                
                // After our truck zone, in the max drag width

//                currentY = prevY + airFallingIncrementY;
                
                
                // Arctangent, forms an S
                double maxDistance = maxAirDragWidth;
                double distanceToBackTruck = currentX - truckEndX;
                double maxFallingHeight = 300;
                
                
                
                double angle = Math.PI * ( (distanceToBackTruck - maxAirDragWidth/2.) / (maxAirDragWidth/2.) );
                
//                System.out.println("angle = " + angle);
                
                double yDisplacement = ( -Math.atan(angle) -  Math.atan(Math.PI) ) ;
                
//                System.out.println("yDisplacement = " + yDisplacement);
                
                currentY = truckY - maxFallingHeight*(yDisplacement/Math.PI);
                
                
                
                prevY = currentY;
                
                
            }else{
                
                // Remaining
                currentY = prevY + airFallingIncrementY;
                
                
                if ( currentY > groundLimit){
                
                    prevY = groundLimit;
                
                }

            }
            
            
            KeyValue kvX = new KeyValue(argSnowflakeImageView.translateXProperty(), currentX );
            KeyValue kvY = new KeyValue(argSnowflakeImageView.translateYProperty(), currentY - 25 + offsetY );
            KeyValue kOpacity;
            
            if( currentX < 0 || currentX > screenWidth ){
            
                kOpacity = new KeyValue( argSnowflakeImageView.opacityProperty() , 0.0);
                
            }else{
                
                kOpacity = new KeyValue(argSnowflakeImageView.opacityProperty(), snowFlakeOpacity*currentOpacityRatio);

            }
            
            KeyFrame kf = new KeyFrame(Duration.millis( currentTime + timeOffset ), kvX);
            tempAirDragAnimation.getKeyFrames().add(kf);  
            kf = new KeyFrame(Duration.millis( currentTime + timeOffset ), kvY);
            tempAirDragAnimation.getKeyFrames().add(kf);  
            kf = new KeyFrame(Duration.millis( currentTime + timeOffset ), kOpacity);
            tempAirDragAnimation.getKeyFrames().add(kf);  

            
        }
                
        tempAirDragAnimation.setCycleCount(1);
        tempAirDragAnimation.setAutoReverse(false);
        
        tempAirDragAnimation.setOnFinished((ActionEvent event) -> {
            
            tempAirDragAnimation.pause();
            createAirDragAnimation(argSnowflakeImageView, offsetTime, offsetY);

            
       });
        
        
        tempAirDragAnimation.play();
        
        
    }
              
    public void createTurbulenceAnimation(ImageView argSnowflakeImageView, int index, int positionX, int positionY, double phaseOffset){

        double currentDistance = ( truckImageView.getBoundsInLocal().getMinX() - frontTruckImageView.getBoundsInLocal().getMaxX() )* meterPerPixel;
        
        double ratioShowing = currentDistance/5;
        if (ratioShowing > 1){
            
            ratioShowing = 1;
            
        }
            
        ratioShowing = 1 - ratioShowing; 
        
        double currentIndexFraction = ((double)index)/((double)numTurbulenceSnowFlakes);
        
        if ( currentIndexFraction > ratioShowing){
            
            argSnowflakeImageView.setOpacity(snowFlakeOpacity);
            
        }else{
            
            argSnowflakeImageView.setOpacity(0.0);
            
        }
        
        
        Timeline turbulenceAnimation = new Timeline();
       
        double truckX = truckImageView.getX() - 50;
        double truckY = truckImageView.getBoundsInLocal().getMinY() + truckImageView.getBoundsInLocal().getHeight()/2. ;
        
        double turbulenceWidth = 75;
        double turbulenceHeight = frontTruckImageView.getBoundsInLocal().getHeight();
        double turbulenceRadius = 15;
        
        double turbulenceResolution = 50;
        double turbulenceTime = 500;
        
        
        
        double currentTime;
        double currentX;
        double currentY;
        
        
        for ( int i = 0 ; i < turbulenceResolution ; i++ ){
            
            
            currentTime = turbulenceTime*(i/turbulenceResolution);
            currentX = truckX + positionX + turbulenceRadius*Math.cos( 2*Math.PI*(currentTime/turbulenceTime + phaseOffset) );
            currentY = truckY + positionY + turbulenceRadius*Math.sin( 2*Math.PI*(currentTime/turbulenceTime + phaseOffset) );
            
            
            KeyValue kvX = new KeyValue(argSnowflakeImageView.translateXProperty(), currentX );
            KeyValue kvY = new KeyValue(argSnowflakeImageView.translateYProperty(), currentY );
            
            KeyFrame kf = new KeyFrame(Duration.millis( currentTime  ), kvX);
            turbulenceAnimation.getKeyFrames().add(kf);  
            kf = new KeyFrame(Duration.millis( currentTime ), kvY);
            turbulenceAnimation.getKeyFrames().add(kf);  
                        
            
            
        }
        
        
        
        turbulenceAnimation.setCycleCount(1);
        turbulenceAnimation.setAutoReverse(false);
        
        turbulenceAnimation.setOnFinished((ActionEvent event) -> {
            
            turbulenceAnimation.pause();
            createTurbulenceAnimation(argSnowflakeImageView, index, positionX, positionY, phaseOffset);
            
            
       });
        
        
        turbulenceAnimation.play();
        
        
    }
    
    static public void updateText(){
        
        double distance;
        distance = ( newXPosition + truckFraction*truckTransitionOffsetX ) - ( -200 + truckLengthPixels + frontTruckFraction*frontTruckTransitionOffsetX);
        distance = distance * meterPerPixel;

        
        double backTruckForwardX = ( newXPosition + truckFraction*truckTransitionOffsetX );
        double frontTruckBackwardX = ( -200 + truckLengthPixels + frontTruckFraction*frontTruckTransitionOffsetX);
        distance = backTruckForwardX - frontTruckBackwardX;
        distance = distance * meterPerPixel;

        
        if (languageIsSwedish){
        
            infoText.setText("Avstånd till lastbil:" );
        
        }else{
            
            infoText.setText("Distance to truck:" );
            
        }
        
        
        double maxDistance = 7.5;
        
        double redR = 255;
        double redG = 0;
        double redB = 0;
        
        double yellowR = 255;
        double yellowG = 255;
        double yellowB = 0;
        
        double greenR = 0;
        double greenG = 255;
        double greenB = 0;
        
        // from YELLOW TO GREEN
        double y2gR = greenR - yellowR;
        double y2gG = greenG - yellowG;
        double y2gB = greenB - yellowB;
        
        // from RED TO YELLOW
        double r2yR = yellowR - redR;
        double r2yG = yellowG - redG;
        double r2yB = yellowB - redB;
        
        double currentR = 0;
        double currentG = 0;
        double currentB = 0;
        
        if ( distance < maxDistance ){
                 
            currentR = redR + (distance/maxDistance)*r2yR;
            currentG = redG + (distance/maxDistance)*r2yG;
            currentB = redB + (distance/maxDistance)*r2yB;
            
        }else{
                        
            currentR = yellowR + ((distance-maxDistance)/maxDistance)*y2gR;
            currentG = yellowG + ((distance-maxDistance)/maxDistance)*y2gG;
            currentB = yellowB + ((distance-maxDistance)/maxDistance)*y2gB;
            
        }
        
        if ( distance < 0 ){
            
            currentR = redR;
            currentG = redG;
            currentB = redB;
                        
        }
        
        if ( distance > 2*maxDistance ){
            
            currentR = greenR;
            currentG = greenG;
            currentB = greenB;
                        
        }
        
        
        
        if ( distance > 0 ){
            
//            infoText.setFill(Color.WHITE);
//            safeText.setText("  SAFE  ");
            
            if (languageIsSwedish){
        
                safeText.setText("  SÄKER  ");
        
            }else{

                safeText.setText("  SAFE  ");

            }
            
            if ( distance < 5 ){
            
                if (languageIsSwedish){

                    safeText.setText("  FARA   ");
                }else{

                    safeText.setText(" DANGER ");

                }

            }
            
            infoTextNumber.setText(dfMeters.format(distance) + " metres");
            
        }else{
            
//            infoText.setFill(Color.RED);
//            safeText.setText(" CRASH  ");
            
            if (languageIsSwedish){
        
                safeText.setText(" KRASCH  ");
        
            }else{

                safeText.setText(" CRASH  ");

            }
            
            infoTextNumber.setText(dfMeters.format(0) + " metres");
            
        }
                
        currentR = currentR/255.;
        currentG = currentG/255.;
        currentB = currentB/255.;
        
        
        
        safeText.setFill(new Color(currentR, currentG, currentB, 1.0) );
        
        updateFuel(distance);
        
        safeText.setLayoutX( screenWidth/2. - safeText.getBoundsInParent().getWidth()/2. );
        
        infoTextNumber.setLayoutX( infoText.getBoundsInParent().getMinX() + infoText.getBoundsInParent().getWidth()/2.  - infoTextNumber.getBoundsInParent().getWidth()/2. );
        
        
//        System.out.println("newXPosition: "+newXPosition+" distance: "+distance);
    }
    
    static public void updateFuel(double distance){
        
        if (distance < 0){
            
            distance = 0;
        
        }
        
        double savings = (1./6.5) * Math.exp( (15.-distance)/3. );
        
        double degOffset = -125;
        double maxAngle = 130;
        double currentAngle = (savings/25)*maxAngle;
        
        if ( currentAngle > 120 ){
            
            currentAngle = 120;
            
        }
        
        if ( currentAngle < 14 ){
            
            currentAngle = 14;
            
        }
        
        fuelNeedleImageView.setRotate( degOffset + currentAngle );
        
        
        
//        fuelSavingsText.setText( "Fuel Saving:" );
        
        if (languageIsSwedish){
        
            fuelSavingsText.setText("Bränslebesparing:" );

        }else{

            fuelSavingsText.setText( "Fuel Saving:" );

        }
        
        
        
        fuelSavingsNumberText.setText( dfFuelSavings.format(savings)+"%" );
        
                
    }

    private void setUpReplayButton() {
        replayLogo.setLayoutX(mediaView.getBoundsInParent().getMinX()+35);
        replayLogo.setLayoutY(mediaView.getBoundsInParent().getMaxY()-100);
        
        replayLogo.setOnMousePressed((MouseEvent me)->
        {
            System.out.println("Restart button pressed");
                        
            showVideo();
            
        });

        RotateTransition rt = new RotateTransition(Duration.millis(3000), replayLogo);
        rt.setByAngle(-360);
        rt.setCycleCount(Animation.INDEFINITE);
        rt.setAutoReverse(false);
        
        rt.play();
        
        
    }

    public static void showVideo()
    {
        moviePane.setOpacity(1.0);
        goToGameButton.setVisible(true);
        hbDemo.setVisible(false);
        hb.setVisible(true);
        replayLogo.setVisible(true);
        moviePane.toFront();
        truckFlags.toFront();
        hb.toFront();
        replayLogo.toFront();
        
        mediaPlayer.play();
        mediaPlayer.seek(Duration.millis(24000));
        subtitlesTimeline.jumpTo("start");
        restartingAnimation.jumpTo("start");
        restartingAnimation.play();
        
    }
    
    private void showDemo()
    {

        double zeroXPosition = truckLengthPixels;
        
        truckImageView.setX(zeroXPosition);
        frontWheelImageView.setX(zeroXPosition + frontWheelOffsetX);
        backWheelImageView.setX(zeroXPosition + backWheelOffsetX);
        slideTruckText.setX(zeroXPosition + 130);
        
        initX = zeroXPosition;
        newXPosition = zeroXPosition;
        
        updateText();
        
        
        replayLogo.setVisible(false);
        hb.setVisible(false);
        hbDemo.setVisible(true);
        goToGameButton.setVisible(false);
        
        mediaPlayer.stop();
        moviePane.setOpacity(0.);
        moviePane.toBack();
        mediaPlayer.seek(Duration.millis(24000));
        subtitlesTimeline.jumpTo("end");

    }
    
}
            
            
