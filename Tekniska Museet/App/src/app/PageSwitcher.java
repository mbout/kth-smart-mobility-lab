/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app;

import java.util.HashMap;
import javafx.beans.property.DoubleProperty;
import javafx.scene.Node;
import javafx.scene.layout.Pane;

/**
 *
 * @author Matteo
 */
public class PageSwitcher extends Pane
{
    private HashMap<String, Node> pages = new HashMap<>();
    
    public PageSwitcher()
    {
        super();
    }
    
    //Add the screen to the collection
    public void addPage(String name, Node page) {
        pages.put(name, page);
    }

    //Returns the Node with the appropriate name
    public Node getPage(String name) {
        return pages.get(name);
    }
    
    public boolean loadPage(String name, ControlledScreen cs)
    {
        try
        {
            cs.setScreenParent(this);
            addPage(name,cs.getPane());        
            return true;
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
            return false;
        }
    }
    
    public void setPage(String name)
    {
        final DoubleProperty opacity = opacityProperty();
        
        if (!getChildren().isEmpty())
        {
            getChildren().remove(0);                 //remove the displayed screen
            getChildren().add(0, pages.get(name));     //add the screen
            if (name.equals("videoMenu"))
            {
                WhatIsPlatooning.showVideo();
            }
//            Timeline fade = new Timeline(
//                    new KeyFrame(Duration.ZERO, new KeyValue(opacity, 1.0)),
//                    new KeyFrame(new Duration(1000), new EventHandler<ActionEvent>() {
//                @Override
//                public void handle(ActionEvent t) {
//                    getChildren().remove(0);                 //remove the displayed screen
//                    getChildren().add(0, pages.get(name));     //add the screen
//                    Timeline fadeIn = new Timeline(
//                            new KeyFrame(Duration.ZERO, new KeyValue(opacity, 0.0)),
//                            new KeyFrame(new Duration(800), new KeyValue(opacity, 1.0)));
//                    fadeIn.play();
//                }
//            }, new KeyValue(opacity, 0.0)));
//            fade.play();
        }
        else
        {
//            setOpacity(0.0);
            getChildren().add(pages.get(name));       //no one else been displayed, then just show
//            Timeline fadeIn = new Timeline(
//                    new KeyFrame(Duration.ZERO, new KeyValue(opacity, 0.0)),
//                    new KeyFrame(new Duration(2500), new KeyValue(opacity, 1.0)));
//            fadeIn.play();
        }
        
        
    }
    
}
