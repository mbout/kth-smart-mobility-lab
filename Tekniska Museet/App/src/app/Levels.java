
/**
 *
 * @author Matteo
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app;

import javafx.scene.Group;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author rui
 */
public class Levels
{
    GameManager gameManager;
    TutorialLevelManager tutorialLevelManager;
    
//    Levels(){
//        
//    }
    
    
    
    
    public Group getLevel(int level){
                
        Group root = new Group();
        
        gameManager = new GameManager(root, level);
       
        return root;
        
    }
    
    public Group getTutorialLevel(){
                
        Group root = new Group();
        
        tutorialLevelManager = new TutorialLevelManager(root);
       
        return root;
        
    }
    
    public void resetLevel(){
        
        if ( gameManager != null ){
        
            gameManager.reset();
            
        }else{
            
            boolean startOver = true;
        
            tutorialLevelManager.reset(startOver);
            
        }
        
    }
    
    public void playButtonPressedLevel(){
        
        if ( gameManager != null ){
        
            gameManager.playButtonPressed();
            
        }else{
            
            boolean startOver = true;
        
            tutorialLevelManager.playButtonPressed();
            
        }
        
    }
    
    public void setLayoutOffsets(double offsetX, double offsetY){
        
        if ( gameManager != null ){
        
            gameManager.setLayoutOffset(offsetX, offsetY);
            
        }else{
        
            tutorialLevelManager.setLayoutOffset(offsetX, offsetY);
            
        }
        
    }
    
    
}

