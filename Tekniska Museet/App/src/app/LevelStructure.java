/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author rui
 */
public class LevelStructure {
    
    final private int[][] map;
    
    final private int[] truck0Pos;
    final private int[] truck1Pos;
    
    final private int[] goal0Pos;
    final private int[] goal1Pos;
    
    final private int truck0Fuel;
    final private int truck1Fuel;
    
    final private int levelId;
    
    private List<Integer[]> truck0Packages, truck1Packages;
    private List<Integer[]> truck0Homes, truck1Homes;
    
    public LevelStructure(int[][] argMap, int[] argTruck0Pos, int[] argTruck1Pos, int[] argGoal0Pos, int[] argGoal1Pos, List<Integer[]> argTruck0Packages,
            List<Integer[]> argTruck1Packages, List<Integer[]> argTruck0Homes, List<Integer[]> argTruck1Homes, int argTruck0Fuel, int argTruck1Fuel, int argLevelId){
        
        levelId = argLevelId;
        
        map = new int[argMap.length][argMap[0].length];
        
        for ( int i = 0; i < argMap.length; i++ ){
            
            for ( int j = 0; j < argMap[i].length; j++ ){
            
                map[i][j] = argMap[i][j];
                
            }
//            System.arraycopy(map[i], 0, argMap[i], 0, argMap[i].length);
//            
//            System.out.println("Arrays.toString(map[i]) = " + Arrays.toString(map[i]));
//            System.out.println("Arrays.toString(argMap[i]) = " + Arrays.toString(argMap[i]));
            
        }
        
        truck0Pos = new int[argTruck0Pos.length];
        for ( int i = 0; i < argTruck0Pos.length; i++ ){
            truck0Pos[i] = argTruck0Pos[i];
        }
        
        truck1Pos = new int[argTruck0Pos.length];
        for ( int i = 0; i < argTruck1Pos.length; i++ ){
            truck1Pos[i] = argTruck1Pos[i];
        }
        
        goal0Pos = new int[argGoal0Pos.length];
        for ( int i = 0; i < argGoal0Pos.length; i++ ){
            goal0Pos[i] = argGoal0Pos[i];
        }
        
        goal1Pos = new int[argGoal1Pos.length];
        for ( int i = 0; i < argGoal1Pos.length; i++ ){
            goal1Pos[i] = argGoal1Pos[i];
        }
        
//        System.arraycopy(argTruck1Pos, 0, truck1Pos, 0, argTruck1Pos.length);
        
        truck0Packages = argTruck0Packages;
        truck0Packages = argTruck0Packages;
        
        truck0Homes = argTruck0Homes;
        truck1Homes = argTruck0Homes;
        
        truck0Fuel = argTruck0Fuel;
        truck1Fuel = argTruck1Fuel;
        
    }
    
    public int getTruckFuel(int index){
        
        if ( index == 0){
            return truck0Fuel;
        }else{
            return truck1Fuel;
        }
                
    }
    
    public int getLevelId(){
        
        return levelId;
        
    }
    
    public int[][] getMap(){
        
        int[][] returnMap = new int[map.length][map[0].length];
        
        for ( int i = 0; i < map.length; i++ ){
            
            for ( int j = 0; j < map[i].length; j++ ){
            
                returnMap[i][j] = map[i][j];
                
            }
                
        }        
        
        return returnMap;
        
    }
    
    public int[] getGoalPos(int index){
        
        int[] returnGoalPos = new int[goal0Pos.length];
        
        if ( index == 0){
            
            for ( int i = 0; i < goal0Pos.length; i++ ){
                returnGoalPos[i] = goal0Pos[i];
            }
//            System.arraycopy(returnTruckPos, 0, truck0Pos, 0, truck0Pos.length);      
            return returnGoalPos;
            
        }
        
        if ( index == 1){
        
            for ( int i = 0; i < goal1Pos.length; i++ ){
                returnGoalPos[i] = goal1Pos[i];
            }
//            System.arraycopy(returnTruckPos, 0, truck1Pos, 0, truck1Pos.length);      
            return returnGoalPos;
            
        }
        
        return null;
        
    }
    
    public int[] getTruckPos(int index){
        
        int[] returnTruckPos = new int[truck0Pos.length];
        
        if ( index == 0){
            
            for ( int i = 0; i < truck0Pos.length; i++ ){
                returnTruckPos[i] = truck0Pos[i];
            }
//            System.arraycopy(returnTruckPos, 0, truck0Pos, 0, truck0Pos.length);      
            return returnTruckPos;
            
        }
        
        if ( index == 1){
        
            for ( int i = 0; i < truck1Pos.length; i++ ){
                returnTruckPos[i] = truck1Pos[i];
            }
//            System.arraycopy(returnTruckPos, 0, truck1Pos, 0, truck1Pos.length);      
            return returnTruckPos;
            
        }
        
        return null;
        
    }
    
    public List<Integer[]> getTruckPackages(int index){
        
        if( index == 0 ){
            
            return truck0Packages;
            
        }
        
        if( index == 1 ){
            
            return truck1Packages;
            
        }
        
        return null;
        
    }
    
    public List<Integer[]> getTruckHomes(int index){
        
        if( index == 0 ){
            
            return truck0Homes;
            
        }
        
        if( index == 1 ){
            
            return truck1Homes;
            
        }
        
        return null;
        
    }
    
}
