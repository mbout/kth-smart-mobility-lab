/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.util.Duration;

/**
 *
 * @author Matteo
 */
public class videoMenu implements ControlledScreen
{
    private PageSwitcher myController;
    private Pane pane;
    
    public videoMenu()
    {
        pane = setPane();
    }

    @Override
    public void setScreenParent(PageSwitcher screenParent) {
        myController = screenParent;
    }

    @Override
    public PageSwitcher getController() {
        return myController;
    }

    @Override
    public Pane getPane() {
        return pane;
    }

    private Pane setPane() {
        
        Pane p = new Pane();
        WhatIsPlatooning wip = new WhatIsPlatooning();
        Group g = wip.requestGroup();
        
        Node bar = g.lookup("#buttonBar");
//        System.out.println("bar = " + bar);
        Group gameButton = (Group) bar.lookup("#gameButton");
//        System.out.println("gameButton = " + gameButton);
        gameButton.setOnMousePressed((me)->
        {
            myController.setPage("levelsMenu");
            System.out.println("pressed");
        });
        
        Node barDemo = g.lookup("#buttonBarDemo");
//        System.out.println("bar = " + bar);
        Group gameButtonDemo = (Group) barDemo.lookup("#gameButton");
//        System.out.println("gameButton = " + gameButton);
        gameButtonDemo.setOnMousePressed((me)->
        {
            myController.setPage("levelsMenu");
            System.out.println("pressed");
        });
        
        
        p.getChildren().addAll(g);
        return p;
    }
    
}
