/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package usingthewheel;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import javafx.animation.Interpolator;
import javafx.animation.KeyValue;
import javafx.animation.ParallelTransition;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextBoundsType;
import javafx.scene.transform.Rotate;
import javafx.util.Duration;

/**
 *
 * @author rui
 */
public class ScoreManager {
    
    double screenWidth = GlobalsUsingTheWheel.screenWidth;
    double screenHeight = GlobalsUsingTheWheel.screenHeight;
    String carPath1, carPath2, roadSignPath, bannerPath, moosePath;
    
    double initialTreeWidth = 50;
    double finalTreeWidth = 600;
    double treeMovementTimeSeconds = 5;
    
    Random randomGenerator;
    
    int[] laneHistory;
    int[] treeHistory;
    int extraSameTime;
    
    int totalDistance;
    
    double firstTime;
    
    double difficulty = 250.;
    
    double timeStep = 0.2;
    
    int differentLanesCounter = 0;
    int maxTimeNotPlatooning = 30;
    
    double fuelGaugeWidth = screenWidth/5.;
    
    Group fuelBar;
    
    Text scoreText;
    
    double maxFuel = 100;
    double fuelDecrement = 0.2;
    double currentFuel = maxFuel;
    double maxDecreaseAngle = 115;
    
    double lastTimeFuelCharged = -10;
    
    ImageView fuelGaugeImageView;
    Rectangle fuelRectangleBackground;
    
    int score;
    
    ScoreManager(){
        
        carPath1 = "file:backCar.png";
        carPath2 = "file:backCar.png";
        
        laneHistory =  new int[500];
        for ( int i = 0; i < laneHistory.length ; i++){
            laneHistory[i] = 1;
        }
        
        treeHistory =  new int[500];
        for ( int i = 0; i < treeHistory.length ; i++){
            treeHistory[i] = 1;
        }
        
        
        randomGenerator = new Random();
        
        extraSameTime = (int) (( difficulty*randomGenerator.nextFloat() )+0.5);
        
        totalDistance = 0;
        
        firstTime = -1;
        
    }
    
    public double getCurrentFuel(){
        
        return currentFuel;
        
    }
    
    public void updateFuel(double myTruckLane, double leadTruckLane, double passedTime){
        
        if ( Math.abs(myTruckLane - leadTruckLane) > .5){
            differentLanesCounter++;           
        }else{
            differentLanesCounter = 0;
        }
        
        if ( passedTime - lastTimeFuelCharged > 2.0){
        
            if (differentLanesCounter > maxTimeNotPlatooning){
                fuelRectangleBackground.setFill(Color.ORANGERED);        
                decreaseFuel();
            }else{
                fuelRectangleBackground.setFill(Color.BLACK);
            }
        
        }else{
            
            fuelRectangleBackground.setFill(Color.GREEN);
            
        }
        
        
        
    }
    
    private void decreaseFuel(){
        
        currentFuel = currentFuel - fuelDecrement;
                
        updateFuel( ((maxFuel - currentFuel)/maxFuel) *maxDecreaseAngle );
        
        if (currentFuel < fuelDecrement){
            fuelRectangleBackground.setFill(Color.RED);
        }
        
        
        
    }
    
    public Group createFuelGauge(){
        
        fuelBar = new Group();
        
        
        Image fuelGaugeImage = new Image("file:fuelBarTransparent.png") ;
//        fuelGaugeImageView = new ImageView( new Image("file:fuelBar.jpg") );
        fuelGaugeImageView = new ImageView( fuelGaugeImage );
        ImageView fuelNeedleImageView = new ImageView( new Image("file:fuelNeedleCenter3.png") );
        
        fuelGaugeImageView.setPreserveRatio(true);
        fuelGaugeImageView.setFitWidth(fuelGaugeWidth);
        
        fuelNeedleImageView.setPreserveRatio(true);
        fuelNeedleImageView.setFitWidth(fuelGaugeWidth);
        fuelNeedleImageView.setId("fuelNeedle");
        
        fuelRectangleBackground = new Rectangle(fuelGaugeWidth, 1.1*fuelGaugeImage.getHeight()*(fuelGaugeWidth/fuelGaugeImage.getWidth() ) );
        
        System.out.println("fuelGaugeImage.getWidth() = " + fuelGaugeImage.getWidth());
        System.out.println("fuelGaugeImage.getHeight() = " + fuelGaugeImage.getHeight());
        
        fuelRectangleBackground.setFill(Color.BLACK);
        
        fuelBar.getChildren().addAll(fuelRectangleBackground,fuelGaugeImageView, fuelNeedleImageView);
//        fuelBar.getChildren().addAll(fuelGaugeImageView, fuelNeedleImageView);
        
        updateFuel( ((maxFuel - currentFuel)/maxFuel) *maxDecreaseAngle );
        
        return fuelBar;
        
    }
    
    private void updateFuel(double decreasedAngle){
        
        ImageView tempImage = (ImageView) fuelBar.lookup("#fuelNeedle");
        
//        tempImage.getTransforms().add(new Rotate(30, 50, 30));
        tempImage.getTransforms().clear();        
        tempImage.getTransforms().add(new Rotate(-decreasedAngle, fuelGaugeWidth/2., fuelGaugeWidth/2.));
        
        
        
    }
    
    public void addExtraFuel(double currentTime){
        
//        ImageView tempImage = (ImageView) fuelBar.lookup("#fuelNeedle");
//        
////        tempImage.getTransforms().add(new Rotate(30, 50, 30));
//        tempImage.getTransforms().clear();        
//        tempImage.getTransforms().add(new Rotate(-decreasedAngle, fuelGaugeWidth/2., fuelGaugeWidth/2.));
        
        currentFuel = Math.min(currentFuel + 30,maxFuel);
                
        updateFuel( ((maxFuel - currentFuel)/maxFuel) *maxDecreaseAngle );
        
        if (currentFuel < fuelDecrement){
            
            fuelRectangleBackground.setFill(Color.GREEN);
        
        }
        
        lastTimeFuelCharged = currentTime;
        
//        ImageView tempImage = (ImageView) fuelBar.lookup("#fuelNeedle");
        
//        tempImage.getTransforms().clear();        
//        tempImage.getTransforms().add(new Rotate(- ((maxFuel - currentFuel)/maxFuel) *maxDecreaseAngle , fuelGaugeWidth/2., fuelGaugeWidth/2.));
        
//        updateFuel( );
        
    }
    
    public Group createScoreBoard(){
        
        Group scoreBoardGroup = new Group();
        
        scoreText = new Text();
        
        scoreText.setFont(new Font("OCR A Extended",50));
        scoreText.setFill(Color.YELLOW);
        scoreText.setBoundsType(TextBoundsType.VISUAL);
        
        scoreText.setText(totalDistance + " METERS");
        
//        scoreText.setLayoutX(screenWidth/2. - scoreText.getBoundsInLocal().getWidth()/2.);
//        scoreText.setLayoutY( scoreText.getBoundsInLocal().getHeight()*(3./2.) );
//        scoreText.setLayoutY( fuelGaugeWidth );
        
        double backgroundRectangleHeight = fuelGaugeWidth/4.;
        
        Rectangle backgroundRectangle = new Rectangle(fuelGaugeWidth, backgroundRectangleHeight);
        
        Image fuelGaugeImage = new Image("file:fuelBar.jpg");
        double ratio = fuelGaugeImage.getWidth()/fuelGaugeImage.getHeight();
        
        System.out.println("ratio = " + ratio);
        
        System.out.println("fuelBar.getBoundsInLocal().getWidth()/ratio = " + fuelBar.getBoundsInLocal().getWidth()/ratio);
        
        backgroundRectangle.setLayoutY( fuelBar.getBoundsInLocal().getWidth()/ratio );
        
        scoreText.setLayoutY( fuelGaugeImageView.getBoundsInLocal().getWidth()/ratio + backgroundRectangleHeight/2. + scoreText.getBoundsInLocal().getHeight()/2. );
        
        scoreText.setLayoutX( fuelGaugeImageView.getBoundsInLocal().getWidth()/2. - scoreText.getBoundsInLocal().getWidth()/2. );
        
        scoreBoardGroup.getChildren().addAll(backgroundRectangle, scoreText);
        
        return scoreBoardGroup;        
        
    }
     
    public void updateScoreBoard(double passedTime){
        
        if ( firstTime < 0){
            
            firstTime = passedTime;
            return;
            
        }
        
        double timeSinceStart = ( passedTime- firstTime )/1000000000.;
                
        totalDistance = (int) (timeSinceStart*20.);
        
        score = totalDistance;
        
//        scoreText.setText("DISTANCE: "+ totalDistance + " METERS");
        scoreText.setText((int)score + " METERS");
        
        scoreText.setLayoutX( fuelGaugeImageView.getBoundsInLocal().getWidth()/2. - scoreText.getBoundsInLocal().getWidth()/2. );
        
//        scoreText.setLayoutX( screenWidth/2. - scoreText.getBoundsInLocal().getWidth()/2.);
//        scoreText.setLayoutY( scoreText.getBoundsInLocal().getHeight()*(3./2.) );
        
        
    }
    
    public int getScore(){
        
        return score;
        
    }
        
}
