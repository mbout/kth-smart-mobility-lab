
import socket, sys, time, math, random, threading
import multiprocessing
import errno

# import xml.etree.ElementTree as ET


# sys.path.append('..\Lanelets')
# import laneletlibrary


from multiprocessing import Pool

sys.path.append('Lanelets')
import laneletmodule
import laneletlibrary

import bodyclasses, smartvehicle

class SimulatorClass:
	"This class implements the Vehicle Simulator Module to be running in the SML World"

	def __init__(self, bodies_array):

		self.construction_time = time.time()

		self.simulation_step_counter = 0

		self.bodies_array = bodies_array


	def simulate_body_readings(self, v2v_method, v2v_range, v2v_angle):

		try:

			current_body_ids = self.bodies_array.keys()
			# To avoid dictionary size change error

			for ego_body_id in current_body_ids:

				if ego_body_id in self.bodies_array:
				# To avoid dictionary size change error

					ego_body = self.bodies_array[ego_body_id]

				else:

					continue

				current_body_readings = []

				# If I'm a person I don't have sensor readings
				if isinstance( ego_body , bodyclasses.Person):

					continue

				for other_body_id in current_body_ids:

					if ego_body_id == other_body_id:

						continue

					if other_body_id in self.bodies_array:
					# To avoid dictionary size change error

						other_body = self.bodies_array[other_body_id]

					else:

						continue

					if isinstance(other_body, bodyclasses.Person):

						#print "Ignoring person in body readings!!!"
						continue

					# print "Checking " + str(ego_body_id) + " against " + str(other_body_id)

					if v2v_method != 'off':

						# print "v2v not off"

						if v2v_method == 'communications':

							other_body_reading = [ other_body.id, other_body.x, other_body.y, other_body.yaw, other_body.x_speed, other_body.y_speed ]

							# print "math.hypot( ego_body.x - other_body.x , ego_body.y - other_body.y ) = " + str(math.hypot( ego_body.x - other_body.x , ego_body.y - other_body.y ))
							# print "v2v_range = " + str(v2v_range)

							if math.hypot( ego_body.x - other_body.x , ego_body.y - other_body.y ) < v2v_range:
								# if 'trailer' in other_body:
								# 	if other_body['trailer']:			
								# 		other_body_reading.append('trailer')
								current_body_readings.append( other_body_reading )

						if v2v_method == 'sensors':

							other_body_reading = [ other_body.id, other_body.x, other_body.y, other_body.yaw, other_body.x_speed, other_body.y_speed ]

							if math.hypot( ego_body.x - other_body.x , ego_body.y - other_body.y ) < v2v_range:

								angle = math.degrees(math.atan2( other_body.y - ego_body.y , other_body.x - ego_body.x ))

								if abs(angle - ego_body.yaw) < v2v_angle/2.:
									# if 'trailer' in other_body:
									# 	if other_body['trailer']:
									# 		other_body_reading.append('trailer')
									current_body_readings.append( other_body_reading )

					else:

						other_body_reading = [ other_body.id, other_body.x, other_body.y, other_body.yaw, other_body.x_speed, other_body.y_speed ]
						current_body_readings.append( other_body_reading )
				# body_reading = {'id': body['id'] , 'readings' : current_body_readings}
				# bodies_readings.append(body_reading)
				ego_body.sensor_readings = current_body_readings

		except:

			print "simulatorclass.py simulate_body_readings() error"
			print "Tip: Maybe the dictionary of bodies_array changed during the loop"
			raise
			# print "Error_msg = " + str(msg)



	def simulate_vehicles(self, time_to_simulate):

		# Will assume that bodies come in the SML scale!
		# X, Y between -3 to 3 metres and angle in degrees
		# Will assume that input velocity comes as SML velocity!
		# Will assume that input steering comes as degrees!

		# print 'simulate_vehicles()'

		air_drag_coeff = 50.
		vehicle_mass = 1000.

		# Since dictionary might change while I'm going through it, I have to access it "indirectly"
		# First I get a copy of the current dictionary keys
		bodies_array_keys = self.bodies_array.keys()

		# I will go over each of these keys
		for body_id in bodies_array_keys:

			# If this key still exists in the dictionary then I'll work on the element
			if body_id in self.bodies_array:

				if self.bodies_array[body_id].simulated:
				# The body needs to be simulated

					if isinstance(self.bodies_array[body_id], bodyclasses.WheeledVehicle):
						# The body is a WheeledVehicle


						# self.bodies_array[body_id].x_speed = math.cos( math.radians( self.bodies_array[body_id].yaw ) )*self.bodies_array[body_id].commands['throttle']
						# self.bodies_array[body_id].y_speed = math.sin( math.radians( self.bodies_array[body_id].yaw ) )*self.bodies_array[body_id].commands['throttle']
						
						# self.bodies_array[body_id].x = self.bodies_array[body_id].x + self.bodies_array[body_id].x_speed*time_to_simulate
						# self.bodies_array[body_id].y = self.bodies_array[body_id].y + self.bodies_array[body_id].y_speed*time_to_simulate

						if isinstance(self.bodies_array[body_id], smartvehicle.SmartVehicle) or True:

							linear_speed = math.hypot(self.bodies_array[body_id].x_speed, self.bodies_array[body_id].y_speed)

							acceleration = ( self.bodies_array[body_id].commands['throttle'] - air_drag_coeff*linear_speed ) / vehicle_mass

						
							# print "self.bodies_array[body_id].commands['throttle'] = " + str(self.bodies_array[body_id].commands['throttle'])

							# print "\n"
							# print "self.bodies_array[body_id].commands['throttle'] = " + str(self.bodies_array[body_id].commands['throttle'])
							# print "linear_speed = " + str(linear_speed)

							linear_speed += acceleration*time_to_simulate

							# Here I am assuming that negative throttles correspond to braking, and as such cannot make the vehicle go backwards
							if self.bodies_array[body_id].commands['throttle'] < 0. and linear_speed < 0.:

								linear_speed = 0.

						else:

							linear_speed = self.bodies_array[body_id].commands['throttle']


						#print body_id, "has linear speed", linear_speed 
						self.bodies_array[body_id].x_speed = math.cos( math.radians( self.bodies_array[body_id].yaw ) )*linear_speed
						self.bodies_array[body_id].y_speed = math.sin( math.radians( self.bodies_array[body_id].yaw ) )*linear_speed
						
						self.bodies_array[body_id].x = self.bodies_array[body_id].x + self.bodies_array[body_id].x_speed*time_to_simulate
						self.bodies_array[body_id].y = self.bodies_array[body_id].y + self.bodies_array[body_id].y_speed*time_to_simulate

						# Differential unicycle model
						# self.bodies_array[body_id].yaw = self.bodies_array[body_id].yaw + self.bodies_array[body_id].commands['steering']*time_to_simulate
						# Simple car model

						self.bodies_array[body_id].yaw = self.bodies_array[body_id].yaw + math.degrees( (linear_speed/self.bodies_array[body_id].axles_distance)*math.tan(self.bodies_array[body_id].commands['steering'])*time_to_simulate )

						# Limit the yaw to the range between 0 and 2pi
						self.bodies_array[body_id].yaw = self.bodies_array[body_id].yaw%360 if self.bodies_array[body_id].yaw > 0 else self.bodies_array[body_id].yaw%-360




		return

	