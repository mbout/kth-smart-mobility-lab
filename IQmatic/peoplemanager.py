import time, math, random

import laneletlibrary
import bodyclasses

class peopleManager:
	"This class manages the persons that are in the SML World"

	def __init__(self, bodies_array, osm_info):

		self.creation_time = time.time()
		self.bodies_array = bodies_array

		self.osm_info = osm_info

		self.bus_osm_nodes = dict()
		# This dictionary will contain the OSM_nodes which correspond to
		# the bus stops. The keys of this dictionary are the string name
		# of the bus stop tag. e.g.: 'bus_stop_1'

		self.bus_stop_person_ids = dict()
		# The keys of this dictionary are the string name of the bus stop
		# tag. e.g.: 'bus_stop_1'. The corresponding entry of each key is 
		# a list which contains the id's of the persons waiting in tha bus
		# stop.

		self.get_bus_stop_nodes(self.osm_info)
		# This will initialize the above dictionaries

		self.number_people = 0
		# Keeps track of the current number of people in the SML World

		self.spawn_period = 1.
		# Ever spaw_period we will generate people

		

	def get_bus_stop_nodes(self, osm_info):
		""" Function running on class initialization. It simply gets the bus stops 
		defined in the map.

		Generates number_people persons on the bus stop bus_stop_string. The person/s
		will have a destination bus_stop_destination_number

		Args:
		self: peoplemanager class.
		osm_info: The osm_info class, which contains all of the road network information.

		Returns:
			Returns nothing it will simply perform the desired action and finish the function.

		"""

		self.bus_osm_nodes = dict()

		bus_stop_string = 'bus_stop_'
		bus_stop_number = 0

		bus_stops_to_process = True

		while True:

			current_bus_stop_string = bus_stop_string + str(bus_stop_number)

			bus_node_id = laneletlibrary.find_node_by_tag(self.osm_info.osm_node_dict, current_bus_stop_string)
			
			if bus_node_id == None:

				if not len( self.bus_osm_nodes ):
					# There are no bus stops, therefore there should also not be people
					raise NameError('Trying to create person manager on a non-bus map.')

				return

			bus_osm_node = laneletlibrary.get_osm_node_by_id(osm_info.osm_node_dict, bus_node_id)

			self.bus_osm_nodes[current_bus_stop_string] = bus_osm_node
			self.bus_stop_person_ids[current_bus_stop_string] = []

			bus_stop_number += 1




	def step(self):
		""" This function is running in a loop, being constantly called from world_class.py
		by function people_manager_handler()

		For now it is simply generating random people at a fixed rate.

		Args:
		self: peoplemanager class.
		
		Returns:
			Returns nothing it will simply perform the desired action and finish the function.

		"""

		current_elapsed_time = time.time() - self.creation_time
		# How long it has passed since this class isntance was created

		desired_number_people = int( math.floor( current_elapsed_time / self.spawn_period ) )
		# The number of people we want to have on the map right now

		self.manage_bus_boarding()
		# Make people get in and out of the bus

		while self.number_people < desired_number_people:

			# Lets get a random bus stop
			bus_stop_number = random.randrange( len(self.bus_osm_nodes) )
			# Get the tag string of this random bus stop
			bus_stop_string = self.bus_osm_nodes.keys()[bus_stop_number]
			
			# The destination bus stop will simply be the next bus stop
			bus_stop_destination_number = (bus_stop_number+1)%len(self.bus_osm_nodes)
			# Get the tag string of this bus stop
			bus_stop_destination_string = self.bus_osm_nodes.keys()[bus_stop_destination_number]
			
			# Lets generate two persons in the bus stop bus_stop_string with a destination bus_stop_destination_string
			self.generate_people_on_bus_stop(bus_stop_string, bus_stop_destination_string, number_people = 2)

		

	def manage_bus_boarding(self):
		""" Manages boarding and unboarding of people on the buses that are stopped at
		bus stops.

		It will simply check if there are buses stoped in bus stops and act accordingly.

		Args:
		self: peoplemanager class.
		
		Returns:
			Returns nothing it will simply perform the desired action and finish the function.

		"""

		persons_to_remove_from_bodies_array = []
		# A list to keep track of which persons we need to erase from the World

		for body_id in self.bodies_array:
			# Lets try to find our bus

			if isinstance( self.bodies_array[body_id] , bodyclasses.BusVehicle ):
				# We found the bus!

				bus_vehicle = self.bodies_array[body_id]

				distance_to_next_stop = bus_vehicle.distance_to_next_stop
				# The distance to the next bus stop

				current_bus_cruise_velocity_m_s = bus_vehicle.cruise_velocity*(1000./3600.)
				# The current cruise velocity in m_s

				estimated_time_of_arrival = distance_to_next_stop/current_bus_cruise_velocity_m_s
				# The estimated time of arrival to next bus stop can be computed.
				# This is an optimistic estimate (underestimate) since the bus actually 
				# slows down in curves

				people_on_board = bus_vehicle.people_on_board
				# The number of people on board of the bus

				# If bus_vehicle.current_bus_stop is different than None, then the bus is stopped
				# in a bus stop
				if bus_vehicle.current_bus_stop:

					# Let everyone get out
					persons_that_left_the_bus = self.make_people_leave_bus( bus_vehicle )
					# The persons that left the bus need to be removed from the bodies_array
					# later. i.e.: erased from the World.
					persons_to_remove_from_bodies_array.extend( persons_that_left_the_bus )
					
					# Let everyone get in
					self.make_people_enter_bus(bus_vehicle)
					
					# If the bus is stopped, this function will make it start moving
					self.make_bus_start_moving(bus_vehicle)

					# You can also set the cruise velocity, in order to arrive faster or slower
					# at the next stop (using the estimated time of arrival)
					self.set_bus_cruise_velocity(bus_vehicle, 20.)
					
		# This function will simply remove from memory the people that left the bus 
		self.remove_persons_from_world(persons_to_remove_from_bodies_array)
		

	def make_people_leave_bus(self, bus_vehicle):
		""" Manages unboarding of people on bus_vehicle.

		It will simply go over all the people on board of the bus, and make them leave the bus 
		in case the current bus stop is their desired destination.

		Args:
		self: peoplemanager class.
		bus_vehicle: the instance of bodyclasses.BusVehicle corresponding to our vehicle
		
		Returns:
			Returns a list with the ids of the persons that left the bus

		"""

		ids_to_exit_bus = []
		persons_to_remove_from_bodies_array = []

		for person_id_on_board in bus_vehicle.ids_on_board:
			# Lets go over all the people on board

			# Does this person wish to exit here?
			if self.bodies_array[person_id_on_board].bus_stop_destination_string == bus_vehicle.current_bus_stop:

				# Add person to removal list
				ids_to_exit_bus.append(person_id_on_board)

				# Add person to removal list
				persons_to_remove_from_bodies_array.append(person_id_on_board)

		# Remove the persons from the bus
		for id_person_to_exit in ids_to_exit_bus:

			bus_vehicle.ids_on_board.remove( id_person_to_exit )
			bus_vehicle.people_on_board -= 1

		return persons_to_remove_from_bodies_array

	def make_people_enter_bus(self, bus_vehicle):
		""" Manages boarding of people on bus_vehicle.

		It will simply go over all the people on the bus stop where the bus_vehicle 
		is currently stopped, and put them inside the bus_vehicle in case the bus_vehicle
		has capacity for them

		Args:
		self: peoplemanager class.
		bus_vehicle: the instance of bodyclasses.BusVehicle corresponding to our vehicle
		
		Returns:
			Returns nothing

		"""

		# We must check if the bus can still hold people
		while bus_vehicle.people_capacity - bus_vehicle.people_on_board > 0:

			# Is there still people in the bus stop?
			if self.bus_stop_person_ids[bus_vehicle.current_bus_stop]:

				# Get the id of a person that wants to board, and remove this person from the list
				person_boarding_id = self.bus_stop_person_ids[bus_vehicle.current_bus_stop].pop(0)
				bus_vehicle.ids_on_board.append(person_boarding_id)
				bus_vehicle.people_on_board += 1

				# Lets just take the person off the screen for now
				self.bodies_array[person_boarding_id].x = 1000.

			else:
				# Bus stop is empty, lets finish the loop
				break

	def make_bus_start_moving(self, bus_vehicle):
		""" Makes the bus start moving, if it is stopped at a bus stop.

		Args:
		self: peoplemanager class.
		bus_vehicle: the instance of bodyclasses.BusVehicle corresponding to our vehicle
		
		Returns:
			Returns nothing

		"""

		bus_vehicle.current_bus_stop = None

	def set_bus_cruise_velocity(self, bus_vehicle, desired_velocity_km_h):
		""" Sets a new cruise velocity for the bus.

		Args:
		self: peoplemanager class.
		bus_vehicle: the instance of bodyclasses.BusVehicle corresponding to our vehicle
		desired_velocity_km_h: the desired cruise velocity (in km/h)
		
		Returns:
			Returns nothing

		"""

		bus_vehicle.cruise_velocity = desired_velocity_km_h

	def remove_persons_from_world(self, persons_to_remove_from_bodies_array):
		""" It will remove the persons given by persons_to_remove_from_bodies_array 
		from the World.

		Args:
		self: peoplemanager class.
		persons_to_remove_from_bodies_array: a list with the ids of the persons 
		to remove from the World
		
		Returns:
			Returns nothing

		"""

		for person_to_remove_from_world in persons_to_remove_from_bodies_array:

			self.bodies_array.pop(person_to_remove_from_world)

	def generate_people_on_bus_stop(self, bus_stop_string, bus_stop_destination_number, number_people = 1):
		""" Generates people on a bus stop

		Generates number_people persons on the bus stop bus_stop_string. The person/s
		will have a destination bus_stop_destination_number

		Args:
		self: peoplemanager class.
		bus_stop_string: A string corresponding to the tag of the desired
			bus stop where we want the person/s to be created.
		bus_stop_destination_number: A string corresponding to the tag of 
			the desired	bus stop where the person/s to leave the bus.
		number_people: Optional variable, which sets the number of persons 
			to be created.

		Returns:
			Returns False in case of error, otherwise it will simply perform 
			the desired action and finish the function.

		"""

		if not bus_stop_string in self.bus_osm_nodes:

			print "Error in peoplemanager.py generate_people_on_bus_stop(), trying to create people on a non existing bus_stop_string"

			return False

		bus_stop_generation_node = self.bus_osm_nodes[bus_stop_string]

		bus_stop_location = [bus_stop_generation_node.x, bus_stop_generation_node.y]

		for idx in xrange(number_people):

			new_id = min( self.bodies_array.keys() ) - 1

			new_person = bodyclasses.Person()

			# For now persons will be placed randomly around the bus stop
			displacement_factor = 5.
			random_displacement = [displacement_factor*( -0.5+random.random() ), displacement_factor*( -0.5+random.random() )]

			new_person.x = bus_stop_location[0] + random_displacement[0]
			new_person.y = bus_stop_location[1] + random_displacement[1]
			new_person.z = 0.
			new_person.yaw = 0.
			new_person.pitch = 0.
			new_person.roll = 0.

			new_person.bus_stop_destination_string = bus_stop_destination_number

			self.bodies_array[new_id] = new_person

			self.bus_stop_person_ids[bus_stop_string].append(new_id)

			self.number_people += 1