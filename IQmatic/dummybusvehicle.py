import socket, sys, time, math, random

sys.path.append('Lanelets')
import laneletmodule
import laneletlibrary

import dummyvehicle
import bodyclasses
import numpy

class DummyBusVehicle(dummyvehicle.DummyVehicle):
	"This class is the Vehicle class for the Vehicle Simulator Module"
	def __init__(self, bodies_array, simulation_period, vehicle_id):

		dummyvehicle.DummyVehicle.__init__(self, bodies_array, simulation_period, vehicle_id)

		self.bus_traj_1 = []
		self.bus_traj_2 = []
		self.bus_traj_3 = []

		self.bus_stop_waiting = False

		# The time, in seconds, that the bus will be stopped at each stop
		self.bus_stop_waiting_time = 5.0

		self.cruise_velocity = 0.
		
	def get_trajectory_tracking_inputs_old(self, current_time):
		# This is the one being used now!
		self.vehicle_iteration += 1

		self.state = [ self.bodies_array[self.id].x, self.bodies_array[self.id].y, math.radians( self.bodies_array[self.id].yaw ) ]
		self.sensor_readings = self.bodies_array[self.id].sensor_readings

		# self.set_desired_velocity( 40.0 )
		self.update_my_desired_velocity()

		current_idx = self.current_trajectory_id

		number_points_ahead = 25

		[best_distance, best_idx] = self.find_closest_trajectory_point( self.state, current_idx, number_points_ahead)

		traj_len = len( self.traj[0] )

		# print "\n"
		# print "self.bodies_array[self.id].next_bus_stop = " + str(self.bodies_array[self.id].next_bus_stop)
		# print "self.bodies_array[self.id].current_bus_stop = " + str(self.bodies_array[self.id].current_bus_stop)
		# print "self.bodies_array[self.id].distance_to_next_stop = " + str(self.bodies_array[self.id].distance_to_next_stop)

		if ( not self.loop_trajectory and (best_idx+5)%traj_len < self.current_trajectory_id ) or best_idx == len(self.traj[0])-1:

			# print "Trajectory Finished"
			self.bodies_array[self.id].commands['throttle'] = 0.
			self.bodies_array[self.id].commands['steering'] = 0.
		

			if self.bodies_array[self.id].current_bus_stop == None and not self.trajectory_finished:

				self.trajectory_finished = True

				self.bodies_array[self.id].current_bus_stop = self.bodies_array[self.id].next_bus_stop

			self.check_bus_stop()


			return

		self.current_trajectory_id = best_idx

		self.compute_distance_to_next_bus_stop(best_idx)

		best_idx += 5

		# reference_state = [self.traj[0][ (current_idx+best_idx)%traj_len ] , self.traj[1][ (current_idx+best_idx)%traj_len ] , self.traj[2][ (current_idx+best_idx)%traj_len ] ]
		reference_state = [self.traj[0][ (best_idx)%traj_len ] , self.traj[1][ (best_idx)%traj_len ] , self.traj[2][ (best_idx)%traj_len ] ]

		# Debugging the trajectory
		# print "self.traj[2] = " + str(self.traj[2]) 
		traveled_distances = []
		changed_directions = []

		for idx in range( 0 , traj_len - 1 ):

			traveled_distances.append( math.hypot( self.traj[0][idx+1] - self.traj[0][idx] , self.traj[1][idx+1] - self.traj[1][idx] ) )
			changed_directions.append( self.traj[2][idx+1] - self.traj[2][idx] )

		[velocity_command, steering_command] = self.controller_action(self.state, reference_state)

		velocity_command = velocity_command
		steering_command = 2.*steering_command

		max_steering_command = math.radians(2.*30.)
		steering_ratio = math.fabs( steering_command / max_steering_command )
		# print "steering_ratio = " + str(steering_ratio)

		# This makes the truck go slower in curves, however it makes the ETA to next bust stop be optimistic!		
		if steering_ratio > 0.2:

			velocity_command = velocity_command*(1 - steering_ratio/2. )

		safety_distance = 10.
		full_stop_distance = 3.
		
		# Will check distance to cars in front
		distance_to_car = self.distance_to_car_in_future_trajectory(current_time, self.sensor_readings, safety_distance)

		if distance_to_car < full_stop_distance:

			velocity_command = 0.
			steering_command = 0.

		elif distance_to_car < safety_distance:

			velocity_command = velocity_command*(distance_to_car/safety_distance)

		self.bodies_array[self.id].commands['throttle'] = velocity_command
		self.bodies_array[self.id].commands['steering'] = steering_command

		return

	def get_trajectory_tracking_inputs(self, current_time):
		# This is the one being used now!
		self.vehicle_iteration += 1

		self.state = [ self.bodies_array[self.id].x, self.bodies_array[self.id].y, math.radians( self.bodies_array[self.id].yaw ) ]
		self.sensor_readings = self.bodies_array[self.id].sensor_readings

		desired_velocity = (self.cruise_velocity / 3.6) * 0.8 
		
		current_idx = self.current_trajectory_id

		number_points_ahead = 25

		best_idx = self.find_closest_trajectory_point( self.state, current_idx, number_points_ahead)
	
		traj_len = len( self.traj[0] )
		
		self.current_trajectory_id = best_idx

		best_idx += 15 # A bigger look ahead, since we should start turning sooner (we are a long bus)

		max_steering_command = 60.

		# reference_state = [self.traj[0][ (current_idx+best_idx)%traj_len ] , self.traj[1][ (current_idx+best_idx)%traj_len ] , self.traj[2][ (current_idx+best_idx)%traj_len ] ]
		reference_state = [self.traj[0][ (best_idx)%traj_len ] , self.traj[1][ (best_idx)%traj_len ] , self.traj[2][ (best_idx)%traj_len ] ]

		steering_command = self.get_steering_command(self.state, reference_state, max_steering_command)

		# if not self.loop_trajectory and ( (best_idx+25)%traj_len < self.current_trajectory_id  or best_idx == len(self.traj[0])-1 ):
		if not self.loop_trajectory and (best_idx+50) > traj_len:

			# print "Trajectory Finished"
			# self.bodies_array[self.id].commands['throttle'] = 0.

			# To make it brake faster
			desired_velocity = -10.

			# [throttle_command, steering_command] = self.controller_action(self.state, reference_state, desired_velocity)
			throttle_command = self.get_throttle_CC(desired_velocity)

			# print "throttle_command = " + str(throttle_command)

			# Let's artificially make it stop faster

			# throttle_command = throttle_command*2.

			self.bodies_array[self.id].commands['throttle'] = throttle_command
			# self.bodies_array[self.id].commands['steering'] = steering_command

			# self.bodies_array[self.id].commands['steering'] = 0.

			current_speed = math.hypot(self.bodies_array[self.id].x_speed, self.bodies_array[self.id].y_speed)

			# print "current_speed = " + str(current_speed)
			# print "1.0*(1000./3600.) = " + str(1.0*(1000./3600.))


			if self.bodies_array[self.id].current_bus_stop == None and not self.trajectory_finished and current_speed < 1.0*(1000./3600.):

				self.trajectory_finished = True

				self.bodies_array[self.id].current_bus_stop = self.bodies_array[self.id].next_bus_stop

			if current_speed < 1.0*(1000./3600.):

				self.check_bus_stop()

			return


		safety_distance = 10.
		full_stop_distance = 3.
		
		# Will check distance to cars in front
		distance_to_car = self.distance_to_car_in_future_trajectory(current_time, self.sensor_readings, safety_distance)

		# desired_velocity = self.cruise_velocity / 3.6

		if distance_to_car < full_stop_distance:

			# print "Full stop, vehicle id: " + str( self.id )

			desired_velocity = 0.
			
		elif distance_to_car < safety_distance:

			desired_velocity = desired_velocity*(distance_to_car/safety_distance)


		max_steering_command = math.radians(max_steering_command)
		steering_ratio = math.fabs( steering_command / max_steering_command )
		# print "steering_ratio = " + str(steering_ratio)
		if steering_ratio > 0.1:

			desired_velocity = desired_velocity*(1 - steering_ratio/2. )

		throttle_command = self.get_throttle_CC(desired_velocity)
		# [throttle_command, steering_command] = self.controller_action(self.state, reference_state, desired_velocity)


		self.bodies_array[self.id].commands['throttle'] = throttle_command
		self.bodies_array[self.id].commands['steering'] = steering_command

		return




	def check_bus_stop(self):

		if self.bus_stop_waiting:

			stopped_time = time.time() - self.bus_stop_arrival_time

			if stopped_time > self.bus_stop_waiting_time:

				if self.traj is self.bus_traj_1:

					self.traj = self.bus_traj_2

				elif self.traj is self.bus_traj_2:

					self.traj = self.bus_traj_3

				elif self.traj is self.bus_traj_3:

					self.traj = self.bus_traj_1

				else:

					raise NameError("Unexpected situation? Which trajectory am I? Dummy Bus Vehicle")

				self.current_trajectory_id = 0
				self.trajectory_finished = False
				self.bus_stop_waiting = False

		else:

			self.bus_stop_arrival_time = time.time()
			self.bus_stop_waiting = True

	def check_bus_stop_old(self):

		# print "Checking Bus Stop"

		if self.bus_stop_waiting:

			stopped_time = time.time() - self.bus_stop_arrival_time

			if stopped_time > self.bus_stop_waiting_time:

				self.go_to_next_bus_stop()

		else:

			self.bus_stop_arrival_time = time.time()
			self.bus_stop_waiting = True

	def check_bus_stop(self):

		print "check_bus_stop"
		print "self.bodies_array[self.id].current_bus_stop = " + str(self.bodies_array[self.id].current_bus_stop) 

		self.bus_stop_waiting = True

		# I will depart when my container gets this order:
		if self.bodies_array[self.id].current_bus_stop == None:

			self.go_to_next_bus_stop()
		

	def go_to_next_bus_stop(self):

		for idx, bus_section_traj in enumerate(self.bus_trajs):

			if self.traj is bus_section_traj:

				new_idx = idx+1
				new_idx = new_idx%len(self.bus_trajs)

				self.traj = self.bus_trajs[new_idx]

				# self.traj_np = self.traj = [traj_x, traj_y, traj_theta, traj_time]
				# self.np_traj = numpy.asarray([ traj_x , traj_y , traj_theta])
				self.np_traj = numpy.asarray([ self.traj[0] , self.traj[1] , self.traj[2]])

				next_idx = new_idx+1
				next_idx = next_idx%len(self.bus_trajs)


				self.bodies_array[self.id].next_bus_stop = self.bus_stop_names[next_idx]
				self.bodies_array[self.id].current_bus_stop = None

			# else:

			# 	raise NameError("Unexpected situation? Which trajectory am I? Dummy Bus Vehicle")

				self.current_trajectory_id = 0
				self.trajectory_finished = False
				self.bus_stop_waiting = False



				return

	def compute_distance_to_next_bus_stop(self, current_trajectory_id):

		number_points_to_bus_stop = len(self.traj[0]) - current_trajectory_id

		# Assuming 0.2 meters per trajectory point:

		distance_to_next_bus_stop = number_points_to_bus_stop*0.2

		# print "distance_to_next_bus_stop = " + str(distance_to_next_bus_stop)

		self.bodies_array[self.id].distance_to_next_stop = distance_to_next_bus_stop


	def set_desired_velocity(self, desired_velocity_km_h):

		# I receive the desired cruise velocity in km/h, and set it as my desired velocity

		self.cruise_velocity = desired_velocity_km_h
		self.bodies_array[self.id].cruise_velocity = desired_velocity_km_h

	def update_my_desired_velocity(self):

		self.cruise_velocity = self.bodies_array[self.id].cruise_velocity

