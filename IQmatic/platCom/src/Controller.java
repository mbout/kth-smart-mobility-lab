import java.awt.event.*;

public class Controller implements ActionListener{
    public Model model;
    public View view;
    
    public Controller(View viewIn, int portNoIn){
        model = new Model(portNoIn, this);
        view = viewIn;
    }

    public void actionPerformed(ActionEvent event){
        String targetString = view.targetField.getText();
        String paramString = view.paramField.getText();

        String commandId = (String)view.list.getSelectedItem();

        model.sendCommand(commandId, targetString, paramString);
    }

    public void printToView(String outString){
        view.printToView(outString);
    }
}
