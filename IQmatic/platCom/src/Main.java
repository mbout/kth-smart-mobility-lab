import javax.swing.JFrame;
import javax.swing.JOptionPane;


public class Main {

    public static void main(String[] args) {
        

        int portNo = Integer.parseInt((String)JOptionPane.showInputDialog("Choose port to access:", 34978));
        View view = new View(portNo);
        view.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        view.pack();
        view.setVisible(true);
    }
} 
