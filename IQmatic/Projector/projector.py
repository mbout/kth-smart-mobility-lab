#!/usr/bin/python

import pygame
import sys, math, os
import threading
import time, datetime
import errno
from xml.etree import ElementTree as ET
from random import randint
from socket import *
from socket import error as  socket_error

import projector
sys.path.append('../')
from mocap import Mocap, Body

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    WORLD = '\033[1;97m' 
    PROJECTOR = '\033[1;32m'
    VEHICLE = '\033[1;95m'
    CMD = '\033[1;93m'
    QUALISYS = '\033[1;96m'

def timed_print(message,color = None,parent= None):

	try:
		color = getattr(bcolors,color)
	except:
		color = ''
	try:
		parent = getattr(bcolors,parent)
	except:
		parent = ''
	print parent + get_current_time() + bcolors.ENDC + ' ' + color + message + bcolors.ENDC

def get_current_time():
	return datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S ")

def start_thread(handler,args=None):
	t = threading.Thread(target=handler,args=args)
	t.daemon = True
	t.start()

def conv_real_2_projector(pos,mode=None):

	global global_variables

	global_variables['projector_origin_px']

	conv_x = float(global_variables['image_size'][0])/float(global_variables['projector_area'][1]-global_variables['projector_area'][0])
	conv_y = float(global_variables['image_size'][1])/float(global_variables['projector_area'][3]-global_variables['projector_area'][2])

	global_variables['projector_origin_px'] = (int(float(global_variables['projector_area'][1])*conv_x),int(float(global_variables['projector_area'][3])*conv_y))

	if isinstance(pos,tuple) and len(pos) == 2 and mode == None:
		x_px = round(global_variables['projector_origin_px'][0] + conv_x*pos[0])
		y_px = round(global_variables['projector_origin_px'][1] - conv_y*pos[1])
		return (int(x_px),int(y_px))

	elif isinstance(pos,tuple) and len(pos) == 2 and mode == 'conv':
		x_px = round(conv_x*pos[0])
		y_px = round(conv_y*pos[1])
		return (int(x_px),int(y_px))	

	elif isinstance(pos,float):
		if mode == 'x':
			value = global_variables['projector_origin_px'][0] + conv_x*pos
		elif mode == 'y':
			value = global_variables['projector_origin_px'][1] - conv_y*pos
		return int(round(value))
		
	elif isinstance(pos,list) and len(pos) == 4 and mode == None:
		value=[0,0,0,0]
		value[0] = int(round(global_variables['projector_origin_px'][0] + conv_x*pos[0]))
		value[1] = int(round(global_variables['projector_origin_px'][0] + conv_x*pos[1]))
		value[2] = int(round(global_variables['projector_origin_px'][1] - conv_x*pos[2]))
		value[3] = int(round(global_variables['projector_origin_px'][1] - conv_x*pos[3]))
		return (value[0],value[1],value[2],value[3])

	elif isinstance(pos,list) and len(pos) == 4 and mode == 'conv':
		value=[0,0,0,0]
		value[0] = int(round(conv_x*pos[0]))
		value[1] = int(round(conv_x*pos[1]))
		value[2] = int(round(conv_x*pos[2]))
		value[3] = int(round(conv_x*pos[3]))
		return (value[0],value[1],value[2],value[3])

def synchronize_bodies_lists(current_bodies_list,bodies_list):
	if bodies_list == None:
		return []
	for current_body in current_bodies_list:
		for body in bodies_list:
			if current_body['id'] == body['id']:
				if body.has_key('controllable'):
					current_body['controllable'] = body['controllable']
				else:
					current_body['controllable'] = False

	merged_bodies_list = current_bodies_list
	id_list = [current_body['id'] for current_body in merged_bodies_list]
	[merged_bodies_list.append(current_body) for current_body in bodies_list if (current_body['id'] not in id_list and current_body['id'] < 0)]
 
	return merged_bodies_list

def prettify(elem):
    rough_string = ET.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")

def print_time_diff(previous_time):
	current_time = time.time()
	time_diff = current_time - previous_time
	previous_time = current_time
	print "time_diff: " + str(time_diff)
	return previous_time

def update_fps(previous_time,window,font,background_color):
	current_time = time.time()
	time_diff = current_time - previous_time
	
	fps = int(1.0 / time_diff)
	
	draw_fps(fps,window,font,background_color)

	return current_time
 
def getPixelArray(image): 
    return pygame.surfarray.array3d(image) 

def saveSurface(pixels, filename = None):
    try:
    	print 'going to save surface'
        surf = pygame.surfarray.make_surface(pixels)
        print 'no error'
    except IndexError:
        (width, height, colours) = pixels.shape
        surf = pygame.display.set_mode((width, height))
        pygame.surfarray.blit_array(surf, pixels)
        surf.update()
    
    if not None:
    	pygame.image.save(surf, filename)
    
    return surf

def rotate_image(car,angle,pos):

	theta = math.radians(angle)
	s = math.sin(theta)
	c = math.cos(theta)

	center = (car.get_width()/2,car.get_height()/2)

	rotation_center = (center[0],center[1])

	new_point = (center[0]-rotation_center[0],center[1]-rotation_center[1])

	new_pos = (new_point[0]*c - new_point[1]*s,new_point[0]*s + new_point[1]*c)

	new_pos = (new_pos[0] + rotation_center[0],new_pos[1] + rotation_center[1])

	pos = (pos[0]+int(new_pos[0]),pos[1]-int(new_pos[1]))

	car_rotated = pygame.transform.rotate(car,angle)

	return [car_rotated, pos]

def rotate_polygon(origin,width,height,yaw):


	p1 = (int(origin[0]-width/2),int(origin[1]-height/2))
	p2 = (int(origin[0]-width/2),int(origin[1]+height/2))
	p3 = (int(origin[0]+width/2),int(origin[1]+height/2))
	p4 = (int(origin[0]+width/2),int(origin[1]-height/2))

	p5 = (int(origin[0] + (p1[0] - origin[0])*math.cos(-yaw) - ((p1[1] - origin[1])*math.sin(-yaw))),int(origin[1] + (p1[0] - origin[0])*math.sin(-yaw) + (p1[1] - origin[1])*math.cos(-yaw)))
	p6 = (int(origin[0] + (p2[0] - origin[0])*math.cos(-yaw) - ((p2[1] - origin[1])*math.sin(-yaw))),int(origin[1] + (p2[0] - origin[0])*math.sin(-yaw) + (p2[1] - origin[1])*math.cos(-yaw)))
	p7 = (int(origin[0] + (p3[0] - origin[0])*math.cos(-yaw) - ((p3[1] - origin[1])*math.sin(-yaw))),int(origin[1] + (p3[0] - origin[0])*math.sin(-yaw) + (p3[1] - origin[1])*math.cos(-yaw)))
	p8 = (int(origin[0] + (p4[0] - origin[0])*math.cos(-yaw) - ((p4[1] - origin[1])*math.sin(-yaw))),int(origin[1] + (p4[0] - origin[0])*math.sin(-yaw) + (p4[1] - origin[1])*math.cos(-yaw)))

	return  [p5,p6,p7,p8]

def draw_origin(window,origin):
	pygame.draw.circle(window,(255,255,255),origin,10,4)

def draw_fps(fps,window,font,background_color):
	global global_variables
	scoretext = font.render("FPS:"+str(fps),True,(255, 255, 255))
	window.blit(scoretext, (global_variables['image_size'][0] - global_variables['image_size'][0]*0.4 , global_variables['image_size'][1] - global_variables['image_size'][1]*0.1))

def draw_message(window,font,msg):
	global global_variables
	msgtext = font.render(msg,True,(255, 255, 255))
	window.blit(msgtext, (global_variables['image_size'][0] - global_variables['image_size'][0]*0.7 , global_variables['image_size'][1] - global_variables['image_size'][1]*0.95))
	#window.blit(msgtext, (global_variables['image_size'][0] - global_variables['image_size'][0]*0.9 , global_variables['image_size'][1] - global_variables['image_size'][1]*0.1))

def draw_bodies(window,bodies_list,sim_origin,sim_conv):

	global global_variables

	radius = 16
	linewidth = 2

	car = global_variables['images']['cars']
	obstacle = global_variables['images']['construction']
	water = global_variables['images']['water']
	crosshairs = global_variables['images']['crosshairs']

	if not isinstance(bodies_list,list):
		return

	gun = [body for body in bodies_list if body.has_key('obstacle') and body['obstacle'] and body['obstacle_type'] == 'gun']

	for body in bodies_list:

		if not isinstance(body,dict):
			continue

		yaw = math.radians(body['yaw'])

		if body['id'] < 0 or body['body_type'] == 'simulator_vehicle':

			if global_variables['only_real_vehicles']:
				continue


			x = body['x']*sim_conv[0] + sim_origin[0]
			y = -body['y']*sim_conv[1] + sim_origin[1]
			pos = (int(round(x)),int(round(y)))

			if body['body_type'] == 'simulator_vehicle':
				the_car = global_variables['images']['simulator_vehicle']
			else:
				if isinstance(car, list):
					car_number = abs(body['id'])%len(car)
					the_car = car[car_number]

			car_size = the_car.get_size()
			car_rotated = pygame.transform.rotate(the_car,body['yaw'])
			car_size_rotated = car_rotated.get_size()
			#pygame.draw.circle(window,(255,0,0),pos,radius/5,0)
			
 			new_x = pos [0] - car_size_rotated[0]/2 
 			new_y = pos [1] - car_size_rotated[1]/2 
 			pos = (int(new_x),int(new_y))


 			if (gun and math.hypot((body['x']-gun[0]['x']),(body['y']-gun[0]['y']))<0.2) or global_variables['show_ids']:
				pygame.draw.circle(window,(0,0,0,0),pos,20,0)
				idtext = global_variables['ids_font'].render(str(body['id']),True,(255, 255, 255))
				window.blit(idtext,pos)

 			elif global_variables['draw_signal']:
				if body['signal_strength'] in  [-1,0]:
					window.blit(global_variables['images']['signal_loss'], pos)
				else:
					window.blit(global_variables['images']['signal_' + str(round(body['signal_strength']/2.0,1))[-1]], pos)

 			window.blit(car_rotated, pos)


		elif body['id'] > 0:
			if global_variables['only_simulated_vehicles'] and not body['obstacle']:
				continue
			x = body['x']*sim_conv[0] + sim_origin[0]
			y = -body['y']*sim_conv[1] + sim_origin[1]

			pos = (int(round(x)),int(round(y)))

			if body.has_key('obstacle') and body['obstacle']:
				if body['obstacle_type'] == 'construction':

					obstacle_size = obstacle.get_size()
					obstacle_rotated = pygame.transform.rotate(obstacle,body['yaw'])

					obstacle_rotated_size = obstacle_rotated.get_size()
				
	 				new_x = pos [0] - obstacle_rotated_size[0]/2 
	 				new_y = pos [1] - obstacle_rotated_size[1]/2 
	 				pos = (int(new_x),int(new_y))

	 				window.blit(obstacle_rotated, pos)

	 			elif body['obstacle_type'] == 'rrt':

	 				width = int(body['width']*sim_conv[0])
					height = int(body['height']*sim_conv[0])

					if width > 0 and height > 0:

						water = pygame.transform.smoothscale(water,(width,height))

		 				water_size = water.get_size()
						water_rotated = pygame.transform.rotate(water,body['yaw'])

						water_rotated_size = water_rotated.get_size()
					
		 				new_x = pos [0] - water_rotated_size[0]/2 
		 				new_y = pos [1] - water_rotated_size[1]/2 
		 				pos = (int(new_x),int(new_y))

		 				window.blit(water_rotated, pos)


	 			elif body['obstacle_type'] == 'circle':
	 				radius_obstacle = int(body['radius']*sim_conv[0])

	 				pygame.draw.circle(window,(255,255,255,127),pos,radius_obstacle,0)

	 			elif body['obstacle_type'] == 'gun':

	 				radius_obstacle = int(body['radius']*sim_conv[0])

	 				crosshairs = pygame.transform.smoothscale(crosshairs,(radius_obstacle*2,radius_obstacle*2))

		 			crosshairs_size = crosshairs.get_size()
					crosshairs_rotated = pygame.transform.rotate(crosshairs,body['roll'])

					crosshairs_rotated_size = crosshairs_rotated.get_size()

		 			new_x = pos [0] - crosshairs_rotated_size[0]/2 
		 			new_y = pos [1] - crosshairs_rotated_size[1]/2 
		 			pos = (int(new_x),int(new_y))

		 			window.blit(crosshairs_rotated, pos)

	 			elif body['obstacle_type'] == 'rectangle':
	 				width = int(body['width']*sim_conv[0])
					height = int(body['height']*sim_conv[0])

					polygon_rotated = rotate_polygon((x,y),width,height,yaw)
					
					pygame.draw.polygon(window,(255,255,255,127),polygon_rotated,0)

				if body['obstacle_type'] != 'gun' and ((gun and math.hypot((body['x']-gun[0]['x']),(body['y']-gun[0]['y']))<0.2) or global_variables['show_ids']):
							pygame.draw.circle(window,(0,0,0,127),pos,radius,0)
							idtext = global_variables['ids_font'].render(str(body['id']),True,(255, 255, 255))
							window.blit(idtext,pos)

 			else:
 				if body['id'] == global_variables['VEHICLE_HIGHLIGHT_ID']:
 					multi = int(datetime.datetime.now().strftime("%S")[-1])
 					pygame.draw.circle(window,(255,0,0,127),pos,int(radius*multi/4.),0)
 				else:

 					if body.has_key('body_type') and body['body_type'] == 'antenna':
						if (gun and math.hypot((body['x']-gun[0]['x']),(body['y']-gun[0]['y']))<0.2) or global_variables['show_ids']:
							pygame.draw.circle(window,(0,0,0,127),pos,radius,0)
							idtext = global_variables['ids_font'].render(str(body['id']),True,(255, 255, 255))
							window.blit(idtext,pos)
 						elif global_variables['draw_signal']:
 							multi = int(datetime.datetime.now().strftime("%f")[0:2])
 							pygame.draw.circle(window,(255,255,255,127),pos,int(radius*multi/6.) + 2,2)
 						else:
 							pygame.draw.circle(window,(255,255,255,127),pos,int(radius),linewidth)

 					else:

 						if (gun and math.hypot((body['x']-gun[0]['x']),(body['y']-gun[0]['y']))<0.2) or global_variables['show_ids']:
	 						pygame.draw.circle(window,(0,0,0,127),pos,radius,0)
	 						idtext = global_variables['ids_font'].render(str(body['id']),True,(255, 255, 255))
							window.blit(idtext,pos)
 						elif global_variables['draw_signal']:
 							if body.has_key('signal_strength') and body['signal_strength'] in  [-1,0]:
 								window.blit(global_variables['images']['signal_loss'], pos)
 							else:
 								window.blit(global_variables['images']['signal_' + str(round(body['signal_strength']/2.0,1))[-1]], pos)
	 					else:
	 						pygame.draw.circle(window,(255,255,255,127),pos,radius,linewidth)

	if global_variables['activate_draw_origin']:
		draw_origin(window,sim_origin)
		#draw_origin(window,origin_virtual)

def draw_rrt(window,sim_conv,sim_origin):

	global global_variables

	tree_color = (255, 255, 255)
	branch_color = (255, 0, 0)
	line_closed = False
	line_thickness = int(2.0)

	total_drawing_time = 5 #seconds

	current_time = time.time() - global_variables['rrt_time']
	#print "current_time: " + str(current_time)
 	list_nodes = global_variables['rrt_tree_nodes'][1:]

 	if current_time < total_drawing_time:
 		current_max_index = int((float(current_time)/float(total_drawing_time))*len(list_nodes))
 		#print "current_max_index: " + str(current_max_index)
 		list_nodes = list_nodes[:current_max_index]

	for node in list_nodes:

		parent_node = global_variables['rrt_tree_nodes'][node[2]]

		line_tuple_list = []

		line_tuple_list.append((int(node[0]*sim_conv[0] + sim_origin[0]),int(-node[1]*sim_conv[1] + sim_origin[1])))
		line_tuple_list.append((int(parent_node[0]*sim_conv[0] + sim_origin[0]),int(-parent_node[1]*sim_conv[1] + sim_origin[1])))

		if current_time < total_drawing_time*2:
			pygame.draw.lines(window, tree_color, line_closed, tuple( line_tuple_list ), line_thickness)

		list_nodes = global_variables['rrt_solution_nodes'][1:]

	if current_time > total_drawing_time:

		for i,node_id in enumerate(list_nodes):

			parent_id = global_variables['rrt_solution_nodes'][i]

			node = global_variables['rrt_tree_nodes'][node_id]
			parent_node = global_variables['rrt_tree_nodes'][parent_id]

			line_tuple_list = []
			line_tuple_list.append((int(node[0]*sim_conv[0] + sim_origin[0]),int(-node[1]*sim_conv[1] + sim_origin[1])))
			line_tuple_list.append((int(parent_node[0]*sim_conv[0] + sim_origin[0]),int(-parent_node[1]*sim_conv[1] + sim_origin[1])))

			pygame.draw.lines(window, branch_color, line_closed, tuple( line_tuple_list ), line_thickness)


	# if current_time > total_drawing_time*4:
	# 	global_variables['draw_rrt'] = False

def draw_sensors(window,bodies_list,sim_conv,sim_origin):

	global global_variables

	if global_variables['v2v_method'] not in ['off','']:

		linewidth = 2

		for body in bodies_list:

			if body['collision']:
				color = (255,0,0,127)
			else:
				color = (255,255,255,127)

			if body['obstacle'] or body['body_type'] in ['trailer','antenna']:
				continue

			if (global_variables['only_real_vehicles'] and body['id'] < 0) or (global_variables['only_simulated_vehicles'] and body['id'] > 0):
				continue

			x = body['x']*sim_conv[0] + sim_origin[0]
			y = -body['y']*sim_conv[1] + sim_origin[1]
			yaw = math.radians(body['yaw'])
			pos = (int(round(x)),int(round(y)))
			if global_variables['v2v_method'] == 'communications':
				if global_variables['v2v_range'] != '': 
					comm_range = int(float(global_variables['v2v_range'])*sim_conv[0])
					if comm_range > 0:
						pygame.draw.circle(window,color,pos,comm_range,linewidth)
						window.set_alpha(75)
			elif global_variables['v2v_method'] == 'sensors':

				if global_variables['v2v_range'] != '' and global_variables['v2v_angle'] != '': 
					v2v_range = float(global_variables['v2v_range'])
					xp = v2v_range*math.cos(yaw)
					yp = v2v_range*math.sin(yaw)
					angle = math.radians(float(global_variables['v2v_angle'])/2.)
					x1 = body['x'] + xp*math.cos(angle) - yp*math.sin(angle)
					y1 = body['y'] + yp*math.cos(angle) + xp*math.sin(angle)
					x1 = x1*sim_conv[0] + sim_origin[0]
					y1 = -y1*sim_conv[1] + sim_origin[1]
					pos1 = (int(round(x1)),int(round(y1)))
					x2 = body['x'] + xp*math.cos(-angle) - yp*math.sin(-angle)
					y2 = body['y'] + yp*math.cos(-angle) + xp*math.sin(-angle)
					x2 = x2*sim_conv[0] + sim_origin[0] 
					y2 = -y2*sim_conv[1] + sim_origin[1]
					pos2 = (int(round(x2)),int(round(y2)))

					#cone_area = [x-v2v_range*sim_conv[0],y-v2v_range*sim_conv[1],2*v2v_range*sim_conv[0],2*v2v_range*sim_conv[1]]
					# pygame.draw.rect(window,color,cone_area,linewidth)
					#pygame.draw.arc(window, color,cone_area, -angle+yaw, angle+yaw, linewidth)
				

					pygame.draw.line(window,color,pos1,pos2,linewidth)
					pygame.draw.line(window,color,pos,pos1,linewidth)
					pygame.draw.line(window,color,pos,pos2,linewidth)

def draw_obstructed_lanelet(window,sim_origin,sim_conv):

	global global_variables

	polygon_color = (255, 0, 0)

	for lanelet in global_variables['lanelet_obtruction']:

		points = [(int(point[0]*sim_conv[0] + sim_origin[0]),int(-point[1]*sim_conv[1] + sim_origin[1])) for point in lanelet['points']] 

		pygame.draw.polygon(window,polygon_color,points,0)

def parse_bodies_xml(data,bodies_list,activate_states_from_mocap):

	world_bodies_list = []
	try:
		root = ET.fromstring(data)
	except:
		print 'error parse: ' +  str(data)
		return bodies_list

	for body_info in root.findall('body_info'):
		body = dict()
		body['x'] = float(body_info.get('x'))
		body['y'] = float(body_info.get('y'))
		body['z'] = float(body_info.get('z'))
		body['roll'] = float(body_info.get('roll'))
		body['pitch'] = float(body_info.get('pitch'))
		body['yaw'] = float(body_info.get('yaw'))

		if body_info.get('signal_strength') == None:
			body['signal_strength'] = 0.0
		else:	
			body['signal_strength'] = float(body_info.get('signal_strength'))


		body['body_type'] = body_info.get('body_type')

		body['id'] = int(body_info.get('id'))
		body['controllable'] = body_info.get('controllable') == 'True'
		body['obstacle'] = body_info.get('obstacle') == 'True'
		body['collision'] = body_info.get('collision') == 'True'

		if body['obstacle']:
			body['obstacle_type'] = body_info.get('obstacle_type')
			if body['obstacle_type'] in ['circle','gun']:
				body['radius'] = float(body_info.get('radius'))
			elif body['obstacle_type'] in ['rectangle','rrt']:
				body['width'] = float(body_info.get('width'))
				body['height'] = float(body_info.get('height'))

		#print "body_info.get('obstacle'): " + str(body_info.get('obstacle'))

		if activate_states_from_mocap and body['id'] > 0:
			continue
		world_bodies_list.append(body)


	return synchronize_bodies_lists(world_bodies_list,bodies_list)

def parse_rrt_xml(data):

	global global_variables

	rrt_tree_nodes = []
	rrt_solution_nodes = []
	try:
		root = ET.fromstring(data)
	except:
		print 'error parse ' +  str(data)
		return None

	[tree_root,solution_root] =root.getchildren()

	rrt_body_id = int(tree_root.get('body_id'))

	print "rrt_body_id: " + str(rrt_body_id)

	for node in tree_root.getchildren():
		x = float(node.get('x'))
		y = float(node.get('y'))

		parent_id = int(node.get('parent_id'))
		
		rrt_tree_nodes.append([x,y,parent_id])

	for node in solution_root.getchildren():
		node_id = int(node.get('id'))

		rrt_solution_nodes.append(node_id)

	return [rrt_tree_nodes,rrt_solution_nodes,rrt_body_id]

def	parse_lanelet_obstruction_xml(data,lanelet_obtruction_list):

	try:
		root = ET.fromstring(data)
	except:
		print 'error parse ' +  str(data)
		return None

	lanelet_polygons = root.getchildren()

	for lanelet_polygon in lanelet_polygons:

		lanelet_id = lanelet_polygon.get('id')
		lanelet_status = lanelet_polygon.get('obstructed') == 'True'

		if lanelet_status:

			lanelet = dict()
			lanelet['lanelet_id']  = lanelet_id
			lanelet['points'] = [(float(point.get('x'))/32.,float(point.get('y'))/32.) for point in  lanelet_polygon.getchildren()[0].getchildren()]
			lanelet_obtruction_list.append(lanelet)

		else:
			lanelet_obtruction_list = [lanelet for lanelet in lanelet_obtruction_list if lanelet_id != lanelet['lanelet_id']]

	return lanelet_obtruction_list
	
def start_projector_client(info,client_type):

	global global_variables

	BUFSIZE = global_variables['BUFSIZE']

	HOST = info['projector_host']
	PORT = info['projector_port']

	ADDR = (HOST,PORT)


	try:
		cli = socket( AF_INET,SOCK_STREAM)
		cli.connect((ADDR))

		data = cli.recv(BUFSIZE)

		if data == 'OK\n' or data == 'OK\n':

			cli.send(client_type)

			data = cli.recv(BUFSIZE)

			if data == 'no wait' or data  =='no wait\r\n':
				global_variables['wait'] = False
			elif data == 'wait for command central' or data  == 'wait for command central\r\n':
				global_variables['wait'] = True
			else:
				return [0,0]
			if client_type == 'projector-main':
				return [cli,global_variables['wait']]
			else:
				return cli
	except:
		return [0,0]

def send_request(cli,request,extra_info=None):

	global global_variables

	BUFSIZE = global_variables['BUFSIZE']

	try:
		cli.send(request)
 	except:
 		return
 	if request == 'BACKGROUND':
 		
 		cli.settimeout(0.1)

		if global_variables['image_folder'] == os.path.join('..','resources'):
			file_name = 'background.bmp'
		else:
			file_name = os.path.join('Projector','background.bmp')

 		with open(file_name,'wb') as img:
 			while True:
 				try:
 					data = cli.recv(BUFSIZE);

				except:
					break

				img.write(data)
		img.close()

	elif request == 'IMAGE_PROPERTIES':
		cli.settimeout(0.1)
		data = cli.recv(BUFSIZE);
		if data == '':
			return
		data = data.split(' ')
		return [int(float(data[1])) , int(float(data[3])), float(data[5])]


	elif request == 'STATES':

		cli.settimeout(0.2)
		
		while True:
			try:
				global_variables['received_data'] = global_variables['received_data'] + cli.recv(BUFSIZE)

			except socket_error as e:
				if e.errno == errno.ECONNRESET:
					global_variables['CLOSE'] = True
					return
			
			if global_variables['received_data'].find('\n') != -1:
				data = global_variables['received_data'].split('\n')
				global_variables['received_data'] = '\n'.join(data[1:])
				data = data[0]
				break


		if global_variables['activate_states_from_world']:
			global_variables['bodies_list'] = parse_bodies_xml(data,global_variables['bodies_list'],global_variables['activate_states_from_mocap'])

		else:
			global_variables['bodies_list'] = []

		if global_variables['activate_states_from_mocap'] and not global_variables['only_simulated_vehicles']:
			try:
		 		mocap_bodies_list = global_variables['Qs'].get_updated_bodies()
		 	except:
		 		print "global_variables['Qs']: " + str(global_variables['Qs'])
		 		mocap_bodies_list = []
		 	try:
		 		global_variables['bodies_list'] = global_variables['bodies_list'] + mocap_bodies_list
		 	except:
		 		global_variables['bodies_list'] = mocap_bodies_list


	elif request == 'HIGHLIGHT':
		data = cli.recv(BUFSIZE);
		if data == 'PORT':
			cli.send(str(extra_info))

def states_handler(states_cli):

	global global_variables

	while not global_variables['CLOSE']:
		send_request(states_cli,'STATES')

	send_request(states_cli,'CLOSE')
	try:
		states_cli.close()
	except:
		return

def highlight_handler(highlight_cli):

	global global_variables

	HOST = ''
	ADDR = (HOST,0)
	BUFSIZE = 16096

	global_variables['highlight_server'] = socket( AF_INET,SOCK_STREAM)
	global_variables['highlight_server'].setsockopt(SOL_SOCKET,SO_REUSEADDR, 1)
	global_variables['highlight_server'].bind((ADDR))

	HIGHLIGHT_PORT = global_variables['highlight_server'].getsockname()[1]

	timed_print('Waiting for highlight requests in port: '+str(HIGHLIGHT_PORT),parent = 'PROJECTOR')

	send_request(highlight_cli,'HIGHLIGHT',HIGHLIGHT_PORT)

	global_variables['highlight_server'].listen(1)

	global_variables['highlight_server'],addr = global_variables['highlight_server'].accept()

	highlight_cli.close()

	global_variables['highlight_server'].settimeout(1)

	global_variables['highlight_data'] = ''

	while not global_variables['CLOSE']:

		while 1:
			try:
				global_variables['highlight_data'] = global_variables['highlight_data'] + global_variables['highlight_server'].recv(BUFSIZE)
			except timeout:
				continue

			if global_variables['highlight_data'].find('\n') != -1:
				data = global_variables['highlight_data'].split('\n')
				global_variables['highlight_data'] = '\n'.join(data[1:])
				data = data[0]
				break

		if data == 'VEHICLE_HIGHLIGHT':
			global_variables['highlight_server'].send('ID')
			global_variables['VEHICLE_HIGHLIGHT_ID'] = int(global_variables['highlight_server'].recv(BUFSIZE))
			#global_variables['new_message'] = 'Truck ('+str(global_variables['VEHICLE_HIGHLIGHT_ID'])+') selected'

		elif data == 'MESSAGE':
			global_variables['highlight_server'].send('OK')
			#global_variables['new_message'] = global_variables['highlight_server'].recv(BUFSIZE)

			message = global_variables['highlight_server'].recv(BUFSIZE)

			message = ET.fromstring(message)

			if message.get('type') == 'task_completed':
				if int(message.get('body_id')) == global_variables['rrt_body_id']:
					global_variables['draw_rrt'] = False
					global_variables['rrt_time'] = 0


		elif data == 'UPDATE_SETTINGS':

			global_variables['highlight_server'].send('OK')

			number_of_settings = int(global_variables['highlight_server'].recv(BUFSIZE))

			global_variables['highlight_server'].send('OK')

			previous_mocap = global_variables['activate_states_from_mocap']

			for i in xrange(number_of_settings):
				try:
					label = global_variables['highlight_server'].recv(BUFSIZE)
				except:
					continue
				global_variables['highlight_server'].send('OK')

				data = global_variables['highlight_server'].recv(BUFSIZE)

				if label == 'projector_area':
					global_variables[label] = [float(ele) for ele in data[1:-1].split(',')]
					#convx = float(global_variables['image_size'][0])/float(global_variables['projector_area'][1]-global_variables['projector_area'][0])
					#convy = float(global_variables['image_size'][1])/float(global_variables['projector_area'][3]-global_variables['projector_area'][2])
				elif label in ['v2v_method','v2v_range','v2v_angle']:
					global_variables[label] = str(data)
				else:
					global_variables[label] = data == 'True'

			if previous_mocap != global_variables['activate_states_from_mocap']:
				if global_variables['activate_states_from_mocap']:
					global_variables['Qs'] = Mocap(info=0)

		elif data == 'RRT':
			global_variables['highlight_server'].send('OK')

			rrt_string = ''

			while True:
				try:
					rrt_string = rrt_string + global_variables['highlight_server'].recv(BUFSIZE)
				except socket_error as e:
					if e.errno == errno.ECONNRESET:
						timed_print('Highlight disconnected','WARNING',parent = 'PROJECTOR')
						global_variables['highlight_server'].close()
						return
			
				if rrt_string.find('\n') != -1:
					data = rrt_string.split('\n')
					global_variables['highlight_data'] = '\n'.join(data[1:])
					rrt_string = data[0]
					break

			parsed_msg = parse_rrt_xml(rrt_string)

			if parsed_msg != None:
				global_variables['rrt_tree_nodes'] = parsed_msg[0]
				global_variables['rrt_solution_nodes'] =  parsed_msg[1]
				global_variables['rrt_body_id'] = parsed_msg[2]
				global_variables['draw_rrt'] = True
				global_variables['rrt_time'] = 0
			else:
				global_variables['draw_rrt'] = False

		elif data == 'LANELET_OBSTRUCTION':
			global_variables['highlight_server'].send('OK')

			obstruction_string = ''

			while True:
				try:
					obstruction_string = obstruction_string + global_variables['highlight_server'].recv(BUFSIZE)
				except socket_error as e:
					if e.errno == errno.ECONNRESET:
						timed_print('Highlight disconnected','WARNING',parent = 'PROJECTOR')
						global_variables['highlight_server'].close()
						return
			
				if obstruction_string.find('\n') != -1:
					data = obstruction_string.split('\n')
					global_variables['highlight_data'] = '\n'.join(data[1:])
					obstruction_string = data[0]
					break

			global_variables['lanelet_obtruction'] = parse_lanelet_obstruction_xml(obstruction_string,global_variables['lanelet_obtruction'])

		else:
			timed_print('Not a valid highlight request: ','WARNING',parent = 'PROJECTOR')

	global_variables['highlight_server'].close()

def get_center_font(image_size,font,text):

	font_dimensions =  font.size(text)
	center = [int(image_size[0]/2), int(image_size[1]/2)]
 	center[0] -= int(font_dimensions[0]/2)
 	center[1] -= int(font_dimensions[1]/2)
 	center = tuple(center)

 	return center

def wait_screen_handler(window,image_folder):
	global global_variables
	
	font_size_initial = 30
	font_size_max = 50

	text = "SML WORLD"

	font_type = 'comicsansms'

	background_color = (0,0,0)

	steel = pygame.image.load(image_folder+"/steel.jpg")

	step = 1
	font_size = font_size_initial

	while not global_variables['CLOSE'] and global_variables['wait']:

		for event in pygame.event.get():
			if event.type == pygame.QUIT: 
				global_variables['CLOSE'] = True
				global_variables['wait'] = False
				pygame.quit()
				return
				#sys.exit(0) 
			elif event.type == pygame.VIDEORESIZE:
				(width, height) = event.size

				if width > global_variables['image_size'][0]:
					width = global_variables['image_size'][0]
				if height > global_variables['image_size'][1]:
					height = global_variables['image_size'][1]
 
				new_size = (width,height)

				global_variables['image_size'] = new_size

		font_sml = pygame.font.SysFont(font_type,font_size)

		rendered_font = font_sml.render(text,True,(255, 255, 255))

		center = get_center_font(global_variables['image_size'],font_sml,text)
		center = (center[0],center[1]-200)

 		#window.fill(background_color)
 		window.blit(steel,(0,0))
		window.blit(rendered_font,center)


		font_size = font_size + step

		if font_size > font_size_max:
			step = -1
		elif font_size < font_size_initial:
			step = 1

		font_warning = pygame.font.SysFont(font_type,20)
		warningtext = "Waiting for command central to connect"
		rendered_font = font_warning.render(warningtext,True,(255, 255, 255))
		center = get_center_font(global_variables['image_size'],font_warning,warningtext)
		center = (center[0],global_variables['image_size'][1] - 150 )
		window.blit(rendered_font,center)

		pygame.display.update()

		time.sleep(0.03)

def scale_image(bg,convx,convy,sim_origin,sim_conv,area):

	global global_variables

	original_size = bg.get_size()

	#print 'bg original size ' + str(original_size)

	bg = pygame.transform.rotozoom(bg,0,convx/sim_conv)
	
	new_size = bg.get_size()

	new_sim_x = int(round(sim_origin[0] + (new_size[0] - original_size[0])/2.))
	new_sim_y = int(round(sim_origin[1] + (new_size[1] - original_size[1])/2.))

	sim_origin = (new_sim_x,new_sim_y)

	sim_conv=[convx,convy]

	#print 'bg new size ' + str(new_size)

	#print 'sim_origin ' + str(sim_origin)

	bg_array = getPixelArray(bg)

	#area_px = conv_real_2_projector(area,'conv')
	projector_area = [area[0]*convx+sim_origin[0],area[1]*convx+sim_origin[0],-area[3]*convy+sim_origin[1],-area[2]*convy+sim_origin[1]]
	projector_area = [int(ele) for ele in projector_area]
	#print 'projector area ' + str(projector_area)
 	bg_array_cropped = bg_array[projector_area[0]:projector_area[1],projector_area[2]:projector_area[3], :]
	bg_array_cropped = bg_array_cropped[0:global_variables['image_size'][0],0:global_variables['image_size'][1],:]
	bg_array = bg_array_cropped
	
	bg = bg_array

	#print 'new sim size ' + str(len(bg_array)) + ' ' + str(len(bg_array[1,:])) 

	new_sim_x = sim_origin[0] - projector_area[0]
	new_sim_y = sim_origin[1] - projector_area[2]

	sim_origin = (new_sim_x,new_sim_y)

	#print 'new sim origin ' + str(sim_origin)

	return [sim_conv,sim_origin,bg]

def load_car_images(image_folder,car_list,sim_conv):

	global global_variables

	car_dimensions = (0.15*1,0.066*1)

	x_px = round(sim_conv[0]*car_dimensions[0])
	y_px = round(sim_conv[1]*car_dimensions[1])
	car_size = (int(x_px),int(y_px))	

	car_images = []

	for car_ele in car_list:

		car = pygame.image.load(os.path.join(image_folder,car_ele + ".png"))
		car = pygame.transform.smoothscale(car,car_size)

		car_images.append(car)

	global_variables['images']['simulator_vehicle'] = pygame.image.load(os.path.join(image_folder,"simulator_vehicle.png"))
	global_variables['images']['simulator_vehicle'] = pygame.transform.smoothscale(global_variables['images']['simulator_vehicle'],car_size)

	return car_images

def start_projector(image_folder,projector_info):

	global global_variables

	global_variables = dict()

	global_variables['CLOSE'] = False
	global_variables['wait'] = False
	global_variables['BUFSIZE'] = 4096
	global_variables['bodies_list'] = []
	global_variables['highlight_server'] = []
	global_variables['projector_origin_px'] = []
	global_variables['lanelet_obtruction'] = []
	global_variables['VEHICLE_HIGHLIGHT_ID'] = 0
	global_variables['activate_states_from_world'] = True
	global_variables['activate_states_from_mocap'] = False
	global_variables['only_simulated_vehicles'] = False
	global_variables['only_real_vehicles'] = False
	global_variables['activate_fps'] = False
	global_variables['activate_draw_origin'] = False
	global_variables['draw_signal'] = False
	global_variables['show_ids'] = False
	global_variables['draw_sensors'] = False
	global_variables['draw_rrt'] = False
	global_variables['image_size'] = (1024,742)
	#global_variables['projector_area'] = [-3.25,4.380,-2.910,2.770]
	global_variables['projector_area'] = [-3.35,4.480,-2.910,2.770]
	global_variables['image_folder'] = image_folder
	global_variables['v2v_method'] = ''
	global_variables['v2v_range'] = ''
	global_variables['v2v_angle'] = ''
	global_variables['received_data'] = ''

	global_variables['Qs'] = []

	info = dict()

	info['projector_host'] = projector_info[0]
	info['projector_port'] = projector_info[1]
	info['origin'] = 0

	# if image_folder == os.path.join('..','resources'):
	# 	[global_variables['rrt_tree_nodes'],global_variables['rrt_solution_nodes']] = parse_rrt_xml(open('../rrt_solution_message_example.xml').read())
	# else:
	# 	[global_variables['rrt_tree_nodes'],global_variables['rrt_solution_nodes']] = parse_rrt_xml(open('rrt_solution_message_example.xml').read())


	global_variables['new_message'] = None

	if global_variables['only_real_vehicles'] and global_variables['only_simulated_vehicles']:
		timed_print('Both only_simulated_vehicles ans only_real_vehicles options selected','FAIL',parent = 'PROJECTOR')
		sys.exit(1)
	elif global_variables['only_simulated_vehicles'] and not global_variables['activate_states_from_world']:
		timed_print('To adquire simulated vehicles you must activate the option activate_states_from_world','FAIL',parent = 'PROJECTOR')
		sys.exit(1)	

	if global_variables['activate_states_from_mocap']:
		global_variables['Qs'] = Mocap(info=0)

	timed_print('Starting Projector Manager','OKGREEN',parent = 'PROJECTOR')

	[cli,global_variables['wait']] = start_projector_client(info,'projector-main')

	if cli == 0:
		timed_print('Problem connecting to SML World ('+projector_info[0]+','+str(projector_info[1])+')','FAIL',parent = 'PROJECTOR')
		return


	window_x = 0
	window_y = 0
	os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (window_x,window_y)

	pygame.init()
	pygame.font.init()

	font = pygame.font.SysFont('monospace',25)
	font.set_bold(True)

	global_variables['ids_font'] = pygame.font.SysFont('monospace',25)
	global_variables['ids_font'].set_bold(True)


	window = pygame.display.set_mode(global_variables['image_size'],pygame.RESIZABLE | pygame.NOFRAME )

	if global_variables['wait']:
		start_thread(wait_screen_handler,args=(window,image_folder))
		#try:
 		data = cli.recv(global_variables['BUFSIZE']);
		#except:
		#	CLOSE = True
		#	return
		if data == 'command _central on':
			global_variables['wait'] = False
		else:
			global_variables['CLOSE'] = True
			return


	timed_print('Request for Image Properties',parent = 'PROJECTOR')
	image_properties = send_request(cli,'IMAGE_PROPERTIES')
	print "image_properties: " + str(image_properties)
	if image_properties == None:
		return

	timed_print('Image Properties Received',parent = 'PROJECTOR')

	timed_print('Request for Background Image',parent = 'PROJECTOR')
	send_request(cli,'BACKGROUND')
	timed_print('Background Image Received',parent = 'PROJECTOR')

 
 	timed_print('Listening to highlights requests from SML World','OKBLUE',parent = 'PROJECTOR')
	highlight_cli = start_projector_client(info,'projector-highlights')
	start_thread(highlight_handler,args=[highlight_cli])


	if global_variables['activate_states_from_world'] or (global_variables['only_real_vehicles'] and not global_variables['activate_states_from_mocap']):
		timed_print('Starting adquiring states from SML World','OKBLUE',parent = 'PROJECTOR')
		states_cli = start_projector_client(info,'projector-states')
		start_thread(states_handler,args=[states_cli])

	# timed_print('Request for Image Properties',parent = 'PROJECTOR')
	# image_properties = send_request(cli,'IMAGE_PROPERTIES')
	# if image_properties == None:
	# 	return
	# #print image_properties

	# timed_print('Image Properties Received',parent = 'PROJECTOR')

	# timed_print('Request for Background Image',parent = 'PROJECTOR')
	# send_request(cli,'BACKGROUND')
	# timed_print('Background Image Received',parent = 'PROJECTOR')

	conv_sml_2_real = 32.0
	conv_real_2_projector('init')

	convx = float(global_variables['image_size'][0])/float(global_variables['projector_area'][1]-global_variables['projector_area'][0])
	convy = float(global_variables['image_size'][1])/float(global_variables['projector_area'][3]-global_variables['projector_area'][2])

	#print 'projector convx ' + str(convx)
	#print 'projector convy ' + str(convy)

	global_variables['sim_origin'] = (image_properties[0],image_properties[1])

	#print 'original origin ' + str(global_variables['sim_origin'])

	global_variables['sim_conv'] = image_properties[2]*conv_sml_2_real

	#print 'real conv ' + str(global_variables['sim_conv'])

	current_time = time.time()

	if image_folder == os.path.join('..','resources'):
		bg = pygame.image.load("background.bmp")
	else:
		bg = pygame.image.load(os.path.join("Projector","background.bmp"))

	[global_variables['sim_conv'],global_variables['sim_origin'],bg_projector] = scale_image(bg,convx,convy,global_variables['sim_origin'],global_variables['sim_conv'],global_variables['projector_area'])

	car_list = ['carWhite','carRed','carBlue','carGreen','carYellow']

	global_variables['images'] = dict()

	global_variables['images']['cars'] = load_car_images(image_folder,car_list,global_variables['sim_conv'])

	global_variables['images']['excavator'] = pygame.image.load(os.path.join(image_folder,"excavator.png"))
	global_variables['images']['construction'] = pygame.image.load(os.path.join(image_folder,"construction.png"))
	global_variables['images']['water'] = pygame.image.load(os.path.join(image_folder,"water.png"))
	global_variables['images']['crosshairs'] = pygame.image.load(os.path.join(image_folder,"crosshairs.png"))
	global_variables['images']['signal_loss'] = pygame.image.load(os.path.join(image_folder,"signal_loss.png"))
	global_variables['images']['antenna'] = pygame.image.load(os.path.join(image_folder,"antenna.png"))


	for i in xrange(6):
		global_variables['images']['signal_'+str(i)] =  pygame.image.load(os.path.join(image_folder,'signal_'+str(i)+'.png'))


	car_dimensions = (0.45*1,0.35*1)
	x_px = round(global_variables['sim_conv'][0]*car_dimensions[0])
	y_px = round(global_variables['sim_conv'][1]*car_dimensions[1])
	car_size = (int(x_px),int(y_px))

	global_variables['images']['excavator'] = pygame.transform.smoothscale(global_variables['images']['excavator'],car_size)
	global_variables['images']['excavator'] = pygame.transform.rotate(global_variables['images']['excavator'],180)

	global_variables['images']['construction'] = pygame.transform.smoothscale(global_variables['images']['construction'],car_size)
	global_variables['images']['signal_loss'] = pygame.transform.smoothscale(global_variables['images']['signal_loss'],car_size)

	global_variables['images']['antenna'] = pygame.transform.smoothscale(global_variables['images']['antenna'],car_size)
	global_variables['images']['antenna'] = pygame.transform.rotate(global_variables['images']['antenna'],180)

	for i in xrange(6):
		global_variables['images']['signal_'+str(i)] = pygame.transform.smoothscale(global_variables['images']['signal_'+str(i)],car_size)


	#####window.blit(bg_projector,(0,0))
	pygame.surfarray.blit_array(window,bg_projector)

	global_variables['rrt_time'] = 0

	while not global_variables['CLOSE']: 
		tic = time.time()
		
		for event in pygame.event.get():
			if event.type == pygame.QUIT: 
				#print event
				send_request(cli,'CLOSE')
				send_request(states_cli,'CLOSE')
				global_variables['CLOSE'] = True
				cli.close()
				highlight_cli.close()
				global_variables['highlight_server'].close()
				pygame.quit()
				return
				#sys.exit(0)
			elif event.type == pygame.VIDEORESIZE:
				(width, height) = event.size

				if width > global_variables['image_size'][0]:
					width = global_variables['image_size'][0]
				if height > global_variables['image_size'][1]:
					height = global_variables['image_size'][1]
 
				new_size = (width,height)

				#print "event.size: " + str(event.size)

				window = pygame.display.set_mode(global_variables['image_size'],pygame.RESIZABLE)
				#print "new_size: " + str(new_size)
				global_variables['image_size'] = new_size 
				global_variables['sim_origin'] = (image_properties[0],image_properties[1])
				global_variables['sim_conv'] = image_properties[2]*conv_sml_2_real

				conv = float(global_variables['image_size'][0])/float(global_variables['projector_area'][1]-global_variables['projector_area'][0])
				
				[global_variables['sim_conv'],global_variables['sim_origin'],bg_projector] = scale_image(bg,convx,convy,global_variables['sim_origin'],global_variables['sim_conv'],global_variables['projector_area'])

				#if global_variables['VEHICLE_HIGHLIGHT_ID'] == 0:
					#####window.blit(bg_projector,(0,0))
				pygame.surfarray.blit_array(window,bg_projector)
				#	pass

			else:
				pass
				#print event
		

		if (global_variables['activate_states_from_mocap'] and not global_variables['activate_states_from_world']) or (global_variables['only_real_vehicles'] and global_variables['activate_states_from_mocap']):
		 	try:
		 		global_variables['bodies_list'] = global_variables['Qs'].get_updated_bodies()
		 	except:
		 		global_variables['bodies_list']

		#if global_variables['VEHICLE_HIGHLIGHT_ID'] == 0:
			#####window.blit(bg_projector,(0,0))
		pygame.surfarray.blit_array(window, bg_projector)
		window.blit(global_variables['images']['excavator'],(750,350))	

		#msg = 'Truck has reached the loading station'


		if global_variables['draw_sensors']:
			draw_sensors(window, global_variables['bodies_list'], global_variables['sim_conv'], global_variables['sim_origin'])

		if global_variables['draw_rrt']:
			if global_variables['rrt_time'] == 0:
				global_variables['rrt_time'] = time.time()
			draw_rrt(window, global_variables['sim_conv'], global_variables['sim_origin'])

		draw_obstructed_lanelet(window,global_variables['sim_origin'],global_variables['sim_conv'])

		draw_bodies(window,global_variables['bodies_list'],global_variables['sim_origin'],global_variables['sim_conv'])

		draw_message(window,font,global_variables['new_message'])

		if global_variables['activate_fps']:
			#if global_variables['VEHICLE_HIGHLIGHT_ID'] == 0:
			background_color = (80, 163, 77)
			#else:
			#	background_color = (0,0,0)
			current_time = update_fps(current_time,window,font,background_color)

		window.blit(global_variables['images']['antenna'],(int(global_variables['image_size'][0]/2),int(global_variables['image_size'][1])-50))	

		pygame.display.flip()

		#print time.time() - tic
 
	try:
		send_request(cli,'CLOSE')
	except:
		pass

def stop_projector():
	global global_variables
	global_variables['CLOSE'] = True

	time.sleep(1)
	pygame.quit()

if __name__ == "__main__":

	image_folder =  os.path.join('..','resources') 

	#projector_info = ('localhost',8000)
	projector_info = ('130.237.43.135',8000)
	#projector_info = ('130.237.50.246',8000)

	start_projector(image_folder,projector_info)