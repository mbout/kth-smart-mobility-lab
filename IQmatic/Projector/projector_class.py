#!/usr/bin/python

import pygame
import sys, math, os
import threading
import time, datetime
import errno
from xml.etree import ElementTree as ET
from random import randint
from socket import *
from socket import error as  socket_error

import projector
sys.path.append('../')
from mocap import Mocap, Body

class Projector(object):
	"""Projector"""

	def __init__(self,projector_info,image_folder):
		super(Projector, self).__init__()
		
		self.info = dict()
		self.info['projector_host'] = projector_info[0]
		self.info['projector_port'] = projector_info[1]

		self.image_folder = image_folder

		self.color = self.get_color()

		self.CLOSE = False
		self.wait = False
		self.BUFSIZE = 4096
		self.bodies_list = []
		self.highlight_server = []
		self.projector_origin_px = []
		self.lanelet_obtruction = []
		self.VEHICLE_HIGHLIGHT_ID = 0
		self.activate_states_from_world = True
		self.activate_states_from_mocap = False
		self.only_simulated_vehicles = False
		self.only_real_vehicles = False
		self.activate_fps = False
		self.activate_draw_origin = False
		self.draw_signal = False
		self.show_ids = False
		self.draw_sensors = False
		self.draw_rrt = False
		self.window = None

		# self.image_size = (1024,742)
		self.image_size = (1400,1026)
		
		# self.image_size = (1920,1176)
		# self.image_size = (1024,900)

		self.conv_sml_2_real = 32.0
		self.bg_projector = None
		#self.projector_area = [-3.25,4.380,-2.910,2.770]
		self.projector_area = [-3.35,4.480,-2.910,2.770]
		self.v2v_method = ''
		self.v2v_range = ''
		self.v2v_angle = ''
		self.received_data = ''

		self.Qs = []

		self.new_message = None

		if self.only_real_vehicles and self.only_simulated_vehicles:
			self.timed_print('Both only_simulated_vehicles ans only_real_vehicles options selected','FAIL',parent = 'PROJECTOR')
			sys.exit(1)
		elif self.only_simulated_vehicles and not self.activate_states_from_world:
			self.timed_print('To adquire simulated vehicles you must activate the option activate_states_from_world','FAIL',parent = 'PROJECTOR')
			sys.exit(1)	

		if self.activate_states_from_mocap:
			self.Qs = self.Mocap(info=0)

	def get_color(self):
		colors = dict()
		colors['HEADER']  = '\033[95m'
		colors['OKBLUE']  = '\033[94m'
		colors['OKGREEN']  = '\033[92m'
		colors['WARNING']  = '\033[93m'
		colors['FAIL']  = '\033[91m'
		colors['ENDC']  = '\033[0m'
		colors['BOLD']  = '\033[1m'
		colors['UNDERLINE']  = '\033[4m'
		colors['WORLD']  = '\033[1;97m'
		colors['PROJECTOR']  = '\033[1;32m'
		colors['VEHICLE']  = '\033[1;95m'
		colors['CMD']  = '\033[1;93m'
		colors['QUALISYS']  = '\033[1;96m'

		self.colors = colors 

	def timed_print(self,message,color = None,parent= None):
		try:
			color = self.colors[color]
		except:
			color = ''
		try:
			parent = self,color[parent]
		except:
			parent = ''
		print parent + self.get_current_time() + self.colors['ENDC']  + ' ' + color + message + self.colors['ENDC']

	def get_current_time(self):
		return datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S ")

	def start_thread(self,handler,args=()):
		t = threading.Thread(target=handler,args=args)
		t.daemon = True
		t.start()

	def conv_real_2_projector(self,pos,mode=None):

		self.projector_origin_px

		conv_x = float(self.image_size[0])/float(self.projector_area[1]-self.projector_area[0])
		conv_y = float(self.image_size[1])/float(self.projector_area[3]-self.projector_area[2])

		self.projector_origin_px = (int(float(self.projector_area[1])*conv_x),int(float(self.projector_area[3])*conv_y))

		if isinstance(pos,tuple) and len(pos) == 2 and mode == None:
			x_px = round(self.projector_origin_px[0] + conv_x*pos[0])
			y_px = round(self.projector_origin_px[1] - conv_y*pos[1])
			return (int(x_px),int(y_px))

		elif isinstance(pos,tuple) and len(pos) == 2 and mode == 'conv':
			x_px = round(conv_x*pos[0])
			y_px = round(conv_y*pos[1])
			return (int(x_px),int(y_px))	

		elif isinstance(pos,float):
			if mode == 'x':
				value = self.projector_origin_px[0] + conv_x*pos
			elif mode == 'y':
				value = self.projector_origin_px[1] - conv_y*pos
			return int(round(value))
			
		elif isinstance(pos,list) and len(pos) == 4 and mode == None:
			value=[0,0,0,0]
			value[0] = int(round(self.projector_origin_px[0] + conv_x*pos[0]))
			value[1] = int(round(self.projector_origin_px[0] + conv_x*pos[1]))
			value[2] = int(round(self.projector_origin_px[1] - conv_x*pos[2]))
			value[3] = int(round(self.projector_origin_px[1] - conv_x*pos[3]))
			return (value[0],value[1],value[2],value[3])

		elif isinstance(pos,list) and len(pos) == 4 and mode == 'conv':
			value=[0,0,0,0]
			value[0] = int(round(conv_x*pos[0]))
			value[1] = int(round(conv_x*pos[1]))
			value[2] = int(round(conv_x*pos[2]))
			value[3] = int(round(conv_x*pos[3]))
			return (value[0],value[1],value[2],value[3])

	def synchronize_bodies_lists(self,current_bodies_list,bodies_list):

		if bodies_list == None:
			return []
		for current_body in current_bodies_list:
			for body in bodies_list:
				if current_body['id'] == body['id']:
					if body.has_key('controllable'):
						current_body['controllable'] = body['controllable']
					else:
						current_body['controllable'] = False

		# body['body_type'] == 'person'

		merged_bodies_list = current_bodies_list
		id_list = [current_body['id'] for current_body in merged_bodies_list]
		[merged_bodies_list.append(current_body) for current_body in bodies_list if (current_body['id'] not in id_list and current_body['id'] < 0 and body['body_type'] != 'person')]
	 
		return merged_bodies_list

	def prettify(self,elem):
	    rough_string = ET.tostring(elem, 'utf-8')
	    reparsed = minidom.parseString(rough_string)
	    return reparsed.toprettyxml(indent="  ")

	def print_time_diff(self,previous_time):
		current_time = time.time()
		time_diff = current_time - previous_time
		previous_time = current_time
		print "time_diff: " + str(time_diff)
		return previous_time

	def update_fps(self,previous_time,font,background_color):
		current_time = time.time()
		time_diff = current_time - previous_time
		
		fps = int(1.0 / time_diff)
		
		self.draw_fps(fps,font,background_color)

		return current_time
	 
	def getPixelArray(self,image): 
	    return pygame.surfarray.array3d(image) 

	def saveSurface(self,pixels, filename = None):
	    try:
	    	print 'going to save surface'
	        surf = pygame.surfarray.make_surface(pixels)
	    except IndexError:
	        (width, height, colours) = pixels.shape
	        surf = pygame.display.set_mode((width, height))
	        pygame.surfarray.blit_array(surf, pixels)
	        surf.update()
	    
	    if not None:
	    	pygame.image.save(surf, filename)
	    
	    return surf

	def rotate_image(self,car,angle,pos):

		theta = math.radians(angle)
		s = math.sin(theta)
		c = math.cos(theta)

		center = (car.get_width()/2,car.get_height()/2)

		rotation_center = (center[0],center[1])

		new_point = (center[0]-rotation_center[0],center[1]-rotation_center[1])

		new_pos = (new_point[0]*c - new_point[1]*s,new_point[0]*s + new_point[1]*c)

		new_pos = (new_pos[0] + rotation_center[0],new_pos[1] + rotation_center[1])

		pos = (pos[0]+int(new_pos[0]),pos[1]-int(new_pos[1]))

		car_rotated = pygame.transform.rotate(car,angle)

		return [car_rotated, pos]

	def rotate_polygon(self,origin,width,height,yaw):

		p1 = (int(origin[0]-width/2),int(origin[1]-height/2))
		p2 = (int(origin[0]-width/2),int(origin[1]+height/2))
		p3 = (int(origin[0]+width/2),int(origin[1]+height/2))
		p4 = (int(origin[0]+width/2),int(origin[1]-height/2))

		p5 = (int(origin[0] + (p1[0] - origin[0])*math.cos(-yaw) - ((p1[1] - origin[1])*math.sin(-yaw))),int(origin[1] + (p1[0] - origin[0])*math.sin(-yaw) + (p1[1] - origin[1])*math.cos(-yaw)))
		p6 = (int(origin[0] + (p2[0] - origin[0])*math.cos(-yaw) - ((p2[1] - origin[1])*math.sin(-yaw))),int(origin[1] + (p2[0] - origin[0])*math.sin(-yaw) + (p2[1] - origin[1])*math.cos(-yaw)))
		p7 = (int(origin[0] + (p3[0] - origin[0])*math.cos(-yaw) - ((p3[1] - origin[1])*math.sin(-yaw))),int(origin[1] + (p3[0] - origin[0])*math.sin(-yaw) + (p3[1] - origin[1])*math.cos(-yaw)))
		p8 = (int(origin[0] + (p4[0] - origin[0])*math.cos(-yaw) - ((p4[1] - origin[1])*math.sin(-yaw))),int(origin[1] + (p4[0] - origin[0])*math.sin(-yaw) + (p4[1] - origin[1])*math.cos(-yaw)))

		return  [p5,p6,p7,p8]

	def draw_origin(self,origin):
		pygame.draw.circle(self.window,(255,255,255),origin,10,4)

	def draw_fps(self,fps,font,background_color):
		scoretext = font.render("FPS:"+str(fps),True,(255, 255, 255))
		self.window.blit(scoretext, (self.image_size[0] - self.image_size[0]*0.4 , self.image_size[1] - self.image_size[1]*0.1))

	def draw_message(self,font,msg):
		msgtext = font.render(msg,True,(255, 255, 255))
		self.window.blit(msgtext, (self.image_size[0] - self.image_size[0]*0.7 , self.image_size[1] - self.image_size[1]*0.95))

	def draw_bodies(self,bodies_list,sim_origin,sim_conv):

		radius = 32
		linewidth = 2

		car = self.images['cars']
		bus = self.images['excavator']
		obstacle = self.images['construction']
		water = self.images['water']
		crosshairs = self.images['crosshairs']
		quadcopter = self.images['quadcopter']

		if not isinstance(bodies_list,list):
			return

		gun = [body for body in bodies_list if body.has_key('obstacle') and body['obstacle'] and body['obstacle_type'] == 'gun']

		for body in bodies_list:

			if not isinstance(body,dict):
				continue

			yaw = math.radians(body['yaw'])

			if body['body_type'] == 'person':

				
				x = body['x']*sim_conv[0] + sim_origin[0]
				y = -body['y']*sim_conv[1] + sim_origin[1]
				pos = (int(round(x)),int(round(y)))

				triangle_size = 5

				pos_1 = (int(round(x-triangle_size)),int(round(y)))
				pos_2 = (int(round(x+triangle_size)),int(round(y)))
				pos_3 = (int(round(x)),int(round(y+triangle_size)))

				# pygame.draw.circle(self.window,(0,0,0,0),pos,5,0)

				pointlist = [ pos_1, pos_2, pos_3 ]
				pygame.draw.polygon(self.window,(0,255,0,0),pointlist,0)


				continue



			if body['id'] < 0 or body['body_type'] == 'simulator_vehicle':

				if self.only_real_vehicles:
					continue

				x = body['x']*sim_conv[0] + sim_origin[0]
				y = -body['y']*sim_conv[1] + sim_origin[1]
				pos = (int(round(x)),int(round(y)))

				if body['body_type'] == 'simulator_vehicle':
					the_car = self.images['simulator_vehicle']
				elif body['body_type'] == 'construction':
					the_car = self.images['excavator']
				elif body['body_type'] == 'bus':
					the_car = self.images['bus']
				elif body['body_type'] == 'smart_vehicle':
					the_car = self.images['smart_vehicle']
				else:
					if isinstance(car, list):
						car_number = abs(body['id'])%len(car)
						the_car = car[car_number]

				car_size = the_car.get_size()
				car_rotated = pygame.transform.rotate(the_car,body['yaw'])
				car_size_rotated = car_rotated.get_size()
				#pygame.draw.circle(self.window,(255,0,0),pos,radius/5,0)
				
	 			new_x = pos [0] - car_size_rotated[0]/2 
	 			new_y = pos [1] - car_size_rotated[1]/2 
	 			pos = (int(new_x),int(new_y))


	 			if (gun and math.hypot((body['x']-gun[0]['x']),(body['y']-gun[0]['y']))<0.2) or self.show_ids:
					pygame.draw.circle(self.window,(0,0,0,0),pos,20,0)
					idtext = self.ids_font.render(str(body['id']),True,(255, 255, 255))
					self.window.blit(idtext,pos)

	 			elif self.draw_signal:
					if body['signal_strength'] in  [-1,0]:
						self.window.blit(self.images['signal_loss'], pos)
					else:
						self.window.blit(self.images['signal_' + str(round(body['signal_strength']/2.0,1))[-1]], pos)

	 			self.window.blit(car_rotated, pos)


			elif body['id'] > 0:
				if self.only_simulated_vehicles and not body['obstacle']:
					continue
				x = body['x']*sim_conv[0] + sim_origin[0]
				y = -body['y']*sim_conv[1] + sim_origin[1]

				pos = (int(round(x)),int(round(y)))

				if body.has_key('obstacle') and body['obstacle']:
					if body['obstacle_type'] == 'construction':

						obstacle_size = obstacle.get_size()
						obstacle_rotated = pygame.transform.rotate(obstacle,body['yaw'])

						obstacle_rotated_size = obstacle_rotated.get_size()
					
		 				new_x = pos [0] - obstacle_rotated_size[0]/2 
		 				new_y = pos [1] - obstacle_rotated_size[1]/2 
		 				pos = (int(new_x),int(new_y))

		 				self.window.blit(obstacle_rotated, pos)

		 			elif body['obstacle_type'] == 'rrt':

		 				width = int(body['width']*sim_conv[0])
						height = int(body['height']*sim_conv[0])

						if width > 0 and height > 0:

							water = pygame.transform.smoothscale(water,(width,height))

			 				water_size = water.get_size()
							water_rotated = pygame.transform.rotate(water,body['yaw'])

							water_rotated_size = water_rotated.get_size()
						
			 				new_x = pos [0] - water_rotated_size[0]/2 
			 				new_y = pos [1] - water_rotated_size[1]/2 
			 				pos = (int(new_x),int(new_y))

			 				self.window.blit(water_rotated, pos)


		 			elif body['obstacle_type'] == 'circle':
		 				radius_obstacle = int(body['radius']*sim_conv[0])

		 				pygame.draw.circle(self.window,(255,255,255,127),pos,radius_obstacle,0)

		 			elif body['obstacle_type'] == 'gun':

		 				radius_obstacle = int(body['radius']*sim_conv[0])

		 				crosshairs = pygame.transform.smoothscale(crosshairs,(radius_obstacle*2,radius_obstacle*2))

			 			crosshairs_size = crosshairs.get_size()
						crosshairs_rotated = pygame.transform.rotate(crosshairs,body['roll'])

						crosshairs_rotated_size = crosshairs_rotated.get_size()

			 			new_x = pos [0] - crosshairs_rotated_size[0]/2 
			 			new_y = pos [1] - crosshairs_rotated_size[1]/2 
			 			pos = (int(new_x),int(new_y))

			 			self.window.blit(crosshairs_rotated, pos)

		 			elif body['obstacle_type'] == 'rectangle':
		 				width = int(body['width']*sim_conv[0])
						height = int(body['height']*sim_conv[0])

						# x1 = pos
						# print "x1: " + str(x1)
						# x2 = (int(pos[0] + width*math.cos(yaw))),int(pos[1] + width*math.sin(yaw)))
						# print "x2: " + str(x2)
						# x3 = (int(pos[0] + width*math.cos(yaw + math.radians(45))),int(pos[1] + height*math.sin(yaw + math.radians(45)))
						# print "x3: " + str(x3)
						# x4 = (int(pos[0] + height*math.cos(yaw))),int(pos[1] + height*math.sin(yaw)))
						# print "x4: " + str(x4)

						# pygame.draw.polygon(self.window,(255,255,255,127),[x1,x2,x3,x4],0)

						polygon_rotated = self.rotate_polygon((x,y),width,height,yaw)
						pygame.draw.polygon(self.window,(255,255,255,127),polygon_rotated,0)
						

					if body['obstacle_type'] != 'gun' and ((gun and math.hypot((body['x']-gun[0]['x']),(body['y']-gun[0]['y']))<0.2) or self.show_ids):
								pygame.draw.circle(self.window,(0,0,0,127),pos,radius,0)
								idtext = self.ids_font.render(str(body['id']),True,(255, 255, 255))
								self.window.blit(idtext,pos)

				
				elif body['body_type'] == 'quadcopter':

			 			quadcopter_size = quadcopter.get_size()
						quadcopter_rotated = pygame.transform.rotate(quadcopter,body['yaw'])

						quadcopter_rotated = pygame.transform.rotozoom(quadcopter_rotated,0, 0.1 + body['z']/10 if body['z'] > 0 else 0)

						quadcopter_rotated_size = quadcopter_rotated.get_size()

			 			new_x = pos [0] - quadcopter_rotated_size[0]/2 
			 			new_y = pos [1] - quadcopter_rotated_size[1]/2 
			 			pos = (int(new_x),int(new_y))

			 			self.window.blit(quadcopter_rotated, pos)

	 			else:

	 				if body['id'] == self.VEHICLE_HIGHLIGHT_ID:
	 					multi = int(datetime.datetime.now().strftime("%S")[-1])
	 					pygame.draw.circle(self.window,(255,0,0,127),pos,int(radius*multi/4.),0)
	 				else:

	 					if body.has_key('body_type') and body['body_type'] == 'antenna':
							if (gun and math.hypot((body['x']-gun[0]['x']),(body['y']-gun[0]['y']))<0.2) or self.show_ids:
								pygame.draw.circle(self.window,(0,0,0,127),pos,radius,0)
								idtext = self.ids_font.render(str(body['id']),True,(255, 255, 255))
								self.window.blit(idtext,pos)
	 						elif self.draw_signal:
	 							multi = int(datetime.datetime.now().strftime("%f")[0:2])
	 							pygame.draw.circle(self.window,(255,255,255,127),pos,int(radius*multi/6.) + 2,2)
	 						else:
	 							pygame.draw.circle(self.window,(255,255,255,127),pos,int(radius),linewidth)

	 					else:

	 						if (gun and math.hypot((body['x']-gun[0]['x']),(body['y']-gun[0]['y']))<0.2) or self.show_ids:
		 						pygame.draw.circle(self.window,(0,0,0,127),pos,radius,0)
		 						idtext = self.ids_font.render(str(body['id']),True,(255, 255, 255))
								self.window.blit(idtext,pos)
	 						elif self.draw_signal:
	 							if body.has_key('signal_strength') and body['signal_strength'] in  [-1,0]:
	 								self.window.blit(self.images['signal_loss'], pos)
	 							else:
	 								self.window.blit(self.images['signal_' + str(round(body['signal_strength']/2.0,1))[-1]], pos)
		 					else:
		 						pygame.draw.circle(self.window,(255,255,255,127),pos,radius,linewidth)

		if self.activate_draw_origin:
			self.draw_origin(sim_origin)

	def draw_rrt_fnc(self,sim_conv,sim_origin):

		tree_color = (255, 255, 255)
		branch_color = (255, 0, 0)
		line_closed = False
		line_thickness = int(2.0)

		total_drawing_time = 5 #seconds

		current_time = time.time() - self.rrt_time
		#print "current_time: " + str(current_time)
	 	list_nodes = self.rrt_tree_nodes[1:]

	 	if current_time < total_drawing_time:
	 		current_max_index = int((float(current_time)/float(total_drawing_time))*len(list_nodes))
	 		#print "current_max_index: " + str(current_max_index)
	 		list_nodes = list_nodes[:current_max_index]

		for node in list_nodes:

			parent_node = self.rrt_tree_nodes[node[2]]

			line_tuple_list = []

			line_tuple_list.append((int(node[0]*sim_conv[0] + sim_origin[0]),int(-node[1]*sim_conv[1] + sim_origin[1])))
			line_tuple_list.append((int(parent_node[0]*sim_conv[0] + sim_origin[0]),int(-parent_node[1]*sim_conv[1] + sim_origin[1])))

			if current_time < total_drawing_time*2:
				pygame.draw.lines(self.window, tree_color, line_closed, tuple( line_tuple_list ), line_thickness)

			list_nodes = self.rrt_solution_nodes[1:]

		if current_time > total_drawing_time:

			for i,node_id in enumerate(list_nodes):

				parent_id = self.rrt_solution_nodes[i]

				node = self.rrt_tree_nodes[node_id]
				parent_node = self.rrt_tree_nodes[parent_id]

				line_tuple_list = []
				line_tuple_list.append((int(node[0]*sim_conv[0] + sim_origin[0]),int(-node[1]*sim_conv[1] + sim_origin[1])))
				line_tuple_list.append((int(parent_node[0]*sim_conv[0] + sim_origin[0]),int(-parent_node[1]*sim_conv[1] + sim_origin[1])))

				pygame.draw.lines(self.window, branch_color, line_closed, tuple( line_tuple_list ), line_thickness)

	def draw_sensors_fnc(self,bodies_list,sim_conv,sim_origin):

		if self.v2v_method not in ['off','']:

			linewidth = 2

			for body in bodies_list:

				# Persons don't have sensors, I will not draw them
				if body['body_type'] == 'person':

					continue

				try:
					body['collision']
				except:


					print "MEGA ERROR:"
					print "body = " + str(body)
					print "bodies_list = " + str(bodies_list)


				if body['collision']:
					color = (255,0,0,127)
				else:
					color = (255,255,255,127)

				if body['obstacle'] or body['body_type'] in ['trailer','antenna']:
					continue

				if (self.only_real_vehicles and body['id'] < 0) or (self.only_simulated_vehicles and body['id'] > 0):
					continue

				x = body['x']*sim_conv[0] + sim_origin[0]
				y = -body['y']*sim_conv[1] + sim_origin[1]
				yaw = math.radians(body['yaw'])
				pos = (int(round(x)),int(round(y)))
				if self.v2v_method == 'communications':
					if self.v2v_range != '': 
						comm_range = int( (float(self.v2v_range)/32.)*sim_conv[0] )
						if comm_range > 0:
							pygame.draw.circle(self.window,color,pos,comm_range,linewidth)
							self.window.set_alpha(75)
				elif self.v2v_method == 'sensors':

					if self.v2v_range != '' and self.v2v_angle != '': 
						v2v_range = float(self.v2v_range)/32.
						xp = v2v_range*math.cos(yaw)
						yp = v2v_range*math.sin(yaw)
						angle = math.radians(float(self.v2v_angle)/2.)
						x1 = body['x'] + xp*math.cos(angle) - yp*math.sin(angle)
						y1 = body['y'] + yp*math.cos(angle) + xp*math.sin(angle)
						x1 = x1*sim_conv[0] + sim_origin[0]
						y1 = -y1*sim_conv[1] + sim_origin[1]
						pos1 = (int(round(x1)),int(round(y1)))
						x2 = body['x'] + xp*math.cos(-angle) - yp*math.sin(-angle)
						y2 = body['y'] + yp*math.cos(-angle) + xp*math.sin(-angle)
						x2 = x2*sim_conv[0] + sim_origin[0] 
						y2 = -y2*sim_conv[1] + sim_origin[1]
						pos2 = (int(round(x2)),int(round(y2)))

						#cone_area = [x-v2v_range*sim_conv[0],y-v2v_range*sim_conv[1],2*v2v_range*sim_conv[0],2*v2v_range*sim_conv[1]]
						#pygame.draw.rect(self.window,color,cone_area,linewidth)
						#pygame.draw.arc(self.window, color,cone_area, -angle+yaw, angle+yaw, linewidth)
					
						pygame.draw.line(self.window,color,pos1,pos2,linewidth)
						pygame.draw.line(self.window,color,pos,pos1,linewidth)
						pygame.draw.line(self.window,color,pos,pos2,linewidth)

	def draw_obstructed_lanelet(self,sim_origin,sim_conv):

		polygon_color = (255, 0, 0)

		try:

			for lanelet in self.lanelet_obtruction:

				points = [(int(point[0]*sim_conv[0] + sim_origin[0]),int(-point[1]*sim_conv[1] + sim_origin[1])) for point in lanelet['points']]

				pygame.draw.polygon(self.window,polygon_color,points,0)

		except:
			pass

	def parse_bodies_xml(self,data,bodies_list,activate_states_from_mocap):

		world_bodies_list = []
		try:
			root = ET.fromstring(data)
		except:
			print 'error parse: ' +  str(data)
			return bodies_list

		for body_info in root.findall('body_info'):
			body = dict()
			body['x'] = float(body_info.get('x'))
			body['y'] = float(body_info.get('y'))
			body['z'] = float(body_info.get('z'))
			body['roll'] = float(body_info.get('roll'))
			body['pitch'] = float(body_info.get('pitch'))
			body['yaw'] = float(body_info.get('yaw'))

			if body_info.get('signal_strength') == None:
				body['signal_strength'] = 0.0
			else:	
				body['signal_strength'] = float(body_info.get('signal_strength'))


			body['body_type'] = body_info.get('body_type')

			body['id'] = int(body_info.get('id'))
			body['controllable'] = body_info.get('controllable') == 'True'
			body['obstacle'] = body_info.get('obstacle') == 'True'
			body['collision'] = body_info.get('collision') == 'True'

			if body['obstacle']:
				body['obstacle_type'] = body_info.get('obstacle_type')
				if body['obstacle_type'] in ['circle','gun']:
					body['radius'] = float(body_info.get('radius'))
				elif body['obstacle_type'] in ['rectangle','rrt']:
					body['width'] = float(body_info.get('width'))
					body['height'] = float(body_info.get('height'))

			#print "body_info.get('obstacle'): " + str(body_info.get('obstacle'))

			if activate_states_from_mocap and body['id'] > 0:
				continue
			world_bodies_list.append(body)


		return self.synchronize_bodies_lists(world_bodies_list,bodies_list)

	def parse_rrt_xml(self,data):

		rrt_tree_nodes = []
		rrt_solution_nodes = []
		try:
			root = ET.fromstring(data)
		except:
			print 'error parse ' +  str(data)
			return None

		[tree_root,solution_root] =root.getchildren()

		rrt_body_id = int(tree_root.get('body_id'))

		print "rrt_body_id: " + str(rrt_body_id)

		for node in tree_root.getchildren():
			x = float(node.get('x'))
			y = float(node.get('y'))

			parent_id = int(node.get('parent_id'))
			
			rrt_tree_nodes.append([x,y,parent_id])

		for node in solution_root.getchildren():
			node_id = int(node.get('id'))

			rrt_solution_nodes.append(node_id)

		return [rrt_tree_nodes,rrt_solution_nodes,rrt_body_id]

	def	parse_lanelet_obstruction_xml(self,data,lanelet_obtruction_list):

		try:
			root = ET.fromstring(data)
		except:
			print 'error parse ' +  str(data)
			return None

		lanelet_polygons = root.getchildren()

		for lanelet_polygon in lanelet_polygons:

			lanelet_id = lanelet_polygon.get('id')
			lanelet_status = lanelet_polygon.get('obstructed') == 'True'

			if lanelet_status:

				lanelet = dict()
				lanelet['lanelet_id']  = lanelet_id
				lanelet['points'] = [(float(point.get('x'))/32.,float(point.get('y'))/32.) for point in  lanelet_polygon.getchildren()[0].getchildren()]
				lanelet_obtruction_list.append(lanelet)

			else:
				lanelet_obtruction_list = [lanelet for lanelet in lanelet_obtruction_list if lanelet_id != lanelet['lanelet_id']]

		return lanelet_obtruction_list
		
	def start_projector_client(self,client_type):

		BUFSIZE = self.BUFSIZE

		HOST = self.info['projector_host']
		PORT = self.info['projector_port']

		ADDR = (HOST,PORT)

		try:
			cli = socket( AF_INET,SOCK_STREAM)
			cli.connect((ADDR))

			data = cli.recv(BUFSIZE).strip('\n').strip('\r').lower()

			if data == 'ok':

				cli.send(client_type)

				data = cli.recv(BUFSIZE).strip('\n').strip('\r').lower()

				if data == 'no wait':
					self.wait = False
				elif data == 'wait for command central':
					self.wait = True
				else:
					return [0,0]
				if client_type == 'projector-main':
					return [cli,self.wait]
				else:
					return cli
		except:
			return [0,0]

	def send_request(self,cli,request,extra_info=None):

		BUFSIZE = self.BUFSIZE

		try:
			cli.send(request)
	 	except:
	 		return

	 	request = request.strip('\n').strip('\r').lower()

	 	if request == 'background':
	 		
	 		cli.settimeout(0.1)

			if self.image_folder == os.path.join('..','resources'):
				file_name = 'background.bmp'
			else:
				file_name = os.path.join('Projector','background.bmp')

	 		with open(file_name,'wb') as img:
	 			while True:
	 				try:
	 					data = cli.recv(BUFSIZE);
					except:
						break

					img.write(data)
			img.close()

		elif request == 'image_properties':
			cli.settimeout(0.1)
			data = cli.recv(BUFSIZE);
			if data == '':
				return
			data = data.split(' ')
			return [int(float(data[1])) , int(float(data[3])), float(data[5])]


		elif request == 'states':

			cli.settimeout(0.2)
			
			while True:
				try:
					self.received_data = self.received_data + cli.recv(BUFSIZE)

				except socket_error as e:
					if e.errno == errno.ECONNRESET:
						self.CLOSE = True
						return
				
				if self.received_data.find('\n') != -1:
					data = self.received_data.split('\n')
					self.received_data = '\n'.join(data[1:])
					data = data[0]
					break


			if self.activate_states_from_world:
				self.bodies_list = self.parse_bodies_xml(data,self.bodies_list,self.activate_states_from_mocap)
				# print "self.bodies_list = " + str(self.bodies_list) 
			else:
				self.bodies_list = []

			if self.activate_states_from_mocap and not self.only_simulated_vehicles:
				try:
			 		mocap_bodies_list = self.Qs.get_updated_bodies()
			 	except:
			 		print "self.Qs: " + str(self.Qs)
			 		mocap_bodies_list = []
			 	# try:
			 	# 	self.bodies_list = self.bodies_list + mocap_bodies_list
			 	# except:
			 	# 	self.bodies_list = mocap_bodies_list
			 	# 	print "RAN THIS"


		elif request == 'highlight':
			data = cli.recv(BUFSIZE);
			if data.strip('\n').strip('\r').lower() == 'port':
				cli.send(str(extra_info))

	def states_handler(self,states_cli):

		global global_variables

		while not self.CLOSE:
			self.send_request(states_cli,'STATES')

		self.send_request(states_cli,'CLOSE')
		try:
			states_cli.close()
		except:
			return

	def highlight_handler(self,highlight_cli):

		HOST = ''
		ADDR = (HOST,0)
		BUFSIZE = 16096

		self.highlight_server = socket( AF_INET,SOCK_STREAM)
		self.highlight_server.setsockopt(SOL_SOCKET,SO_REUSEADDR, 1)
		self.highlight_server.bind((ADDR))

		HIGHLIGHT_PORT = self.highlight_server.getsockname()[1]

		self.timed_print('Waiting for highlight requests in port: '+str(HIGHLIGHT_PORT),parent = 'PROJECTOR')

		self.send_request(highlight_cli,'HIGHLIGHT',HIGHLIGHT_PORT)

		self.highlight_server.listen(1)

		self.highlight_server,addr = self.highlight_server.accept()

		highlight_cli.close()

		self.highlight_server.settimeout(1)

		self.highlight_data = ''

		while not self.CLOSE:

			while not self.CLOSE:
				try:
					self.highlight_data = self.highlight_data + self.highlight_server.recv(BUFSIZE)
				except timeout:
					continue

				if self.highlight_data.find('\n') != -1:
					data = self.highlight_data.split('\n')
					self.highlight_data = '\n'.join(data[1:])
					data = data[0].strip('\n').strip('\r').lower()
					break

			if data == 'vehicle_highlight':
				self.highlight_server.send('ID')
				self.VEHICLE_HIGHLIGHT_ID = int(self.highlight_server.recv(BUFSIZE))
				#self.new_message'] = 'Truck ('+str(self.VEHICLE_HIGHLIGHT_ID'])+') selected'

			elif data == 'message':
				self.highlight_server.send('OK')
				#self.new_message'] = self.highlight_server'].recv(BUFSIZE)

				message = self.highlight_server.recv(BUFSIZE)

				message = ET.fromstring(message)

				if message.get('type') == 'task_completed':
					if int(message.get('body_id')) == self.rrt_body_id:
						self.draw_rrt = False
						self.rrt_time = 0


			elif data == 'update_settings':

				self.highlight_server.send('OK')

				number_of_settings = int(self.highlight_server.recv(BUFSIZE))

				self.highlight_server.send('OK')

				previous_mocap = self.activate_states_from_mocap

				for i in xrange(number_of_settings):
					try:
						label = self.highlight_server.recv(BUFSIZE)
					except:
						continue
					self.highlight_server.send('OK')

					data = self.highlight_server.recv(BUFSIZE)

					if label == 'projector_area':
						setattr(self, label, [float(ele) for ele in data[1:-1].split(',')])
						#convx = float(self.image_size'][0])/float(self.projector_area'][1]-self.projector_area'][0])
						#convy = float(self.image_size'][1])/float(self.projector_area'][3]-self.projector_area'][2])
					elif label in ['v2v_method','v2v_range','v2v_angle']:
						setattr(self, label, str(data))
					elif label == 'projector_resolution':

						self.image_size = tuple([int(ele) for ele in data[1:-1].split(',')])

						print "self.image_size: " + str(self.image_size)

						# self.window = pygame.display.set_mode(self.image_size,pygame.RESIZABLE)
						# self.sim_origin = (image_properties[0],image_properties[1])
						# self.sim_conv = image_properties[2]*self.conv_sml_2_real

						# conv = float(self.image_size[0])/float(self.projector_area[1]-self.projector_area[0])
						
						# [self.sim_conv,self.sim_origin,self.bg_projector] = self.scale_image(bg,convx,convy,self.sim_origin,self.sim_conv,self.projector_area)

						# # pygame.surfarray.blit_array(self.window,self.bg_projector)



					else:
						setattr(self, label, data == 'True')


				if previous_mocap != self.activate_states_from_mocap:
					if self.activate_states_from_mocap:
						self.Qs = Mocap(info=0)

			elif data == 'rrt':
				self.highlight_server.send('OK')

				rrt_string = ''

				while not self.CLOSE:
					try:
						rrt_string = rrt_string + self.highlight_server.recv(BUFSIZE)
					except socket_error as e:
						if e.errno == errno.ECONNRESET:
							self.timed_print('Highlight disconnected','WARNING',parent = 'PROJECTOR')
							self.highlight_server.close()
							return
				
					if rrt_string.find('\n') != -1:
						data = rrt_string.split('\n')
						self.highlight_data = '\n'.join(data[1:])
						rrt_string = data[0]
						break

				parsed_msg = self.parse_rrt_xml(rrt_string)

				if parsed_msg != None:
					self.rrt_tree_nodes = parsed_msg[0]
					self.rrt_solution_nodes =  parsed_msg[1]
					self.rrt_body_id = parsed_msg[2]
					self.draw_rrt = True
					self.rrt_time = 0
				else:
					self.draw_rrt = False

			elif data == 'lanelet_obstruction':
				self.highlight_server.send('OK')

				obstruction_string = ''

				while not self.CLOSE:
					try:
						obstruction_string = obstruction_string + self.highlight_server.recv(BUFSIZE)
					except socket_error as e:
						if e.errno == errno.ECONNRESET:
							self.timed_print('Highlight disconnected','WARNING',parent = 'PROJECTOR')
							self.highlight_server.close()
							return
				
					if obstruction_string.find('\n') != -1:
						data = obstruction_string.split('\n')
						self.highlight_data = '\n'.join(data[1:])
						obstruction_string = data[0]
						break

				self.lanelet_obtruction = self.parse_lanelet_obstruction_xml(obstruction_string,self.lanelet_obtruction)

			else:
				self.timed_print('Not a valid highlight request: ','WARNING',parent = 'PROJECTOR')

		self.highlight_server.close()

	def get_center_font(self,image_size,font,text):

		font_dimensions =  font.size(text)
		center = [int(image_size[0]/2), int(image_size[1]/2)]
	 	center[0] -= int(font_dimensions[0]/2)
	 	center[1] -= int(font_dimensions[1]/2)
	 	center = tuple(center)

	 	return center

	def wait_screen_handler(self):
		
		font_size_initial = 30
		font_size_max = 50

		text = "SML WORLD"

		font_type = 'comicsansms'

		background_color = (0,0,0)

		steel = pygame.image.load(self.image_folder+"/steel.jpg")

		step = 1
		font_size = font_size_initial

		while not self.CLOSE and self.wait:

			for event in pygame.event.get():
				if event.type == pygame.QUIT: 
					self.CLOSE = True
					self.wait = False
					pygame.quit()
					return
					#sys.exit(0) 
				elif event.type == pygame.VIDEORESIZE:
					(width, height) = event.size

					if width > self.image_size[0]:
						width = self.image_size[0]
					if height > self.image_size[1]:
						height = self.image_size[1]
	 
					new_size = (width,height)

					self.image_size = new_size

			font_sml = pygame.font.SysFont(font_type,font_size)

			rendered_font = font_sml.render(text,True,(255, 255, 255))

			center = self.get_center_font(self.image_size,font_sml,text)
			center = (center[0],center[1]-200)

	 		#self.window.fill(background_color)
	 		self.window.blit(steel,(0,0))
			self.window.blit(rendered_font,center)


			font_size = font_size + step

			if font_size > font_size_max:
				step = -1
			elif font_size < font_size_initial:
				step = 1

			font_warning = pygame.font.SysFont(font_type,20)
			warningtext = "Waiting for command central to connect"
			rendered_font = font_warning.render(warningtext,True,(255, 255, 255))
			center = self.get_center_font(self.image_size,font_warning,warningtext)
			center = (center[0],self.image_size[1] - 150 )
			self.window.blit(rendered_font,center)

			pygame.display.update()

			time.sleep(0.03)

	def scale_image(self,bg,convx,convy,sim_origin,sim_conv,area):

		original_size = bg.get_size()

		print "convx/sim_conv: " + str(convx/sim_conv)
		print "convy/sim_conv: " + str(convy/sim_conv)

		bg = pygame.transform.rotozoom(bg,0,convy/sim_conv)
		
		new_size = bg.get_size()

		new_sim_x = int(round(sim_origin[0] + (new_size[0] - original_size[0])/2.))
		new_sim_y = int(round(sim_origin[1] + (new_size[1] - original_size[1])/2.))

		sim_origin = (new_sim_x,new_sim_y)

		sim_conv=[convx,convy]

		#print 'bg new size ' + str(new_size)

		#print 'sim_origin ' + str(sim_origin)

		bg_array = self.getPixelArray(bg)

		#area_px = conv_real_2_projector(area,'conv')
		projector_area = [area[0]*convx+sim_origin[0],area[1]*convx+sim_origin[0],-area[3]*convy+sim_origin[1],-area[2]*convy+sim_origin[1]]
		projector_area = [int(ele) for ele in projector_area]
		#print 'projector area ' + str(projector_area)
	 	bg_array_cropped = bg_array[projector_area[0]:projector_area[1],projector_area[2]:projector_area[3], :]
		bg_array_cropped = bg_array_cropped[0:self.image_size[0],0:self.image_size[1],:]
		bg_array = bg_array_cropped
		
		bg = bg_array

		#print 'new sim size ' + str(len(bg_array)) + ' ' + str(len(bg_array[1,:])) 

		new_sim_x = sim_origin[0] - projector_area[0]
		new_sim_y = sim_origin[1] - projector_area[2]

		sim_origin = (new_sim_x,new_sim_y)

		#print 'new sim origin ' + str(sim_origin)

		return [sim_conv,sim_origin,bg]

	def load_car_images(self,car_list,sim_conv):

		car_dimensions = (0.15*1*(1.0/0.6),0.066*1)

		x_px = round(sim_conv[0]*car_dimensions[0])
		y_px = round(sim_conv[1]*car_dimensions[1])
		car_size = (int(x_px),int(y_px))	

		car_images = []

		for car_ele in car_list:

			car = pygame.image.load(os.path.join(self.image_folder,car_ele + ".png"))
			car = pygame.transform.smoothscale(car,car_size)

			car_images.append(car)

		self.images['simulator_vehicle'] = pygame.image.load(os.path.join(self.image_folder,"carUnityOffset.png"))
		self.images['simulator_vehicle'] = pygame.transform.smoothscale(self.images['simulator_vehicle'],car_size)

		return car_images

	def load_bus_image(self,sim_conv):
		# Based on load_car_images
		car_dimensions = (0.30*1*(2.0/1.2),0.095*1)

		x_px = round(sim_conv[0]*car_dimensions[0])
		y_px = round(sim_conv[1]*car_dimensions[1])
		bus_size = (int(x_px),int(y_px))	

		bus_image = pygame.image.load(os.path.join(self.image_folder,"busOffset.png"))
		bus_image = pygame.transform.smoothscale(bus_image,bus_size)

		return bus_image

	def load_smart_vehicle_image(self,sim_conv):
		# Based on load_car_images
		car_dimensions = (0.15*1*(1.0/0.6),0.066*1)

		x_px = round(sim_conv[0]*car_dimensions[0])
		y_px = round(sim_conv[1]*car_dimensions[1])
		car_size = (int(x_px),int(y_px))	

		smart_vehicle_image = pygame.image.load(os.path.join(self.image_folder,"carSmartOffset.png"))
		smart_vehicle_image = pygame.transform.smoothscale(smart_vehicle_image,car_size)

		return smart_vehicle_image

	def start_projector(self):

		# if image_folder == os.path.join('..','resources'):
		# 	[self.rrt_tree_nodes'],self.rrt_solution_nodes']] = parse_rrt_xml(open('../rrt_solution_message_example.xml').read())
		# else:
		# 	[self.rrt_tree_nodes'],self.rrt_solution_nodes']] = parse_rrt_xml(open('rrt_solution_message_example.xml').read())


		self.new_message = None

		if self.only_real_vehicles and self.only_simulated_vehicles:
			self.timed_print('Both only_simulated_vehicles ans only_real_vehicles options selected','FAIL',parent = 'PROJECTOR')
			sys.exit(1)
		elif self.only_simulated_vehicles and not self.activate_states_from_world:
			self.timed_print('To adquire simulated vehicles you must activate the option activate_states_from_world','FAIL',parent = 'PROJECTOR')
			sys.exit(1)	

		if self.activate_states_from_mocap:
			self.Qs = Mocap(info=0)

		self.timed_print('Starting Projector Manager','OKGREEN',parent = 'PROJECTOR')

		[cli,self.wait] = self.start_projector_client('projector-main')

		if cli == 0:
			self.timed_print('Problem connecting to SML World ('+self.info['projector_host']+','+str(self.info['projector_port'])+')','FAIL',parent = 'PROJECTOR')
			return

		window_x = 0
		window_y = 0
		os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (window_x,window_y)

		pygame.init()
		pygame.font.init()

		font = pygame.font.SysFont('monospace',25)
		font.set_bold(True)

		self.ids_font = pygame.font.SysFont('monospace',25)
		self.ids_font.set_bold(True)


		self.window = pygame.display.set_mode(self.image_size,pygame.RESIZABLE | pygame.NOFRAME )

		if self.wait:
			self.start_thread(self.wait_screen_handler,args=())
			try:
	 			data = cli.recv(self.BUFSIZE);
	 		except:
	 			self.CLOSE = True
				return

			if data == 'command _central on':
				self.wait = False
			else:
				self.CLOSE = True
				return


		self.timed_print('Request for Image Properties',parent = 'PROJECTOR')
		image_properties = self.send_request(cli,'IMAGE_PROPERTIES')
		if image_properties == None:
			return

		self.timed_print('Image Properties Received',parent = 'PROJECTOR')

		self.timed_print('Request for Background Image',parent = 'PROJECTOR')
		self.send_request(cli,'BACKGROUND')
		self.timed_print('Background Image Received',parent = 'PROJECTOR')

	 
	 	self.timed_print('Listening to highlights requests from SML World','OKBLUE',parent = 'PROJECTOR')
		highlight_cli = self.start_projector_client('projector-highlights')
		self.start_thread(self.highlight_handler,args=[highlight_cli])


		if self.activate_states_from_world or (self.only_real_vehicles and not self.activate_states_from_mocap):
			self.timed_print('Starting adquiring states from SML World','OKBLUE',parent = 'PROJECTOR')
			states_cli = self.start_projector_client('projector-states')
			self.start_thread(self.states_handler,args=[states_cli])

		self.conv_real_2_projector('init')

		convx = float(self.image_size[0])/float(self.projector_area[1]-self.projector_area[0])
		convy = float(self.image_size[1])/float(self.projector_area[3]-self.projector_area[2])

		self.sim_origin = (image_properties[0],image_properties[1])

		self.sim_conv = image_properties[2]*self.conv_sml_2_real

		current_time = time.time()

		if self.image_folder == os.path.join('..','resources'):
			bg = pygame.image.load("background.bmp")
		else:
			bg = pygame.image.load(os.path.join("Projector","background.bmp"))

		[self.sim_conv,self.sim_origin,self.bg_projector] = self.scale_image(bg,convx,convy,self.sim_origin,self.sim_conv,self.projector_area)

		# car_list = ['carWhite','carRed','carBlue','carGreen','carYellow']
		car_list = ['carWhiteOffset','carRedOffset','carBlueOffset','carGreenOffset','carYellowOffset']
		
		self.images = dict()

		self.images['cars'] = self.load_car_images(car_list,self.sim_conv)
		self.images['bus'] = self.load_bus_image(self.sim_conv)
		self.images['smart_vehicle'] = self.load_smart_vehicle_image(self.sim_conv)
		self.images['excavator'] = pygame.image.load(os.path.join(self.image_folder,"excavator.png"))
		self.images['construction'] = pygame.image.load(os.path.join(self.image_folder,"construction.png"))
		self.images['water'] = pygame.image.load(os.path.join(self.image_folder,"water.png"))
		self.images['crosshairs'] = pygame.image.load(os.path.join(self.image_folder,"crosshairs.png"))
		self.images['signal_loss'] = pygame.image.load(os.path.join(self.image_folder,"signal_loss.png"))
		self.images['antenna'] = pygame.image.load(os.path.join(self.image_folder,"antenna.png"))
		self.images['quadcopter'] = pygame.image.load(os.path.join(self.image_folder,"quadcopter.png"))

		for i in xrange(6):
			self.images['signal_'+str(i)] =  pygame.image.load(os.path.join(self.image_folder,'signal_'+str(i)+'.png'))


		car_dimensions = (0.45*1,0.35*1)
		x_px = round(self.sim_conv[0]*car_dimensions[0])
		y_px = round(self.sim_conv[1]*car_dimensions[1])
		car_size = (int(x_px),int(y_px))

		self.images['excavator'] = pygame.transform.smoothscale(self.images['excavator'],car_size)

		self.images['construction'] = pygame.transform.smoothscale(self.images['construction'],car_size)
		self.images['signal_loss'] = pygame.transform.smoothscale(self.images['signal_loss'],car_size)

		self.images['quadcopter'] = pygame.transform.rotate(self.images['quadcopter'],90)
		#self.images['quadcopter'] = pygame.transform.smoothscale(self.images['quadcopter'],car_size)

		self.images['antenna'] = pygame.transform.smoothscale(self.images['antenna'],car_size)
		self.images['antenna'] = pygame.transform.rotate(self.images['antenna'],180)

		for i in xrange(6):
			self.images['signal_'+str(i)] = pygame.transform.smoothscale(self.images['signal_'+str(i)],car_size)


		#####self.window.blit(self.bg_projector,(0,0))
		pygame.surfarray.blit_array(self.window,self.bg_projector)

		self.rrt_time = 0

		while not self.CLOSE: 
			tic = time.time()
			
			for event in pygame.event.get():
				if event.type == pygame.QUIT: 
					#print event
					self.send_request(cli,'CLOSE')
					self.send_request(states_cli,'CLOSE')
					self.CLOSE = True
					cli.close()
					highlight_cli.close()
					self.highlight_server.close()
					pygame.quit()
					return
					#sys.exit(0)
				elif event.type == pygame.VIDEORESIZE:
					(width, height) = event.size

					print "event.size: " + str(event.size)

					if width > self.image_size[0]:
						width = self.image_size[0]
					if height > self.image_size[1]:
						height = self.image_size[1]
	 
					new_size = (width,height)


					self.window = pygame.display.set_mode(self.image_size,pygame.RESIZABLE)
					#print "new_size: " + str(new_size)
					self.image_size = new_size 
					self.sim_origin = (image_properties[0],image_properties[1])
					self.sim_conv = image_properties[2]*self.conv_sml_2_real

					conv = float(self.image_size[0])/float(self.projector_area[1]-self.projector_area[0])
					
					[self.sim_conv,self.sim_origin,self.bg_projector] = self.scale_image(bg,convx,convy,self.sim_origin,self.sim_conv,self.projector_area)

					pygame.surfarray.blit_array(self.window,self.bg_projector)

				else:
					pass
			

			if (self.activate_states_from_mocap and not self.activate_states_from_world) or (self.only_real_vehicles and self.activate_states_from_mocap):
			 	try:
			 		self.bodies_list = self.Qs.get_updated_bodies()
			 	except:
			 		self.bodies_list

			pygame.surfarray.blit_array(self.window, self.bg_projector)

			
			if self.draw_sensors:
				self.draw_sensors_fnc(self.bodies_list, self.sim_conv, self.sim_origin)

			if self.draw_rrt:
				if self.rrt_time == 0:
					self.rrt_time = time.time()
				self.draw_rrt_fnc( self.sim_conv, self.sim_origin)

			self.draw_obstructed_lanelet(self.sim_origin,self.sim_conv)

			self.draw_bodies(self.bodies_list,self.sim_origin,self.sim_conv)

			self.draw_message(font,self.new_message)

			if self.activate_fps:
				background_color = (80, 163, 77)
				current_time = self.update_fps(current_time,font,background_color)

			self.window.blit(self.images['antenna'],(int(self.image_size[0]/2),int(self.image_size[1])-50))	

			pygame.display.flip()
	 
		try:
			self.send_request(cli,'CLOSE')
		except:
			pass

	def stop_projector(self):
		self.CLOSE = True
		time.sleep(1)
		pygame.quit()

def run(projector_info,image_folder):
		projector = Projector(projector_info,image_folder)
		projector.start_projector()

if __name__ == "__main__":

	image_folder =  os.path.join('..','resources') 

	projector_info = ('130.237.43.135',8000)
	#projector_info = ('130.237.50.246',8000)

	run(projector_info,image_folder)