import math
import smartvehicle

class v2vManager:
	"This manages what each vehicle is currently sensing"


	def __init__(self, bodies_array):

		self.bodies_array = bodies_array

	def network_step(self):
		# The network step will simulate a communications network
		# Basically the output of every car, given by v2v_network_output, will
		# be the input of every other car (v2v_network_input)
		# At the end of the step, every car's v2v_network_output is emptied


		# I will get all of the messages that are being outputted by vehicles
		current_network_inputs = []

		for body_id in self.bodies_array:

			body = self.bodies_array[body_id]

			if not isinstance(body, smartvehicle.SmartVehicle) and body_id != -100:

				continue

			if body.v2v_network_output:

				current_network_inputs.extend(body.v2v_network_output)

		# I will put every message in every vehicle input
		# I shall also empty the v2v_network_output of every car
		for body_id in self.bodies_array:

			if isinstance(self.bodies_array[body_id], smartvehicle.SmartVehicle) or body_id == -100:


				self.bodies_array[body_id].v2v_network_input = current_network_inputs[:]

				del self.bodies_array[body_id].v2v_network_output[:]


		return

	def wifi_step(self, v2v_range):
		# The wifi step will simulate a communications network
		# Basically the output of every car, given by v2v_wifi_output, will
		# be the input of every other car in a close vicinity (v2v_range)
		# At the end of the step, every car's v2v_wifi_output is emptied

		current_body_ids = self.bodies_array.keys()
		# Need to avoid dictionary size changes during loop

		for ego_body_id in current_body_ids:

			if ego_body_id in self.bodies_array:
			# Need to check if dictionary still has the item

				ego_body = self.bodies_array[ego_body_id]

				if isinstance(ego_body, smartvehicle.SmartVehicle):

					current_wifi_inputs = []

					ego_body = ego_body

					for other_body_id in current_body_ids:

						if other_body_id in self.bodies_array:
						# Need to check if dictionary still has the item
							other_body = self.bodies_array[other_body_id]

							if ego_body_id == other_body_id or not isinstance(other_body, smartvehicle.SmartVehicle):

								continue

							distance = math.hypot(ego_body.x - other_body.x, ego_body.y - other_body.y) 

							if distance > v2v_range:

								continue

							if other_body.v2v_wifi_output:

								current_wifi_inputs.extend(other_body.v2v_wifi_output)

					ego_body.v2v_wifi_input = current_wifi_inputs

		# I need to empty the v2v_wifi_output
		for body_id in current_body_ids:

			if body_id in self.bodies_array:

				current_body = self.bodies_array[body_id]

				if isinstance(current_body, smartvehicle.SmartVehicle):

					del current_body.v2v_wifi_output[:]

		return


