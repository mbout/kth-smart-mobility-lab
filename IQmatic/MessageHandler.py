import json, time, math
import threading, Queue
import collections
from message_protocols import protoconst

from message_protocols.command_protocols import *
from message_protocols.misc_protocols import *
from message_protocols.platoon_merge_protocols import *
from message_protocols.rear_merge_protocols import *


class MessageHandler:

	def __init__(self, vehicle):
		self.vehicle = vehicle

		self.decoder = json.JSONDecoder()
		self.encoder = json.JSONEncoder()
		
		# Below are variables used for targeted communications


		print "initializing events"
		self.incoming_message = threading.Event()

		print "initializing message queues"
		self.in_queue = collections.deque()

		print "initializing messageing threads"
		self.done = False
		self.in_thread = threading.Thread(target = self.incomingThread)
		self.in_thread.daemon = True
		self.in_thread.start()


		# TODO import message_protocols
		# add protocol handlers here
		self.protocolHandlers = {protoconst.DES_MERGE: desired_merge_p.DesiredMergeP(vehicle), 
			protoconst.REQ_MERGE: request_merge_p.RequestMergeP(vehicle), 
			protoconst.STOM: STOM_p.STOMP(vehicle), 
			protoconst.MERGE_COMPLETE: merge_complete_p.MergeCompleteP(vehicle), 
			protoconst.NEW_VEL: NewVelocity.NewVelocityP(vehicle),
			protoconst.DES_PLAT_MERGE: desired_platoon_merge.DesiredPlatoonMergeP(vehicle),
			protoconst.MERGE_PREP_COMP: merge_prep_comparison_p.MergePrepComparisonP(vehicle),
			protoconst.HOLD_TO_MERGE: hold_to_merge_p.HoldToMergeP(vehicle),
			protoconst.MERGE_PREP_DONE: merge_prep_done_p.MergePrepDoneP(vehicle),
			protoconst.CLEAR_FOR_MERGE: clear_for_merge_p.ClearForMergeP(vehicle),
			protoconst.REQ_PLAT_MERGE: request_platoon_merge_p.RequestPlatoonMergeP(vehicle),
			protoconst.PLAT_STOM: platoon_STOM_p.PlatoonSTOMP(vehicle),
			protoconst.SPLIT_PLAT: split_platoon_p.SplitPlatoonP(vehicle),
			protoconst.PLAT_MERGE_DONE: platoon_merge_done_p.PlatoonMergeDoneP(vehicle),
			protoconst.LANE_CHANGE: lane_change_p.LaneChangeP(vehicle),
			protoconst.FWD_PARTNER_INFO: forward_partner_info_p.ForwardPartnerInfoP(vehicle),
			protoconst.REQ_MER_LEN_INFO: request_merge_length_info_p.RequestMergeLengthInfoP(vehicle),
			protoconst.MER_LEN_INFO: merge_length_info_p.MergeLengthInfoP(vehicle),
			protoconst.MER_LEN_COUNT_COMPL: merge_length_count_completed_p.MergeLengthCountCompletedP(vehicle),
			protoconst.DES_UNSP_MER: desired_unspecified_merge_p.DesiredUnspecifiedMergeP(vehicle),
			protoconst.NEW_VEL_FWD: NewVelocityForward.NewVelocityFwdP(vehicle),
			protoconst.ABORT_PLAT_MER: abort_platoon_merge_p.AbortPlatoonMergeP(vehicle)}


	def generateMessage(self):

		message_dict = dict()
		message_dict['time'] = time.time()
		message_dict['message_id'] = hash(message_dict['time'])
		message_dict['vehicle_id'] = self.vehicle.id
		message_dict['vehicle_length'] = self.vehicle.length
		message_dict['vehicle_width'] = self.vehicle.width
		message_dict['x'] = self.vehicle.bodies_array[self.vehicle.id].x
		message_dict['y'] = self.vehicle.bodies_array[self.vehicle.id].y
		message_dict['velocity'] = self.vehicle.bodies_array[self.vehicle.id].commands['throttle']
		message_dict['yaw_rate'] = 0.
		message_dict['desired_lane'] = self.vehicle.desired_lane
		message_dict['merge_request_flag'] = self.vehicle.merge_request_flag
		message_dict['STOM_flag'] = self.vehicle.STOM_flag
		message_dict['merging_flag'] = self.vehicle.merging_flag
		message_dict['platoon_id'] = 0
		message_dict['desired_merge_flag'] = self.vehicle.desired_merge_flag
		message_dict['merge_accept_flag'] = self.vehicle.merge_accept_flag
		message_dict['fwd_pair_partner'] = self.vehicle.fwd_pair_partner
		message_dict['bwd_pair_partner'] = self.vehicle.bwd_pair_partner
		message_dict['new_bwd_pair_partner'] = self.vehicle.new_bwd_pair_partner
		message_dict['new_fwd_pair_partner'] = self.vehicle.new_fwd_pair_partner
		message_dict['platooned_vehicle'] = self.vehicle.platooned_vehicle

		# message_string = self.encoder.encode(message_dict)

		return message_dict
		# return message_string



	def decodeMessage(self, json_string):
		message_dict = self.decoder.decode(json_string)

		return message_dict




	# Here we have targeted communications over network or wifi


	def sendMessage(self, target_id, message):


		# if you want to have a delay for the message
		# time.sleep(0)


		self.vehicle.bodies_array[target_id].messenger.receiveMessage(message)


	def receiveMessage(self, message):
		"""TODO: Docstring for receiveMessage.
		:returns: TODO

		"""

		#response = self.handleNetworkMessage(message_dict)

		#if self.response.respond:
		#	self.sendResponse(response)

		self.in_queue.append(message)
		self.incoming_message.set()

	def handleNetworkMessageTest(self, message_dict):
		""" Test for the messenger, just prints the received message dictionary

		:message_dict: dictionary containing the message data
		:returns: void

		"""
		
		print message_dict
		if message_dict['desired_merge_flag']:
			response = Response(message_dict['vehicle_id'], desired_merge_flag = False, merge_accept_flag = True, EOC = True)
			self.sendResponse(response)
	
	def incomingThread(self):
		
		while not self.done:
			self.incoming_message.wait()

			# all already checked messages, for only communicating with one at a time
			# could be improved

			while len(self.in_queue) > 0:
				try:
					# takes the oldest message, or maybe not?
					message = self.in_queue.popleft()
					# checks if the message has already been checked
					# TODO what happens if external source adds message in loop, since we readd it? should maybe check last entry
					# in queue if communication is initiated to minimize performance loss, and synchronize the list

					response_message = self.protocolHandlers[message.protocol_id].handleMessage(message)
					#print "receiving message in: ", self.vehicle.id, "message:", message.protocol_id
					if response_message != None:
						self.sendMessage(message.vehicle_id, response_message)
					
				except ValueError:
					self.done = True
					print "Incoming message thread shutting down because of error for id: ", self.vehicle.id
					break

			self.incoming_message.clear()
			
	# Below is message protocol starters

	def sendDesiredMerge(self, target_id):
		print self.vehicle.id, "sending desired merge"
		message = self.protocolHandlers[protoconst.DES_MERGE].sendDesiredMerge(target_id)
		self.sendMessage(target_id, message)
		return True

	def sendMergeRequest(self, target_id):
		message = self.protocolHandlers[protoconst.REQ_MERGE].sendRequestMerge(target_id)
		self.sendMessage(target_id, message)

	def sendSTOM(self, target_id):
		message = self.protocolHandlers[protoconst.STOM].sendSTOM(target_id)
		self.sendMessage(target_id, message)
		
	def sendMergeComplete(self, target_id):
		message = self.protocolHandlers[protoconst.MERGE_COMPLETE].sendMergeComplete(target_id)
		self.sendMessage(target_id, message)

	def sendNewVelocity(self, target_id, new_velocity):
		message = self.protocolHandlers[protoconst.NEW_VEL].getNewVelocityMessage(target_id, new_velocity)
		self.sendMessage(target_id, message)

	def sendDesiredPlatoonMerge(self, target_id):
		message = self.protocolHandlers[protoconst.DES_PLAT_MERGE].sendFirstMessage(target_id)
		self.sendMessage(target_id, message)

	def sendDoMergePrepComp(self, target_id, merge_comparison_id, in_merging_platoon):
		message = self.protocolHandlers[protoconst.MERGE_PREP_COMP].sendFirstMessage(target_id, merge_comparison_id, in_merging_platoon)
		self.sendMessage(target_id, message)

	def sendHoldToMerge(self, target_id, in_merging_platoon):
		message = self.protocolHandlers[protoconst.HOLD_TO_MERGE].sendFirstMessage(target_id, in_merging_platoon)
		self.sendMessage(target_id, message)

	def sendMergePrepDone(self, target_id, STOM_partner):
		message = self.protocolHandlers[protoconst.MERGE_PREP_DONE].sendFirstMessage(target_id, STOM_partner)
		self.sendMessage(target_id, message)

	def sendClearForMerge(self, target_id):
		message = self.protocolHandlers[protoconst.CLEAR_FOR_MERGE].sendFirstMessage(target_id)
		self.sendMessage(target_id, message)

	def sendPlatoonMergeRequest(self, target_id):
		message = self.protocolHandlers[protoconst.REQ_PLAT_MERGE].sendFirstMessage(target_id)
		self.sendMessage(target_id, message)

	def sendPlatoonSTOM(self, target_id, lane_string):
		message = self.protocolHandlers[protoconst.PLAT_STOM].sendFirstMessage(target_id, lane_string)
		self.sendMessage(target_id, message)

	def sendSplitPlat(self, target_id):
		message = self.protocolHandlers[protoconst.SPLIT_PLAT].sendFirstMessage(target_id)
		self.sendMessage(target_id, message)

	def sendPlatoonMergeDone(self, target_id, forward):
		message = self.protocolHandlers[protoconst.PLAT_MERGE_DONE].sendFirstMessage(target_id, forward)
		self.sendMessage(target_id, message)

	def sendLaneChange(self, target_id, new_lane):
		message = self.protocolHandlers[protoconst.LANE_CHANGE].sendLaneChange(target_id, new_lane)
		self.sendMessage(target_id, message)

	def sendForwardPartnerInfo(self, target_id, extra_info = {}):
		# extra_info is a dictionary of extra info you want to send that is not included in the standard info
		message = self.protocolHandlers[protoconst.FWD_PARTNER_INFO].sendForwardPartnerInfo(target_id, extra_info)
		self.sendMessage(target_id, message)

	def sendRequestMergeLengthInfo(self, target_id, leader_id):
		message = self.protocolHandlers[protoconst.REQ_MER_LEN_INFO].sendRequestMergeLengthInfo(target_id, leader_id)
		self.sendMessage(target_id, message)

	def sendMergeLengthInfo(self, target_id, in_merging_platoon_info):
		message = self.protocolHandlers[protoconst.MER_LEN_INFO].sendMergeLengthInfo(target_id, in_merging_platoon_info)
		self.sendMessage(target_id, message)

	def sendMergeLengthCountCompleted(self, target_id, merge_length_count_completed):
		message = self.protocolHandlers[protoconst.MER_LEN_COUNT_COMPL].sendMergeLengthCountCompleted(target_id, merge_length_count_completed)
		self.sendMessage(target_id, message)

	def sendDesiredUnspecifiedMerge(self, target_id):
		message = self.protocolHandlers[protoconst.DES_UNSP_MER].sendDesiredUnspecifiedMerge(target_id)
		self.sendMessage(target_id, message)

	def sendNewVelocityFwd(self, target_id, new_velocity):
		message = self.protocolHandlers[protoconst.NEW_VEL_FWD].sendNewVelocityFwd(target_id, new_velocity)
		self.sendMessage(target_id, message)

	def sendAbortPlatoonMerge(self, target_id, fwd = False, bwd = False):
		message = self.protocolHandlers[protoconst.ABORT_PLAT_MER].sendAbortPlatoonMerge(target_id, fwd, bwd)
		self.sendMessage(target_id, message)