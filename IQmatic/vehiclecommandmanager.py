#!/usr/bin/python

#change at line 393
import random

import datetime, time, threading
from socket import *

import urllib
import re

from socket import error as socketerror
from socket import herror as socketherror
from socket import gaierror as socketerror
from socket import timeout as sockettimeout

import xmlcomms
import dummytrafficmanager
import platooningmanager
import intersectionmanager

class VehicleCommandManager(object):
	"""Projector"""

	def __init__(self, bodies_array, vehicles_rate, buffersize, lanelet_module, vehicles_desired_rate):
		super(VehicleCommandManager, self).__init__()

		self.colors = self.get_color

		self.buffersize = buffersize
		self.vehicles_rate = vehicles_rate
		self.vehicle_output_rate = 50

		self.vehicle_commands = dict()
		self.bodies_list = []
		self.close = False

		self.dummy_traffic_manager = []

		self.bodies_readings = []

		self.initial_states = dict()

		self.vehicles_desired_rate = vehicles_desired_rate

		self.dummy_traffic_manager = dummytrafficmanager.DummyTrafficManager(bodies_array, lanelet_module.osm_info, vehicles_desired_rate)

		self.platooning_vehicles_manager = platooningmanager.PlatooningManager(bodies_array, lanelet_module.osm_info, vehicles_desired_rate)

		self.intersection_vehicles_manager = intersectionmanager.IntersectionManager(bodies_array, lanelet_module.osm_info, vehicles_desired_rate)

		# This is the bodies array, sent from world_class.py
		# It gives us access to all of the current bodies
		# Chaning this dictionary, will produce changes in the
		# bodies array in the world class
		self.bodies_array = bodies_array

	@property
	def get_color(self):
		colors = dict()
		colors['HEADER'] = '\033[95m'
		colors['OKBLUE'] = '\033[94m'
		colors['OKGREEN'] = '\033[92m'
		colors['WARNING'] = '\033[93m'
		colors['FAIL'] = '\033[91m'
		colors['ENDC'] = '\033[0m'
		colors['BOLD'] = '\033[1m'
		colors['UNDERLINE'] = '\033[4m'
		colors['WORLD'] = '\033[1;97m'
		colors['PROJECTOR'] = '\033[1;32m'
		colors['VEHICLE'] = '\033[1;95m'
		colors['CMD'] = '\033[1;93m'
		colors['QUALISYS'] = '\033[1;96m'

		return colors

	def timed_print(self, message, color=None, parent=None):
		if color in self.colors:
			color = self.colors[color]
		else:
			color = ''
		if parent in self.colors:
			parent = self.colors[parent]
		else:
			parent = ''
		print parent + self.get_current_time + self.colors['ENDC'] + ' ' + color + message + self.colors['ENDC']

	@property
	def get_current_time(self):
		return datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S ")

	def step_old(self, bodies_list, bodies_readings, vehicles_rate , vehicle_output_rate = None ,simulator_is_closing = False):

		self.bodies_list = bodies_list

		self.bodies_readings = bodies_readings

		self.close = simulator_is_closing

		if vehicle_output_rate is not None:
			self.vehicle_output_rate = vehicle_output_rate


		# If initial_state dictionary has items
		if self.initial_states:
			initial_states_return = self.initial_states
			self.initial_states = dict()
			return [self.vehicle_commands, initial_states_return]

		return self.vehicle_commands

	def step(self, vehicles_rate , vehicle_output_rate = None ,simulator_is_closing = False):

		self.close = simulator_is_closing

		if vehicle_output_rate is not None:
			self.vehicle_output_rate = vehicle_output_rate

		# If initial_state dictionary has items
		if self.initial_states:
			initial_states_return = self.initial_states
			self.initial_states = dict()
			return [self.vehicle_commands, initial_states_return]

		return self.vehicle_commands


	def get_external_ip(self):
		site = urllib.urlopen("http://checkip.dyndns.org/").read()
		grab = re.findall('([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)', site)
		address = grab[0]
		return address

	def real_vehicle_output_handler(self, conn):

		# my_ip_address = gethostbyname(gethostname())
		my_ip_address = self.get_external_ip()

		# Create a UDP socket
		udp_socket = socket(AF_INET, SOCK_DGRAM)

		udp_socket.bind( (my_ip_address, 0) )

		my_port = udp_socket.getsockname()[1]

		time.sleep(1.0)

		data = str(my_ip_address) + '\n'
		conn.send(data)

		print "Sent " + str(data)

		time.sleep(1.0)

		data = str(my_port) + '\n'
		conn.send(data)

		print "Sent " + str(data)

		# Will wait to know the ID I am outputing to
		body_id_message, address = udp_socket.recvfrom(self.buffersize)
		body_id_message = body_id_message.strip('\n').strip('\r').lower()

		print "Received first UDP communication from outputter"
		print "Will close original TCP connection"

		conn.close()

		vehicle_id = xmlcomms.process_body_id_message_string(body_id_message)

		self.timed_print('Connected a new real vehicle outputter with id: ' + str(vehicle_id)  , 'OKGREEN', parent='VEHICLE')

		udp_socket.settimeout(0.01)

		throttle = 0.
		steering = 0.

		while not self.close:

			tic = time.time()

			found_my_vehicle_id = False

			# Will look for the input corresponding to the vehicle I'm outputting to
			for temp_id in self.vehicle_commands.keys():

				# Found it
				if temp_id == vehicle_id:

					commands_to_output = self.vehicle_commands[temp_id]

					throttle = commands_to_output['throttle'] 
					steering = commands_to_output['steering'] 

					found_my_vehicle_id = True

					# Exit the loop
					break

			# Issue warning in case I'm not finding the vehicle I am suppose to output
			if not found_my_vehicle_id:

				print 'Vehicle outputter does not find output for its id: ' + str(vehicle_id)


			# Sorry Pedro for not formatting it in XML
			commands_message = 'throttle:' + str(throttle) + ':steering:' + str(steering) +  ':time:' + str(time.time()) + '\n'

			try:
				
				udp_socket.sendto(commands_message, address)
				print "commands_message: " + str(commands_message)

			except sockettimeout:
				# print "Vehicle outputter socket exception: sockettimeout"
				pass	
			except socketerror:
				print "Vehicle outputter socket exception: socketerror"
				pass

			toc = time.time()

			if toc - tic > 0:
				rate = int(round(1 / (toc - tic)))
				if rate > self.vehicle_output_rate:
					# print "(1./self.vehicle_output_rate) - (1./rate): " + str((1./self.vehicle_output_rate) - (1./rate))
					time.sleep( (1./self.vehicle_output_rate) - (1./rate) )

			elif toc - tic == 0:
				time.sleep( (1./self.vehicle_output_rate))
		

		self.timed_print('Real vehicle outputter with id: ' + str(vehicle_id) + ' has stopped', 'WARNING', parent='VEHICLE')
	
	def vehicle_controller_handler(self,conn,controller_type,vehicle_id = None):

		# I will keep track if the initial state was read by the parent step() function
		# When I know it has been read, I will stop outputting my initial state, and will
		# only output my output_commands
		initial_state_was_read = False

		# Debugging: For measurement of internal Output Manager -> Simulator -> Output Manager time measurement
		# debug_first_moving_time = -1
		# debug_first_movement_time = -1

		if controller_type == 'real_controller':

			body_id_message = conn.recv(self.buffersize).strip('\n').strip('\r').lower()

			vehicle_id = xmlcomms.process_body_id_message_string(body_id_message)

			self.timed_print('Connected new real vehicle with id: ' + str(vehicle_id)  , 'OKGREEN', parent='VEHICLE')

		elif controller_type == 'simulated_controller':
		
			new_id_xml = xmlcomms.get_simulated_body_id_message(vehicle_id)
			print "new_id_xml: " + str(new_id_xml)
			conn.send(new_id_xml + '\n')

			inital_state_xml = conn.recv(self.buffersize)

			print "inital_state_xml: " + str(inital_state_xml)

			initial_state = xmlcomms.process_initial_state_simulated_vehicle_message_string(inital_state_xml)

			# Will start output my initial state to the intiial_states dictionary
			self.initial_states[vehicle_id] = initial_state

			print "initial_state: " + str(initial_state)

			self.timed_print('Creating new simulated vehicle with id: ' + str(vehicle_id)  , 'OKGREEN', parent='VEHICLE')


		conn.settimeout(0.01)


		while not self.close:

			tic = time.time()

			vehicle_state = [body for body in self.bodies_list if body['id'] == vehicle_id]

			# Debugging: For measurement of internal Output Manager -> Simulator -> Output Manager time measurement
			# if vehicle_state:

			# 	if vehicle_state[0]['x'] != 0 and debug_first_movement_time < 0.:

			# 		print "First Movement detected"
			# 		debug_first_movement_time = time.time()
			# 		print "debug_first_moving_time = " + str(debug_first_moving_time) 
			# 		print "debug_first_movement_time = " + str(debug_first_movement_time) 
			# 		print debug_first_movement_time - debug_first_moving_time
			# 		print 1./(debug_first_movement_time - debug_first_moving_time)



			if vehicle_state:

				states_message = []

				if vehicle_id == 16:
					trailer_id = 14
					trailer_state = [body for body in self.bodies_list if body['id'] == trailer_id]

					if trailer_state:

						# print "vehicle_state = " + str(vehicle_state)

						# print "vehicle_state[0]['ts'] = " + str(vehicle_state[0]['ts'])
						# print "trailer_state[0]['ts'] = " + str(trailer_state[0]['ts'])

						states_to_send = [ vehicle_state[0], trailer_state[0] ]
						states_message = xmlcomms.get_vehicle_states_reply_message_string(states_to_send) + '\n'

				if vehicle_id == 10:
					trailer_id = 13
					trailer_state = [body for body in self.bodies_list if body['id'] == trailer_id]

					if trailer_state:

						# print "vehicle_state = " + str(vehicle_state)

						# print "vehicle_state[0]['ts'] = " + str(vehicle_state[0]['ts'])
						# print "trailer_state[0]['ts'] = " + str(trailer_state[0]['ts'])

						states_to_send = [ vehicle_state[0], trailer_state[0] ]
						states_message = xmlcomms.get_vehicle_states_reply_message_string(states_to_send) + '\n'
						
				else:
					states_message = xmlcomms.get_vehicle_states_reply_message_string(vehicle_state) + '\n'
				try:
					conn.send(states_message)
				except:
					pass
					# print "Timeout?"
				# print "states_message: " + str(states_message)
			else:
				conn.send('states_message_empty\n')

			try:
				commands_string = conn.recv(self.buffersize)
				if commands_string.find('\n') != -1:
					data = commands_string.split('\n')
					while '' in data:
						data.remove('')
					commands_string = data[-1]


				if commands_string != '':
					commands = xmlcomms.process_vehicle_commands_message_string(commands_string)
					self.vehicle_commands[vehicle_id] = commands

					print "vehicle_controller_handler: received commands = " + str(commands) + ' for truck ' + str(vehicle_id)

					# Debugging: For measurement of internal Output Manager -> Simulator -> Output Manager time measurement
					# if commands['throttle'] != 0 and debug_first_moving_time < 0.:

					# 	print "First Moving requested"
					# 	debug_first_moving_time = time.time()
					

			except timeout:
				pass
			except error:
				break

			toc = time.time()

			if toc - tic > 0:
				rate = int(round(1 / (toc - tic)))
				if rate > self.vehicles_rate:
					time.sleep(1. / self.vehicles_rate - 1. / rate)						

		if controller_type == 'real_controller':
			self.timed_print('Real vehicle with id: ' + str(vehicle_id) + ' has stopped', 'WARNING', parent='VEHICLE')
		elif controller_type == 'simulated_controller':
			self.timed_print('Simulated vehicle with id: ' + str(vehicle_id) + ' has stopped', 'WARNING', parent='VEHICLE')

	def setup_dummy_traffic_vehicle(self, current_simulated_id):
		
		self.dummy_traffic_manager.create_random_vehicles(self.vehicles_desired_rate, current_simulated_id)

	def start_dummy_traffic_vehicle(self):

		self.dummy_traffic_manager.start_controlling_vehicles()

		random_vehicles_dict = self.dummy_traffic_manager.vehicles_dict

		random_initial_states_dict = dict()

		for vehicle_id in random_vehicles_dict:

			current_random_vehicle = random_vehicles_dict[vehicle_id]

			initial_state_dict = dict()
			initial_state_dict['x'] = current_random_vehicle.state[0]/32.
			initial_state_dict['y'] = current_random_vehicle.state[1]/32.
			initial_state_dict['yaw'] = current_random_vehicle.state[2]

			random_initial_states_dict[vehicle_id] = initial_state_dict

		self.initial_states.update(random_initial_states_dict)

		#t = threading.Thread(target=self.dummy_traffic_step, args=([self.vehicles_desired_rate]) )
		#t.daemon = True
		#t.start()

	def dummy_traffic_step(self, vehicles_desired_rate):

		dummy_traffic_inputs_dict = self.dummy_traffic_manager.command_step(vehicles_desired_rate, simulator_is_closing=self.close)

		self.vehicle_commands.update(dummy_traffic_inputs_dict)

	def setup_platooning_vehicle(self, current_simulated_id):

		print "setup_platooning_vehicle"
		print "setup_platooning_vehicle ---- current_simulated_id = " + str(current_simulated_id)
		self.platooning_vehicles_manager.create_platooning_vehicle(self.vehicles_desired_rate, current_simulated_id, int(600*random.random()))

	# def setup_intersection_vehicle(self, current_simulated_id):

	# 	print "setup_intersection_vehicle"
	# 	print "setup_intersection_vehicle ---- current_simulated_id = " + str(current_simulated_id)

	# 	traj_start_index = 0 # Makes the vehicle start at the beginning of the traj

	# 	self.intersection_vehicles_manager.create_intersection_vehicle(self.vehicles_desired_rate, current_simulated_id, traj_start_index)
		
	def start_platooning_vehicles(self):

		self.platooning_vehicles_manager.start_controlling_vehicles()

		platooning_vehicles_dict = self.platooning_vehicles_manager.vehicles_dict

		random_initial_states_dict = dict()

		for vehicle_id in platooning_vehicles_dict:

			current_random_vehicle = platooning_vehicles_dict[vehicle_id]

			initial_state_dict = dict()
			initial_state_dict['x'] = current_random_vehicle.state[0]/32.
			initial_state_dict['y'] = current_random_vehicle.state[1]/32.
			initial_state_dict['yaw'] = current_random_vehicle.state[2]

			random_initial_states_dict[vehicle_id] = initial_state_dict

		self.initial_states.update(random_initial_states_dict)


	def platooning_vehicles_step(self, vehicles_desired_rate):

		platooning_vehicles_inputs_dict = self.platooning_vehicles_manager.command_step(self.bodies_readings, self.bodies_list, vehicles_desired_rate, simulator_is_closing=self.close)

		self.vehicle_commands.update(platooning_vehicles_inputs_dict)




