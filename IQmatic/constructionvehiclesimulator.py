import math
import xmlcomms



class ConstructionVehicleSimulator:
	"This class implements a simulator that manages the construction vehicles (caterpillars) in the SML World"

	def __init__(self, arg_construction_vehicles):	

		self.construction_vehicles = []
		self.orders = []
		self.max_yaw_rate = 2.0

		for construction_vehicle in arg_construction_vehicles:

			self.construction_vehicles.append(construction_vehicle)

	def step(self):

		orders_completed = []

		for current_order in self.orders:

			# print "self.orders = " + str(self.orders)

			vehicle_to_command = current_order['id']

			for current_vehicle in self.construction_vehicles:

				if current_vehicle['id'] == vehicle_to_command :

					yaw_order = self.correct_angle( current_order['yaw'] )

					current_yaw = self.correct_angle( current_vehicle['yaw'] )

					angle_difference = self.get_angle_difference(yaw_order, current_yaw)

					angle_to_move = angle_difference

					if math.fabs(angle_difference) > self.max_yaw_rate:

						angle_to_move = math.copysign(self.max_yaw_rate, angle_difference)

					new_yaw = current_yaw + angle_to_move

					current_vehicle['yaw'] = new_yaw

					if new_yaw == yaw_order:

						orders_completed.append(current_order)

		if orders_completed:

			orders_completed_string = xmlcomms.get_construction_finished_xml_string(orders_completed, log_flag = False)

			orders_completed_string = orders_completed_string + '\n'

			self.message_to_command_central = orders_completed_string

		else:

			self.message_to_command_central = []
			
		for completed_order in orders_completed:

			print "Caterpillar " + str(completed_order['id']) + " finished its task!"
			self.orders.remove(completed_order)

		return self.construction_vehicles

	def process_construction_order(self, xml_construction_order,log_flag = False):

		new_orders = xmlcomms.process_construction_command_xml_string(xml_construction_order, log_flag)

		self.orders.extend(new_orders)

	def get_message_to_command_central(self):

		string_to_return =  self.message_to_command_central

		self.message_to_command_central = []

		return string_to_return

	def correct_angle(self, angle_to_correct):			

		while angle_to_correct > 180.0:

			angle_to_correct = angle_to_correct - 360.0

		while angle_to_correct < -180.0:

			angle_to_correct = angle_to_correct + 360.0

		return angle_to_correct

	def get_angle_difference(self, angle_0, angle_1):			

		delta_angle = angle_0 - angle_1

		if delta_angle > 180.0:

			delta_angle = delta_angle - 180.0

		if delta_angle < -180.0:

			delta_angle = delta_angle + 180.0

		return delta_angle
