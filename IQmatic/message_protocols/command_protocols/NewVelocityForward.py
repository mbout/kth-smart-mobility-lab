from .. import protoconst
from ..Message import Message


class NewVelocityFwdP():

	"""Handles the velocity change commands sending"""

	def __init__(self, vehicle):
		"""TODO: to be defined1. """
		self.vehicle = vehicle
		self.vehicle_id = self.vehicle.id
		self.function_dict = {'set_velocity': self.setVelocity, 'C_B': self.C_B, 'EOC': self.EOC}
		self.protocol_id = protoconst.NEW_VEL_FWD
		
		self.C_B = False

		self.new_velocity = None

	def handleMessage(self, message):
		other_id = message.getVehicleID()
		initiator = message.getInitiator()
		com_id = message.getComID()

		#Message structure: Message(protocol id, self vehicle id, if self is initator, conversation id)
		out_message = Message(self.protocol_id, self.vehicle_id, not initiator, com_id)

		while message.hasNext():
			next_attribute = message.getNext()
			if next_attribute.getName() in self.function_dict:
				self.function_dict[next_attribute.getName()](other_id, next_attribute.getValue(), out_message)
			else:
				print "Attribute",next_attribute.name, "in protocol", self.protocol_id
				out_message.append("C_B", "Attribute not in protocol dictionary")
				out_message.append("EOC", 0)
				
		if out_message.hasNext():
			return out_message
		else:
			return None



	def sendNewVelocityFwd(self, target_id, new_velocity):

		out_message = Message(self.protocol_id, self.vehicle.id, 1)
		out_message.append('set_velocity', new_velocity)
		out_message.append('EOC', 1)
		return out_message
		


	def setVelocity(self, other_id, message_value, out_message):
		# TODO: make it check if sender is in same platoon, or in a platoon that is trying to merge with own platoon
		if True == True:
			self.new_velocity = message_value
		else:
			out_message.append('C_B',"The sender does is authorized to set new velocity")
			out_message.append('EOC', 0)

		return out_message



	def C_B(self, other_id, message_value, out_message):

		print "received communication breakdown: ", message_value
		self.C_B = True

	def EOC(self, other_id, message_value, out_message):
		
		if not self.C_B:

			if not out_message.getInitiator():

				if self.new_velocity != None:
					#print self.vehicle.id, "SETTING RECEIVED VELOCITY:", self.new_velocity
					self.vehicle.supervisory_module.setVelocity(self.new_velocity)
					if self.vehicle.fwd_pair_partner:
						self.vehicle.messenger.sendNewVelocityFwd(self.vehicle.fwd_pair_partner, self.new_velocity)
					if self.vehicle.new_fwd_pair_partner:
						self.vehicle.messenger.sendNewVelocityFwd(self.vehicle.new_fwd_pair_partner, self.new_velocity)
					self.new_velocity = None

		else:
			print "COMMUNICATION BREAKDOWN"
			self.C_B = False

		if message_value:
			out_message.append('EOC', 0)
			
				


				
	

	




