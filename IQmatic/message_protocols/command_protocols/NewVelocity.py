from .. import protoconst
from ..Message import Message


class NewVelocityP():

	"""Handles the velocity change commands sending"""

	def __init__(self, vehicle):
		"""TODO: to be defined1. """
		self.vehicle = vehicle
		self.function_dict = {'set_velocity': self.setVelocity, 'C_B': self.C_B, 'EOC': self.EOC}
		
		self.C_B = False

		self.new_velocity = None

	def handleMessage(self, message):
		if message.getProtocolID() ==  protoconst.NEW_VEL:
			out_message = Message(message.getProtocolID(), self.vehicle.id, not message.getInitiator(), message.getComID())

			while message.hasNext():
				attribute = message.getNext()
				if attribute.getName() in self.function_dict:
					self.function_dict[attribute.getName()](message.vehicle_id, attribute.value, out_message)
				else:
					print "ERROR: Message attribute not part of protocol"
					out_message.append('C_B', "message attribute not part of protocol")
					out_message.append('EOC', 0)

			if out_message.hasNext():
				return out_message
			else:
				return None



	def getNewVelocityMessage(self, target_id, new_velocity):

		out_message = Message(protoconst.NEW_VEL, self.vehicle.id, 1)
		out_message.append('set_velocity', new_velocity)
		out_message.append('EOC', 1)
		return out_message
		




	def setVelocity(self, other_id, message_value, out_message):
		# TODO: make it check if sender is in same platoon, or in a platoon that is trying to merge with own platoon
		if other_id == self.vehicle.new_fwd_pair_partner or other_id == self.vehicle.fwd_pair_partner or True == True:
			self.new_velocity = message_value
		else:
			print "something is wrong"
			out_message.append('C_B',"The sender does is authorized to set new velocity")
			out_message.append('EOC', 0)

		return out_message



	def C_B(self, other_id, message_value, out_message):

		print "received communication breakdown: ", message_value
		self.C_B = True

	def EOC(self, other_id, message_value, out_message):
		
		if not self.C_B:

			if not out_message.getInitiator():

				if self.new_velocity != None:
					self.vehicle.supervisory_module.setVelocity(self.new_velocity)
					self.new_velocity = None

		else:
			print "COMMUNICATION BREAKDOWN"
			self.C_B = False

		if message_value:
			out_message.append('EOC', 0)
			
				


				
	

	




