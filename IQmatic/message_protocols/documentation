--- How to use the private messaging system ---

* All protocols go in seperate folders. Protocols that don't belong in any specific folder can go in the misc_protocols folder.

* Use the PROTOCOL_TEMPLATE found in the message_protocols folder

* All protocols must import Message and protoconst found in the message_protocols folder

* To send the first message, a send****() function in the protocol is used, which is called from the MessageHandler class. This function should be put last in MessageHandler.py. In turn, supervisory_module calls a function in MessageHandler which calls the send****() function.

* MessageHandler needs to import the folder in which the protocol is found, using 
from message_protocols.some_protocols_folder import *
This also means that the name of the protocol must be in the __init__.py file in it's specified map.

* A variable name for the protocol, used by the program, also needs to be added to the protoconst.py file, where a number, the protocol id, is simply assigned to the protocol. This number is only used as a reference by the program, and does not need to be remembered, as long as it is a new number. 

* In MessageHandler the variable name of the protocol needs to be added to the protocolHandlers dictionary, where the variable name is entered as a key and the creation of the protocol object as the value.

* All messages are Message objects (class found in message_protocols). They have the properties protocol_id (corresponding to the number specified in the protoconst.py file), vehicle_id (the senders id), com_id (communication id, which is just set to the time at which the communication was initiated), initiator (is True for the vehicle that initiated the communication) and attributes. attributes is a deque() object, which is the content of the message. To add to the content, use the append('attribute_name', value) function (note: not the same as pythons list append()) to the Message object. The 'attribute_name' should call a function, specified in the function_dict in the protocol. The value is the actual content of the message, and can be any type or object.

* In the protocol, the handleMessage() function receives a message from MessageHandler, creates the out_message object and calls the specified functions. Messages are then appended to the out_messages according to the protocol.

* Every protocol must have a C_B (communication breakdown) function, which is called whenever something goes wrong in the protocol.

* Every protocol must end with an EOC (end of communication) attribute, which upon reception performs whatever operations are desired by the protocol. Different operations can be performed depending on whether the reciever initiated the communication or not. The EOC attribute has value 1 or 0, if 1 an EOC is expected to be sent back, if 0 it is not.
