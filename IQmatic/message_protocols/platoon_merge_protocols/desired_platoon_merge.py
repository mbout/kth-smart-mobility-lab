from ..Message import Message
from .. import protoconst

class DesiredPlatoonMergeP():

	def __init__(self, vehicle):

		self.vehicle = vehicle
		self.vehicle_id = self.vehicle.id

		self.function_dict = {'desired_platoon_merge': self.desiredPlatoonMerge, 'hold_to_merge': self.holdToMerge, 'merge_comparison_id': self.mergeComparisonId, 'C_B':self.C_B, 'EOC':self.EOC} # message attribute:function call
		# change to name of protocol
		self.protocol_id = protoconst.DES_PLAT_MERGE

		self.C_B = False
		#protocol variables
		self.sent_des_plat_merge = None
		self.hold_to_merge = None
		self.merge_comparison_id = None


	def handleMessage(self, message): # message is Message object

		#protocol_id = message.getProtocolID()
		#com_id = message.getComID()
		other_id = message.getVehicleID()
		initiator = message.getInitiator()
		com_id = message.getComID()
	
		#TODO: if message.getTargetID() != self.vehicle_id:
		#TODO: if message.getProtocolID() != self.protocol_id:

		#Message structure: Message(protocol id, self vehicle id, if self is initator, conversation id)
		out_message = Message(self.protocol_id, self.vehicle_id, not initiator, com_id)

		while message.hasNext():
			next_attribute = message.getNext()
			# print next_attribute.name
			if next_attribute.getName() in self.function_dict:
				self.function_dict[next_attribute.getName()](other_id, next_attribute.getValue(), out_message)
			else:
				print next_attribute.name
				out_message.append("C_B", "Part of message not in protocol dictionary")

		if out_message.hasNext():
			return out_message
		else:
			return None



	def sendFirstMessage(self,target_id):

		out_message = Message(self.protocol_id, self.vehicle_id, True)
		out_message.append('desired_platoon_merge', 1)
		self.sent_des_plat_merge = target_id
		
		return out_message


	#Functions for recieved messages:
	#def firstMessage(self, other_id, message_value, out_message):
	#	...

	def desiredPlatoonMerge(self, other_id, message_value, out_message):
		if not self.vehicle.new_bwd_pair_partner:
			#print "sending hold_to_merge"
			self.new_bwd_pair_partner = other_id
			self.hold_to_merge = self.vehicle.id
			
			out_message.append('hold_to_merge', self.vehicle.id)
			out_message.append('merge_comparison_id', self.vehicle.bwd_pair_partner)
			out_message.append('EOC', 1)
		else:
			out_message.append('C_B', "Already merging with someone")
			out_message.append('EOC', 1)

	def holdToMerge(self, other_id, message_value, out_message):
		if self.sent_des_plat_merge == other_id:
			self.hold_to_merge = message_value

		else: 
			out_message.append('C_B', "hold to merge from wrong id")
			out_message.append('EOC', 0)


	def mergeComparisonId(self, other_id, message_value, out_message):
		if self.sent_des_plat_merge == other_id:
			self.merge_comparison_id = message_value
		else:
			out_message.append('C_B', "merge comparison id from wrong id")
			out_message.append('EOC', 0)


	def C_B(self, other_id, message_value, out_message):

		self.C_B = True
		print message_value


	def EOC(self, other_id, message_value, out_message):

		if out_message.getInitiator():
			if not self.C_B:
				if self.sent_des_plat_merge != None and self.hold_to_merge != None and self.merge_comparison_id != None:
					self.vehicle.hold_to_merge = self.hold_to_merge
					self.vehicle.merge_comparison_id = self.merge_comparison_id
					self.vehicle.upcoming_platoon_merge = True
					self.vehicle.in_merging_platoon = True
					# TODO doMergePrepComparison()
					self.vehicle.supervisory_module.doMergePrepComparison()

			else:
				print "COMMUNICATION BREAKDOWN"
				self.C_B = False
			# reset flags

			self.send_des_plat_merge = None
			self.hold_to_merge = None
			self.merge_comparison_id = None

		else:
			if not self.C_B:
				if self.hold_to_merge != None:
					#do commands
					self.vehicle.hold_to_merge = self.hold_to_merge
					self.vehicle.new_bwd_pair_partner = self.new_bwd_pair_partner
					#print "The new_bwd_pair_partner of ", self.vehicle.id, " is ", self.vehicle.new_bwd_pair_partner
					self.vehicle.upcoming_platoon_merge = True
					self.vehicle.in_merging_platoon = False
					self.vehicle.got_platoon_merge_initiation_from = other_id
					
			else:
				print "COMMUNICATION BREAKDOWN"
				self.C_B = False
			# reset flags

			self.hold_to_merge = None

		if message_value == 1:
			out_message.append('EOC',0) # 0 means no expecation of response


