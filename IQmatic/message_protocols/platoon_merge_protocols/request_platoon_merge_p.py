from ..Message import Message
from .. import protoconst

class RequestPlatoonMergeP():

	def __init__(self, vehicle):

		self.vehicle = vehicle
		self.vehicle_id = self.vehicle.id

		self.function_dict = {'platoon_merge_request': self.platoonMergeRequest, 'wait_for_STOM': self.waitForSTOM, 'C_B':self.C_B, 'EOC':self.EOC} # message attribute:function call
		# change to name of protocol
		self.protocol_id = protoconst.REQ_PLAT_MERGE

		self.C_B = False
		#protocol variables
		self.sent_platoon_merge_request = None
		self.platoon_merge_request = None
		self.wait_for_STOM = None


	def handleMessage(self, message): # message is Message object

		#protocol_id = message.getProtocolID()
		#com_id = message.getComID()
		other_id = message.getVehicleID()
		initiator = message.getInitiator()
		com_id = message.getComID()
	
		#TODO: if message.getTargetID() != self.vehicle_id:
		#TODO: if message.getProtocolID() != self.protocol_id:

		#Message structure: Message(protocol id, self vehicle id, if self is initator, conversation id)
		out_message = Message(self.protocol_id, self.vehicle_id, not initiator, com_id)

		while message.hasNext():
			next_attribute = message.getNext()
			# print next_attribute.name
			if next_attribute.getName() in self.function_dict:
				self.function_dict[next_attribute.getName()](other_id, next_attribute.getValue(), out_message)
			else:
				print next_attribute.name
				out_message.append("C_B", "Part of message not in protocol dictionary")

		if out_message.hasNext():
			return out_message
		else:
			return None



	def sendFirstMessage(self,target_id):

		out_message = Message(self.protocol_id, self.vehicle_id, True)
		out_message.append('platoon_merge_request', 1)
		self.sent_platoon_merge_request = target_id
		return out_message


	#Functions for recieved messages:
	#def firstMessage(self, other_id, message_value, out_message):
	#	...

	def platoonMergeRequest(self, other_id, message_value, out_message):

		if self.vehicle.upcoming_platoon_merge and other_id in self.vehicle.perceived_objects:
			self.platoon_merge_request = other_id
			out_message.append('wait_for_STOM', self.vehicle.id)
		else:
			out_message.append('C_B', "Unexpected platoon_merge_request received")
			out_message.append('EOC', 1)

	def waitForSTOM(self, other_id, message_value, out_message):

		if self.sent_platoon_merge_request and other_id == self.vehicle.platoon_STOM_partner:
			self.wait_for_STOM = other_id
			out_message.append('EOC', 1)
		else:
			out_message.append('C_B', "Unexpected wait_for_STOM received")
			out_message.append('EOC', 1)


	def C_B(self, other_id, message_value, out_message):

		self.C_B = True
		print message_value


	def EOC(self, other_id, message_value, out_message):

		if out_message.getInitiator():
			if not self.C_B:
				if self.sent_platoon_merge_request == other_id and self.wait_for_STOM == other_id:
					#do commands
					self.vehicle.wait_for_STOM = self.wait_for_STOM
					self.vehicle.supervisory_module.waitForPlatoonSTOM()
			else:
				print "COMMUNICATION BREAKDOWN"
				self.C_B = False
			# reset flags
			self.sent_platoon_merge_request = None
			self.wait_for_STOM = None

		else:
			if not self.C_B:
				if self.platoon_merge_request == other_id:
					#do commands
					self.vehicle.platoon_merge_request = self.platoon_merge_request 
					self.vehicle.supervisory_module.preparePlatoonSTOM()
					
			else:
				print "COMMUNICATION BREAKDOWN"
				self.C_B = False
			# reset flags
			self.platoon_merge_request = None

		if message_value == 1:
			out_message.append('EOC',0) # 0 means no expecation of response


