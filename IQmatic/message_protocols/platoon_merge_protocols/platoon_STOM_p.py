from ..Message import Message
from .. import protoconst

class PlatoonSTOMP():

	def __init__(self, vehicle):

		self.vehicle = vehicle
		self.vehicle_id = self.vehicle.id

		self.function_dict = {'STOM': self.STOM, 'new_lane': self.newLane, 'C_B':self.C_B, 'EOC':self.EOC} # message attribute:function call
		# change to name of protocol
		self.protocol_id = protoconst.PLAT_STOM

		self.C_B = False
		#protocol variables
		self.sent_platoon_STOM = None
		self.STOM = None
		self.new_lane = None


	def handleMessage(self, message): # message is Message object

		#protocol_id = message.getProtocolID()
		#com_id = message.getComID()
		other_id = message.getVehicleID()
		initiator = message.getInitiator()
		com_id = message.getComID()
	
		#TODO: if message.getTargetID() != self.vehicle_id:
		#TODO: if message.getProtocolID() != self.protocol_id:

		#Message structure: Message(protocol id, self vehicle id, if self is initator, conversation id)
		out_message = Message(self.protocol_id, self.vehicle_id, not initiator, com_id)

		while message.hasNext():
			next_attribute = message.getNext()
			# print next_attribute.name
			if next_attribute.getName() in self.function_dict:
				self.function_dict[next_attribute.getName()](other_id, next_attribute.getValue(), out_message)
			else:
				print next_attribute.name
				out_message.append("C_B", "Part of message not in protocol dictionary")

		if out_message.hasNext():
			return out_message
		else:
			return None



	def sendFirstMessage(self,target_id, lane_string):

		out_message = Message(self.protocol_id, self.vehicle_id, True)
		out_message.append('STOM', 1)
		out_message.append('new_lane', lane_string)
		out_message.append('EOC', 1)
		self.sent_platoon_STOM = target_id
		
		return out_message


	#Functions for recieved messages:
	#def firstMessage(self, other_id, message_value, out_message):
	#	...

	def STOM(self, other_id, message_value, out_message):
		if other_id == self.vehicle.platoon_STOM_partner:
			self.STOM = other_id
		else:
			out_message.append('C_B', "Unexpected STOM received from " + str(other_id))
			out_message.append('EOC', 0)
	
	def newLane(self, other_id, message_value, out_message):
		if other_id == self.STOM:
			self.new_lane = message_value
		else:
			out_message.append('C_B', "New lane sent from non-STOM partner")
			out_message.append('EOC', 0)


	def C_B(self, other_id, message_value, out_message):

		self.C_B = True
		print message_value


	def EOC(self, other_id, message_value, out_message):

		if out_message.getInitiator():
			if not self.C_B:
				if self.sent_platoon_STOM == other_id:
					#do commands
					self.vehicle.sent_platoon_STOM = self.sent_platoon_STOM
			else:
				print "COMMUNICATION BREAKDOWN"
				self.C_B = False
			# reset flags
			self.sent_platoon_STOM = None

		else:
			if not self.C_B:
				if self.STOM == self.vehicle.platoon_STOM_partner:
					#do commands
					self.vehicle.platoon_STOM = self.STOM
					self.vehicle.platoon_new_lane = self.new_lane
					self.vehicle.STOM_event.set()

			else:
				print "COMMUNICATION BREAKDOWN"
				self.C_B = False
			# reset flags
			self.STOM  =  None
			self.new_lane = None

		if message_value == 1:
			out_message.append('EOC',0) # 0 means no expecation of response


