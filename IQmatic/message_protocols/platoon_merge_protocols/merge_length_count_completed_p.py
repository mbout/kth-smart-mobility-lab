from ..Message import Message
from .. import protoconst

class MergeLengthCountCompletedP():

	def __init__(self, vehicle):

		self.vehicle = vehicle
		self.vehicle_id = self.vehicle.id

		self.function_dict = {'C_B':self.C_B, 'EOC':self.EOC, 'merge_length_count_completed':self.MergeLengthCountCompleted} # message attribute:function call
		
		# change to name of protocol
		self.protocol_id = protoconst.MER_LEN_COUNT_COMPL

		self.C_B = False
		self.merge_length_count_completed = None
		#protocol variables


	def handleMessage(self, message): # message is Message object


		#com_id = message.getComID()
		other_id = message.getVehicleID()
		initiator = message.getInitiator()
		com_id = message.getComID()
	

		#Message structure: Message(protocol id, self vehicle id, if self is initator, conversation id)
		out_message = Message(self.protocol_id, self.vehicle_id, not initiator, com_id)

		while message.hasNext():
			next_attribute = message.getNext()
			if next_attribute.getName() in self.function_dict:
				self.function_dict[next_attribute.getName()](other_id, next_attribute.getValue(), out_message)
			else:
				print "Attribute",next_attribute.name, "in protocol", self.protocol_id
				out_message.append("C_B", "Attribute not in protocol dictionary")
				out_message.append("EOC", 0)
				
		if out_message.hasNext():
			return out_message
		else:
			return None



	def sendMergeLengthCountCompleted(self, target_id, merge_length_count_completed):

		out_message = Message(self.protocol_id, self.vehicle_id, True)
		out_message.append('merge_length_count_completed', merge_length_count_completed)
		out_message.append('EOC',1)
		
		return out_message



	def MergeLengthCountCompleted(self, other_id, message_value, out_message):
		if isinstance(message_value, bool):
			self.merge_length_count_completed = message_value
		else:
			out_message.append('C_B', 'Incorrect type for "merge_length_count_completed"')



	def C_B(self, other_id, message_value, out_message):

		print "received communication breakdown: ", message_value
		self.C_B = True



	def EOC(self, other_id, message_value, out_message):

		if not out_message.getInitiator():
			if not self.C_B:
				if self.merge_length_count_completed != None:
					self.vehicle.merge_length_count_completed = self.merge_length_count_completed # Maybe make this set an event instead

			else:
				print "COMMUNICATION BREAKDOWN"
				self.C_B = False
			self.merge_length_count_completed = None


		if message_value == 1:
			out_message.append('EOC',0) # 0 means no expecation of response


