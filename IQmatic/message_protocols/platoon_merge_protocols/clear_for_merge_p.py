from ..Message import Message
from .. import protoconst

class ClearForMergeP():

	def __init__(self, vehicle):

		self.vehicle = vehicle
		self.vehicle_id = self.vehicle.id

		self.function_dict = {'clear_for_merge': self.clearForMerge, 'C_B':self.C_B, 'EOC':self.EOC} # message attribute:function call
		# change to name of protocol
		self.protocol_id = protoconst.CLEAR_FOR_MERGE
		

		self.C_B = False
		#protocol variables
		self.sent_clear_for_merge = None
		self.clear_for_merge = None


	def handleMessage(self, message): # message is Message object

		#protocol_id = message.getProtocolID()
		#com_id = message.getComID()
		other_id = message.getVehicleID()
		initiator = message.getInitiator()
		com_id = message.getComID()
	
		#TODO: if message.getTargetID() != self.vehicle_id:
		#TODO: if message.getProtocolID() != self.protocol_id:

		#Message structure: Message(protocol id, self vehicle id, if self is initator, conversation id)
		out_message = Message(self.protocol_id, self.vehicle_id, not initiator, com_id)

		while message.hasNext():
			next_attribute = message.getNext()
			# print next_attribute.name
			if next_attribute.getName() in self.function_dict:
				self.function_dict[next_attribute.getName()](other_id, next_attribute.getValue(), out_message)
			else:
				print next_attribute.name
				out_message.append("C_B", "Part of message not in protocol dictionary")

		if out_message.hasNext():
			return out_message
		else:
			return None



	def sendFirstMessage(self,target_id):

		out_message = Message(self.protocol_id, self.vehicle_id, True)
		out_message.append('clear_for_merge', 1)
		out_message.append('EOC', 1)
		self.sent_clear_for_merge = target_id
		
		return out_message


	#Functions for recieved messages:
	#def firstMessage(self, other_id, message_value, out_message):
	#	...
	
	def clearForMerge(self, other_id, message_value, out_message):

		if other_id == self.vehicle.hold_to_merge:
			self.clear_for_merge = message_value
		else:
			out_message.append('C_B', "unauthorized clear for merge received")
			out_message.append('EOC', 0)


	def C_B(self, other_id, message_value, out_message):

		self.C_B = True
		print message_value


	def EOC(self, other_id, message_value, out_message):

		if out_message.getInitiator():
			if not self.C_B:
				if self.sent_clear_for_merge == 1:
					#do commands
					self.vehicle.sent_clear_for_merge = self.sent_clear_for_merge
			else:
				print "COMMUNICATION BREAKDOWN"
				self.C_B = False
			# reset flags
			self.sent_clear_for_merge = None

		else:
			if not self.C_B:
				if self.clear_for_merge == 1:
					#do commands
					self.vehicle.clear_for_merge = self.clear_for_merge
					self.vehicle.supervisory_module.handleClearForMerge()
			else:
				print "COMMUNICATION BREAKDOWN"
				self.C_B = False
			# reset flags
			self.clear_for_merge = None

		if message_value == 1:
			out_message.append('EOC',0) # 0 means no expecation of response


