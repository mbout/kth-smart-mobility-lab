# This is init file for the rear_merge_protocols module
__all__ = ['desired_merge_p',
		'merge_complete_p',
		'request_merge_p',
		'STOM_p']