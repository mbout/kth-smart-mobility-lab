from ..Message import Message
from .. import protoconst

class STOMP():

	def __init__(self, vehicle):

		self.vehicle = vehicle
		self.vehicle_id = self.vehicle.id

		self.function_dict = {'C_B':self.C_B, 'EOC':self.EOC, 'STOM':self.STOM}
		self.protocol_id = protoconst.STOM

		self.C_B = False
		self.STOM_flag = None
		self.merging_flag = None


	def handleMessage(self, message): # message is Message object

		#protocol_id = message.getProtocolID()
		#com_id = message.getComID()
		other_id = message.getVehicleID()
		initiator = message.getInitiator()
		com_id = message.getComID()
	
		#TODO: if message.getTargetID() != self.vehicle_id:
		#TODO: if message.getProtocolID() != self.protocol_id:

		#Message structure: Message(protocol id, self vehicle id, if self is initator, conversation id)
		out_message = Message(self.protocol_id, self.vehicle_id, not initiator, com_id)

		while message.hasNext():
			next_attribute = message.getNext()
			# print next_attribute.name
			if next_attribute.getName() in self.function_dict:
				self.function_dict[next_attribute.getName()](other_id, next_attribute.getValue(), out_message)
			else:
				print next_attribute.name
				out_message.append("C_B", "Part of message not in protocol dictionary")

		if out_message.hasNext():
			return out_message
		else:
			return None



	def sendSTOM(self, target_id):

		out_message = Message(self.protocol_id, self.vehicle_id, True)
		out_message.append('STOM', 1)
		self.STOM_flag = target_id
		
		return out_message


	def STOM(self, other_id, message_value, out_message):

		if self.vehicle.preparing_to_merge_flag == other_id:
			if self.vehicle.new_fwd_pair_partner == other_id:
				out_message.append("EOC",1)
				self.merging_flag = other_id
			elif self.vehicle.new_bwd_pair_partner == other_id:
				#TODO - merging in between
				pass
		else:
			out_message.append('C_B', 'Got STOM, but not preparing to merge with sender')
			out_message.append('EOC', 1)


	def C_B(self, other_id, message_value, out_message):

		self.C_B = True
		print message_value


	def EOC(self, other_id, message_value, out_message):

		if out_message.getInitiator():
			if not self.C_B:
				if self.STOM_flag:
					self.vehicle.STOM_flag = self.STOM_flag
					self.vehicle.preparing_STOM_flag = None
					self.vehicle.messenger.sendLaneChange(self.vehicle.new_bwd_pair_partner, self.vehicle.desired_lane)
			else:
				print "COMMUNICATION BREAKDOWN"
				self.C_B = False
			self.STOM_flag = None
			self.C_B = False

		else:
			if not self.C_B:
				if self.merging_flag:
					self.vehicle.merging_flag = self.merging_flag
					self.vehicle.preparing_to_merge_flag = None
					self.vehicle.supervisory_module.setACCCTarget(self.vehicle.new_fwd_pair_partner, self.vehicle.merge_distance)
			else:
				print "COMMUNICATION BREAKDOWN"
				self.C_B = False
			self.merging_flag = None
			self.C_B = False

		if message_value == 1:
			out_message.append('EOC',0) # 0 means no expecation of response


