from ..Message import Message
from .. import protoconst

class MergeCompleteP():

	def __init__(self, vehicle):

		self.vehicle = vehicle
		self.vehicle_id = self.vehicle.id

		self.function_dict = {'C_B':self.C_B, 'EOC':self.EOC, 'mergeComplete':self.mergeComplete}
		self.protocol_id = protoconst.MERGE_COMPLETE

		self.C_B = False


	def handleMessage(self, message): # message is Message object

		#protocol_id = message.getProtocolID()
		#com_id = message.getComID()
		other_id = message.getVehicleID()
		initiator = message.getInitiator()
		com_id = message.getComID()
	
		#TODO: if message.getTargetID() != self.vehicle_id:
		#TODO: if message.getProtocolID() != self.protocol_id:

		#Message structure: Message(protocol id, self vehicle id, if self is initator, conversation id)
		out_message = Message(self.protocol_id, self.vehicle_id, not initiator, com_id)

		while message.hasNext():
			next_attribute = message.getNext()
			# print next_attribute.name
			if next_attribute.getName() in self.function_dict:
				self.function_dict[next_attribute.getName()](other_id, next_attribute.getValue(), out_message)
			else:
				print next_attribute.name
				out_message.append("C_B", "Part of message not in protocol dictionary")

		if out_message.hasNext():
			return out_message
		else:
			return None



	def sendMergeComplete(self, target_id):

		out_message = Message(self.protocol_id, self.vehicle_id, True)
		out_message.append('mergeComplete', 1)
		
		return out_message


	def mergeComplete(self, other_id, message_value, out_message):

		if self.vehicle.STOM_flag == other_id:
			out_message.append('EOC',1)
		#else: some C_B


	def C_B(self, other_id, message_value, out_message):

		self.C_B = True
		print message_value


	def EOC(self, other_id, message_value, out_message):

		if out_message.getInitiator():
			if not self.C_B:
				if self.vehicle.new_fwd_pair_partner == other_id:
					self.vehicle.fwd_pair_partner = other_id
					self.vehicle.platooned_vehicle = True
					self.vehicle.new_fwd_pair_partner = None
					self.vehicle.merging_flag = None
					self.vehicle.ongoing_platoon_merge = False

			else:
				print "COMMUNICATION BREAKDOWN"
				self.C_B = False


		else:
			if not self.C_B:

				if self.vehicle.new_bwd_pair_partner == other_id:
					self.vehicle.bwd_pair_partner = other_id
					self.vehicle.platooned_vehicle = True
					self.vehicle.new_bwd_pair_partner = None
					self.vehicle.STOM_flag = None
					self.vehicle.supervisory_module.alertFollowers(self.vehicle.desired_velocity)
					self.vehicle.ongoing_platoon_merge = False

			else:
				print "COMMUNICATION BREAKDOWN"
				self.C_B = False


		if self.vehicle.upcoming_platoon_merge:
			self.vehicle.clear_for_merge = None
			self.vehicle.in_merging_platoon = None
			self.vehicle.hold_to_merge = None
			self.vehicle.platoon_STOM_partner = None
			self.vehicle.platoon_STOM = None
			self.vehicle.platoon_new_lane = None
			self.vehicle.upcoming_platoon_merge = None
			self.vehicle.merge_prep_done = None


		if message_value == 1:
			out_message.append('EOC',0) # 0 means no expecation of response


