from ..Message import Message
from .. import protoconst

class DesiredMergeP():

	def __init__(self, vehicle):

		self.vehicle = vehicle
		self.vehicle_id = self.vehicle.id

		self.function_dict = {'desired_merge':self.desiredMerge, 'accepted_merge':self.acceptedMerge, 'set_velocity':self.setVelocity, 'C_B':self.C_B, 'EOC':self.EOC}
		self.protocol_id = protoconst.DES_MERGE

		self.sent_accept_merge_flag = None
		self.desired_merge_flag = None
		self.new_velocity = None
		self.new_bwd_pair_partner = None
		self.new_fwd_pair_partner = None
		self.hold_to_merge = None
		self.merge_comparison_id = None
		self.C_B = False


	def handleMessage(self, message): # message is Message object

		#protocol_id = message.getProtocolID()
		#com_id = message.getComID()
		other_id = message.getVehicleID()
		initiator = message.getInitiator()
		com_id = message.getComID()
	
		#TODO: if message.getTargetID() != self.vehicle_id:
		#TODO: if message.getProtocolID() != self.protocol_id:

		#Message structure: Message(protocol id, self vehicle id, if self is initator, conversation id)
		out_message = Message(self.protocol_id, self.vehicle_id, not initiator, com_id)

		while message.hasNext():
			next_attribute = message.getNext()
			# print next_attribute.name
			if next_attribute.getName() in self.function_dict:
				self.function_dict[next_attribute.getName()](other_id, next_attribute.getValue(), out_message)
			else:
				print next_attribute.name
				out_message.append("C_B", "Part of message not in protocol dictionary")

		if out_message.hasNext():
			return out_message
		else:
			return None



	def sendDesiredMerge(self, target_id):
		out_message = Message(self.protocol_id, self.vehicle_id, True)
		out_message.append('desired_merge', 1)
		self.desired_merge_flag = target_id
		
		return out_message


	def desiredMerge(self, other_id, message_value, out_message): # out_message contains protocol_id, con_id and whether or not self is initator
		print "vehicle", self.vehicle_id, "received desired merge from", other_id

		if self.sent_accept_merge_flag or self.vehicle.new_bwd_pair_partner or self.new_bwd_pair_partner:
			out_message.append('accepted_merge',0)
			out_message.append('EOC',1) # 1 means expectation of EOC response
		else:
			out_message.append('accepted_merge',1)

			if self.vehicle.platooned_vehicle or self.vehicle.new_fwd_pair_partner:
				out_message.append('set_velocity',self.vehicle.desired_velocity)
			else:
				out_message.append('set_velocity',1.4*self.vehicle.desired_velocity)

			out_message.append('EOC',1)

			self.sent_accept_merge_flag = other_id
			self.new_bwd_pair_partner = other_id


	def acceptedMerge(self, other_id, message_value, out_message):

		if not self.desired_merge_flag == other_id: #OBS: THIS FLAG IS NOT ASSIGNED IN THIS CLASS!!!
			out_message.append('C_B',"Desired merge doesn't match messenger id")
			out_message.append('EOC',0)
		else:
			if message_value == 1:
				self.new_fwd_pair_partner = other_id
	

	def setVelocity(self, other_id, message_value, out_message):

		if other_id == self.new_fwd_pair_partner:
			self.new_velocity = message_value
		else:
			out_message.append('C_B',"Messenger id doesn't match recipients new fwd pair partner")
			out_message.append('EOC',1)


	def C_B(self, other_id, message_value, out_message):

		self.C_B = True
		print message_value


	def EOC(self, other_id, message_value, out_message):

		if out_message.getInitiator():
			if not self.C_B:
				if self.new_fwd_pair_partner and self.new_velocity:
					self.vehicle.new_fwd_pair_partner = self.new_fwd_pair_partner
					self.vehicle.desired_merge_flag = self.desired_merge_flag
					self.vehicle.supervisory_module.setVelocity(self.new_velocity)
				#else: ?
			else:
				print "COMMUNICATION BREAKDOWN"
				self.C_B = False

			self.desired_merge_flag = None
			self.new_velocity = None
			self.new_fwd_pair_partner = None

		else:
			if not self.C_B:
				if self.new_bwd_pair_partner:
					self.vehicle.new_bwd_pair_partner = self.new_bwd_pair_partner
				#else: ?
			else:
				print "COMMUNICATION BREAKDOWN"
				self.C_B = False

			self.sent_accept_merge_flag = None
			self.new_bwd_pair_partner = None

		if message_value == 1:
			out_message.append('EOC',0) # 0 means no expecation of response


