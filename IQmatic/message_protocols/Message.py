import time
import collections

class Message():

	def __init__(self, protocol_id, vehicle_id, initiator, com_id = time.time()):
		self.protocol_id = protocol_id
		self.vehicle_id = vehicle_id
		self.com_id = com_id
		self.initiator = initiator
		self.attributes = collections.deque()

	def append(self, attribute_name, value):

		self.attributes.append(Attribute(attribute_name, value))

	def getNext(self):
		if len(self.attributes) > 0:
			return self.attributes.popleft()
		else:
			return None

	def hasNext(self):
		return len(self.attributes) > 0

	def getProtocolID(self):
		return self.protocol_id

	def getVehicleID(self):
		return self.vehicle_id

	def getComID(self):
		return self.com_id

	def getInitiator(self):
		return self.initiator



class Attribute():

	def __init__(self, attribute_name, attribute_value):
		self.name = attribute_name
		self.value = attribute_value
	
	def getName(self):
		return self.name

	def getValue(self):
		return self.value


