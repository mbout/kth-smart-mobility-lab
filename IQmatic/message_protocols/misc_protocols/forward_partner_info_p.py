from ..Message import Message
from .. import protoconst

class ForwardPartnerInfoP():

	def __init__(self, vehicle):

		self.vehicle = vehicle
		self.vehicle_id = self.vehicle.id

		self.function_dict = {'C_B':self.C_B, 'EOC':self.EOC, 'vehicle_info':self.ForwardPartnerInfo} # message attribute:function call
		# change to name of protocol
		self.protocol_id = protoconst.FWD_PARTNER_INFO

		self.vehicle_info = None
		self.C_B = False
		#protocol variables


	def handleMessage(self, message): # message is Message object

		#protocol_id = message.getProtocolID()
		#com_id = message.getComID()
		other_id = message.getVehicleID()
		initiator = message.getInitiator()
		com_id = message.getComID()
	
		#TODO: if message.getTargetID() != self.vehicle_id:
		#TODO: if message.getProtocolID() != self.protocol_id:

		#Message structure: Message(protocol id, self vehicle id, if self is initator, conversation id)
		out_message = Message(self.protocol_id, self.vehicle_id, not initiator, com_id)

		while message.hasNext():
			next_attribute = message.getNext()
			# print next_attribute.name
			if next_attribute.getName() in self.function_dict:
				self.function_dict[next_attribute.getName()](other_id, next_attribute.getValue(), out_message)
			else:
				print "Attribute",next_attribute.name, "in protocol", self.protocol_id
				out_message.append("C_B", "Attribute not in protocol dictionary")
				out_message.append("EOC", 0)

		if out_message.hasNext():
			return out_message
		else:
			return None



	def sendForwardPartnerInfo(self, target_id, extra_info):

		out_message = Message(self.protocol_id, self.vehicle_id, True)
		info_dict = {}
		info_dict['vehicle_id'] = self.vehicle.id
		info_dict['lane'] = self.vehicle.desired_lane
		info_dict['velocity'] = self.vehicle.desired_velocity
		info_dict['x'] = self.vehicle.x
		info_dict['y'] = self.vehicle.y
		info_dict.update(extra_info)

		out_message.append('vehicle_info', info_dict)

		out_message.append('EOC', 1)
		
		return out_message


	def ForwardPartnerInfo(self, other_id, message_value, out_message):
		if other_id == self.vehicle.fwd_pair_partner or other_id == self.vehicle.new_fwd_pair_partner:
			self.vehicle_info = message_value

		else:
			out_message.append('C_B', "Sender's info not desired due to not paired with self")



	def C_B(self, other_id, message_value, out_message):

		print "received communication breakdown: ", message_value
		self.C_B = True

	def EOC(self, other_id, message_value, out_message):

		if not out_message.getInitiator():
			if not self.C_B:
				if self.vehicle_info != None:
					self.vehicle.forward_partner_info = self.vehicle_info
					self.vehicle.supervisory_module.handleFwdPartnerInfo()


			else:
				print "COMMUNICATION BREAKDOWN"
				self.C_B = False
			self.vehicle_info = None

		else:
			if self.C_B:
				print "COMMUNICATION BREAKDOWN"
				self.C_B = False

		if message_value == 1:
			out_message.append('EOC',0) # 0 means no expecation of response


