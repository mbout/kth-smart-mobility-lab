# This is init file for the misc_protocols module
__all__ = ['forward_partner_info_p', 
		'RequestVelocity',
		'desired_unspecified_merge_p']