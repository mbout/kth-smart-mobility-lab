/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command_central;

import java.util.concurrent.BlockingQueue;

/**
 *
 * @author Matteo
 */
class DrawDataThread implements Runnable
{
    private final long SLEEP_TIME = 50;
    
    private Thread thread;
    private final String threadName;    
    private final BlockingQueue queue;
    
    public DrawDataThread(String name, BlockingQueue q)
    {
        threadName = name;
        queue = q;
        System.out.println("THREAD MESSAGE: Created " + threadName);

    }
    
    
    @Override
    public void run()
    {
        System.out.println("THREAD MESSAGE: Running " +  threadName );
        while(true)
        {
            try
            {
                if (queue.peek()!=null)
                {
                    float[][] toDraw = (float[][]) queue.take();
                    drawBodies(toDraw);
                    System.out.println("");
                }
                Thread.sleep(SLEEP_TIME);            
            }
            catch (InterruptedException ex) {
                System.out.println("THREAD MESSAGE: Thread " + threadName + " interrupted");
            }
        }
        
    }
    
    public void start ()
    {
        System.out.println("THREAD MESSAGE: Starting " +  threadName );
        if (thread == null)
        {
           thread = new Thread (this, threadName);
           thread.start ();
        }

    }
    
    public void stop ()
    {
        System.out.println("THREAD MESSAGE: Stopping " + threadName);
        if (thread!=null)
        {
            thread.interrupt();
        }
    }

    private void drawBodies(float[][] toDraw) {
        System.out.println("Drawing bodies ... " + toDraw[0]);
    }
        
        
    
}
