package command_central;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matteo
 */
public class Mocap {
    
    private Socket socket;
    private String host;
    private int port;
    private InputStream inputStream;
    private OutputStream outputStream;
    
    private final int BYTES_PACKET_SIZE = 4;
    private final int BYTES_PACKET_TYPE = 4;
    private final int BYTES_HEADER = BYTES_PACKET_TYPE + BYTES_PACKET_SIZE;
    
    private final int BYTES_TIMESTAMP = 8;
    private final int BYTES_FRAMENR = 4;
    private final int BYTES_COMPONENT_COUNT = 4;
    private final int BYTES_COMPONENT_SIZE = 4;
    private final int BYTES_COMPONENT_TYPE = 4;
    private final int BYTES_COMPONENT_BODYCOUNT = 4;
    private final int BYTES_JUNK = 4;
    private final int BYTES_ONE_DOF_INFO = 4;
    private final int BYTES_TRAILER = 1;
    
    private final int SOCKET_TIMEOUT = 2; // in seconds
    
    private final String[] MSG_TYPES = {"Error","Command","XML","Data","No more data","C3D file","Event","Discover","QTM file"};
    private final String[] EVENT_TYPES = {"Connected","Connection Closed","Capture Started","Capture Stopped","","Calibration Started",
        "Calibration Stopped","RT From File Started","RT From File Stopped","Waiting For Trigger"};
    private final String[] DOFS = {"x","y","z","a1","a2","a3"};
    
    private final String SET_VERSION_STRING = "Version 1.11";
    private final String GET_DATA_STRING = "GetCurrentFrame 6DEuler";
    private final String BE_MASTER_STRING = "TakeControl sml";
    private final String NEW_RT_STRING = "New";
    private final String CLOSE_RT_STRING = "Close";
    private final String NEED_MASTER_STRING = "You must be master to issue this command";
    private final String ALREADY_CLOSED_STRING = "No connection to close";
    private final String GET_STATE_STRING = "GetState";
    private final String RELEASE_CONTROL_STRING = "ReleaseControl";
    
    private final ByteOrder BE = ByteOrder.BIG_ENDIAN;
    private final ByteOrder LE = ByteOrder.LITTLE_ENDIAN;
    
    private final float INF = Float.POSITIVE_INFINITY;
    
    InetSocketAddress inetSocketAddress;
    
//    private String status;
    
    public Mocap() throws IOException 
    {
        this("smlremote.no-ip.biz",22224);
    }
    
    public Mocap(String argHost, int argPort) throws IOException
    {
        host = argHost;
        port = argPort;
//        status = "Mocap initialized";
        socket = new Socket();
        inetSocketAddress = new InetSocketAddress(host,port);
        socket.connect(inetSocketAddress,SOCKET_TIMEOUT*1000);
        inputStream = socket.getInputStream();
        outputStream = socket.getOutputStream();
        receiveWelcomeMessage();
        setVersion();

    }
    
    public void startupRTServer() throws IOException
    {
        String response = createNewMeasurement();
        if (response.substring(0,response.length()-1).equals(NEED_MASTER_STRING)) //last byte is always \x00
        {
            becomeMaster();
//            Thread.sleep(200);
            createNewMeasurement();
        }    
    }
    
    public void closeRTServer() throws IOException
    {
        String response = closeMeasurement();
        if (response.substring(0,response.length()-1).equals(NEED_MASTER_STRING)) //last byte is always \x00
        {
            becomeMaster();
            closeMeasurement();
        }
    }
//    public String getStatus()
//    {
//        return status;
//    }

    
    public float[] requestBodyData(int body_nr) throws IOException
    {
        System.out.println("Requesting data for body nr"+body_nr+"...");
        float[][] bodies = requestAllBodies(false);
        float[] reqBody = bodies[body_nr];
        float dof;
//        System.out.println("--------------------------");
        System.out.println("Body nr "+body_nr);
        for (int j=0;j<reqBody.length;j++)
            {
                dof = reqBody[j];
                System.out.println(DOFS[j]+"\t"+dof);
            }
    	return reqBody;
    }
    
    public float[][] requestAllBodies(boolean printInfo) throws IOException
    {
        if (printInfo)
            System.out.println("Requesting all the bodies...");
        byte[] packet = buildPacket(GET_DATA_STRING,1);
        sendRequest(packet);
        float[][] data = parseDataComponent();
        if (data==null)
        {
            data = new float[1][1];
            data[0][0] = INF;
            return data;
        }
        if (printInfo)
        {
//            System.out.println("--------------------------");
            System.out.println("All bodies:");
//            System.out.println();        
        }
        int count = 0;
        for (float[] body : data)
        {
            if (printInfo) System.out.println("body nr "+ count);
//            byte[] hex_dof = null;
            float dof;
            for (int j=0;j<body.length;j++)
            {
                dof = body[j];
                if (printInfo) System.out.println(DOFS[j]+"\t"+dof);
//                hex_dof = ByteBuffer.allocate(4).putFloat(dof).array();
//                if (printInfo)
//                {
//                    for (byte b: hex_dof)
//                    {
//                        System.out.format("0x%01X",b);
//                        System.out.print("\t");                
//                    }
//                    System.out.println("");                
//                }
            }
            if (printInfo) System.out.println("");
            count ++;
        }
        return data;
    }

    private void sendRequest(byte[] request) throws IOException
    {
        outputStream.write(request);
    }
    
    private void setVersion() throws IOException
    {
        System.out.println("Requesting to set version...");
        byte[] packet = buildPacket(SET_VERSION_STRING,1);
        sendRequest(packet);
        parseMessage();
    }
    
    private void getState() throws IOException
    {
        System.out.println("Requesting QTM state...");
        byte[] packet = buildPacket(GET_STATE_STRING,1);
        sendRequest(packet);
        parseEventComponent();
    }
    
    private void becomeMaster() throws IOException
    {
        System.out.println("Requesting to become master...");
        byte[] packet = buildPacket(BE_MASTER_STRING,1);
        sendRequest(packet);
        parseMessage();
    }
    
    private void releaseMaster() throws IOException
    {
        System.out.println("Requesting to release control...");
        byte[] packet = buildPacket(RELEASE_CONTROL_STRING,1);
        sendRequest(packet);
        parseMessage();
    }
    
    private String createNewMeasurement() throws IOException
    {
        System.out.println("Requesting to create new measurement...");
        byte[] packet = buildPacket(NEW_RT_STRING, 1);
        sendRequest(packet);
        return parseStringComponent();
    }
    
    private String closeMeasurement() throws IOException
    {
        System.out.println("Requesting to close measurement...");
        byte[] packet = buildPacket(CLOSE_RT_STRING, 1);
        sendRequest(packet);
        return parseStringComponent();
    }
    
    private byte[] buildPacket(String content, int type)
    {
        byte[] message = content.getBytes(Charset.forName("UTF-8"));
        int totalLength = message.length + BYTES_PACKET_SIZE + BYTES_PACKET_TYPE + BYTES_TRAILER;
        byte[] size_header = ByteBuffer.allocate(BYTES_PACKET_SIZE).putInt(totalLength).array();
        byte[] type_header = ByteBuffer.allocate(BYTES_PACKET_TYPE).putInt(type).array();
        byte[] trailer = {0};
        byte[] header = new byte[BYTES_PACKET_SIZE + BYTES_PACKET_TYPE];
        System.arraycopy(size_header,0,header,0,size_header.length);
        System.arraycopy(type_header,0,header,size_header.length,type_header.length);
        byte[] rest = new byte[message.length + BYTES_TRAILER];
        System.arraycopy(message,0,rest,0,message.length);
        System.arraycopy(trailer,0,rest,message.length,1);
        byte[] byteArray = new byte[totalLength];
        System.arraycopy(header,0,byteArray,0,header.length);
        System.arraycopy(rest,0,byteArray,header.length,rest.length);
//        for (byte b : byteArray)
//        {
//            System.out.format("0x%02X" , b);
//            System.out.println();
//        }
        return byteArray;
    }
    
    private Object[] parseMessage() throws IOException
    {
        float[][] data = null;
        String str_message = null;
        String event_type = null;
        
        int info_packet_size = receiveIntBuffer(BYTES_PACKET_SIZE,BE);
//        System.out.println("info_packet_size = " + info_packet_size);
        int info_packet_type = receiveIntBuffer(BYTES_PACKET_TYPE,BE);
//        System.out.println("info_packet_type = " + info_packet_type);
        if (info_packet_type==3)
        {
            data = parseDataMessage();
        }
        else if(info_packet_type==6)
        {
            int event_type_int = receiveByteBuffer(BE);
            event_type = EVENT_TYPES[event_type_int];
            System.out.println("State: " + event_type);
        }
        else if(info_packet_type==4)
        {
            System.out.println("No More Data Available");
//            startupRTServer();
        }
        else
        {
            str_message = receiveStringBuffer(info_packet_size-BYTES_HEADER);
//            System.out.println("--------------------------");
            System.out.println("QTM SERVER MESSAGE: " + str_message);
        }
        
        Object[] toReturn = {data,str_message,event_type};
        return toReturn;
        
    }

    private int receiveByteBuffer(ByteOrder byteOrder) throws IOException
    {
        int read = inputStream.read();
        return read;
    }
    
    private int receiveIntBuffer(int bytes_nr, ByteOrder byteOrder) throws IOException
    {
        byte[] packet = new byte[bytes_nr];
        inputStream.read(packet); //the read bytes go in packet_size
//        for (byte b : packet)
//        {
//            System.out.format("0x%02X" , b);
//            System.out.println();
//        }
        ByteBuffer wrapped = ByteBuffer.wrap(packet).order(byteOrder);
        int info = wrapped.getInt();
        return info;
    }
    
    private float receiveFloatBuffer(int bytes_nr, ByteOrder byteOrder) throws IOException
    {
        byte[] packet = new byte[bytes_nr];
        inputStream.read(packet); //the read bytes go in packet_size
//        for (byte b : packet)
//        {
//            System.out.format("0x%02X" , b);
//            System.out.println();
//        }
        ByteBuffer wrapped = ByteBuffer.wrap(packet).order(byteOrder);
        float info = wrapped.getFloat();
        return info;
    }
    
    private long receiveLongBuffer(int bytes_nr, ByteOrder byteOrder) throws IOException
    {
        byte[] packet = new byte[bytes_nr];
        inputStream.read(packet); //the read bytes go in packet_size
//        for (byte b : packet)
//        {
//            System.out.format("0x%02X" , b);
//            System.out.println();
//        }
        ByteBuffer wrapped = ByteBuffer.wrap(packet).order(byteOrder);
        long info = wrapped.getLong();
        return info;
    }
    
    private String receiveStringBuffer(int bytes_nr) throws IOException
    {
        byte[] packet = new byte[bytes_nr];
        inputStream.read(packet); //the read bytes go in packet_size
        String info = new String(packet,"UTF-8");
        return info;
    }
    
    
    private float[][] parseDataMessage() throws IOException
    {
        float[][] data = null;
        receiveLongBuffer(BYTES_TIMESTAMP,BE); 
        receiveIntBuffer(BYTES_FRAMENR,BE);
        int compCount = receiveIntBuffer(BYTES_COMPONENT_COUNT,BE); // = 1 if requested current frame
        for (int count=0; count<compCount; count++)
        {
            receiveIntBuffer(BYTES_COMPONENT_SIZE,BE);
            int compType = receiveIntBuffer(BYTES_COMPONENT_TYPE,BE);
            if (compType!=6)
            {
                throw new IOException("requested data type not manageable by the parser");
            }
            int bodyCount = receiveIntBuffer(BYTES_COMPONENT_BODYCOUNT,BE);
            data = new float[bodyCount][6];
            receiveIntBuffer(BYTES_JUNK,BE);
            for (int b=0; b<bodyCount; b++)
            {
                for (int dof=0; dof<6; dof++)
                {
                    data[b][dof] = receiveFloatBuffer(BYTES_ONE_DOF_INFO,BE);
                }
            }
        }
        
        return data;
    }

    
    private void receiveWelcomeMessage() throws IOException
    {
        System.out.println("Parsing welcome message...");
        parseMessage();
    }    
    
    public void close() throws IOException
    {
        inputStream.close();
        outputStream.close();
        socket.close();
    }
    
    public boolean isSocketConnected() {
        return socket.isConnected();
    }
    
    public boolean isSocketClosed() {
        return socket.isClosed();
    }
    

    private float[][] parseDataComponent() throws IOException
    {
        return (float[][]) parseMessage()[0];
    }
    
    private String parseStringComponent() throws IOException
    {
        return (String) parseMessage()[1];
    }
    
    private String parseEventComponent() throws IOException
    {
        return (String) parseMessage()[2];
    }
    
    public static boolean isValidData(long data)
    {
        return !Float.isInfinite(data);
    }


    public static void main (String[] args) throws IOException, InterruptedException
    {
        Mocap mocap = new Mocap();
//        mocap.startupRTServer();
//        mocap.closeRTServer();
//        float[][] allBodies = mocap.requestAllBodies(true);
//        System.out.println("allBodies = " + allBodies[0][0]);
//        System.out.println("isInf? "+Float.isInfinite(allBodies[0][0]));
//        mocap.closeRTServer();
    }
}

