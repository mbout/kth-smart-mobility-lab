/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command_central;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;

/**
 *
 * @author Matteo
 */
public class MocapListenerThread implements Runnable
{
    private final long SLEEP_TIME = 50;
    
    private Thread thread;
    private final String threadName;
    private final Mocap mocap;
    private final BlockingQueue queue;
    
    public MocapListenerThread (String name, BlockingQueue q) throws IOException
    {
        threadName = name;
        queue = q;
        mocap = new Mocap();
//        mocap.requestAllBodies(false);
//        mocap.startupRTServer();
        System.out.println("THREAD MESSAGE: Created " + threadName);
    }
    
    @Override
    public void run()
    {
        System.out.println("THREAD MESSAGE: Running " +  threadName );
        try
        {
            while(true)
            {
//                System.out.println("fetching bodies");
                float[][] bodies = mocap.requestAllBodies(false);
                queue.put(bodies);
                System.out.println("");
                Thread.sleep(SLEEP_TIME);
            }        
        }
        catch (InterruptedException ex)
        {
            System.out.println("THREAD MESSAGE: Thread " +  threadName + " interrupted.");
        } catch (IOException ex)
        {
            System.out.println("THREAD MESSAGE: Error: " + ex.getMessage());
        }
    }
    
    public void start ()
    {
        System.out.println("THREAD MESSAGE: Starting " +  threadName );
        if (thread == null)
        {
           thread = new Thread (this, threadName);
           thread.start ();
        }
    }
    
    public void stop ()
    {
        System.out.println("THREAD MESSAGE: Stopping " + threadName);
        if (thread!=null)
        {
            thread.interrupt();
        }
    }


    
}
