import socket, sys, time, math, random, threading

sys.path.append('Lanelets')
import laneletmodule
import laneletlibrary


import vehiclesupervisorymodule
import vehiclecontrolmodule
import vehicleperceptionmodule
import MessageHandler
import bodyclasses

import numpy


# TODO
# add more to handle message function

class SmartVehicle(bodyclasses.WheeledVehicle):
	"This class is the Smart Vehicle class used by the Platooning Manager Module"
	"This class is supposed to encompass three other classes which are running in threads"
	"The three other classes are the SupervisoryModule, the PerceptionModule and the "
	"ControlModule"
	"This class manages all the three Modules at the same time, making sure they are able to"
	"communicate with each other"
	"This class also interfaces the vehicle with the SML World, receiving body readings and V2V, "
	"and outputting velocity and steering commands and V2V"



	length = 4.12 # Very carefully calculated through complex scientific observations and experiments
	width = 1.7 # Pure guesswork
	
	# Very caluculation

	def __init__(self, simulation_period, vehicle_id, osm_info, bodies_array, lane_start_point_index = 0):

		if not isinstance(vehicle_id, int):
			raise NameError('In class SmartVehicle: constructor: id must be an integer')

		bodyclasses.WheeledVehicle.__init__(self)

		self.bodies_array = bodies_array

		# Smart Vehicle can use the wifi/network, to achieve communications and agreement with other vehicles

		self.wifi_range = 0.

		self.v2v_wifi_input = []
		self.v2v_wifi_output = []
		self.v2v_network_input = []
		self.v2v_network_output = []

		self.commands = dict()
		self.commands['throttle'] = 0.0
		self.commands['steering'] = 0.0

		self.id = vehicle_id
		self.simulation_period = 1./simulation_period

		# Stores the input commands in the format:
		# [current_velocity_command, current_steering_command]
		self.input = [0, 0]
		self.desired_velocity = 30.0 + 10.0*random.random()

		# Variables to store the lane trajectories
		
		des_vel = 10.0

		# Definition of string constants
		self.RIGHT_LANE_STRING = "right_lane"
		self.CENTER_LANE_STRING = "center_lane"
		self.LEFT_LANE_STRING = "left_lane"
		
		# Initialization of the lane trajectorys
		self.traj = []
		self.right_lane_traj = []
		self.center_lane_traj = []
		self.left_lane_traj = []

		self.getLaneTrajectorys(osm_info)

		self.lane_traj_dict = {self.RIGHT_LANE_STRING: self.right_lane_traj, self.CENTER_LANE_STRING: self.center_lane_traj, self.LEFT_LANE_STRING: self.left_lane_traj}


		self.current_body_readings = None

		
		
		# current variables #####################################

		self.closest_objects = {}
		self.perception_grid = None
		self.perceived_objects = None
		
		self.desired_lane = self.RIGHT_LANE_STRING
		if self.id%2 == 0:
			self.desired_lane = self.CENTER_LANE_STRING

		self.traj = self.getLaneTrajectory(self.desired_lane)

		self.new_desired_lanes = []

		#########################################################

		# Message flags + message generator
		self.messenger = MessageHandler.MessageHandler(self)

		self.saved_obstacle_messages = {}
		self.emergency_velocity_flag = False

		self.saved_smartvehicle_ids = set()
		self.saved_wifi_messages = {}
		self.saved_network_messages = {}
		self.desired_merge_flag = None # will be a vehicle id
		self.merge_accept_flag = None # will be a vehicle id
		self.desired_merge_rel_coords = None # will be list of floats

		self.merge_request_flag = False # will be True/False
		self.merging_flag = False # will be True/False
		self.merge_distance = 9

		self.new_fwd_pair_partner = None
		self.new_bwd_pair_partner = None
		self.fwd_pair_partner = None
		self.bwd_pair_partner = None
		self.platooned_vehicle = False # True if vehicle belongs to a platoon

		self.STOM_flag = False

		# Private message flags exclusive
		self.preparing_STOM_flag = None # will be a vehicle id
		self.preparing_to_merge_flag = None # will be a vehicle id
		self.STOM_flag = None # will be a vehicle id
		self.merging_flag = None # will be a vehicle id

		# set through messages preparing and during platoon merge
		self.hold_to_merge = None
		self.merge_comparison_id = None
		self.upcoming_platoon_merge = None
		self.in_merging_platoon = None
		self.merge_prep_done = None
		self.platoon_STOM_partner = None
		self.clear_for_merge = None
		self.platoon_merge_request = None
		self.wait_for_STOM = None
		self.platoon_STOM = None
		self.platoon_new_lane = None


		# some platooning variables
		self.got_platoon_merge_initiation_from = None
		self.platoon_leader = False
		self.forward_partner_info = {}
		self.platoon_merge_length = 0
		self.counted_merging_vehicles = []
		self.merge_length_count_completed = False
		self.merge_length_count_timer = None
		#self.merge_length_count_completed_event = threading.Event()


		# Overtaking variables
		self.overtaking_vehicle_flag = False
		self.overtaking_original_lanes = [] # needs to be list for recursive overtaking
		self.overtaken_vehicles = [] # needs to be list for recursive overtaking
		self.overtaking_original_velocity = None # will be float
		self.new_desired_lanes = [] # for when a new lane is desired, but not currently available


		# parameters for merging distance
		self.maximum_merge_error = 2.
		self.maximum_merged_error = 2.

		# ACCC parameters, rest of them are in control module
		self.ACCC_ready = threading.Event()
		self.ACCC_ready_margin = 1.0
		self.ACCC_max_deviation = 4.
		
		# other events, internal flag initially false, unless expicitly set(), pun intended
		self.STOM_event = threading.Event()
		self.lanekeeping_ready = threading.Event()

		# lanekeeping margin
		self.lanekeeping_margin = 0.3

		# event waiter aborters
		self.send_platoon_merge_request_abort = False
		self.start_merge_abort = False
		self.send_STOM_abort = False
		self.fix_lane_change_abort = False
		self.send_platoon_merge_done_abort = False
		#self.send_merge_length_count_completed_abort = False

		# Creating the Supervisory Module, setting its rate and starting its thread
		self.supervisory_module = vehiclesupervisorymodule.VehicleSupervisoryModule( self.desired_velocity, self.desired_lane , self )
		self.supervisory_module_rate = 30.
		self.start_thread( self.supervisory_module_thread, args=([]) )

		# Creating the Control Module, setting its rate and starting its thread
		self.control_module = vehiclecontrolmodule.VehicleControlModule(self, self.bodies_array, self.id)
		self.control_module_rate = 20.
		time.sleep(0.02) # This sleep is just to try to make the prints more separate
		self.set_vehicle_on_trajectory_state(lane_start_point_index)
		self.start_thread( self.control_module_thread, args=([]) )

		
		"""
		# WARNING: function must come between initialization of control module and perception module
		success = self.getLaneTrajectorys(osm_info)

		# success = self.set_trajectory(temp_vehicle, self.osm_info, node_start = -1)

		if not success:
			# Could not find a suitable trajectory. Try a new vehicle
			print "Could not find a suitable trajectory. Try a new vehicle"
		"""


		# Creating the Perception Module, setting its rate and starting its thread:
	
		self.perception_module = vehicleperceptionmodule.VehiclePerceptionModule(self)
		self.perception_module_rate = 10.
		time.sleep(0.02) # This sleep is just to try to make the prints more separate
		self.start_thread( self.perception_module_thread, args=([]) )

		
		self.assignPerceptionModule()

		self.commandDict = {"merge-after": self.messenger.sendDesiredMerge,
				"merge-between": self.messenger.sendPlatoonMergeRequest,
				"ACCC-target": self.supervisory_module.setACCCTarget,
				"set-speed": self.supervisory_module.setVelocity}

		self.stateDict = {"des_vel": self.getDesVel,
				"fwd": self.getFwdPP,
				"nfwd": self.getNewFwdPP,
				"bwd": self.getBwdPP,
				"nbwd": self.getNewBwdPP}

		# Tips
		# self.state = [real_world_meters, radians]
		# print "self.state = " + str(self.state)
		# closeby_body_readings = [real_world_meters, radians]
		# print "closeby_body_readings = " + str(closeby_body_readings)
		# self.traj[0] in real_world_meters
		# self.traj[2] in radians
		# print "self.traj[2] = " + str(self.traj[2])

	def assignPerceptionModule(self):
		self.supervisory_module.perception_module = self.perception_module
		self.control_module.perception_module = self.perception_module

	def getLaneTrajectory(self, lane_traj_string):
		if self.lane_traj_dict:
			return self.lane_traj_dict[lane_traj_string]
		else:
			print "ERROR: no lane traj dict created"
			return None

	def getCurrentLaneTrajectory(self):
		if self.lane_traj_dict:
			return self.lane_traj_dict[self.desired_lane]

	def getCurrentLaneTrajectoryString(self):
		if self.desired_lane:
			return self.desired_lane
		else:
			return None

	def getLaneTrajectorys(self, osm_info):
		"""
		sets the lane trajectorys of the vehicle

		:osm_info: some kind of info
		:returns: boolean True

		"""
		start_id = -1
		node_start = -1

		# right_lane_node_id = -1543
		# center_lane_node_id = -1424
		# left_lane_node_id = -1297

		# node_id_list = [ osm_info.osm_node_list[idx].id for idx in xrange( len( osm_info.osm_node_list ) ) ]

		# right_lane_node_start_idx = node_id_list.index(right_lane_node_id)		
		right_lane_node_id = laneletlibrary.find_node_by_tag(osm_info.osm_node_dict, 'right_lane_node')
		# center_lane_node_start_idx = node_id_list.index(center_lane_node_id)
		center_lane_node_id = laneletlibrary.find_node_by_tag(osm_info.osm_node_dict, 'center_lane_node')
		# left_lane_node_start_idx = node_id_list.index(left_lane_node_id)
		left_lane_node_id = laneletlibrary.find_node_by_tag(osm_info.osm_node_dict, 'left_lane_node')


		points_per_meter = 5.

		[right_lane_traj_x, right_lane_traj_y] = laneletlibrary.get_closed_trajectory_from_node_id(right_lane_node_id, osm_info, points_per_meter)
		[center_lane_traj_x, center_lane_traj_y] = laneletlibrary.get_closed_trajectory_from_node_id(center_lane_node_id, osm_info, points_per_meter)
		[left_lane_traj_x, left_lane_traj_y] = laneletlibrary.get_closed_trajectory_from_node_id(left_lane_node_id, osm_info, points_per_meter)

		if len(right_lane_traj_x) == 0 or len(center_lane_traj_x) == 0 or len(left_lane_traj_x) == 0 :
			#Error, could not find a suitable trajectory
			# Can happen if a vehicle is in a node for trucks only
			print "PlatooningManager Error: could not find a suitable trajectory"
			return False

		desired_velocity = 10

		# vehicle.set_state_trajectory(traj_x, traj_y, desired_velocity)
		self.set_right_lane_trajectory( right_lane_traj_x, right_lane_traj_y, desired_velocity )
		self.set_center_lane_trajectory( center_lane_traj_x, center_lane_traj_y, desired_velocity )
		self.set_left_lane_trajectory( left_lane_traj_x, left_lane_traj_y, desired_velocity )



		return True



	def set_vehicle_on_trajectory_state(self, idx = 0):
		# This function is used to place the vehicle in the initial position
		# of the trajectory

		# TODO make so that this does not need to be 0
		idx = int(-self.id*50)
		if self.id%2 != 0:
			idx += -10
		idx += 100

		self.control_module.current_trajectory_id = idx

		if len( self.traj ) == 0:

			raise NameError('In class SmartVehicle: trying to use set_vehicle_on_trajectory_state when trajectory is not yet defined')

		self.x = self.traj[0][idx]
		self.y = self.traj[1][idx]
		self.yaw = math.degrees(self.traj[2][idx])

		self.z = 0.0
		self.roll = 0.0


	def set_initial_input(self, input_vector):	
		# To set up the initial input commands of the vehicle

		# In case self.input is empty
		if not self.input:

			for dimension in range( len( input_vector ) ):

				self.input.append( input_vector[dimension] )

		else:

			if len( self.input ) == len( input_vector ):

				for dimension in range( len( input_vector ) ):

					self.input[dimension] = input_vector[dimension]

			else:

				print "Error in SmartVehicle set_initial_input(): trying to set an input with wrong dimensions!"
	
	
	def set_right_lane_trajectory(self, traj_x, traj_y, desired_velocity):
		# Function used to define the right lane trajectory
		self.set_lane_trajectory( traj_x, traj_y, desired_velocity, self.RIGHT_LANE_STRING)

	def set_center_lane_trajectory(self, traj_x, traj_y, desired_velocity):
		# Function used to define the center lane trajectory
		self.set_lane_trajectory( traj_x, traj_y, desired_velocity, self.CENTER_LANE_STRING)

	def set_left_lane_trajectory(self, traj_x, traj_y, desired_velocity):
		# Function used to define the left lane trajectory
		self.set_lane_trajectory( traj_x, traj_y, desired_velocity, self.LEFT_LANE_STRING)

	def set_lane_trajectory(self, traj_x, traj_y, desired_velocity, lane_position):
		# Function used to set a lane trajectory
		
		traj_theta = []
		temp_theta = 0
		traj_time = []
		current_time = 0

		for idx in range( len ( traj_x ) - 1 ):

			delta_x = traj_x[idx+1] - traj_x[idx]
			delta_y = traj_y[idx+1] - traj_y[idx]

			temp_theta = math.atan2(delta_y, delta_x)
			traj_theta.append(temp_theta)

			traj_time.append(current_time)

			distance_moved = math.hypot(delta_x, delta_y) # Euclidean norm
			time_passed = distance_moved/desired_velocity

			current_time = current_time + time_passed

		traj_theta.append(temp_theta)
		traj_time.append(current_time)

		if len(traj_y) != len(traj_x) or len(traj_time) != len(traj_x) or len(traj_theta) != len(traj_x):

			raise NameError("SmartVehicle Trajectory creation resulted in a mistake!")

		if lane_position == self.RIGHT_LANE_STRING:

			self.right_lane_traj = [traj_x, traj_y, traj_theta, traj_time]
			self.np_right_lane_traj = numpy.asarray([ traj_x , traj_y , traj_theta])

		else:
		
			if lane_position == self.CENTER_LANE_STRING:

				self.center_lane_traj = [traj_x, traj_y, traj_theta, traj_time]
				self.np_center_lane_traj = numpy.asarray([ traj_x , traj_y , traj_theta])

			else:

				if lane_position == self.LEFT_LANE_STRING:

					self.left_lane_traj = [traj_x, traj_y, traj_theta, traj_time]
					self.np_left_lane_traj = numpy.asarray([ traj_x , traj_y , traj_theta])

				else:

					print "ERROR SmartVehicle: No suitable lane found!"

		return

	def get_current_state(self, radians = True):
		#Function to simply return the current state
		# returns [x, y, yaw]

		state_vector = []

		state_vector.append( self.x )
		state_vector.append( self.y )

		if radians:

			state_vector.append( self.yaw )

		else:

			state_vector.append( math.degrees(self.yaw) )

		return state_vector

	def get_current_input(self):
		#Function to simply return the current input commands
		input_vector = []

		for dimension in range( len( self.input ) ):

			input_vector.append( self.input[dimension] )

		return input_vector

	def supervisory_module_thread(self):

		# This is the function that will be running continuously on a thread
		# This function is supposed to manage/interface with the Supervisory Layer

		while True:

			start_time = time.time()

			# Run a step of the Supervisory Module
			self.supervisory_module.step()

			
			# Make the thread sleep and run with adequate cycle time
			self.make_thread_sleep(start_time, self.supervisory_module_rate,  "In SmartVehicle: supervisory_module_step thread is taking too long!", verbose = True)


	def control_module_thread(self):

		# This is the function that will be running continuously on a thread
		# This function is supposed to manage/interface with the Control Layer

		# Must wait until initialization of vehicle is complete

		while True:

			start_time = time.time()

			# self.control_module.state = self.state

			# Tell the control module the velocity it should have
			# self.control_module.desired_velocity = self.velocity

			# Tell the control module the desired lane
			self.control_module.desired_lane = self.desired_lane

			self.control_module.step()

			# self.input = [self.control_module.current_velocity_command, self.current_steering_command]

			# print "Smart Vehicle: self.input = " + str(self.input)

			# Make the thread sleep and run with adequate cycle time
			self.make_thread_sleep(start_time, self.control_module_rate,  "In SmartVehicle: control_module_step thread is taking too long!")

	def perception_module_thread(self):

		# This is the function that will be running continuously on a thread
		# This function is supposed to manage/interface with the Perception Layer

		while True:

			start_time = time.time()

			self.perception_module.step()

			# Make the thread sleep and run with adequate cycle time
			self.make_thread_sleep(start_time, self.perception_module_rate,  "In SmartVehicle: perception_module_step thread is taking too long!", verbose = True)


	def make_thread_sleep(self, start_time, desired_rate,  warning_string, verbose = True):

		# A simple function that will make the thread sleep (or the cycle wait)
		# in order to respect the desired rate.
		# It will also issue a warning in case the desired rate cannot be performed
		# (due to taking too much time)
		end_time = time.time()
		elapsed_time = end_time - start_time
		desired_cycle_time = 1./desired_rate
		sleep_time = desired_cycle_time - elapsed_time
		if sleep_time <= 0:
			if verbose:
				print warning_string
		else:
			time.sleep(sleep_time)

	def start_thread(self, handler, args=()):
		# Function that simplifies thread creation
		t = threading.Thread(target=handler, args=args)
		t.daemon = True
		# self.threads.append(t)
		t.start()


	def set_body_readings(self, body_readings):
		# This functions is called at a very high frequency from the platooning manager module
		# It simply updates the body_readings to the most current ones
		self.current_body_readings = body_readings

		self.control_module.current_body_readings = body_readings



	def get_low_level_inputs(self):
		# This functions is called at a very high frequency from the platooning manager module
		# It simply returns the current low level inputs
		# to the Inputs Commands Manager
	
		# return [self.current_velocity_command, self.current_steering_command]
		# print "self.input = " + str(self.input)
		return self.input

	def getPerceivedObject(self, identity):
		if identity in self.perceived_objects:
			return self.perceived_objects[identity]
		else:
			return None

	def receiveMessage(self, messageDict):
		self.messenger.receiveMessage(messageDict)

	def getState(self, params):
		state = []
		for param in params:
			if param in self.stateDict:
				tup = []
				tup.append(param)
				tup.append(self.stateDict[param]())
				state.append(tup)

		return state

############## getters for the stateDict ##########

	def getDesVel(self):
		return self.desired_velocity

	def getFwdPP(self):
		return self.fwd_pair_partner

	def getBwdPP(self):
		return self.bwd_pair_partner

	def getNewFwdPP(self):
		return self.new_fwd_pair_partner

	def getNewBwdPP(self):
		return self.new_bwd_pair_partner



###################################################
