
import socket, sys, time, math, random, threading, datetime


class IntersectionVehicleControlModule:
	"This class is the Platooning Vehicle class used by the Platooning Manager Module"
	def __init__(self, vehicle, bodies_array, vehicle_id):

		self.bodies_array = bodies_array
		self.vehicle_id = vehicle_id


		self.vehicle = vehicle
		self.current_body_readings = []
		self.desired_velocity = 0.
		self.state = []

		self.traj = self.vehicle.traj

		self.current_trajectory_id = 0

		# Variables to store the low level commands that will be
		# read by the SmartVehicle module and sent to the Simulator
		self.current_velocity_command = 0.
		self.current_steering_command = 0.

		self.perception_module = None
		self.perceived_objects = None

		self.ACCC = False
		self.CC = True
		self.lateral_following = False

		# variables for the accc to work
		self.ACCC_target_id = None
		self.ACCC_desired_distance = None
		self.ACCC_distance = None

		# The last time the ACCC controller was executed
		# Useful for computations of the Integral and Derivative part of the PID controller
		self.last_ACCC_time = []
		# The integrated error of the ACCC used for the Integral part of the PID controller
		self.ACCC_I_e = []
		# The last error of the ACCC, used for the Derivative part of the PID controller
		self.last_ACCC_e = []
	
		# ACCC PID gain
		self.ACCC_k_p = 5.
		self.ACCC_k_i = 1.
		self.ACCC_k_d = 0.

		#Tips
		# self.state = [real_world_meters, radians]
		# print "self.state = " + str(self.state)
		# closeby_body_readings = [real_world_meters, radians]
		# print "closeby_body_readings = " + str(closeby_body_readings)
		# self.traj[0] in real_world_meters
		# self.traj[2] in radians
		# print "self.traj[2] = " + str(self.traj[2])

		print "VehicleControlModule started"

	def step(self):

		# This is the step function that is running at each cycle of the thread
		# in SmartVehicle

		if self.vehicle_id in self.bodies_array:

			self.state = [self.bodies_array[self.vehicle_id].x, self.bodies_array[self.vehicle_id].y, math.radians(self.bodies_array[self.vehicle_id].yaw) ]


		# You should now be able to use self.getPerceivedObject(id) to fetch relative coordinates of the other vehicles

		if self.ACCC:
			self.updateACCCTarget()
			# print self.ACCC_distance
			

		# self.smooth_lane_change(self.left_lane_traj)
		if self.vehicle_id in self.bodies_array:

			[speed, steering] = self.compute_low_level_inputs()

			# For now we will be stopped
			# speed = 0.

			# print "InterSectionVehicleControlModule speed = " + str(speed)

			self.bodies_array[self.vehicle_id].commands['throttle'] = speed
			self.bodies_array[self.vehicle_id].commands['steering'] = steering

			# print "self.bodies_array[self.vehicle_id].yaw = " + str(self.bodies_array[self.vehicle_id].yaw)

			# if not self.bodies_array[self.vehicle_id].v2v_wifi_output:

				# print "ADDING MESSAGE"

			#self.bodies_array[self.vehicle_id].v2v_wifi_output = ['I am vehicle ' + str(self.vehicle_id) + ' at time ' + str( time.time() ) ]

			#self.bodies_array[self.vehicle_id].v2v_network_output = ['I am vehicle ' + str(self.vehicle_id) + ' at time ' + str( time.time() ) ]

			# print "self.bodies_array[self.vehicle_id].sensor_readings = " + str(self.bodies_array[self.vehicle_id].sensor_readings)

			# if self.vehicle_id == -4:

			# 	print str(time.time()) + ": Vehicle WIFI" + str(self.vehicle_id) + " is receiving : " + str( self.bodies_array[self.vehicle_id].v2v_wifi_input )
			# 	print str(time.time()) + ": Vehicle NETWORK" + str(self.vehicle_id) + " is receiving : " + str( self.bodies_array[self.vehicle_id].v2v_network_input )




		# print "Control Module Step"
		# print "Control Module Step: self.state = " + str(self.state)

	def updateACCCTarget(self):
		target_coords = self.vehicle.getPerceivedObject(self.ACCC_target_id)
		if target_coords:
			self.ACCC_distance = target_coords[0]
		



	def find_closest_trajectory_point(self, state, current_idx, number_points_ahead, trajectory_to_search = None):
		# This function will look for the closest point (in the trajectory) to state.
		# The points in the trajectory that are searched range from current_idx to current_idx + number_points_ahead 
		# (trajectory indexes wrap around)

		# print "find_closest_trajectory_point()"

		
		search_range = range(0, number_points_ahead)



		# print "state = " + str(state)

		# print "self.traj[1] = " + str(self.traj[1])
		# self.traj[1] comes in real world metres

		# print "self.traj[2] = " + str(self.traj[2])
		# self.traj[2] comes in radians


		if not trajectory_to_search:
			# print "Default self.traj"
			trajectory_to_search = self.traj
			# temp_distance = [math.hypot( state[0]  - self.traj[0][ (current_idx+i)%traj_len ] , state[1] - self.traj[1][ (current_idx+i)%traj_len ] ) for i in search_range]
			
		whole_search_range = range(len(trajectory_to_search[0]))
		traj_len = len( trajectory_to_search[0] )
		temp_distance = [math.hypot( state[0]  - trajectory_to_search[0][ (i)%traj_len ] , state[1] - trajectory_to_search[1][ (i)%traj_len ] ) for i in whole_search_range]

		# Find the closest trajectory point that matches my desired speed and current heading
		best_distance = min(temp_distance)
		best_idx = temp_distance.index(best_distance)

		return [best_distance, best_idx]



	def compute_low_level_inputs(self):
		# This function simply returns the current low level inputs
		# to the Inputs Commands Manager
		# print "--------------------------"
		# print "compute_low_level_inputs()"

		closeby_body_readings = []

		# print "self.current_body_readings = " + str(self.current_body_readings)

		if self.current_body_readings:

			for temp_body_reading in self.current_body_readings['readings']:

				temp_closeby_body_reading = [ temp_body_reading[0]*32. , temp_body_reading[1]*32. , math.radians( temp_body_reading[2] ) ]

				distance_to_body = math.hypot( self.state[0] - temp_closeby_body_reading[0] , self.state[1] - temp_closeby_body_reading[1] )

				# print "distance_to_body = " + str(distance_to_body)

				if distance_to_body < 30.0:

					closeby_body_readings.append(temp_closeby_body_reading)

		desired_velocity = self.desired_velocity

		current_idx = self.current_trajectory_id

		number_points_ahead = 25

		# print "self.state = " + str(self.state)
		# self.state = [-39.44159273512578, -38.36738366701975, -2.3967515204479533]
		# self.state comes in real world metres and radians

		[best_distance, best_idx] = self.find_closest_trajectory_point( self.state, current_idx, number_points_ahead)

		current_closest_trajectory_point = best_idx

		traj_len = len( self.traj[0] )

		best_idx += 5

		reference_state = [self.traj[0][ (best_idx)%traj_len ] , self.traj[1][ (best_idx)%traj_len ] , self.traj[2][ (best_idx)%traj_len ] ]
		# print "reference_state = " + str(reference_state)
		# print "self.state = " + str(self.state)

		# print "get_trajectory_tracking_inputs reference_state = " + str(reference_state)

		# converted_state = [self.state[0]/32., self.state[1]/32., self.state[2]]
		[velocity_command, steering_command] = self.controller_action(self.state, reference_state)

		best_idx += 5

		reference_state = [self.traj[0][ (best_idx)%traj_len ] , self.traj[1][ (best_idx)%traj_len ] , self.traj[2][ (best_idx)%traj_len ] ]

		# print "reference_state = " + str(reference_state)

		# print "get_trajectory_tracking_inputs reference_state = " + str(reference_state)

		# converted_state = [self.state[0]/32., self.state[1]/32., self.state[2]]
		[velocity_command, steering_command] = self.controller_action(self.state, reference_state)

		# print "[velocity_command, steering_command] = " + str([velocity_command, steering_command])
	
		# print "Control Module: [velocity_command, steering_command] = " + str([velocity_command, steering_command])

		self.current_velocity_command = velocity_command
		self.current_steering_command = steering_command
		# Steering command should be outputteed in DEGREES

		self.current_trajectory_id = current_closest_trajectory_point

		return [velocity_command, steering_command]


	def get_velocity_ACCC(self):
		# This function is called when the ACCC mode is on

		# It simply runs a PID that outputs the longitudinal velocity
		# The error source of this PID is the error in the current distance
		# to the front vehicle, agains the desired distance

		if self.ACCC_distance:
			current_error = self.ACCC_distance - self.ACCC_desired_distance
		else:
			return self.desired_velocity

		#dt = datetime.now()
		#current_time = dt.
		current_time = time.time()


		if self.last_ACCC_time:

			# Compute last sampling time
			time_passed = current_time - self.last_ACCC_time
			self.last_ACCC_time = current_time

			# Integrate the error
			self.ACCC_I_e += time_passed*current_error

			# Compute the derivative of the error
			self.derivative_error = (current_error - self.last_ACCC_e)/time_passed

			# Compute PID output
			velocity_ACCC = self.desired_velocity + self.ACCC_k_p*current_error + self.ACCC_k_i*self.ACCC_I_e + self.ACCC_k_d*self.derivative_error

		else:

			# Initialization of the ACCC controller (first iteration of this controller)
			self.last_ACCC_time = current_time
			self.last_ACCC_e = current_error
			self.ACCC_I_e = 0.0
			self.derivative_error = 0.0

			velocity_ACCC = self.desired_velocity

		return velocity_ACCC

	def reset_ACCC(self):
		# To make sure that the ACCC is reseted when it is not used
		self.last_ACCC_time = []
		self.ACCC_I_e = []
		self.last_ACCC_e = []
		
	def controller_action(self, current_state, current_reference):

		tracking_error = self.tracking_error(current_state, current_reference)
		# print "tracking_error = " + str(tracking_error) 
		while tracking_error[2] > math.pi:
			tracking_error[2] = tracking_error[2] - math.pi
		while tracking_error[2] < -math.pi:
			tracking_error[2] = tracking_error[2] + math.pi
		# current_state[2] comes in radians
		# current_reference[2] comes in radians
		# current_state[2] = math.radians( current_state[2] )
		# print "current_state = " + str(current_state) 
		# print "current_reference = " + str(current_reference)
		# current_reference[2] comes in radians
		# print "tracking_error = " + str(tracking_error)
		# velocity_command = tracking_error[0]*0.01
		# velocity_command = 0.35

		# print "self.ACCC = " + str(self.ACCC)
		# Simulator assumes that input velocity comes as SML velocity!
		if self.ACCC:
			velocity_command = self.get_velocity_ACCC()
		else:
			self.reset_ACCC()
			velocity_command = self.desired_velocity
		# print "VehicleControlModule() : velocity_command = " + str(velocity_command)
		# print "VehicleControlModule() : self.desired_velocity = " + str(self.desired_velocity)
		# velocity_command = velocity_command/32.

		steering_command = 8.0* (10.0*tracking_error[1] + 15.0*tracking_error[2])

		# print "\n\n\n\nsteering_command = " + str(steering_command) 

		# steering_command = -steering_command
		# max_steering_amplitude = 100
		# steering_command = max( min(max_steering_amplitude, steering_command) , -max_steering_amplitude)
		# print "steering_command = " + str(steering_command) 


		# Working well with the normal Scania city
		# steering_command = 8.0* (10.0*tracking_error[1] + 10.0*tracking_error[2])

		# steering_command_max = math.radians(20)

		# if steering_command > steering_command_max:
		# 	# print "Max steering reached"
		# 	steering_command = steering_command_max

		# if steering_command < -steering_command_max:
		# 	# print "Max steering reached"
		# 	steering_command = -steering_command_max

		command_inputs = [velocity_command, steering_command]
		# print "command_inputs = " + str(command_inputs)
		return command_inputs

	def tracking_error(self, current_state, current_reference):

		error_x = math.cos( current_state[2] )*(current_reference[0] - current_state[0]) + math.sin( current_state[2] )*(current_reference[1] - current_state[1])
		error_y = -math.sin( current_state[2] )*(current_reference[0] - current_state[0]) + math.cos( current_state[2] )*(current_reference[1] - current_state[1])
		error_theta = current_reference[2] - current_state[2]

		# print "current_reference[2] = " + str(current_reference[2])
		# print "current_state[2] = " + str(current_state[2])

		# print "error_theta = " + str(error_theta)

		while error_theta > math.pi:
			error_theta -= 2*math.pi
		while error_theta < -math.pi:
			error_theta += 2*math.pi

		# print "error_theta = " + str(error_theta)


		return [error_x, error_y, error_theta]
		# error_x = math.cos( current_state[2] )*(current_reference[0] - current_state[0]) + math.sin( current_state[2] )*(current_reference[1] - current_state[1])

