# import xmlcomms

# import laneletvisualisation
# import customclasses
# import pygame
import sys, time, math, random, threading
import multiprocessing
import bodyclasses
# import xml.etree.ElementTree as ET


# sys.path.append('..\Lanelets')
# import laneletlibrary


from multiprocessing import Pool

sys.path.append('Lanelets')
import laneletmodule
import laneletlibrary



class VehicleSimulator:
	"This class implements the Vehicle Simulator Module to be running in the SML World"

	def __init__(self, bodies_array, osm_info, simulation_period):

		self.osm_info = osm_info

		self.CLOSE = False

		self.vehicles = []

		self.iteration = 0

		self.vehicles_dict = dict()

		self.inputs = dict()

		# self.thread_rate = 50.
		self.thread_rate = simulation_period

		self.construction_time = time.time()

		self.simulation_step_counter = 0

		# This is the bodies array, sent from commands manager module
		# It gives us access to all of the current bodies
		# Chaning this dictionary, will produce changes in the
		# bodies array in the world class
		self.bodies_array = bodies_array


	def start_simulating_vehicles(self):
		# This function creates the independent vehicle threads
		# which will run the control loop and simulate the vehicles
		for vehicle in self.vehicles:

			self.vehicles_dict[vehicle.id] = vehicle

			print "Starting vehicle number " + str(vehicle.id)

			self.start_thread( self.vehicle_step,  args=([vehicle]) )

	def start_controlling_vehicles(self):
		# This function is an alternative to the start_simulating_vehicles()
		# function. This function will create independent vehicle threads which
		# will simply run the control loop and compute the actuator signals
		for vehicle in self.vehicles:

			self.vehicles_dict[vehicle.id] = vehicle

			print "Starting vehicle number " + str(vehicle.id)

			self.start_thread( self.vehicle_control_step,  args=([vehicle]) )

	def start_thread(self, handler, args=()):
		t = threading.Thread(target=handler, args=args)
		t.daemon = True
		# self.threads.append(t)
		t.start()

	
	def vehicle_control_step(self, vehicle):
		# This is an alternative to the vehicle_step() function
		# that runs on an independent thread function running
		# for each vehicle.
		# This function simply uses a controller to compute the 
		# desired actuator signals of the vehicle

		# print 'Started controlling vehicle ' + str(vehicle.id)
		
		# print "vehicle_control_step--------------------------"

		vehicle.set_vehicle_on_trajectory_state(0)

		while not self.CLOSE:

			tic_thread_step =  time.time()
			current_time = tic_thread_step - self.construction_time

			vehicle.vehicle_iteration = vehicle.vehicle_iteration + 1

			current_vehicle_state = []


			# I will get my current state from the bodies_array

			read_timer = time.time()

			my_body = self.bodies_array[vehicle.id]
			vehicle.state = [ my_body.x, my_body.y, math.radians( my_body.yaw ) ]

			read_time = time.time() - read_timer
			# print "read_time = " + str(read_time) 

			control_timer = time.time()


			no_readings_flag = True

			sensor_readings = my_body.sensor_readings

			# if vehicle.id == -2:
			# 	print "sensor_readings = " + str(sensor_readings) 

			[vehicle_velocity, vehicle_steering] = vehicle.get_trajectory_tracking_inputs( current_time , sensor_readings)

			control_time = time.time() - control_timer
			# print "control_time = " + str(control_time) 

			write_timer = time.time()

			# Now that I have computed my desired commands, I will overwrite them in the world class object
			# self.bodies_array[vehicle.id].commands = [vehicle_velocity, vehicle_steering] 
			self.bodies_array[vehicle.id].commands['throttle'] = vehicle_velocity*32.
			self.bodies_array[vehicle.id].commands['steering'] = vehicle_steering

			write_time = time.time() - write_timer
			# print "write_time = " + str(write_time) 

			toc_thread_step = time.time()

			

			if toc_thread_step - tic_thread_step > 0:
				current_rate = (1. / (toc_thread_step - tic_thread_step))
				if current_rate > self.thread_rate:
					sleeping_time = 1. / self.thread_rate - 1. / current_rate
					time.sleep(sleeping_time)
				else:

					print "vehicle.id = " + str(vehicle.id) + " will not sleep"
					# print "toc_thread_step - tic_thread_step = " + str(toc_thread_step - tic_thread_step)
					pass
			else:
				sleeping_time = 1. / self.thread_rate
				time.sleep(sleeping_time)


		print 'Stopped vehicle ' + str(vehicle.id)

	def step(self, simulation_period, simulator_is_closing = False):
		# Step is the original step function which computes actuator signals using a controller
		# and then simulates said actuator signals.

		self.simulation_step_counter = self.simulation_step_counter + 1

		self.CLOSE = simulator_is_closing

		self.thread_rate = simulation_period

		current_vehicle_states = []

		for vehicle_tag in self.vehicles_dict:

			current_vehicle = self.vehicles_dict[vehicle_tag]

			current_state_degrees = current_vehicle.get_current_state(radians = False)
			current_vehicle_state = {'x': current_state_degrees[0]/32., 'y': current_state_degrees[1]/32.,'z': 0, 'roll': 0 ,'pitch': 0,'yaw': current_state_degrees[2] , 'id': current_vehicle.get_id()}

			current_vehicle_states.append(current_vehicle_state)

		return current_vehicle_states

	def command_step(self, simulation_period, simulator_is_closing = False):
		# Command Step is an alternative to the original step function. This command_step
		# function only computes the actuator signals without actually simulating the body

		self.simulation_step_counter = self.simulation_step_counter + 1

		self.CLOSE = simulator_is_closing

		self.thread_rate = simulation_period
		self.thread_rate = 10

		current_vehicle_inputs = dict()

		for input_tag in self.inputs:

			current_input = dict()

			current_input['throttle'] = self.inputs[input_tag][0]
			current_input['steering'] = self.inputs[input_tag][1]
			
			# current_state_degrees = current_vehicle.get_current_state(radians = False)
			# current_vehicle_state = {'x': current_state_degrees[0]/32., 'y': current_state_degrees[1]/32.,'z': 0, 'roll': 0 ,'pitch': 0,'yaw': current_state_degrees[2] , 'id': current_vehicle.get_id()}

			current_vehicle_inputs[input_tag] = current_input
			
		return current_vehicle_inputs

	def get_vehicle_inputs_step(self, bodies_readings, simulation_period, simulator_is_closing = False):

		self.simulation_step_counter = self.simulation_step_counter + 1

		self.CLOSE = simulator_is_closing

		self.bodies_readings = bodies_readings

		self.thread_rate = simulation_period

		current_vehicle_inputs = []

		for vehicle_tag in self.input:

			current_input = self.input[vehicle_tag]

			# current_state_degrees = current_vehicle.get_current_state(radians = False)
			# current_vehicle_state = {'x': current_state_degrees[0]/32., 'y': current_state_degrees[1]/32.,'z': 0, 'roll': 0 ,'pitch': 0,'yaw': current_state_degrees[2] , 'id': current_vehicle.get_id()}

			current_vehicle_inputs.append(current_input)

		return current_vehicle_inputs

	def simulate_inputs(self, inputs, simulator_is_closing = False):

		current_time = time.time()

		self.simulation_step_counter = self.simulation_step_counter + 1

		self.CLOSE = simulator_is_closing

		self.thread_rate = simulation_period

		current_vehicle_states = []

		input_ids = inputs.keys()

		for vehicle_tag in self.vehicles_dict:
			
			current_vehicle = self.vehicles_dict[vehicle_tag]

		 	
		 	if current_vehicle.id in input_ids:

		 		input_vector = inputs[current_vehicle.id]

		 		current_vehicle.set_current_input(input_vector)
		 		current_vehicle.simulate(current_time)
		 	else:

		 		print "Did not find input for current vehicle"

			# current_vehicle = self.vehicles_dict[vehicle_tag]

			current_state_degrees = current_vehicle.get_current_state(radians = False)
			current_vehicle_state = {'x': current_state_degrees[0]/32., 'y': current_state_degrees[1]/32.,'z': 0, 'roll': 0 ,'pitch': 0,'yaw': current_state_degrees[2] , 'id': current_vehicle.get_id()}

			current_vehicle_states.append(current_vehicle_state)

		return current_vehicle_states

	def test_parallel(self, number_to_square):

		return number_to_square*number_to_square

	def get_trucks_in_collision(self, bodies_list):

		# print '---'

		real_bodies = []

		# print 'bodies_list = ' + str(bodies_list)

		for body in bodies_list:

			if body['id'] > 0:

				# Orientations are coming in degrees
				real_bodies.append( [ 32.0*body['x'], 32.0*body['y'], body['yaw'], body['id'] ] )


		safety_distance = 2.0

		bodies_in_collision = []

		for body in real_bodies:

			colliding = False			

			for current_vehicle in self.vehicles:

				vehicle_state = current_vehicle.get_current_state()

				for distance_ahead in range(1,17):

					x_front = body[0] + 0.5*float(distance_ahead)*math.cos( math.radians( body[2] ) )
					y_front = body[1] + 0.5*float(distance_ahead)*math.sin( math.radians( body[2] ) )

					if math.hypot( x_front - vehicle_state[0] , y_front - vehicle_state[1] ) < safety_distance:

						bodies_in_collision.append( body[3] )
						colliding = True
						# print body[3]
						# print 'Body in Collision'
						break

				if colliding:
					break

		# print 'bodies_in_collision' + str(bodies_in_collision)

		return bodies_in_collision

	def whatevs(self, current_vehicle, current_vehicle_states, body_readings, full_stop = False):

		tic = time.time()

		current_time = time.time() - self.construction_time

		success = False

		if not full_stop:

			if current_vehicle.get_id() != -300:

				success = current_vehicle.fake_controller_trajectory_tracking( current_time , body_readings)

		current_state_degrees = current_vehicle.get_current_state(radians = False)
		current_vehicle_state = {'x': current_state_degrees[0]/32., 'y': current_state_degrees[1]/32.,'z': 0, 'roll': 0 ,'pitch': 0,'yaw': current_state_degrees[2] , 'id': current_vehicle.get_id()}
		current_vehicle_states.append(current_vehicle_state)

		return current_vehicle_states

	def create_random_vehicles(self, simulation_period, vehicle_id):

		vehicle_number = 0

		while 1:

			temp_vehicle = vehicle.Vehicle(simulation_period, vehicle_id)

			state_vector = [0, 0, 0]
			temp_vehicle.set_initial_state(state_vector)

			input_vector = [0, 0]
			temp_vehicle.set_initial_input(input_vector)

			success = self.set_trajectory(temp_vehicle, self.osm_info, node_start = -1)

			if not success:
				# Could not find a suitable trajectory. Try a new vehicle
				continue

			minimum_distance = 2.0

			safe_to_add = True

			for other_vehicle in self.vehicles:

				other_vehicle_state = other_vehicle.get_current_state()
				current_vehicle_state = temp_vehicle.get_current_state()

				if math.hypot( current_vehicle_state[0] - other_vehicle_state[0] , current_vehicle_state[1] - other_vehicle_state[1] ) < minimum_distance:

					#print "TOO CLOSE"
					safe_to_add = False

					break

			if not safe_to_add:
				continue

			# The new vehicle was created and added to our list of dummy vehicles
			self.vehicles.append(temp_vehicle)

			# We need to create the corresponding body object in the world class bodies_array
			new_body = bodyclasses.DummyVehicle()

			new_body.id = vehicle_id

			new_body.x = temp_vehicle.state[0]
			new_body.y = temp_vehicle.state[1]
			new_body.yaw = temp_vehicle.state[2]

			new_body.commands = dict()
			new_body.commands['throttle'] = temp_vehicle.input[0]
			new_body.commands['steering'] = temp_vehicle.input[1]

			self.bodies_array[vehicle_id] = new_body

			return

	def set_trajectory(self, vehicle, osm_info, node_start = -1):

		start_id = -1

		if node_start == -1:
		
			start_id = osm_info.osm_node_list[ random.randrange( len(osm_info.osm_node_list) ) ].id

		else:

			start_id = osm_info.osm_node_list[ node_start ].id

		points_per_meter = 5.

		[traj_x, traj_y] = laneletlibrary.get_closed_trajectory_from_node_id(start_id, osm_info.osm_lanelet_list, osm_info.osm_way_list, osm_info.osm_node_list, points_per_meter)

		if len(traj_x) == 0:
			#Error, could not find a suitable trajectory
			# Can happen if a vehicle is in a node for trucks only
			return False

		if math.hypot( traj_x[0] - traj_x[-1] , traj_y[0] - traj_y[-1] ) > 10.0:
			return False

		desired_velocity = 10

		vehicle.set_state_trajectory(traj_x, traj_y, desired_velocity)

		vehicle.set_vehicle_on_trajectory_state()

		return True

