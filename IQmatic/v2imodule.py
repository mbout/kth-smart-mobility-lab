import time, sys, math
# import pip

# import shapely
# from shapely.geometry import mapping, shape
from shapely.geometry import Polygon
# import shapely

sys.path.append('Lanelets')
# import laneletmodule
import laneletlibrary

class v2iManager:
	"This class is infrastructure intelligence manager"

	def __init__(self, lanelet_module):

		tic = time.time()

		[self.lanelet_polygons, self.lanelet_ids] = self.create_lanelet_polygons(lanelet_module)

		elapsed_time = time.time() - tic

		#print "Constructed V2I Module in " + str(elapsed_time) + " seconds."
	
	def step(self, bodies_list):

		obstacles_list = [body for body in bodies_list if ( body.has_key('obstacle') and body['obstacle'] == True ) ]

		obstacles_ids_list = [body['id'] for body in bodies_list if ( body.has_key('obstacle') and body['obstacle'] == True ) ]
		obstacles_pos_list = [[body['x'], body['y']] for body in bodies_list if ( body.has_key('obstacle') and body['obstacle'] == True ) ]

		# print "\n STEP \n"
		# print "obstacles_ids_list = " + str(obstacles_ids_list)
		# print "obstacles_pos_list = " + str(obstacles_pos_list)

		obstacle_polygons = self.create_obstacle_polygons(obstacles_list)

		lanelet_collision_ids = self.get_lanelet_colisions( obstacle_polygons )

		# if len( lanelet_collision_ids ) == 0:
		
		# 	return None

		return lanelet_collision_ids

		# obstacle['type'] == 'circle'
		# obstacle['radius']
		# obstacle['type'] == 'rectangle'
		# obstacle['width']
		# obstacle['height']

		# print "v2vManager.step()"

		# print bodies_list


	def get_lanelet_colisions( self, obstacle_polygons ):

		lanelet_collision_ids = []

		for obstacle_polygon in obstacle_polygons:

			for idx, lanelet_polygon in enumerate( self.lanelet_polygons ):

				if not obstacle_polygon.disjoint(lanelet_polygon):

					lanelet_collision_ids.append( self.lanelet_ids[idx] )


		# print str( len( lanelet_collision_ids ) ) + " lanelet(s) is(are) in collision"

		if len( lanelet_collision_ids ) != 0:
			pass
			#print "Lanelets obstructed: " + str( lanelet_collision_ids )

		return lanelet_collision_ids



	def create_obstacle_polygons(self, obstacles_list):

		obstacle_polygons = []

		for obstacle in obstacles_list:

			obstacle_point_tuple_list = []

			# print "obstacle['type'] = " + str(obstacle['type'])

			if obstacle['obstacle_type']in ['circle','gun']:

				num_points_circle = 10
				center_x = obstacle['x']*32.0
				center_y = obstacle['y']*32.0
				radius = obstacle['radius']*32.0
				
				for num_point in range( num_points_circle ):

					point_x = center_x + radius*math.cos( ( float(num_point)/float(num_points_circle) )*2*math.pi )
					point_y = center_y + radius*math.sin( ( float(num_point)/float(num_points_circle) )*2*math.pi )

					obstacle_point_tuple_list.append( (point_x, point_y) )

				obstacle_polygons.append( Polygon( obstacle_point_tuple_list ) )

			elif obstacle['obstacle_type'] == 'rectangle':

				center_x = obstacle['x']*32.0
				center_y = obstacle['y']*32.0
				yaw = obstacle['yaw']

				width = obstacle['width']*32.0
				height = obstacle['height']*32.0

				obstacle_point_tuple_list = []

				point_x = center_x + (width/2.0)*math.cos( math.radians(yaw) ) - (height/2.0)*math.sin( math.radians(yaw) )
				point_y = center_y + (width/2.0)*math.sin( math.radians(yaw) ) + (height/2.0)*math.cos( math.radians(yaw) )

				obstacle_point_tuple_list.append( (point_x, point_y) )

				point_x = center_x + (width/2.0)*math.cos( math.radians(yaw) ) - (-height/2.0)*math.sin( math.radians(yaw) )
				point_y = center_y + (width/2.0)*math.sin( math.radians(yaw) ) + (-height/2.0)*math.cos( math.radians(yaw) )

				obstacle_point_tuple_list.append( (point_x, point_y) )

				point_x = center_x + (-width/2.0)*math.cos( math.radians(yaw) ) - (-height/2.0)*math.sin( math.radians(yaw) )
				point_y = center_y + (-width/2.0)*math.sin( math.radians(yaw) ) + (-height/2.0)*math.cos( math.radians(yaw) )

				obstacle_point_tuple_list.append( (point_x, point_y) )

				point_x = center_x + (-width/2.0)*math.cos( math.radians(yaw) ) - (height/2.0)*math.sin( math.radians(yaw) )
				point_y = center_y + (-width/2.0)*math.sin( math.radians(yaw) ) + (height/2.0)*math.cos( math.radians(yaw) )

				obstacle_point_tuple_list.append( (point_x, point_y) )

				obstacle_polygons.append( Polygon( obstacle_point_tuple_list ) )

			else:
				pass
				#print "Obstacle type not handled yet: obstacle['type'] = " + str(obstacle['type']) 

				#print "Obstacle type not handled yet: obstacle['type'] = " + str(obstacle['type']) 
			

		return obstacle_polygons

	def get_new_polygon_lanelets(self, prev_obstructed_lanelets, lanelet_collision_ids):

		new_polygon_lanelets = []

		freed_lanelets = [lanelet_id for lanelet_id in prev_obstructed_lanelets if lanelet_id not in lanelet_collision_ids ]
		
		for lanelet_id in freed_lanelets:

			temp_polygon_lanelet = { 'id' : lanelet_id, 'obstructed' : False }

			new_polygon_lanelets.append(temp_polygon_lanelet)
			# temp_polygon_lanelet = { 'id' : idx , 'obstructed' : True , 'points' : points_list}


		new_lanelets = [lanelet_id for lanelet_id in lanelet_collision_ids if lanelet_id not in prev_obstructed_lanelets ]

		for lanelet_id in new_lanelets:

			points_list = self.get_lanelet_polygon_points(lanelet_id)

			temp_polygon_lanelet = { 'id' : lanelet_id, 'obstructed' : True , 'points' : points_list}

			new_polygon_lanelets.append(temp_polygon_lanelet)


		return new_polygon_lanelets

	def get_lanelet_polygon_points(self, lanelet_id):

		polygon_shapely_points = []

		for idx, lanelet_id_it in enumerate( self.lanelet_ids ):

			if lanelet_id_it == lanelet_id:

				lanelet_polygon = self.lanelet_polygons[idx]

				# polygon_shapely_points = list( lanelet_polygon.boundary )
				# print "lanelet_polygon = " + str(lanelet_polygon)
				# print "lanelet_polygon.boundary = " + str(lanelet_polygon.boundary)
				# print "lanelet_polygon.boundary.coords = " + str(lanelet_polygon.boundary.coords)
				# print "lanelet_polygon.boundary.coords[:] = " + str(lanelet_polygon.boundary.coords[:])

				break

		points_list = []

		for shapely_point in lanelet_polygon.boundary.coords[:]:

			# print [ shapely_point[0], shapely_point[1] ]
			points_list.append( [ shapely_point[0], shapely_point[1] ] )

		return points_list


	def create_lanelet_polygons(self, lanelet_module):

		lanelet_polygons_list = []
		lanelet_ids_list = []

		for lanelet in lanelet_module.osm_lanelet_list:

			lanelet_ids_list.append( lanelet.id )

			left_node_ids = lanelet.left_osm_way.node_ids
			right_node_ids = lanelet.right_osm_way.node_ids

			left_node_positions = []
			right_node_positions = []

			for idx in range( len( left_node_ids ) ):

				node = laneletlibrary.get_osm_node_by_id( lanelet_module.osm_node_dict, left_node_ids[idx] )
				left_node_positions.append( [node.x, node.y] )

				node = laneletlibrary.get_osm_node_by_id( lanelet_module.osm_node_dict, right_node_ids[idx] )
				right_node_positions.append( [node.x, node.y] )


			original_distance =  math.hypot( left_node_positions[0][0] - right_node_positions[0][0] , left_node_positions[0][1] - right_node_positions[0][1] )
			flipped_distance =  math.hypot( left_node_positions[0][0] - right_node_positions[-1][0] , left_node_positions[0][1] - right_node_positions[-1][1] )

			if original_distance < flipped_distance:

				right_node_positions.reverse()

			left_node_positions.extend(right_node_positions)

			point_tuple_list = []

			for temp_point in left_node_positions:

				point_tuple_list.append( (temp_point[0], temp_point[1]) )

			lanelet_polygon = Polygon(point_tuple_list)

			lanelet_polygons_list.append(lanelet_polygon)

			# node_ids.extend( lanelet.left_osm_way )


		return [lanelet_polygons_list, lanelet_ids_list]



# installed_packages = pip.get_installed_distributions()
# installed_packages_list = sorted(["%s==%s" % (i.key, i.version)
#      for i in installed_packages])
# print(installed_packages_list)

# 