import sys, time, math, random, threading
import multiprocessing
# import xml.etree.ElementTree as ET


# sys.path.append('..\Lanelets')
# import laneletlibrary


from multiprocessing import Pool

sys.path.append('Lanelets')
import laneletmodule
import laneletlibrary

import bodyclasses
# import platooningvehicle
import smartvehicle



class PlatooningManager:
	"This class implements the Platooning Manager Module which will be an interface for the Platooning Vehicles"

	def __init__(self, bodies_array, osm_info, simulation_period):

		self.osm_info = osm_info

		self.CLOSE = False

		self.vehicles = []

		self.iteration = 0

		self.vehicles_dict = dict()

		self.bodies_list = []

		self.inputs = dict()

		# self.thread_rate = 50.
		self.thread_rate = simulation_period

		self.construction_time = time.time()

		self.bodies_readings = []

		self.simulation_step_counter = 0

		self.bodies_array = bodies_array

	def create_platooning_vehicle(self, simulation_period, vehicle_id, start_index = 0):

		vehicle_number = 0

		while 1:

			# temp_vehicle = platooningvehicle.PlatooningVehicle(simulation_period, vehicle_id)
			temp_vehicle = smartvehicle.SmartVehicle(simulation_period, vehicle_id, self.osm_info, self.bodies_array, start_index)

			state_vector = [0, 0, 0]

			input_vector = [0, 0]
			temp_vehicle.set_initial_input(input_vector)
			minimum_distance = 2.0

			safe_to_add = True

			for other_vehicle in self.vehicles:

				other_vehicle_state = other_vehicle.get_current_state()
				current_vehicle_state = temp_vehicle.get_current_state()

				if math.hypot( current_vehicle_state[0] - other_vehicle_state[0] , current_vehicle_state[1] - other_vehicle_state[1] ) < minimum_distance:

					#print "TOO CLOSE"
					# safe_to_add = False

					break

			if not safe_to_add:
				print "Vehicle is too close, will not place it here."
				continue


			"""
			current_body = bodyclasses.SmartVehicle()

			current_vehicle_state = temp_vehicle.get_current_state()

			current_body.id = vehicle_id

			current_body.x = current_vehicle_state[0]
			current_body.y = current_vehicle_state[1]
			current_body.z = 0.0

			current_body.yaw = current_vehicle_state[2]
			current_body.pitch = 0.0
			current_body.roll = 0.0
			current_body.commands = dict()
			current_body.commands['throttle'] = 0.0
			current_body.commands['steering'] = 0.0
			"""

			self.bodies_array[vehicle_id] = temp_vehicle 

			self.vehicles.append(temp_vehicle)

			# temp_vehicle.start()

			return

	def set_trajectory(self, vehicle, osm_info, node_start = -1):

		start_id = -1

		right_lane_node_id = -1543
		center_lane_node_id = -1424
		left_lane_node_id = -1297

		
		# node_start_idx = osm_info.osm_node_list.id.index(node_start)
		# node_id_list = [ osm_info.osm_node_list[idx].id for idx in xrange( len( osm_info.osm_node_list ) ) ]

		# right_lane_node_start_idx = node_id_list.index(right_lane_node_id)
		# center_lane_node_start_idx = node_id_list.index(center_lane_node_id)
		# left_lane_node_start_idx = node_id_list.index(left_lane_node_id)
		right_lane_node_id = laneletlibrary.find_node_by_tag(osm_node_dict, 'right_lane_node')
		center_lane_node_id = laneletlibrary.find_node_by_tag(osm_node_dict, 'center_lane_node')
		left_lane_node_id = laneletlibrary.find_node_by_tag(osm_node_dict, 'left_lane_node')

		points_per_meter = 5.

		# [right_lane_traj_x, right_lane_traj_y] = laneletlibrary.get_closed_trajectory_from_node_id(right_lane_node_id, osm_info.osm_lanelet_list, osm_info.osm_way_list, osm_info.osm_node_list, points_per_meter)
		# [center_lane_traj_x, center_lane_traj_y] = laneletlibrary.get_closed_trajectory_from_node_id(center_lane_node_id, osm_info.osm_lanelet_list, osm_info.osm_way_list, osm_info.osm_node_list, points_per_meter)
		# [left_lane_traj_x, left_lane_traj_y] = laneletlibrary.get_closed_trajectory_from_node_id(left_lane_node_id, osm_info.osm_lanelet_list, osm_info.osm_way_list, osm_info.osm_node_list, points_per_meter)
		[right_lane_traj_x, right_lane_traj_y] = laneletlibrary.get_closed_trajectory_from_node_id(right_lane_node_id, osm_info, points_per_meter)
		[center_lane_traj_x, center_lane_traj_y] = laneletlibrary.get_closed_trajectory_from_node_id(center_lane_node_id, osm_info, points_per_meter)
		[left_lane_traj_x, left_lane_traj_y] = laneletlibrary.get_closed_trajectory_from_node_id(left_lane_node_id, osm_info, points_per_meter)

		if len(right_lane_traj_x) == 0 or len(center_lane_traj_x) == 0 or len(left_lane_traj_x) == 0 :
			#Error, could not find a suitable trajectory
			# Can happen if a vehicle is in a node for trucks only
			print "PlatooningManager Error: could not find a suitable trajectory"
			return False

		# if math.hypot( traj_x[0] - traj_x[-1] , traj_y[0] - traj_y[-1] ) > 10.0:
		# 	return False

		desired_velocity = 10

		# vehicle.set_state_trajectory(traj_x, traj_y, desired_velocity)
		vehicle.set_right_lane_trajectory( right_lane_traj_x, right_lane_traj_y, desired_velocity )
		vehicle.set_center_lane_trajectory( center_lane_traj_x, center_lane_traj_y, desired_velocity )
		vehicle.set_left_lane_trajectory( left_lane_traj_x, left_lane_traj_y, desired_velocity )

		vehicle.set_vehicle_on_trajectory_state()

		return True

	def start_controlling_vehicles(self):
		# This function is an alternative to the start_simulating_vehicles()
		# function. This function will create independent vehicle threads which
		# will simply run the control loop and compute the actuator signals

		for vehicle in self.vehicles:

			self.vehicles_dict[vehicle.id] = vehicle

			print "Starting PLATOON vehicle number " + str(vehicle.id)

			self.start_thread( self.platooning_vehicle_step,  args=([vehicle]) )

	def platooning_vehicle_step(self, vehicle):
		# This a function that will run continuously on a thread for each platooning vehicle
		# Basically this thread will serve to update the body readings (and other inputs to the vehicle)
		# And to retrieve the current outputs (vehicle_commands, message_to_broadcast, etc... )

		this_thread_rate = 100

		# vehicle.set_vehicle_on_trajectory_state(0)

		vehicle_id = vehicle.id

		while not self.CLOSE:

			# print "1. / self.thread_rate = " + str(1. / self.thread_rate)

			tic_thread_step =  time.time()
			current_time = tic_thread_step - self.construction_time

			current_vehicle_state = []
			# I will get my current state from the current_states
			for body in self.bodies_list:

				if body['id'] == vehicle_id:

					vehicle.state = [ body['x']*32., body['y']*32., math.radians(body['yaw']) ]


			no_readings_flag = True

			for body_reading in self.bodies_readings:

				if body_reading['id'] == vehicle_id:

					if vehicle_id != -300:


						vehicle.set_body_readings(body_reading)
						[vehicle_velocity, vehicle_steering] = vehicle.get_low_level_inputs()
						# [vehicle_velocity, vehicle_steering] = vehicle.get_low_level_inputs( current_time , body_reading)

						# print "PlatooningManager: [vehicle_velocity, vehicle_steering] = " + str([vehicle_velocity, vehicle_steering])

						self.inputs[vehicle_id] = [vehicle_velocity, vehicle_steering]


					no_readings_flag = False				
					break
				
			if no_readings_flag:

				fake_body_reading = { 'id' : vehicle_id , 'readings' : [] }
				# current_vehicle_states = self.whatevs(vehicle, current_vehicle_states, fake_body_reading, full_stop)
				if vehicle_id != -300:

					vehicle.set_body_readings(fake_body_reading)
					[vehicle_velocity, vehicle_steering] = vehicle.get_low_level_inputs()
					# [vehicle_velocity, vehicle_steering] = vehicle.get_low_level_inputs( current_time , fake_body_reading)
					
					self.inputs[vehicle_id] = [vehicle_velocity, vehicle_steering]

					self.vehicles_dict[vehicle_id] = vehicle
			
			toc_thread_step = time.time()

			if toc_thread_step - tic_thread_step > 0:
				current_rate = (1. / (toc_thread_step - tic_thread_step))
				if current_rate > this_thread_rate:
					sleeping_time = 1. / this_thread_rate - 1. / current_rate
					time.sleep(sleeping_time)
				else:
					print "vehicle_id = " + str(vehicle_id) + " will not sleep"
					pass
			else:
				sleeping_time = 1. / this_thread_rate
				time.sleep(sleeping_time)


		print 'Stopped platooning vehicle ' + str(vehicle_id)


	def command_step(self, bodies_readings, bodies_list, simulation_period, simulator_is_closing = False):
		# Command Step is the function that interfaces the Platooning Manager with the Input Commands Manager.
		# This function is called at a very high rate. Everytime it is called the Platooning Manager updates
		# its own internal memory of bodies_readings and bodies_list to be in accordance with the most up-to-date
		# body_readings and bodies_list from Input Commands Manager. It also outputs the most recent Platooning Vehicles
		# commands!

		self.simulation_step_counter = self.simulation_step_counter + 1

		self.CLOSE = simulator_is_closing

		self.bodies_readings = bodies_readings
		self.bodies_list = bodies_list

		self.thread_rate = simulation_period
		self.thread_rate = 10

		current_vehicle_inputs = dict()

		for input_tag in self.inputs:

			current_input = dict()

			current_input['throttle'] = self.inputs[input_tag][0]
			current_input['steering'] = self.inputs[input_tag][1]
			
			current_vehicle_inputs[input_tag] = current_input
			
		return current_vehicle_inputs

	def start_thread(self, handler, args=()):

		t = threading.Thread(target=handler, args=args)
		t.daemon = True
		# self.threads.append(t)
		t.start()
