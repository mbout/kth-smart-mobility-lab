import socket, sys, time, math, random, threading

sys.path.append('Lanelets')
import laneletmodule
import laneletlibrary


import intersectionvehiclesupervisorymodule
import intersectionvehiclecontrolmodule
import intersectionvehicleperceptionmodule
import MessageHandler
import bodyclasses


# TODO
# add more to handle message function

class IntersectionVehicle(bodyclasses.WheeledVehicle):
	"This class is the Intersection Vehicle class used by the Platooning Manager Module"
	"This class is supposed to encompass three other classes which are running in threads"
	"The three other classes are the SupervisoryModule, the PerceptionModule and the "
	"ControlModule"
	"This class manages all the three Modules at the same time, making sure they are able to"
	"communicate with each other"
	"This class also interfaces the vehicle with the SML World, receiving body readings and V2V, "
	"and outputting velocity and steering commands and V2V"



	length = 5.0
	width = 1.7

	def __init__(self, simulation_period, vehicle_id, osm_info, bodies_array, osm_trajectory_id = 0, trajectory_start_point_index = 0):

		if not isinstance(vehicle_id, int):
			raise NameError('In class SmartVehicle: constructor: id must be an integer')

		bodyclasses.WheeledVehicle.__init__(self)

		self.bodies_array = bodies_array

		# Smart Vehicle can use the wifi/network, to achieve communications and agreement with other vehicles

		self.wifi_range = 0.

		self.v2v_wifi_input = []
		self.v2v_wifi_output = []
		self.v2v_network_input = []
		self.v2v_network_output = []

		self.commands = dict()
		self.commands['throttle'] = 0.0
		self.commands['steering'] = 0.0

		self.id = vehicle_id
		self.simulation_period = 1./simulation_period

		# Stores the input commands in the format:
		# [current_velocity_command, current_steering_command]
		self.input = [0, 0]
		self.velocity = 30.0 + 10.0*random.random()

		# Variables to store the lane trajectories
		
		des_vel = 10.0
		
		# Initialization of the lane trajectorys
		self.traj = []
		
		self.getTrajectory(osm_info, osm_trajectory_id)

		self.current_body_readings = None

		
		
		# current variables #####################################

		self.perception_grid = None
		self.perceived_objects = None
		
		# self.traj = self.getLaneTrajectory(self.desired_lane)

		#########################################################

		# Message flags + message generator
		self.messenger = MessageHandler.MessageHandler(self)

		self.saved_wifi_messages = {}
		self.saved_network_messages = {}
		self.desired_merge_flag = None # will be a vehicle id
		self.merge_accept_flag = None # will be a vehicle id
		self.desired_merge_rel_coords = None

		self.merge_request_flag = False # will be a vehicle id
		self.merging_flag = False # will be a vehicle id
		self.merge_distance = 8

		self.fwd_pair_partner = None
		self.bwd_pair_partner = None

		self.STOM_flag = False

		# parameters for merging distance
		self.maximum_merge_error = 2.
		self.maximum_merged_error = 2.


		self.messenger = MessageHandler.MessageHandler(self)
		# Creating the Supervisory Module, setting its rate and starting its thread
		self.supervisory_module = intersectionvehiclesupervisorymodule.IntersectionVehicleSupervisoryModule( self.velocity, self )
		self.supervisory_module_rate = 30.
		self.start_thread( self.supervisory_module_thread, args=([]) )

		# Creating the Control Module, setting its rate and starting its thread
		self.control_module = intersectionvehiclecontrolmodule.IntersectionVehicleControlModule( self, self.bodies_array, self.id )
		self.control_module_rate = 20.
		time.sleep(0.02) # This sleep is just to try to make the prints more separate
		self.set_vehicle_on_trajectory_state(trajectory_start_point_index)
		self.start_thread( self.control_module_thread, args=([]) )

		
		"""
		# WARNING: function must come between initialization of control module and perception module
		success = self.getLaneTrajectorys(osm_info)

		# success = self.set_trajectory(temp_vehicle, self.osm_info, node_start = -1)

		if not success:
			# Could not find a suitable trajectory. Try a new vehicle
			print "Could not find a suitable trajectory. Try a new vehicle"
		"""


		# Creating the Perception Module, setting its rate and starting its thread:
	
		self.perception_module = intersectionvehicleperceptionmodule.IntersectionVehiclePerceptionModule(self)
		self.perception_module_rate = 10.
		time.sleep(0.02) # This sleep is just to try to make the prints more separate
		self.start_thread( self.perception_module_thread, args=([]) )

		
		self.assignPerceptionModule()


		# Tips
		# self.state = [real_world_meters, radians]
		# print "self.state = " + str(self.state)
		# closeby_body_readings = [real_world_meters, radians]
		# print "closeby_body_readings = " + str(closeby_body_readings)
		# self.traj[0] in real_world_meters
		# self.traj[2] in radians
		# print "self.traj[2] = " + str(self.traj[2])

	def assignPerceptionModule(self):
		self.supervisory_module.perception_module = self.perception_module
		self.control_module.perception_module = self.perception_module

	def getLaneTrajectory(self, lane_traj_string):
		if self.lane_traj_dict:
			return self.lane_traj_dict[lane_traj_string]
		else:
			print "ERROR: no lane traj dict created"
			return None

	def getCurrentLaneTrajectory(self):
		if self.lane_traj_dict:
			return self.lane_traj_dict[self.desired_lane]

	def getCurrentLaneTrajectoryString(self):
		if self.desired_lane:
			return self.desired_lane
		else:
			return None

	def getTrajectory(self, osm_info, osm_trajectory_id):
		"""
		sets the trajectory of the vehicle

		:osm_info: some kind of info
		:returns: boolean True

		"""
		start_id = -1
		node_start = -1

		

		node_id_list = [ osm_info.osm_node_list[idx].id for idx in xrange( len( osm_info.osm_node_list ) ) ]

		if osm_trajectory_id == 0:

			#Choose a random osm node for the trajectory
			random_node_id = int( float(len(node_id_list) )*random.random() )
			traj_node_id = node_id_list[random_node_id]

		else:

			traj_node_id = osm_trajectory_id
			

		points_per_meter = 5.

		[traj_x, traj_y] = laneletlibrary.get_closed_trajectory_from_node_id(traj_node_id, osm_info.osm_lanelet_list, osm_info.osm_way_list, osm_info.osm_node_list, points_per_meter)
		
		if len(traj_x) == 0:
			#Error, could not find a suitable trajectory
			# Can happen if a vehicle is in a node for trucks only
			# print "Intersection Vehicle Error: could not find a suitable trajectory"
			raise NameError("Intersection Vehicle Error: could not find a suitable trajectory")
			return False

		desired_velocity = 10

		# vehicle.set_state_trajectory(traj_x, traj_y, desired_velocity)
		self.set_trajectory( traj_x, traj_y, desired_velocity )

		return True



	def set_vehicle_on_trajectory_state(self, idx = 0):
		# This function is used to place the vehicle in the initial position
		# of the trajectory

		# TODO make so that this does not need to be 0
		# idx = int(-self.id*300)
		# idx = 0
		self.control_module.current_trajectory_id = idx

		if len( self.traj ) == 0:

			raise NameError('In class SmartVehicle: trying to use set_vehicle_on_trajectory_state when trajectory is not yet defined')

		print "len(self.traj[0]) = " + str(len(self.traj[0]))

		self.x = self.traj[0][idx]
		self.y = self.traj[1][idx]
		self.yaw = self.traj[2][idx]

		self.z = 0.0
		self.roll = 0.0


	def set_initial_input(self, input_vector):	
		# To set up the initial input commands of the vehicle

		# In case self.input is empty
		if not self.input:

			for dimension in range( len( input_vector ) ):

				self.input.append( input_vector[dimension] )

		else:

			if len( self.input ) == len( input_vector ):

				for dimension in range( len( input_vector ) ):

					self.input[dimension] = input_vector[dimension]

			else:

				print "Error in SmartVehicle set_initial_input(): trying to set an input with wrong dimensions!"
	
	def set_trajectory(self, traj_x, traj_y, desired_velocity):
		# Function used to set a lane trajectory
		
		traj_theta = []
		temp_theta = 0
		traj_time = []
		current_time = 0

		for idx in range( len ( traj_x ) - 1 ):

			delta_x = traj_x[idx+1] - traj_x[idx]
			delta_y = traj_y[idx+1] - traj_y[idx]

			temp_theta = math.atan2(delta_y, delta_x)
			traj_theta.append(temp_theta)

			traj_time.append(current_time)

			distance_moved = math.hypot(delta_x, delta_y) # Euclidean norm
			time_passed = distance_moved/desired_velocity

			current_time = current_time + time_passed

		traj_theta.append(temp_theta)
		traj_time.append(current_time)

		if len(traj_y) != len(traj_x) or len(traj_time) != len(traj_x) or len(traj_theta) != len(traj_x):

			raise NameError("SmartVehicle Trajectory creation resulted in a mistake!")


		self.traj = [traj_x, traj_y, traj_theta, traj_time]

		return

	def get_current_state(self, radians = True):
		#Function to simply return the current state
		# returns [x, y, yaw]

		state_vector = []

		state_vector.append( self.x )
		state_vector.append( self.y )

		if radians:

			state_vector.append( self.yaw )

		else:

			state_vector.append( math.degrees(self.yaw) )

		return state_vector

	def get_current_input(self):
		#Function to simply return the current input commands
		input_vector = []

		for dimension in range( len( self.input ) ):

			input_vector.append( self.input[dimension] )

		return input_vector

	def supervisory_module_thread(self):

		# This is the function that will be running continuously on a thread
		# This function is supposed to manage/interface with the Supervisory Layer

		while True:

			start_time = time.time()

			# Run a step of the Supervisory Module
			self.supervisory_module.step()

			# Update my velocity to be the one decided by the supervisory mode
			self.velocity = self.supervisory_module.desired_velocity	

			# Make the thread sleep and run with adequate cycle time
			self.make_thread_sleep(start_time, self.supervisory_module_rate,  "In SmartVehicle: supervisory_module_step thread is taking too long!")


	def control_module_thread(self):

		# This is the function that will be running continuously on a thread
		# This function is supposed to manage/interface with the Control Layer

		# Must wait until initialization of vehicle is complete
		while True:

			if self.traj:

				break

			else:

				print "Control Module Thread Waiting for Vehicle To be Initialized"
				time.sleep(0.5)


		while True:

			start_time = time.time()

			# self.control_module.state = self.state

			# Tell the control module the velocity it should have
			self.control_module.desired_velocity = self.velocity

			self.control_module.step()

			# self.input = [self.control_module.current_velocity_command, self.current_steering_command]

			# print "Smart Vehicle: self.input = " + str(self.input)

			# Make the thread sleep and run with adequate cycle time
			self.make_thread_sleep(start_time, self.control_module_rate,  "In SmartVehicle: control_module_step thread is taking too long!")

	def perception_module_thread(self):

		# This is the function that will be running continuously on a thread
		# This function is supposed to manage/interface with the Perception Layer

		while True:

			start_time = time.time()

			self.perception_module.step()

			# Make the thread sleep and run with adequate cycle time
			self.make_thread_sleep(start_time, self.perception_module_rate,  "In SmartVehicle: perception_module_step thread is taking too long!")


	def make_thread_sleep(self, start_time, desired_rate,  warning_string):

		# A simple function that will make the thread sleep (or the cycle wait)
		# in order to respect the desired rate.
		# It will also issue a warning in case the desired rate cannot be performed
		# (due to taking too much time)
		end_time = time.time()
		elapsed_time = end_time - start_time
		desired_cycle_time = 1./desired_rate
		sleep_time = desired_cycle_time - elapsed_time
		if sleep_time <= 0:
			print warning_string
		else:
			time.sleep(sleep_time)

	def start_thread(self, handler, args=()):
		# Function that simplifies thread creation
		t = threading.Thread(target=handler, args=args)
		t.daemon = True
		# self.threads.append(t)
		t.start()


	def set_body_readings(self, body_readings):
		# This functions is called at a very high frequency from the platooning manager module
		# It simply updates the body_readings to the most current ones
		self.current_body_readings = body_readings

		self.control_module.current_body_readings = body_readings



	def get_low_level_inputs(self):
		# This functions is called at a very high frequency from the platooning manager module
		# It simply returns the current low level inputs
		# to the Inputs Commands Manager
	
		# return [self.current_velocity_command, self.current_steering_command]
		# print "self.input = " + str(self.input)
		return self.input

	def getPerceivedObject(self, identity):
		if self.perceived_objects[identity]:
			return self.perceived_objects[identity]
		else:
			return None

	def receiveMessage(self, messageDict):
		self.messenger.receiveMessage(messageDict)

