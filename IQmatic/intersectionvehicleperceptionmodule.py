import math
import smartvehicle



class IntersectionVehiclePerceptionModule:
	"This class is the Perception Module, that is running in a child thread of "
	"the Smart Vehicle module. The Perception Module should be in charge of getting"
	"the current sensor readings and V2V messages, process them and generate useful"
	"information to be sent to the Supervisory Module and/or Control Module"

	# just guesses right now

	def __init__(self, vehicle):
		
		self.module_ready = False
		self.vehicle = vehicle
		self.current_index = None
		

		self.fl = []
		self.fm = []
		self.fr = []
		self.l = []
		self.r = []
		self.rl = []
		self.rm = []
		self.rr = []

		self.mean_dists = {}
		self.tot_dists = {}

		# self.generateMeanDistances()


		self.new_perceived_objects = {}
		self.closest_objects = {}

		# Calculating the mean distance between points in lane_traj
		#Tips
		# self.state = [real_world_meters, radians]
		# print "self.state = " + str(self.state)
		# closeby_body_readings = [real_world_meters, radians] TODO what is this real world meters?
		# print "closeby_body_readings = " + str(closeby_body_readings)
		# self.traj[0] in real_world_meters
		# self.traj[2] in radians
		# print "self.traj[2] = " + str(self.traj[2])

		print "VehiclePerceptionModule started"
	def generateMeanDistances(self):

		traj_list = ['left_lane', 'center_lane', 'right_lane']
		for traj_string in traj_list:
			tot_distance = 0.
			traj = self.vehicle.getLaneTrajectory(traj_string)
			
			# print "first "+ str((traj[0][0], traj[1][0]))
			for i in range(len(traj[0]) - 1):
				dist = math.hypot(traj[0][i+1] - traj[0][i], traj[1][i+1] - traj[1][i])
				tot_distance += dist

			# print "last: " + str((traj[0][len(traj[0]) - 1], traj[1][len(traj[0]) - 1]))

			self.tot_dists[traj_string] = tot_distance
			self.mean_dists[traj_string] = tot_distance/(len(traj[0]) - 1)


	def step(self):
	
		# print self.vehicle.state
		# This is the step function that is running at each cycle of the thread
		# in SmartVehicle

		# print "Perception Module Step"


		self.fl = []
		self.fm = []
		self.fr = []
		self.l = []
		self.r = []
		self.rl = []
		self.rm = []
		self.rr = []

		try:
			body_data = self.vehicle.bodies_array[self.vehicle.id]
		except:
			body_data = None
			pass

		if bool(body_data):
			# check for initialization

			self.x = body_data.x
			self.y = body_data.y
			coord_vector = [self.x, self.y]
			self.theta = math.radians(body_data.yaw)
			# self.current_lane_string = self.vehicle.getCurrentLaneTrajectoryString()

			# get the road relative coordinates
			# self.updateRoadCoord(coord_vector, self.current_lane_string)

		if bool(body_data):
			# checking for initialization
			

			for reading in body_data.sensor_readings:
				#print reading
				# classifying every reading
				other_coords = [reading[1], reading[2]]
				rw_coords = [other_coords[0], other_coords[1]]
				
				other_body_id = reading[0]

				# converts into road relative coordinates
				# road_coords = self.convertToRoadCoord(rw_coords, self.current_lane_string)
				
				# relative to vehicle, takes into account the periodiciy of the oval
				# relative_road_coords = self.getRelativeRoadCoordsOval(road_coords, self.current_lane_string)	

				#print relative_road_coords

				# self.classify(rw_coords);
				# self.classifyRoadCoords(other_body_id, relative_road_coords)
			
			self.setPerceivedObjects()
			self.setPerceptionGrid()
			self.findClosestObjects()

			self.module_ready = True



#			print self.current_index

		# print "(Front left, Front middle, Front right): (" + str(bool(self.fl)) + ", " + str(bool(self.fm)) + ", " + str(bool(self.fr)) + ")"



	def getRelativeRoadCoordsOval(self, road_coords, trajectory_string):
		#print road_coords
		if road_coords[0] > self.road_x:
			x_alt1 = road_coords[0] - self.road_x
			x_alt2 = road_coords[0] - self.tot_dists[trajectory_string]- self.road_x
			relative_road_x = x_alt1 if abs(x_alt1) < abs(x_alt2) else x_alt2
		else:
			x_alt1 = self.road_x - road_coords[0]
			x_alt2 = self.road_x - self.tot_dists[trajectory_string]- road_coords[0]
			relative_road_x = -x_alt1 if abs(x_alt1) < abs(x_alt2) else -x_alt2

		return [relative_road_x, road_coords[1] - self.road_y]

	def classifyRoadCoords(self, other_body_id, relative_coords):

		self.addPerceivedBody(other_body_id, relative_coords)

		if relative_coords[0] > smartvehicle.SmartVehicle.length:
			# If object is in front of vehicle

			#print "front of car"

			if relative_coords[1] > smartvehicle.SmartVehicle.width:
				# If it is to left
				
				self.fl.append(other_body_id)
				
			elif relative_coords[1] < -smartvehicle.SmartVehicle.width:
				# to the right

				self.fr.append(other_body_id)
				
			else:
				# middle
				self.fm.append(other_body_id)

		elif relative_coords[0] < -smartvehicle.SmartVehicle.length:
			# to the rear

			if relative_coords[1] > smartvehicle.SmartVehicle.width:
				# left
				
				self.rl.append(other_body_id)
				
			elif relative_coords[1] < -smartvehicle.SmartVehicle.width:
				#right

				self.rr.append(other_body_id)
				
			else:
				# middle
				self.rm.append(other_body_id)

		else:
			# to the sides
			if relative_coords[1] > smartvehicle.SmartVehicle.width:
				# left

				self.l.append(other_body_id)

			elif relative_coords[1] < -smartvehicle.SmartVehicle.width:
				#right

				self.r.append(other_body_id)

	def addPerceivedBody(self, other_body_id, relative_road_coords):

		self.new_perceived_objects[other_body_id] = relative_road_coords

	def setPerceivedObjects(self):
		self.vehicle.perceived_objects = self.new_perceived_objects
		self.new_perceived_objects = {}

	def getPerceivedObject(self, identity):

		if identity in self.vehicle.perceived_objects:
			return self.vehicle.perceived_objects[identity]

		else:
			return None

	def findClosestObjects(self):

		new_closest_objects = {}
		zero_vector = [0, 0]

		directions = ['fm', 'l', 'r', 'rm', 'fl', 'fr', 'rl', 'rr']
		for direction in directions:
			if self.vehicle.perception_grid[direction]:
				closest_id = self.vehicle.perception_grid[direction][0]
				closest_distance = VehiclePerceptionModule.distance(zero_vector, self.getPerceivedObject(closest_id))
				index = 1
				while index < len(self.vehicle.perception_grid[direction]):
					new_body_id = self.vehicle.perception_grid[direction][index]
					new_distance = VehiclePerceptionModule.distance(zero_vector, self.getPerceivedObject(new_body_id))

					if new_distance < closest_distance:
						closest_id = new_body_id
						closest_distance = new_distance

					index += 1

				new_closest_objects[direction] = closest_id

		self.vehicle.closest_objects = new_closest_objects

	def getClosestId(self, direction):

		if self.vehicle.closest_objects[direction]:
			return self.vehicle.closest_objects[direction]
		else:
			return None


	def setPerceptionGrid(self):
		"""Sets the perception grid of the vehicle object
		:returns: TODO

		"""

		perception_grid = dict()
		
		perception_grid['fl'] = self.fl
		perception_grid['fm'] = self.fm
		perception_grid['fr'] = self.fr
		perception_grid['l'] = self.l
		perception_grid['r'] = self.r
		perception_grid['rl'] = self.rl
		perception_grid['rm'] = self.rm
		perception_grid['rr'] = self.rr


		self.vehicle.perception_grid = perception_grid

	def convertToRoadCoord(self, coords, lane_traj_string):
		"""
		Takes absolute coordinates and transforms them into (approximate) road coords
		road coords is if you think of the road as a straight line
		x indicates the longitudinal coordinates
		y indicates the transversal (with y > 0 is left of the center lane)

		"""
		lane_traj = self.vehicle.getLaneTrajectory(lane_traj_string)
		closest_index = 0
		closest_distance = math.hypot(coords[0] - lane_traj[0][0], coords[1] - lane_traj[0][1])
		for index in range(len(lane_traj[0])):

			distance = math.hypot(coords[0] - lane_traj[0][index], coords[1] - lane_traj[1][index])

			if distance < closest_distance:
				closest_distance = distance
				closest_index = index

		x = closest_index*self.mean_dists[lane_traj_string]
		y = self.getRoadY(closest_index, lane_traj_string, coords)
		return (x, y, closest_index)


	def updateRoadCoord(self, coordinates, lane_traj_string):
		""" an attempt to make convertToRoadCoord more effective when computing for own vehicle

		"""
		lane_traj = self.vehicle.getLaneTrajectory(lane_traj_string)

		if self.current_index == None or lane_traj_string == self.old_lane_string:
			self.current_index = self.convertToRoadCoord(coordinates, lane_traj_string)[2]

		else:
			counter = 0
			v1 = [lane_traj[0][self.current_index], lane_traj[1][self.current_index]]
			v2 = coordinates
			old_d = VehiclePerceptionModule.distance( v1, v2 )
			counter += 1
			closest_counter = 0

			while counter < 700:
				new_index = (self.current_index + counter)%len(lane_traj[0])
				v1 = [lane_traj[0][(self.current_index+counter)%len(lane_traj[0])], lane_traj[1][(self.current_index + counter)%len(lane_traj[0])]]
				new_d = VehiclePerceptionModule.distance(v1, v2)

				if new_d < old_d:
					old_d = new_d
					closest_counter = counter
				else:
					break

				counter += 1

			self.current_index = (self.current_index + closest_counter)%len(lane_traj[0])
		self.road_x = self.current_index*self.mean_dists[lane_traj_string]
		self.road_y = self.getRoadY(self.current_index, lane_traj_string, coordinates)

		self.old_lane_string = lane_traj_string


	def getRoadY(self, index, lane_traj_string, coordinates):
		""" get the road relative y coordinates based on the selected lane, a positive value means left of lane trajectory """

		lane_traj = self.vehicle.getLaneTrajectory(lane_traj_string)

		if not index == len(lane_traj[0]) - 1:
			tangent = [lane_traj[0][index+1] - lane_traj[0][index], lane_traj[1][index + 1] - lane_traj[1][index]]
		else:
			tangent = [lane_traj[0][index] - lane_traj[0][index-1], lane_traj[1][index] - lane_traj[1][index-1]]

		tangent = [tangent[0]/math.hypot(tangent[0], tangent[1]), tangent[1]/math.hypot(tangent[0], tangent[1])]
		normal = [-tangent[1], tangent[0]]

		relCoords = [coordinates[0] - lane_traj[0][index], coordinates[1] - lane_traj[1][index]]

		road_y = relCoords[0]*normal[0] + relCoords[1]*normal[1]

		return road_y
		

	@staticmethod
	def distance(v1, v2):
		""" calculates the distance between two different objects

		:v1: vector 1 [x, y]
		:v2: vector 2 [x, y]
		:returns: euclidean distance

		"""
		return math.hypot(v1[0] - v2[0], v1[1] - v2[1])


    
