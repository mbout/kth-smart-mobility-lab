import laneletlibrary
import pygame
import os
import xml.etree.ElementTree as ET
import time
import random


def draw_lanelet(lanelet, osm_node_dict, world_surface):

	node_tuple_list = []

	for node_id in lanelet.left_osm_way.node_ids:

		current_node = laneletlibrary.get_osm_node_by_id(osm_node_dict, node_id)

		current_tuple = ( int( current_node.pixel_x ) , int( current_node.pixel_y ) )
		node_tuple_list.append( current_tuple )

	for idx in range( len ( lanelet.right_osm_way.node_ids ) ):

		current_node = laneletlibrary.get_osm_node_by_id(osm_node_dict, lanelet.right_osm_way.node_ids[-1 - idx])

		current_tuple = ( int( current_node.pixel_x ) , int( current_node.pixel_y ) )
		node_tuple_list.append( current_tuple )

	COLOR = (77, 76, 69) # Asphalt black

	pygame.draw.polygon(world_surface, COLOR, tuple( node_tuple_list ) )

def draw_lanelet_pattern(lanelet, osm_node_dict, world_surface):

	node_tuple_list = []

	for node_id in lanelet.left_osm_way.node_ids:

		current_node = laneletlibrary.get_osm_node_by_id(osm_node_dict, node_id)

		current_tuple = ( int( current_node.pixel_x ) , int( current_node.pixel_y ) )
		node_tuple_list.append( current_tuple )

	for idx in range( len ( lanelet.right_osm_way.node_ids ) ):

		current_node = laneletlibrary.get_osm_node_by_id(osm_node_dict, lanelet.right_osm_way.node_ids[-1 - idx])

		current_tuple = ( int( current_node.pixel_x ) , int( current_node.pixel_y ) )
		node_tuple_list.append( current_tuple )

	COLOR = (77, 76, 69) # Asphalt black

	pygame.draw.polygon(world_surface, COLOR, tuple( node_tuple_list ) )


def repeat_pattern(surface, pattern, mask, darkness = 1.0):

	# It will change surface by reference

	width, height =  surface.get_size()

	pattern_width, pattern_height = pattern.get_size()

	# print 'width ' + str(width)
	# print 'height ' + str(height)

	if darkness != 1.0:

		for i in range(width):

			for j in range(height):

				color = mask.get_at( (i,j) )

				if color[0] == 0:

					# surface.set_at( (i,j) , (255, 0, 0) )
					color = pattern.get_at( ( i%pattern_width , j%pattern_height ) )

					new_color = ( darkness*color[0], darkness*color[1], darkness*color[2] )

					surface.set_at( (i,j) , new_color )
					# surface.set_at( (i,j) , pattern.get_at( ( i%pattern_width , j%pattern_height ) ) )

	else:

		for i in range(width):

			for j in range(height):

				color = mask.get_at( (i,j) )

				if color[0] == 0:

					surface.set_at( (i,j) , pattern.get_at( ( i%pattern_width , j%pattern_height ) ) )


def get_lanelet_mask(osm_info, canvas_width, canvas_height, fill_color, mask_color, trucks_only = False):

	mask = pygame.Surface((canvas_width, canvas_height), 0, 32)

	mask.fill(fill_color)

	for lanelet in osm_info.osm_lanelet_list:

		# draw_lanelet(lanelet, osm_info.osm_node_dict, world_surface)
		if (trucks_only and not lanelet.truck_only) or (not trucks_only and lanelet.truck_only) :

			continue

		node_tuple_list = []

		for node_id in lanelet.left_osm_way.node_ids:

			current_node = laneletlibrary.get_osm_node_by_id(osm_info.osm_node_dict, node_id)

			current_tuple = ( int( current_node.pixel_x ) , int( current_node.pixel_y ) )
			node_tuple_list.append( current_tuple )

		for idx in range( len ( lanelet.right_osm_way.node_ids ) ):

			current_node = laneletlibrary.get_osm_node_by_id(osm_info.osm_node_dict, lanelet.right_osm_way.node_ids[-1 - idx])

			current_tuple = ( int( current_node.pixel_x ) , int( current_node.pixel_y ) )
			node_tuple_list.append( current_tuple )

		pygame.draw.polygon(mask, mask_color, tuple( node_tuple_list ) )

	return mask

def get_way_line_type_mask(osm_info, canvas_width, canvas_height, line_type, fill_color, mask_color):

	mask = pygame.Surface((canvas_width, canvas_height), 0, 32)

	mask.fill(fill_color)

	for way in osm_info.osm_way_list:

		# draw_lanelet(lanelet, osm_info.osm_node_dict, world_surface)

		if way.line_type != line_type:

			continue
		
		node_tuple_list = []

		for node_id in way.node_ids:

			current_node = laneletlibrary.get_osm_node_by_id(osm_info.osm_node_dict, node_id)

			current_tuple = ( int( current_node.pixel_x ) , int( current_node.pixel_y ) )
			node_tuple_list.append( current_tuple )

		pygame.draw.polygon(mask, mask_color, tuple( node_tuple_list ) )

	return mask

# def get_free_space_obstacle_mask(osm_info, canvas_width, canvas_height, fill_color, mask_color):

# 	mask = pygame.Surface((canvas_width, canvas_height), 0, 32)

# 	mask.fill(fill_color)

# 	for way in osm_info.osm_way_list:

# 		# draw_lanelet(lanelet, osm_info.osm_node_dict, world_surface)

# 		if way.line_type != 'free_space_obstacle':

# 			continue
		
# 		node_tuple_list = []

# 		for node_id in way.node_ids:

# 			current_node = laneletlibrary.get_osm_node_by_id(osm_info.osm_node_dict, node_id)

# 			current_tuple = ( int( current_node.pixel_x ) , int( current_node.pixel_y ) )
# 			node_tuple_list.append( current_tuple )

# 		pygame.draw.polygon(mask, mask_color, tuple( node_tuple_list ) )

# 	return mask





def draw_way(way, osm_node_dict, world_surface, darkness = 1.0, oversize = 1.0):

	if way.line_type == None:
		return

	node_tuple_list = []

	for node_id in way.node_ids:

		current_node = laneletlibrary.get_osm_node_by_id(osm_node_dict, node_id)

		current_tuple = ( int( current_node.pixel_x ) , int( current_node.pixel_y ) )
		node_tuple_list.append( current_tuple )

	COLOR = ()
	line_thickness = 0

	if way.line_type == 'exterior':
		COLOR = (191, 179, 8) # Dirty yellow
		line_thickness = 2*oversize

	elif way.line_type == 'interior':
		COLOR = (245, 243, 223) # Dirty white 
		line_thickness = 2*oversize

	else:

		return

	if darkness != 1.0:

		NEW_COLOR = (darkness*COLOR[0], darkness*COLOR[1], darkness*COLOR[2])

	else:

		NEW_COLOR = COLOR

	# print node_tuple_list
	line_closed = False
	

	pygame.draw.lines(world_surface, NEW_COLOR, line_closed, tuple( node_tuple_list ), line_thickness)


def draw_all_lanelets(osm_info, world_surface):

	# draw_lanelet(osm_info.osm_lanelet_list[10], osm_info.osm_node_dict, world_surface)

	for lanelet in osm_info.osm_lanelet_list:

		draw_lanelet(lanelet, osm_info.osm_node_dict, world_surface)


def draw_all_lanelets_pattern(osm_info, world_surface):

	# draw_lanelet(osm_info.osm_lanelet_list[10], osm_info.osm_node_dict, world_surface)

	for lanelet in osm_info.osm_lanelet_list:

		draw_lanelet_pattern(lanelet, osm_info.osm_node_dict, world_surface)



def draw_all_ways(osm_info, world_surface, darkness = 1.0, oversize = 1.0):

	for way in osm_info.osm_way_list:

		draw_way(way, osm_info.osm_node_dict, world_surface, darkness, oversize)


def save_lanelet_mask_for_internet(lanelet_mask, mud_mask, free_space_mask, canvas_width, canvas_height):

	root = ET.Element('message')
	root_trees = ET.Element('message')

	root.set("type", "lanelet_mask_for_internet")
	root_trees.set("type", "trees_for_internet")

	sub_sampling = 2

	root.set("pixel_width", str(canvas_width/sub_sampling) )	
	root.set("pixel_height", str(canvas_height/sub_sampling) )	

	root_trees.set("pixel_width", str(canvas_width/sub_sampling) )	
	root_trees.set("pixel_height", str(canvas_height/sub_sampling) )	
	


	# BLACK = (0, 0, 0)
	# size = canvas_width, canvas_height
	# screen = pygame.display.set_mode(size)
	# screen.fill(BLACK)
	# screen.blit(mud_mask, (0,0) )
	# pygame.display.flip()
	# print "DOOOOOOOOOOOOOOOOOOOOOONNNNNNNNNNNNNNNNNNNNNNNNNNNNEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE"
	# time.sleep(10.0)

	tree_chance = 0.002

	tree_count = 0
	trees_mask = []

	for i in range(canvas_width/sub_sampling):

		current_tree_dimension = []

		for j in range(canvas_height/sub_sampling):

			lanelet_value = lanelet_mask.get_at( (i*sub_sampling,j*sub_sampling) )
			mud_value = mud_mask.get_at( (i*sub_sampling,j*sub_sampling) )
			free_space_value = free_space_mask.get_at( (i*sub_sampling,j*sub_sampling) )

			pixel = ET.SubElement(root, "pixel" )
			pixel.set("x", str(i) )
			pixel.set("y", str(j) )

			#if lanelet_value[0] == 255 or mud_value[0] == 255:
			if lanelet_value[0] == 255 or mud_value[0] == 0 or free_space_value[0] == 0:

				current_tree_dimension.append(False)
				pixel.set("value", str(0) )

			else:

				if random.random() < tree_chance:

					tree_count = tree_count + 1
					new_tree = ET.SubElement(root_trees, "tree" )
					new_tree.set("x", str(i) )
					new_tree.set("y", str(j) )

				current_tree_dimension.append(True)
				pixel.set("value", str(1) )

		trees_mask.append(current_tree_dimension)

	xml_string = ET.tostring(root)

	f = open('lanelet_mask_for_internet.xml', 'w')

 	f.write(xml_string)

	f.close()

	print "Created " + str( tree_count ) + "trees"

	xml_string = ET.tostring(root_trees)

	f = open('trees_for_internet.xml', 'w')

 	f.write(xml_string)

	f.close()

def create_world_surface(osm_info, canvas_width, canvas_height, oversize = 1):

	world_surface = pygame.Surface((canvas_width, canvas_height), 0, 32)

	FOREST_GREEN = (80, 163, 77)
	DEBUG_PINK = (225, 80, 191)
	BLACK = 0, 0, 0
	WHITE = 255, 255, 255
	fill_color = WHITE
	mask_color = BLACK

	world_surface.fill(DEBUG_PINK)

	forest = pygame.image.load(os.path.join("resources","forest.jpg"))
	# forest = pygame.image.load("forestContinuous2.jpg")
	original_width, original_height = forest.get_size()
	scale_ratio = 1.0/1.5 * oversize
	new_size = (int(scale_ratio*original_width), int(scale_ratio*original_height) )
	forestRescaled = pygame.transform.scale(forest,  new_size )
	mask = get_lanelet_mask(osm_info, canvas_width, canvas_height, mask_color, fill_color)



	lanelet_mask = pygame.Surface((canvas_width, canvas_height), 0, 32)
	lanelet_mask.fill(BLACK)
	pygame.Surface.blit(lanelet_mask, mask, (0,0))

	#lanelet_mask = pygame.Surface.blit(mask)

	repeat_pattern(world_surface, forestRescaled, mask, darkness = 0.7)

	# ball = pygame.image.load("mud.jpg")
	# ball = pygame.image.load("mud2.png")
	ball = pygame.image.load(os.path.join("resources","mud3.jpg"))
	original_width, original_height = ball.get_size()
	scale_ratio = 1.0/10.0 * oversize
	new_size = (int(scale_ratio*original_width), int(scale_ratio*original_height) )
	ballRescaled = pygame.transform.scale(ball,  new_size )
	trucks_only = True

	mask = get_lanelet_mask(osm_info, canvas_width, canvas_height, fill_color, mask_color, trucks_only)
	repeat_pattern(world_surface, ballRescaled, mask, darkness = 1.)

	mud_mask = pygame.Surface((canvas_width, canvas_height), 0, 32)
	mud_mask.fill(BLACK)
	pygame.Surface.blit(mud_mask, mask, (0,0))

	ball = pygame.image.load(os.path.join("resources","mud3.jpg"))
	original_width, original_height = ball.get_size()
	scale_ratio = 15.0/10.0 * oversize
	new_size = (int(scale_ratio*original_width), int(scale_ratio*original_height) )
	ballRescaled = pygame.transform.scale(ball,  new_size )
	trucks_only = True

	mask = get_way_line_type_mask(osm_info, canvas_width, canvas_height, 'free_space',fill_color, mask_color)
	#mask = get_free_space_mask(osm_info, canvas_width, canvas_height, fill_color, mask_color, trucks_only)
	repeat_pattern(world_surface, ballRescaled, mask, darkness = 1.)

	##mud_mask = pygame.Surface.copy(mask)
	free_space_mask = pygame.Surface((canvas_width, canvas_height), 0, 32)
	# mud_mask.blit(mask)
	pygame.Surface.blit(free_space_mask, mask, (0,0))

	save_lanelet_mask_for_internet(lanelet_mask, mud_mask, free_space_mask, canvas_width, canvas_height)

	# water = pygame.image.load( os.path.join("resources","mud2.png") )
	# water = pygame.image.load( os.path.join("resources","gold.jpg") )
	water = pygame.image.load( os.path.join("resources","iron.jpg") )
	original_width, original_height = water.get_size()
	scale_ratio = 10.0/200.0 * oversize
	new_size = (int(scale_ratio*original_width), int(scale_ratio*original_height) )
	waterRescaled = pygame.transform.scale(water,  new_size )
	trucks_only = True

	
	mask = get_way_line_type_mask(osm_info, canvas_width, canvas_height, 'pile_stuff', fill_color, mask_color)
	# mask = get_free_space_obstacle_mask(osm_info, canvas_width, canvas_height, fill_color, mask_color)
	#mask = get_free_space_mask(osm_info, canvas_width, canvas_height, fill_color, mask_color, trucks_only)
	repeat_pattern(world_surface, waterRescaled, mask, darkness = .8)



	water = pygame.image.load( os.path.join("resources","parkingPattern.png") )
	original_width, original_height = water.get_size()
	scale_ratio = 70.0/200.0 * oversize
	new_size = (int(scale_ratio*original_width), int(scale_ratio*original_height) )
	waterRescaled = pygame.transform.scale(water,  new_size )
	trucks_only = True

	
	mask = get_way_line_type_mask(osm_info, canvas_width, canvas_height, 'parking_lot', fill_color, mask_color)
	# mask = get_free_space_obstacle_mask(osm_info, canvas_width, canvas_height, fill_color, mask_color)
	#mask = get_free_space_mask(osm_info, canvas_width, canvas_height, fill_color, mask_color, trucks_only)
	repeat_pattern(world_surface, waterRescaled, mask, darkness = .8)


	asphalt = pygame.image.load( os.path.join("resources","asphalt.jpg") )
	original_width, original_height = asphalt.get_size()
	scale_ratio = 1.0/2.0 * oversize
	new_size = (int(scale_ratio*original_width), int(scale_ratio*original_height) )
	asphaltRescaled = pygame.transform.scale(asphalt,  new_size )

	mask = get_lanelet_mask(osm_info, canvas_width, canvas_height, fill_color, mask_color)
	repeat_pattern(world_surface, asphaltRescaled, mask)

	draw_all_ways(osm_info, world_surface, darkness = 0.9, oversize = oversize)

	return world_surface

def create_world_surface_old(osm_info, canvas_width, canvas_height):

	world_surface = pygame.Surface((canvas_width, canvas_height), 0, 32)

	FOREST_GREEN = (80, 163, 77)
	world_surface.fill(FOREST_GREEN)

	# laneletvisualisation.draw_all_lanelets(osm_info, world_surface)
	draw_all_lanelets_pattern(osm_info, world_surface)
	draw_all_ways(osm_info, world_surface)
