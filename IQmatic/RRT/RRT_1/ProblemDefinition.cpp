/* 
 *     This file is a part of RRT Truck Trailer Planner 
 * 
 *     Copyright (C) <2015>  <Rui Oliveira> <rfoli@kth.se>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "ProblemDefinition.h"

ProblemDefinition::ProblemDefinition() {
        
    // Simply assign default values when the object is constructed.
    
    state_dimension = 4;
    
    // In case the user fails to specify alternative values, these will be used
    euler_simulation_time = 0.1;
    number_euler_steps = 10;

    truck_length = 0.115;
    truck_width = 0.08;
    trailer_length = 0.166;
    trailer_width = 0.08;

    environment_width = 2.;
    environment_min_x = 0.;
    environment_max_x = 0.;
    environment_min_y = 0.;
    environment_max_y = 0.;
    goal_tolerance = 0.2;    

    max_steering_angle = M_PI/6; // Corresponds to 30 degrees
        
}

ProblemDefinition::ProblemDefinition(const ProblemDefinition& orig) {
}

void ProblemDefinition::read_environment_and_obstacles_from_file(std::string file_name){
    
    std::cout << "Reading obstacles from file " << file_name.c_str() << std::endl;
    
    std::string current_line;
    
    std::ifstream myfile ( file_name.c_str() );
    
    if (myfile.is_open())
    {
        
        std::size_t found_obstacle_string;
        bool is_obstacle_line;
        bool was_obstacle = false;
        
        std::size_t found_limits_string;
        bool is_limits_line;
        bool was_limits = false;
        
        std::vector<double> current_points_x;
        std::vector<double> current_points_y;
        std::vector<double> * current_points_x_pointer = &current_points_x;
        std::vector<double> * current_points_y_pointer = &current_points_y;
        
        while ( getline (myfile,current_line) )
        {
            
            found_obstacle_string = current_line.find("Obstacle");
            is_obstacle_line = found_obstacle_string!=std::string::npos;
            
            found_limits_string = current_line.find("Limits");
            is_limits_line = found_limits_string!=std::string::npos;
            
            if ( is_obstacle_line ){
                
                store_points( current_points_x_pointer, current_points_y_pointer, was_obstacle, was_limits);
                
                was_obstacle = 1;
                was_limits = 0;
                                
            }else if ( is_limits_line ){
                
                store_points( current_points_x_pointer, current_points_y_pointer, was_obstacle, was_limits);
                
                was_obstacle = 0;
                was_limits = 1;
                
            }else{
                
                double point_coord[2];
                parse_point_line(current_line, point_coord);
                push_points_to_buffer(current_points_x_pointer, current_points_y_pointer, point_coord);
                
            }
                        
        }
        
        store_points( current_points_x_pointer, current_points_y_pointer, was_obstacle, was_limits );
                
        myfile.close();
        
    }else{
        
        std::cout << "Unable to open file: " << file_name.c_str() << '\n';
        
    }
        
}

void ProblemDefinition::update_space_limits(std::vector<double> * current_points_x, std::vector<double> * current_points_y){
    
    double current_x;
    double current_y;
    
    for ( unsigned i = 0; i < current_points_x->size() ; i++ ){

        current_x = current_points_x->at(i);
        current_y = current_points_y->at(i);
        
        if ( current_x < environment_min_x ){
            
            environment_min_x = current_x;
            
        }
        if ( current_x > environment_max_x ){
            
            environment_max_x = current_x;
            
        }
        if ( current_y < environment_min_y ){
            
            environment_min_y = current_y;
            
        }
        if ( current_y > environment_max_y ){
            
            environment_max_y = current_y;
            
        }
            
    }
    
}


void ProblemDefinition::store_points(std::vector<double> * current_points_x, std::vector<double> * current_points_y, bool was_obstacle, bool was_limits){
    
    if ( current_points_x->size() != 0 ){
    
        std::cout << "Creating polygon with points : \t";
        
        model::polygon<model::d2::point_xy<double> > poly;

        for (unsigned i = 0; i < current_points_x->size() ; i++ ){

            model::d2::point_xy<double> pointTesting;
            assign_values(pointTesting, current_points_x->at(i), current_points_y->at(i) );
            
            std::cout << "(" << current_points_x->at(i) << "," << current_points_y->at(i) << ") \t";
            
            append( poly, pointTesting );

        }

        // Polygons should be closed, and directed clockwise. Correct makes sure of it.
        // We need to perform it, as we are not sure if the user is giving us the
        // obstacles in a good format.
        correct(poly);
                    
        if ( was_obstacle ){

            std::cout << "Store points in obstacle: " << current_points_x->size() << " points.\n";
            polygon_obstacles_list.push_back(poly);

        }else if ( was_limits ){

            std::cout << "Store points in limits: " << current_points_x->size() << " points.\n";
            polygon_environment = poly;
            update_space_limits(current_points_x, current_points_y);

        }

        current_points_x->clear();
        current_points_y->clear();

    }
        
}

void ProblemDefinition::parse_point_line(std::string current_line, double point_coord[]){
    
    std::string delimiter = " ";
    size_t pos = 0;
    std::string token;

    pos = current_line.find(delimiter);
    // First number
    token = current_line.substr(0, pos);                
    double x = atof(token.c_str());
    current_line.erase(0, pos + delimiter.length());

    pos = current_line.find(delimiter);
    // Second number
    token = current_line.substr(0, pos);
    double y = atof(token.c_str());
    current_line.erase(0, pos + delimiter.length());

    point_coord[0] = x;
    point_coord[1] = y;
        
}

void ProblemDefinition::push_points_to_buffer(std::vector<double> * current_points_x, std::vector<double> * current_points_y, double point_coord[]){
    
    current_points_x->push_back( point_coord[0] );
    current_points_y->push_back( point_coord[1] );
        
}

ProblemDefinition::~ProblemDefinition() {
}

