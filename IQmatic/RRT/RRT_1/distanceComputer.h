/* 
 * 
 *     This file is a part of RRT Truck Trailer Planner 
 * 
 */

// The functions here declared are used for computation of distances between
// states.

#ifndef DISTANCECOMPUTER_H
#define	DISTANCECOMPUTER_H

#include "ProblemDefinition.h"

#include <cmath>

// Gets the metric distance between states, as a simple euclidean norm of the 
// whole state (4 dimensional case).
float get_metric_distance(float *state_a, float *state_b);

// Gets a xy distance between states, as a simple euclidean norm of the 
// x and y dimensions.
float get_xy_distance(float *state_a, float *state_b);

// Gets the distance of a state to a position xy. Assuming the state encodes a 
// truck and trailer vehicle, the distance is the euclidean norm in the XY space
// from the front of the car to an xy position.
float get_car_front_distance(float *state_truck, float *xy, const ProblemDefinition *prob_def_ptr);

// Acts as an wrapper for different heuristic metric functions.
float get_distance(float *state_a, float *state_b, const ProblemDefinition *prob_def_ptr);

// Checks if the goal was reached, according to some metric and paremeters defined
// in the ProblemDefinition object.
bool goal_check(float *state_a, float *state_b, const ProblemDefinition *prob_def_ptr);

#endif	/* DISTANCECOMPUTER_H */

