/* 
 * 
 *     This file is a part of RRT Truck Trailer Planner 
 * 
 */

#ifndef _rrt_H
#define	_rrt_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdlib.h>
#include <cmath> 
#include <math.h>
#include <time.h>

#include "collision.h"
#include "ProblemDefinition.h"
#include "TreeNode.h"
#include "systemSimulator.h"
#include "distanceComputer.h"
#include "systemSimulator.h"
#include "treeManager.h"

// Requires include of some of the Boost C++ Libraries
#include <boost/geometry/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <boost/geometry/algorithms/append.hpp>
#include <boost/geometry/io/wkt/wkt.hpp>
#include <boost/geometry/geometries/adapted/c_array.hpp>
#include <boost/geometry/geometries/multi_polygon.hpp>

using namespace boost::geometry;

#endif	/* _rrt_H */

