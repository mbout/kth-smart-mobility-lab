/*
 *     RRT Truck Trailer Planner - This program tries to plan a feasible 
 *     trajectory that takes a truck and trailer vehicle from an initial 
 *     position to a goal region, while avoiding obstacles and being confined
 *     to a limited environment. 
 *     Based on the algorithm described in: 
 *     Rapidly-exploring random trees: A new tool for path planning.
 *     S. M. LaValle. TR 98-11, Computer Science Dept.,
 *     Iowa State University, October 1998
 * 
 *     Copyright (C) <2015>  <Rui Oliveira> <rfoli@kth.se>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "rrt.h"

int main(int argc, char**argv) {
        
    clock_t t;
    t = clock();
    
    // Create tree for the RRT search
    std::vector<TreeNode> rrt_tree_vector;     
    
    // Create object to store the problem definitions
    ProblemDefinition prob_def = ProblemDefinition();
    
    // Read the obstacle and environment from a file
    prob_def.read_environment_and_obstacles_from_file("cppInputObstacles.txt");
    
    // Create a pointer to our non changing object, so that it can be easily
    // passed to intermediate steps of the search algorithm
    const ProblemDefinition *prob_def_ptr = &prob_def;

    // Define a timeout that when reached, will stop the search algorithm
    float timeout = 1.0;
    
    // Create the goal position pointer
    float goal_xy[2];
        
    // Initialize the tree and goal position from the problem definitions 
    initialize_tree_list(argc, argv, rrt_tree_vector, goal_xy, &prob_def);
    
    // The goal state will have the dimension as our vehicle state
    float goal_state[prob_def.state_dimension];
    goal_state[0] = goal_xy[0];    goal_state[1] = goal_xy[1];
    goal_state[2] = 0;    goal_state[3] = 0;
    
    // Initialize a pointer to keep track of the random state used by the search
    float random_state[prob_def.state_dimension];
    
    // Initialize a pointer to keep track of the best input used by the search
    float best_input[2];
    
    // Initialize a boolean to know if a movement is possible
    bool possible_movement;
    
    // Initialize a pointer to keep track of the state resulting after applying 
    // an input
    float final_state[prob_def.state_dimension];
    
    
    // Initializing a new seed for random generator
    // If we desire to always obtain the same solution for the same initial
    // problem definition, we just need to make sure that the argument of srand
    // is always the same number (e.g. srand (1) );
    srand ( time(NULL) );

    // A variable to keep track of how much time is the algorithm taking
    float time_elapsed;
    
    // A variable to keep track if the goal was found or not after the search 
    // algorithm finishes
    bool goal_found = false;
    
    // Define the maximum allowed iterations
    int max_iterations = 100000;
    
    // A variable to keep track of the closest node in the tree
    int closest_node_index;
    
    // Issue a warning if the problem is unsolvable right from the start
    if (is_in_collision(rrt_tree_vector.at(0).state_configuration, prob_def_ptr) ){
        
        std::cout << "Initial state is in collision, search not attempted!" << std::endl;
        
        // Will skip the search algorithm
        max_iterations = 0;
        
    }
    
    int search_iteration = 0;
    
    while ( search_iteration < max_iterations){
        
        search_iteration++;
        
        // Get a random state
        get_random_state(random_state, goal_state, prob_def_ptr);
                
        // If the random state is in collision ignore it, and try a new one
        if ( is_in_collision(random_state, prob_def_ptr) ){
            
            continue;
            
        }
                
        // Find the closest node in the tree to the current random state
        closest_node_index = find_closest_node(rrt_tree_vector, random_state, prob_def_ptr);
        
        // Check if the current input results in a valid movement
        possible_movement = find_best_input( rrt_tree_vector.at(closest_node_index).state_configuration, random_state, best_input, prob_def_ptr); 
        
        // In case movement is impossible, ignore it, and start over
        if (!possible_movement){
            
            continue;
            
        }        
        
        euler_integration( rrt_tree_vector.at(closest_node_index).state_configuration, final_state, best_input, prob_def_ptr);     
        
        // In case the final state is in collision, ignore it, and start over
        if ( is_in_collision(final_state, prob_def_ptr) ){
            
            continue;
            
        }
        
        // In case the final state is not inside the environment, ignore it, 
        // and start over
        if ( !is_inside_environment(final_state, prob_def_ptr) ){
        
            continue;
            
        }
        
        // The final state has passed all the tests, add it to the tree
        add_tree_node(rrt_tree_vector, final_state, closest_node_index, prob_def_ptr);
        
        // Check if this final state is close enough to the goal state
        if ( goal_check(final_state, goal_state, prob_def_ptr) ){
            
            std::cout << "Goal found." << std::endl;
            goal_found = true;
            
            // Stop the search loop
            break;
            
        }
        
        // Measure the current time the search has taken
        time_elapsed = clock() - ((float)t);
        time_elapsed = time_elapsed/CLOCKS_PER_SEC;
        
        // If it has passed more time that the allowed, stop the search loop
        if ( time_elapsed > timeout ){
            
            std::cout << "Too much time spent in the search (timeout = " 
                    << timeout << ")" << std::endl;
            goal_found = false;
            break;
            
        }
                
    }
    
    // If the goal was not found, simply find the closest node in the tree to it
    if (!goal_found){
        std::cout << "Goal not found" << std::endl;
    }
    
    // Show some statistics to the user
    std::cout << "Searched " << rrt_tree_vector.size() << " nodes in the tree, with a total of "
            << search_iteration << " search iterations." << std::endl;
    t = clock() - t;
    printf ("It took me %lu clicks (%f seconds).\n",t,((float)t)/CLOCKS_PER_SEC);
    
    // Write the tree nodes to a file
    write_tree_nodes_to_file(rrt_tree_vector, "cppOutputTreeNodes.txt");
    
    int leaf_node_id;
    
    if (goal_found){
        
        // In case the goal was found we just take the last node added
        leaf_node_id = rrt_tree_vector.size()-1;
        
    }else{
        
        // In case the goal was not found, we choose the leaf node closest to
        // the goal
        leaf_node_id = find_closest_node(rrt_tree_vector, goal_state, prob_def_ptr);
        
    }
    
    // Write the tree solution to a file
    write_tree_solution_to_file(rrt_tree_vector, "cppOutputTreeSolution.txt", leaf_node_id);
           
    return 0;
    
}


