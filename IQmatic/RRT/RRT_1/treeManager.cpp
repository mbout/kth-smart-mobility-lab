/* 
 *     This file is a part of RRT Truck Trailer Planner 
 * 
 *     Copyright (C) <2015>  <Rui Oliveira> <rfoli@kth.se>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "treeManager.h"

void initialize_tree_list(int argc, char**argv, std::vector<TreeNode> &rrt_tree_vector, float *goal_xy, ProblemDefinition *prob_def_ptr){
    
    TreeNode initial_tree_node;
   
    if ( argc == 14 ){
        
        std::cout << "14 arguments provided, will use them as initial" <<
                " configuration and goal point and environment limits," <<
                " goal tolerance, and simulation time (1 meter per second)," <<
                " and max steering\n";
        initial_tree_node.state_configuration[0] = atof( argv[1] );
        initial_tree_node.state_configuration[1] = atof( argv[2] );
        initial_tree_node.state_configuration[2] = atof( argv[3] );
        initial_tree_node.state_configuration[3] = atof( argv[4] );
        goal_xy[0] = atof( argv[5] );
        goal_xy[1] = atof( argv[6] );
        std::cout << "Ignoring provided space limits, will compute them from the environment limits polygon\n";
      
        prob_def_ptr->goal_tolerance = atof( argv[11] );
        prob_def_ptr->euler_simulation_time = atof( argv[12] );
        prob_def_ptr->max_steering_angle = atof( argv[13] );
        
    }else{
        
        std::cout << "Invalid number of arguments, will use default problem definitions\t";
        initial_tree_node.state_configuration[0] = -prob_def_ptr->environment_width/4.;
        initial_tree_node.state_configuration[1] = -prob_def_ptr->environment_width/4.;
        initial_tree_node.state_configuration[2] = M_PI;
        initial_tree_node.state_configuration[3] = M_PI;
        goal_xy[0] = 0;
        goal_xy[1] = 0;
        prob_def_ptr->goal_tolerance = 0.1;
        prob_def_ptr->euler_simulation_time = 0.1;
        prob_def_ptr->max_steering_angle = M_PI/6;
        
    }    
    
    std::cout << "Initial configuration: " << "(" << initial_tree_node.state_configuration[0] << ", " 
                << initial_tree_node.state_configuration[1] << ", " 
                << initial_tree_node.state_configuration[2] << ", " 
                << initial_tree_node.state_configuration[3] << ")\t";
    std::cout << "Goal: " << "(" << goal_xy[0] << ", " << goal_xy[1] << ")\t";
    std::cout << "Goal tolerance: " << prob_def_ptr->goal_tolerance << "\t";
    std::cout << "Environment limits: " << "[" << prob_def_ptr->environment_min_x << ", " 
            << prob_def_ptr->environment_max_x << "]x[" << prob_def_ptr->environment_min_y << ", " 
            << prob_def_ptr->environment_max_y << "]\t";
    std::cout << "Goal tolerance: " << prob_def_ptr->goal_tolerance << "\t";
    std::cout << "Euler simulation time: " << prob_def_ptr->euler_simulation_time << "\t";
    std::cout << "Maximum steering angle: " << prob_def_ptr->max_steering_angle << "\t";
    
    initial_tree_node.parent_node_id = -1;
    
    rrt_tree_vector.push_back(initial_tree_node);
    
}

int find_closest_node(std::vector<TreeNode> &rrt_tree_vector, float *goal_state, const ProblemDefinition *prob_def_ptr){
    
    float minimal_distance = 1000;
    int minimal_distance_index = -1;
    
    // Search the whole tree, and find the node with the smallest distance, as
    // given by the get_distance function
    for (unsigned i = 0; i < rrt_tree_vector.size(); i++){
        
        float * state_configuration;
        state_configuration = rrt_tree_vector.at(i).state_configuration;
        
        float current_distance;
        current_distance = get_distance(state_configuration, goal_state, prob_def_ptr);
                
        if ( current_distance < minimal_distance ){
            
            minimal_distance = current_distance;
            minimal_distance_index = i;
            
        }
        
    }
    
    return minimal_distance_index;
    
}

void add_tree_node(std::vector<TreeNode> &rrt_tree_vector, float *new_state,int parent_index, const ProblemDefinition *prob_def_ptr){
    
    TreeNode new_tree_node;
    
    for (int i = 0; i < prob_def_ptr->state_dimension; i++){
    
        new_tree_node.state_configuration[i] = new_state[i];

    }
    
    new_tree_node.parent_node_id = parent_index;
    
    rrt_tree_vector.push_back(new_tree_node);
    
}

void print_tree_nodes(std::vector<TreeNode> &rrt_tree_vector, const ProblemDefinition *prob_def_ptr){
    
    std::cout << "Printing tree" << std::endl;
    
    for (unsigned i = 0; i < rrt_tree_vector.size(); i++){
    
        TreeNode current_tree_node = rrt_tree_vector.at(i);
        
        std::cout << "Node number: " << i  << " with parent " << current_tree_node.parent_node_id << std::endl;
        
        for (int j = 0; j < prob_def_ptr->state_dimension; j++){
            
            std::cout << current_tree_node.state_configuration[j] << " ";
            
        }
        
        std::cout << std::endl;

    }
        
}


void write_tree_nodes_to_file(std::vector<TreeNode> &rrt_tree_vector, std::string file_name){
    
    std::ofstream myFile;
   
    myFile.open (file_name.c_str());
    
    for (unsigned i = 0; i < rrt_tree_vector.size(); i++ ){
        TreeNode current_tree_node = rrt_tree_vector.at(i);
        myFile << current_tree_node.state_configuration[0] << " " << current_tree_node.state_configuration[1]
                << " " << current_tree_node.parent_node_id  << "\n";
    }
    
    myFile.close();
    
}


void write_tree_solution_to_file(std::vector<TreeNode> &rrt_tree_vector, std::string file_name, int leaf_node_id){
    
    // Write in a file the solution branch (or the branch that takes us closer
    // to the goal)
    std::ofstream myFile;
    
    myFile.open (file_name.c_str());
        
    // Write the node ids, starting from the leaf, and following the parents 
    // until the root node
    while ( leaf_node_id != -1){

        myFile << leaf_node_id << "\n";
        leaf_node_id = rrt_tree_vector.at(leaf_node_id).parent_node_id;            

    }
    
    myFile.close();
    
}
