#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=Cygwin_4.x-Windows
CND_DLIB_EXT=dll
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/ProblemDefinition.o \
	${OBJECTDIR}/collision.o \
	${OBJECTDIR}/distanceComputer.o \
	${OBJECTDIR}/rrt.o \
	${OBJECTDIR}/systemSimulator.o \
	${OBJECTDIR}/treeManager.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/rrt_1.exe

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/rrt_1.exe: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/rrt_1 ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/ProblemDefinition.o: ProblemDefinition.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -Wall -I../../../../boost_1_58_0 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ProblemDefinition.o ProblemDefinition.cpp

${OBJECTDIR}/collision.o: collision.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -Wall -I../../../../boost_1_58_0 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/collision.o collision.cpp

${OBJECTDIR}/distanceComputer.o: distanceComputer.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -Wall -I../../../../boost_1_58_0 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/distanceComputer.o distanceComputer.cpp

${OBJECTDIR}/rrt.o: rrt.cc 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -Wall -I../../../../boost_1_58_0 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/rrt.o rrt.cc

${OBJECTDIR}/systemSimulator.o: systemSimulator.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -Wall -I../../../../boost_1_58_0 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/systemSimulator.o systemSimulator.cpp

${OBJECTDIR}/treeManager.o: treeManager.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -Wall -I../../../../boost_1_58_0 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/treeManager.o treeManager.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/rrt_1.exe

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
