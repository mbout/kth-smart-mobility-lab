/* 
 *     This file is a part of RRT Truck Trailer Planner 
 * 
 *     Copyright (C) <2015>  <Rui Oliveira> <rfoli@kth.se>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "distanceComputer.h"
    
float get_metric_distance(float *state_a, float *state_b){
    
    float distance = 0;
    
    for (int i = 0; i < 4; i++){
        
        distance = distance + std::abs( state_a[i] - state_b[i] );
    
    }
    
    return distance;
        
}

float get_xy_distance(float *state_a, float *state_b){
        
    float distance = 0;
    
    for (int i = 0; i < 2; i++){
        
        distance = distance + std::abs( state_a[i] - state_b[i] );
    
    }
    
    return distance;
        
}

float get_car_front_distance(float *state_truck, float *xy, const ProblemDefinition *prob_def_ptr){
    
    float distance = 0;
    
    float truck_front_pos[2];
    
    // Computing the position of the front of the truck
    truck_front_pos[0] = state_truck[0] + prob_def_ptr->trailer_length*cos(state_truck[2]) + prob_def_ptr->truck_length*cos(state_truck[3]);
    truck_front_pos[1] = state_truck[1] + prob_def_ptr->trailer_length*sin(state_truck[2]) + prob_def_ptr->truck_length*sin(state_truck[3]);
    
    
    for (int i = 0; i < 2; i++){
        
        distance = distance + std::abs( truck_front_pos[i] - xy[i] );
    
    }
    
    return distance;
        
}

float get_distance(float *state_a, float *state_b, const ProblemDefinition *prob_def_ptr){
            
    // Change the heuristic by changing this function
    // return get_metric_distance(state_a, state_b);
    // return get_xy_distance(state_a, state_b);
    
    float xy_point[2];
    xy_point[0] = state_b[0];
    xy_point[1] = state_b[1];
        
    return get_car_front_distance(state_a, xy_point, prob_def_ptr);
            
}


bool goal_check(float *state_a, float *state_b, const ProblemDefinition *prob_def_ptr){
    
    if ( get_distance(state_a, state_b, prob_def_ptr) < prob_def_ptr->goal_tolerance ){
        
        return true;
        
    }
    
    return false;
    
}