import sys, string, os, math, time
import pygame
import xml.etree.ElementTree as ET
from xml.dom import minidom

def make_file_empty(filename):

	f = open(filename, 'w')

	f.write("")

	f.close()

def make_empty_file(filename):

	f = open(filename, 'w')

	f.write("")

	f.close()

def read_tree_nodes_file(filename):

	f = open(filename, 'r')

	tree_nodes = []

	for line in f:

		current_node = []

		numbersList = line.split()

		current_node.append( float(numbersList[0]) ) # the x-position 
		current_node.append( float(numbersList[1]) ) # the y-position 
		current_node.append( int(numbersList[2]) ) # the parent id

		tree_nodes.append( current_node )

	# print tree_nodes

	return tree_nodes

def read_tree_solution_file(filename):

	f = open(filename, 'r')

	tree_solution = []

	for line in f:
		
		tree_solution.append( int( line ) ) # the parent id

	# print tree_nodes

	return tree_solution

def get_rrt_arguments(initial_pose=[1.,1.,1.,1.], goal_coord=[-1.,-1.], space_limits=[-2.,2.,-2.,2.], goal_tolerance=.1, euler_sim_time=.1, max_steering_angle=.35):

	x = initial_pose[0]
	y = initial_pose[1]
	theta_0 = initial_pose[2]
	theta_1 = initial_pose[3]

	goal_x = goal_coord[0]
	goal_y = goal_coord[1]

	space_lim_min_x = space_limits[0]
	space_lim_max_x = space_limits[1]
	space_lim_min_y = space_limits[2]
	space_lim_max_y = space_limits[3]

	goal_tolerance = 0.1
	euler_sim_time = 0.1
	max_steering_angle = math.radians(20)

	args_string = str(x) + str(' ') + str(y) + str(' ') + str(theta_0) + str(' ') +	str(theta_1)
	args_string = args_string + str(' ') + str(goal_x) + str(' ') + str(goal_y)
	args_string = args_string + str(' ') + str(space_lim_min_x) + str(' ') + str(space_lim_max_x) + str(' ') + str(space_lim_min_y) + str(' ') + str(space_lim_max_y)	
	args_string = args_string + str(' ') + str(goal_tolerance) + str(' ') + str(euler_sim_time) + str(' ') + str(max_steering_angle)

	return args_string

def get_tree_surface(canvas_width, canvas_height, tree_nodes, solution_nodes, space_limits):

	resized_tree_nodes = []

	rescale_x = canvas_width/( space_limits[1] - space_limits[0] )
	rescale_y = canvas_height/( space_limits[3] - space_limits[2] )

	for tree_node in tree_nodes:

		resized_tree_nodes.append( [ (tree_node[0]-space_limits[0])*rescale_x, (tree_node[1]-space_limits[2])*rescale_y, tree_node[2] ] )


	world_surface = pygame.Surface((canvas_width, canvas_height), 0, 32)

	fill_color = (0, 0, 0)
	world_surface.fill(fill_color)

	tree_color = (255, 255, 255)
	branch_color = (255, 0, 0)
	line_closed = False
	line_thickness = int(4.0)


	for current_tree_node_id in range(1, len(tree_nodes) ):

		current_tree_node = resized_tree_nodes[current_tree_node_id]
		current_parent_tree_node = resized_tree_nodes[ current_tree_node[2] ]

		line_tuple_list = []
		line_tuple_list.append( ( int(current_tree_node[0]),int(current_tree_node[1]) ) )
		line_tuple_list.append( ( int(current_parent_tree_node[0]),int(current_parent_tree_node[1]) ) )

		pygame.draw.lines(world_surface, tree_color, line_closed, tuple( line_tuple_list ), line_thickness)

	for solution_nodes_it in range(1, len(solution_nodes) ):

		solution_node_id = solution_nodes[solution_nodes_it]
		solution_node_parent_id = solution_nodes[solution_nodes_it-1]

		current_tree_node = resized_tree_nodes[solution_node_id]
		current_parent_tree_node = resized_tree_nodes[ solution_node_parent_id ]

		line_tuple_list = []
		line_tuple_list.append( ( int(current_tree_node[0]),int(current_tree_node[1]) ) )
		line_tuple_list.append( ( int(current_parent_tree_node[0]),int(current_parent_tree_node[1]) ) )

		pygame.draw.lines(world_surface, branch_color, line_closed, tuple( line_tuple_list ), line_thickness)

	return world_surface

def call_rrt(initial_pose=[1.,1.,1.,1.], goal_coord=[-1.,-1.], space_limits=[-2.,2.,-2.,2.], goal_tolerance=.1, euler_sim_time=.1,	max_steering_angle=.35):

	make_empty_file("obstacles_read_by_cpp.txt")

	args_string = get_rrt_arguments(initial_pose, goal_coord, space_limits, goal_tolerance, euler_sim_time, max_steering_angle)

	if sys.platform == 'linux2':
		executable_name = "./rrt"
	else:
		executable_name = "rrt_1.exe"

	os.system(executable_name + ' ' + args_string)

	tree_nodes =  read_tree_nodes_file("cppOutputTreeNodes.txt")

	solution_nodes = read_tree_solution_file("cppOutputTreeSolution.txt")

	return [tree_nodes, solution_nodes]

def draw_rrt(tree_nodes, solution_nodes, space_limits):

	image_width = 900
	image_height = 900

	rrt_tree_surface = get_tree_surface(image_width, image_height, tree_nodes, solution_nodes, space_limits)

	size = width, height = image_width, image_height

	screen = pygame.display.set_mode(size)

	# screen.fill(BLACK)

	screen.blit(rrt_tree_surface, (0,0) )

	pygame.display.flip()

	print "DOOOOOOOOOOOOOOOOOOOOOONNNNNNNNNNNNNNNNNNNNNNNNNNNNEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE"

	time.sleep(3.0)

def prettify(elem):
    """Return a pretty-printed XML string for the Element.
    """
    rough_string = ET.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")

def get_rrt_solution_message(rrt_nodes, solution_ids):
	"Process an incoming controlled_body_info message"
	
	root = ET.Element('message')

	root.set("type", "rrt_solution")

	tree_root = ET.SubElement(root, "tree")

	# for tree_node in rrt_nodes:
	for it in range( len( rrt_nodes ) ):

		tree_node = rrt_nodes[it]
		ET.SubElement(tree_root, "tree_node", id = str(it), x = str(tree_node[0]), y = str(tree_node[1]), parent_id = str(tree_node[2]) )

	solution_root = ET.SubElement(root, "solution")

	# for tree_node in rrt_nodes:
	for it in range( len( solution_ids ) ):

		solution_id = solution_ids[it]
		ET.SubElement(solution_root, "node", id = str(solution_id), order = str(it) )

	# print "tree:"
	xml_string = ET.tostring(root)
	# print xml_string
	# print prettify(root)

	return xml_string




space_limits=[-2.,2.,-2.,2.]

[tree_nodes, solution_nodes] = call_rrt(space_limits = space_limits)

get_rrt_solution_message(tree_nodes, solution_nodes)

draw_rrt(tree_nodes, solution_nodes, space_limits)
