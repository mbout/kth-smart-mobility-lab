/* 
 * 
 *     This file is a part of RRT Truck Trailer Planner 
 * 
 */

#ifndef PROBLEMDEFINITION_H
#define	PROBLEMDEFINITION_H

#include <iostream>
#include <vector>
#include <fstream> 
#include <stdlib.h>
#include <math.h>

// Requires include of some of the Boost C++ Libraries
#include <boost/geometry/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <boost/geometry/algorithms/append.hpp>
#include <boost/geometry/io/wkt/wkt.hpp>
#include <boost/geometry/geometries/adapted/c_array.hpp>
#include <boost/geometry/geometries/multi_polygon.hpp>

using namespace boost::geometry;

// A class with the main purpose of storing relevant information of the problem,
// as defined by the user.
class ProblemDefinition {
public:
    
    // The dimension of the state of the vehicle we are planning for
    int state_dimension;
    
    // The simulation time used each euler simulation
    float euler_simulation_time;
    
    // The number os steps that each euler simulation has
    int number_euler_steps;

    // The length of the truck (tractor) in meters
    float truck_length;
        
    // The width of the truck (tractor) in meters
    float truck_width;
    
    // The length of the trailer in meters
    float trailer_length;
    
    // The width of the trailer in meters
    float trailer_width;

    // The width of the environment in meters
    float environment_width;
    
    // The minimum x coordinate of the environment in meters
    float environment_min_x;
    
    // The maximum x coordinate of the environment in meters
    float environment_max_x;
    
    // The minimum y coordinate of the environment in meters
    float environment_min_y;
    
    // The maximum y coordinate of the environment in meters
    float environment_max_y;
    
    // The allowed goal tolerance in meters. That is, we need to get, at least,
    // as close as goal_tolerance, in order to consider the planning solved.
    float goal_tolerance;    
    
    // The maximum allowed steering angle in radians.
    float max_steering_angle;
    
    // A polygon defining the environment limits
    model::polygon<boost::geometry::model::d2::point_xy<double> > polygon_environment;
    
    // A list of polygons defining the obstacles
    std::vector<model::polygon<boost::geometry::model::d2::point_xy<double> > > polygon_obstacles_list;
    
    ProblemDefinition(); 
    ProblemDefinition(const ProblemDefinition& orig);
    
    // Read the environment and obstacle polygons from a file location given
    // by the input argument.
    void read_environment_and_obstacles_from_file(std::string file_name);
    
    virtual ~ProblemDefinition();
private:
    
    // Update the space limit attributes according to the current polygon environment.
    void update_space_limits(std::vector<double> * current_points_x, std::vector<double> * current_points_y);
    
    // Store the points currently parsed from a file, into the corresponding polygon.
    void store_points(std::vector<double> * current_points_x, std::vector<double> * current_points_y, bool was_obstacle, bool was_limits);

    // Parse a file line, and get the points in it.
    void parse_point_line(std::string current_line, double point_coord[]);

    // Push the points given as argument to a buffer given as argument.
    void push_points_to_buffer(std::vector<double> * current_points_x, std::vector<double> * current_points_y, double point_coord[]);
    
};



#endif	/* PROBLEMDEFINITION_H */

