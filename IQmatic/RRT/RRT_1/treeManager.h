/* 
 * 
 *     This file is a part of RRT Truck Trailer Planner 
 * 
 */

// The functions here defined, are used to manipulate and use the search tree

#ifndef TREEMANAGER_H
#define	TREEMANAGER_H

#include "TreeNode.h"
#include "ProblemDefinition.h"
#include "distanceComputer.h"

#include <vector>
#include <iostream>
#include <cmath>        // M_PI constant
#include <stdlib.h>     /* atof */

// Initializes the tree structure, that will contain the nodes used by the search 
// algorithm. It also changes some parameters in the ProblemDefinition object, 
// in case they have been defined by the user, when it call the executable.
void initialize_tree_list(int , char**, std::vector<TreeNode> &, float *, ProblemDefinition *);

// Finds the closest node in the tree to a determined state, returning its index.
int find_closest_node(std::vector<TreeNode> &, float *, const ProblemDefinition *);

// Adds a new node to the tree, given a parent index and a state.
void add_tree_node(std::vector<TreeNode> &, float *,int , const ProblemDefinition *);

// Prints the tree nodes in the output (usually terminal).
void print_tree_nodes(std::vector<TreeNode> &, const ProblemDefinition *);

// Writes the tree nodes to the file location specified by the input argument.
// The file is structured as a series of line, where each line contains the 
// xy position of the node and the corresponding id
void write_tree_nodes_to_file(std::vector<TreeNode> &, std::string);

// Writes the tree solution to the file location specified by the input argument.
// The file is structured as a series of line, where each line the id of each 
// consecutive node in the solution branch.
void write_tree_solution_to_file(std::vector<TreeNode> &, std::string, int);

#endif	/* TREEMANAGER_H */

