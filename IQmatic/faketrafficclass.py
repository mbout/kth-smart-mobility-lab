
import socket, sys, time, math, random, threading
import multiprocessing
# import xml.etree.ElementTree as ET


# sys.path.append('..\Lanelets')
# import laneletlibrary


from multiprocessing import Pool

sys.path.append('Lanelets')
import laneletmodule
import laneletlibrary




class FakeTraffic:
	"This class implements the Vehicle Simulator Module to be running in the SML World"

	def __init__(self):

		# self.osm_info = osm_info

		# self.CLOSE = False

		# self.vehicles = []

		self.iteration = 0

		self.vehicle_states = dict()

		self.vehicle_inputs = dict()

		self.construction_time = time.time()

		self.bodies_readings = []

		self.simulation_step_counter = 0

	def create_random_car(self):

		x_value = random.random()
		y_value = random.random()
		theta_value = random.random()*math.pi*2.

		current_id = - len(self.vehicle_states) - 1

		self.vehicle_states[current_id] = [x_value, y_value, theta_value]
		self.vehicle_inputs[current_id] = [0., 0.]

	def get_bodies_list(self):

		bodies_list = []

		for current_id in self.vehicle_states.keys():

			current_body = dict()

			current_state = self.vehicle_states[current_id]

			current_body['id'] = current_id
			current_body['x'] = current_state[0]
			current_body['y'] = current_state[1]
			current_body['yaw'] = current_state[2]

			bodies_list.append(current_body)

			self.vehicle_inputs[current_id] = [0.01, 0.1]

		return bodies_list

	def update_car_states(self, bodies_list):

		car_ids = self.vehicle_states.keys()		

		for current_body in bodies_list:

			current_id = current_body['id']

			if current_id in car_ids:

				self.vehicle_states[current_id] = [current_body['x'], current_body['y'], current_body['yaw']]

	def update_inputs(self):

		for current_id in self.vehicle_states.keys():

			self.vehicle_inputs[current_id] = [0.01, 0.1]
