#!/usr/bin/python

import threading
import sys
import os
import errno
import math
import random
import datetime
import time
import pygame

import simulatorclass
import faketrafficclass

from PIL import Image
from socket import *
from multiprocessing import Process

from socket import error as socketerror
from socket import herror as socketherror
# from socket import gaierror as socketerror
from socket import timeout as sockettimeout

from PyQt4 import QtCore, QtGui

sys.path.append('Lanelets')
import laneletmodule
import laneletlibrary

sys.path.append('RRT')
import rrtlibrary

sys.path.append('GUI')
import smlgui

import vehiclesimulator
import constructionvehiclesimulator
import realvehiclemanager
import xmlcomms
import v2vmodule
import v2imodule
import vehiclecommandmanager
import peoplemanager
import bodyclasses
from mocap import Mocap

import yappi

import platoonCommand


# noinspection PyBroadException,PyArgumentList
class SMLworld(object):
	"""SMLworld project"""

	def __init__(self, server_mode_active):
		super(SMLworld, self).__init__()

		self.colors = self.get_color

		self.close = False

		self.server_mode = server_mode_active

		self.threads = []
		self.qualisys_info = None
		self.Qs = None
		self.gui = None

		self.log_xml = False
		self.mocap_on = False
		self.gui_changed = False
		self.vehicle_simulator_ready = False		
		self.projector_wait_for_command_central = False
		self.load_saved_background = False
		self.close_sml_world = False

		self.highlight_mode = False
		self.highlighted_vehicle_id = 0
		self.server_port = 0
		self.buffersize = 1024

		self.list_of_modules = ['main','mocap','highlight','projector_manager','command_central','v2i','v2v','simulated_vehicles','vehicle_simulation','simulator_receiver','simulator','vehicle_manager','vehicle_controller','vehicle_command_manager','vehicle_output_manager']

		for module in self.list_of_modules:
			setattr(self, module + '_connected', False)
			setattr(self, module + '_desired_rate', 1)
			setattr(self, module + '_info', dict())


		self.activate_draw_origin = False
		self.draw_sensors = False
		self.draw_signal = False
		self.show_ids = False
		self.activate_fps = False
		self.activate_states_from_mocap = False
		self.activate_states_from_world = False
		self.only_simulated_vehicles = False
		self.only_real_vehicles = False

		self.bodies_list_xml = ''

		self.rrt_reply_string = ''
		self.msg_to_reroute_to_projector = ''
		self.msg_to_reroute_to_cmd_central = ''
		self.v2v_method = ''
		self.cmd_data = ''
		self.msg_task_to_projector = None
		self.msg_to_reroute_to_construction = None
		self.new_message = None
		self.v2v_range = ''
		self.v2v_angle = ''
		self.number_vehicles = 0
		self.current_simulated_id = -1
		self.projector_area = []
		self.projector_info = tuple()
		self.projector_resolution = tuple()
		self.xml_file_location = ''

		self.msg_to_reroute = dict()
		self.msg_to_reroute_to_simulator = dict()
		# self.simulator_body = []

		# This is the dictionary which will have all of the bodies (real/simulated, smart/dummy, bus/persons, etc...) in the World
		self.bodies_array = dict() 

		self.vehicle_info = []
		self.controllables_list = []
		self.bodies_readings = []
		self.bodies_list = []
		self.obstructed_lanelets = []
		self.projector_thread = []
		self.construction_vehicles = []

		self.obstacles_list = self.read_obstacles_from_file()

		self.trailer_list, self.vehicle_info = self.read_vehicle_info_from_file()

		if not self.server_mode:
			self.Dialog = QtGui.QDialog()
			self.gui = smlgui.Ui_SMLworld()
			self.gui.setupUi(self.Dialog)

			# enable custom window hint
			self.Dialog.setWindowFlags(self.Dialog.windowFlags() | QtCore.Qt.CustomizeWindowHint)

			# disable (but not hide) close button
			self.Dialog.setWindowFlags(self.Dialog.windowFlags() & ~QtCore.Qt.WindowCloseButtonHint)

			self.Dialog.setWindowFlags(self.Dialog.windowFlags() & ~QtCore.Qt.WindowTitleHint)

			[self.gui.obstacle_id.addItem(str(obstacle[0])) for obstacle in self.obstacles_list]

			self.change_obstacle_id()

			self.gui.stop.setVisible(False)
			self.gui.simulatedprogressbar.setVisible(False)
			self.gui.stop_projector.setVisible(False)
			self.gui.start_projector.setDisabled(True)
			self.gui.waiting_label.setVisible(False)

			QtCore.QObject.connect(self.gui.start, QtCore.SIGNAL("clicked()"), self.start_sml_world)
			QtCore.QObject.connect(self.gui.stop, QtCore.SIGNAL("clicked()"), self.stop_sml_world)

			QtCore.QObject.connect(self.gui.openimage, QtCore.SIGNAL("clicked()"), self.open_background_image)
			QtCore.QObject.connect(self.gui.xml_file_location_button, QtCore.SIGNAL("clicked()"), self.new_xml_file_location)

			QtCore.QObject.connect(self.gui.start_projector, QtCore.SIGNAL("clicked()"), self.start_projector)
			QtCore.QObject.connect(self.gui.stop_projector, QtCore.SIGNAL("clicked()"), self.stop_projector)

			QtCore.QObject.connect(self.gui.close_button, QtCore.SIGNAL("clicked()"), self.close_world)
			QtCore.QObject.connect(self.gui.close_button, QtCore.SIGNAL("changeEvent()"), self.close_world)

			QtCore.QObject.connect(self.gui.save_settings, QtCore.SIGNAL("clicked()"), self.save_settings_in_file)

			QtCore.QObject.connect(self.gui.obstacle_type, QtCore.SIGNAL("currentIndexChanged(const QString&)"), self.change_obstacle_type)
			QtCore.QObject.connect(self.gui.obstacle_id, QtCore.SIGNAL("currentIndexChanged(const QString&)"), self.change_obstacle_id)

			QtCore.QObject.connect(self.gui.obstacle_radius, QtCore.SIGNAL("valueChanged(double)"), self.change_obstacle_radius)
			QtCore.QObject.connect(self.gui.obstacle_width, QtCore.SIGNAL("valueChanged(double)"), self.change_obstacle_width)
			QtCore.QObject.connect(self.gui.obstacle_height, QtCore.SIGNAL("valueChanged(double)"), self.change_obstacle_height)

			QtCore.QObject.connect(self.gui.obstacle_add, QtCore.SIGNAL("clicked()"), self.add_obstacle)
			QtCore.QObject.connect(self.gui.obstacle_remove, QtCore.SIGNAL("clicked()"), self.remove_obstacle)

			QtCore.QObject.connect(self.gui.trailer_status, QtCore.SIGNAL("clicked()"), self.trailer_status_change)
			QtCore.QObject.connect(self.gui.body_id, QtCore.SIGNAL("currentIndexChanged(const QString&)"), self.vehicle_id_change)
			QtCore.QObject.connect(self.gui.body_type, QtCore.SIGNAL("currentIndexChanged(const QString&)"), self.vehicle_type_change)
			QtCore.QObject.connect(self.gui.trailer_id, QtCore.SIGNAL("currentIndexChanged(const QString&)"), self.trailer_id_change)


		self.read_default_settings_from_file()


		self.platoonCommandPort = 34978

	@property
	def get_color(self):
		colors = dict()
		colors['HEADER'] = '\033[95m'
		colors['OKBLUE'] = '\033[94m'
		colors['OKGREEN'] = '\033[92m'
		colors['WARNING'] = '\033[93m'
		colors['FAIL'] = '\033[91m'
		colors['ENDC'] = '\033[0m'
		colors['BOLD'] = '\033[1m'
		colors['UNDERLINE'] = '\033[4m'
		colors['WORLD'] = '\033[1;97m'
		colors['PROJECTOR'] = '\033[1;32m'
		colors['VEHICLE'] = '\033[1;95m'
		colors['CMD'] = '\033[1;93m'
		colors['QUALISYS'] = '\033[1;96m'

		return colors

	def timed_print(self, message, color=None, parent=None):
		if color in self.colors:
			color = self.colors[color]
		else:
			color = ''
		if parent in self.colors:
			parent = self.colors[parent]
		else:
			parent = ''
		print parent + self.get_current_time + self.colors['ENDC'] + ' ' + color + message + self.colors['ENDC']

	@property
	def get_current_time(self):
		return datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S ")

	def start_thread(self, handler, args=()):
		t = threading.Thread(target=handler, args=args)
		t.daemon = True
		self.threads.append(t)
		t.start()

	def waiting_for_desired_rate(self, starting_time, stopping_time, module, parent_function_str = ''):
		if stopping_time - starting_time > 0:
			rate = int(round(1. / (stopping_time - starting_time)))
			if rate > getattr(self,module + '_desired_rate'):
				time.sleep(1. / getattr(self,module + '_desired_rate') - 1. / rate)
			else:
				print "Something is not sleeping, and it is: " + parent_function_str + " from module " + module
			getattr(self, module + '_info')['rate'] = str(int(round(1 / (time.time() - starting_time))))
		elif stopping_time - starting_time == 0:
			time.sleep(1. / getattr(self,module + '_desired_rate'))
		
			

	def start_mocap_socket(self):

		while not self.close:
			tic = time.time()
			try:
				self.Qs = Mocap(self.qualisys_info[0], self.qualisys_info[1])
			except:
				toc = time.time()
				self.waiting_for_desired_rate(tic,toc,'mocap')
				return None
			if self.Qs.socket is None:
				if self.mocap_on:
					self.timed_print('Qualisys computer is down please turn it on', 'FAIL')
					self.mocap_on = False
			else:
				break
		self.timed_print('Connected to Qualisys at ' + self.Qs.host + ' in port:' + str(self.Qs.port), 'OKBLUE', 'QUALISYS')

		self.timed_print('Qualisys computer is ON', 'OKGREEN', parent='QUALISYS')
		self.mocap_connected = True
		self.mocap_info['ip'] = self.Qs.host
		self.mocap_info['port'] = str(self.Qs.port)
		self.mocap_info['rate'] = str(0)

	def synchronize_bodies_lists(self, current_bodies_list, bodies_list):
		# current_bodies_list is the most recent info
		# bodies_list is the previous info
	
		for current_body in current_bodies_list:
			for body in bodies_list:
				if current_body['id'] == body['id']:
					if 'controllable' in body:
						current_body['controllable'] = body['controllable']
					else:
						current_body['controllable'] = False

		merged_bodies_list = current_bodies_list

		id_list = [current_body['id'] for current_body in merged_bodies_list]
		[merged_bodies_list.append(current_body) for current_body in bodies_list if (current_body['id'] not in id_list)]

		merged_bodies_ids_list = []

		obstacles_id_list = [obstacle[0] for obstacle in self.obstacles_list]

		antenna_ids = [vehicle[0] for vehicle in self.vehicle_info if vehicle[1] == 'antenna']

		antennas_position = []

		for body in merged_bodies_list:
			if body['id'] in antenna_ids:
				antennas_position.append([body['x'], body['y']])

		for body in merged_bodies_list:
			merged_bodies_ids_list.append(body['id'])

			if 'controllable' in body and body['id'] not in self.controllables_list and body['controllable']:
				body['controllable'] = False

			body['trailer'] = body['id'] in self.trailer_list

			if 'body_type' not in body:
				body['body_type'] = 'truck'
			if 'trailer_id' not in body:
				body['trailer_id'] = 0

			for vehicle in self.vehicle_info:
				if vehicle[0] == body['id']:
					body['body_type'] = vehicle[1]
					body['trailer_id'] = vehicle[2]

			for tag in ['x', 'y', 'z', 'roll', 'pitch', 'yaw']:
				if tag not in body:
					body[tag] = 0.0

			if antennas_position:

				antenna_range = 150 / 32

				distance_to_antennas = min(
					[math.hypot(body['x'] - other_body[0], body['y'] - other_body[1]) for other_body in antennas_position])

				distance_to_antennas = distance_to_antennas if distance_to_antennas < antenna_range else antenna_range

				body['signal_strength'] = (antenna_range - distance_to_antennas) / antenna_range

			else:
				body['signal_strength'] = -1.0

			body['collision'] = body['id'] in [reading['id'] for reading in self.bodies_readings if reading['readings']]

			try:
				id_index = obstacles_id_list.index(body['id'])
			except:
				id_index = None

			if id_index is not None:
				body['obstacle'] = True
				body['obstacle_type'] = self.obstacles_list[id_index][1]
				if body['obstacle_type'] in ['gun']:
					body['radius'] = self.obstacles_list[id_index][2]

					pitch = math.radians(-body['pitch'])
					yaw = math.radians(body['yaw'])

					body['x'] += body['z'] * math.cos(yaw) / math.tan(pitch)
					body['y'] += body['z'] * math.sin(yaw) / math.tan(pitch)
				elif body['obstacle_type'] in ['circle']:
					body['radius'] = self.obstacles_list[id_index][2]
					if self.obstacles_list[id_index] > 3:
						body['obstacle_type'] = self.obstacles_list[id_index][3]
					else:
						body['obstacle_type'] = 'normal'

				elif body['obstacle_type'] in ['rectangle', 'rrt']:
					body['width'] = self.obstacles_list[id_index][2]
					body['height'] = self.obstacles_list[id_index][3]
			else:
				body['obstacle'] = False

		for controllable_id in self.controllables_list:

			if controllable_id not in merged_bodies_ids_list:
				# print "Appending unseeable body with id " + str(controllable_id)

				body = dict()
				body['id'] = controllable_id
				body['x'] = -10000
				body['y'] = -10000
				body['z'] = -10000
				body['roll'] = -10000
				body['pitch'] = -10000
				body['yaw'] = -10000
				body['controllable'] = True
				merged_bodies_list.append(body)

		return merged_bodies_list

	# def vehicle_simulation_old(self):

	# 	self.vehicle_simulation_connected = True

	# 	simulator_class = simulatorclass.SimulatorClass()

	# 	##################################### Construction ############################################

	# 	self.construction_vehicles = []

	# 	current_vehicle = dict()
	# 	current_vehicle['x'] = -0.5
	# 	current_vehicle['y'] = 0.1
	# 	current_vehicle['yaw'] = 90.0
	# 	current_vehicle['id'] = self.current_simulated_id
	# 	self.current_simulated_id -= 1 
	# 	current_vehicle['body_type'] = 'construction'
	# 	self.construction_vehicles.append(current_vehicle)

	# 	current_vehicle = dict()
	# 	current_vehicle['x'] = 2.9
	# 	current_vehicle['y'] = 0.2
	# 	current_vehicle['yaw'] = 90.0
	# 	current_vehicle['id'] = self.current_simulated_id
	# 	self.current_simulated_id -= 1 
	# 	current_vehicle['body_type'] = 'construction'
	# 	self.construction_vehicles.append(current_vehicle)

	# 	self.bodies_list = self.synchronize_bodies_lists(self.construction_vehicles, self.bodies_list)

	# 	construction_simulator = constructionvehiclesimulator.ConstructionVehicleSimulator(self.construction_vehicles)

	# 	################################################################################################
		
	# 	tic = time.time()
	# 	toc = tic
	# 	prev_toc = toc

	# 	while not self.close:

	# 		tic = time.time()

	# 		time_to_simulate = toc - prev_toc

	# 		simulated_bodies = simulator_class.simulate_vehicles(self.bodies_list, self.vehicle_input_commands, time_to_simulate)

	# 		##################################### Construction ############################################


	# 		if self.msg_to_reroute_to_construction:
	# 			construction_simulator.process_construction_order(self.msg_to_reroute_to_construction['string'], log_flag=self.log_xml)
	# 			self.msg_to_reroute_to_construction = None

	# 		construction_bodies = construction_simulator.step()

	# 		message_to_cmd_central = construction_simulator.get_message_to_command_central()

	# 		if message_to_cmd_central:
	# 			print "There is a message to reroute to command central: " + message_to_cmd_central
	# 			self.msg_to_reroute_to_cmd_central = message_to_cmd_central

	# 		self.bodies_list = self.synchronize_bodies_lists(simulated_bodies + construction_bodies, self.bodies_list)

	# 		################################################################################################

	# 		# self.bodies_list = self.synchronize_bodies_lists(simulated_bodies, self.bodies_list)

	# 		self.bodies_list_xml = xmlcomms.get_vehicle_states_reply_message_string(self.bodies_list,log_flag=self.log_xml)

	# 		prev_toc = toc
	# 		toc = time.time()


	# 		self.waiting_for_desired_rate(tic,toc,'vehicle_simulation')

	# 	self.vehicle_simulation_connected = False
	# 	self.timed_print('Vehicle simulation stopped', 'WARNING', parent='VEHICLE')

	def vehicle_simulation(self):

		self.vehicle_simulation_connected = True

		simulator_class = simulatorclass.SimulatorClass(self.bodies_array)

		##################################### Construction ############################################

		time.sleep(0.5)

		self.construction_vehicles = []

		excavator_1 = bodyclasses.ConstructionVehicle()

		excavator_1.x = -0.5*32.
		excavator_1.y = 0.1*32.
		excavator_1.z = 0.*32.
		excavator_1.yaw = 90.0
		excavator_1.pitch = 0.
		excavator_1.roll = 0.
		excavator_1.commands = dict()
		excavator_1.commands['throttle'] = 0.
		excavator_1.commands['steering'] = 0.

		excavator_1.id = -21


		# self.bodies_array[excavator_1.id] = excavator_1


		excavator_2 = bodyclasses.ConstructionVehicle()

		excavator_2.x = 2.9*32.
		excavator_2.y = 0.2*32.
		excavator_2.z = 0.*32.
		excavator_2.yaw = 90.0
		excavator_2.pitch = 0.
		excavator_2.commands = dict()
		excavator_2.commands['throttle'] = 0.
		excavator_2.commands['steering'] = 0.
		excavator_2.roll = 0.

		excavator_2.id = -22

		# self.bodies_array[excavator_2.id] = excavator_2

		# 	current_vehicle = dict()
		# 	current_vehicle['x'] = -0.5
		# 	current_vehicle['y'] = 0.1
		# 	current_vehicle['yaw'] = 90.0
		# 	current_vehicle['id'] = self.current_simulated_id
		# 	self.current_simulated_id -= 1 
		# 	current_vehicle['body_type'] = 'construction'
		# 	self.construction_vehicles.append(current_vehicle)

		# 	current_vehicle = dict()
		# 	current_vehicle['x'] = 2.9
		# 	current_vehicle['y'] = 0.2
		# 	current_vehicle['yaw'] = 90.0
		# 	current_vehicle['id'] = self.current_simulated_id
		# 	self.current_simulated_id -= 1 
		# 	current_vehicle['body_type'] = 'construction'
		# 	self.construction_vehicles.append(current_vehicle)

		# self.bodies_list = self.synchronize_bodies_lists(self.construction_vehicles, self.bodies_list)

		# construction_simulator = constructionvehiclesimulator.ConstructionVehicleSimulator(self.construction_vehicles)

		################################################################################################



		
		tic = time.time()
		toc = tic
		prev_toc = toc

		while not self.close:

			tic = time.time()

			time_to_simulate = toc - prev_toc

			# simulated_bodies = simulator_class.simulate_vehicles(self.bodies_list, self.vehicle_input_commands, time_to_simulate)
			simulated_bodies = simulator_class.simulate_vehicles(time_to_simulate)

			simulator_class.simulate_body_readings(self.v2v_method, self.v2v_range, self.v2v_angle)
					
			# time.sleep(0.2)





			prev_toc = toc
			toc = time.time()


			self.waiting_for_desired_rate(tic,toc,'vehicle_simulation', 'vehicle_simulation')

		self.vehicle_simulation_connected = False
		self.timed_print('Vehicle simulation stopped', 'WARNING', parent='VEHICLE')

	def controlled_vehicles_handler(self, conn):

		real_vehicle_manage = realvehiclemanager.RealVehicleManager()

		conn.send(str(real_vehicle_manage.port) + '\n')

		self.timed_print('Creating Vehicles Server in Port:' + str(real_vehicle_manage.port), 'OKGREEN', parent='VEHICLE')

		controlled_vehicle_ids = []

		self.vehicle_manager_connected = True

		while not self.close:

			tic = time.time()

			result = real_vehicle_manage.step(self.log_xml)

			if result is not None:

				if isinstance(result, int):

					if result == -9:
						# Connection closed

						for body in self.bodies_list:

							if body['id'] in controlled_vehicle_ids:
								body['controllable'] = False

						self.controllables_list = []

						break

			if self.msg_to_reroute is not None:

				try:
					real_vehicle_manage.send_string_to_controlled_vehicles(self.msg_to_reroute['string'])
				except:
					print "controlled_vehicles_handler: NOT WELL HANDLED ERROR"
					self.controllables_list = []
					return

				print "controlled_vehicles_handler self.msg_to_reroute['type'] = " + str(self.msg_to_reroute['type'])
				print "controlled_vehicles_handler self.msg_to_reroute['string'] = " + str(
					self.msg_to_reroute['string'])

				if self.msg_to_reroute['type'] == 'collision':
					self.timed_print('Sent collision_warning ( vehicle ' + str(
						self.msg_to_reroute['trucks_in_collision']) + ') to vehicle manager', parent='VEHICLE')
				elif self.msg_to_reroute['type'] == 'vehicle_stop_command':
					self.timed_print('Sent vehicle_stop_command to vehicle manager', parent='VEHICLE')
				elif self.msg_to_reroute['type'] == 'reverse_trajectory_request':
					self.timed_print('Asked for a reverse trajectory to vehicle manager', parent='VEHICLE')
				else:
					self.timed_print('Sent trajectory to vehicle manager', parent='VEHICLE')
				self.msg_to_reroute = None

			vehicle_readings_reply_message_string = xmlcomms.get_controllable_vehicle_readings_reply_message_string(
				self.bodies_list, self.bodies_readings, log_flag=self.log_xml)

			vehicle_readings_reply_message_string += '\n'

			try:
				real_vehicle_manage.send_string_to_controlled_vehicles(vehicle_readings_reply_message_string)
			except:
				print "controlled_vehicles_handler: NOT WELL HANDLED ERROR 2"
				print "tried to send message: vehicle_readings_reply_message_string = " + str(vehicle_readings_reply_message_string)
				self.controllables_list = []
				break

			if result is not None:

				if isinstance(result, list):

					message_type = result[0]

					if message_type == "task_completed":

						# The body id of the truck that finished a task
						completed_task_body_id = result[1]
						# The task_completed message sent by the Controller
						message_string = result[2]

						self.msg_to_reroute_to_cmd_central = message_string

						self.timed_print("A Truck " + str(completed_task_body_id) + " has completed a task", parent='VEHICLE')

					elif message_type == "controlled_body_info":

						# Contains the ids of the bodies controlled by the Controller
						controllable_bodies_info = result[1]

						for body_id in controllable_bodies_info:

							self.controllables_list.append(body_id)

							for body in self.bodies_list:

								if body['id'] == body_id:
									controlled_vehicle_ids.append(body_id)

									body['controllable'] = True

						self.timed_print('Controllable vehicles: ' + str(self.controllables_list), parent='VEHICLE')

					elif message_type == "reverse_trajectory_solution":

						message_string = result[1]

						self.msg_to_reroute_to_cmd_central = message_string

						self.timed_print("Should reroute reverse trajectory solution to CMD Central", parent='VEHICLE')
						print "reverse trajectory message_string = " + str(message_string)

			toc = time.time()

			self.waiting_for_desired_rate(tic,toc,'vehicle_manager')

		self.vehicle_manager_connected = False

		self.timed_print('Controlled vehicles handler stopped', 'WARNING', parent='VEHICLE')

	# def mocap_handler_old(self):

	# 	self.mocap_on = True
	# 	fail_count = 0

	# 	self.start_mocap_socket()

	# 	while not self.close and self.Qs:

	# 		tic = time.time()

	# 		mocap_bodies_list = self.Qs.get_updated_bodies()

	# 		if mocap_bodies_list == 'off':
	# 			if fail_count > 50:
	# 				self.timed_print('Qualisys computer is down please turn it on', 'FAIL', parent='QUALISYS')
	# 				self.mocap_connected = False
	# 				self.Qs.socket.close()
	# 				time.sleep(15)
	# 				self.start_mocap_socket()
	# 				fail_count = 0
	# 				toc = time.time()
	# 				self.waiting_for_desired_rate(tic,toc,'mocap')
	# 				continue

	# 			else:
	# 				fail_count += 1
	# 		try:
	# 			try:

	# 				if self.simulator_body:
	# 					mocap_bodies_list.append(self.simulator_body)

	# 				mocap_bodies_list_ids = [body['id'] for body in mocap_bodies_list]
	# 				self.bodies_list = [body for body in self.bodies_list if
	# 									(body['id'] in mocap_bodies_list_ids or body['id'] < 0)]
	# 				self.bodies_list = self.synchronize_bodies_lists(mocap_bodies_list, self.bodies_list)

	# 				print "mocap_bodies_list = " + str(mocap_bodies_list)


	# 				# self.bodies_list_xml = xmlcomms.get_vehicle_states_reply_message_string(self.bodies_list,log_flag=self.log_xml)
	# 				# self.bodies_list_xml = xmlcomms.get_vehicle_states_reply_message_string(self.bodies_array,log_flag=self.log_xml)

	# 			except:
	# 				self.timed_print('error in mocap_handler()', 'WARNING', parent='QUALISYS')
	# 				# self.bodies_list_xml = xmlcomms.get_vehicle_states_reply_message_string(self.bodies_list,log_flag=self.log_xml)
	# 				# self.bodies_list_xml = xmlcomms.get_vehicle_states_reply_message_string(self.bodies_array,log_flag=self.log_xml)

	# 			if not self.mocap_on:
	# 				self.mocap_connected = True
	# 				self.timed_print('Qualisys computer is ON', 'OKGREEN', parent='QUALISYS')
	# 				self.mocap_on = True

	# 		except TypeError:
	# 			# Qs._stop_measurement()
	# 			if self.mocap_on:
	# 				self.mocap_on = False

	# 			self.bodies_list_xml = xmlcomms.get_vehicle_states_reply_message_string(self.bodies_list,log_flag=self.log_xml)

	# 		toc = time.time()
	# 		self.waiting_for_desired_rate(tic,toc,'mocap')

	# 	if self.Qs:
	# 		if self.Qs.socket:
	# 			self.Qs.socket.close()
	# 	self.mocap_connected = False
	# 	self.timed_print('Mocap handler stopped', 'WARNING', parent='QUALISYS')

	def mocap_handler(self):

		print "Mocap handler was started"

		self.mocap_on = True
		fail_count = 0

		self.start_mocap_socket()

		while not self.close and self.Qs:

			# print "Mocap Handler cycle"

			tic = time.time()

			# print "Mocap step"

			if not self.activate_states_from_mocap:

				body_ids_to_remove = []

				for body_id in self.bodies_array:

					if body_id > 0 and body_id < 30:

						body_ids_to_remove.append(body_id)
						
				# print "body_ids_to_remove = " + str(body_ids_to_remove) 

				for body_id_to_remove in body_ids_to_remove:

					# print "Removing body " + str(body_id_to_remove)
					del self.bodies_array[body_id_to_remove]

				time.sleep(0.2)
				continue

			mocap_bodies_list = self.Qs.get_updated_bodies()

			if mocap_bodies_list == 'off':
				if fail_count > 50:
					self.timed_print('Qualisys computer is down please turn it on', 'FAIL', parent='QUALISYS')
					self.mocap_connected = False
					self.Qs.socket.close()
					time.sleep(15)
					self.start_mocap_socket()
					fail_count = 0
					toc = time.time()
					self.waiting_for_desired_rate(tic,toc,'mocap')
					continue

				else:
					fail_count += 1
			try:
				try:

					# if self.simulator_body:
					# 	mocap_bodies_list.append(self.simulator_body)

					mocap_bodies_list_ids = [body['id'] for body in mocap_bodies_list]

					# print "Current number qualisys bodies = " + str( len(mocap_bodies_list_ids) )
					# self.bodies_list = [body for body in self.bodies_list if
					# 					(body['id'] in mocap_bodies_list_ids or body['id'] < 0)]
					# self.bodies_list = self.synchronize_bodies_lists(mocap_bodies_list, self.bodies_list)

					for idx, qualisys_id in enumerate(mocap_bodies_list_ids):

						if not qualisys_id in self.bodies_array:
							# The body is not yet on the bodies_array, I have to create it

							self.bodies_array[qualisys_id] = bodyclasses.CircularObstacle()
							self.bodies_array[qualisys_id].radius = 0.1
							self.bodies_array[qualisys_id].id = qualisys_id

						self.bodies_array[qualisys_id].x = mocap_bodies_list[idx]['x']*32.
						self.bodies_array[qualisys_id].y = mocap_bodies_list[idx]['y']*32.
						self.bodies_array[qualisys_id].z = mocap_bodies_list[idx]['z']*32.
						self.bodies_array[qualisys_id].yaw = mocap_bodies_list[idx]['yaw']
						self.bodies_array[qualisys_id].pitch = mocap_bodies_list[idx]['pitch']
						self.bodies_array[qualisys_id].roll = mocap_bodies_list[idx]['roll']

#					if 8 in self.bodies_array:

#						print "Iris 2 is being detected"
#						print "Iris2.x = " + str( self.bodies_array[8].x )


					# self.bodies_array



		# for current_body in current_bodies_list:
			# for body in bodies_list:
			# 	if current_body['id'] == body['id']:
			# 		if 'controllable' in body:
			# 			current_body['controllable'] = body['controllable']
			# 		else:
			# 			current_body['controllable'] = False


				except:
					self.timed_print('error in mocap_handler()', 'WARNING', parent='QUALISYS')
					raise
				if not self.mocap_on:
					self.mocap_connected = True
					self.timed_print('Qualisys computer is ON', 'OKGREEN', parent='QUALISYS')
					self.mocap_on = True

			except TypeError:
				# Qs._stop_measurement()
				if self.mocap_on:
					self.mocap_on = False


			toc = time.time()
			self.waiting_for_desired_rate(tic,toc,'mocap')

		if self.Qs:
			if self.Qs.socket:
				self.Qs.socket.close()
		self.mocap_connected = False
		self.timed_print('Mocap handler stopped', 'WARNING', parent='QUALISYS')

	def highlight_handler(self, highlight_client):

		previous_highlight_mode = False
		previous_highlighted_vehicle_id = 0

		self.highlight_connected = True

		self.gui_changed = True

		time.sleep(1)

		while not self.close:
			tic = time.time()

			if self.highlight_mode != previous_highlight_mode or self.highlighted_vehicle_id != previous_highlighted_vehicle_id:
				change_in_highlight = True
				previous_highlight_mode = self.highlight_mode
				previous_highlighted_vehicle_id = self.highlighted_vehicle_id
			else:
				change_in_highlight = False

			if change_in_highlight:
				try:
					highlight_client.send('VEHICLE_HIGHLIGHT\n')
				except:
					self.timed_print('Highlight request to the projector failed (Projector is off)', 'FAIL', parent='WORLD')
					break
				if highlight_client.recv(self.buffersize) == 'ID':
					if self.highlight_mode:
						highlight_client.send(str(self.highlighted_vehicle_id))
						self.timed_print('Sent request for selected vehicle: ' + str(self.highlighted_vehicle_id), parent='PROJECTOR')
					else:
						highlight_client.send('0')
						self.timed_print('Sent request for unselect vehicles:', parent='PROJECTOR')

			if self.msg_task_to_projector is not None:
				pass
			# try:
			# highlight_client.send('MESSAGE')
			# except:
			# timed_print('Highlight request to the projector failed (Projector is off)' ,'FAIL',parent = 'WORLD')
			# 	break
			# if highlight_client.recv(self.buffersize) == 'OK':
			# 	highlight_client.send(self.msg_task_to_projector )
			# 	self.msg_task_to_projector  = None

			if self.gui_changed:
				try:
					highlight_client.send('UPDATE_SETTINGS\n')
				except:
					self.timed_print('Highlight request to the projector failed (Projector is off)', 'FAIL', parent='WORLD')
					self.gui_changed = False
					break

				try:

					if highlight_client.recv(self.buffersize) == 'OK':

						list_of_settings = ['activate_draw_origin', 'draw_sensors', 'draw_signal', 'show_ids',
											'activate_fps', 'activate_states_from_mocap', 'activate_states_from_world',
											'only_simulated_vehicles', 'only_real_vehicles', 'projector_area',
											'v2v_method', 'v2v_range', 'v2v_angle','projector_resolution']

						highlight_client.send(str(len(list_of_settings)))

						if highlight_client.recv(self.buffersize) == 'OK':
							for setting in list_of_settings:
								highlight_client.send(setting)
								if highlight_client.recv(self.buffersize) == 'OK':
									atrr = getattr(self, setting)
									if atrr != '':
										highlight_client.send(str(atrr))
									else:
										highlight_client.send('0')
								time.sleep(0.05)

				except:
					self.gui_changed = False
					break

				self.gui_changed = False

			if self.rrt_reply_string:

				self.timed_print('Sending RRT information to projector', parent='WORLD')

				try:
					highlight_client.send('RRT\n')
				except:
					self.timed_print('Highlight request to the projector failed (Projector is off)', 'FAIL', parent='WORLD')
					self.rrt_reply_string = None
					break

				if highlight_client.recv(self.buffersize) == 'OK':
					highlight_client.send(self.rrt_reply_string)

				self.rrt_reply_string = None

			if self.msg_to_reroute_to_projector:

				self.timed_print('Sending Lanelet obstruction information to projector', parent='WORLD')
				try:
					highlight_client.send('LANELET_OBSTRUCTION\n')
				except:
					self.timed_print('Highlight request to the projector failed (Projector is off)', 'FAIL', parent='WORLD')
					self.msg_to_reroute_to_projector = None
					break

				if highlight_client.recv(self.buffersize) == 'OK':
					highlight_client.send(self.msg_to_reroute_to_projector)

				self.msg_to_reroute_to_projector = None

			toc = time.time()

			self.waiting_for_desired_rate(tic,toc,'highlight')

			# time.sleep(0.3)

		self.highlight_mode = False

		self.highlight_connected = False

		self.timed_print('Highlight handler stopped', 'WARNING', parent='PROJECTOR')

	def projector_manager_handler(self, conn, lanelet_module, client_mode):

		self.timed_print('Waiting for projector requests', parent='PROJECTOR')

		msg = 'no wait'

		self.projector_manager_connected = True

		if client_mode.strip('\n').strip('\r').lower() == 'projector-main' and self.projector_wait_for_command_central:
			msg = 'wait for command central'
			conn.send(msg)
			while not self.close and not self.command_central_connected:
				time.sleep(0.1)
			conn.send('command _central on')
		else:
			conn.send(msg)

		while not self.close:
			tic = time.time()
			try:
				request = conn.recv(self.buffersize).strip('\n').strip('\r').lower()

			except:
				conn.close()
				break
			if request == 'background':

				self.timed_print('Request for background image from projector', parent='PROJECTOR')

				image = open(os.path.join('resources', 'world_surface.bmp'), 'rb')
				image_data = image.read()
				image.close()
				# image_server.send(image_data)
				conn.send(image_data)

			elif request == 'image_properties':

				self.timed_print('Request for image properties from projector', parent='PROJECTOR')

				data = 'center_pixel_x ' + str(lanelet_module.center_pixel_x) + ' center_pixel_y ' + str(
					lanelet_module.center_pixel_y) + ' pixel_per_meter ' + str(lanelet_module.pixel_per_meter)

				conn.send(data)

			elif request == 'states':
				if self.bodies_list_xml is not None:

					# print "------------START-----------"
					# print "self.bodies_list_xml = " + str(self.bodies_list_xml) 

					states_response_success = False

					while not states_response_success:

						try:

							#bodies_array_copy = copy.deepcopy(self.bodies_array)

							self.bodies_list_xml = xmlcomms.get_vehicle_states_reply_message_string(self.bodies_array,log_flag=self.log_xml)
							
							# print "\n\nself.bodies_list_xml = " + str(self.bodies_list_xml)

							conn.send(self.bodies_list_xml + '\n')

							states_response_success = True

						except Exception, exception_caught:

							print "Caught an error in projector_manager_handler states request, might have happened due to dictionary changing size during read loop, will try again."
							print "exception_caught = " + str(exception_caught)

							time.sleep(0.05)
							# raise
					# print "self.bodies_list_xml = " + str(self.bodies_list_xml) 
					# print "------------END-----------"

			elif request == 'traffic_lights':

				self.timed_print('Request for traffic lights from projector', parent='PROJECTOR')

			elif request == 'highlight':

				self.timed_print('Request for highlights from projector', parent='PROJECTOR')

				conn.send('PORT')

				port = int(conn.recv(self.buffersize))
				host = conn.getsockname()[0]
				addr = (host, port)

				highlight_client = socket(AF_INET, SOCK_STREAM)
				try:
					highlight_client.connect(addr)
					self.timed_print('Projector listening to highlight requests from SML world in port:' + str(port), 'OKGREEN', parent='PROJECTOR')
				except:
					self.timed_print('Failed to connect to Projector Manager ' + host + ' in port:' + str(port), 'FAIL', parent='PROJECTOR')
					self.projector_manager_connected = False
					return

				self.start_thread(self.highlight_handler, args=([highlight_client]))
				return

			elif request == 'close':

				self.timed_print('Request for close from projector', parent='PROJECTOR')
				conn.close()
				self.timed_print('Projector Manager disconnected', 'WARNING', parent='PROJECTOR')
				break

			toc = time.time()

			self.waiting_for_desired_rate(tic,toc,'projector_manager')

		self.projector_manager_connected = False

		if client_mode == 'projector-states':
			self.timed_print('Projector states manager handler stopped', 'WARNING', parent='PROJECTOR')
		elif client_mode == 'projector-main':
			self.timed_print('Projector manager handler stopped', 'WARNING', parent='PROJECTOR')

	def command_central_handler(self, conn, lanelet_module):

		self.timed_print('Waiting for trajectory request from command central', parent='CMD')

		trajectory_info = lanelet_module.get_road_info_message_string(self.log_xml)
		conn.send(trajectory_info)
		self.timed_print('Trajectory info sent to command central', parent='CMD')

		self.cmd_data = ''

		while not self.close:

			tic = time.time()

			request_string = ''

			while True:

				self.bodies_list = []

				for body_id_key in self.bodies_array:

					#body['id'] == truck_id:
					#initial_pose = [body['x'], body['y'], math.radians(body['yaw']), math.radians(body['yaw'])]

					body = dict()
					body['id'] = body_id_key
					body['x'] = self.bodies_array[body_id_key].x/32.
					body['y'] = self.bodies_array[body_id_key].y/32.
					body['yaw'] = self.bodies_array[body_id_key].yaw

					if body_id_key > 0:

						body['controllable'] = True
						body['obstacle'] = False

						body['trailer'] = False
						body['trailer_id'] = 0

						body['signal_strength'] = 1.0

					if body_id_key == 5:

						body['obstacle'] = True
						body['obstacle_type'] = 'rrt'
						body['width'] = 0.4
						body['height'] = 0.3

					# obstacles_list = [[[body['x'], body['y'], body['yaw']], [body['width'], body['height'], 0]] for body in self.bodies_list if 'obstacle' in body and body['obstacle'] and body['obstacle_type'] == 'rrt']


					self.bodies_list.append(body)

					#body_info.set( 'controllable' , 'False' )
					#body_info.set( 'obstacle' , 'False' )

					#body_info.set( 'trailer' , 'False' )
					#body_info.set( 'trailer_id' , '0' )


					#body_info.set( 'pitch' , '0.0' )
					#body_info.set( 'roll' , '0.0' )
					#body_info.set( 'z' , '0.0' )

					#body_info.set( 'signal_strength', '-1.0')





				if self.cmd_data.find('\n') == -1:

					try:
						self.cmd_data = self.cmd_data + conn.recv(self.buffersize)
					# print "self.cmd_data: " + str(self.cmd_data)

					except socketerror as e:
						if e.errno == errno.ECONNRESET:
							self.timed_print('Command Central disconnected', 'WARNING', parent='CMD')
							self.command_central_connected = False
							conn.close()
							return

				if self.cmd_data.find('\n') != -1:

					data = self.cmd_data.split('\n')
					while '' in data:
						data.remove('')

					message_types = [xmlcomms.get_message_type(message) for message in data]

					vehicle_states_type = [i for i, x in enumerate(message_types) if x == 'vehicle_states_request']

					if vehicle_states_type:
						others_type = [i for i, x in enumerate(message_types) if x != 'vehicle_states_request']

						requests_to_save = [vehicle_states_type[0]] + others_type if isinstance(others_type, list) else [others_type]

						data = [data[i] for i in requests_to_save]

					if len(data) == 1:
						self.cmd_data = ''
					else:
						self.cmd_data = '\n'.join(data[1:])

					request_string = data[0].strip('\n').strip('\r').lower()

					break

			if request_string == 'close':
				conn.close()
				self.highlight_mode = False
				self.timed_print('Command Central disconnected', 'WARNING', parent='CMD')
				self.command_central_connected = False
				return

			if self.msg_to_reroute_to_cmd_central is not None:
				conn.send(self.msg_to_reroute_to_cmd_central)
				self.msg_task_to_projector = self.msg_to_reroute_to_cmd_central
				self.msg_to_reroute_to_cmd_central = None
				continue

			command_central_reply = lanelet_module.process_and_reply(request_string, self.bodies_list)

			if command_central_reply is None:

				self.timed_print('Invalid message received, do nothing', 'FAIL', parent='CMD')

				print "invalid request_string = " + str(request_string)

			elif isinstance(command_central_reply, list):

				request_type = command_central_reply[0]

				if request_type == 'body_trajectory_request':

					self.timed_print('Received body trajectory request from command central', parent='CMD')

					body_id = command_central_reply[1][0]
					destination_node_id = command_central_reply[1][1]

					points_per_meter = 5.0
					truck_vehicle = True
					[traj_x, traj_y, lanelet_path] = laneletlibrary.get_body_trajectory_to_node_id(body_id, destination_node_id, self.bodies_list, lanelet_module.osm_info, points_per_meter, truck_vehicle, self.obstructed_lanelets)

					root = et.Element('message')
					root.set("type", "trajectory_reply")
					root.set("id", "5")

					for i in range(len(traj_x)):
						current_node = et.SubElement(root, 'point')
						current_node.set("x", str(traj_x[i]))
						current_node.set("y", str(traj_y[i]))
						current_node.set("time", str(i))

					for idx, lanelet_id in enumerate(lanelet_path):
						current_node = et.SubElement(root, 'lanelet')
						current_node.set("id", str(lanelet_id))
						current_node.set("time", str(idx))

					reply_string = et.tostring(root) + '\n'

					if len(traj_x) == 0:

						self.timed_print('ERROR: Will send and empty trajectory', parent='CMD')

					else:

						self.timed_print('Start position in trajectory (' + str(traj_x[0]) + ',' + str(traj_y[0]) + ')', parent='CMD')
						self.timed_print('Ending point in trajectory (' + str(traj_x[-1]) + ',' + str(traj_y[-1]) + ')', parent='CMD')

					self.timed_print('Sent trajectory for truck id ' + str(body_id) + ' to destination id ' + str(destination_node_id), parent='CMD')

					conn.send(reply_string)

				elif request_type == 'rrt_trajectory_request':

					self.timed_print('Received request for RRT trajectory', parent='CMD')

					request_string = command_central_reply[1]

					[initial_pose, goal_position, truck_id] = xmlcomms.process_rrt_trajectory_request_xml_string(
						request_string, log_flag=self.log_xml)

					for body in self.bodies_list:

						if body['id'] == truck_id:
							initial_pose = [body['x'], body['y'], math.radians(body['yaw']), math.radians(body['yaw'])]

					space_limits = [-1., 1., -1.4, 0.6]

					test_debug = [body for body in self.bodies_list if 'obstacle' in body and body['obstacle']]

					print test_debug

					obstacles_list = [[[body['x'], body['y'], body['yaw']], [body['width'], body['height'], 0]] for body in self.bodies_list if 'obstacle' in body and body['obstacle'] and body['obstacle_type'] == 'rrt']

					obstacle_list_corrected = []

					# for obstacle in obstacles_list:
					# 	# noinspection PyTypeChecker
					# 	obstacle[0][0] = obstacle[0][0] + (obstacle[1][0] / 2.0) * math.cos(math.radians(obstacle[0][2])) - (-obstacle[1][1] / 2.0) * math.sin(math.radians(obstacle[0][2]))
					# 	# noinspection PyTypeChecker
					# 	obstacle[0][1] = obstacle[0][1] + (obstacle[1][0] / 2.0) * math.sin(math.radians(obstacle[0][2])) + (-obstacle[1][1] / 2.0) * math.cos(math.radians(obstacle[0][2]))
					# 	obstacle[0][2] = obstacle[0][2]
					# 	obstacle_list_corrected.append(obstacle)

					self.timed_print("Will run RRT with space limits corresponding to the OSM way of the RRT space", parent='CMD')
					environment_points_list = lanelet_module.get_rrt_environment_points_list()

					# Need a fake extra layer for now.
					environment_points_list = [environment_points_list]

					rrtlibrary.write_boost_obstacles_and_environment_to_file(obstacles_list, environment_points_list, "cppInputObstacles.txt")

					[tree_nodes, solution_nodes] = rrtlibrary.call_rrt(initial_pose, goal_position, space_limits, goal_tolerance=.02, euler_sim_time=.1, max_steering_angle=.35)

					reply_string = xmlcomms.get_rrt_solution_message(tree_nodes, solution_nodes, truck_id, log_flag=self.log_xml)

					reply_string += '\n'

					self.rrt_reply_string = reply_string

					self.timed_print('Sent rrt trajectory to command central', parent='CMD')

					conn.send(reply_string)

					time.sleep(0.2)

				elif request_type == 'vehicle_stop_command':

					self.timed_print('Received vehicle stop command', parent='CMD')

					self.msg_to_reroute = dict()
					self.msg_to_reroute['type'] = 'vehicle_stop_command'
					self.msg_to_reroute['string'] = command_central_reply[1] + '\n'

				elif request_type == 'body_trajectory_command':

					self.timed_print('Received body trajectory command', parent='CMD')

					self.msg_to_reroute = dict()
					self.msg_to_reroute['type'] = 'trajectory'
					self.msg_to_reroute['string'] = command_central_reply[1] + '\n'

					self.msg_to_reroute_to_simulator = dict()
					self.msg_to_reroute_to_simulator['type'] = 'trajectory'
					self.msg_to_reroute_to_simulator['string'] = command_central_reply[1] + '\n'

				elif request_type == 'reverse_trajectory_request':

					self.timed_print('Received a reverse trajectory request', parent='CMD')
					self.msg_to_reroute = dict()
					self.msg_to_reroute['type'] = 'reverse_trajectory_request'
					self.msg_to_reroute['string'] = command_central_reply[1] + '\n'

					print "command central handler: self.msg_to_reroute['type'] = " + str(self.msg_to_reroute['type'])
					print "command central handler: self.msg_to_reroute['string'] = " + str(
						self.msg_to_reroute['string'])

				elif request_type == 'reverse_trajectory_command':

					self.timed_print('Received a reverse trajectory command', parent='CMD')

					self.msg_to_reroute = dict()
					self.msg_to_reroute['type'] = 'reverse_trajectory_confirm'
					self.msg_to_reroute['string'] = command_central_reply[1] + '\n'

				elif request_type == 'construction_command':

					self.timed_print('Received construction caterpillar command', parent='CMD')

					self.msg_to_reroute_to_construction = dict()
					self.msg_to_reroute_to_construction['type'] = 'construction_command'
					self.msg_to_reroute_to_construction['string'] = command_central_reply[1] + '\n'

			elif isinstance(command_central_reply, str):

				conn.send(command_central_reply)

			elif isinstance(command_central_reply, dict):

				if command_central_reply['reply_type'] == 'highlight_reply':
					self.highlight_mode = command_central_reply['highlight']
					self.highlighted_vehicle_id = command_central_reply['vehicle_id']
					if self.highlight_mode == 0:
						self.timed_print('Highlight mode is now deactivated', parent='CMD')
					else:
						self.timed_print('Truck ' + str(self.highlighted_vehicle_id) + ' is highlighted', parent='CMD')
				elif command_central_reply['reply_type'] == 'obstacle_command':

					print "request_string = " + str(request_string)

					if int(command_central_reply['id']) not in [ele[0] for ele in self.obstacles_list]:

						print "Added obstacle sent from command central"

						print command_central_reply

						self.obstacles_list.append([command_central_reply['id'], command_central_reply['type'], command_central_reply['radius'], command_central_reply['obstacle_type']])
					else:

						print "Replaced obstacle sent from command central"
						for obstacle in self.obstacles_list:
							if obstacle[0] == command_central_reply['id']:
								obstacle = [command_central_reply['id'], command_central_reply['type'], command_central_reply['radius'], command_central_reply['obstacle_type']]


					new_body = dict()
					new_body['x'] = command_central_reply['x']
					new_body['y'] = command_central_reply['y']
					new_body['yaw'] = 0
					new_body['id'] = command_central_reply['id']
					new_body['obstacle'] = True
					new_body['obstacle_type'] = command_central_reply['obstacle_type']
					new_body['radius'] = command_central_reply['radius']

					print new_body

					print [ele['id'] for ele in self.bodies_list]
					self.synchronize_bodies_lists([new_body], self.bodies_list)
					print [ele['id'] for ele in self.bodies_list]

			elif command_central_reply:
				pass
			# print "Special request handled internally"

			else:
				self.timed_print("Invalid message, do not even know how to handle", 'FAIL', parent='CMD')
				self.timed_print("Message: " + request_string, 'FAIL', parent='CMD')

			toc = time.time()

			self.waiting_for_desired_rate(tic,toc,'command_central')

		self.timed_print('Command central handler stopped', 'WARNING', parent='CMD')
		self.command_central_connected = False

	def people_manager_handler(self):

		people_manager = peoplemanager.peopleManager(self.bodies_array)

		desired_rate = 1.

		while not self.close:

			tic = time.time()

			people_manager.step()

			toc = time.time()

			if toc - tic > 0:
				rate = int(round(1. / (toc - tic)))
				if rate > desired_rate:
					time.sleep(1. / desired_rate - 1. / rate)
				else:
					print "Something is not sleeping, and it is people manager handler"
			elif toc - tic == 0:
				time.sleep(1. / desired_rate)



	def v2i_handler(self, lanelet_module):

		v2imanager = v2imodule.v2iManager(lanelet_module.osm_info)

		prev_obstructed_lanelets = []

		self.v2i_info['rate'] = str(0)

		self.v2i_connected = True

		while not self.close:

			tic = time.time()

			lanelet_collision_ids = v2imanager.step(self.bodies_list)

			self.obstructed_lanelets = lanelet_collision_ids

			if set(prev_obstructed_lanelets) != set(lanelet_collision_ids):

				if self.command_central_connected:
					self.msg_to_reroute_to_cmd_central = xmlcomms.get_lanelet_obstruction_message_string(lanelet_collision_ids, log_flag=self.log_xml) + '\n'

				new_polygons_lanelets = v2imanager.get_new_polygon_lanelets(prev_obstructed_lanelets, lanelet_collision_ids)
				self.msg_to_reroute_to_projector = xmlcomms.get_lanelet_polygon_obstruction_message_string(new_polygons_lanelets, log_flag=self.log_xml) + '\n'

				if self.projector_manager_connected:
					new_polygons_lanelets = v2imanager.get_new_polygon_lanelets(prev_obstructed_lanelets, lanelet_collision_ids)

					self.msg_to_reroute_to_projector = xmlcomms.get_lanelet_polygon_obstruction_message_string(new_polygons_lanelets, log_flag=self.log_xml) + '\n'

			prev_obstructed_lanelets = lanelet_collision_ids

			toc = time.time()

			self.waiting_for_desired_rate(tic,toc,'v2i')

		self.v2i_connected = False
		self.timed_print('V2I handler stopped', 'WARNING', parent='VEHICLE')

	def v2v_handler(self):
		# V2V handler is the thread that will simulate communications (WiFi and Network)

		v2vmanager = v2vmodule.v2vManager(self.bodies_array)

		self.v2v_info['rate'] = str(0)

		self.v2v_connected = True

		v2v_network_rate = .5

		last_network_time = time.time()

		while not self.close:

			tic = time.time()

			if tic - last_network_time > 1./v2v_network_rate:

				v2vmanager.network_step()
				last_network_time = time.time()

			v2vmanager.wifi_step(self.v2v_range)

			toc = time.time()

			self.waiting_for_desired_rate(tic,toc,'v2v')

		self.v2v_connected = False
		self.timed_print('V2V handler stopped', 'WARNING', parent='VEHICLE')

	def dummy_traffic_caller(self):

		while not self.close:

			tic = time.time()

			self.commands_manager.dummy_traffic_step(self.simulated_vehicles_desired_rate)

			self.commands_manager.platooning_vehicles_step(self.simulated_vehicles_desired_rate)

			toc = time.time()

			self.waiting_for_desired_rate(tic,toc,'simulated_vehicles')

		self.commands_manager.dummy_traffic_step(self.simulated_vehicles_desired_rate)

		self.timed_print('Dummy traffic stopped', 'WARNING', parent='VEHICLE')

	def vehicle_simulator_handler(self, lanelet_module):

		self.timed_print('Starting simulation setup for ' + str(self.number_vehicles) + ' vehicles', 'OKBLUE', parent='WORLD')
		tic = time.time()
		
		vehicle_simulator = vehiclesimulator.VehicleSimulator(lanelet_module.osm_info, self.simulated_vehicles_desired_rate)

		for vehicle_id in xrange(self.number_vehicles):
			vehicle_simulator.create_random_vehicles(self.simulated_vehicles_desired_rate, self.current_simulated_id)
			self.current_simulated_id -=1
			time.sleep(0.05)

		vehicle_simulator.start_simulating_vehicles()

		toc = time.time() - tic

		self.timed_print('Simulation setup ready ( creation time: ' + str(toc) + ' s )', 'OKGREEN', parent='WORLD')

		self.vehicle_simulator_ready = True

		self.simulated_vehicles_connected = True

		construction_vehicles = []

		current_vehicle = dict()
		current_vehicle['x'] = -0.5
		current_vehicle['y'] = 0.1
		current_vehicle['yaw'] = 90.0
		current_vehicle['id'] = self.current_simulated_id
		self.current_simulated_id -= 1 
		current_vehicle['body_type'] = 'construction'
		construction_vehicles.append(current_vehicle)

		current_vehicle = dict()
		current_vehicle['x'] = 2.9
		current_vehicle['y'] = 0.2
		current_vehicle['yaw'] = 90.0
		current_vehicle['id'] = self.current_simulated_id
		self.current_simulated_id -= 1 
		current_vehicle['body_type'] = 'construction'
		construction_vehicles.append(current_vehicle)

		construction_simulator = constructionvehiclesimulator.ConstructionVehicleSimulator(construction_vehicles)

		while not self.close:

			tic = time.time()

			simulated_bodies = vehicle_simulator.step(self.bodies_readings,self.simulated_vehicles_desired_rate,simulator_is_closing = self.close)

			if self.msg_to_reroute_to_construction:
				construction_simulator.process_construction_order(self.msg_to_reroute_to_construction['string'], log_flag=self.log_xml)
				self.msg_to_reroute_to_construction = None

			construction_bodies = construction_simulator.step()

			self.bodies_list = self.synchronize_bodies_lists(construction_bodies + simulated_bodies, self.bodies_list)

			# print "self.bodies_list = " + str(self.bodies_list)

			message_to_cmd_central = construction_simulator.get_message_to_command_central()

			if message_to_cmd_central:
				print "There is a message to reroute to command central: " + message_to_cmd_central
				self.msg_to_reroute_to_cmd_central = message_to_cmd_central

			toc = time.time()

			self.waiting_for_desired_rate(tic,toc,'simulated_vehicles')

		simulated_bodies = vehicle_simulator.step(self.bodies_readings, self.simulated_vehicles_desired_rate, simulator_is_closing = True)

		self.simulated_vehicles_connected = False

		self.timed_print('Vehicle simulator handler stopped', 'WARNING', parent='VEHICLE')

	
	def vehicle_command_manager_initializer(self, lanelet_module):

		self.commands_manager = vehiclecommandmanager.VehicleCommandManager(self.bodies_array, self.vehicle_controller_desired_rate, self.buffersize,lanelet_module, self.simulated_vehicles_desired_rate)

		self.vehicle_command_manager_info['rate'] = str(0)

		self.vehicle_command_manager_connected = True

		# This will create the commands manager submodule for dummy traffic vehicles

		# We are going to create the dummy vehicles
		for vehicle_id in xrange(self.number_vehicles):

			tic = time.time()

			# self.commands_manager.setup_dummy_traffic_vehicle(self.current_simulated_id)
			self.commands_manager.dummy_traffic_manager.create_random_vehicle(self.commands_manager.vehicles_desired_rate, self.current_simulated_id)
			self.current_simulated_id -= 1

			toc = time.time()
			self.waiting_for_desired_rate(tic,toc,'vehicle_command_manager')


		'''ADDITION'''
		# Creating broken vehicle
		self.commands_manager.dummy_traffic_manager.create_broken_vehicles_thread(self.commands_manager.vehicles_desired_rate, -100)
		'''END'''


		# We are going to create the bus vehicle
		number_bus_vehicles = 0
		for vehicle_id in xrange(number_bus_vehicles):

			tic = time.time()

			# self.commands_manager.setup_dummy_traffic_vehicle(self.current_simulated_id)
			self.commands_manager.dummy_traffic_manager.create_bus_vehicle(self.commands_manager.vehicles_desired_rate, self.current_simulated_id)
			self.current_simulated_id -= 1

			toc = time.time()
			self.waiting_for_desired_rate(tic,toc,'vehicle_command_manager')

		# In case we are studying the platooning scenario we set it to bigger than zero. Otherwise zero.
        # Sets the number of platooning vehicles
		number_platooning_vehicles = 0

		# Creates the smart vehicles
		for vehicle_id in xrange(number_platooning_vehicles):

			tic = time.time()

			self.commands_manager.setup_platooning_vehicle(self.current_simulated_id)
			self.current_simulated_id -= 1

			toc = time.time()
			self.waiting_for_desired_rate(tic,toc,'vehicle_command_manager')


		# In case we are studying the intersection scenario we set it to True. Otherwise False.
		simulate_intersection = False

		if simulate_intersection:

			self.commands_manager.intersection_vehicles_manager.setup_intersection_scenario(self.current_simulated_id)

		# # Sets the number of intersection vehicles
		# number_intersection_vehicles = 10

		# # Creates the smart vehicles
		# for vehicle_id in xrange(number_intersection_vehicles):

		# 	tic = time.time()

		# 	self.commands_manager.setup_intersection_vehicle(self.current_simulated_id)
		# 	self.current_simulated_id -= 1

		# 	toc = time.time()
		# 	self.waiting_for_desired_rate(tic,toc,'vehicle_command_manager')

		self.vehicle_simulator_ready = True
		self.simulated_vehicles_connected = True

		self.commands_manager.start_dummy_traffic_vehicle()
		# self.commands_manager.start_platooning_vehicles()
		

		self.platoonCommandThread = platoonCommand.platoonCommand(self.platoonCommandPort, self.bodies_array, number_platooning_vehicles)
		self.platoonCommandThread.start()


	def vehicle_command_manager_handler(self, lanelet_module):

		t = threading.Thread(target=self.dummy_traffic_caller)
		t.daemon = True
		t.start()

		while not self.close:

			tic = time.time()

			# print "I'm dumb and I know it"

			# commands_manager_output = self.commands_manager.step(self.bodies_list, self.bodies_readings, self.vehicle_controller_desired_rate, vehicle_output_rate=self.vehicle_output_manager_desired_rate ,simulator_is_closing = self.close)
			# commands_manager_output = self.commands_manager.step(self.vehicle_controller_desired_rate, vehicle_output_rate=self.vehicle_output_manager_desired_rate ,simulator_is_closing = self.close)
			# This step function became obsolete

			# if isinstance(commands_manager_output, list):
			# 	# The output is both vehicle input commands and initial_states
			# 	self.vehicle_input_commands = commands_manager_output[0]
			# 	new_initial_states = commands_manager_output[1]

			# 	new_vehicles = []

			# 	for body_id in new_initial_states.keys():

			# 		current_vehicle = dict()
			# 		current_vehicle['x'] = new_initial_states[body_id]['x']
			# 		current_vehicle['y'] = new_initial_states[body_id]['y']
			# 		current_vehicle['yaw'] = new_initial_states[body_id]['yaw']
			# 		current_vehicle['id'] = body_id
			# 		current_vehicle['body_type'] = 'vehicle'
			# 		new_vehicles.append(current_vehicle)

			# 	self.bodies_list = self.synchronize_bodies_lists(new_vehicles, self.bodies_list)

			# else:
			# 	# The output is simply the vehicle input commands
			# 	self.vehicle_input_commands = commands_manager_output


			toc = time.time()

			self.waiting_for_desired_rate(tic,toc,'vehicle_command_manager')

		# self.commands_manager.step(self.bodies_list, self.bodies_readings, self.vehicle_controller_desired_rate, vehicle_output_rate=self.vehicle_output_manager_desired_rate, simulator_is_closing = self.close)

		self.vehicle_command_manager_connected = False
		self.simulated_vehicles_connected = False
		self.timed_print('Vehicle command manager handler stopped', 'WARNING', parent='VEHICLE')

	def create_connections_server(self):

		host = ''
		addr = (host, self.server_port)

		serv = socket(AF_INET, SOCK_STREAM)

		serv.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)

		try:
			serv.bind(addr)
			serv.listen(5)
			serv.settimeout(0.2)

		except:

			self.timed_print('SML World already running in ' + host + 'in port ' + str(self.server_port), 'FAIL', parent='WORLD')
			self.close = True
			sys.exit(1)

		return serv

	def start_connections_server(self, serv, lanelet_module):

		f = open("file_sources/passwordForCommandCentral.txt", 'r')

		passwordstring = f.read()

		# lanelet_module.set_surface_properties(canvas_width = 1920, canvas_height = 1080, viewing_gap = 0, pixel_per_meter = 720/6)
		lanelet_module.set_surface_properties(canvas_width=1920, canvas_height=1080, viewing_gap=0, pixel_per_meter=1020 / 6)

		if not self.load_saved_background:
			background_roads = lanelet_module.get_world_surface()
			pygame.image.save(background_roads, os.path.join('resources', 'world_surface.bmp'))

		self.vehicle_command_manager_info['number_real_vehicles'] = 0
		self.vehicle_command_manager_info['number_simulated_vehicles'] = 0

		while not self.close:
			try:

				try:
					conn, addr = serv.accept()
					conn.setblocking(1)
				except timeout:
					continue
				self.timed_print('IP: ' + str(addr[0]) + ' Port: ' + str(addr[1]) + ' ...connected!', 'OKBLUE', parent='WORLD')

				conn.send('OK\n')

				data = conn.recv(self.buffersize).strip('\n').strip('\r').lower()

				if data in ['projector-main', 'projector-states', 'projector-highlights']:

					if data == 'projector-main':
						self.timed_print('Projector Manager connected', 'OKGREEN', parent='PROJECTOR')
						self.projector_manager_info['ip'] = addr[0]
						self.projector_manager_info['port'] = str(addr[1])
						self.projector_manager_info['rate'] = str(0)
					elif data == 'projector-states':
						self.timed_print('Projector receiving states from SML world', 'OKGREEN', parent='PROJECTOR')

					elif data == 'projector-highlights':
						self.highlight_info['ip'] = addr[0]
						self.highlight_info['port'] = str(addr[1])
						self.highlight_info['rate'] = str(0)

					self.start_thread(self.projector_manager_handler, args=(conn, lanelet_module, data))

				elif 'command' in data:

					if data == 'command' + passwordstring:
						self.timed_print('Command central connected', 'OKGREEN', parent='CMD')
						self.command_central_info['ip'] = addr[0]
						self.command_central_info['port'] = str(addr[1])
						self.command_central_info['rate'] = str(0)
						self.start_thread(self.command_central_handler, args=(conn, lanelet_module))
						self.command_central_connected = True

					else:
						self.timed_print("SECURITY THREAT, CLOSING INTRUDER SOCKET", 'FAIL', parent='CMD')
						conn.close()

				elif data == 'vehicle':
					self.timed_print('Vehicle connected')
					self.vehicle_manager_info['ip'] = addr[0]
					self.vehicle_manager_info['port'] = str(addr[1])
					self.vehicle_manager_info['rate'] = str(0)
					self.start_thread(self.controlled_vehicles_handler, args=([conn]))


				elif data == 'simulated_controller':
					self.timed_print('Simulated vehicle controller connected','OKGREEN')
					self.vehicle_command_manager_info['number_simulated_vehicles'] += 1
					self.start_thread(self.commands_manager.vehicle_controller_handler, args=(conn,data,self.current_simulated_id))
					self.current_simulated_id -= 1 

				elif data == 'real_controller':
					self.timed_print('Real vehicle controller connected','OKGREEN')
					self.vehicle_command_manager_info['number_real_vehicles'] += 1 
					self.start_thread(self.commands_manager.vehicle_controller_handler, args=(conn,data))

				elif data == 'real_vehicle_output':
					self.vehicle_output_manager_info['ip'] = addr[0]
					self.vehicle_output_manager_info['port'] = str(addr[1])
					self.timed_print('Vehicle output manager connected','OKGREEN')
					self.vehicle_output_manager_connected = True
					self.start_thread(self.commands_manager.real_vehicle_output_handler, args=([conn]))

				else:
					print "data: " + str(data)

			except KeyboardInterrupt:
				break

		serv.close()
		self.timed_print('Connections server stopped', 'WARNING', parent='WORLD')
		self.command_central_connected = False
		self.close = True

	@staticmethod
	def get_states_message(states, ids_list):

		root = et.Element('message')

		root.set("type", "states_broadcast")

		for idx, state in enumerate(states):
			et.SubElement(root, "body", id=str(ids_list[idx]), x=str(state[0]), y=str(state[1]), yaw=str(state[2]), )

		xml_string = et.tostring(root)

		return xml_string

	@staticmethod
	def get_trees_message():

		root = et.Element('message')

		root.set("type", "trees")

		# y 110 +- 20

		spreadness_x = 512
		spreadness_y = 30

		center_x = 0
		center_y = 100

		num_trees = 300

		trees = []

		for idx in range(num_trees):
			trees.append([center_x + spreadness_x * (random.random() - 0.5), center_y + 2 * spreadness_y * (random.random() - 0.5), 'tree'])

		center_y = -100

		for idx in range(num_trees):
			trees.append([center_x + spreadness_x * (random.random() - 0.5), center_y + 2 * spreadness_y * (random.random() - 0.5), 'tree'])

		num_trees = 50

		center_x = 220
		center_y = 0

		spreadness_x = 30
		spreadness_y = 60

		for idx in range(num_trees):
			trees.append([center_x + spreadness_x * (random.random() - 0.5), center_y + 2 * spreadness_y * (random.random() - 0.5), 'tree'])

		center_x = -240

		for idx in range(num_trees):
			trees.append([center_x + spreadness_x * (random.random() - 0.5), center_y + 2 * spreadness_y * (random.random() - 0.5), 'tree'])

		for idx, tree in enumerate(trees):
			et.SubElement(root, "tree", x=str(tree[0]), y=str(tree[1]), type=tree[2])

		xml_string = et.tostring(root)

		return xml_string

	def unity_simulator_receiver_handler(self):

		tcp_ip = ''
		tcp_port = 55017
		self.buffersize = 1024

		self.timed_print("Simulator receiver handler", 'OKGREEN')

		s = socket(AF_INET, SOCK_STREAM)
		s.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)

		s.bind((tcp_ip, tcp_port))
		s.listen(1)

		s.settimeout(1)

		self.simulator_receiver_connected = True

		self.simulator_receiver_info['ip'] = 'None'
		self.simulator_receiver_info['port'] = 'None'
		self.simulator_receiver_info['rate'] = str(0)

		while not self.close:

			self.timed_print('Simulator receiver handler waiting for connections at port: ' + str(tcp_port))

			while not self.close:

				caught = False

				try:
					conn, addr = s.accept()
					# print "CONNECTED: Simulator receiver"
					break
				except timeout:
					# print "Simulator receiver socket timed out!"
					caught = True
					continue
				except error:					
					# print "Simulator receiver unknown error caught"
					caught = True
					continue
				else:
					# print '!!!!!!!!!!!!!!!!!!!!!!!!!'

					# print "Caught = " + str(caught)
					raise
					# s.close()
					# self.simulator_receiver_connected = False
					# self.timed_print('Simulator receiver handler is closed', 'WARNING')
					# return
					continue
			if self.close:
				break

			self.simulator_receiver_info['ip'] = addr[0]
			self.simulator_receiver_info['port'] = str(addr[1])

			self.timed_print('IP: ' + addr[0] + ' just connected to the simulater receiver handler')

			conn.settimeout(0.5 * (1 / float(self.simulator_receiver_desired_rate)))

			try:

				while not self.close:

					# print 'unity_simulator_receiver_handler'

					tic = time.time()

					try:

						data = conn.recv(self.buffersize).strip('\n').strip('\r').lower()

						if data == 'close':
							self.timed_print('Simulator receiver disconnected', 'WARNING', parent='CMD')
							conn.close()
							break

						# print "received data from simulator: " + str(data)

						if data:


							messages_received = filter( None, data.split('\n') )
							# print "Unity Received: messages_received = " + str(messages_received)
							last_string = messages_received[len(messages_received)-1]

							[unity_simulator_car_id, unity_simulator_car_x, unity_simulator_car_y, unity_simulator_car_yaw] = xmlcomms.process_unity_simulator_message_string(last_string)
							# print "[unity_simulator_car_id, unity_simulator_car_x, unity_simulator_car_y, unity_simulator_car_yaw] = " + str([unity_simulator_car_id, unity_simulator_car_x, unity_simulator_car_y, unity_simulator_car_yaw])
							if unity_simulator_car_id in self.bodies_array:

								self.bodies_array[unity_simulator_car_id].x = unity_simulator_car_x
								self.bodies_array[unity_simulator_car_id].y = unity_simulator_car_y
								self.bodies_array[unity_simulator_car_id].yaw = -unity_simulator_car_yaw + 90.

							else:

								new_body = bodyclasses.UnitySimulatorCar()

								new_body.id = unity_simulator_car_id

								new_body.x = unity_simulator_car_x
								new_body.y = unity_simulator_car_y
								new_body.z = 0.
								new_body.yaw = unity_simulator_car_yaw
								new_body.pitch = 0.
								new_body.roll = 0.

								self.bodies_array[unity_simulator_car_id] = new_body

							# unity_simulator_body = dict
							# {"id": unity_simulator_car_id, "x": unity_simulator_car_x / 32., "y": unity_simulator_car_y / 32., "z": 0., "yaw": -unity_simulator_car_yaw + 90., "roll": 0., "pitch": 0., "body_type": "simulator_vehicle"}

							# self.simulator_body = simulator_body
					except timeout:

						# print "Simple receive time out, ignoring"
						pass

					except socketerror as e:
						if e.errno in [errno.ECONNRESET, errno.EPIPE]:
							self.timed_print('Simulator receiver disconnected', 'WARNING', parent='CMD')
							conn.close()
							self.simulator_receiver_info['ip'] = 'None'
							self.simulator_receiver_info['port'] = 'None'
							self.simulator_receiver_info['rate'] = str(0)
							break
					else:
						pass

					toc = time.time()

					self.waiting_for_desired_rate(tic,toc,'simulator_receiver')

			except error as msg:
				self.timed_print("Exception on socket in simulator receiver handler:" + str(msg), 'FAIL')
				raise
				try:
					conn.close()
				except:
					pass
		try:
			conn.close()
		except:
			pass

		self.timed_print("Simulator receiver handler is closed", 'WARNING')

		self.simulator_receiver_connected = False

	def unity_simulator_handler(self):

		self.timed_print('Simulator handler started', 'OKGREEN')

		tcp_ip = ''
		tcp_port = 5017

		s = socket(AF_INET, SOCK_STREAM)

		s.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)

		s.bind((tcp_ip, tcp_port))
		s.listen(1)
		s.settimeout(1)

		self.simulator_connected = True

		self.simulator_info['ip'] = 'None'
		self.simulator_info['port'] = 'None'
		self.simulator_info['rate'] = str(0)

		while not self.close:

			self.timed_print('Simulator handler waiting for connections at port: ' + str(tcp_port))

			while not self.close:

				caught = False

				try:
					conn, addr = s.accept()
					# print "CONNECTED: Simulator sender"
					break
				except sockettimeout:
					# print "Simulator sender socket exception: sockettimeout"
					caught = True	
					continue	
				except socketherror:
					# print "Simulator sender socket exception: socketherror"
					caught = True		
					continue
				except socketgaierror:
					# print "Simulator sender socket exception: socketgaierror"
					caught = True	
					continue	
				except socketerror:
					# print "Simulator sender socket exception: socketerror"
					caught = True	
					continue				
				except timeout:
					# print "Simulator sender socket exception: timeout"
					caught = True
					continue
				# except socket.timeout: # cannot use this exception
				# 	print "Simulator sender socket exception: socket.timeout"
				# 	caught = True
				except error:
					# print "Simulator sender socket exception: error"
					caught = True
					continue
				else:
					# print '?????????????'
					# print "Caught = " + str(caught)
					raise
					# s.close()
					# self.simulator_connected = False
					# self.timed_print('Simulator handler is closed', 'WARNING')
					# return
					continue
			if self.close:
				break

			self.simulator_info['ip'] = addr[0]
			self.simulator_info['port'] = str(addr[1])

			self.timed_print('IP: ' + addr[0] + ' just connected to the simulater handler')

			conn.setblocking(1)

			try:

				while not self.close:
					# print 'unity_simulator_handler'

					tic = time.time()

					# self.msg_to_reroute_to_internet  = dict()
					# self.msg_to_reroute_to_internet ['type  = 'trajectory'
					# # self.msg_to_reroute_to_internet ['string  = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><message body_id=\"2\" type=\"body_trajectory_command\"><trajectory><point seq=\"0\" x=\"12.5875908646\" y=\"21.0662086337\"/><point seq=\"1\" x=\"12.7844305973\" y=\"21.1009729736\"/><point seq=\"2\" x=\"12.9812703299\" y=\"21.1357373135\"/></trajectory></message>"
					# self.msg_to_reroute_to_internet ['string  = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><message body_id=\"2\" type=\"body_trajectory_command\"><trajectory><point seq=\"0\" x=\"10.0\" y=\"21.0\"/><point seq=\"1\" x=\"20.0\" y=\"31.1\"/><point seq=\"2\" x=\"30.0\" y=\"41.1\"/></trajectory></message>"

					if self.msg_to_reroute_to_simulator is not None:

						if self.msg_to_reroute_to_simulator['type'] == 'collision':
							self.timed_print('Sent collision_warning ( vehicle ' + str(self.msg_to_reroute['trucks_in_collision']) + ') to simulator', parent='VEHICLE')
						elif self.msg_to_reroute_to_simulator['type'] == 'vehicle_stop_command':
							self.timed_print('Sent vehicle_stop_command to simulator', parent='VEHICLE')
						else:
							# timed_print('Sent trajectory to vehicle manager',parent = 'VEHICLE')

							# self.msg_to_reroute_to_internet  = dict()
							# self.msg_to_reroute_to_internet ['type  = 'trajectory'
							# self.msg_to_reroute_to_internet ['string  =  command_central_reply[1]

							conn.send(self.msg_to_reroute_to_simulator['string'] + '\n')

							self.msg_to_reroute_to_simulator = None

							time.sleep(1. / self.simulator_desired_rate)
							continue

						self.msg_to_reroute_to_simulator = None

					if not self.bodies_array:
						time.sleep(1. / self.simulator_desired_rate)
						continue


					trees_sent = True
					if trees_sent:
						# states_string = xmlcomms.get_vehicle_states_reply_message_string_old(self.bodies_list) + '\n'
						try:
							states_string = xmlcomms.get_vehicle_states_reply_message_string(self.bodies_array) + '\n'
							conn.send(states_string)
						except:
							print "unity_simulator_handler error caught, probably the dictionary size changed "
							time.sleep(0.01)
					else:
						states_string = self.get_trees_message() + '\n'
						conn.send(states_string)

					

					if self.close:
						conn.close()
						break

					toc = time.time()

					self.waiting_for_desired_rate(tic,toc,'simulator')

			except socketerror as e:
				if e.errno in [errno.ECONNRESET, errno.EPIPE]:
					self.timed_print('Simulator disconnected', 'WARNING', parent='CMD')
					conn.close()
					self.simulator_info['ip'] = 'None'
					self.simulator_info['port'] = 'None'
					self.simulator_info['rate'] = str(0)
					continue
			else:
				self.timed_print("Exception on socket in simulator handler", 'FAIL')

				try:
					conn.close()
				except:
					pass

		try:
			conn.close()
		except:
			pass

		self.simulator_connected = False

		self.timed_print('Simulator handler is closed', 'WARNING')

	def start_sml_world(self):

		self.bodies_list_xml = ''
		self.bodies_list = []
		self.bodies_readings = []
		self.controllables_list = []
		self.obstructed_lanelets = []
		self.new_message = None
		self.close = False
		self.highlighted_vehicle_id = 0
		self.current_simulated_id = -1
		self.highlight_mode = False
		self.msg_to_reroute = None
		self.msg_to_reroute_to_cmd_central = None
		self.msg_to_reroute_to_projector = None
		self.msg_to_reroute_to_simulator = None
		self.msg_to_reroute_to_construction = None
		self.msg_task_to_projector = None
		self.vehicle_input_commands = None

		self.gui_changed = False
		self.projector_area = []
		self.rrt_reply_string = None

		if not self.server_mode:
			self.get_settings_from_gui()

			# self.gui.sisemptop.setVisible(True)
			self.gui.start.setVisible(False)
			self.gui.start_projector.setDisabled(False)
			# self.gui.waiting_label.setVisible(True)
			self.gui.simulatedprogressbar.setVisible(True)

		self.timed_print('Creating Connections Server in Port:' + str(self.server_port), parent='WORLD')
		serv = self.create_connections_server()

		# xml_file_location = 'Lanelets/myEleventhCity.xml'
		self.timed_print('Parsing ' + self.xml_file_location, parent='WORLD')
		lanelet_module = laneletmodule.LaneletModule()
		lanelet_module.load_xml_world_info(self.xml_file_location)
		self.timed_print(self.xml_file_location + ' is loaded', parent='WORLD')

		# self.initiate_body_array()

		self.timed_print('Starting vehicle command manager handler', parent='WORLD')
		self.vehicle_command_manager_initializer(lanelet_module)

		# print "Remove this sleep"
		# time.sleep(2)
		# print "Remove this sleep"

		if self.activate_states_from_mocap:

			self.start_thread(self.mocap_handler)

		else:

			print "WARNING: Mocap Handler was not started due to not being ticked on the GUI. If you want to use the MOCAP restart the program and tick this option!"

		# self.timed_print('Starting vehicle simulator handler', parent='WORLD')
		# self.start_thread(self.vehicle_unity_simulator_handler, args=([lanelet_module]))

		# self.timed_print('Starting traffic controller', parent='WORLD')
		# self.start_thread(self.fake_traffic_controller, args=())

		# self.start_thread(self.vehicle_unity_simulator_handler, args=([lanelet_module]))

		self.timed_print('Starting V2V handler', parent='WORLD')
		self.start_thread( self.v2v_handler, args=() )

		self.timed_print('Starting V2I handler', parent='WORLD')
		self.start_thread( self.v2i_handler, args=([lanelet_module]) )

		# self.timed_print('Starting people handler', parent='WORLD')
		# self.start_thread( self.people_manager_handler, args=() )
		

		# self.start_thread(self.vehicle_command_manager_handler, args=([lanelet_module]))

		self.timed_print('Starting Connections Server', parent='WORLD')
		self.start_thread(self.start_connections_server, args=(serv, lanelet_module))

		self.timed_print('Starting unity simulator sender thread', parent='WORLD')
		self.start_thread(self.unity_simulator_handler)
		
		self.timed_print('Starting unity simulator receiver thread', parent='WORLD')
		self.start_thread(self.unity_simulator_receiver_handler)

		self.timed_print('Starting vehicle simulation', parent='WORLD')
		self.start_thread(self.vehicle_simulation, args=())

	def stop_sml_world(self):

		if not self.server_mode:
			self.gui.stop.setVisible(False)
			self.gui.start.setVisible(True)
			self.gui.simulatedprogressbar.setVisible(False)
			self.gui.stop_projector.setVisible(False)
			self.gui.start_projector.setVisible(True)
			self.gui.start_projector.setDisabled(True)

		if self.projector_thread:
			self.stop_projector()

		self.close = True

	def start_projector(self):

		sys.path.append('Projector')
		import projector_class

		self.get_settings_from_gui()

		self.gui.stop_projector.setVisible(True)
		self.gui.start_projector.setVisible(False)

		image_folder = 'resources'

		# p = Process(target = projector.start_projector,args = (image_folder,self.projector_info) )
		self.projector_thread = Process(target=projector_class.run, args=(self.projector_info, image_folder))

		self.projector_thread.start()

	def stop_projector(self):

		self.gui.start_projector.setVisible(True)
		self.gui.stop_projector.setVisible(False)

		self.projector_manager_connected = False
		self.highlight_connected = False

		self.projector_thread.terminate()

	@staticmethod
	def read_obstacles_from_file():

		obstacle_file = "file_sources/obstacles_list.txt"

		obstacles = []

		if os.path.exists(obstacle_file):

			f = open(obstacle_file, 'r')

			data = f.read()

			f.close()

			if len(data) == 0:
				return obstacles

			if data[-1] == '\n':
				data = data[:-1]

			data = data.split('\n')

			for obstacle in data:

				obstacle_elements = obstacle.split(' ')

				if obstacle_elements[1] in ['rectangle', 'rrt']:
					obstacles.append([int(obstacle_elements[0]), obstacle_elements[1], float(obstacle_elements[2]), float(obstacle_elements[3])])
				elif obstacle_elements[1] in ['circle', 'gun']:
					obstacles.append([int(obstacle_elements[0]), obstacle_elements[1], float(obstacle_elements[2])])
				else:
					obstacles.append([int(obstacle_elements[0]), obstacle_elements[1]])
		return obstacles

	def update_vehicle_list(self):

		current_list = [int(self.gui.body_id.itemText(i)) for i in xrange(self.gui.body_id.count())]
		current_trailer_list = [int(self.gui.trailer_id.itemText(i)) for i in xrange(self.gui.trailer_id.count())]

		body_list = [body['id'] for body in self.bodies_list if body['id'] > 0]

		trailer_list = [body['id'] for body in self.bodies_list if body['id'] > 0 and 'body_type' in body and body['body_type'] == 'trailer']
		# trailer_list = []

		for body_id in list(set(body_list) - set(current_list)):
			self.gui.body_id.addItem(str(body_id))

		for trailer_id in list(set(trailer_list) - set(current_trailer_list)):
			self.gui.trailer_id.addItem(str(trailer_id))

		for body_id in list(set(current_list) - set(body_list)):
			itemindex = self.gui.body_id.findText(str(body_id))
			self.gui.body_id.removeItem(itemindex)

		for trailer_id in list(set(current_trailer_list) - set(trailer_list)):
			itemindex = self.gui.trailer_id.findText(str(trailer_id))
			self.gui.trailer_id.removeItem(itemindex)

	def save_vehicle_info_in_file(self):

		f = open('file_sources/vehicle_info.txt', 'w')

		for info in self.vehicle_info:
			info_str = str(info[0]) + ' trailer:' + str(info[0] in self.trailer_list) + ' type:' + str(info[1]) + ' trailer_id:' + str(info[2])
			f.write(info_str)
			f.write('\n')

		vehicle_ids = [ele[0] for ele in self.vehicle_info]

		for vehicle in [ele for ele in self.trailer_list if ele not in vehicle_ids]:
			info_str = str(vehicle) + ' trailer:' + str(vehicle in self.trailer_list) + ' type:vehicle' + ' trailer_id:' + str(0)
			f.write(info_str)
			f.write('\n')

		f.close()

	def vehicle_id_change(self):

		current_body_id = self.gui.body_id.currentText()

		if current_body_id == '':
			self.gui.body_type.setDisabled(True)
			self.gui.body_type_label.setDisabled(True)
			self.gui.trailer_status.setDisabled(True)
			return

		self.gui.body_type.setDisabled(False)
		self.gui.body_type_label.setDisabled(False)
		self.gui.trailer_status.setDisabled(False)

		current_body_id = int(current_body_id)

		self.gui.trailer_status.setChecked(current_body_id in self.trailer_list)

		self.gui.trailer_id.setDisabled(not self.gui.trailer_status.isChecked())
		self.gui.trailer_id_label.setDisabled(not self.gui.trailer_status.isChecked())

		vehicle_info_ids = [ele[0] for ele in self.vehicle_info]

		current_body_type = 'vehicle'

		if current_body_id in vehicle_info_ids:
			for index, vehicle in enumerate(self.vehicle_info):
				if vehicle[0] == current_body_id:
					current_body_type = vehicle[1]
					break

		itemindex = self.gui.body_type.findText(current_body_type)
		self.gui.body_type.setCurrentIndex(itemindex)

	def vehicle_type_change(self):

		current_body_id = self.gui.body_id.currentText()
		if current_body_id == '':
			return
		current_body_id = int(current_body_id)

		current_body_type = self.gui.body_type.currentText()

		if self.gui.trailer_status.isChecked():
			current_trailer_id = self.gui.trailer_id.currentText()
		else:
			current_trailer_id = 0

		vehicle_info_ids = [ele[0] for ele in self.vehicle_info]

		if current_body_id in vehicle_info_ids:
			for index, vehicle in enumerate(self.vehicle_info):
				if vehicle[0] == current_body_id:
					self.vehicle_info.pop(index)

		self.vehicle_info.append([current_body_id, current_body_type, current_trailer_id])

		self.save_vehicle_info_in_file()

	def trailer_id_change(self):

		current_body_id = self.gui.body_id.currentText()

		if current_body_id == '':
			return

		for vehicle in self.vehicle_info:
			if vehicle[0] == int(current_body_id) and self.gui.trailer_status.isChecked():
				vehicle[2] = int(self.gui.trailer_id.currentText())
				break

		self.save_vehicle_info_in_file()

	def trailer_status_change(self):

		current_body_id = self.gui.body_id.currentText()

		if current_body_id == '':
			return

		self.gui.trailer_id.setDisabled(not self.gui.trailer_status.isChecked())
		self.gui.trailer_id_label.setDisabled(not self.gui.trailer_status.isChecked())

		current_body_id = int(current_body_id)

		if current_body_id not in self.trailer_list and self.gui.trailer_status.isChecked():

			self.trailer_list.append(current_body_id)

			if int(current_body_id) not in [ele[0] for ele in self.vehicle_info]:
				self.vehicle_info.append([current_body_id, self.gui.body_type.currentText(), int(self.gui.trailer_id.currentText())])
			else:
				for vehicle in self.vehicle_info:
					if vehicle[0] == int(current_body_id):
						vehicle[2] = self.gui.trailer_id.currentText()
						break

		elif current_body_id in self.trailer_list and not self.gui.trailer_status.isChecked():

			self.trailer_list.pop(self.trailer_list.index(current_body_id))

			for vehicle in self.vehicle_info:
				if vehicle[0] == int(current_body_id):
					vehicle[2] = 0
					break

		self.save_vehicle_info_in_file()

	@staticmethod
	def read_vehicle_info_from_file():

		vehicle_file = "file_sources/vehicle_info.txt"

		trailer_list = []

		vehicle_info = []

		if os.path.exists(vehicle_file):

			f = open(vehicle_file, 'r')

			data = f.read()

			if data == '':
				return [[], []]

			if data[-1] == '\n':
				data = data[:-1]

			data = data.split('\n')

			for vehicle in data:

				vehicle_elements = vehicle.split(' ')

				body_id = vehicle_elements[0]

				trailer_id = 0
				body_type = 'vehicle'

				for element in vehicle_elements:

					tags = element.split(':')

					if tags[0] == 'trailer_id':
						trailer_id = tags[1]
					if tags[0] == 'trailer' and tags[1] == 'True':
						trailer_list.append(int(body_id))
					if tags[0] == 'type':
						body_type = tags[1]

				vehicle_info.append([int(body_id), body_type, trailer_id])

		return [trailer_list, vehicle_info]

	def save_settings_in_file(self):

		attribute_list_1 = ['qualisys_info', 'projector_info', 'xml_file_location', 'activate_draw_origin', 'draw_sensors', 'draw_signal', 'show_ids', 'activate_fps', 'activate_states_from_mocap', 'activate_states_from_world', 'only_simulated_vehicles', 'only_real_vehicles']
		attribute_list_2 = [ele + '_desired_rate' for ele in self.list_of_modules]
		attribute_list_3 = ['projector_area', 'load_saved_background', 'projector_wait_for_command_central', 'log_xml', 'number_vehicles', 'server_port', 'buffersize', 'v2v_method', 'v2v_range', 'v2v_angle']

		attribute_list = attribute_list_1 + attribute_list_2 + attribute_list_3

		with open('file_sources/default_settings.txt', 'w') as f:

			for attribute in attribute_list:

				f.write(attribute + ':' + str(getattr(self, attribute)) + '\n')

		self.timed_print('Save default setting to file', 'OKGREEN')

	def read_default_settings_from_file(self):

		with open('file_sources/default_settings.txt', 'r') as f:

				settings = f.read()

				if not settings:
					return

				settings = settings.split('\n')

				for setting in settings[:-1]:

					setting_attribute, setting_value = setting.split(':')

					if setting_attribute in [ele + '_desired_rate' for ele in self.list_of_modules]:
						setattr(self, setting_attribute, int(setting_value))
						getattr(self.gui, setting_attribute).setValue(int(setting_value))

					elif setting_attribute in ['number_vehicles', 'server_port', 'buffersize']:
						setattr(self, setting_attribute, int(setting_value.strip()))
						getattr(self.gui, setting_attribute).setText(setting_value.strip())

					elif setting_attribute in ['activate_draw_origin', 'draw_sensors', 'draw_signal', 'show_ids', 'activate_fps', 'activate_states_from_mocap', 'activate_states_from_world', 'only_simulated_vehicles', 'only_real_vehicles', 'load_saved_background', 'projector_wait_for_command_central', 'log_xml']:
						state = setting_value == 'True'
						setattr(self, setting_attribute, state)
						if state:
							getattr(self.gui, setting_attribute).setChecked(state)

					elif setting_attribute in ['v2v_range', 'v2v_angle']:
						setattr(self, setting_attribute, float(setting_value))
						getattr(self.gui, setting_attribute).setText(setting_value)

					elif setting_attribute in ['v2v_method']:
						setattr(self, setting_attribute, setting_value)

						if self.v2v_method == 'communications':
							self.gui.v2v_comm.setChecked(True)
						elif self.v2v_method == 'sensors':
							self.gui.v2v_sensors.setChecked(True)
						elif self.v2v_method == 'off':
							self.gui.v2v_off.setChecked(True)

					elif setting_attribute in ['xml_file_location']:
						setattr(self, setting_attribute, setting_value)
						setting_value_1 = setting_value.split('/')
						setting_value_2 = setting_value.split('\\')
						if len(setting_value_1) > 1:
							setting_value = setting_value_1[-1]
						elif setting_value_2:
							setting_value = setting_value_2[-1]
						print setting_value
						getattr(self.gui, setting_attribute).setText(setting_value)

					elif setting_attribute in ['projector_area']:
						area = [float(ele) for ele in setting_value[1:-1].split(',')]
						setattr(self, setting_attribute, area)
						self.gui.xmin.setText(str(area[0]))
						self.gui.xmax.setText(str(area[1]))
						self.gui.ymin.setText(str(area[2]))
						self.gui.ymax.setText(str(area[2]))

					elif setting_attribute in ['qualisys_info', 'projector_info']:
						string_part, number_part = setting_value[1:-1].split(',')
						setattr(self, setting_attribute, (string_part, int(number_part.strip())))

						name = setting_attribute.split('_')[0]

						getattr(self.gui, name + '_host').setText(string_part[1:-1])
						getattr(self.gui, name + '_port').setText(number_part.strip())

		self.timed_print('Default settings loaded', 'OKGREEN')

	def get_settings_from_gui(self):

		# Qualisys
		qualisys_host = str(self.gui.qualisys_host.text())
		qualisys_port = int(self.gui.qualisys_port.text() if str(self.gui.qualisys_port.text()).isdigit() else 0)
		self.qualisys_info = (qualisys_host, qualisys_port)

		# Projector
		projector_host = str(self.gui.projector_host.text())
		projector_port = int(self.gui.projector_port.text() if str(self.gui.projector_port.text()).isdigit() else 0)

		projector_width = int(self.gui.projector_width.text() if str(self.gui.projector_width.text()).isdigit() else 0)
		projector_height = int(self.gui.projector_height.text() if str(self.gui.projector_height.text()).isdigit() else 0)

		self.projector_resolution = (projector_width,projector_height)

		self.projector_info = (projector_host, projector_port)

		self.xml_file_location = os.path.join('Lanelets', str(self.gui.xml_file_location.text()))

		self.activate_draw_origin = bool(self.gui.activate_draw_origin.checkState())
		self.draw_sensors = bool(self.gui.draw_sensors.checkState())
		self.draw_signal = bool(self.gui.draw_signal.checkState())
		self.show_ids = bool(self.gui.show_ids.checkState())
		self.activate_fps = bool(self.gui.activate_fps.checkState())
		self.activate_states_from_mocap = bool(self.gui.activate_states_from_mocap.checkState())
		self.activate_states_from_world = bool(self.gui.activate_states_from_world.checkState())
		self.only_simulated_vehicles = bool(self.gui.only_simulated_vehicles.checkState())
		self.only_real_vehicles = bool(self.gui.only_real_vehicles.checkState())


		for module in self.list_of_modules:
			try:
				setattr(self, module + '_desired_rate', getattr(self.gui, module + '_desired_rate').value())
			except:
				raise
				pass


		xmin = float(self.gui.xmin.text() if str(self.gui.xmin.text()).replace('.','',1).isdigit() else 0)
		xmax = float(self.gui.xmax.text() if str(self.gui.xmax.text()).replace('.','',1).isdigit() else 0)
		ymin = float(self.gui.ymin.text() if str(self.gui.ymin.text()).replace('.','',1).isdigit() else 0)
		ymax = float(self.gui.ymax.text() if str(self.gui.ymax.text()).replace('.','',1).isdigit() else 0)

		self.projector_area = [xmin, xmax, ymin, ymax]

		# Settings

		self.load_saved_background = self.gui.load_saved_background.checkState() == 2
		self.projector_wait_for_command_central = self.gui.projector_wait_for_command_central.checkState() == 2
		self.log_xml = self.gui.log_xml.checkState() == 2

		self.number_vehicles = int(self.gui.number_vehicles.text() if str(self.gui.number_vehicles.text()).isdigit() else 0)
		self.server_port = int(self.gui.server_port.text() if str(self.gui.server_port.text()).isdigit() else 0)
		self.buffersize = int(self.gui.buffersize.text() if str(self.gui.buffersize.text()).isdigit() else 0)

		if self.gui.v2v_comm.isChecked():
			self.v2v_method = 'communications'
			self.v2v_range = float(self.gui.v2v_range.text() if str(self.gui.v2v_range.text()).replace('.','',1).isdigit() else 0)
			self.gui.v2v_range.setDisabled(False)
			self.gui.range_label.setDisabled(False)
			self.gui.v2v_angle.setDisabled(True)
			self.gui.angle_label.setDisabled(True)
		elif self.gui.v2v_sensors.isChecked():
			self.v2v_method = 'sensors'
			self.v2v_range = float(self.gui.v2v_range.text() if str(self.gui.v2v_range.text()).replace('.','',1).isdigit() else 0)
			self.v2v_angle = float(self.gui.v2v_angle.text() if str(self.gui.v2v_angle.text()).replace('.','',1).isdigit() else 0)
			self.gui.v2v_range.setDisabled(False)
			self.gui.range_label.setDisabled(False)
			self.gui.v2v_angle.setDisabled(False)
			self.gui.angle_label.setDisabled(False)
		elif self.gui.v2v_off.isChecked():
			self.v2v_method = 'off'
			self.gui.v2v_angle.setDisabled(True)
			self.gui.angle_label.setDisabled(True)
			self.gui.v2v_range.setDisabled(True)
			self.gui.range_label.setDisabled(True)

	def check_gui_change(self):

		test_list = [self.activate_draw_origin == bool(self.gui.activate_draw_origin.checkState()),
					self.draw_sensors == bool(self.gui.draw_sensors.checkState()),
					self.draw_signal == bool(self.gui.draw_signal.checkState()),
					self.show_ids == bool(self.gui.show_ids.checkState()),
					self.activate_fps == bool(self.gui.activate_fps.checkState()),
					self.activate_states_from_mocap == bool(self.gui.activate_states_from_mocap.checkState()),
					self.activate_states_from_world == bool(self.gui.activate_states_from_world.checkState()),
					self.only_simulated_vehicles == bool(self.gui.only_simulated_vehicles.checkState()),
					self.only_real_vehicles == bool(self.gui.only_real_vehicles.checkState())]

		new_v2v = dict()

		if self.v2v_method != '':
			if self.gui.v2v_comm.isChecked():
				new_v2v['method'] = 'communications'
				new_v2v['range'] = float(self.gui.v2v_range.text())
				test_list.append(new_v2v['method'] == self.v2v_method)
				test_list.append(new_v2v['range'] == self.v2v_range)
			elif self.gui.v2v_sensors.isChecked():
				new_v2v['method'] = 'sensors'
				new_v2v['range'] = float(self.gui.v2v_range.text())
				new_v2v['angle'] = float(self.gui.v2v_angle.text())
				test_list.append(new_v2v['method'] == self.v2v_method)
				test_list.append(new_v2v['range'] == self.v2v_range)
				test_list.append(new_v2v['angle'] == self.v2v_angle)
			elif self.gui.v2v_off.isChecked():
				new_v2v['method'] = 'off'
				test_list.append(new_v2v['method'] == self.v2v_method)

		xmin = float(self.gui.xmin.text() if str(self.gui.xmin.text()).replace('.','',1).isdigit() else 0.0)
		xmax = float(self.gui.xmax.text() if str(self.gui.xmax.text()).replace('.','',1).isdigit() else 0.0)
		ymin = float(self.gui.ymin.text() if str(self.gui.ymin.text()).replace('.','',1).isdigit() else 0.0)
		ymax = float(self.gui.ymax.text() if str(self.gui.ymax.text()).replace('.','',1).isdigit() else 0.0)

		test_list.append(self.projector_area == [xmin, xmax, ymin, ymax])

		projector_width = int(self.gui.projector_width.text() if str(self.gui.projector_width.text()).isdigit() else 0)
		projector_height = int(self.gui.projector_height.text() if str(self.gui.projector_height.text()).isdigit() else 0)

		test_list.append(self.projector_resolution == (projector_width,projector_height))

		if not self.gui_changed:
			self.gui_changed = not all(test_list)

	def initialize_gui_module_colors(self):

		for module in self.list_of_modules:

			if getattr(self, module + '_connected'):
				getattr(self.gui, module + '_frame').setStyleSheet("background-color: rgb(54,99,9); color: rgb(255, 255, 255);")
				getattr(self.gui, module + '_desired_rate').setStyleSheet("background-color: rgb(54,99,9); color: rgb(255, 255, 255); border: rgb(212,255,255)")
			else:
				getattr(self.gui, module + '_frame').setStyleSheet("background-color: rgb(135,26,26); color: rgb(255, 255, 255);")
				getattr(self.gui, module + '_desired_rate').setStyleSheet("background-color: rgb(135,26,26); color: rgb(255, 255, 255); border: rgb(212,255,255)")
				getattr(self.gui, module + '_text').setText(' ')


	def update_gui(self):

		self.check_gui_change()

		self.get_settings_from_gui()

		self.update_vehicle_list()


		if self.vehicle_simulator_ready:
			self.gui.stop.setVisible(True)
			self.gui.waiting_label.setVisible(False)
			self.vehicle_simulator_ready = False
			self.gui.simulatedprogressbar.setVisible(False)
		else:
			# self.gui.simulatedprogressbar.setStyleSheet("QProgressBar {background-color: red};");
			self.gui.simulatedprogressbar.setRange(0,self.number_vehicles)
			self.gui.simulatedprogressbar.setValue(abs(self.current_simulated_id))
			self.gui.simulatedprogressbar.setFormat(str(abs(self.current_simulated_id))+'/'+str(self.number_vehicles) + ' vehicles')

		if bool(self.main_info):
			self.gui.main_text.setText(self.main_info['rate'] + ' Hz')

		if bool(self.simulated_vehicles_info):
			self.gui.simulated_vehicles_text.setText(str(self.number_vehicles) + ' vehicles' + '\n')
			self.gui.simulated_vehicles_text.append(self.simulated_vehicles_info['rate'] + ' Hz')

		if bool(self.mocap_info):
			self.gui.mocap_text.setText(self.mocap_info['ip'])
			self.gui.mocap_text.append(self.mocap_info['port'])
			self.gui.mocap_text.append(self.mocap_info['rate'] + ' Hz')

		if bool(self.v2v_info):
			if self.v2v_method != '':

				self.gui.v2v_text.setText(self.v2v_method)

				if self.v2v_method != 'off':
					self.gui.v2v_text.append('Range: ' + str(self.v2v_range))
					if self.v2v_method == 'sensors':
						self.gui.v2v_text.append('Angle: ' + str(self.v2v_angle))

				self.gui.v2v_text.append(self.v2v_info['rate'] + ' Hz')
			else:
				self.gui.v2v_text.setText(self.v2v_info['rate'] + ' Hz')

		if bool(self.v2i_info):
			self.gui.v2i_text.setText(self.v2i_info['rate'] + ' Hz')

		if bool(self.vehicle_simulation_info):
			self.gui.vehicle_simulation_text.setText(self.vehicle_simulation_info['rate'] + ' Hz')

		if bool(self.projector_manager_info):
			self.gui.projector_manager_text.setText(self.projector_manager_info['ip'])
			self.gui.projector_manager_text.append(self.projector_manager_info['port'])
			self.gui.projector_manager_text.append(self.projector_manager_info['rate'] + ' Hz')

		if bool(self.highlight_info):
			self.gui.highlight_text.setText(self.highlight_info['ip'])
			self.gui.highlight_text.append(self.highlight_info['port'])
			self.gui.highlight_text.append(self.highlight_info['rate'] + ' Hz')

		if bool(self.command_central_info):
			self.gui.command_central_text.setText(self.command_central_info['ip'])
			self.gui.command_central_text.append(self.command_central_info['port'])
			self.gui.command_central_text.append(self.command_central_info['rate'] + ' Hz')

		if bool(self.vehicle_manager_info):
			self.gui.vehicle_manager_text.setText(self.vehicle_manager_info['ip'])
			self.gui.vehicle_manager_text.append(self.vehicle_manager_info['port'])
			self.gui.vehicle_manager_text.append('Controllable Vehicles')

			self.gui.vehicle_manager_text.append(str(self.controllables_list) )
			self.gui.vehicle_manager_text.append(self.vehicle_manager_info['rate'] + ' Hz')

		if bool(self.vehicle_command_manager_info):

			if self.vehicle_command_manager_info.has_key('rate'):
				self.gui.vehicle_command_manager_text.setText(self.vehicle_command_manager_info['rate'] + ' Hz')

		if bool(self.vehicle_output_manager_info):
			self.gui.vehicle_output_manager_text.setText(self.vehicle_output_manager_info['ip'])
			self.gui.vehicle_output_manager_text.append(self.vehicle_output_manager_info['port'])

		if bool(self.simulator_info):
			self.gui.simulator_text.setText(self.simulator_info['ip'])
			self.gui.simulator_text.append(self.simulator_info['port'])
			self.gui.simulator_text.append(self.simulator_info['rate'] + ' Hz')

		if bool(self.simulator_receiver_info):
			self.gui.simulator_receiver_text.setText(self.simulator_receiver_info['ip'])
			self.gui.simulator_receiver_text.append(self.simulator_receiver_info['port'])
			self.gui.simulator_receiver_text.append(self.simulator_receiver_info['rate'] + ' Hz')


		for module in self.list_of_modules:
			# By comparing with the previous module_states I can know when there was a change, and so, I will
			# only change its color (style) when this change happens
			# (before every cycle changed the style, even if it was to the same style as before)
			if getattr(self, module + '_connected') != self.previous_module_states[module + '_connected']:

				if getattr(self, module + '_connected'):
					getattr(self.gui, module + '_frame').setStyleSheet("background-color: rgb(54,99,9); color: rgb(255, 255, 255);")
					getattr(self.gui, module + '_desired_rate').setStyleSheet("background-color: rgb(54,99,9); color: rgb(255, 255, 255); border: rgb(212,255,255)")
				else:
					getattr(self.gui, module + '_frame').setStyleSheet("background-color: rgb(135,26,26); color: rgb(255, 255, 255);")
					getattr(self.gui, module + '_desired_rate').setStyleSheet("background-color: rgb(135,26,26); color: rgb(255, 255, 255); border: rgb(212,255,255)")
					getattr(self.gui, module + '_text').setText(' ')

	@staticmethod
	def open_background_image():
		bg_image = os.path.join('resources', 'world_surface.bmp')

		im = Image.open(bg_image)
		im.show()

	def change_obstacle_type(self):

		obstacles_id_list = [obstacle[0] for obstacle in self.obstacles_list]

		current_obstacle_id = int(self.gui.obstacle_id.currentText())
		current_obstacle_type = str(self.gui.obstacle_type.currentText())

		try:
			id_index = obstacles_id_list.index(current_obstacle_id)
			changed_obstacle = [current_obstacle_id, current_obstacle_type]
			if current_obstacle_type in ['circle', 'gun']:
				changed_obstacle.append(0.05)
			elif current_obstacle_type in ['rectangle', 'rrt']:
				if len(self.obstacles_list[id_index]) == 4:
					changed_obstacle.append(self.obstacles_list[id_index][2])
					changed_obstacle.append(self.obstacles_list[id_index][3])
				else:
					changed_obstacle.append(1.0)
					changed_obstacle.append(1.0)

			self.obstacles_list[id_index] = changed_obstacle

			self.change_obstacle_id()
		except:
			raise

		f = open('file_sources/obstacles_list.txt', 'w')

		for obstacle in self.obstacles_list:
			for element in obstacle:
				f.write(str(element) + ' ')
			f.write('\n')

	def change_obstacle_id(self):

		current_obstacle_id = self.gui.obstacle_id.currentText()

		if current_obstacle_id == '':
			self.gui.obstacle_type.setDisabled(True)
			self.gui.obstacle_id.setDisabled(True)
			self.gui.obstacle_radius.setVisible(False)
			self.gui.obstacle_radius_label.setVisible(False)
			self.gui.obstacle_height.setVisible(False)
			self.gui.obstacle_height_label.setVisible(False)
			self.gui.obstacle_width.setVisible(False)
			self.gui.obstacle_width_label.setVisible(False)
			return

		current_obstacle_id = int(current_obstacle_id)

		obstacles_id_list = [obstacle[0] for obstacle in self.obstacles_list]

		try:
			id_index = obstacles_id_list.index(current_obstacle_id)
			obstacle_type = self.obstacles_list[id_index][1]

			itemindex = self.gui.obstacle_type.findText(obstacle_type)
			self.gui.obstacle_type.setCurrentIndex(itemindex)

			if obstacle_type in ['circle', 'gun']:

				self.gui.obstacle_radius.setValue(self.obstacles_list[id_index][2])

				self.gui.obstacle_radius.setVisible(True)
				self.gui.obstacle_radius_label.setVisible(True)
				self.gui.obstacle_height.setVisible(False)
				self.gui.obstacle_height_label.setVisible(False)
				self.gui.obstacle_width.setVisible(False)
				self.gui.obstacle_width_label.setVisible(False)

			elif obstacle_type in ['rectangle', 'rrt']:

				self.gui.obstacle_width.setValue(self.obstacles_list[id_index][2])
				self.gui.obstacle_height.setValue(self.obstacles_list[id_index][3])

				self.gui.obstacle_radius.setVisible(False)
				self.gui.obstacle_radius_label.setVisible(False)
				self.gui.obstacle_height.setVisible(True)
				self.gui.obstacle_height_label.setVisible(True)
				self.gui.obstacle_width.setVisible(True)
				self.gui.obstacle_width_label.setVisible(True)
			else:
				self.gui.obstacle_radius.setVisible(False)
				self.gui.obstacle_radius_label.setVisible(False)
				self.gui.obstacle_height.setVisible(False)
				self.gui.obstacle_height_label.setVisible(False)
				self.gui.obstacle_width.setVisible(False)
				self.gui.obstacle_width_label.setVisible(False)

		except:
			return

	def add_obstacle(self):

		new_obstacle_id = self.gui.selected_obstacle_id.text()

		if new_obstacle_id == '':
			return

		new_obstacle_id = int(new_obstacle_id)

		if not self.obstacles_list:
			self.gui.obstacle_id.setDisabled(False)
			self.gui.obstacle_type.setDisabled(False)

		obstacles_id_list = [obstacle[0] for obstacle in self.obstacles_list]

		if new_obstacle_id not in obstacles_id_list:

			self.obstacles_list.append([new_obstacle_id, 'circle', 0.05])

			self.gui.obstacle_id.addItem(str(new_obstacle_id))

			new_item_index = self.gui.obstacle_id.findText(str(new_obstacle_id))

			self.gui.obstacle_id.setCurrentIndex(new_item_index)

			self.gui.selected_obstacle_id.setText('')

			self.change_obstacle_id()

			self.change_obstacle_type()

			f = open('file_sources/obstacles_list.txt', 'w')

			for obstacle in self.obstacles_list:
				for element in obstacle:
					f.write(str(element) + ' ')
				f.write('\n')

	def remove_obstacle(self):

		to_delete_obstacle_id = self.gui.selected_obstacle_id.text()

		if to_delete_obstacle_id == '':
			return

		to_delete_obstacle_id = int(to_delete_obstacle_id)

		obstacles_id_list = [obstacle[0] for obstacle in self.obstacles_list]

		try:
			self.gui.selected_obstacle_id.setText('')
			id_index = obstacles_id_list.index(to_delete_obstacle_id)

			itemindex = self.gui.obstacle_id.findText(str(self.obstacles_list[id_index][0]))

			self.gui.obstacle_id.removeItem(itemindex)

			self.obstacles_list.pop(id_index)

			f = open('file_sources/obstacles_list.txt', 'w')

			for obstacle in self.obstacles_list:
				for element in obstacle:
					f.write(str(element) + ' ')
				f.write('\n')

		except:
			pass

	def change_obstacle_radius(self):

		new_radius = self.gui.obstacle_radius.value()

		current_obstacle_id = int(self.gui.obstacle_id.currentText())

		obstacles_id_list = [obstacle[0] for obstacle in self.obstacles_list]

		id_index = obstacles_id_list.index(current_obstacle_id)

		self.obstacles_list[id_index][2] = new_radius

		f = open('file_sources/obstacles_list.txt', 'w')

		for obstacle in self.obstacles_list:
			for element in obstacle:
				f.write(str(element) + ' ')
			f.write('\n')

	def change_obstacle_width(self):

		new_width = self.gui.obstacle_width.value()

		current_obstacle_id = int(self.gui.obstacle_id.currentText())

		obstacles_id_list = [obstacle[0] for obstacle in self.obstacles_list]

		id_index = obstacles_id_list.index(current_obstacle_id)

		self.obstacles_list[id_index][2] = new_width

		f = open('file_sources/obstacles_list.txt', 'w')

		for obstacle in self.obstacles_list:
			for element in obstacle:
				f.write(str(element) + ' ')
			f.write('\n')

	def change_obstacle_height(self):

		new_height = self.gui.obstacle_height.value()

		current_obstacle_id = int(self.gui.obstacle_id.currentText())

		obstacles_id_list = [obstacle[0] for obstacle in self.obstacles_list]

		id_index = obstacles_id_list.index(current_obstacle_id)

		self.obstacles_list[id_index][3] = new_height

		f = open('file_sources/obstacles_list.txt', 'w')

		for obstacle in self.obstacles_list:
			for element in obstacle:
				f.write(str(element) + ' ')
			f.write('\n')

	def new_xml_file_location(self):

		file_path = str(QtGui.QFileDialog.getOpenFileName(directory="Lanelets"))

		file_name = os.path.split(file_path)[1]

		self.gui.xml_file_location.setText(file_name)

	def close_world(self):
		self.close_sml_world = True

	def save_previous_module_states(self):

		# print "save_previous_module_states"

		self.previous_module_states = dict()

		for module in self.list_of_modules:

			self.previous_module_states[module + '_connected'] = getattr(self, module + '_connected')

		# print "previous_module_states = " + str(previous_module_states) 


def main():

	server_mode = False

	if sys.argv > 1:
		for arg in sys.argv:
			server_mode = arg == '--server'

	if not server_mode:

		ex = QtGui.QApplication(sys.argv)

		smlworld = SMLworld(False)
		smlworld.timed_print('Object created successfully', 'OKGREEN', parent='WORLD')

		smlworld.Dialog.show()

		smlworld.main_connected = True

		smlworld.save_previous_module_states()

		smlworld.initialize_gui_module_colors()

		try:

			while not smlworld.close_sml_world:
				tic_main = time.time()

				smlworld.update_gui()

				smlworld.save_previous_module_states()

				ex.processEvents()

				toc_main = time.time()

				smlworld.waiting_for_desired_rate(tic_main, toc_main, 'main')

		except KeyboardInterrupt:
			smlworld.timed_print('KeyboardInterrupt', 'FAIL', parent='WORLD')

		smlworld.main_connected = False

		ex.quit()
		smlworld.Dialog.close()
		smlworld.stop_sml_world()

		smlworld.timed_print('World terminated', 'OKGREEN', parent='WORLD')

		time.sleep(1)

	else:
		smlworld = SMLworld(True)
		smlworld.server_port = 8000
		smlworld.buffersize = 128096
		smlworld.xml_file_location = os.path.join('Lanelets', 'myTwelfthCity.xml')
		smlworld.load_saved_background = False
		smlworld.number_vehicles = 10
		smlworld.projector_wait_for_command_central = False

		smlworld.v2v_method = 'communications'
		smlworld.v2v_range = 0.4
		smlworld.v2v_angle = 20

		smlworld.obstacles_list = smlworld.read_obstacles_from_file()
		smlworld.trailer_list = read_vehicle_info_from_file()

		smlworld.simulated_vehicles_desired_rate = 50
		smlworld.mocap_desired_rate = 50
		smlworld.v2v_desired_rate = 4
		smlworld.v2i_desired_rate = 2
		smlworld.projector_desired_rate = 50
		smlworld.highlight_desired_rate = 5
		smlworld.command_central_desired_rate = 30
		smlworld.vehicle_desired_rate = 5
		smlworld.simulator_desired_rate = 10
		smlworld.simulator_receiver_desired_rate = 30

		smlworld.main_info = dict()
		smlworld.simulated_vehicles_info = dict()
		smlworld.mocap_info = dict()
		smlworld.v2v_info = dict()
		smlworld.v2i_info = dict()
		smlworld.projector_manager_info = dict()
		smlworld.highlight_info = dict()
		smlworld.command_central_info = dict()
		smlworld.vehicle_manager_info = dict()

		smlworld.qualisys_info = ('130.237.43.74', 22224)

		smlworld.start_sml_world()


if __name__ == "__main__":

	# yappi.start()
	main()
	# yappi.get_func_stats().print_all()
	# yappi.get_thread_stats().print_all() # I don't need to see the threads