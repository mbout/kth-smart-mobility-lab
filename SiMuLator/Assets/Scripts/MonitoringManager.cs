﻿using UnityEngine;
using System.Collections;

public class MonitoringManager : MonoBehaviour {

	CameraManager cameraManager;
	GameObject monitoringGroup;
	public GameObject carImagePrefab;
	public GameObject ownCarImagePrefab;

	public float carSensorXFraction;
	public float carSensorYFraction;
	public float carSensorCenterX;
	public float carSensorCenterY;
	public float carSensorScaling;

	public float sensorRange;

	// Use this for initialization
	void Start () {
	
		cameraManager = GameObject.FindGameObjectWithTag("CameraManager").GetComponent<CameraManager>();
		monitoringGroup = GameObject.FindGameObjectWithTag("MonitoringGroup");

		//GameObject player = GameObject.FindGameOb = GameObject.FindGameObjectWithTag("CameraManager");jectWithTag("Player");

		carSensorScaling = 5.5f*(Screen.width/800.0f);

	}
	
	// Update is called once per frame
	void Update () {

		carSensorCenterX = Screen.width * carSensorXFraction;
		carSensorCenterY = Screen.height * carSensorYFraction ;

		if (cameraManager.lastId > 0 && cameraManager.cameraMode == 0) {

			monitoringGroup.SetActive ( true );

		} else {

//			Debug.Log ("monitoringManager active = false");

			monitoringGroup.SetActive ( false );

			GameObject[] carImages = GameObject.FindGameObjectsWithTag ("CanvasCarSensor");

			if (carImages != null ){

				for (int i = 0; i < carImages.Length; i++) {
					
					Destroy(carImages[i]);

				}

			}



		}

		//monitoringManager.SetActive ( cameraManager.lastId > 0);

		// GetComponent<Transform> ().eulerAngles.y;
			
	}

	public void ResetBirdView(){

		GameObject[] carImages = GameObject.FindGameObjectsWithTag ("CanvasCarSensor");

		for (int i = 0; i < carImages.Length; i++) {
			
			Destroy( carImages [i] );

		}

	}


	public void UpdateBirdView(ArrayList states, ArrayList ids){

		if (cameraManager.lastId <= 0) {
			
			return;			
			
		}

		if (cameraManager.cameraMode != 0) {
			
			return;
			
		}

		int ownId = cameraManager.lastId;
		float[] ownState = new float[3];

		if ( ids.Contains (ownId)) {

			int idx = ids.IndexOf (ownId);

			ownState = (float[]) states[idx];
			
		} else {

			Debug.Log ("My own ID not found");

		}


		//ArrayList states = new ArrayList ();
		//ArrayList idsForMonitoring = new ArrayList();

		GameObject[] carImages = GameObject.FindGameObjectsWithTag ("CanvasCarSensor");

		ArrayList existingCarImageIds = new ArrayList ();

		for (int i = 0; i < carImages.Length; i++) {

			int currentId = carImages[i].GetComponent<CarSensorScript>().vehicleId;

			//if ( currentId != -5){
//
//				continue;
//
//			}



			if ( ids.Contains(currentId) ){
								
				Debug.Log("Updating carSensed");

				int idx = ids.IndexOf(currentId);

				float[] state = (float[]) states[idx];

				float distanceToOther = Mathf.Sqrt( Mathf.Pow( state[0] - ownState[0] , 2) + Mathf.Pow( state[1] - ownState[1] , 2) );

				if ( distanceToOther < sensorRange ){

					float posX = carSensorCenterX;
					posX = posX + (state[0] - ownState[0])*carSensorScaling;
					
					float posY = carSensorCenterY;
					posY = posY + (state[1] - ownState[1])*carSensorScaling;
					//posY = 0.0f;



					float xDisplacement = (state[0] - ownState[0]);
					float yDisplacement = (state[1] - ownState[1]);
					
					float rotationAngleRadians = ownState[2]*Mathf.Deg2Rad;
					
					float xPixelDisplacement = xDisplacement*Mathf.Cos( rotationAngleRadians ) - yDisplacement*Mathf.Sin( rotationAngleRadians );
					float yPixelDisplacement = xDisplacement*Mathf.Sin( rotationAngleRadians ) + yDisplacement*Mathf.Cos( rotationAngleRadians );
					
					posX = carSensorCenterX + (xPixelDisplacement)*carSensorScaling;
					posY = carSensorCenterY + (yPixelDisplacement)*carSensorScaling;




					float yaw = 0.0f;
					yaw = yaw - state[2] + 90.0f + ownState[2];
									
					//posX = 250.0f;
					//posY = 250.0f;

					carImages[i].GetComponentsInChildren<RectTransform>()[1].position = new Vector3(posX, posY, 0.0f);
					carImages[i].GetComponentsInChildren<RectTransform>()[1].rotation = Quaternion.Euler(0, 0, yaw);

					//carImages[i].GetComponent<RectTransform>().position = new Vector3(posX, posY, 0.0f);

					ids.RemoveAt(idx);
					states.RemoveAt(idx);

					existingCarImageIds.Add ( currentId );

					continue;

				}


			}

			Debug.Log("Destroying carSensed");

			Destroy(carImages[i]);

			// Should DESTROY
			//GameObject newCarImage = Instantiate(carImagePrefab);

			//newCarImage.GetComponent<RectTransform>().position = new Vector3(posX, posY, 0.0f);
			//newCarImage.GetComponent<CarImageScript>().carId = currentId;


			existingCarImageIds.Add ( currentId );

		}

		// ids and states ArrayList now only contain the states that are not currently drawn
		for (int i = 0; i < ids.Count; i++) {
				


			Debug.Log("Creating new carSensed");

			float[] state = (float[]) states[i];
			
			float posX = carSensorCenterX;
			posX = posX + state[0];
			
			float posY = carSensorCenterY;
			posY = posY + state[1];
			//posY = 0.0f;


			float distanceToOther = Mathf.Sqrt( Mathf.Pow( state[0] - ownState[0] , 2) + Mathf.Pow( state[1] - ownState[1] , 2) );

			if ( distanceToOther < sensorRange ){

				float xDisplacement = (state[0] - ownState[0]);
				float yDisplacement = (state[1] - ownState[1]);

				float rotationAngleRadians = ownState[2]*Mathf.Deg2Rad;

				float xPixelDisplacement = xDisplacement*Mathf.Cos( rotationAngleRadians ) - yDisplacement*Mathf.Sin( rotationAngleRadians );
				float yPixelDisplacement = xDisplacement*Mathf.Sin( rotationAngleRadians ) + yDisplacement*Mathf.Cos( rotationAngleRadians );

				posX = carSensorCenterX + (xPixelDisplacement)*carSensorScaling;
				posY = carSensorCenterY + (yPixelDisplacement)*carSensorScaling;

				float yaw = 0.0f;
				yaw = yaw - state[2] + 90.0f + ownState[2];

				//posX = 250.0f;
				//posY = 250.0f;

				GameObject newCarImage;

				if ( (int) ids[i] == ownId ){
					
					newCarImage = Instantiate(ownCarImagePrefab);
					
				}else{

					newCarImage = Instantiate(carImagePrefab);

				}

				 

				newCarImage.GetComponentsInChildren<RectTransform>()[1].position = new Vector3(posX, posY, 0.0f);
				newCarImage.GetComponentsInChildren<RectTransform>()[1].rotation = Quaternion.Euler(0, 0, yaw);
				//newCarImage.GetComponent<RectTransform>().position = new Vector3(posX, posY, 0.0f);
				newCarImage.GetComponent<CarSensorScript>().vehicleId = (int) ids[i];

			}

			//newCarImage.GetComponent<RectTransform>().parent = GameObject.FindGameObjectWithTag("MonitoringSystem").GetComponent<Transform>();

			//GameObject monitoringSystem = GameObject.FindGameObjectWithTag("MonitoringSystem");
			//newCarImage.GetComponent<RectTransform>().parent = monitoringSystem.transform.GetComponentsInChildren();
			//Transform monitoringGroup = monitoringSystem.GetComponentInChildren(Transform);
			//newCarImage.GetComponent<RectTransform>().parent = monitoringGroup;

		}



		//for (  )

		//carImage.tag = "CarImage" + -1;
		//RectTransform carImageTransform = carImage.GetComponent<RectTransform> ();
			
		//carImageTransform.position = new Vector3 (200.0f, 200.0f, 0.0f);

	}
}
