﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FrameRateCounter : MonoBehaviour {

	public bool show;

	int frameCount = 0;
	float dt = 0.0f;
	float fps = 0.0f;
	float updateRate = 4.0f;

	public Text fpsShower;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		frameCount++;
		dt += Time.deltaTime;
		if (dt > 1.0f/updateRate)
		{
			fps = frameCount / dt ;
			frameCount = 0;
			dt -= 1.0f/updateRate;

			if ( show ){

				fpsShower.text = "FPS: " + fps;

			}else{

				fpsShower.text = "";

			}

		}


	}
}
