﻿using UnityEngine;
using System.Collections;

public class BotController : MonoBehaviour {

	public int botCarId;
	public float currentSteering;

	private float prevX, prevY, prevYaw, prevTime, newX, newY, newYaw, newTime;

	public float filteredLinearVelocity = 0.0f;
	public float filteredAngularVelocity = 0.0f;
	int numSamples = 2;

	bool firstTime = true;

	// Use this for initialization
	void Start () {

		prevX = 0;
		prevY = 0;
		prevYaw = 0;
		prevTime = 0;
		newX = 0;
		newY = 0;
		newYaw = 0;
		newTime = 0.001f;
	
	}
	
	// Update is called once per frame
	void Update () {
	
		float currentTime = Time.time;

		float a = (currentTime - prevTime)/(newTime - prevTime); // how close it is to newTime

		//Debug.Log ("urrentTime - prevTime = " + (nurrentTime - prevTime) );
		//Debug.Log ("newTime - prevTime = " + (newTime - prevTime) );

		if (a < 0.0f || a > 1.0f) {

			//Debug.Log ("a out of bounds: a = " + a);

		}

		float currentX = (1 - a) * prevX + a * newX;
		float currentY = (1 - a) * prevY + a * newY;
		float currentYaw = (1 - a) * prevYaw + a * newYaw;

		//Debug.Log ("prevTime = " + prevTime + " currentTime = " + currentTime );
		//Debug.Log ("currentX = " + currentX + " currentY = " + currentY + " currentYaw = " + currentYaw + " a = " + a);


		//Debug.Log ("currentX = " + currentX + " currentY = " + currentY + " currentYaw = " + currentYaw );

//		Vector3 currentPosition = GetComponent<Transform> ().position;
		Vector3 desiredPosition = new Vector3 (currentX, 0.0f, currentY);

//		if (Vector3.Distance (currentPosition, desiredPosition) > 10) {

		GetComponent<Transform> ().position = desiredPosition;
		
		GetComponent<Transform> ().eulerAngles = new Vector3 (0.0f, currentYaw, 0.0f);

//		}



//		if (firstTime) {
//
//			GetComponent<Transform> ().position = new Vector3 (currentX, 0.0f, currentY);
//
//			GetComponent<Transform> ().eulerAngles = new Vector3 (0.0f, currentYaw, 0.0f);
//
//			firstTime = false;
//
//		}

	}

	public void SetBotCarId(int argBotCarId){

		botCarId = argBotCarId;

	}

	public void UpdateBotCarState(Vector3 botCarPosition, float yawValue){

		prevX = newX;
		prevY = newY;
		prevYaw = newYaw;
		prevTime = newTime;

		newX = botCarPosition.x;
		newY = botCarPosition.z;
		newYaw = yawValue;
		newTime = Time.time;


		float yawDiff = newYaw - prevYaw;

		if (yawDiff > 180.0f) {

			yawDiff = yawDiff -360.0f;

		}
		if (yawDiff < -180.0f) {
			
			yawDiff = yawDiff + 360.0f;
			
		}

		float angularVelocity = yawDiff / (newTime - prevTime);
		angularVelocity = angularVelocity * Mathf.Deg2Rad;

		float linearVelocity = Mathf.Sqrt ( Mathf.Pow (newX - prevX, 2) + Mathf.Pow (newY - prevY, 2) );
		linearVelocity = linearVelocity / (newTime - prevTime);

		if (Mathf.Abs (linearVelocity) < (5.0f * 1000.0f / 3600.0f) ) {

			linearVelocity = 0.0f;
			angularVelocity = 0.0f;

		}

//		float filteredVelocity = 0.0f;
//		float filteredYaw = 0.0f;
//		int numSamples = 3;

		float newSampleWeight = 1.0f / ((float)numSamples);


		filteredLinearVelocity = newSampleWeight*linearVelocity + (1.0f-newSampleWeight)*filteredLinearVelocity;
		filteredAngularVelocity = newSampleWeight*angularVelocity + (1.0f-newSampleWeight)*filteredAngularVelocity;


		float axisLength = 1.5f;

		float steeringRadians;

		if ( filteredLinearVelocity == 0.0f ){

			steeringRadians = 0.0f;

		}else{

			steeringRadians = Mathf.Atan ((filteredAngularVelocity * axisLength) / filteredLinearVelocity);

		}

		currentSteering = steeringRadians * Mathf.Rad2Deg;
//			/ (newTime - prevTime);


		//currentSteering

		
//		float deltaX = newX - prevX;
//		float deltaY = newY - prevY;
//		float deltaYaw = newYaw - prevYaw;
//		float deltaTime = newTime - prevTime;
//
//		// USE TIME STAMPS FROM SIMULATOR MAYBE!!!!=====?????
//
//
//		GetComponent<Rigidbody> ().velocity = new Vector3(deltaX/deltaTime, 0.0f, deltaY/deltaTime);
		//GetComponent<Rigidbody> ().angularVelocity = new Vector3(0.0f, -deltaYaw/deltaTime, 0.0f);

		// GetComponent<Transform> ().position = botCarPosition;
		// GetComponent<Transform> ().eulerAngles = new Vector3 (0.0f, yawValue, 0.0f);

	}
}
