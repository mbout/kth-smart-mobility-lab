﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AxisDebuggerScript : MonoBehaviour {

	private Text debuggingText;

	// Use this for initialization
	void Start () {
	
		debuggingText = GameObject.FindGameObjectWithTag ("AxisDebugger").GetComponentInChildren<Text> ();

	}
	
	// Update is called once per frame
	void Update () {

		string debuggingString = "";

		float steering = Input.GetAxis ("Steering");
		debuggingString = debuggingString + "Steering = " + steering + '\n';

		float throttle = Input.GetAxis ("Throttle");
		debuggingString = debuggingString + "Throttle = " + throttle + '\n';

		float brake = Input.GetAxis ("Brake");
		debuggingString = debuggingString + "Brake = " + brake + '\n';

		float clutch = Input.GetAxis ("Clutch");
		debuggingString = debuggingString + "Clutch = " + clutch + '\n';

		float time = Time.time;
		debuggingString = debuggingString + "Time = " + time + '\n';

		debuggingText.text = debuggingString;
	
	}
}
