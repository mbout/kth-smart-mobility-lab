
timeStep = 0.1;
finalTime = 60;
time = 0:timeStep:finalTime;

numberLaps = 1;

xRoad = cos(numberLaps*time*((2*pi)/(finalTime)) );
yRoad = sin(numberLaps*time*((2*pi)/(finalTime)) );

squircleFactor = .5;

xRoad = sign(xRoad).*abs(xRoad.^squircleFactor);
yRoad = sign(yRoad).*abs(yRoad.^squircleFactor);

insideRoadFactor = 0.975;

figure; hold on;
% plot(xRoad,yRoad);
% plot(insideRoadFactor*xRoad,insideRoadFactor*yRoad);

xRoadOut = xRoad;
yRoadOut = yRoad;

xRoadIn = insideRoadFactor*xRoad;
yRoadIn = insideRoadFactor*yRoad;

plot(xRoadOut,yRoadOut);
plot(xRoadIn,yRoadIn);
