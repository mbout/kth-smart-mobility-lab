function [ xReference, yReference ] = getTruckReferenceOvertake(truckOrders, xRoadOut, yRoadOut, xRoadIn, yRoadIn, roadValue)
%GETTRUCKREFERENCE Summary of this function goes here
%   Detailed explanation goes here

    while truckOrders <= 0
        
        truckOrders = truckOrders +1;
        
    end
    
    index = round(truckOrders*length(xRoadOut));
    if index == 0
        index = 1;
    end
    
    xReference = xRoadOut(index)*roadValue + xRoadIn(index)*(1-roadValue);
    yReference = yRoadOut(index)*roadValue + yRoadIn(index)*(1-roadValue);
    
end

