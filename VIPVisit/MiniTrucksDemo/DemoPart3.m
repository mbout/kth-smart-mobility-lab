clear

timeStep = 0.1;
finalTime = 60;
time = 0:timeStep:finalTime;
numberLaps = 1;

squircleFactor = .5;
% [xRoadOut, yRoadOut] = getSquircle( squircleFactor , time, finalTime, numberLaps);
[xRoadOut, yRoadOut] = getSquircleNormalized( squircleFactor , time, finalTime, numberLaps);

timeEntrance = 0:timeStep:10;
xRoadEntrance = cos( (pi/2)-(pi/2)*timeEntrance/(timeEntrance(end)) ) - max(cos( (pi/2)-(pi/2)*timeEntrance/(timeEntrance(end)) )) ;
xRoadEntrance = 0.5*xRoadEntrance + min(xRoadOut);
yRoadEntrance = 0.5*sin( (pi/2)-(pi/2)*timeEntrance/(timeEntrance(end)) );

xRoadExit = cos( -(pi/2)*timeEntrance/(timeEntrance(end)) ) - max(cos( (pi/2)-(pi/2)*timeEntrance/(timeEntrance(end)) )) ;
xRoadExit = 0.5*xRoadExit + min(xRoadOut);
yRoadExit = 0.5*sin( -(pi/2)*timeEntrance/(timeEntrance(end)) );

insideRoadFactor = 0.9;
xRoadIn = insideRoadFactor*xRoadOut; yRoadIn = insideRoadFactor*yRoadOut;

platooningDistance = 0.05;


figure(); hold on;
set(gcf, 'position', [1921,433,1024,692])
width=6.18;
x_offset=-2.36;
height=4.4;
y_offset=-2.4;
axis([x_offset x_offset+width y_offset y_offset+height])

scaleX = 2.0;
scaleY = 2.0;
offsetX = 0.5;
offsetY = 0.0;

xRoadOut = scaleX*xRoadOut + offsetX;
yRoadOut = scaleY*yRoadOut + offsetY;
xRoadIn = scaleX*xRoadIn + offsetX;
yRoadIn = scaleY*yRoadIn + offsetY;
xRoadEntrance = scaleX*xRoadEntrance + offsetX;
yRoadEntrance = scaleY*yRoadEntrance + offsetY;
xRoadExit = scaleX*xRoadExit + offsetX;
yRoadExit = scaleY*yRoadExit + offsetY;

plot(xRoadOut,yRoadOut);
plot(xRoadIn,yRoadIn);
plot(xRoadEntrance,yRoadEntrance);
plot(xRoadExit,yRoadExit);


handleLeadTruck = plot( xRoadOut(1), yRoadOut(1) );
handleLeadFollowingTruck = plot( xRoadIn(1), yRoadIn(1) );


truck1Orders = time;
truck2Orders = time;
truck3Orders = time;

truck2Distance = ones(size(truck1Orders))*platooningDistance;

% tempIndex = round( length(truck2Distance)/4 );
% truck2Distance(1:tempIndex) = truck2Distance(1:tempIndex) +  [0.2:-0.2/( (tempIndex)-1 ):0] ;

initialDistance3 = 0.3;
truck3Distance = ones(size(truck1Orders))*2*platooningDistance;

% truck3Distance = 
timeToStartOpening = 15;
timeChanging = 5;
timeToFinishOpening = 40;

% figure; hold on;

openingIndexes = ceil( (timeToStartOpening)/timeStep ):floor( (timeToStartOpening+timeChanging)/timeStep );
openingDistance = openingIndexes - ceil( (timeToStartOpening)/timeStep );
openingDistance = openingDistance/( floor( (timeToStartOpening+timeChanging)/timeStep ) - ceil( (timeToStartOpening)/timeStep ) );
openingDistance = 2*openingDistance;
truck3Distance( openingIndexes ) = platooningDistance*(2+openingDistance);

% plot(truck3Distance,'k')

truck3Distance( ceil( (timeToStartOpening+timeChanging)/timeStep ):floor( (timeToFinishOpening-timeChanging)/timeStep ) ) = 4*platooningDistance;

% plot(truck3Distance,'g')

openingIndexes = ceil( (timeToFinishOpening-timeChanging)/timeStep ):floor( (timeToFinishOpening)/timeStep );
openingDistance = openingIndexes - ceil( (timeToFinishOpening-timeChanging)/timeStep );
openingDistance = openingDistance/( floor( (timeToFinishOpening)/timeStep) - ceil( (timeToFinishOpening-timeChanging)/timeStep ) );
openingDistance = 2*openingDistance;
truck3Distance(openingIndexes) = platooningDistance*(4 - openingDistance);

% plot(truck3Distance,'b')

timeToStartOpening = 27.5;
timeChanging = 2.5;
timeToFinishOpening = 33.85;

carOrders = time/time(end);
carOrders = carOrders*1.3;
carOrders = carOrders - 0.175;

carLane = zeros(size(carOrders));

openingIndexes = ceil( (timeToStartOpening)/timeStep ):floor( (timeToStartOpening+timeChanging)/timeStep );
openingDistance = openingIndexes - ceil( (timeToStartOpening)/timeStep );
openingDistance = openingDistance/( floor( (timeToStartOpening+timeChanging)/timeStep ) - ceil( (timeToStartOpening)/timeStep ) );
carLane( openingIndexes ) = openingDistance;

carLane( ceil( (timeToStartOpening+timeChanging)/timeStep ):floor( (timeToFinishOpening-timeChanging)/timeStep ) ) = 1;

openingIndexes = ceil( (timeToFinishOpening-timeChanging)/timeStep ):floor( (timeToFinishOpening)/timeStep );
openingDistance = openingIndexes - ceil( (timeToFinishOpening-timeChanging)/timeStep );
openingDistance = openingDistance/( floor( (timeToFinishOpening)/timeStep) - ceil( (timeToFinishOpening-timeChanging)/timeStep ) );
% carLane(openingIndexes) = 1+openingDistance;
carLane(openingIndexes) = 2;

carLane(openingIndexes(end):end)=2;

truck1Orders = time/time(end);
truck2Orders = truck1Orders - truck2Distance; 
truck3Orders = truck1Orders - truck3Distance;

carExitingIndex = ceil( (timeToFinishOpening-timeChanging)/timeStep );




% handleLeadTruck = plot( leadTruckReference(1), leadTruckReference(2), 'ro');
% handleLeadFollowingTruck = plot( followingTruckReference(1), followingTruckReference(2), 'go');
% handleThirdTruck = plot( thirdTruckReference(1), thirdTruckReference(2), 'ko');

% startingIndex = floor(length(time)*(1.2/3));
startingIndex = 1;

for i = startingIndex:length(time)
    
%     leadTruckReference = [xRoadOut(i) yRoadOut(i)];
%     followingTruckReference = [xRoadIn(i) yRoadIn(i)];

%     leadTruckReference = getTruckReference(truck1Orders, roadValue);
    [ xReference, yReference ] = getTruckReferenceOvertake(truck1Orders(i), xRoadOut, yRoadOut, xRoadIn, yRoadIn, 1);
    leadTruckReference = [ xReference, yReference ];
    xReferenceTruck1(i) = xReference; yReferenceTruck1(i) = yReference;
    
    [ xReference, yReference ] = getTruckReferenceOvertake(truck2Orders(i), xRoadOut, yRoadOut, xRoadIn, yRoadIn, 1);
    followingTruckReference = [ xReference, yReference ];
    xReferenceTruck2(i) = xReference; yReferenceTruck2(i) = yReference;
    
    [ xReference, yReference ] = getTruckReferenceOvertake(truck3Orders(i), xRoadOut, yRoadOut, xRoadIn, yRoadIn, 1);
    thirdTruckReference = [ xReference, yReference ];
    xReferenceTruck3(i) = xReference; yReferenceTruck3(i) = yReference;
    
    
%     disp(i)
    
    [ xReference, yReference ] = getCarReference(carOrders(i), xRoadOut, yRoadOut, xRoadIn, yRoadIn, xRoadExit, yRoadExit, carLane(i), i-carExitingIndex+1);
    carReference = [ xReference, yReference ];
    
%     thirdTruckReference = [ truck3ReferenceX(i), truck3ReferenceY(i) ];
%     truckDistance = 10;
%     if i-truckDistance > 0
%         followingTruckReference = [xRoadIn(i-truckDistance) yRoadIn(i-truckDistance)];
%     else
%         followingTruckReference = [xRoadIn(1) yRoadIn(1)];
%     end

    if (i>startingIndex)

        delete(handleLeadTruck);
        delete(handleLeadFollowingTruck);
        delete(handleThirdTruck);
        delete(handleCar);
    
    end
    
    handleLeadTruck = plot( leadTruckReference(1), leadTruckReference(2), 'ro');
    handleLeadFollowingTruck = plot( followingTruckReference(1), followingTruckReference(2), 'go');
    handleThirdTruck = plot( thirdTruckReference(1), thirdTruckReference(2), 'ko');
    handleCar = plot( carReference(1), carReference(2), 'kx');
    
    pause(timeStep);
    
end

% save('demo3References','xReferenceTruck1','xReferenceTruck1','xReferenceTruck2','yReferenceTruck2','xReferenceTruck3','yReferenceTruck3')

