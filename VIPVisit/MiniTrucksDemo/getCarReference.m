function [ xReference, yReference ] = getCarReference(truckOrders, xRoadOut, yRoadOut, xRoadIn, yRoadIn, xRoadExit, yRoadExit, roadValue, exitIndex)
%GETTRUCKREFERENCE Summary of this function goes here
%   Detailed explanation goes here

    while truckOrders <= 0
        
        truckOrders = truckOrders +1;
        
    end
    
    index = round(truckOrders*length(xRoadOut));
    if index == 0
        index = 1;
    end
    
    if (roadValue <= 1)
    
        xReference = xRoadOut(index)*roadValue + xRoadIn(index)*(1-roadValue);
        yReference = yRoadOut(index)*roadValue + yRoadIn(index)*(1-roadValue);
        
    else
        
        roadValue = roadValue-1;
        
        if exitIndex < length(xRoadExit)
        
            xReference = xRoadExit(exitIndex)*roadValue + xRoadOut(index)*(1-roadValue);
            yReference = yRoadExit(exitIndex)*roadValue + yRoadOut(index)*(1-roadValue);
            
        else
           
            xReference = xRoadExit(end)*roadValue;
            yReference = yRoadExit(end)*roadValue;
            
        end
%         disp(index-carExitingIndex)
    

        
    end
        
    
end

