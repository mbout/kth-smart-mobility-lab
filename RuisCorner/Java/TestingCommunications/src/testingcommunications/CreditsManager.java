/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testingcommunications;

import java.util.ArrayList;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.ParallelTransition;
import javafx.animation.Timeline;
import javafx.event.EventHandler;
import javafx.geometry.Point3D;
import javafx.geometry.VPos;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextBoundsType;
import javafx.util.Duration;

/**
 *
 * @author rui
 */
public class CreditsManager {
    
    double creditsWidth;
    double creditsHeight;
    public Group creditsGroup;
    
    public CreditsManager(double argWidth, double argHeight){
        
        creditsWidth = argWidth;
        creditsHeight = argHeight;
        
        createCreditsGroup();
                
    }
    
    private void createCreditsGroup(){
        
        creditsGroup = new Group();
        
        creditsGroup.getChildren().add( getCreditsBackground() );
        
        double desiredWidth = 0.5*creditsWidth;
        double desiredHeight = 0.8*creditsHeight;
        
        Group logoGroup = getLogoGroup(desiredWidth, desiredHeight);
        
        double realWidth = logoGroup.getBoundsInLocal().getWidth();
        double realHeight = logoGroup.getBoundsInLocal().getHeight();
        
        logoGroup.setLayoutX( desiredWidth/2.0 - realWidth/2.0 );
        logoGroup.setLayoutY( creditsHeight/2.0 - realHeight/2.0 ); 
        
        creditsGroup.getChildren().add( logoGroup );
        
        double textWidth = creditsWidth - desiredWidth;
        double textHeight = 0.8*creditsHeight;
        
        Group textGroup = getTextGroup(textWidth, textHeight);
        
        realWidth = textGroup.getBoundsInLocal().getWidth();
        realHeight = textGroup.getBoundsInLocal().getHeight();
        
        textGroup.setLayoutX( creditsWidth - textWidth/2.0 - realWidth/2.0 );
        textGroup.setLayoutY( creditsHeight/2.0 - realHeight/2.0 ); 
        
        creditsGroup.getChildren().add( textGroup );
                
        setUpNetworkVisualisationSwitch(creditsGroup);
        
    }
    
    private void setUpNetworkVisualisationSwitch(Group argGroup){
        
        if ( Globals.networkVisualisationSwitchIsAvailable ){
        
            argGroup.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent me) {
                    System.out.println("Switching network visualisation to " + !Globals.drawingManager.worldViewManager.networkVisualisationOn);
                    Globals.drawingManager.worldViewManager.networkVisualisationOn = 
                            !Globals.drawingManager.worldViewManager.networkVisualisationOn;
                }
            });
        
        }
        
    }
    
    Group getCreditsBackground(){
        
        Group creditsBackgroundGroup = new Group();
        
        Rectangle creditsBackgroundRect = new Rectangle(creditsWidth, creditsHeight);
        
        creditsBackgroundRect.setFill(Color.BLACK);
        
        creditsBackgroundGroup.getChildren().add(creditsBackgroundRect);
        
        return creditsBackgroundGroup;
        
    }
    
    Group getTextGroup(double desiredWidth, double desiredHeight){
        
        Group textGroup = new Group();
        
        String creditsString = "SML World v0.7\n\nCreated by:\n\nPedro Alvito\nRui Oliveira\n\nSmart Mobility Lab 2015";
        String testString = "A";
        
        Text creditsText = new Text();
        creditsText.setText(creditsString);
        creditsText.setTextAlignment(TextAlignment.CENTER);
        creditsText.setFill(Color.WHITE);
        
        creditsText.setBoundsType(TextBoundsType.VISUAL);
        
        double safetyWidth = 0.95*desiredWidth;
        double safetyHeight = 0.95*desiredHeight;
        
        int fontSize = 1;
        double lineHeight = 0;
        for ( int i = 1 ; i < 100; i++){
            
            creditsText.setFont( Font.font("Verdana", FontWeight.BOLD, i) );
            
            if ( creditsText.getBoundsInLocal().getWidth() > safetyWidth || creditsText.getBoundsInLocal().getHeight() > safetyHeight ){
                
                break;
                
            }
            
            creditsText.setText(testString);            
            lineHeight = creditsText.getBoundsInLocal().getHeight();
            creditsText.setText(creditsString);
            fontSize = i;
                        
        }
        
        creditsText.setFont( Font.font("Verdana", FontWeight.BOLD, fontSize) );
        
        VPos vpost = creditsText.getTextOrigin();
        
        creditsText.setLayoutY( lineHeight );
                
        textGroup.getChildren().add( creditsText );
        
        return textGroup;
        
    }
    
    Group getLogoGroup(double desiredWidth, double desiredHeight){
        
        Group logoGroup = new Group();
        
        double requestedWidth = desiredWidth;
        double requestedHeight = desiredHeight;
        
        if ( requestedWidth > requestedHeight ){
            
            requestedWidth = requestedHeight;
            
        }else{
            
            requestedHeight = requestedWidth;
            
        }
        
        boolean preserveRatio = false;
        boolean smooth = true;
                
        Image kthLogoImage = new Image("file:kthLogoCropped2.png", requestedWidth, requestedHeight, preserveRatio, smooth);
//        Image scaniaLogoImage = new Image("file:scaniaLogoCropped.png", requestedWidth, requestedHeight, preserveRatio, smooth);
//        Image ericssonLogoImage = new Image("file:ericssonLogoCropped.png", requestedWidth, requestedHeight, preserveRatio, smooth);
        
        ImageView kthLogoImageView = new ImageView(kthLogoImage);
//        ImageView scaniaLogoImageView = new ImageView(scaniaLogoImage);
//        ImageView ericssonLogoImageView = new ImageView(ericssonLogoImage);
                
        ArrayList<ImageView> logosToDisplay = new ArrayList<>();
        logosToDisplay.add(kthLogoImageView);
//        logosToDisplay.add(scaniaLogoImageView);
//        logosToDisplay.add(ericssonLogoImageView);
        
        setLogoAnimation(logosToDisplay);
        
//        logoGroup.getChildren().addAll(scaniaLogoImageView, ericssonLogoImageView, kthLogoImageView);
        logoGroup.getChildren().addAll(kthLogoImageView);
        
        return logoGroup;
        
    }
    
    void setLogoAnimation(ArrayList<ImageView> logosToDisplay){
        
        double rotationPeriodMillisPerLogo = 5000;
        double rotationPeriodMillis = rotationPeriodMillisPerLogo*logosToDisplay.size();
        
        
        
        
        Timeline timeline = new Timeline();
        
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.setAutoReverse(false);
        
//        timeline.
        Interpolator interpolatorType = Interpolator.DISCRETE;
        Interpolator rotationInterpolatorType = Interpolator.LINEAR;
        
        Point3D rotationAxis = new Point3D(0, 1, 0);
        
        for ( ImageView imageView : logosToDisplay ){
            
            imageView.setOpacity(0.0);
            imageView.setRotationAxis(rotationAxis);
            
        }
        
        KeyValue kv;
        KeyFrame kf;
                
        for ( int i = 0 ; i < logosToDisplay.size() ; i++ ){
            
            ImageView currentImageView = logosToDisplay.get(i);
            
            double startTime = ( (double) i)/( (double) logosToDisplay.size() );
            double endTime = ( (double) i+1)/( (double) logosToDisplay.size() );
            
            System.out.println("startTime = " + startTime);
            System.out.println("endTime = " + endTime);
            
            kv = new KeyValue(currentImageView.rotateProperty() , -90.0, rotationInterpolatorType);
            kf = new KeyFrame(Duration.millis( rotationPeriodMillis*startTime ), kv);
            timeline.getKeyFrames().add(kf);


            kv = new KeyValue(currentImageView.opacityProperty(), 1.0, interpolatorType);
            kf = new KeyFrame(Duration.millis( rotationPeriodMillis*startTime ), kv);
            timeline.getKeyFrames().add(kf);


            kv = new KeyValue(currentImageView.opacityProperty(), 0.0, interpolatorType);
            kf = new KeyFrame(Duration.millis( rotationPeriodMillis*endTime ), kv);
            timeline.getKeyFrames().add(kf);

            kv = new KeyValue(currentImageView.rotateProperty() , 90.0, rotationInterpolatorType);
            kf = new KeyFrame(Duration.millis( rotationPeriodMillis*endTime ), kv);
            timeline.getKeyFrames().add(kf);
//        
        }
        
        
//        kv = new KeyValue(scaniaLogoImageView.rotateProperty() , 90.0, rotationInterpolatorType);
//        kf = new KeyFrame(Duration.millis( rotationPeriodMillis*(0./6.) ), kv);
//        timeline.getKeyFrames().add(kf);
//        
//        kv = new KeyValue(kthLogoImageView.rotateProperty() , -90.0, rotationInterpolatorType);
//        kf = new KeyFrame(Duration.millis( rotationPeriodMillis*(0./6.) ), kv);
//        timeline.getKeyFrames().add(kf);
//        
//        
//        kv = new KeyValue(scaniaLogoImageView.opacityProperty(), 1.0, interpolatorType);
//        kf = new KeyFrame(Duration.millis( rotationPeriodMillis*(2./6.) ), kv);
//        timeline.getKeyFrames().add(kf);
//        
//        kv = new KeyValue(kthLogoImageView.opacityProperty(), 0.0, interpolatorType);
//        kf = new KeyFrame(Duration.millis( rotationPeriodMillis*(2./6.) ), kv);
//        timeline.getKeyFrames().add(kf);
//        
//        kv = new KeyValue(scaniaLogoImageView.opacityProperty(), 1.0, interpolatorType);
//        kf = new KeyFrame(Duration.millis( rotationPeriodMillis*(2./6.) ), kv);
//        timeline.getKeyFrames().add(kf);
//        
//        kv = new KeyValue(kthLogoImageView.opacityProperty(), 1.0, interpolatorType);
//        kf = new KeyFrame(Duration.millis( rotationPeriodMillis*(4./6.) ), kv);
//        timeline.getKeyFrames().add(kf);
//        
//        
//        kv = new KeyValue(scaniaLogoImageView.rotateProperty() , 450.0, rotationInterpolatorType);
//        kf = new KeyFrame(Duration.millis( rotationPeriodMillis*(4./6.) ), kv);
//        timeline.getKeyFrames().add(kf);
//        
//        kv = new KeyValue(kthLogoImageView.rotateProperty() , 270.0, rotationInterpolatorType);
//        kf = new KeyFrame(Duration.millis( rotationPeriodMillis*(4./6.) ), kv);
//        timeline.getKeyFrames().add(kf);
        
        timeline.play();
        
        
    }
    
}
