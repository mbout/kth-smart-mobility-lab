/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testingcommunications;

import com.sun.javaws.jnl.InformationDesc;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.paint.Color;

/**
 *
 * @author rui
 */
class DestinationInformation{
    
    String destinationName;
    String destinationDescription;
    
    ArrayList<OsmNode> destinationNodesList;
    
    DestinationInformation(String argDestinationName, String argDestinationDescription, ArrayList<OsmNode> argDestinationNodesList){
        
        destinationName = argDestinationName;
        destinationDescription = argDestinationDescription;
        destinationNodesList = argDestinationNodesList;
        
    }
        
}


class MissionInformation{
    
    String missionName;
    // Go to A
    // Go to B
    // Load
    // Deliver
        
    String missionState;
    // Queue, Progress
    // Null when initialized
    
    String detailedInformation;
    // Waiting for a truck to have cargo, waiting for a truck to be free
    // Null when initialized
    
    int missionId;
    // In order to be able to identify
    // Must be defined at initialization
    
    int assignedTruck;
    // Initialized as Integer.MAX_VALUE
    
    static int maxMissionId = 0;
        
    
    MissionInformation(String argMissionName, int argMissionId){
        
        missionName = argMissionName;
        missionState = null;
        detailedInformation = null;
        missionId = argMissionId;
        assignedTruck = Integer.MAX_VALUE;
        
    }
    
    
}

class TruckMissionInformation{
        
    int truckId;
    boolean controllability;
    boolean controllabilityChange;
    
    String truckState;
    String truckPosition;
    
    OsmNode truckOccupancyNode;
    
    ArrayList<OsmLanelet> truckTrajectoryLanelets;
        
    boolean hasCargo;
    
    TruckMissionInformation(int argTruckId, boolean argControllability, boolean argControllabilityChange,
            String argTruckState, String argTruckPosition, ArrayList<OsmLanelet> argTruckTrajectoryLanelets,
            OsmNode argTruckOccupancyNode){
        
        truckId = argTruckId;
        
        controllability = argControllability;
        controllabilityChange = argControllabilityChange;     

        truckState = argTruckState;
        truckPosition = argTruckPosition;
                    
                    
        truckTrajectoryLanelets = argTruckTrajectoryLanelets;
                    
        truckOccupancyNode = argTruckOccupancyNode;
        
        hasCargo = false;
        
    }
            
    
}

public class MissionManager {
    
    ArrayList<MissionInformation> missionQueue = new ArrayList<>();
    ArrayList<TruckMissionInformation> truckInformationList = new ArrayList<>();
    
    ArrayList<Integer> currentlyObstructedLaneletIds = new ArrayList<>();
    ArrayList<Integer> newlyObstructedLaneletIds = new ArrayList<>();
    
    // State String Constants
    String undefinedString = "Undefined";
    String availableString = "Available";
    String emergencyStopString = "EmergeGoingToAncyStop";
    
    String onMissionToLoadEntranceString = "GoingToLoadEntrance";
    String onMissionToLoadParkingString = "GoingToLoadParking";
    String onMissionToLoadExitString = "GoingToLoadExit";
    String onMissionToUnloadString = "GoingToUnload";
    String onMissionToParkString = "GoingToPark";
    
    // Position String Constants
    String onLoadEntranceString = "Load Entrance";
    String onLoadParkingString = "Load Parking";
    String onLoadExitString = "Load Exit";
    String onUnloadString = "Unload";
    String onParkString = "Park";
    
//    ArrayList<OsmNode> loadNodes = new ArrayList<>();
//    ArrayList<OsmNode> deliverNodes = new ArrayList<>();
//    ArrayList<OsmNode> freeSpaceNodes = new ArrayList<>();
    
    ArrayList<DestinationInformation> destinationInformationList = new ArrayList<>();
    
    int loadPileId;
    int unloadPileId;
    double loadPileSize;
    double unloadPileSize;
    double[] loadPilePosition;
    double[] unloadPilePosition;
    double pileDelta;
    
    
    boolean recheckNeeded = false;
    
    OsmNode lastAskedDestinationNode = null;
        
    /**
     *
     * @param argTruckIds
     */
    public MissionManager(int[] argTruckIds){
               
        loadPileId = Globals.loadPileId;
        unloadPileId = Globals.unloadPileId;;
        loadPileSize = 0.3;
        unloadPileSize = 0.1;
        loadPilePosition = new double[2];
        loadPilePosition[0] = -0.3; loadPilePosition[1] = -0.3;
        unloadPilePosition = new double[2];
        unloadPilePosition[0] = 1.3; unloadPilePosition[1] = 0.3;
        pileDelta = 0.05;
        
        for ( int i = 0; i < argTruckIds.length; i++ ){
            
            int truckId = argTruckIds[i];
            boolean initialControllability = false;
            boolean initialControllabilityChange = false;
            
            String initialTruckState = availableString;
            String initialTruckPosition = onParkString;
            ArrayList<OsmLanelet> initialTruckTrajectoryLanelets = new ArrayList<OsmLanelet>();
            OsmNode initialTruckOccupancyNode = null;
            
            TruckMissionInformation truckMissionInformation = new TruckMissionInformation(
                    truckId, initialControllability, initialControllabilityChange,
            initialTruckState, initialTruckPosition, initialTruckTrajectoryLanelets,
            initialTruckOccupancyNode);
                    
            truckInformationList.add(truckMissionInformation);
            
        }
        
        processDestinations();
        
    }
        
    public boolean addMissionToQueue(String missionType){
        
        boolean success = false;
        
        int currentMissionId = MissionInformation.maxMissionId + 1;
        
        if (true){
            
            MissionInformation missionToAdd = new MissionInformation(missionType, currentMissionId);
            missionToAdd.missionState = "To be processed";
            missionQueue.add(missionToAdd);
            MissionInformation.maxMissionId++;
            Globals.drawingManager.actionsManager.missionGuiManager.updateMissionQueue(missionToAdd);
            success = true;
            return success;
            
        }
        
//        {
//        
//        missionName = argMissionName;
//        missionState = null;
//        detailedInformation = null;
//        missionId = argMissionId;
//        assignedTruck = Integer.MAX_VALUE;
//        
//    }
        
        return success;        
        
    }
    
    
    /**
     *
     * @return
     */
    public ArrayList<String> getControllabilityChangesLogMessages(){
        
        ArrayList<String> logMessages = new ArrayList<>();
        
        for ( TruckMissionInformation truckMissionInformation : truckInformationList ){
            
            if ( truckMissionInformation.controllabilityChange ){
                
                int truckId = truckMissionInformation.truckId;
                String logMessage;
                
                if ( truckMissionInformation.controllability ){
                    
                    logMessage = "Truck " + truckId + " has connected";
                    
                }else{
                    
                    logMessage = "Truck " + truckId + " has disconnected";
                    
                }
                
                logMessages.add(logMessage);
                
            }
            
            
        }
        
        return logMessages;
        
    }
    
    /**
     *
     * @return
     */
    public boolean checkTruckControllability(){
        
        boolean changeOccured = false;
        
        ArrayList<Integer> truckIds = new ArrayList<>();
        ArrayList<Boolean> truckControllabilities = new ArrayList<>();
        for ( TruckMissionInformation truckMissionInformation : truckInformationList ){
            truckIds.add(truckMissionInformation.truckId);
            truckControllabilities.add(truckMissionInformation.controllability);
        }
        
        for ( QualisysBody body : Globals.smlWorldInfo.getQualisysBodiesList() ){
            
            //Is bodyId in the truck Ids?
            if ( truckIds.contains( body.getId() ) ){
                
                int truckIdx = getTruckIndexFromId( body.getId() );
                boolean controllable = truckControllabilities.get( truckIdx );
                
                if ( controllable != body.isControllable() ){
                                        
                    truckInformationList.get(truckIdx).controllability = body.isControllable();
                    
                    if ( body.isControllable() ){
                           
                        truckInformationList.get(truckIdx).truckOccupancyNode = getTruckClosestLocation( body );
                        
                    }else{
                        
                        truckInformationList.get(truckIdx).truckOccupancyNode = null;
                        
                    }
                    
                    truckInformationList.get(truckIdx).controllabilityChange = true;
                    changeOccured = true;
                    
                }
                                
            }
            
        }
        
        return changeOccured;
        
    }
    
    public void initializeTruckLocationsToNearest(){
        
        double bestDistance = Double.MAX_VALUE;
        String bestDestinationString = "";
        
        for ( TruckMissionInformation truckInfo : truckInformationList ){
            
            QualisysBody currentBody = null;
            
            for ( QualisysBody tempBody : Globals.xmlProcesser.qualisysBodiesList ){
            
                if ( tempBody.getId() == truckInfo.truckId ){
                    
                    currentBody = tempBody;
                    
                }
                
            }
            
            if ( currentBody == null ){
                
                continue;
                
            }
            
            for ( DestinationInformation destInfo : destinationInformationList ){

                for ( OsmNode osmNode : destInfo.destinationNodesList ){

                    double distanceToNode = Math.hypot(osmNode.X/32. - currentBody.getX(),
                            osmNode.Y/32. - currentBody.getY());

                    if ( distanceToNode < bestDistance ){
                        
                        bestDistance = distanceToNode;
                        bestDestinationString = destInfo.destinationName;
                    }
                    
                }

            }
            
            if ( !truckInfo.equals("") ){
            
                truckInfo.truckPosition = bestDestinationString;
            
            }
            
        }
        
        
        
        
    }
    
    private OsmNode getTruckClosestLocation(QualisysBody body){
        
        OsmNode closestNode = null;
        float bestDistance = Float.MAX_VALUE;
        
        ArrayList< OsmNode > listDestinationNodesList = new ArrayList<>();
        
        for ( DestinationInformation destinationInformation : destinationInformationList ){
            
            for ( OsmNode osmNode : destinationInformation.destinationNodesList ){
                
                listDestinationNodesList.add(osmNode);
                
            }
            
        }
                
            
        for ( OsmNode currentNode : listDestinationNodesList ){
            
            float currentDistance = (float) Math.hypot( currentNode.X - body.getX()*32.0 , currentNode.Y - body.getY()*32.0 );

            if ( currentDistance < bestDistance ){

                closestNode = currentNode;
                bestDistance = currentDistance;

            }

        }
        
        return closestNode;
        
    }
    
    /**
     *
     * @return
     */
    public ArrayList<Integer> getTruckOccupancyAsIds(){
       
        ArrayList<Integer> truckOccupancyIds = new ArrayList<>();
        
        ArrayList<OsmNode> truckOccupancyNodes = new ArrayList<>();
        
        for ( TruckMissionInformation truckMissionInformation : truckInformationList ){
            truckOccupancyNodes.add(truckMissionInformation.truckOccupancyNode);
        }
        
        
        for ( OsmNode truckOccupancyNode : truckOccupancyNodes ){
            
            truckOccupancyIds.add(truckOccupancyNode.id);
            
        }
        
        return truckOccupancyIds;
        
    }
    
    /**
     *
     */
    public void resetControllabilityChange(){
          
        for (TruckMissionInformation truckMissionInformation : truckInformationList ){
            
            truckMissionInformation.controllabilityChange = false;
            
        }
                        
    }
    
    private void processDestinations(){
        
        List<OsmNode> tempNodeList = Globals.xmlProcesser.nodeList;
        
        for ( OsmNode tempNode : tempNodeList ){
            
            if ( tempNode.destination != null){
                

                if ( tempNode.destination.equals("park") ){
                                        
                    String destinationName = "Park";
                    String destinationDescription = "The parking lot where the trucks are kept.";
                    
                    addNodeToDestinationInformationList(tempNode, destinationName, destinationDescription);
                    
                    
                }else if ( tempNode.destination.equals("unload") ){
                                        
                    String destinationName = "Unload";
                    String destinationDescription = "Area where trucks can unload their cargo.";
                    
                    addNodeToDestinationInformationList(tempNode, destinationName, destinationDescription);
                    
                }else if ( tempNode.destination.equals("load_exit") ){
                    
                    String destinationName = "Load Exit";
                    String destinationDescription = "Exit from Loading Area into the road network.";
                    
                    addNodeToDestinationInformationList(tempNode, destinationName, destinationDescription);
                    
                }else if ( tempNode.destination.equals("load_entrance") ){
                    
                    String destinationName = "Load Entrance";
                    String destinationDescription = "Entrance of Loading area from the road network.";
                    
                    addNodeToDestinationInformationList(tempNode, destinationName, destinationDescription);
                    
                }else{
                    
                    System.err.println("Unknown destination discovered!!! Please fix!!!");
                    
                }
                
            }
            
        }
        
    }
    
    private void addNodeToDestinationInformationList(OsmNode newOsmNode, String destinationName, String destinationDescription){
                
        for ( DestinationInformation destinationInformation : destinationInformationList){
            
            if ( destinationInformation.destinationName.equals(destinationName) ){
                                
                destinationInformation.destinationNodesList.add(newOsmNode);
                
                return;
                
            }
            
        }
        
        ArrayList<OsmNode> destinationOsmNodeList = new ArrayList<>();
        destinationOsmNodeList.add(newOsmNode);
        
        DestinationInformation newDestinationInformation = new DestinationInformation(destinationName, destinationDescription, destinationOsmNodeList);
                        
        destinationInformationList.add(newDestinationInformation);
        
    }
    
    private ArrayList<OsmNode> getOsmNodesListFromDestinationName(String destinationName){
              
        System.out.println("getOsmNodesListFromDestinationName()");
        
        for ( DestinationInformation destinationInformation : destinationInformationList){

            if ( destinationInformation.destinationName.equals(destinationName) ){
                
                System.out.println("getOsmNodesListFromDestinationName() FOUND suitable destinationName = " + destinationName);
                
                return destinationInformation.destinationNodesList;

            }

        }
       
        System.out.println("getOsmNodesListFromDestinationName() did not find suitable for destinationName = " + destinationName);
        
        ArrayList<OsmNode> destinationNodes = new ArrayList<>();

        return destinationNodes;
        
    }
        
    /**
     *
     * @return
     */
    public ArrayList<String> getTruckStates(){
        
        ArrayList<String> truckStates = new ArrayList<>();
        
        for ( TruckMissionInformation truckMissionInformation : truckInformationList ){
            truckStates.add(truckMissionInformation.truckState);
        }
        
        return truckStates;
        
    }
    
    /**
     *
     * @return
     */
    public ArrayList<String> getTruckPositions(){
        
        ArrayList<String> truckPositions = new ArrayList<>();
        
        for ( TruckMissionInformation truckMissionInformation : truckInformationList ){
            truckPositions.add(truckMissionInformation.truckPosition);
        }
        
        return truckPositions;
        
    }
    
    /**
     *
     * @return
     */
    public String getTruckPosition(int truckId){
        
        String truckPosition = "";
        
        for ( TruckMissionInformation truckMissionInformation : truckInformationList ){
            
            if (truckMissionInformation.truckId == truckId){
            
                truckPosition = truckMissionInformation.truckPosition;
            
            }
        }
        
        return truckPosition;
        
    }
    
    /**
     *
     * @param argTruckId
     */
    public void sendTruckToPark(int argTruckId){
        
        sendTruckToPosition(onMissionToParkString, argTruckId);
        
     
        int truckIdx = getTruckIndexFromId(argTruckId);
        
        truckInformationList.get( getTruckIndexFromId(argTruckId) ).truckOccupancyNode = lastAskedDestinationNode;
        
    }
    
    /**
     *
     * @param argTruckId
     */
    public void sendTruckToUnload(int argTruckId){
        
        sendTruckToPosition(onMissionToUnloadString, argTruckId);
        
        truckInformationList.get( getTruckIndexFromId(argTruckId) ).truckOccupancyNode = lastAskedDestinationNode;
        
    }
        /**
     *
     * @param argTruckId
     */
    public void sendTruckToLoadEntrance(int argTruckId){
        
        sendTruckToPosition(onMissionToLoadEntranceString, argTruckId);
        
        truckInformationList.get( getTruckIndexFromId(argTruckId) ).truckOccupancyNode = lastAskedDestinationNode;
                
    }
    
    public void sendTruckToLoadExit(int argTruckId){
        
        sendTruckToPosition(onMissionToLoadExitString, argTruckId);
        
        truckInformationList.get( getTruckIndexFromId(argTruckId) ).truckOccupancyNode = lastAskedDestinationNode;
                
    }
        /**
     *
     * @param argTruckId
     */
    public void sendTruckToLoadParking(int argTruckId){
        
        sendTruckToPosition(onMissionToLoadParkingString, argTruckId);
        
        truckInformationList.get( getTruckIndexFromId(argTruckId) ).truckOccupancyNode = lastAskedDestinationNode;
                
    }
    
    /**
     *
     * @param argTruckId
     */
//    public void sendTruckToFreeArea(int argTruckId){
//        
//        sendTruckToPosition(onMissionToFreeAreaString, argTruckId);
//        
//        System.out.println("sendTruckToFreeArea, is setting the truckOccupancyNode to 99, should fix!");
//        truckInformationList.get( getTruckIndexFromId(argTruckId) ).truckOccupancyNode = null;
//        
//    }
    
    private void sendTruckToPosition(String argPosition, int argTruckId){
        
        int i = getTruckIndexFromId(argTruckId);
        
        truckInformationList.get(i).truckState = argPosition;
        
        // Add new mission to queue
                
//        String missionName = "Move to " + argPosition;
                    
//        addMissionToQueue(missionName);
//        MissionInformation missionToAdd = new MissionInformation(argPosition, argTruckId)
                
//                MissionInformation(String argMissionName, int argMissionId){
//        
//        missionName = argMissionName;
//        missionState = null;
//        detailedInformation = null;
//        missionId = argMissionId;
//        assignedTruck = Integer.MAX_VALUE;
//        
//    }
                
//                missionToAdd.missionState = "To be processed";
//    missionQueue.add(missionToAdd);
        
    }
    
    /**
     *
     * @param argTruckId
     */
    public void setTruckArrival(int argTruckId){
        
        clearTruckLaneletPath(argTruckId);
        
        ArrayList<String> truckStates = new ArrayList<>();
        
        for ( TruckMissionInformation truckMissionInformation : truckInformationList ){
            truckStates.add(truckMissionInformation.truckState);
        }
        
        int i = getTruckIndexFromId(argTruckId);
                
        if ( truckStates.get(i).equals(onMissionToParkString) ){

            truckInformationList.get(i).truckPosition = onParkString;

        }else if( truckStates.get(i).equals(onMissionToUnloadString) ){

            truckInformationList.get(i).truckPosition = onUnloadString;

        }else if( truckStates.get(i).equals(onMissionToLoadEntranceString) ){

            truckInformationList.get(i).truckPosition = onLoadEntranceString;

        }else if( truckStates.get(i).equals(onMissionToLoadParkingString) ){

            truckInformationList.get(i).truckPosition = onLoadParkingString;

        }else if( truckStates.get(i).equals(onMissionToLoadExitString) ){

            truckInformationList.get(i).truckPosition = onLoadExitString;

        }
//        else if( truckStates.get(i).equals(onMissionToFreeAreaExitString) ){
//
//            truckInformationList.get(i).truckPosition = onFreeAreaExitString;
//
//        }

        truckInformationList.get(i).truckState = availableString;
                                
    }
    
    /**
     *
     * @param bodiesToStop
     */
    public void setTruckEmergencyStop(int[] bodiesToStop){
        
        for ( int i = 0; i < bodiesToStop.length ; i++ ){
            
            int truckId = bodiesToStop[i];
            
            clearTruckLaneletPath(truckId);
        
            int truckIndex = getTruckIndexFromId(truckId);

            truckInformationList.get(truckIndex).truckState = emergencyStopString;
            
        }
        
    }
    
    public boolean isLoadAreaAvailable(){
                
        for ( TruckMissionInformation truckMissionInfo : truckInformationList ){
            
            String currentTruckPositionString = truckMissionInfo.truckPosition;
            
            if ( currentTruckPositionString.equals(onLoadEntranceString) ||
                 currentTruckPositionString.equals(onLoadParkingString) ){
                
                return false;
                
            }
            
        }
        
        return true;
        
    }
    
    public void setTruckCargo(int argTruckId, boolean cargo){
        
        int i = getTruckIndexFromId(argTruckId);
     
        truckInformationList.get(i).hasCargo = cargo;
        
    }
        
    public boolean hasTruckCargo(int argTruckId){
        
        int i = getTruckIndexFromId(argTruckId);
     
        return truckInformationList.get(i).hasCargo;
        
    }
    
    public void changePileSize(double deltaSize){
        
        if ( deltaSize > 0.0 ){
                                        
            changePileSize(unloadPileId, unloadPileSize, deltaSize, unloadPilePosition);
            unloadPileSize = unloadPileSize + deltaSize;
            
        }else{
            
            changePileSize(loadPileId, loadPileSize, deltaSize, loadPilePosition);
            loadPileSize = loadPileSize + deltaSize;
            
        }
        
    }
    
    public void changePileSize(int pileId, double pileSize, double deltaSize, double[] pilePosition){
        
//        int obstacleId = 45;
//        double obstacleRadius = 0.4;
        
        pileSize = pileSize + deltaSize;
        
        String obstacleRequestString = Globals.xmlProcesser.getObstacleRequestMessageString(pileId, pileSize, pilePosition[0], pilePosition[1]);

        Globals.smlCommunications.sendOutputMessage(obstacleRequestString);

        System.out.println("obstacleRequestString sent = " + obstacleRequestString);
        
        
        
    }
    
    public void processExcavatorCompletion(ArrayList<Integer> excavatorIds){
        
        List<QualisysBody> qualisysBodiesList = Globals.xmlProcesser.qualisysBodiesList;
        
        for ( int excavatorId : excavatorIds ){
        
            boolean cargoChanged = false;
            
            for ( QualisysBody  currentBody : qualisysBodiesList ){
                
                if ( currentBody.getId() == excavatorId ){
                    
                    double currentAngle = currentBody.getTheta();
                    
                    if ( excavatorId == -200 ){
                        // Excavator located at the loading area
                        
                        if ( currentAngle < -80.0 && currentAngle > -100.0 ){
                            // Load poiting towards truck
                            // Should load truck at the loading area
                            loadTrucksOnLoadingSpot();
                            cargoChanged = true;
                            
                            double deltaSize = -pileDelta;                            
//                            changePileSize(loadPileId, loadPileSize, deltaSize, loadPilePosition);
                                    
                                    
                        }                        
                        
                    }
                    
                    if ( excavatorId == -201 ){
                        // Excavator located at the unloading area
                        
                        if ( currentAngle > 170.0 || currentAngle < -170.0 ){
                            // Load poiting towards truck
                            // Should load truck at the loading area
                            unloadTrucksOnUnloadingSpot();
                            cargoChanged = true;
                            
                            double deltaSize = pileDelta;                            
//                            changePileSize(unloadPileId, unloadPileSize, deltaSize, unloadPilePosition);
                            
                        }
                        
                    }
                    
                }
                
            }
              
            if (!cargoChanged){
                continue;
            }        
        
            double desiredNextYaw;
            int bodyId; 
            if ( excavatorId == -200 ){
                desiredNextYaw = 89;
                bodyId = -200;
            }else{
                desiredNextYaw = 89;
                bodyId = -201;
            }
            ArrayList<Double> desiredYaws = new ArrayList<>();
            desiredYaws.add(desiredNextYaw);
            ArrayList<Integer> bodyIds = new ArrayList<>();
            bodyIds.add(bodyId);
            String excavatorMessageString = Globals.xmlProcesser.getConstructionCommandMessageString(bodyIds, desiredYaws);
            Globals.smlCommunications.sendOutputMessage(excavatorMessageString);
            System.out.println("AUTOMATICALY sending excavator command back to place = " + excavatorMessageString);
        
        }
        
    }
    
    
    private void loadTrucksOnLoadingSpot(){
        
        for ( TruckMissionInformation truckInfo : truckInformationList ){
            
            if ( truckInfo.truckPosition.equals(onLoadParkingString) ){
                
                truckInfo.hasCargo = true;
                
            }
            
        }
        
        
    }
    
    private void unloadTrucksOnUnloadingSpot(){
        
        for ( TruckMissionInformation truckInfo : truckInformationList ){
            
            if ( truckInfo.truckPosition.equals(onUnloadString) ){
                
                truckInfo.hasCargo = false;
                
            }
            
        }
        
        
    }
    
    /**
     *
     * @param argTruckId
     * @return
     */
    public boolean isTruckAvailable(int argTruckId){
        
        int i = getTruckIndexFromId(argTruckId);
                
        ArrayList<String> truckStates = new ArrayList<>();
        
        for ( TruckMissionInformation truckMissionInformation : truckInformationList ){
            truckStates.add(truckMissionInformation.truckState);
        }
        
        return ( truckStates.get(i).equals(availableString) || truckStates.get(i).equals(undefinedString) || truckStates.get(i).equals(emergencyStopString));
                           
    }
    
    /**
     *
     * @param argTruckId
     * @return
     */
    public boolean isTruckOnUnload(int argTruckId){
                
        return isTruckOnLocation(onUnloadString, argTruckId);
        
    }
    
    /**
     *
     * @param argTruckId
     * @return
     */
    public boolean isTruckOnParking(int argTruckId){
                
        return isTruckOnLocation(onParkString, argTruckId);
        
    }
    
    /**
     *
     * @param argTruckId
     * @return
     */
    public boolean isTruckOnLoadEntrance(int argTruckId){
                
        return isTruckOnLocation(onLoadEntranceString, argTruckId);
        
    }
    
    /**
     *
     * @param argTruckId
     * @return
     */
    public boolean isTruckOnLoadParking(int argTruckId){
                
        return isTruckOnLocation(onLoadParkingString, argTruckId);
        
    }
    
    /**
     *
     * @param argTruckId
     * @return
     */
    public boolean isTruckOnLoadExit(int argTruckId){
                
        return isTruckOnLocation(onLoadExitString, argTruckId);
        
    }
    
//    public boolean isTruckOnFreeAreaParking(int argTruckId){
//                
//        return isTruckOnLocation(onFreeAreaParkingString, argTruckId);
//        
//    }
    
    private boolean isTruckOnLocation(String argString, int argTruckId){
                
        int i = getTruckIndexFromId(argTruckId);
                
        return truckInformationList.get(i).truckPosition.equals(argString);

    }
        
    private int getTruckIndexFromId(int argTruckId){
        
        for ( int i = 0; i < truckInformationList.size(); i++ ){
            
            if ( truckInformationList.get(i).truckId == argTruckId ){
                
                return i;
                
            }
            
        }
        
        // Returns -1 if truck not found
        System.out.println("Truck id not found in Mission Manager, should not happen, please fix!!!");
        return -1;
        
    }
        
    private OsmNode findAvailableDestination(ArrayList<OsmNode> locationNodes){
        
        
        ArrayList<OsmNode> truckOccupancyNodes = new ArrayList<>();
        
        for ( TruckMissionInformation truckMissionInformation : truckInformationList ){
            truckOccupancyNodes.add(truckMissionInformation.truckOccupancyNode);
        }
        
        for ( OsmNode tempNode : locationNodes ){
            
            if ( !truckOccupancyNodes.contains(tempNode) ){
                
                return tempNode;
                
            }            
            
        }
        
        return null;
        
    }
    
        
    /**
     *
     * @param desiredDestination
     * @return
     */
    public int getAvailableDestinationId(String desiredDestination){
        
        System.out.println("getAvailableDestinationId()");
        
        System.out.println("desiredDestination = " + desiredDestination);
        
        ArrayList<OsmNode> desiredDestinationList = null;
                
        desiredDestinationList = getOsmNodesListFromDestinationName(desiredDestination);
        
//        if ( desiredDestination.equals("park") ){
//            
////            desiredDestinationList = loadNodes;
//            desiredDestinationList = getOsmNodesListFromDestinationName("OnB");
//                       
//            
//        }else if ( desiredDestination.equals("unload") ){
//            
////            desiredDestinationList = deliverNodes;
//            desiredDestinationList = getOsmNodesListFromDestinationName("OnA");
//            
//        }else if ( desiredDestination.equals("load_entrance") ){
//            
////            desiredDestinationList = freeSpaceNodes;
//            desiredDestinationList = getOsmNodesListFromDestinationName("Free Space");           
//            
//        }else if ( desiredDestination.equals("load_exit") ){
//                    
//            System.out.println("desiredDestination.equals(load_exit) = " + desiredDestination.equals("load_exit"));
//            
////            desiredDestinationList = freeSpaceNodes;
//            desiredDestinationList = getOsmNodesListFromDestinationName("Free Space Exit");           
//            
//            System.out.println("desiredDestinationList = " + desiredDestinationList);
//            
//        }else{
//            
//            System.err.println("Unknown DESTINATION string! Returning 99 ");
//            
//            return 99;
//            
//        }
                
        lastAskedDestinationNode  = findAvailableDestination(desiredDestinationList);
        
        System.err.println("returning lastAskedDestinationNode");
        
        if ( lastAskedDestinationNode == null ){
            
            
            return 99;

        }else{

            return lastAskedDestinationNode.id;

        }
                
    }
    
    
    public int getDestinationId(String desiredDestination){
        
        System.out.println("getDestinationId()");
        
        System.out.println("desiredDestination = " + desiredDestination);
        
        ArrayList<OsmNode> desiredDestinationList = null;
                
        desiredDestinationList = getOsmNodesListFromDestinationName(desiredDestination);
        
        
        
        if ( desiredDestinationList.isEmpty() ){
            
            return 99;
            
        }else{            
            
            return desiredDestinationList.get(0).id;
            
        }
                
    }
    
    
    
    
    /**
     *
     * @param argBodyId
     * @param trajectoryLaneletList
     */
    public void setTruckLaneletPath(int argBodyId, ArrayList<OsmLanelet> trajectoryLaneletList){
        
        if ( trajectoryLaneletList != null ){
            
            int truckIdx = getTruckIndexFromId(argBodyId);

            truckInformationList.get(truckIdx).truckTrajectoryLanelets = trajectoryLaneletList;      
        
        }
        
    }
    
    /**
     *
     * @param argBodyId
     */
    public void clearTruckLaneletPath(int argBodyId){
        
        int i = getTruckIndexFromId(argBodyId);
        truckInformationList.get(i).truckTrajectoryLanelets = new ArrayList<>();
        
    }
    
    /**
     *
     * @param argBodyId
     * @return
     */
    public ArrayList<OsmLanelet> getTruckLaneletPath(int argBodyId){
        
        int truckIdx = getTruckIndexFromId(argBodyId);
        
        return truckInformationList.get(truckIdx).truckTrajectoryLanelets;
        
    }
        
    /**
     *
     * @param argBodyId
     * @param argLaneletId
     * @return
     */
    public boolean isLaneletInTrajectory(int argBodyId, int argLaneletId){
        
        boolean obstruction = false;
        
        int truckIdx = getTruckIndexFromId(argBodyId);
        
        ArrayList<OsmLanelet> trajLanelets = truckInformationList.get(truckIdx).truckTrajectoryLanelets;
        
        for ( OsmLanelet tempLanelet : trajLanelets ){
            
            if ( tempLanelet.id == argLaneletId ){
                                
                return true;
                
            }
            
        }
        
        return obstruction;
    
    }
    
    /**
     *
     * @param argBodyId
     * @param argTruckState
     * @param argLaneletId
     * @return
     */
    public boolean isTruckObstructedByLanelet(int argBodyId, double[] argTruckState, int argLaneletId){
        
        System.err.println("isTruckObstructedByLanelet()");
        
        boolean obstruction = false;
        
        int truckIdx = getTruckIndexFromId(argBodyId);
        
        ArrayList<OsmLanelet> trajLanelets = truckInformationList.get(truckIdx).truckTrajectoryLanelets;
        
        double minDistance = 1000000;
        int bestLanelet = 99;
        
        for ( OsmLanelet tempLanelet : trajLanelets ){
            
//            tempLanelet.leftWayId
                  
            OsmWay leftWay = null;
            OsmWay rightWay = null;
            
            for ( OsmWay tempWay : Globals.smlWorldInfo.wayList ){
                
                if ( tempWay.id == tempLanelet.leftWayId ){
                    leftWay = tempWay;
                }
                if ( tempWay.id == tempLanelet.rightWayId ){
                    rightWay = tempWay;
                }
                
            }
            
            for ( int tempNodeId : leftWay.nodeIds ){
                
                
                
                for ( OsmNode tempNode : Globals.smlWorldInfo.nodeList ){
                    
                    if ( tempNode.id == tempNodeId ){
                        
                        double currentDist = Math.sqrt( Math.pow(tempNode.X - argTruckState[0], 2) + Math.pow(tempNode.Y - argTruckState[1], 2));  
                        
                        if ( currentDist < minDistance ){
                            
                            minDistance = currentDist;
                            bestLanelet = tempLanelet.id;
                            
                        }
                        
                        break;
                        
                    }
                    
                }          
                
            }
                        
        }
        
        if ( bestLanelet == 99 ){
            
            System.err.print("Closest Lanelet not found in isTruckObstructedByLanelet()");
            
        }
        
        
        // DEBUG STUFF:
        System.out.println("isTruckObstructedByLanelet");
        ArrayList<Integer> debugLaneletIds = new ArrayList<>();
        for ( OsmLanelet tempLanelet : trajLanelets ){
            
            debugLaneletIds.add( tempLanelet.id );
            
        }
        
        System.out.println("trajectoryLanelets = " + debugLaneletIds.toString());
        System.out.println("closestLaneletFound = " + bestLanelet);
        
        int currentLaneletIdx = debugLaneletIds.indexOf(bestLanelet);
        int obstructedLaneletIdx = debugLaneletIds.indexOf(argLaneletId);
        
        System.out.println("currentLaneletIdx = " + currentLaneletIdx);
        System.out.println("obstructedLaneletIdx = " + obstructedLaneletIdx);
        
        if ( currentLaneletIdx < obstructedLaneletIdx ){
            
            obstruction = true;
            System.out.println("obstruction = " + obstruction);
        
            return obstruction;
        }
        
        for ( OsmLanelet trajLanelet : trajLanelets ){
            
            if ( argLaneletId == trajLanelet.id ){
                
                obstruction = true;
                System.err.println("obstruction from one of traj lanelets being the same as one of the obstructeds" );

                return obstruction;
                
            }
            
        }
                        
        System.out.println("obstruction = " + obstruction);
        
        return obstruction;
        
    }
        
    /**
     *
     */
    public void updateLaneletObstructions(){
        
        newlyObstructedLaneletIds.clear();
        
//        currentlyObstructedLaneletIds
        ArrayList<Integer> oldObstructedLaneletIds = new ArrayList<>();
        
        for ( int laneletId : currentlyObstructedLaneletIds ){
            
            oldObstructedLaneletIds.add(laneletId);
                    
        }
        
        currentlyObstructedLaneletIds = new ArrayList<>();
        
        for ( int laneletId : Globals.xmlProcesser.laneletsObstructedList ){
            
            currentlyObstructedLaneletIds.add(laneletId);
                    
        }
        
        for ( int laneletId : currentlyObstructedLaneletIds ){
            
            if ( !oldObstructedLaneletIds.contains(laneletId) ){
                
                // A new lanelet is obstructed
                newlyObstructedLaneletIds.add(laneletId);
                recheckNeeded = true;
                                
            }
            
        }
        
        ArrayList<Integer> freedLaneletIds = new ArrayList<>();
                
        for ( int laneletId : oldObstructedLaneletIds ){
            
            if ( !currentlyObstructedLaneletIds.contains(laneletId) ){
                
                // A new lanelet is obstructed
                freedLaneletIds.add(laneletId);
//                recheckNeeded = true;
                                
            }
                    
        }
        
        if (!newlyObstructedLaneletIds.isEmpty()){
            
            String freedLaneletsString = "Traffic information: Lanelets ";
            
            for ( int i = 0 ; i < newlyObstructedLaneletIds.size() - 1; i++ ){
                
                
                freedLaneletsString = freedLaneletsString + newlyObstructedLaneletIds.get(i) + ", ";
                
            }
            newlyObstructedLaneletIds.get(newlyObstructedLaneletIds.size()-1);
            freedLaneletsString = freedLaneletsString + newlyObstructedLaneletIds.get(newlyObstructedLaneletIds.size()-1);
            freedLaneletsString = freedLaneletsString + " are congestioned";
            
            Globals.drawingManager.logManager.addLogEntry(freedLaneletsString, Color.YELLOW);
            
            Globals.routePlanner.updateAdjacencyMatrixFromObstructions(newlyObstructedLaneletIds, false);
            
        }
        
        if (!freedLaneletIds.isEmpty()){
            
            String freedLaneletsString = "Traffic information: Lanelets ";
            
            for ( int i = 0 ; i < freedLaneletIds.size() - 1; i++ ){
                
                
                freedLaneletsString = freedLaneletsString + freedLaneletIds.get(i) + ", ";
                
            }
            freedLaneletIds.get(freedLaneletIds.size()-1);
            freedLaneletsString = freedLaneletsString + freedLaneletIds.get(freedLaneletIds.size()-1);
            freedLaneletsString = freedLaneletsString + " became free";
            
            Globals.drawingManager.logManager.addLogEntry(freedLaneletsString, Color.YELLOW);
            
            Globals.routePlanner.updateAdjacencyMatrixFromObstructions(freedLaneletIds, true);
            
        }
                
        
    }
    
    /**
     *
     * @param bodyStatesList
     * @return
     */
    public ArrayList<Integer> recheckTrucksForObstruction(ArrayList<double[]> bodyStatesList){
        
        recheckNeeded = false;
        
        ArrayList<Integer> bodiesToReplan = new ArrayList<>();
        
        for ( int laneletId : newlyObstructedLaneletIds){
            
            for ( int i = 0; i < truckInformationList.size(); i++){
            
                int bodyId = truckInformationList.get(i).truckId;
                
                if ( isLaneletInTrajectory(bodyId, laneletId) ){
                    
                    if ( isTruckObstructedByLanelet(bodyId, bodyStatesList.get(i), laneletId) ){
                    
                        if ( !bodiesToReplan.contains(bodyId) ){
                                                    
                            bodiesToReplan.add( bodyId );
                            System.out.println("Body " + bodyId + " needs replanning");
                            
                        }
                    
                    }
                    
                }
                
            }
            
        }
        
        return bodiesToReplan;
        
    }
    
}
