/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testingcommunications;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author rui
 */
public class RoutePlanner {
    
    private ArrayList<OsmNode> osmNodeList;
    private ArrayList<OsmWay> osmWayList;
    private ArrayList<OsmLanelet> osmLaneletList;
    private double[][] adjacencyMatrix;
    private double[][] defaultAdjacencyMatrix;
    
    double pointsPerMeterTrajectory = 5.0;
    
    public ArrayList<OsmLanelet> lastLaneletPath;
    
    public RoutePlanner(){
        
        osmNodeList = null;
        osmWayList = null;
        osmLaneletList = null;
        adjacencyMatrix = null;
        
    }
    
    public void setOsmNodeList(ArrayList<OsmNode> argOsmNodeList){
        
        osmNodeList = argOsmNodeList;
        
    }
    
    public void setOsmWayList(ArrayList<OsmWay> argOsmWayList){
        
        osmWayList = argOsmWayList;
        
    }
    
    public void setOsmLaneletList(ArrayList<OsmLanelet> argOsmLaneletList){
        
        osmLaneletList = argOsmLaneletList;
        
    }
        
    private void setDefaultAdjacencyMatrix(){
        
        defaultAdjacencyMatrix = new double[adjacencyMatrix.length][adjacencyMatrix[0].length];
        
        for ( int i = 0 ; i < adjacencyMatrix.length ; i++ ){
            
            for ( int j = 0 ; j < adjacencyMatrix[i].length ; j++ ){

                defaultAdjacencyMatrix[i][j] = adjacencyMatrix[i][j];

            }
            
        }
        
    }
    
    
    public ArrayList<OsmLanelet> getLastLaneletPath(){
        
//        ArrayList<Integer> lastLaneletPathIds = new ArrayList<>();
        
//        for ( OsmLanelet osmLanelet : lastLaneletPath ){
            
//            lastLaneletPathIds.add( osmLanelet.id );
            
//        }
        
        return lastLaneletPath;
        
    }
    
    public void updateAdjacencyMatrixFromObstructions(ArrayList<Integer> obstructedLaneletIds, boolean freeLanelets){
        
        if ( obstructedLaneletIds.isEmpty() ){
            
            setAdjacencyMatrixToDefault();
            
        }else{
            
            for ( int laneletId : obstructedLaneletIds ){
                
                int laneletIdx = 0;
                
                for ( int i = 0 ; i < osmLaneletList.size() ; i++ ){
                    
                    if ( osmLaneletList.get(i).id == laneletId ){
                        
                        laneletIdx = i;
                        break;
                        
                    }
                    
                }
                
                for ( int i = 0 ; i < adjacencyMatrix.length ; i++ ){
                    
                    if ( freeLanelets ){
                        
                        adjacencyMatrix[i][laneletIdx] = defaultAdjacencyMatrix[i][laneletIdx];
                        adjacencyMatrix[laneletIdx][i] = defaultAdjacencyMatrix[laneletIdx][i];
                        
                    }else{
                        
                        adjacencyMatrix[i][laneletIdx] = Double.MAX_VALUE;
                        adjacencyMatrix[laneletIdx][i] = Double.MAX_VALUE;
                        
                    }
                    
                    
                    
                }                
                
            }
            
        }
        
    }
    
    public void setAdjacencyMatrixToDefault(){
        
        adjacencyMatrix = new double[defaultAdjacencyMatrix.length][defaultAdjacencyMatrix[0].length];
        
        for ( int i = 0 ; i < defaultAdjacencyMatrix.length ; i++ ){
            
            for ( int j = 0 ; j < defaultAdjacencyMatrix[i].length ; j++ ){

                adjacencyMatrix[i][j] = defaultAdjacencyMatrix[i][j];

            }
            
        }
        
    }
    
    public void createAdjacencyMatrix(){
        
        updateAdjacencyMatrix();
        
        setDefaultAdjacencyMatrix();
        
//        defaultAdjacencyMatrix = new double[adjacencyMatrix.length][adjacencyMatrix[0].length];
        
//        defaultAdjacencyMatrix = adjacencyMatrix;
        
//        System.out.println("defaultAdjacencyMatrix[2][2] = " + defaultAdjacencyMatrix[2][2]);
//        adjacencyMatrix[2][2] = 9.0;
//        System.out.println("defaultAdjacencyMatrix[2][2] = " + defaultAdjacencyMatrix[2][2]);
        
    }
    
    public void updateAdjacencyMatrix(){
                
        int numLanelets = osmLaneletList.size();
        
        adjacencyMatrix = new double[numLanelets][numLanelets];
        
        for ( int i = 0; i < numLanelets; i++ ){
            
//            String debugString = "";
            
            for ( int j = 0; j < numLanelets; j++ ){
            
                adjacencyMatrix[i][j] = computeLaneletsAdjacency(i, j);
                
//                debugString = debugString + adjacencyMatrix[i][j] + " ";
           
            }
            
//            System.out.println(debugString);
            
        }
        
        
//        adjacencyMatrix = new double[4][4];
//        
//        adjacencyMatrix[0][0] = Double.MAX_VALUE;
//        adjacencyMatrix[0][1] = 5.0;
//        adjacencyMatrix[0][2] = 20.0;
//        adjacencyMatrix[0][3] = Double.MAX_VALUE;
//        adjacencyMatrix[1][0] = 5.0;
//        adjacencyMatrix[1][1] = Double.MAX_VALUE;
//        adjacencyMatrix[1][2] = 8.0;
//        adjacencyMatrix[1][3] = Double.MAX_VALUE;
//        adjacencyMatrix[2][0] = 20.0;
//        adjacencyMatrix[2][1] = 8.0;
//        adjacencyMatrix[2][2] = Double.MAX_VALUE;
//        adjacencyMatrix[2][3] = Double.MAX_VALUE;
//        adjacencyMatrix[3][0] = Double.MAX_VALUE;
//        adjacencyMatrix[3][1] = Double.MAX_VALUE;
//        adjacencyMatrix[3][2] = Double.MAX_VALUE;
//        adjacencyMatrix[3][3] = Double.MAX_VALUE;
        
        
    }
    
    public ArrayList<double[]> getTrajectoryFromTruckToNode(QualisysBody truck, int destinationId){
        
        ArrayList<double[]> trajectoryList= new ArrayList<>();
        
//        trajectoryList.add(new double[]{0.0, 0.0});
//        trajectoryList.add(new double[]{5.0, 5.0});
        
        
        String truckPositionString = Globals.missionManager.getTruckPosition(truck.getId());
        
        OsmNode startOsmNode;
        
        if ( truckPositionString.equals("Load Exit") ){
                        
            System.out.println("\n\n\n\n\nSPECIAL node id Load Exit being asked\n\n\n\n\n");
            int startId = Globals.missionManager.getDestinationId(truckPositionString);
            
            System.out.println("startId = " + startId);
            
            startOsmNode = getOsmNodeById(startId);
            
        }else{
            
            startOsmNode = findClosestNode(truck.getX(), truck.getY());
            
        }
        
        
        OsmNode endOsmNode = getOsmNodeById(destinationId);
        
        trajectoryList = getShortestPathBetweenOsmNodes(startOsmNode, endOsmNode);
        
        for ( int i = 0 ; i < trajectoryList.size() ; i++ ){
            
            trajectoryList.get(i)[0] = trajectoryList.get(i)[0] /32.;
            trajectoryList.get(i)[1] = trajectoryList.get(i)[1] /32.;
            
        }
        
        System.out.println("trajectoryList.size() = " + trajectoryList.size());
        
        // Trajectory should come out in SML world coordinates
        return trajectoryList;
        
    }
    
    private ArrayList<double[]> getShortestPathBetweenOsmNodes(OsmNode startOsmNode, OsmNode endOsmNode){
        
        System.out.println("getShortestPathBetweenOsmNodes()");
        
        ArrayList<OsmLanelet> possibleStartingLanelets = getContainingLanelets(startOsmNode);
        ArrayList<OsmLanelet> possibleEndingLanelets = getContainingLanelets(endOsmNode);
        
        for ( OsmLanelet startOsmLanelet : possibleStartingLanelets ){
            
            for ( OsmLanelet endOsmLanelet : possibleEndingLanelets ){
            
                ArrayList<OsmLanelet> shortestLaneletPath = dijkstraShortestPath(startOsmLanelet, endOsmLanelet);
//                ArrayList<OsmLanelet> shortestLaneletPath = dijkstraShortestPath(startOsmLanelet, startOsmLanelet);
                
                System.out.println("shortestLaneletPath.size() = " + shortestLaneletPath.size());
                
                ArrayList<OsmNode[]> dijkstraOsmNodes = getLaneletPathOrderedOsmNodes(shortestLaneletPath);
                
                System.out.println("dijkstraOsmNodes = " + dijkstraOsmNodes);
                
                lastLaneletPath = shortestLaneletPath;
                
                return getTrajectoryFromShortestPath(dijkstraOsmNodes, startOsmNode, endOsmNode);
                
            }
            
        }
        
        return null;
        
    }
    
    private ArrayList<OsmNode[]> getLaneletPathOrderedOsmNodes(ArrayList<OsmLanelet> laneletPath){
        
        ArrayList<OsmNode[]> orderedOsmNodes = new ArrayList<>();
        
        ArrayList<OsmWay> orderedLeftWays = new ArrayList<>();
        ArrayList<OsmWay> orderedRightWays = new ArrayList<>();
        
        for ( OsmLanelet osmLanelet : laneletPath ){
            
            orderedLeftWays.add( getOsmWayById( osmLanelet.leftWayId) );
            orderedRightWays.add( getOsmWayById( osmLanelet.rightWayId) );
            
        }
        
        for ( int i = 0; i < orderedLeftWays.size(); i++ ){
                        
            OsmWay leftWay = orderedLeftWays.get(i);
            OsmWay rightWay = orderedRightWays.get(i); // Right way is always correct
            
            OsmNode startLeftNode = getOsmNodeById( leftWay.nodeIds.get(0) );
            OsmNode endLeftNode = getOsmNodeById( leftWay.nodeIds.get( leftWay.nodeIds.size() - 1 ) );
            
            OsmNode startRightNode = getOsmNodeById( rightWay.nodeIds.get(0) );
            
            double startLeftNodeDistance = Math.hypot(startLeftNode.X - startRightNode.X, startLeftNode.Y - startRightNode.Y);
            double endLeftNodeDistance = Math.hypot(endLeftNode.X - startRightNode.X, endLeftNode.Y - startRightNode.Y);
            
            ArrayList<Integer> correctedLeftNodeIds = new ArrayList<>();
            
            if ( endLeftNodeDistance < startLeftNodeDistance ){
                // Need to flip node ids!
                                
                for ( int j = leftWay.nodeIds.size() - 1 ; j >= 0 ; j-- ){
                    
                    correctedLeftNodeIds.add( leftWay.nodeIds.get(j) );
                    
                }
                
                
            }else{
                
                for ( int j = 0 ; j < leftWay.nodeIds.size() ; j++ ){
                    
                    correctedLeftNodeIds.add( leftWay.nodeIds.get(j) );
                    
                }
                
            }
            
            for ( int j = 0; j < rightWay.nodeIds.size(); j++ ){
                
                OsmNode[] osmNodeLeftAndRight = new OsmNode[2];     
                
                osmNodeLeftAndRight[0] = getOsmNodeById(correctedLeftNodeIds.get(j));
                osmNodeLeftAndRight[1] = getOsmNodeById(rightWay.nodeIds.get(j));
                
                orderedOsmNodes.add(osmNodeLeftAndRight);
                
            }
                        
        }
        
        return orderedOsmNodes;
        
    }
    
    private ArrayList<double[]> getTrajectoryFromShortestPath(ArrayList<OsmNode[]> laneletBounds, OsmNode startOsmNode, OsmNode endOsmNode){
        
        System.out.println("getTrajectoryFromShortestPath()");
        
        ArrayList<double[]> trajectoryPoints = new ArrayList<>();
        
        // Crop to startOsmNode and endOsmNode
        
        int bestStartId = -1;
        int bestEndId = -1;
        double distanceStart = Double.MAX_VALUE;
        double distanceEnd = Double.MAX_VALUE;
        
        for ( int i = 0 ; i < laneletBounds.size() ; i++ ){
         
            OsmNode[] osmNodes = laneletBounds.get(i);
            
            double[] centerOsmNode = new double[2];
            
            centerOsmNode[0] = 0.5*(osmNodes[0].X + osmNodes[1].X);
            centerOsmNode[1] = 0.5*(osmNodes[0].Y + osmNodes[1].Y);
            
            double distanceToStartNode = Math.hypot(centerOsmNode[0] - startOsmNode.X, centerOsmNode[1] - startOsmNode.Y);
            
            if ( distanceToStartNode < distanceStart ){
                
                distanceStart = distanceToStartNode;
                bestStartId = i;
                
            }
                        
            double distanceToEndNode = Math.hypot(centerOsmNode[0] - endOsmNode.X, centerOsmNode[1] - endOsmNode.Y);
            
            if ( distanceToEndNode < distanceEnd ){
                
                distanceEnd = distanceToEndNode;
                bestEndId = i;
                
            }
            
        }
        
        for ( int i = bestStartId; i <= bestEndId; i++ ){
            
            double[] trajectoryPoint = new double[2];
            
            OsmNode leftOsmNode = laneletBounds.get(i)[0];
            OsmNode rightOsmNode = laneletBounds.get(i)[1];
            
            trajectoryPoint[0] = 0.5*( leftOsmNode.X + rightOsmNode.X );
            trajectoryPoint[1] = 0.5*( leftOsmNode.Y + rightOsmNode.Y );
            
            trajectoryPoints.add(trajectoryPoint);
            
        }        
        
        return trajectoryPoints;
        
    }
    
//    private ArrayList<double[]> getTrajectoryFromShortestPath(ArrayList<OsmLanelet> shortestLaneletPath, OsmNode startOsmNode, OsmNode endOsmNode){
        
//        System.out.println("getTrajectoryFromShortestPath()");
//        
//        ArrayList<double[]> trajectoryPoints = new ArrayList<>();
//        
//        for ( OsmLanelet osmLanelet : shortestLaneletPath ){
//            
//            ArrayList<double[]> currentTrajectoryPoints = getTrajectoryFromLanelet(osmLanelet);
//            
//            for ( int i = 0 ; i < currentTrajectoryPoints.size() ; i++ ){
//                
//                trajectoryPoints.add( currentTrajectoryPoints.get(i) );
//                
//            }            
//            
//        }
//        
//        System.out.println("trajectoryPoints.size() = " + trajectoryPoints.size());
//        
//        return trajectoryPoints;
//        
//    }
    
    private ArrayList<double[]> getTrajectoryFromLanelet(OsmLanelet osmLanelet){
        
       ArrayList<OsmNode> leftWayOsmNodes = getOsmWayOsmNodeList(osmLanelet.leftWayId);
       ArrayList<OsmNode> rightWayOsmNodes = getOsmWayOsmNodeList(osmLanelet.rightWayId);
       
       ArrayList<Double> leftWayLengths = new ArrayList<>();
       ArrayList<Double> rightWayLengths = new ArrayList<>();
       
       leftWayLengths.add(0.0);
       for ( int i = 1 ; i < leftWayOsmNodes.size() ; i++ ){
           
           OsmNode backNode = leftWayOsmNodes.get(i-1);
           OsmNode frontNode = leftWayOsmNodes.get(i);
           
           double currentDistance = leftWayLengths.get(i-1);
           
           currentDistance = currentDistance + Math.hypot(frontNode.X - backNode.X, frontNode.Y - backNode.Y);
           
           leftWayLengths.add(currentDistance);
           
       }
       
       rightWayLengths.add(0.0);
       for ( int i = 1 ; i < rightWayOsmNodes.size() ; i++ ){
           
           OsmNode backNode = rightWayOsmNodes.get(i-1);
           OsmNode frontNode = rightWayOsmNodes.get(i);
           
           double currentDistance = rightWayLengths.get(i-1);
           
           currentDistance = currentDistance + Math.hypot(frontNode.X - backNode.X, frontNode.Y - backNode.Y);
           
           rightWayLengths.add(currentDistance);
           
       }
       
       ArrayList<double[]> trajectoryPoints = new ArrayList<>();
       
       double finalLength = 0.5*( leftWayLengths.get(leftWayLengths.size() - 1) + leftWayLengths.get(leftWayLengths.size() - 1) );
       
       int numTrajectoryPoints = (int) Math.floor( pointsPerMeterTrajectory*finalLength );
       
       for ( int i = 0 ; i < numTrajectoryPoints ; i++ ){
           
           double currentLength = (((double)i)/((double)numTrajectoryPoints))*finalLength;
           
           double[] leftPoint = new double[2];
           
           for ( int j = 1 ; j < leftWayLengths.size() ; j ++ ){
               
               if ( leftWayLengths.get(j-1) <= currentLength && leftWayLengths.get(j) >= currentLength ){
                   
                   double a = currentLength - leftWayLengths.get(j-1);
                   double b = leftWayLengths.get(j) - currentLength;
                   
                   a = a/(a+b);
                   
                   leftPoint[0] = 0.5*( (1.-a)*leftWayOsmNodes.get(j-1).X + a*leftWayOsmNodes.get(j).X );
                   leftPoint[1] = 0.5*( (1.-a)*leftWayOsmNodes.get(j-1).Y + a*leftWayOsmNodes.get(j).Y );
                   break;
                                      
               }
               
           }
           
           double[] rightPoint = new double[2];
           
           for ( int j = 1 ; j < rightWayLengths.size() ; j ++ ){
               
               if ( rightWayLengths.get(j-1) <= currentLength && rightWayLengths.get(j) >= currentLength ){
                   
                   double a = currentLength - rightWayLengths.get(j-1);
                   double b = rightWayLengths.get(j) - currentLength;
                   
                   a = a/(a+b);
                   
                   rightPoint[0] = 0.5*( (1.-a)*rightWayOsmNodes.get(j-1).X + a*rightWayOsmNodes.get(j).X );
                   rightPoint[1] = 0.5*( (1.-a)*rightWayOsmNodes.get(j-1).Y + a*rightWayOsmNodes.get(j).Y );
                   break;
                                      
               }
               
           }
           
           double[] interpolatedPoint = new double[2];
           
           interpolatedPoint[0] = 0.5*(leftPoint[0] + rightPoint[0]);
           interpolatedPoint[1] = 0.5*(leftPoint[1] + rightPoint[1]);
           
           trajectoryPoints.add(interpolatedPoint);
           
       }
       
       trajectoryPoints = new ArrayList<>();
       
       for ( int i = 0 ; i < leftWayOsmNodes.size() ; i++ ){
           
           OsmNode osmNode = leftWayOsmNodes.get(i);
           
//           double currentDistance = leftWayLengths.get(i-1);
           
//           currentDistance = currentDistance + Math.hypot(frontNode.X - backNode.X, frontNode.Y - backNode.Y);
           
           double[] newPoint = new double[2];
           newPoint[0] = osmNode.X;
           newPoint[1] = osmNode.Y;
           
           trajectoryPoints.add(newPoint);
           
       }
             
       return trajectoryPoints;
        
    }
    
    private ArrayList<OsmLanelet> getContainingLanelets(OsmNode osmNode){
        
        System.out.println("getContainingLanelets");
        
        System.out.println("osmNode.id = " + osmNode.id);
        
        ArrayList<OsmLanelet> containingLanelets = new ArrayList<>();
        
        for ( OsmLanelet osmLanelet : osmLaneletList ){
            
            OsmWay leftOsmWay = null;
            OsmWay rightOsmWay = null;
            
            for ( OsmWay osmWay : osmWayList ){
                
                if ( osmWay.id == osmLanelet.leftWayId ){
                    
                    leftOsmWay = osmWay;
                    
                }
                
                if ( osmWay.id == osmLanelet.rightWayId ){
                    
                    rightOsmWay = osmWay;
                    
                }
                
                if ( leftOsmWay!= null && rightOsmWay!= null ){
                    
                    break;
                    
                }
                
            }
            
            if ( leftOsmWay.nodeIds.contains( osmNode.id ) || rightOsmWay.nodeIds.contains( osmNode.id ) ){
                
                containingLanelets.add(osmLanelet);
                
            }
            
        }
        
        System.out.println("containingLanelets.size() = " + containingLanelets.size());
        
        return containingLanelets;
        
    }
    
    private ArrayList<OsmLanelet> dijkstraShortestPath(OsmLanelet laneletSource, OsmLanelet laneletTarget){
        
        int laneletSourceIndex = osmLaneletList.indexOf(laneletSource);
        int laneletTargetIndex = osmLaneletList.indexOf(laneletTarget);
        
        ArrayList<Integer> laneletsIndexes = dijkstraShortestPath(laneletSourceIndex, laneletTargetIndex);
        
        ArrayList<OsmLanelet> laneletsShortestPath = new ArrayList<>();
        
        for ( int laneletIndex : laneletsIndexes ){
            
            laneletsShortestPath.add( osmLaneletList.get( laneletIndex ) );
            
        }
        
        return laneletsShortestPath;
        
    }
    
    public ArrayList<Integer> dijkstraShortestPath(int laneletSourceIndex, int laneletTargetIndex){
        
        int numberVertices = osmLaneletList.size();
//        int numberVertices = 4;
        
        double[] distance = new double[numberVertices];
        int[] previousNode = new int[numberVertices];
        ArrayList<Integer> unvisitedNodes = new ArrayList<>();
        
        for ( int i = 0; i < numberVertices ; i++){
            
            distance[i] = Double.MAX_VALUE/2.0;
            previousNode[i] = -1;
            unvisitedNodes.add(i);
            
        }
        
        distance[laneletSourceIndex] = 0;
        previousNode[laneletSourceIndex] = -1;
        
        while ( unvisitedNodes.size() != 0 ){
            
            int nextNodeToVisitIndex = -1;
            double minimumDistance = Double.MAX_VALUE;
            
            for ( int unvisitedNodeIndex : unvisitedNodes ){
                
                if ( distance[unvisitedNodeIndex] < minimumDistance ){
                    
                    nextNodeToVisitIndex = unvisitedNodeIndex;
                    minimumDistance = distance[unvisitedNodeIndex];
                    
                }
                
            }
            
//            unvisitedNodes.remove
            unvisitedNodes.remove( (Integer) nextNodeToVisitIndex );
            
            for ( int i = 0; i < numberVertices; i++ ){
                
                double newDistance = distance[nextNodeToVisitIndex] + adjacencyMatrix[nextNodeToVisitIndex][i];
                
                if ( newDistance < distance[i]  ){
                    
                    distance[i] = newDistance;
                    previousNode[i] = nextNodeToVisitIndex;
                    
                }
                
            }
                        
        }
        
        
        ArrayList<Integer> flippedShortestPath = new ArrayList<>();
                
        flippedShortestPath.add( laneletTargetIndex );
        
        for ( int i = previousNode[laneletTargetIndex]; i != -1 ; i = previousNode[i] ){
            
            flippedShortestPath.add(i);
            
        }
        
        ArrayList<Integer> shortestPath = new ArrayList<>();
                
        for ( int i = flippedShortestPath.size()-1 ; i > -1; i-- ){
            
            shortestPath.add( flippedShortestPath.get(i) );
            
        }
        
//        System.out.println("Shortest Path = " + shortestPath.toString() );
//        System.out.println("Distance = " + Arrays.toString(distance) );
//        System.out.println("Previous node = " + Arrays.toString(previousNode) );
        
        return shortestPath;
        
    }
    
    private double computeLaneletsAdjacency(int i, int j){
        
        if ( i == j ){
            
            return Double.MAX_VALUE;
            
        }
        
        OsmLanelet osmLanelet0 = osmLaneletList.get(i);
        OsmLanelet osmLanelet1 = osmLaneletList.get(j);
        
        int leftWayId = osmLanelet0.leftWayId;
        ArrayList<OsmNode> leftNodes0 = getOsmWayOsmNodeList(leftWayId);
        int rightWayId = osmLanelet0.rightWayId;
        ArrayList<OsmNode> rightNodes0 = getOsmWayOsmNodeList(rightWayId);
        
        leftNodes0 = rearrangeWaysInSameDirection(rightNodes0, leftNodes0);
        
        double averageWay0Length = (getWayLength(rightNodes0) + getWayLength(leftNodes0) )/2.0;
        
        leftWayId = osmLanelet1.leftWayId;
        ArrayList<OsmNode> leftNodes1 = getOsmWayOsmNodeList(leftWayId);
        rightWayId = osmLanelet1.rightWayId;
        ArrayList<OsmNode> rightNodes1 = getOsmWayOsmNodeList(rightWayId);

        leftNodes1 = rearrangeWaysInSameDirection(rightNodes1, leftNodes1);
        
        double averageWay1Length = (getWayLength(rightNodes1) + getWayLength(leftNodes1) )/2.0;
        
        double combinedAdjacencyLength = (averageWay0Length + averageWay1Length)/2.0;
        
//        if ( rightNodes0.get(0).id == rightNodes1.get(rightNodes1.size()-1).id && leftNodes0.get(0).id == leftNodes1.get(rightNodes1.size()-1).id ){
//            
//            return combinedAdjacencyLength;
//            
//        }
        
        if( rightNodes1.get(0).id == rightNodes0.get(rightNodes0.size()-1).id && leftNodes1.get(0).id == leftNodes0.get(rightNodes0.size()-1).id ){
            
            return combinedAdjacencyLength;
            
        }
        
        return Double.MAX_VALUE;
                
    }
    
//    private double computeLaneletsAdjacency(int i, int j){
//        
//        if ( i == j ){
//            
//            return Double.MAX_VALUE;
//            
//        }
//        
//        OsmLanelet osmLanelet0 = osmLaneletList.get(i);
//        OsmLanelet osmLanelet1 = osmLaneletList.get(j);
//        
//        int leftWayId = osmLanelet0.leftWayId;
//        ArrayList<OsmNode> leftNodes0 = getOsmWayOsmNodeList(leftWayId);
//        int rightWayId = osmLanelet0.rightWayId;
//        ArrayList<OsmNode> rightNodes0 = getOsmWayOsmNodeList(rightWayId);
//        
//        leftNodes0 = rearrangeWaysInSameDirection(rightNodes0, leftNodes0);
//        
//        double averageWay0Length = (getWayLength(rightNodes0) + getWayLength(leftNodes0) )/2.0;
//        
//        leftWayId = osmLanelet1.leftWayId;
//        ArrayList<OsmNode> leftNodes1 = getOsmWayOsmNodeList(leftWayId);
//        rightWayId = osmLanelet1.rightWayId;
//        ArrayList<OsmNode> rightNodes1 = getOsmWayOsmNodeList(rightWayId);
//
//        leftNodes1 = rearrangeWaysInSameDirection(rightNodes1, leftNodes1);
//        
//        double averageWay1Length = (getWayLength(rightNodes1) + getWayLength(leftNodes1) )/2.0;
//        
//        double combinedAdjacencyLength = (averageWay0Length + averageWay1Length)/2.0;
//        
//        if ( rightNodes0.get(0).id == rightNodes1.get(rightNodes1.size()-1).id && leftNodes0.get(0).id == leftNodes1.get(rightNodes1.size()-1).id ){
//            
//            return combinedAdjacencyLength;
//            
//        }
//        
//        if( rightNodes1.get(0).id == rightNodes0.get(rightNodes0.size()-1).id && leftNodes1.get(0).id == leftNodes0.get(rightNodes0.size()-1).id ){
//            
//            return combinedAdjacencyLength;
//            
//        }
//        
//        return Double.MAX_VALUE;
//                
//    }
    
    private double getWayLength(ArrayList<OsmNode> osmNodesList){
        
        double length = 0;
        
        for ( int i = 0; i < osmNodesList.size()-1 ; i++ ){
            
            OsmNode OsmNode0 = osmNodesList.get(i);
            OsmNode OsmNode1 = osmNodesList.get(i+1);
            
            length = length + Math.hypot(OsmNode0.X - OsmNode1.X, OsmNode0.Y - OsmNode1.Y);
            
        }
        
        return length;
        
    }
    
    private ArrayList<OsmNode> rearrangeWaysInSameDirection(ArrayList<OsmNode> wellOrientedList, ArrayList<OsmNode>  listToReorient){
                
        double originalDistance = Math.hypot( wellOrientedList.get(0).X - listToReorient.get(0).X,
                wellOrientedList.get(0).Y - listToReorient.get(0).Y);
        
        double rearrangedDistance = Math.hypot( wellOrientedList.get(0).X - listToReorient.get(listToReorient.size()-1).X,
                wellOrientedList.get(0).Y - listToReorient.get(listToReorient.size()-1).Y);
        
        if ( rearrangedDistance < originalDistance ){
            
            ArrayList<OsmNode> reorientedList = new ArrayList<>();
            
            for ( int i = listToReorient.size()-1 ; i >= 0 ; i--){
                
                reorientedList.add(listToReorient.get(i));
                
            } 
            
            return reorientedList;
            
        }else{
            
            return listToReorient;
            
        }
                
    }
    
    private ArrayList<OsmNode> getOsmWayOsmNodeList(int wayId){
        
        List<Integer> nodeIdsList = null;
        
        for ( OsmWay osmWay : osmWayList ){
            
            if ( osmWay.id == wayId ){
                
                nodeIdsList = osmWay.nodeIds;
                break;
                
            }
            
        }
        
        if ( nodeIdsList == null ){
            
            return null;
            
        }
        
        ArrayList<OsmNode> tempOsmNodeList = new ArrayList<>();
        
        for ( int nodeId : nodeIdsList ){
            
            OsmNode osmNode = getOsmNodeById(nodeId);
            
            if (osmNode != null){
                
                tempOsmNodeList.add(osmNode);
                
            }else{
                
                System.err.print("Did not find a node in a way!");
                return null;
                
            }
            
        }
        
        return tempOsmNodeList;
     
    }
    
    private OsmWay getOsmWayById(int wayId){
        
        for ( OsmWay osmWay : osmWayList ){
            
            if ( osmWay.id == wayId ){
                
                return osmWay;
                
            }
            
        }
                
        return null;
     
    }
        
    private OsmNode getOsmNodeById(int nodeId){
        
        OsmNode node = null;
        
        for ( OsmNode tempNode: osmNodeList){
            
            if ( tempNode.id == nodeId ){
                
                return tempNode;
                
            }
            
        }
        
        return node;        
        
    }
    
    private OsmNode findClosestNode(double argX, double argY){
        
        OsmNode closestOsmNode = null;
        
        argX = argX*32.;
        argY = argY*32.;
        
        double bestDistance = Double.MAX_VALUE;
        
        for ( OsmNode tempOsmNode : osmNodeList ){
            
            if ( tempOsmNode.destination == "free_space_boundary" ){
                
                continue;
                
            }
            
            OsmWay parentOsmWay = null;
            
            for ( OsmWay tempOsmWay : osmWayList ){
                
                if ( tempOsmWay.nodeIds.contains(tempOsmNode.id) ){
                          
                    parentOsmWay = tempOsmWay;
                    break;
                    
                }
                                        
            }
            
            if ( parentOsmWay == null ){
                
                System.err.println("findClosestNode() did not find parent way");
                continue;
                
            }
            
            if ( parentOsmWay.wayType.equals("free_space") ){
                
                System.out.println("Ignoring free_space way node in findClosestNode()");
                continue;
                
            }
                        
            double currentDistance = Math.sqrt( Math.pow( tempOsmNode.X - argX , 2 ) + Math.pow( tempOsmNode.Y - argY , 2 ) );
            
            if (currentDistance < bestDistance){
                
                bestDistance = currentDistance;
                closestOsmNode = tempOsmNode;
                
            }
            
        }
        
        
     
            
        
        
        
        
//        System.out.println("argX = " + argX);
//        System.out.println("closestOsmNode.X = " + closestOsmNode.X);
        
//        System.out.println("Found the closest node at distance = " + bestDistance);
        
        return closestOsmNode;    
        
    }
    
}
