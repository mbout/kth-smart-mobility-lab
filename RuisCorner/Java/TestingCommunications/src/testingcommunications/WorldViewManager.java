/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testingcommunications;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.animation.Interpolator;
import javafx.animation.ParallelTransition;
import javafx.animation.PauseTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.StrokeTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Scale;
import javafx.util.Duration;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

/**
 *
 * @author rui
 */
class BodyUpdateTime{
    
    public int id;
    public double lastUpdateTime;
    
    BodyUpdateTime(int argId, double argUpdateTime ){
        
        id = argId;
        lastUpdateTime = argUpdateTime;
        
    }
    
}

class NetworkAntenna{
    
    public double x;
    public double y;
    
    public double minimumRange;
    public double maximumRange;
    public double strength;
    
    NetworkAntenna(double argX, double argY ){
        
        x = argX;
        y = argY;
        minimumRange = 0.0;
        maximumRange = Double.MAX_VALUE;
        strength = 1.0;
        
    }
    
    NetworkAntenna(double argX, double argY, double argMaximumRange, double argStrength){
        
        x = argX;
        y = argY;
        minimumRange = 0.0;
        maximumRange = argMaximumRange;
        strength = argStrength;
        
    }
    
    NetworkAntenna(double argX, double argY, double argMinimumRange, double argMaximumRange, double argStrength){
        
        x = argX;
        y = argY;
        minimumRange = argMinimumRange;
        maximumRange = argMaximumRange;
        strength = argStrength;
        
    }
    
    
}


public class WorldViewManager {
    
    Group worldViewGroup;
    
    double worldViewWidth;
    double worldViewHeight;
    
    Image carImageRed, carImageBlue, carImageGreen, carImageYellow, carImageWhite,
            waterImage, rockImage, truckTopImage,
            trailerTopImage, trailerTopFullImage, excavatorTopImage;
        
    List<OsmNode> resizedNodeList;
    List<TrajectoryPoint> resizedTrajectoryPointList;
    
    boolean networkVisualisationOn = false;
    
    double minXCoordinate = 0;
    double minYCoordinate = 0;
    double shrikingCoordinate = 0;
    
    double maxNumberBars = 5.0;
    Paint boundsColor = Color.RED;
    Paint barsColor = Color.RED;
    
    double originalImageWidth;
    double originalImageHeight;
    
    ArrayList<BodyUpdateTime> bodyUpdateTimes;
    
    ArrayList<NetworkAntenna> networkAntennasList;
        
    public WorldViewManager(double argWidth, double argHeight){
        
        worldViewWidth = argWidth;
        worldViewHeight = argHeight;
        
        worldViewGroup = new Group();
        
//        Globals.drawingManager.screenRoot.addEventFilter(KeyEvent.KEY_PRESSED, event -> {
//            if (event.getCode() == KeyCode.N) {
//                networkVisualisationOn = !networkVisualisationOn;
//                
//                if ( networkVisualisationOn ){
//                    
//                    networkAntennasList = new ArrayList<>();
//                    createAntennas();
//                    
//                }
//                
//                System.out.println("N PRESSED!");
//                event.consume();
//            }
//        });
        
        bodyUpdateTimes = new ArrayList<>();
        
//        if ( networkVisualisationOn ){
         
        networkAntennasList = new ArrayList<>();
        createAntennas();
        
//        }
            
        
    }    
    
    public void createAntennas(){
        
        double antennaPosX = 200;
        double antennaPosY = 100;
        double antennaMinimumRange = 200.0;
        double antennaMaximumRange = 400.0;
        double antennaStrength = 1.0;
        
        NetworkAntenna networkAntenna = new NetworkAntenna(antennaPosX, antennaPosY, antennaMinimumRange, antennaMaximumRange, antennaStrength);
        
        networkAntennasList.add(networkAntenna);
    
    }
    
    public void drawImage(Paint backgroundColor){
        
        Scale scale = new Scale();
        scale.setPivotX(50);
        scale.setPivotY(50);
        worldViewGroup.getTransforms().add(scale);

        EventHandler<ScrollEvent> zoomOnScroll = new EventHandler<ScrollEvent>() {

            @Override
            public void handle(ScrollEvent event) {
                double scaleDelta = 0;
                if (event.getDeltaY() < 0) {
                    scaleDelta = -0.1;
                } else {
                    scaleDelta = 0.1;
                }
                
                scale.setPivotX( event.getSceneX() );
                scale.setPivotY( event.getSceneY() );
                
//                System.out.println("SCROLLING STUFF");
                
                if ( scale.getX() + scaleDelta > 1.0){
                
                    scale.setX(scale.getX() + scaleDelta);
                    scale.setY(scale.getY() + scaleDelta);
                
                }else{
                    
                    scale.setX(1.0);
                    scale.setY(1.0);
                    
                }
                //optional
//                line1.setStrokeWidth(1/scale.getX());
            }
            
        };
        
//        worldViewGroup.setOnScroll(zoomOnScroll);
            
        
        Image image = new Image("file:receive_image.bmp");
        originalImageWidth = image.getWidth();
        originalImageHeight = image.getHeight();
        
        if ( image.getWidth() < 1.0 ){
            
            System.err.println("Image not found!");
            
        }
        
        ImageView imageView = new ImageView(image);
        
        imageView.setPreserveRatio(false);
        imageView.setFitWidth(worldViewWidth);
        imageView.setFitHeight(worldViewHeight);
       
        worldViewGroup.getChildren().add(imageView);
        
        
//        if ( networkVisualisationOn ){
        
        worldViewGroup.getChildren().add( createAntennasGroup() );
            
//        }
        
        Rectangle backgroundColorRectangle = new Rectangle();
        backgroundColorRectangle.setWidth(worldViewWidth);
        backgroundColorRectangle.setHeight(worldViewHeight);
        backgroundColorRectangle.setFill(backgroundColor);
         
        worldViewGroup.getChildren().add(backgroundColorRectangle);       
        backgroundColorRectangle.toBack();
        
    }
    
    private Group createAntennasGroup(){
        
        System.err.println("createAntennasGroup()");
        
        Group antennasGroup = new Group();
        
        Image antennaImage = new Image("file:antenna.png");
        
        if ( antennaImage.getWidth() < 1.0 ){
            
            System.err.println("Antenna image not found!");
            
        }
        
        for ( QualisysBody qualisysBody : Globals.smlWorldInfo.getQualisysBodiesList() ){
            
            if ( !qualisysBody.getBodyType().equals("antenna") ){
                
                continue;
                
            }
            
            ImageView antennaImageView = new ImageView(antennaImage);
            
            antennaImageView.setPreserveRatio(true);
            antennaImageView.setFitWidth(50.0);
            
            double antennaLayoutX = convertToPixelX( qualisysBody.getX() ) - antennaImageView.getBoundsInLocal().getWidth()/2.0;
            double antennaLayoutY = convertToPixelY( qualisysBody.getY() ) - antennaImageView.getBoundsInLocal().getHeight()/2.0;
            
            antennaImageView.setLayoutX( antennaLayoutX );
            antennaImageView.setLayoutY( antennaLayoutY );
            
            antennaImageView.setId("imageView");
            
            antennasGroup.getChildren().add(antennaImageView);
            
            Group antennaAnimationGroup = createAntennaAnimation(antennaLayoutX + antennaImageView.getBoundsInLocal().getWidth()/2.0 ,
                    antennaLayoutY + antennaImageView.getBoundsInLocal().getHeight()/10.0 );
                                    
            Group antennaGroup = new Group(antennaImageView, antennaAnimationGroup);
            antennaGroup.setId(""+qualisysBody.getId());
            
            antennasGroup.getChildren().add(antennaGroup);
            
        }
//        
//        for ( NetworkAntenna networkAntenna : networkAntennasList ){
//            
//            ImageView antennaImageView = new ImageView(antennaImage);
//            
//            antennaImageView.setPreserveRatio(true);
//            antennaImageView.setFitWidth(50.0);
//            
//            double antennaLayoutX = convertToPixelX( networkAntenna.x/32. ) - antennaImageView.getBoundsInLocal().getWidth()/2.0;
//            double antennaLayoutY = convertToPixelY( networkAntenna.y/32. ) - antennaImageView.getBoundsInLocal().getHeight()/2.0;
//            
//            antennaImageView.setLayoutX( antennaLayoutX );
//            antennaImageView.setLayoutY( antennaLayoutY );
//            
//            antennasGroup.getChildren().add(antennaImageView);
//            
//            Group antennaAnimationGroup = createAntennaAnimation(antennaLayoutX + antennaImageView.getBoundsInLocal().getWidth()/2.0 ,
//                    antennaLayoutY + antennaImageView.getBoundsInLocal().getHeight()/10.0 );
//            
//            antennasGroup.getChildren().add(antennaAnimationGroup);
//            
//        }
        
        antennasGroup.setId("antennasGroup");
        
        return antennasGroup;
                
    }
    
    private void updateAntennasGroup(){
        
//        System.out.println("updateAntennasGroup");
        
        Group antennasGroup = (Group) worldViewGroup.lookup("#antennasGroup");
        
        if ( antennasGroup == null ){
            
            System.err.println("Antennas group lookup failed!");
            return;
            
        }
        
        if ( !networkVisualisationOn ){
            
            antennasGroup.getChildren().clear();
            return;
            
        }
        
        Image antennaImage = new Image("file:antenna.png");
        
        if ( antennaImage.getWidth() < 1.0 ){
            
            System.err.println("Antenna image not found!");
            
        }
        
        ArrayList<QualisysBody> antennasQualisysBodyList = new ArrayList<>();
        
        for ( QualisysBody qualisysBody : Globals.smlWorldInfo.getQualisysBodiesList() ){
            
            if ( !qualisysBody.getBodyType().equals("antenna") ){
                
                continue;
                
            }
            
            antennasQualisysBodyList.add(qualisysBody);
                        
            Group antennaGroup = (Group) antennasGroup.lookup( "#"+qualisysBody.getId() );
            
            double antennaLayoutX;
            double antennaLayoutY;
            
            if ( antennaGroup == null ){
            
                System.out.println("Creating an antenna");
                
                ImageView antennaImageView = new ImageView(antennaImage);

                antennaImageView.setPreserveRatio(true);
                antennaImageView.setFitWidth(50.0);

                antennaImageView.setId("imageView");

//                antennasGroup.getChildren().add(antennaImageView);

                antennaLayoutX = convertToPixelX( qualisysBody.getX() ) - antennaImageView.getBoundsInLocal().getWidth()/2.0;
                antennaLayoutY = convertToPixelY( qualisysBody.getY() ) - antennaImageView.getBoundsInLocal().getHeight()/2.0;
                
//                Group antennaAnimationGroup = createAntennaAnimation(antennaLayoutX + antennaImageView.getBoundsInLocal().getWidth()/2.0 ,
//                        antennaLayoutY + antennaImageView.getBoundsInLocal().getHeight()/10.0 );
//
//                antennaImageView.setLayoutX(antennaLayoutX);
//                antennaImageView.setLayoutY(antennaLayoutY);
                
                Group antennaAnimationGroup = createAntennaAnimation(antennaImageView.getBoundsInLocal().getWidth()/2.0 ,
                        antennaImageView.getBoundsInLocal().getHeight()/10.0 );

                antennaGroup = new Group(antennaAnimationGroup, antennaImageView);
                //                antennaAnimationGroup.setLayoutX( antennaLayoutX );
//                antennaAnimationGroup.setLayoutY( antennaLayoutY );
                
                antennaGroup.setId(""+qualisysBody.getId());
                antennasGroup.getChildren().add(antennaGroup);
                
                antennaGroup.setLayoutX(antennaLayoutX);
                antennaGroup.setLayoutY(antennaLayoutY);
                
            }else{
                
                ImageView antennaImageView = (ImageView) antennaGroup.lookup("#imageView");
                
                antennaLayoutX = convertToPixelX( qualisysBody.getX() ) - antennaImageView.getBoundsInLocal().getWidth()/2.0;
                antennaLayoutY = convertToPixelY( qualisysBody.getY() ) - antennaImageView.getBoundsInLocal().getHeight()/2.0;

                antennaGroup.setLayoutX( antennaLayoutX );
                antennaGroup.setLayoutY( antennaLayoutY );
                
            }
                        
        }
        
        if ( antennasQualisysBodyList.size() < antennasGroup.getChildren().size() ){
            
            System.out.println("Will remove antennas");
            
            for ( int i = 0 ; i < antennasGroup.getChildren().size() ; i++ ){
                
                Group antennaGroup = (Group) antennasGroup.getChildren().get(i);
                
                boolean isAntennaInAntennaBodies = false;
                
                for ( QualisysBody antennaQualisysBody : antennasQualisysBodyList ){
                    
                    String antennaIdString =  "" + antennaQualisysBody.getId();
                    
                    if ( antennaGroup.getId().equals(antennaIdString) ){
                        
                        isAntennaInAntennaBodies = true;
                        break;
                        
                    }
                    
                }
                
                if ( !isAntennaInAntennaBodies ){
                    
                    antennasGroup.getChildren().remove(antennaGroup);
                    
                }                
                
            }
            
            
        }
        
//        for ( NetworkAntenna networkAntenna : networkAntennasList ){
//            
//            ImageView antennaImageView = new ImageView(antennaImage);
//            
//            antennaImageView.setPreserveRatio(true);
//            antennaImageView.setFitWidth(50.0);
//            
//            double antennaLayoutX = convertToPixelX( networkAntenna.x/32. ) - antennaImageView.getBoundsInLocal().getWidth()/2.0;
//            double antennaLayoutY = convertToPixelY( networkAntenna.y/32. ) - antennaImageView.getBoundsInLocal().getHeight()/2.0;
//            
//            antennaImageView.setLayoutX( antennaLayoutX );
//            antennaImageView.setLayoutY( antennaLayoutY );
//            
//            antennasGroup.getChildren().add(antennaImageView);
//            
//            Group antennaAnimationGroup = createAntennaAnimation(antennaLayoutX + antennaImageView.getBoundsInLocal().getWidth()/2.0 ,
//                    antennaLayoutY + antennaImageView.getBoundsInLocal().getHeight()/10.0 );
//            
//            antennasGroup.getChildren().add(antennaAnimationGroup);
//            
//        }
        
//        antennasGroup.setId("antennasGroup");
        
//        return antennasGroup;
                
    }
    
    private Group createAntennaAnimation(double positionX, double positionY){
        
        Group antennaAnimation = new Group();
        
        Circle antennaRange = new Circle(positionX, positionY, 10.0);
        antennaRange.setStroke(Color.WHITE);
        antennaRange.setFill(Color.TRANSPARENT);
        antennaRange.setStrokeWidth(1.0);
        
        double animationTime = 2000.0;
        
        ScaleTransition st = new ScaleTransition(Duration.millis(animationTime), antennaRange);
        st.setFromX(0.0);
        st.setFromY(0.0);
//        st.setToX(20.0);
//        st.setToY(20.0);
        st.setToX(100.0);
        st.setToY(100.0);
        
        
        FadeTransition ft = new FadeTransition(Duration.millis(animationTime), antennaRange);
        ft.setFromValue(1.0);
        ft.setToValue(0.0);
        
        ParallelTransition pt = new ParallelTransition(st, ft);
                
        PauseTransition pauseT = new PauseTransition(Duration.millis(.1*animationTime));
        
        SequentialTransition seqT = new SequentialTransition(pt, pauseT);
        seqT.setCycleCount(Animation.INDEFINITE);
        
        seqT.play();        
        
        antennaAnimation.getChildren().add(antennaRange);
        
        return antennaAnimation;
        
    }
    
    public void initializeCarImages(){
        
        double carLength = 1.0*3.95; // Metres

        double newCarWidth = carLength*Globals.smlWorldInfo.pixelPerMeterX;
        
        carImageRed = new Image("file:carRed.png", newCarWidth, 0, true, false);
        carImageBlue = new Image("file:carBlue.png", newCarWidth, 0, true, false);
        carImageGreen = new Image("file:carGreen.png", newCarWidth, 0, true, false);
        carImageYellow = new Image("file:carYellow.png", newCarWidth, 0, true, false);
        carImageWhite = new Image("file:carWhite.png", newCarWidth, 0, true, false);
        
        double waterLength = 0.4*32; // Metres
        double newWaterWidth = waterLength*Globals.smlWorldInfo.pixelPerMeterX;
        waterImage = new Image("file:water.jpg", newWaterWidth, 0, true, false);
        rockImage = new Image("file:rockSeamless.png", newWaterWidth, 0, true, false);
//        waterImage = new Image("file:stone.jpg", newWaterWidth, 0, true, false);
        
        double truckLength = 32.0*0.19; // Metres
        double newTruckWidth = truckLength*Globals.smlWorldInfo.pixelPerMeterX;
        truckTopImage = new Image("file:truckTop.png", newTruckWidth, 0, true, false);
        
        double trailerLength = 32.0*0.28; // Metres
        double newTrailerWidth = trailerLength*Globals.smlWorldInfo.pixelPerMeterX;
        trailerTopImage = new Image("file:trailerTop.png", newTrailerWidth, 0, true, false);
        trailerTopFullImage = new Image("file:trailerTopFull.png", newTrailerWidth, 0, true, false);
        
        double excavatorLength = 32.0*0.35; // Metres
        double newExcavatorWidth = excavatorLength*Globals.smlWorldInfo.pixelPerMeterX;
        excavatorTopImage = new Image("file:excavatorTop.png", newExcavatorWidth, 0, true, false);
        
        
    }
    
    
    public Group createNetworkConnectivity(double width, double height, double connectivityStrength){
        
        Group connectivityGroup = new Group();
        
        double boundRatio = 0.1;
        double boundSize = boundRatio*height;
                
        Rectangle boundsRectangleLeft = new Rectangle(boundSize, height);
        boundsRectangleLeft.setFill(boundsColor);
        boundsRectangleLeft.setStrokeWidth(0.0);
        
        Rectangle boundsRectangleRight = new Rectangle(boundSize, height);
        boundsRectangleRight.setFill(boundsColor);
        boundsRectangleRight.setStrokeWidth(0.0);
        boundsRectangleRight.setLayoutX(width - boundSize);
                
        Rectangle boundsRectangleTop = new Rectangle(width, boundSize);
        boundsRectangleTop.setFill(boundsColor);
        boundsRectangleTop.setStrokeWidth(0.0);
        
        Rectangle boundsRectangleBottom = new Rectangle(width, boundSize);
        boundsRectangleBottom.setFill(boundsColor);
        boundsRectangleBottom.setStrokeWidth(0.0);
        boundsRectangleBottom.setLayoutY(height - boundSize);
        
        connectivityGroup.getChildren().addAll(boundsRectangleLeft, boundsRectangleRight, boundsRectangleTop, boundsRectangleBottom);
                
        int numberBars = Math.round( (float) (connectivityStrength*maxNumberBars) );
        
        for ( int i = 0; i < maxNumberBars; i++){
            
            Rectangle barRectangle = new Rectangle( ( (width - 2*boundSize ) - (maxNumberBars+1)*boundSize )/maxNumberBars , height - boundSize*4 );
            
            if ( i < numberBars ){
                barRectangle.setFill(barsColor);
            }else{
                barRectangle.setFill(Color.TRANSPARENT);
            }
            
            barRectangle.setStrokeWidth(0.0);
//            barRectangle.setLayoutX( width*(((float)(i+1))/maxNumberBars) -(boundRatio*height) - barRectangle.getBoundsInLocal().getWidth()/2.0);
//            barRectangle.setLayoutX( boundSize + i*2*boundSize + ( i + 0.5 )*barRectangle.getBoundsInLocal().getWidth() );
            barRectangle.setLayoutX( 2*boundSize + ((double)(i))*(boundSize + barRectangle.getBoundsInLocal().getWidth()) );
            barRectangle.setLayoutY( height/2.0 - barRectangle.getBoundsInLocal().getHeight()/2.0);
            barRectangle.setId(""+i);    
            
            connectivityGroup.getChildren().add(barRectangle);
            
        }
        
        return connectivityGroup;
        
    }
    
    public Group updateNetworkConnectivity(Group connectivityGroup, double connectivityStrength){
        
        double boundRatio = 0.1;
        double width = connectivityGroup.getBoundsInLocal().getWidth();
        double height = connectivityGroup.getBoundsInLocal().getHeight();
        
        int numberBars = Math.round( (float) (connectivityStrength*maxNumberBars) );
        
        for ( int i = 0; i < maxNumberBars; i++){
            
            Rectangle barRectangle = (Rectangle) connectivityGroup.lookup("#"+i);
            
            if ( i < numberBars ){
                
               barRectangle.setFill(barsColor);
             
            }else{
                
                barRectangle.setFill(Color.TRANSPARENT);
                
            }
            
        }
        
        
        return connectivityGroup;
        
    }
    
    
    public void drawStates(boolean drawEverything) throws TransformerException, ParserConfigurationException, InterruptedException{
                
//        System.out.println("TO REPLACE");
//        updateLaneletObstructions();
//        System.out.println("TO REPLACE");
        
//        Globals.drawingManager.actionsManager.initializeReverseGoalPose();
        
//        truckPositionString.equals("Load Exit") ){
//                        
//        System.out.println("\n\n\n\n\nSPECIAL node id Load Exit being asked\n\n\n\n\n");
//        String truckPositionString = "Load Exit";
//        int startId = Globals.missionManager.getDestinationId(truckPositionString);
//        System.out.println("startId = " + startId);
        
        if (networkVisualisationOn){
            
            Globals.drawingManager.drawMissionManager(Globals.drawingManager.screenRoot);
        
        }
            
        updateAntennasGroup();
        
        if ( Globals.missionManager != null ){
            
            if ( Globals.missionManager.recheckNeeded ){
            
                //System.out.println("RECHECK!");

                ArrayList<double[]> bodyStatesList = new ArrayList<>();

                for ( QualisysBody body : Globals.smlWorldInfo.getQualisysBodiesList() ){

                    int bodyId = body.getId();

                    if ( bodyId != Globals.body1QualisysId && bodyId != Globals.body2QualisysId && bodyId != Globals.body3QualisysId &&
                            bodyId != 808 && bodyId != 22 && bodyId > 0){

                        continue;

                    }
                    
                    double[] bodyState = new double[2];
                    bodyState[0] = body.getX()*32.0;
                    bodyState[1] = body.getY()*32.0;

                    bodyStatesList.add( bodyState );

                }

                double[] bodyState = new double[2];
                bodyState[0] = 5000.0;
                bodyState[1] = 5000.0;

                bodyStatesList.add( bodyState ); // Pedro icon
                bodyStatesList.add( bodyState ); // Jonas icon

                ArrayList<Integer> bodiesToRecheck = Globals.missionManager.recheckTrucksForObstruction(bodyStatesList);
                
                //System.out.println("bodiesToRecheck.toString() = " + bodiesToRecheck.toString() );
                
                int[] bodiesToRecheckArray = new int[bodiesToRecheck.size()];
                
                for ( int i = 0 ; i < bodiesToRecheckArray.length ; i++ ){
                    
                    bodiesToRecheckArray[i] = bodiesToRecheck.get(i);
                    
                }
                
                if ( bodiesToRecheckArray.length != 0 ){
                                
                    String stopCommandMessage = Globals.xmlProcesser.getVehicleStopCommandMessageString( bodiesToRecheckArray );
                    
                    Globals.drawingManager.logManager.addLogEntry("Truck " + bodiesToRecheckArray[0] + " aborted due to traffic congestion.", Color.ORANGE);
                    
                    System.err.println("stopCommandMessage = " + stopCommandMessage);
                    Globals.smlCommunications.sendOutputMessage(stopCommandMessage);           
                    
                    Globals.missionManager.setTruckEmergencyStop( bodiesToRecheckArray );


                }
                       
            }
                
        }
        
                        
        if (carImageRed == null){
                
//            Images have not yet been loaded
            initializeCarImages();
                        
        }
        
        
        double lowlightOpacity = 1.0;
        
        List<QualisysBody> currentQualisysBodiesList = Globals.smlWorldInfo.getQualisysBodiesList();
                        
        double circleRadius = 10.0;
        double opacityValue = 0.4;
        
        List<Integer> currentBodiesToDisplay = new ArrayList<>();
        
        for (QualisysBody body : currentQualisysBodiesList){
            
            if ( body.getId() == Globals.loadPileId ){
                
                System.out.println("Found a Globals.loadPileId!");
                
            }
            
//            if ( body.getId() == Globals.rrtMiniBoxId ){
//                
//                body.setObstacleType("rrt");
//                drawObstacle(body);
//                currentBodiesToDisplay.add(body.getId());
//                continue; 
//                
//            }
            
            if ( body.isObstacle() ){
                
//                System.out.println("body.getObstacleType() = " + body.getObstacleType());
                
                if ( body.getObstacleType().equals("rrt") || body.getObstacleType().equals("rectangle") 
                        || body.getObstacleType().equals("circle") || body.getObstacleType().equals("gun") ){
                
//                    System.out.println("Equaled");
                    
                    drawObstacle(body);
                    currentBodiesToDisplay.add(body.getId());
                    continue; 
                
                }
                
                
                
//                if ( body.getId() == 22 ){
//                
////                    System.out.println("Equaled");
//                    
//                    drawObstacle(body);
//                    currentBodiesToDisplay.add(body.getId());
//                    continue; 
//                
//                }
                
                continue;
                
            }
            
            if ( body.getBodyType().equals("construction") ){
                    
//                System.out.println("body.getId() = " + body.getId());
                
                drawExcavator(body);
                currentBodiesToDisplay.add(body.getId());
                continue; 

            }

//            if ( body.isTrailer ){
//                
//                drawTrailer(body);  
//                currentBodiesToDisplay.add(body.getId());
//                continue;
//                
//            }
            
            if ( body.getId() == Globals.trailer1QualisysId ||  body.getId() == Globals.trailer2QualisysId ||  body.getId() == Globals.trailer3QualisysId ){
                
                drawTrailer(body);  
                currentBodiesToDisplay.add(body.getId());
                continue;
                
            }
            
            
            
            
            int bodyId = body.getId();
            
            if ( bodyId != Globals.body1QualisysId && bodyId != Globals.body2QualisysId && bodyId != Globals.body3QualisysId
                    && bodyId != 808 && bodyId != 22 && bodyId > 0){
                
//                System.out.println("Ignoring body id : " + bodyId);
                
                continue;
                
            }
            
                                                
            currentBodiesToDisplay.add(bodyId);
            
            double xPixel = 0;
            double yPixel = 0;
            
//            xPixel = (body.getX())*Globals.smlWorldInfo.pixelPerMeterX*32. + Globals.smlWorldInfo.centerPixelX;
//            yPixel = -(body.getY())*Globals.smlWorldInfo.pixelPerMeterY*32. + Globals.smlWorldInfo.centerPixelY;
//            
//            xPixel = xPixel*(windowWidth/1920.0);
            
            xPixel = convertToPixelX(body.getX());
            yPixel = convertToPixelY(body.getY());
            
            double networkConnectivity = 0.0;
            
            if ( networkVisualisationOn ){
                    
                networkConnectivity = body.getNetworkConnectivity();
            
            }
            
            
                        
            xPixel = xPixel;
            yPixel = yPixel;
            
            Group tempGroup = (Group) worldViewGroup.lookup("#body"+bodyId);

//            Circle tempCircle = (Circle) statesRoot.lookup("#"+body.getId());
            
            int colorId = body.getId();
            
            if ( colorId < 0 ){
                
                colorId = - colorId;
                
            }
                    
            colorId = colorId % 5;
            
            
            if ( tempGroup != null ){
                
                if ( !shouldUpdate(body, networkConnectivity) && networkVisualisationOn){
                    
                    tempGroup.toFront();
                    continue;
                    
                }
                
                
//                tempGroup.getChildren().clear();
                tempGroup.getChildren().remove(tempGroup.lookup("#car"));
                                
                Image carImage;
//                
                switch (colorId){
                    case 1:
//                        carImage = new Image("file:carRed.png");
                        carImage = carImageRed;
                        break;
                    case 2:
//                        carImage = new Image("file:carBlue.png");
                        carImage = carImageBlue;
                        break;
                    case 3:
//                        carImage = new Image("file:carGreen.png");
                        carImage = carImageGreen;
                        break;
                    case 4:
//                        carImage = new Image("file:carYellow.png");
                        carImage = carImageYellow;
                        break;
                    default:
//                        carImage = new Image("file:carWhite.png");
                        carImage = carImageWhite;
                        break;
                }
                
                if ( body.getId() > 0){
                    // Its a qualisys Truck!
                    carImage = truckTopImage;
                    
                }
                
//                double originalCarWidth = carImage.getWidth();
//                double originalCarHeight = carImage.getHeight();
//                
                ImageView carImageView = new ImageView(carImage);
//                carImageView.setPreserveRatio(true);
//                
                double carLength = 1.0*3.95; // Metres
//                
                double newCarWidth = carLength*Globals.smlWorldInfo.pixelPerMeterX;
//                double newCarHeight = (newCarWidth/originalCarWidth)*originalCarHeight;
                double newCarHeight = carImage.getHeight();
//                
//                carImageView.setFitWidth( newCarWidth );
                                
                Rotate carRotation = new Rotate();
                
                double rotationPivotX = newCarWidth/3.0;
                double rotationPivotY = newCarHeight/2.0;
                
                carRotation.setPivotX(rotationPivotX);
                carRotation.setPivotY(rotationPivotY);
                
                double bodyAngleDegrees = body.getTheta();
//                double bodyAngleDegrees = currentAngle;
                
                double rotationOffsetX = rotationPivotX*Math.cos( Math.toRadians( bodyAngleDegrees ) ) + rotationPivotY*Math.sin( Math.toRadians( bodyAngleDegrees ) );
                double rotationOffsetY = -rotationPivotX*Math.sin( Math.toRadians( bodyAngleDegrees ) ) + rotationPivotY*Math.cos( Math.toRadians( bodyAngleDegrees ) );
                
                // Set the angle in degrees
                // body.getTheta() comes in degrees
                carRotation.setAngle( -bodyAngleDegrees );
                
//                carImageView.setLayoutX(xPixel - 0*rotationOffsetX);
//                carImageView.setLayoutY(yPixel - 0*rotationOffsetY);
                carImageView.setLayoutX(xPixel - rotationPivotX);
                carImageView.setLayoutY(yPixel - rotationPivotY);
                                
                carImageView.getTransforms().add(carRotation);
                
                carImageView.setId("car");
                
                
                Group networkBars = (Group) tempGroup.lookup("#networkBars");
                
                if ( networkVisualisationOn ){
                    
                    if ( networkBars == null ){
                        
                        networkBars = createNetworkConnectivity(0.5*70.0, 0.5*30.0, networkConnectivity);
                        networkBars.setId("networkBars");

                        networkBars.setLayoutX(xPixel+20);
                        networkBars.setLayoutY(yPixel+20);

                        tempGroup.getChildren().add(networkBars);
                    
                    }else{
                        
                        networkBars = updateNetworkConnectivity(networkBars, networkConnectivity);
                        networkBars.setId("networkBars");

                        networkBars.setLayoutX(xPixel+20);
                        networkBars.setLayoutY(yPixel+20);     
                        
                    }
                    
                }else{
                    
                    if ( networkBars != null ){
                        
                        tempGroup.getChildren().remove(networkBars);
                    
                    }               
                    
                }
                                
                if ( bodyId > 0 ){
                
                    EventHandler truckClickEvent = new EventHandler<MouseEvent>(){
                        @Override
                        public void handle(MouseEvent arg0) {

                            System.out.println("TRUCK CLICK");
                            
                            if ( Globals.drawingManager.actionsManager.waitingForReverseTrajectory ){

                                return;

                            }

                            Globals.drawingManager.clearTrajectory();
                            Globals.drawingManager.clearRipple();
                            Globals.drawingManager.actionsManager.cleanUpAfterRRT();

                            if ( Globals.missionManager.isTruckOnLoadEntrance(bodyId) ){

                                Globals.drawingManager.actionsManager.showTruckOrdersMenuFreeAreaEntrance(bodyId);

                            }else if ( Globals.missionManager.isTruckOnLoadParking(bodyId) ){

                                Globals.drawingManager.actionsManager.showTruckOrdersMenuFreeAreaParking(bodyId);

                            }else{

                                Globals.drawingManager.actionsManager.showTruckOrdersMenu(bodyId);

                            }

                        }

                    };
                    
                    tempGroup.setOnMouseClicked(truckClickEvent);
                                    
                }
                
                tempGroup.getChildren().add( carImageView );
                
            }else{
                
                EventHandler stateClickEvent = new EventHandler<MouseEvent>(){
                    @Override
                    public void handle(MouseEvent arg0) {
                
                        int stateId = bodyId;
//                        System.out.println("TO REPLACE");
////                        try {
////                            processStateClick(stateId, true);
////                        } catch (TransformerException ex) {
////                            Logger.getLogger(TestingCommunications.class.getName()).log(Level.SEVERE, null, ex);
////                        } catch (ParserConfigurationException ex) {
////                            Logger.getLogger(TestingCommunications.class.getName()).log(Level.SEVERE, null, ex);
////                        }
//                        System.out.println("TO REPLACE");
                        
                        
                    }
                };
                
             
                
                Image carImage = new Image("file:car.png");
                double originalCarWidth = carImage.getWidth();
                double originalCarHeight = carImage.getHeight();
                
                ImageView carImageView = new ImageView(carImage);
                carImageView.setPreserveRatio(true);
                
                double carLength = 3.95; // Metres
                
                double newCarWidth = carLength*Globals.smlWorldInfo.pixelPerMeterX;
                double newCarHeight = (newCarWidth/originalCarWidth)*originalCarHeight;
                
                carImageView.setFitWidth( newCarWidth );
                                
                Rotate carRotation = new Rotate();
                carRotation.setPivotX(newCarWidth/3.0);
                carRotation.setPivotY(newCarHeight/2.0);
                
                // Set the angle in degrees
                // body.getTheta() comes in degrees
                carRotation.setAngle( -body.getTheta() );
                
                carImageView.getTransforms().add(carRotation);
                
                carImageView.setLayoutX(xPixel);
                carImageView.setLayoutY(yPixel);
                
                carImageView.setId("car");
                                
                tempGroup = new Group(carImageView);
                
                if ( networkVisualisationOn ){
                
                    Group networkBars = createNetworkConnectivity(0.5*70.0, 0.5*30.0, networkConnectivity);
                    networkBars.setId("networkBars");

                    networkBars.setLayoutX(xPixel+20);
                    networkBars.setLayoutY(yPixel+20);

                    tempGroup.getChildren().add(networkBars);

                }
                
                tempGroup.setId("body"+bodyId);
                
                
                tempGroup.setOnMouseClicked(stateClickEvent);
                
                worldViewGroup.getChildren().add( tempGroup );
                
            }
            
            tempGroup.toFront();
                        
        }
        
//        currentBodiesToDisplay.clear();
        clearStates(currentBodiesToDisplay);
                   
//                currentBodiesToDisplay.add(bodyId);
        
    }
        
    private boolean shouldUpdate(QualisysBody body, double networkConnectivity){
                
        boolean updateTimeFound = false;
    
        boolean shouldUpdate = false;
        double currentTime = System.currentTimeMillis();
        
        double currentUpdateRate = (1.0/32.0)*Math.exp(1.0-networkConnectivity);
        
        for ( BodyUpdateTime bodyUpdateTime : bodyUpdateTimes ){

            if ( bodyUpdateTime.id == body.getId() ){

                updateTimeFound = true;

                double timeSinceUpdate = currentTime - bodyUpdateTime.lastUpdateTime;
                timeSinceUpdate = timeSinceUpdate/1000.;
//                System.out.println("currentUpdateRate = " + currentUpdateRate);
//                System.out.println("timeSinceUpdate = " + timeSinceUpdate);
                
                if ( timeSinceUpdate > currentUpdateRate ){
                    
                    shouldUpdate = true;
                    bodyUpdateTime.lastUpdateTime = currentTime;
                    
                }

                break;

            }

        }

        if (!updateTimeFound){
    
            BodyUpdateTime bodyUpdateTime = new BodyUpdateTime(body.getId(), currentTime);
            bodyUpdateTimes.add(bodyUpdateTime);
            
        }
            
        return shouldUpdate;
    
    }
            
    public void clearStates(List<Integer> currentBodiesToDisplay){
        
        
        for ( int i = 0; i < worldViewGroup.getChildren().size(); i++ ){
            
            Node currentNode = worldViewGroup.getChildren().get(i);
            
            String idString = currentNode.getId();
                        
            if ( idString != null ){
                
                if ( !idString.contains("body") ){
                    
                    continue;
                    
                }
                
//                worldViewGroup.getChildren().remove( currentNode );
                
                String idOnlyString = idString.replace("body", "");
                                
                int nodeId;
                try {                    
                    nodeId = Integer.parseInt( idOnlyString );
                } catch (NumberFormatException ex) {
//                    Logger.getLogger(TestingCommunications.class.getName()).log(Level.SEVERE, null, ex);
                    continue;
                }
                                               

                if ( !currentBodiesToDisplay.contains(nodeId) ){

                    worldViewGroup.getChildren().remove( currentNode );

                }
            
            }
            
        }     
        
    }
    
    public void drawTrailer(QualisysBody trailerBody){
        
        double xPixel = 0;
        double yPixel = 0;
        
        // Special body!
        double bodyX;
        double bodyY;
        
        bodyX = trailerBody.getX();
        bodyY = trailerBody.getY();
        
        xPixel = convertToPixelX(bodyX);
        yPixel = convertToPixelY(bodyY);

        Group tempGroup = (Group) worldViewGroup.lookup("#body"+trailerBody.getId());

        boolean hasCargo;
        
        if ( trailerBody.getId() == Globals.trailer1QualisysId ){
            
            hasCargo = Globals.missionManager.hasTruckCargo(Globals.body1QualisysId);
            
        }else{
            
            if ( trailerBody.getId() == Globals.trailer2QualisysId ){
            
            hasCargo = Globals.missionManager.hasTruckCargo(Globals.body2QualisysId);
            
            }else{


                if ( trailerBody.getId() == Globals.trailer3QualisysId ){
            
                    hasCargo = Globals.missionManager.hasTruckCargo(Globals.body3QualisysId);

                }else{

                    hasCargo = false;

                }
                

            }
            
        }
        
        
        if ( tempGroup != null ){

            tempGroup.getChildren().clear();

        }else{

            tempGroup = new Group();
            tempGroup.setId( "body"+trailerBody.getId() );
            worldViewGroup.getChildren().add(tempGroup);

        }
        
        ImageView trailerImageView;
        
        if (hasCargo){
        
            trailerImageView = new ImageView(trailerTopFullImage);
            if ( trailerTopFullImage.isError()){
                System.out.println("trailerTopFullImage.isError() = " + trailerTopFullImage.isError());
            }
            
        }else{
            
            trailerImageView = new ImageView(trailerTopImage);
            if ( trailerTopImage.isError()){
                System.out.println("trailerTopImage.isError() = " + trailerTopImage.isError());
            }
            
        }

//                
        double trailerWidth = 0.2;
        double trailerHeight = 0.1;

        double newTrailerWidth = trailerWidth*32.*Globals.smlWorldInfo.pixelPerMeterX;
        double newTrailerHeight = trailerHeight*32.*Globals.smlWorldInfo.pixelPerMeterY; //

        trailerImageView.setPreserveRatio(false);
        trailerImageView.setFitWidth( newTrailerWidth );
        trailerImageView.setFitHeight( newTrailerHeight );

//        System.out.println("trailerBody.getObstacleType() = " + trailerBody.getObstacleType());
//        System.out.println("trailerBody.width = " + trailerBody.width);
//        System.out.println("trailerBody.height = " + trailerBody.height);

        double rotationPivotX = newTrailerWidth/4.0;
        double rotationPivotY = newTrailerHeight/2.0;

        Rotate trailerRotation = new Rotate();

        trailerRotation.setPivotX(xPixel);
        trailerRotation.setPivotY(yPixel);

        double bodyAngleDegrees = trailerBody.getTheta();

        trailerRotation.setAngle( -bodyAngleDegrees );
//                double bodyAngleDegrees = currentAngle;

        double rotationOffsetX = rotationPivotX*Math.cos( Math.toRadians( bodyAngleDegrees ) ) + rotationPivotY*Math.sin( Math.toRadians( bodyAngleDegrees ) );
        double rotationOffsetY = -rotationPivotX*Math.sin( Math.toRadians( bodyAngleDegrees ) ) + rotationPivotY*Math.cos( Math.toRadians( bodyAngleDegrees ) );

        // Set the angle in degrees
        // body.getTheta() comes in degrees


//                carImageView.setLayoutX(xPixel - 0*rotationOffsetX);
//                carImageView.setLayoutY(yPixel - 0*rotationOffsetY);
        trailerImageView.setX(xPixel - rotationPivotX);

        trailerImageView.setY(yPixel - rotationPivotY);

        trailerImageView.getTransforms().clear();

//        System.out.println("waterImageView.getBoundsInParent().toString() = " + waterImageView.getBoundsInParent().toString());
//        System.out.println("waterImageView.getBoundsInLocal().toString() = " + waterImageView.getBoundsInLocal().toString());

        trailerImageView.getTransforms().add(trailerRotation);
        
        tempGroup.getChildren().add( trailerImageView );
        trailerImageView.toFront();
        
        
        
    }
    
    
    public void drawExcavator(QualisysBody excavatorBody){
        
//        System.out.println("drawExcavator()");
        
        String excavatorImageIdString = "excavator";
        
        double xPixel = 0;
        double yPixel = 0;
        
        // Special body!
        double bodyX;
        double bodyY;
        
        bodyX = excavatorBody.getX();
        bodyY = excavatorBody.getY();
        
        xPixel = convertToPixelX(bodyX);
        yPixel = convertToPixelY(bodyY);

        Group tempGroup = (Group) worldViewGroup.lookup("#body"+excavatorBody.getId());

        ImageView excavatorImageView = null;
        
        if ( tempGroup != null ){

            excavatorImageView = (ImageView) tempGroup.lookup("#"+excavatorImageIdString);
//            tempGroup.getChildren().clear();
            
        }else{

            tempGroup = new Group();
            tempGroup.setId( "body"+excavatorBody.getId() );
            worldViewGroup.getChildren().add(tempGroup);

        }
        
        double excavatorWidth = 0.6;
        double excavatorHeight = 0.2;

        double newExcavatorWidth = excavatorWidth*32.*Globals.smlWorldInfo.pixelPerMeterX;
        double newExcavatorHeight = excavatorHeight*32.*Globals.smlWorldInfo.pixelPerMeterY; //
        
        if (excavatorImageView == null){
                
            excavatorImageView = new ImageView(excavatorTopImage);
            if ( excavatorTopImage.isError()){
                System.out.println("trailerTopImage.isError() = " + excavatorTopImage.isError());
            }

            

            excavatorImageView.setPreserveRatio(false);
            excavatorImageView.setFitWidth( newExcavatorWidth );
            excavatorImageView.setFitHeight( newExcavatorHeight );

            excavatorImageView.setId(excavatorImageIdString);
                        
            
            
            
            tempGroup.getChildren().add( excavatorImageView );
            
        }

        double rotationPivotX = newExcavatorWidth/4.0;
        double rotationPivotY = newExcavatorHeight/2.0;

        Rotate excavatorRotation = new Rotate();

        excavatorRotation.setPivotX(xPixel);
        excavatorRotation.setPivotY(yPixel);

        double bodyAngleDegrees = excavatorBody.getTheta();
        
        excavatorRotation.setAngle( -bodyAngleDegrees );

        double rotationOffsetX = rotationPivotX*Math.cos( Math.toRadians( bodyAngleDegrees ) ) + rotationPivotY*Math.sin( Math.toRadians( bodyAngleDegrees ) );
        double rotationOffsetY = -rotationPivotX*Math.sin( Math.toRadians( bodyAngleDegrees ) ) + rotationPivotY*Math.cos( Math.toRadians( bodyAngleDegrees ) );

        excavatorImageView.setX(xPixel - rotationPivotX);

        excavatorImageView.setY(yPixel - rotationPivotY);

        excavatorImageView.getTransforms().clear();

        excavatorImageView.getTransforms().add(excavatorRotation);
        
        
        
        EventHandler<MouseEvent> excavatorAction = new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {

                System.out.println("Pressed excavator " + excavatorBody.getId());

                ArrayList<Integer> bodyIds = new ArrayList<>();
                ArrayList<Double> desiredYaws = new ArrayList<>();

                bodyIds.add(excavatorBody.getId());
                
                double desiredNextYaw = 90.;

                if ( excavatorBody.getId() == -200 ){

                    
                        if ( excavatorBody.getTheta() > 45 ){
                            desiredNextYaw = -90.;
                        }
                        if ( excavatorBody.getTheta() < -45 ){
                            desiredNextYaw = 90.;
                        }
                        
                        
                        double deltaSize = -Globals.missionManager.pileDelta;
//                        Globals.missionManager.changePileSize(deltaSize);
                      
//                    Globals.missionManager.changePileSize(Globals.missionManager.loadPileId,
//                            Globals.missionManager.loadPileSize, deltaSize, Globals.missionManager.loadPilePosition);
//                    try {
//                        Thread.sleep((long)200.0);
//                    } catch (InterruptedException ex) {
//                        Logger.getLogger(WorldViewManager.class.getName()).log(Level.SEVERE, null, ex);
//                    }
                    
                
                }else{
                    
                    if ( excavatorBody.getTheta() >= 80  && excavatorBody.getTheta() <= 100){
                        desiredNextYaw = 180.;
                    }else{
//                    if ( excavatorBody.getTheta() < -135 && excavatorBody.getTheta() > 135 ){
                        desiredNextYaw = 90.;
                    }
                    
                    
                    
                    
                    double deltaSize = Globals.missionManager.pileDelta;      
//                    Globals.missionManager.changePileSize(deltaSize);
////                    Globals.missionManager.changePileSize(Globals.missionManager.unloadPileId,
//                            Globals.missionManager.unloadPileSize, deltaSize, Globals.missionManager.unloadPilePosition);
                    
//                    try {
//                        Thread.sleep((long)200.0);
//                    } catch (InterruptedException ex) {
//                        Logger.getLogger(WorldViewManager.class.getName()).log(Level.SEVERE, null, ex);
//                    }
                    
                }
                
                desiredYaws.add( desiredNextYaw );
                
                String excavatorMessageString = Globals.xmlProcesser.getConstructionCommandMessageString(bodyIds, desiredYaws);
                Globals.smlCommunications.sendOutputMessage(excavatorMessageString);

                System.out.println("Sending excavator command = " + excavatorMessageString);

            }
        };


        excavatorImageView.setOnMouseClicked(excavatorAction);
        
        excavatorImageView.toFront();
        
        
        
    }
    
    
    public void drawObstacle(QualisysBody obstacleBody){
        
//        currentBodiesToDisplay.add(bodyId);
                       
        double xPixel = 0;
        double yPixel = 0;
        
        // Special body!
        double bodyX;
        double bodyY;
        
//        bodyX = obstacleBody.getX() - 0.2*Math.cos( Math.toRadians( obstacleBody.getTheta() ) )
                
//        bodyX = obstacleBody.getX() + (-0.2)*Math.cos( Math.toRadians( obstacleBody.getTheta() ) ) - (-0.15)*Math.sin( Math.toRadians( obstacleBody.getTheta() ) );
//        bodyY = obstacleBody.getY() + (-0.2)*Math.sin( Math.toRadians( obstacleBody.getTheta() ) ) + (-0.15)*Math.cos( Math.toRadians( obstacleBody.getTheta() ) );
        bodyX = obstacleBody.getX();
        bodyY = obstacleBody.getY();
        
        Polyline polyline = new Polyline();
        
        double pixelXTest = convertToPixelX( obstacleBody.getX() + (-0.2)*Math.cos( Math.toRadians( obstacleBody.getTheta() ) ) - (-0.15)*Math.sin( Math.toRadians( obstacleBody.getTheta() ) ) );
        double pixelYTest = convertToPixelY( obstacleBody.getY() + (-0.2)*Math.sin( Math.toRadians( obstacleBody.getTheta() ) ) + (-0.15)*Math.cos( Math.toRadians( obstacleBody.getTheta() ) ) );

        polyline.getPoints().addAll(new Double[]{ pixelXTest, pixelYTest });
        
        pixelXTest = convertToPixelX( obstacleBody.getX() + (-0.2)*Math.cos( Math.toRadians( obstacleBody.getTheta() ) ) - (0.15)*Math.sin( Math.toRadians( obstacleBody.getTheta() ) ) );
        pixelYTest = convertToPixelY( obstacleBody.getY() + (-0.2)*Math.sin( Math.toRadians( obstacleBody.getTheta() ) ) + (0.15)*Math.cos( Math.toRadians( obstacleBody.getTheta() ) ) );

        polyline.getPoints().addAll(new Double[]{ pixelXTest, pixelYTest });
        
        pixelXTest = convertToPixelX( obstacleBody.getX() + (0.2)*Math.cos( Math.toRadians( obstacleBody.getTheta() ) ) - (0.15)*Math.sin( Math.toRadians( obstacleBody.getTheta() ) ) );
        pixelYTest = convertToPixelY( obstacleBody.getY() + (0.2)*Math.sin( Math.toRadians( obstacleBody.getTheta() ) ) + (0.15)*Math.cos( Math.toRadians( obstacleBody.getTheta() ) ) );

        polyline.getPoints().addAll(new Double[]{ pixelXTest, pixelYTest });
        
        pixelXTest = convertToPixelX( obstacleBody.getX() + (0.2)*Math.cos( Math.toRadians( obstacleBody.getTheta() ) ) - (-0.15)*Math.sin( Math.toRadians( obstacleBody.getTheta() ) ) );
        pixelYTest = convertToPixelY( obstacleBody.getY() + (0.2)*Math.sin( Math.toRadians( obstacleBody.getTheta() ) ) + (-0.15)*Math.cos( Math.toRadians( obstacleBody.getTheta() ) ) );

        polyline.getPoints().addAll(new Double[]{ pixelXTest, pixelYTest });
        
        pixelXTest = convertToPixelX( obstacleBody.getX() + (-0.2)*Math.cos( Math.toRadians( obstacleBody.getTheta() ) ) - (-0.15)*Math.sin( Math.toRadians( obstacleBody.getTheta() ) ) );
        pixelYTest = convertToPixelY( obstacleBody.getY() + (-0.2)*Math.sin( Math.toRadians( obstacleBody.getTheta() ) ) + (-0.15)*Math.cos( Math.toRadians( obstacleBody.getTheta() ) ) );

        polyline.getPoints().addAll(new Double[]{ pixelXTest, pixelYTest });
        
        polyline.setStrokeWidth(5.0);
        polyline.setStroke(Color.ORANGERED);
        polyline.setOpacity(0.5);
        
        
//        xPixel = (bodyX)*Globals.smlWorldInfo.pixelPerMeterX*32. + Globals.smlWorldInfo.centerPixelX;
//        yPixel = -(bodyY)*Globals.smlWorldInfo.pixelPerMeterY*32. + Globals.smlWorldInfo.centerPixelY;
//
//        xPixel = xPixel*(windowWidth/1920.0);
        
                
        xPixel = convertToPixelX(bodyX);
        yPixel = convertToPixelY(bodyY);

        Group tempGroup = (Group) worldViewGroup.lookup("#body"+obstacleBody.getId());


        if ( tempGroup != null ){

            tempGroup.getChildren().clear();

        }else{

            tempGroup = new Group();
            tempGroup.setId( "body"+obstacleBody.getId() );
            worldViewGroup.getChildren().add(tempGroup);

        }

        ImageView waterImageView;
        
        if ( obstacleBody.getObstacleType().equals("rrt") || obstacleBody.getObstacleType().equals("rectangle") ){
        
//            System.out.println("Drawing a rrt/rectangle");
            
            waterImageView = new ImageView(waterImage);
            if ( waterImage.isError()){
                System.out.println("waterImage.isError() = " + waterImage.isError());
            }

//            System.out.println("obstacleBody.width = " + obstacleBody.width);
//            System.out.println("obstacleBody.width = " + obstacleBody.width);
            
    //                
            double obstacleWidth = obstacleBody.width;
            double obstacleHeight = obstacleBody.height;

            double newWaterWidth = obstacleWidth*32.*Globals.smlWorldInfo.pixelPerMeterX;
            double newWaterHeight = obstacleHeight*32.*Globals.smlWorldInfo.pixelPerMeterY; //

            waterImageView.setPreserveRatio(false);
            waterImageView.setFitWidth( newWaterWidth );
            waterImageView.setFitHeight( newWaterHeight );

//            System.out.println("obstacleBody.getObstacleType() = " + obstacleBody.getObstacleType());
//            System.out.println("obstacleBody.width = " + obstacleBody.width);
//            System.out.println("obstacleBody.height = " + obstacleBody.height);
            
            double rotationPivotX = newWaterWidth/2.0;
            double rotationPivotY = newWaterHeight/2.0;

            Rotate carRotation = new Rotate();

            carRotation.setPivotX(xPixel);
            carRotation.setPivotY(yPixel);

            double bodyAngleDegrees = obstacleBody.getTheta();

            carRotation.setAngle( -bodyAngleDegrees );
    //                double bodyAngleDegrees = currentAngle;

            double rotationOffsetX = rotationPivotX*Math.cos( Math.toRadians( bodyAngleDegrees ) ) + rotationPivotY*Math.sin( Math.toRadians( bodyAngleDegrees ) );
            double rotationOffsetY = -rotationPivotX*Math.sin( Math.toRadians( bodyAngleDegrees ) ) + rotationPivotY*Math.cos( Math.toRadians( bodyAngleDegrees ) );

            // Set the angle in degrees
            // body.getTheta() comes in degrees


    //                carImageView.setLayoutX(xPixel - 0*rotationOffsetX);
    //                carImageView.setLayoutY(yPixel - 0*rotationOffsetY);
            waterImageView.setX(xPixel - rotationPivotX);

            waterImageView.setY(yPixel - rotationPivotY);

            waterImageView.getTransforms().clear();

    //        System.out.println("waterImageView.getBoundsInParent().toString() = " + waterImageView.getBoundsInParent().toString());
    //        System.out.println("waterImageView.getBoundsInLocal().toString() = " + waterImageView.getBoundsInLocal().toString());

            waterImageView.getTransforms().add(carRotation);
            
        }else if( obstacleBody.getObstacleType().equals("gun") || obstacleBody.getObstacleType().equals("circle") ){
            
//            System.out.println("Drawing a gun/circle with id " + obstacleBody.getId());
            
            waterImageView = new ImageView(rockImage);
            if ( rockImage.isError()){
                System.out.println("rockImage.isError() = " + rockImage.isError());
            }

            double obstacleRadius = obstacleBody.radius;
            
//            System.out.println("obstacleBody.getObstacleType() = " + obstacleBody.getObstacleType());
//            System.out.println("obstacleBody.radius = " + obstacleBody.radius);
            
            double obstacleWidth = obstacleRadius*2.0;
            double obstacleHeight = obstacleRadius*2.0;

            double newWaterWidth = obstacleWidth*32.*Globals.smlWorldInfo.pixelPerMeterX;
            double newWaterHeight = obstacleHeight*32.*Globals.smlWorldInfo.pixelPerMeterY; //

            waterImageView.setPreserveRatio( false );
            waterImageView.setFitWidth( newWaterWidth );
            waterImageView.setFitHeight( newWaterHeight );

            waterImageView.setX( xPixel - newWaterWidth/2.0 );
            waterImageView.setY( yPixel - newWaterHeight/2.0 );
            
        }else{
            
            System.out.println("obstacleBody.getObstacleType() = " + obstacleBody.getObstacleType() );
            System.err.println("drawObstacle(): Trying to draw an obstacle for which I am not ready!");
            return;
            
        }
        
        tempGroup.getChildren().add( waterImageView );
        waterImageView.toFront();
        
//        tempGroup.getChildren().add( polyline );
        
//        Circle debugCircle = new Circle(xPixel, yPixel, 5);
//        tempGroup.getChildren().add( debugCircle );
//        debugCircle.toFront();
        
                
    }
    
    public double convertToPixelX(double argX){
        
        double xPixel = argX*Globals.smlWorldInfo.pixelPerMeterX*32. + Globals.smlWorldInfo.centerPixelX;
        
//        xPixel = xPixel + leftInset;
        
        xPixel = xPixel*(worldViewWidth/originalImageWidth);    
//        *(worldViewWidth/originalImageWidth)
        
        return xPixel;
        
    }
    
    public double convertToPixelY(double argY){
        
        double yPixel = -argY*Globals.smlWorldInfo.pixelPerMeterY*32. + Globals.smlWorldInfo.centerPixelY;
        
//        yPixel = yPixel + topInset;
        
//        System.out.println("topInset = " + topInset);
        
        yPixel = yPixel*(worldViewHeight/originalImageHeight);    
        
        return yPixel;
        
    }
    
    public double convertToX(double argPixelX){
        
//        double xPixel = argX*Globals.smlWorldInfo.pixelPerMeterX*32. + Globals.smlWorldInfo.centerPixelX;
//        xPixel = xPixel*(windowWidth/1920.0); 
        
        double x = argPixelX*(originalImageWidth/worldViewWidth);
        x = x - Globals.smlWorldInfo.centerPixelX;
        x = x / ( Globals.smlWorldInfo.pixelPerMeterX*32. );
        
        System.out.println("convertToX(argPixelX)");
        System.out.println("argPixelX = " + argPixelX);
        System.out.println("x = " + x);
        
        System.out.println("convertToPixelX(x)");
        double pixelX = convertToPixelX(x);
        System.out.println("x = " + x);
        System.out.println("pixelX = " + pixelX);
        
        
        
        return x;
        
    }
    
    public double convertToY(double argPixelY){
        
//        double yPixel = -argY*Globals.smlWorldInfo.pixelPerMeterY*32. + Globals.smlWorldInfo.centerPixelY;
    
        
        double y = argPixelY*(originalImageHeight/worldViewHeight);  
        y = y - Globals.smlWorldInfo.centerPixelY;
        y = y / ( -Globals.smlWorldInfo.pixelPerMeterY*32. );
                
        System.out.println("convertToY(argPixelY)");
        System.out.println("argPixelY = " + argPixelY);
        System.out.println("y = " + y);
        
        System.out.println("convertToPixelY(y)");
        double pixelY = convertToPixelY(y);
        System.out.println("y = " + y);
        System.out.println("pixelY = " + pixelY);
        
        
        return y;
        
    }
       
    
    public void makeRipple(int selectedId){
        
        List<QualisysBody> currentQualisysBodiesList = Globals.smlWorldInfo.getQualisysBodiesList();
                        
        Group ripplesGroup = (Group) worldViewGroup.lookup("#ripplesGroup");
        
        if ( ripplesGroup == null ){
            
            ripplesGroup = new Group();
            ripplesGroup.setId("ripplesGroup");
            worldViewGroup.getChildren().add(ripplesGroup);
            
        }else{
            
            ripplesGroup.getChildren().clear();
            
        }
        
        for (QualisysBody body : currentQualisysBodiesList){
            
            if ( body.getId() != selectedId ){
                
                continue;
                
            }
            
            double truckLength = 4.0;
            
//            double xPixel = (body.getX() + ( (truckLength/2.0) /32.0)*Math.cos( Math.toRadians( body.getTheta()) ) )*Globals.smlWorldInfo.pixelPerMeterX*32. + Globals.smlWorldInfo.centerPixelX;
//            double yPixel = -(body.getY() + ( (truckLength/2.0) /32.0)*Math.sin( Math.toRadians( body.getTheta()) ) )*Globals.smlWorldInfo.pixelPerMeterY*32. + Globals.smlWorldInfo.centerPixelY;
//            
//            xPixel = xPixel*(windowWidth/1920.0);
            
            double xPixel = convertToPixelX( (body.getX() + ( (truckLength/2.0) /32.0)*Math.cos( Math.toRadians( body.getTheta()) ) ) );
            double yPixel = convertToPixelY( (body.getY() + ( (truckLength/2.0) /32.0)*Math.sin( Math.toRadians( body.getTheta()) ) ) );
            
            int maxCircles = 5;
            double initialRadius = 10.0;
            double endZoom = 2.0;
            
            ParallelTransition pt = new ParallelTransition();
            
//            double baseDurationAnimation = 2000;
            double baseDurationAnimation = 5000;
            
            for (int i = 0; i < maxCircles; i++){
            
                Circle rippleCircle = new Circle();
                rippleCircle.setRadius(initialRadius);

                rippleCircle.setFill(Color.TRANSPARENT);

                rippleCircle.setLayoutX(xPixel);
                rippleCircle.setLayoutY(yPixel);

                rippleCircle.setStroke(Color.WHITE);
                rippleCircle.setStrokeWidth(1.0);
                
                rippleCircle.setOpacity(0.);

                ripplesGroup.getChildren().add(rippleCircle);
                
                
                
                double durationAnimation = baseDurationAnimation*(((double)(i+1) )/((double)maxCircles) );
                
                FadeTransition ft = new FadeTransition(Duration.millis(durationAnimation), rippleCircle);
                ft.setFromValue(1.0);
                ft.setToValue(0.0);
                ft.setCycleCount(1);
                ft.setAutoReverse(false);

//                System.out.println("(1+((double)i)/((double)maxCircles) = " + (1+((double)i)/((double)maxCircles)) );
//                System.out.println("durationAnimation = " + durationAnimation);
                
                ScaleTransition st = new ScaleTransition(Duration.millis(durationAnimation), rippleCircle);
                st.setByX(endZoom);
                st.setByY(endZoom);
                st.setCycleCount(1);
                st.setAutoReverse(false);
                
                PauseTransition pauseTransition = new PauseTransition( Duration.millis( baseDurationAnimation - durationAnimation ));
                
                SequentialTransition seqTransitionA = new SequentialTransition (pauseTransition, ft);
                SequentialTransition seqTransitionB = new SequentialTransition (pauseTransition, st);
    //            ft.play();
                
//                pt.getChildren().addAll(seqTransitionA, seqTransitionB);
                pt.getChildren().addAll(ft, st);
            
            }
            
            pt.play();
        
        
        }
        
    }
    
    public void clearRipple(){
        
        Group ripplesGroup = (Group) worldViewGroup.lookup("#ripplesGroup");
        
        if ( ripplesGroup != null ){
            
            ripplesGroup.getChildren().clear();
            
        }
        
    }
        
    public void createResizedNodes(List<OsmNode> nodeList){
        
//        resizedNodeList
        
        double minX = 100000;
        double minY = 100000;
        double maxX = -100000;
        double maxY = -100000;
                
        for ( int i = 0; i < nodeList.size(); i++ ){
            
            double currentX = nodeList.get(i).X;
            double currentY = nodeList.get(i).Y;
            
            if (currentX < minX){
                minX = currentX;
            }
            if (currentY < minY){
                minY = currentY;
            }
            if (currentX > maxX){
                maxX = currentX;
            }
            if (currentY > maxY){
                maxY = currentY;
            }
            
            
        }
        
        double shrinkingX = worldViewWidth/(maxX-minX);
        double shrinkingY = worldViewHeight/(maxY-minY);
        
        double shriking;
        
        if (shrinkingX < shrinkingY){
            shriking = shrinkingX;
        }else{            
            shriking = shrinkingY;
        }
        
        minXCoordinate = minX;
        minYCoordinate = minY;
        shrikingCoordinate = shriking;
        
        resizedNodeList = new ArrayList();
        
        for ( int i = 0; i < nodeList.size() ; i++ ){
            
            OsmNode tempNode = nodeList.get(i);
            
            double[] resizedValues = resizeToScreen(tempNode.X, tempNode.Y);
//            Node newNode = new Node(tempNode.id, resizedValues[0], resizedValues[1]);         
                        
            double pixelX = Globals.smlWorldInfo.centerPixelX + tempNode.X*Globals.smlWorldInfo.pixelPerMeterX;
            double pixelY = Globals.smlWorldInfo.centerPixelY - tempNode.Y*Globals.smlWorldInfo.pixelPerMeterY;
            OsmNode newNode = new OsmNode(tempNode.id, pixelX, pixelY);         
            
            resizedNodeList.add(newNode);
            
        }
        
    }
    
    public double[] resizeToScreen(double x, double y){
        
        double[] resizedCoordinates = new double[2];
        
        resizedCoordinates[0] = (x - minXCoordinate)*shrikingCoordinate;
        resizedCoordinates[1] = worldViewHeight - (y - minYCoordinate)*shrikingCoordinate;
        
        return resizedCoordinates;
        
    }
    
    public Group getWayPolygon(int wayId){
                
        Group wayGroup = new Group();
        
        OsmWay way = null;
        
        for( OsmWay tempWay : Globals.xmlProcesser.wayList ){
            
            if ( tempWay.id == wayId){
                
                way = tempWay;
                
            }
                                    
        }
        
        ArrayList<OsmNode> wayNodes = new ArrayList<>();
                
        for ( int nodeId : way.nodeIds ){
            
            for ( OsmNode node : Globals.xmlProcesser.nodeList ){
                
                if ( node.id == nodeId ){
                    
                    wayNodes.add(node);
                    
                }
                
            }
            
        }
        
        ArrayList<double[]> polygonPoints = new ArrayList<>();
        
        for ( OsmNode node : wayNodes ){
            
            double[] nodePosition = new double[2];
            nodePosition[0] = convertToPixelX(node.X/32.);
            nodePosition[1] = convertToPixelY(node.Y/32.);
            
            polygonPoints.add(nodePosition);
            
        }
                
        Polygon polygon = new Polygon();
        polygon.setFill(Color.WHITE);
        
        FadeTransition ft = new FadeTransition(Duration.millis(1000), polygon);
        ft.setFromValue(0.3);
        ft.setToValue(0.0);
        ft.setCycleCount( Animation.INDEFINITE );
        ft.setAutoReverse(true);
        
        ft.play();
        
        for ( int i = 0 ; i < polygonPoints.size() ; i++ ){
            
            double[] currentPoint = new double[2];
            currentPoint = polygonPoints.get(i);
                        
            polygon.getPoints().add(currentPoint[0]);
            polygon.getPoints().add(currentPoint[1]);
            
        }
                
        wayGroup.getChildren().add(polygon);
              
        return wayGroup;
        
    }
    
    public void createResizedPoints(List<OsmNode> nodeList, List<TrajectoryPoint> trajectoryPointList){
        
//        resizedNodeList
        
        double minX = 100000;
        double minY = 100000;
        double maxX = -100000;
        double maxY = -100000;
        
        for ( int i = 0; i < nodeList.size(); i++ ){
            
            double currentX = nodeList.get(i).X;
            double currentY = nodeList.get(i).Y;
            
            if (currentX < minX){
                minX = currentX;
            }
            if (currentY < minY){
                minY = currentY;
            }
            if (currentX > minX){
                maxX = currentX;
            }
            if (currentY > minY){
                maxY = currentY;
            }
            
            
        }
        
        double shrinkingX = worldViewWidth/(maxX-minX);
        double shrinkingY = worldViewHeight/(maxY-minY);
        
        double shriking;
        
        if (shrinkingX < shrinkingY){
            shriking = shrinkingX;
        }else{            
            shriking = shrinkingY;
        }
        
        
        resizedTrajectoryPointList = new ArrayList();
        
        for ( int i = 0; i < trajectoryPointList.size() ; i++ ){
            
            TrajectoryPoint tempPoint = trajectoryPointList.get(i);
//            TrajectoryPoint newPoint = new TrajectoryPoint(tempPoint.time, (tempPoint.pixelX - minX)*shriking, (tempPoint.pixelY - minY)*shriking);
                        
            double pixelX = Globals.smlWorldInfo.centerPixelX + tempPoint.X*Globals.smlWorldInfo.pixelPerMeterX;
            double pixelY = Globals.smlWorldInfo.centerPixelY - tempPoint.Y*Globals.smlWorldInfo.pixelPerMeterY;
//            Node newNode = new Node(tempNode.id, pixelX, pixelY);         
            
            TrajectoryPoint newPoint = new TrajectoryPoint(tempPoint.time, pixelX, pixelY);
            
            resizedTrajectoryPointList.add(newPoint);
            
        }
        
    }
    
    public void clearTrajectory(){
        
        Group trajectoryGroup = (Group) worldViewGroup.lookup("#trajectoryGroup");
        
        if ( trajectoryGroup != null ){
            
            trajectoryGroup.getChildren().clear();
            
        }
        
    }
    
    public void drawTrajectory(ArrayList<double[]> trajectoryPointsList, ArrayList<OsmLanelet> laneletPathIds){
        
        
        System.out.println("drawTrajectory()");
        
        Group trajectoryGroup = (Group) worldViewGroup.lookup("#trajectoryGroup");
        
        if ( trajectoryGroup == null ){
            
            trajectoryGroup = new Group();
            trajectoryGroup.setId("trajectoryGroup");
            worldViewGroup.getChildren().add(trajectoryGroup);
            
        }else{
            
            trajectoryGroup.getChildren().clear();
            
        }
        
//        List<OsmNode> nodeList = Globals.xmlProcesser.getNodeList();
//        List<TrajectoryPoint> trajectoryPointList = Globals.xmlProcesser.getTrajectoryPointList(); 
        
//        createResizedPoints(nodeList, trajectoryPointList);
        
        Polyline polyline = new Polyline();
                
        for ( int i = 0; i < trajectoryPointsList.size(); i++ ){
        
            double[] tempPoint = trajectoryPointsList.get(i);
            
            double pixelX = convertToPixelX(tempPoint[0]);
            double pixelY = convertToPixelY(tempPoint[1]);
//            double pixelX = convertToPixelX(tempPoint.X/32.);
//            double pixelY = convertToPixelY(tempPoint.Y/32.);
            
            polyline.getPoints().addAll(new Double[]{ pixelX, pixelY });
                
        }
        
        polyline.setStrokeWidth(5.0);
        polyline.setStroke(Color.ORANGERED);
        polyline.setOpacity(0.5);
        
        trajectoryGroup.getChildren().add( polyline );

        Globals.drawingManager.createTrajectoryButton(laneletPathIds);
        
        
    }
   
    public void drawTrajectory(){
        
        System.out.println("drawTrajectory()");
        
        Group trajectoryGroup = (Group) worldViewGroup.lookup("#trajectoryGroup");
        
        if ( trajectoryGroup == null ){
            
            trajectoryGroup = new Group();
            trajectoryGroup.setId("trajectoryGroup");
            worldViewGroup.getChildren().add(trajectoryGroup);
            
        }else{
            
            trajectoryGroup.getChildren().clear();
            
        }
        
        List<OsmNode> nodeList = Globals.xmlProcesser.getNodeList();
        List<TrajectoryPoint> trajectoryPointList = Globals.xmlProcesser.getTrajectoryPointList(); 
        
        createResizedPoints(nodeList, trajectoryPointList);
        
        Polyline polyline = new Polyline();
        
        int test_stuff = 20;
        
        for ( int i = 0; i < resizedTrajectoryPointList.size(); i++ ){
        
            TrajectoryPoint notTempPoint = trajectoryPointList.get(i);
            
//            System.out.println("Original: notTempPoint.X = " + notTempPoint.X + " notTempPoint.Y = " + notTempPoint.Y );
            
//            TrajectoryPoint tempPoint = resizedTrajectoryPointList.get(i);
            TrajectoryPoint tempPoint = trajectoryPointList.get(i);
            
//            System.out.println("Resized: tempPoint.X = " + tempPoint.X + " tempPoint.Y = " + tempPoint.Y );
//            double[] resizedValues = resizeToScreen(tempPoint.pixelX, tempPoint.pixelY);  
//            Circle tempCircle = new Circle(resizedValues[0], resizedValues[1], 2.0);
            
//            double pixelX = Globals.smlWorldInfo.centerPixelX + tempPoint.X*Globals.smlWorldInfo.pixelPerMeterX;
//            double pixelY = Globals.smlWorldInfo.centerPixelY - tempPoint.Y*Globals.smlWorldInfo.pixelPerMeterY;
//            
//            pixelX = pixelX*(windowWidth/1920.0);
//            
//            System.out.println("Original: pixelX = " + pixelX + " pixelY = " + pixelY );
            
            double pixelX = convertToPixelX(tempPoint.X/32.);
            double pixelY = convertToPixelY(tempPoint.Y/32.);
            
//            System.out.println("Original: pixelXNew = " + pixelXNew + " pixelYNew = " + pixelYNew );
            
            
//            Circle tempCircle = new Circle(pixelX, pixelY, 2.0);
//            trajectoryRoot.getChildren().add( tempCircle );
            
            polyline.getPoints().addAll(new Double[]{ pixelX, pixelY });
//            Circle tempCircle = new Circle(tempPoint.pixelX, tempPoint.pixelY, 2.0);
                
        }
        
        polyline.setStrokeWidth(5.0);
        polyline.setStroke(Color.ORANGERED);
        polyline.setOpacity(0.5);
        
        trajectoryGroup.getChildren().add( polyline );
        
        Globals.drawingManager.createTrajectoryButton(null);
                
    }
    
    
    public void drawReverseTrajectory(){
        
        System.out.println("drawReverseTrajectory()");
        
        Group trajectoryGroup = (Group) worldViewGroup.lookup("#trajectoryGroup");
        
        if ( trajectoryGroup == null ){
            
            trajectoryGroup = new Group();
            trajectoryGroup.setId("trajectoryGroup");
            worldViewGroup.getChildren().add(trajectoryGroup);
            
        }else{
            
            trajectoryGroup.getChildren().clear();
            
        }
        
//        List<OsmNode> nodeList = Globals.xmlProcesser.getNodeList();
        List<TrajectoryPoint> trajectoryPointList = Globals.xmlProcesser.getTrajectoryPointList(); 
        
//        createResizedPoints(nodeList, trajectoryPointList);
        
        Polyline polyline = new Polyline();
        
        int test_stuff = 20;
        
        for ( int i = 0; i < trajectoryPointList.size(); i++ ){
            
            TrajectoryPoint notTempPoint = trajectoryPointList.get(i);
            
            System.out.println("Original: notTempPoint.X = " + notTempPoint.X + " notTempPoint.Y = " + notTempPoint.Y );
        
//            TrajectoryPoint tempPoint = resizedTrajectoryPointList.get(i);
            TrajectoryPoint tempPoint = trajectoryPointList.get(i);
            
//            double[] resizedValues = resizeToScreen(tempPoint.pixelX, tempPoint.pixelY);  
//            Circle tempCircle = new Circle(resizedValues[0], resizedValues[1], 2.0);
            
//            double pixelX = Globals.smlWorldInfo.centerPixelX + tempPoint.X*Globals.smlWorldInfo.pixelPerMeterX;
//            double pixelY = Globals.smlWorldInfo.centerPixelY - tempPoint.Y*Globals.smlWorldInfo.pixelPerMeterY;
//            
//            pixelX = pixelX*(windowWidth/1920.0);
//            
//            System.out.println("Resized: tempPoint.X = " + tempPoint.X + " tempPoint.Y = " + tempPoint.Y );
            
            
            
            double pixelX = convertToPixelX(tempPoint.X);
            double pixelY = convertToPixelY(tempPoint.Y);
            
//            System.out.println("Original: pixelX = " + pixelX + " pixelY = " + pixelY );
            
            
//            Circle tempCircle = new Circle(pixelX, pixelY, 2.0);
//            trajectoryRoot.getChildren().add( tempCircle );
            
            polyline.getPoints().addAll(new Double[]{ pixelX, pixelY });
//            Circle tempCircle = new Circle(tempPoint.pixelX, tempPoint.pixelY, 2.0);
                
        }
        
        polyline.setStrokeWidth(5.0);
        polyline.setStroke(Color.ORANGERED);
        polyline.setOpacity(0.5);
        
        trajectoryGroup.getChildren().add( polyline );
        
        Globals.drawingManager.createTrajectoryButton(null);
                
    }
    
    public void clearRrtSolution(){
                
        worldViewGroup.getChildren().remove( worldViewGroup.lookup("#rrtGroup") );
        
    }
    
    public void drawRrtSolution() throws InterruptedException{
        
//        Group statesRootGroup = (Group) worldViewGroup.lookup("#statesRootGroup");
        
        Group rrtSolutionGroup = (Group) worldViewGroup.lookup("#rrtGroup");
        
        if ( rrtSolutionGroup != null ){
            
            rrtSolutionGroup.getChildren().clear();
            
        }else{
            
            rrtSolutionGroup = new Group();
            rrtSolutionGroup.setId("rrtGroup");
            worldViewGroup.getChildren().add(rrtSolutionGroup);
            
        }
        
        
        ArrayList<RrtNode> rrtTree = Globals.currentRrtSolution.getTreeNodes();
        
        for ( int i = 1 ; i < rrtTree.size() ; i++ ){
            
            RrtNode currentNode = rrtTree.get(i);
            RrtNode parentNode = rrtTree.get(currentNode.parentId);
            
            Line currentBranchLine = new Line();
            currentBranchLine.setStartX( convertToPixelX(currentNode.x) );
            currentBranchLine.setStartY( convertToPixelY(currentNode.y) );
            currentBranchLine.setEndX( convertToPixelX(parentNode.x) );
            currentBranchLine.setEndY( convertToPixelY(parentNode.y) );
            currentBranchLine.setStroke(Color.LIGHTBLUE);
            
            rrtSolutionGroup.getChildren().add(currentBranchLine);
            
        }
        
        ArrayList<Integer> rrtSolution = Globals.currentRrtSolution.getSolutionNodes();
        
        for ( int i = 1 ; i < rrtSolution.size() ; i++ ){
            
            RrtNode currentNode = rrtTree.get( rrtSolution.get(i) );
            RrtNode parentNode = rrtTree.get( rrtSolution.get(i-1) );
            
            Line currentBranchLine = new Line();
            currentBranchLine.setStartX( convertToPixelX(currentNode.x) );
            currentBranchLine.setStartY( convertToPixelY(currentNode.y) );
            currentBranchLine.setEndX( convertToPixelX(parentNode.x) );
            currentBranchLine.setEndY( convertToPixelY(parentNode.y) );
            currentBranchLine.setStroke(Color.FIREBRICK);
            currentBranchLine.setStrokeWidth(2.5);
//            , currentNode.y, parentNode.x, parentNode.y);
//            rrtSolutionGroup.getChildren().add(currentBranchLine);
            
            rrtSolutionGroup.getChildren().add(currentBranchLine);
            
        }              
        
    }
    
    
}
