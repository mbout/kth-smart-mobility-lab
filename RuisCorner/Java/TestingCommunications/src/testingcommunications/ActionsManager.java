/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testingcommunications;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.InputEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.SwipeEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.util.Duration;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

/**
 *
 * @author rui
 */
public class ActionsManager {
    
    double actionsManagerWidth;
    double actionsManagerHeight;
    double tabHeight;
    
    // SCREENCHANGES
    double standardFontReduction = Globals.screenWidth/1920.0;
    
    Group actionsManagerGroup;
    Group trucksMenuGroup;
    
    double menuLayoutX = 0;
    double menuLayoutY = 0;
    
    double standardButtonHeight = 100;
    
    long afterTrajectoryMessageStop = 1000;
    
    BorderPane borderPane;
    
    List<TruckInformation> truckInformationList;
    
    List<Boolean> truckGoABoolean;
    List<Boolean> truckGoBBoolean;
        
    int lastPressedTruck = -1000;
    String lastPressedDestination = null;
    
    List<Integer> completedBodies;
    
    ArrayList<double[]> latestTrajectoryPointsList;
    
    double closeButtonWidth = 50;
        
//    ArrayList<>
    
    MissionGuiManager missionGuiManager;
    
    boolean waitingForReverseTrajectory = false;
    
    double leftButtonPostTrajectoryOffsetX = -800;
    double rightButtonPostTrajectoryOffsetX = 0;
    
    double[] reverseGoalPose = null;
    
    Image truckImageXRayEmpty, truckImageXRayFull;
    
    public ActionsManager(double argWidth, double argHeight){
               
//        truckImageXRayEmpty = new Image("file:truckXray000.png", 100, 0, true, false);
//        truckImageXRayFull = new Image("file:truckXray075.png", 100, 0, true, false);
        truckImageXRayEmpty = new Image("file:truckXray000.png");
        truckImageXRayFull = new Image("file:truckXray075.png");
                
        completedBodies = new ArrayList<>();
        
        truckGoABoolean = new ArrayList<>();
        
        truckGoABoolean.add(false);
        truckGoABoolean.add(false);
        truckGoABoolean.add(false);
        
        truckGoBBoolean = new ArrayList<>();
        
        truckGoBBoolean.add(false);
        truckGoBBoolean.add(false);
        truckGoBBoolean.add(false);
        
        actionsManagerWidth = argWidth;
        actionsManagerHeight = argHeight;
        
        actionsManagerGroup = new Group();
        
        TabPane tabPane = new TabPane();
        
//        tabPane.setMinWidth(500);

        borderPane = new BorderPane();
//        for (int i = 0; i < 5; i++) {
//            Tab tab = new Tab();
//            tab.setText("Tab" + i);
//            HBox hbox = new HBox();
//            hbox.getChildren().add(new Label("Tab" + i));
//            hbox.setAlignment(Pos.CENTER);
//            tab.setContent(hbox);
//            tabPane.getTabs().add(tab);
//        }
        
        Tab tab = new Tab();
        tab.setStyle("-fx-font-size: 15px;-fx-alignment: CENTER;");
        tab.setText("Missions");
        tab.setClosable(false);
//        http://stackoverflow.com/questions/16922138/how-to-set-tab-name-size-in-javafx
        
//        ;
//        HBox hbox = new HBox();
//        hbox.getChildren().add(new Label("Missions"));
//        hbox.setAlignment(Pos.CENTER);
//        tab.setContent(hbox);
//        
        
        tabPane.getTabs().add(tab);
//        tabHeight = tab.getTabPane().getBoundsInLocal().getHeight();
        
        missionGuiManager = new MissionGuiManager(argWidth, argHeight);
                
        tab.setContent( missionGuiManager.getMissionGuiGroup() );
        
//        System.out.println("tab.getProperties().toString() = " + tab.getProperties().toString());
        
        tabHeight = missionGuiManager.getMissionGuiGroup().getBoundsInParent().getMinY();
        
        for ( int i = 0; i < 4; i++ ){
        
            Group groupToAdd = new Group();
            
            Rectangle rectBlue = new Rectangle(actionsManagerWidth, 200);
            rectBlue.setFill(Color.ALICEBLUE);
            
            Rectangle rectRed = new Rectangle(actionsManagerWidth, 200);
            rectRed.setFill(Color.RED);
            rectRed.setLayoutY(200);
            
            groupToAdd.getChildren().add(rectBlue);
            groupToAdd.getChildren().add(rectRed);
            
//            missionGuiManager.addGroupToMissionQueue(groupToAdd);            
        }
        
        
        
        
        tab = new Tab();
        tab.setStyle("-fx-font-size: 15px;-fx-alignment: CENTER;");
        tab.setText("Drivers");
        tab.setClosable(false);
//        tabHeight = tab.getTabPane().getBoundsInLocal().getHeight();
//        tab.
//        hbox = new HBox();
//        hbox.getChildren().add(new Label("Drivers"));
//        hbox.setAlignment(Pos.CENTER);
        createTruckInformationList();
//        tab.setContent( getTrucksMenu() );
        tabPane.getTabs().add(tab);
        
        SingleSelectionModel<Tab> selectionModel = tabPane.getSelectionModel();
        selectionModel.select(1); //select by index starting with 0
        
        // bind to take available space
        borderPane.setPrefWidth(actionsManagerWidth); 
        borderPane.setPrefHeight(actionsManagerHeight);
//        borderPane.prefHeightProperty().bind(scene.heightProperty());
//        borderPane.prefWidthProperty().bind(scene.widthProperty());
        
        borderPane.setCenter(tabPane);
        
//        actionsManagerGroup.getChildren().add(borderPane);
        
        Group trucksMenuTempGroup = getTrucksMenu();
        
        trucksMenuTempGroup.setLayoutX(Globals.screenWidth*Globals.mapXRatio);
        
        actionsManagerGroup.getChildren().add(trucksMenuTempGroup);
        
        
        
        
        
    }
    
    Group getDriversGroup(){
        
        Group driversGroup = new Group();
        
        Rectangle rect = new Rectangle(actionsManagerWidth, actionsManagerHeight);    
        rect.setFill(Color.BLUE);
        
        driversGroup.getChildren().add(rect);
        
        return driversGroup;
        
    }
    
    public void setMenuLayout(double argMenuLayoutX, double argMenuLayoutY){
        
        menuLayoutX = argMenuLayoutX;
        menuLayoutY = argMenuLayoutY;       
        
        borderPane.setLayoutX(menuLayoutX);
        borderPane.setLayoutY(menuLayoutY);
        
    }
    
    public void promptExitOpenAreaCommand(int argBodyCompleted){
        
        TruckInformation currentBodyTruckInformation = null;

        for (TruckInformation tempTruckInformation : truckInformationList){
            
            if ( tempTruckInformation.getTruckId() == argBodyCompleted ){
                
                currentBodyTruckInformation = tempTruckInformation;
                
            }
            
        }
        
        Group truckMenuRoot = (Group) actionsManagerGroup.lookup("#truckMenuRoot");
        Group trajectoryRoot = (Group) actionsManagerGroup.lookup("#trajectoryRoot");
        
        showTruckOrdersMenu(currentBodyTruckInformation);
        
    }
        
    public boolean isIdControllable(int argId){
        
        boolean controllable = false;
        List<QualisysBody> currentQualisysBodiesList = Globals.smlWorldInfo.getQualisysBodiesList();
        
        for (QualisysBody body : currentQualisysBodiesList){
                   
                if ( body.isControllable() && body.getId() == argId ){

                    controllable = true;
                    return controllable;
                }

            }
                
        return controllable;        
        
    }
      
    public void showTruckOrdersMenu(int bodyId){
        
        for ( TruckInformation truckInformation : truckInformationList ){
        
            if ( truckInformation.getTruckId() == bodyId ){
            
                showTruckOrdersMenu(truckInformation);
            
            }
        
        }
        
    }
    
    public void showTruckOrdersMenu(TruckInformation truckInformation){
    
        System.out.println("\n\nshowTruckOrdersMenu()");
        
        actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#trajectoryButton") );
        actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#abortTrajectoryButton") );
        
        Group truckOrdersMenu = (Group) actionsManagerGroup.lookup("#truckOrdersMenu");
        
        if ( truckOrdersMenu != null ){
            
            System.out.println("CLEANING THIS");
            truckOrdersMenu.getChildren().clear();
            
        }else{
            
            truckOrdersMenu = new Group();
            truckOrdersMenu.setId("truckOrdersMenu");
            actionsManagerGroup.getChildren().add(truckOrdersMenu);
            
        }
                
        double truckOrdersMenuWidth = Globals.drawingManager.windowWidth*0.55;
        double truckOrdersMenuHeight = Globals.drawingManager.windowHeight*0.55;
        
        Rectangle truckOrdersMenuBackground = new Rectangle();
        truckOrdersMenuBackground.setWidth(truckOrdersMenuWidth);
        truckOrdersMenuBackground.setHeight(truckOrdersMenuHeight);
//        
        double menuLayoutX = ( Globals.drawingManager.windowWidth -truckOrdersMenuWidth)*0.5;
        double menuLayoutY = ( Globals.drawingManager.windowHeight -truckOrdersMenuHeight)*0.5;
        
        truckOrdersMenuBackground.setLayoutX( menuLayoutX );
        truckOrdersMenuBackground.setLayoutY( menuLayoutY );
        
        truckOrdersMenuBackground.setArcWidth(20);
        truckOrdersMenuBackground.setArcHeight(20); 
        
        truckOrdersMenuBackground.setFill(Color.SLATEGRAY);
        truckOrdersMenuBackground.setStroke(Color.DARKSLATEGRAY);
        truckOrdersMenuBackground.setStrokeWidth(15);
        
        truckOrdersMenu.getChildren().add( truckOrdersMenuBackground );
        
        double humanTruckRatio = 0.3;
        double photoRatio = 0.8;
        
        Image truckDriverImage = truckInformation.getTruckDriverImage();
        ImageView truckDriverImageView = new ImageView( truckDriverImage );
        
        truckDriverImageView.setPreserveRatio(true);
        truckDriverImageView.setFitWidth(humanTruckRatio*truckOrdersMenuWidth*photoRatio);
              
        truckDriverImageView.setLayoutX( menuLayoutX + humanTruckRatio*truckOrdersMenuWidth*0.5 - truckDriverImageView.getFitWidth()/2.0 );
        truckDriverImageView.setLayoutY( menuLayoutY + humanTruckRatio*truckOrdersMenuWidth*0.5 - truckDriverImageView.getFitWidth()/2.0 );
        
        truckOrdersMenu.getChildren().add( truckDriverImageView );
               
        Text nameText = new Text("Responsible: "+truckInformation.getTruckDriverName());
        nameText.setFont(new Font(20));
        nameText.setLayoutX( menuLayoutX + humanTruckRatio*truckOrdersMenuWidth*0.5 - nameText.getBoundsInLocal().getWidth()/2.0 );
        nameText.setLayoutY( truckDriverImageView.getBoundsInParent().getMaxY() + nameText.getBoundsInLocal().getHeight() );
        
        truckOrdersMenu.getChildren().add( nameText );
        
        Text contactText = new Text("Contact: "+truckInformation.getTruckDriverContact());
        contactText.setFont(new Font(20));
        contactText.setLayoutX( menuLayoutX + humanTruckRatio*truckOrdersMenuWidth*0.5 - contactText.getBoundsInLocal().getWidth()/2.0 );
        contactText.setLayoutY( nameText.getBoundsInParent().getMaxY() + contactText.getBoundsInLocal().getHeight() );
        
        if ( truckInformation.getTruckModel().equals("excavator") ){
            
            contactText.setText("EXCAVATOR");
            
        }
        
        truckOrdersMenu.getChildren().add( contactText );
        
        boolean isAvailable = isIdControllable( truckInformation.getTruckId() );
        
        boolean isDetected;
        
        if (isAvailable){
        
            List<QualisysBody> currentQualisysBodiesList = Globals.smlWorldInfo.getQualisysBodiesList();
            QualisysBody currentBody = null;
            for (QualisysBody tempBody : currentQualisysBodiesList){
                if ( tempBody.getId() == truckInformation.getTruckId() ){
                    currentBody = tempBody;
                    System.out.println("currentBody.getX() = " + currentBody.getX());
                    System.out.println("currentBody.getY() = " + currentBody.getY());
                    break;
                }
            }
            // Body is undetected by Qualisys, even though it is controllable         
            isDetected = currentBody.getX() > -9000;
            
        }else{
            
            isDetected = false;
            
        }
        
        
        
        Text statusText = new Text();
        
//        if ( isAvailable ){
//                        
//            if ( isDetected ){
//                       
//                statusText.setText("AVAILABLE");
//                statusText.setFill(Color.GREEN);
//                
//            }else{
//                
//                statusText.setText("DISCONNECTED");
//                statusText.setFill(Color.YELLOW);
//                            
//            }
//            
//        }else{
//            
//            statusText.setText("UNAVAILABLE");
//            statusText.setFill(Color.RED);
//            
//        }
        if ( isAvailable ){
                                                                    
//            isTruckCargoed = Globals.missionManager.hasTruckCargo(body.getId());

            if ( Globals.missionManager.isTruckAvailable(truckInformation.getTruckId()) ){

                if ( !isDetected ){

//                    statusRectangle.setFill(Color.YELLOW);
//                    statusText.setFill(Color.YELLOW);
//                    statusText.setText("UNAVAILABLE");
                    statusText.setText("UNAVAILABLE");
                    statusText.setFill(Color.YELLOW);

                }else{
//                    statusRectangle.setFill(Color.GREEN);
//                    statusText.setFill(Color.GREEN);
//                    statusText.setText("AVAILABLE");
                    statusText.setText("AVAILABLE");
                    statusText.setFill(Color.GREEN);
                }

            }else{

//                statusRectangle.setFill(Color.CYAN);
//                statusText.setFill(Color.CYAN);
//                statusText.setText("BUSY");
                statusText.setText("BUSY");
                statusText.setFill(Color.CYAN);
                
            }

        }else{

//            statusRectangle.setFill(Color.RED);
//            statusText.setFill(Color.RED);
//            statusText.setText("OFFLINE");
            statusText.setText("OFFLINE");
            statusText.setFill(Color.RED);

        }
        
        statusText.setFont( Font.font("Verdana", FontWeight.BOLD, 30*standardFontReduction) );
//        Font tempFont = new Font(20);
        statusText.setLayoutX( menuLayoutX + humanTruckRatio*truckOrdersMenuWidth*0.5 - statusText.getBoundsInLocal().getWidth()/2.0 );
        statusText.setLayoutY( contactText.getBoundsInParent().getMaxY() + statusText.getBoundsInLocal().getHeight() );
        
        truckOrdersMenu.getChildren().add( statusText );
        
//        DRAWING THE TRUCK SIDE NOW
        
//        Image truckImage = truckInformation.getTruckAndDriverImage();
        boolean isTruckWithCargo = Globals.missionManager.hasTruckCargo( truckInformation.getTruckId() );
        
        Image truckImage;
        
        if ( isTruckWithCargo ){
            
            truckImage = truckImageXRayFull;
            
        }else{
            
            truckImage = truckImageXRayEmpty;
            
        }
        
        
        ImageView truckImageView = new ImageView( truckImage );
        
        double truckPhotoRatio = 0.6;
        
        truckImageView.setPreserveRatio(true);
        truckImageView.setFitWidth( (1.0-humanTruckRatio)*truckOrdersMenuWidth*truckPhotoRatio);
              
        truckImageView.setLayoutX( menuLayoutX + humanTruckRatio*truckOrdersMenuWidth + (1.0-humanTruckRatio)*truckOrdersMenuWidth*0.5 - truckImageView.getFitWidth()/2.0 );
        truckImageView.setLayoutY( menuLayoutY + humanTruckRatio*truckOrdersMenuWidth*0.1 );
        
        truckOrdersMenu.getChildren().add( truckImageView );
        
        double truckSectionCenterX = menuLayoutX + humanTruckRatio*truckOrdersMenuWidth + (1-humanTruckRatio)*truckOrdersMenuWidth*0.5;
        
        Text truckModelText = new Text("Model: "+truckInformation.getTruckModel());
        truckModelText.setFont(new Font(20));
        truckModelText.setLayoutX( truckSectionCenterX - truckModelText.getBoundsInLocal().getWidth()/2.0 );
        truckModelText.setLayoutY( truckImageView.getBoundsInParent().getMaxY() + truckModelText.getBoundsInLocal().getHeight() );
        
        truckOrdersMenu.getChildren().add( truckModelText );
        
        Text gasText = new Text("Fuel left: "+((int)truckInformation.getTruckFuel())+ "L");
        gasText.setFont(new Font(20));
        gasText.setLayoutX( truckSectionCenterX - gasText.getBoundsInLocal().getWidth()/2.0 );
        gasText.setLayoutY( truckModelText.getBoundsInParent().getMaxY() + gasText.getBoundsInLocal().getHeight() );
        
        truckOrdersMenu.getChildren().add( gasText );

        
        //        DRAWING THE COMMANDS NOW
        
        double buttonsHeight = 100;
        int numberButtons = 3;
        double buttonsWidth = truckOrdersMenuWidth*(0.8/((double)numberButtons));
        double buttonSpacing = truckOrdersMenuWidth - ((double)numberButtons)*buttonsWidth ;
        buttonSpacing = buttonSpacing/((double)(numberButtons+1));
        
        
        
        EventHandler returnButtonEvent = new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent arg0) {
               
                removeTruckOrdersMenu();
                
                // Clear the trajectory from the screen
                Globals.drawingManager.clearTrajectory();
                
                boolean highlight = false;
                
                processStateClick(truckInformation.getTruckId(), highlight);
                
            }
        };

        
        
        
        for ( int i = 0; i < numberButtons ; i++ ){
            
            Group buttonGroup = new Group();
            
            Rectangle buttonRectangle = new Rectangle();
            buttonRectangle.setWidth(buttonsWidth);
            buttonRectangle.setHeight(buttonsHeight);
            
            buttonRectangle.setLayoutX(menuLayoutX + ((double)(i+1))*buttonSpacing + ((double)i)*buttonsWidth );
            buttonRectangle.setLayoutY(menuLayoutY + truckOrdersMenuHeight - buttonsHeight - buttonSpacing);
            
            buttonRectangle.setFill(Color.SLATEGRAY);
            buttonRectangle.setStroke(Color.DARKSLATEGRAY);
            buttonRectangle.setStrokeWidth(5);
            buttonRectangle.setArcWidth(10);
            buttonRectangle.setArcHeight(10); 
            
            buttonGroup.getChildren().add( buttonRectangle );
            
            Text buttonText = new Text();
            
            
            
            if (i == 0){
                
                String destinationString = "Park";
                
                buttonText.setText( destinationString );
                
                EventHandler firstButtonEvent = new EventHandler<MouseEvent>(){
                    @Override
                    public void handle(MouseEvent arg0) {

                        lastPressedTruck = truckInformation.getTruckId();
                        lastPressedDestination = destinationString;
                        
                        int destinationId = Globals.missionManager.getAvailableDestinationId(destinationString);
                        
                        ArrayList<double[]> trajectoryPointsList = new ArrayList<>();
                        ArrayList<OsmLanelet> laneletPath = new ArrayList<>();
                        
                        for ( QualisysBody qualisysBody : Globals.smlWorldInfo.getQualisysBodiesList() ){
                            if ( qualisysBody.getId() ==  truckInformation.getTruckId() ){
                                trajectoryPointsList = Globals.routePlanner.getTrajectoryFromTruckToNode(qualisysBody, destinationId);
                                laneletPath = Globals.routePlanner.getLastLaneletPath();
                                break;
                            }
                        }

                        Globals.drawingManager.drawTrajectory(trajectoryPointsList, laneletPath);

                        latestTrajectoryPointsList = trajectoryPointsList;
                                
                        removeTruckOrdersMenu();

                    }
                };   
                
                System.out.println("Park button:");
                int destinationId = Globals.missionManager.getAvailableDestinationId(destinationString);
                
                System.out.println("destinationId = " + destinationId);
                
                boolean deliverAvailability = true;
                deliverAvailability = isAvailable && isDetected && Globals.missionManager.isTruckAvailable( truckInformation.getTruckId() );
                
                System.out.println("deliverAvailability = " + deliverAvailability);
                
//                deliverAvailability = deliverAvailability && !Globals.missionManager.isTruckOnParking(truckInformation.getTruckId());
//                deliverAvailability = deliverAvailability && !Globals.missionManager.isTruckOnFreeArea(truckInformation.getTruckId());
//                deliverAvailability = deliverAvailability && !Globals.missionManager.isTruckOnFreeAreaEntrance(truckInformation.getTruckId());
                boolean isDeliverAreaAvailable = destinationId != 99;
                
//                deliverAvailability = deliverAvailability && isDeliverAreaAvailable;
                
                System.out.println("deliverAvailability = " + deliverAvailability);
                
//                System.out.println("Globals.missionManager.getTruckPosition( truckInformation.getTruckId() ) = " + Globals.missionManager.getTruckPosition( truckInformation.getTruckId() ));
//                System.out.println("Globals.missionManager.isTruckOnA(truckInformation.getTruckId() = " + Globals.missionManager.isTruckOnA(truckInformation.getTruckId()) );
                
                if ( deliverAvailability && !Globals.missionManager.isTruckOnParking(truckInformation.getTruckId() ) ){
                
                    buttonGroup.setOnMouseClicked(firstButtonEvent);
                
                }else{
                    
                    buttonGroup.setOpacity(0.2);
                    
                }
                
            }
            if (i == 1){
                
                String destinationString = "Unload";
                                                
                buttonText.setText( destinationString );
                
                
                EventHandler secondButtonEvent = new EventHandler<MouseEvent>(){
                    @Override
                    public void handle(MouseEvent arg0) {

                        lastPressedTruck = truckInformation.getTruckId();
                        lastPressedDestination = destinationString;
                            
                        int destinationId = Globals.missionManager.getAvailableDestinationId(destinationString);
                        
                        ArrayList<double[]> trajectoryPointsList = new ArrayList<>();
                        ArrayList<OsmLanelet> laneletPath = new ArrayList<>();

                        for ( QualisysBody qualisysBody : Globals.smlWorldInfo.getQualisysBodiesList() ){
                            if ( qualisysBody.getId() ==  truckInformation.getTruckId() ){
                                trajectoryPointsList = Globals.routePlanner.getTrajectoryFromTruckToNode(qualisysBody, destinationId);
                                laneletPath = Globals.routePlanner.getLastLaneletPath();
                                break;
                            }
                        }
                        
                        Globals.drawingManager.drawTrajectory(trajectoryPointsList, laneletPath);

                        latestTrajectoryPointsList = trajectoryPointsList;
                        
                        removeTruckOrdersMenu();

                    }
                };  
                
                
                int destinationId = Globals.missionManager.getAvailableDestinationId(destinationString);
                
                System.out.println("destinationId = " + destinationId);
                
                boolean loadAvailability = true;
                loadAvailability = isAvailable && isDetected && Globals.missionManager.isTruckAvailable( truckInformation.getTruckId() );
                System.out.println("loadAvailability = " + loadAvailability);
//                loadAvailability = loadAvailability && !Globals.missionManager.isTruckOnB(truckInformation.getTruckId());
//                System.out.println("loadAvailability = " + loadAvailability);
////                loadAvailability = loadAvailability && !Globals.missionManager.isTruckOnFreeArea(truckInformation.getTruckId());
//                System.out.println("loadAvailability = " + loadAvailability);
//                loadAvailability = loadAvailability && !Globals.missionManager.isTruckOnFreeAreaEntrance(truckInformation.getTruckId());
//                System.out.println("loadAvailability = " + loadAvailability);
                boolean isLoadAreaAvailable = destinationId != 99;
//                loadAvailability = loadAvailability && isLoadAreaAvailable;
                System.out.println("loadAvailability = " + loadAvailability);
                            
//                System.out.println("Globals.missionManager.getTruckPosition( truckInformation.getTruckId() ) = " + Globals.missionManager.getTruckPosition( truckInformation.getTruckId() ));
//                System.out.println("Globals.missionManager.isTruckOnB(truckInformation.getTruckId() = " + Globals.missionManager.isTruckOnB(truckInformation.getTruckId()) );
                
                boolean hasCargo = Globals.missionManager.hasTruckCargo(truckInformation.getTruckId());
                
                System.out.println("loadAvailability = " + loadAvailability);
                System.out.println("hasCargo = " + hasCargo);
                
//                if ( loadAvailability && hasCargo){
                if ( loadAvailability ){
                
                    buttonGroup.setOnMouseClicked(secondButtonEvent);
                
                }else{
                    
                    buttonGroup.setOpacity(0.2);
                    
                }
                
                
                
            }
            if (i == 2){
                                
                String destinationString = "Load Entrance";
                
                buttonText.setText( destinationString );
                
                
                EventHandler secondButtonEvent = new EventHandler<MouseEvent>(){
                    @Override
                    public void handle(MouseEvent arg0) {

                        lastPressedTruck = truckInformation.getTruckId();
                        lastPressedDestination = destinationString;
                        
                        int destinationId = Globals.missionManager.getAvailableDestinationId(destinationString);
                                       
                        if ( destinationId != 99 ){
                            
                            ArrayList<double[]> trajectoryPointsList = new ArrayList<>();
                            ArrayList<OsmLanelet> laneletPath = new ArrayList<>();

                            for ( QualisysBody qualisysBody : Globals.smlWorldInfo.getQualisysBodiesList() ){
                                if ( qualisysBody.getId() ==  truckInformation.getTruckId() ){
                                    trajectoryPointsList = Globals.routePlanner.getTrajectoryFromTruckToNode(qualisysBody, destinationId);
                                    laneletPath = Globals.routePlanner.getLastLaneletPath();
                                    break;
                                }
                            }

                            Globals.drawingManager.drawTrajectory(trajectoryPointsList, laneletPath);

                            latestTrajectoryPointsList = trajectoryPointsList;
                        
                        }
                        
                        removeTruckOrdersMenu();

                    }
                };  
                
                int destinationId = Globals.missionManager.getAvailableDestinationId(destinationString);
                
                System.out.println("destinationId = " + destinationId);
                
                boolean freespaceAvailability = true;
                freespaceAvailability = isAvailable && isDetected && Globals.missionManager.isTruckAvailable( truckInformation.getTruckId() );
                freespaceAvailability = freespaceAvailability && !Globals.missionManager.isTruckOnLoadEntrance(truckInformation.getTruckId());
                freespaceAvailability = freespaceAvailability && !Globals.missionManager.isTruckOnLoadParking(truckInformation.getTruckId());
                freespaceAvailability = freespaceAvailability && !Globals.missionManager.isTruckOnLoadExit(truckInformation.getTruckId());
                boolean isFreeSpaceAreaAvailable = destinationId != 99;
                freespaceAvailability = freespaceAvailability && isFreeSpaceAreaAvailable;
                
                
                
                if ( freespaceAvailability && !Globals.missionManager.isTruckOnLoadEntrance(truckInformation.getTruckId()) ){
                
                    buttonGroup.setOnMouseClicked(secondButtonEvent);
                
                }else{
                    
                    buttonGroup.setOpacity(0.2);
                    
                }
                
                
                
            }
//            if (i == 3){
//                buttonText.setText("RETURN");
//                
//                buttonGroup.setOnMouseClicked(returnButtonEvent);
//                
//            }
            
//            System.out.println("DRAWING CLOSE BUTTOn");
            
            if (i == 2){
                
                System.out.println("DRAWING CLOSE BUTTOn");
                
                Image closeButtonImage = new Image("file:closeButton.png", 100, 0, true, false);
                ImageView closeButtonImageView = new ImageView(closeButtonImage);
                
                closeButtonImageView.setPreserveRatio(true);
                closeButtonImageView.setFitWidth(closeButtonWidth);
                
                double xPosition = menuLayoutX + truckOrdersMenuWidth - closeButtonWidth/2 - closeButtonWidth;
                double yPosition = menuLayoutY + closeButtonWidth/2;
                                
                closeButtonImageView.setLayoutX(xPosition);
                closeButtonImageView.setLayoutY(yPosition);
                 
                
                Group closeButtonGroup = new Group();
                
                closeButtonGroup.getChildren().add( closeButtonImageView );           

                truckOrdersMenu.getChildren().add( closeButtonGroup );
                
                closeButtonGroup.setOnMouseClicked(returnButtonEvent);
                
            }
            
            buttonText.setFont( Font.font("Verdana", FontWeight.BOLD, 30*standardFontReduction) );

            buttonText.setLayoutX( buttonRectangle.getBoundsInParent().getMinX() + buttonRectangle.getBoundsInParent().getWidth()/2.0 - buttonText.getBoundsInParent().getWidth()/2.0 );
            buttonText.setLayoutY( buttonRectangle.getBoundsInParent().getMinY() + buttonRectangle.getBoundsInParent().getHeight()/2.0 + buttonText.getBoundsInParent().getHeight()/2.0 );

            buttonGroup.getChildren().add( buttonText );           

            truckOrdersMenu.getChildren().add( buttonGroup );
            
        }
        
        truckOrdersMenu.toFront();
                
    }
      
    public void showTruckOrdersMenuFreeAreaEntrance(int bodyId){
        
        for ( TruckInformation truckInformation : truckInformationList ){
        
            if ( truckInformation.getTruckId() == bodyId ){
            
                showTruckOrdersMenuFreeAreaEntrance(truckInformation);
            
            }
        
        }
        
    }
    
    public void initializeReverseGoalPose(){
        
        reverseGoalPose = new double[4];
        
        OsmWay parkingOsmWay = null;
        
        for ( OsmWay tempOsmWay : Globals.xmlProcesser.wayList ){
            
            if ( tempOsmWay.wayType.equals("parking_lot") ){
                
                parkingOsmWay = tempOsmWay;
                
            }
            
        }
        
        if ( parkingOsmWay == null ){
        
            System.err.println("initializeReverseGoalPose() : did not find the parking_spot way");
            
            reverseGoalPose[0] = 0.0;
            reverseGoalPose[1] = 0.0;
        
        }else{
            
            ArrayList<OsmNode> parkingNodes = new ArrayList<>();
            
            for ( OsmNode tempOsmNode : Globals.xmlProcesser.nodeList ){
                
                if ( parkingOsmWay.nodeIds.contains(tempOsmNode.id) ){
                    
                    parkingNodes.add(tempOsmNode);
                    
                }
                
            }
            
            for ( OsmNode tempParkingNode : parkingNodes ){
                
                reverseGoalPose[0] = reverseGoalPose[0] + (tempParkingNode.X/32.)/((double)parkingNodes.size());
                reverseGoalPose[1] = reverseGoalPose[1] + (tempParkingNode.Y/32.)/((double)parkingNodes.size());
                
            }
                        
        }
        
        
        reverseGoalPose[2] = 0.0;
        reverseGoalPose[3] = 0.0;
        
        System.out.println("reverseGoalPose[0] = " + reverseGoalPose[0]);
        System.out.println("reverseGoalPose[1] = " + reverseGoalPose[1]);
        System.out.println("reverseGoalPose[2] = " + reverseGoalPose[2]);
        System.out.println("reverseGoalPose[3] = " + reverseGoalPose[3]);
        
    }
    
    public int getCorrespondingTrailerId(int currentTruckId){
        
        int trailerId;
        
        if ( currentTruckId == Globals.body1QualisysId ){
                                    
            trailerId = Globals.trailer1QualisysId;

        }else{

            if ( currentTruckId == Globals.body2QualisysId ){

                trailerId = Globals.trailer2QualisysId;

            }else{

                if ( currentTruckId == Globals.body3QualisysId ){

                    trailerId = Globals.trailer3QualisysId;

                }else{

                    System.err.println("showTruckOrdersMenuFreeAreaEntrance() : Did not find corresponding trailer ID!");
                    trailerId = 0;

                }

            }

        }
        
        return trailerId;
        
    }
    
    public void showTruckOrdersMenuFreeAreaEntrance(TruckInformation truckInformation){
    
        System.out.println("showTruckOrdersMenuFreeAreaEntrance()");
        
        actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#trajectoryButton") );
        actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#abortTrajectoryButton") );
        
        Group truckOrdersMenu = (Group) actionsManagerGroup.lookup("#truckOrdersMenu");
        
        if ( truckOrdersMenu != null ){
            
            System.out.println("CLEANING THIS");
            truckOrdersMenu.getChildren().clear();
            
        }else{
            
            truckOrdersMenu = new Group();
            truckOrdersMenu.setId("truckOrdersMenu");
            actionsManagerGroup.getChildren().add(truckOrdersMenu);
            
        }
                
        double truckOrdersMenuWidth = Globals.drawingManager.windowWidth*0.55;
        double truckOrdersMenuHeight = Globals.drawingManager.windowHeight*0.55;
        
        Rectangle truckOrdersMenuBackground = new Rectangle();
        truckOrdersMenuBackground.setWidth(truckOrdersMenuWidth);
        truckOrdersMenuBackground.setHeight(truckOrdersMenuHeight);
//        
        double menuLayoutX = ( Globals.drawingManager.windowWidth -truckOrdersMenuWidth)*0.5;
        double menuLayoutY = ( Globals.drawingManager.windowHeight -truckOrdersMenuHeight)*0.5;
        
        truckOrdersMenuBackground.setLayoutX( menuLayoutX );
        truckOrdersMenuBackground.setLayoutY( menuLayoutY );
        
        truckOrdersMenuBackground.setArcWidth(20);
        truckOrdersMenuBackground.setArcHeight(20); 
        
        truckOrdersMenuBackground.setFill(Color.SLATEGRAY);
        truckOrdersMenuBackground.setStroke(Color.DARKSLATEGRAY);
        truckOrdersMenuBackground.setStrokeWidth(15);
        
        truckOrdersMenu.getChildren().add( truckOrdersMenuBackground );
        
        double humanTruckRatio = 0.3;
        double photoRatio = 0.8;
                
        Image truckDriverImage = truckInformation.getTruckDriverImage();
        ImageView truckDriverImageView = new ImageView( truckDriverImage );
        
        truckDriverImageView.setPreserveRatio(true);
        truckDriverImageView.setFitWidth(humanTruckRatio*truckOrdersMenuWidth*photoRatio);
              
        truckDriverImageView.setLayoutX( menuLayoutX + humanTruckRatio*truckOrdersMenuWidth*0.5 - truckDriverImageView.getFitWidth()/2.0 );
        truckDriverImageView.setLayoutY( menuLayoutY + humanTruckRatio*truckOrdersMenuWidth*0.5 - truckDriverImageView.getFitWidth()/2.0 );
        
        truckOrdersMenu.getChildren().add( truckDriverImageView );
               
        Text nameText = new Text("Responsible: "+truckInformation.getTruckDriverName());
        nameText.setFont(new Font(20));
        nameText.setLayoutX( menuLayoutX + humanTruckRatio*truckOrdersMenuWidth*0.5 - nameText.getBoundsInLocal().getWidth()/2.0 );
        nameText.setLayoutY( truckDriverImageView.getBoundsInParent().getMaxY() + nameText.getBoundsInLocal().getHeight() );
        
        truckOrdersMenu.getChildren().add( nameText );
        
        Text contactText = new Text("Contact: "+truckInformation.getTruckDriverContact());
        contactText.setFont(new Font(20));
        contactText.setLayoutX( menuLayoutX + humanTruckRatio*truckOrdersMenuWidth*0.5 - contactText.getBoundsInLocal().getWidth()/2.0 );
        contactText.setLayoutY( nameText.getBoundsInParent().getMaxY() + contactText.getBoundsInLocal().getHeight() );
        
        truckOrdersMenu.getChildren().add( contactText );
        
        boolean isAvailable = isIdControllable( truckInformation.getTruckId() );
        
        boolean isDetected;
        
        if (isAvailable){
        
            List<QualisysBody> currentQualisysBodiesList = Globals.smlWorldInfo.getQualisysBodiesList();
            QualisysBody currentBody = null;
            for (QualisysBody tempBody : currentQualisysBodiesList){
                if ( tempBody.getId() == truckInformation.getTruckId() ){
                    currentBody = tempBody;
                    System.out.println("currentBody.getX() = " + currentBody.getX());
                    System.out.println("currentBody.getY() = " + currentBody.getY());
                    break;
                }
            }
            // Body is undetected by Qualisys, even though it is controllable         
            isDetected = currentBody.getX() > -9000;
            
        }else{
            
            isDetected = false;
            
        }
        
        
        
        Text statusText = new Text();
        
        if ( isAvailable ){
                        
            if ( isDetected ){
                       
                statusText.setText("AVAILABLE");
                statusText.setFill(Color.GREEN);
                
            }else{
                
                statusText.setText("DISCONNECTED");
                statusText.setFill(Color.YELLOW);
                            
            }
            
        }else{
            
            statusText.setText("UNAVAILABLE");
            statusText.setFill(Color.RED);
            
        }
        
        statusText.setFont( Font.font("Verdana", FontWeight.BOLD, 30*standardFontReduction) );
//        Font tempFont = new Font(20);
        statusText.setLayoutX( menuLayoutX + humanTruckRatio*truckOrdersMenuWidth*0.5 - statusText.getBoundsInLocal().getWidth()/2.0 );
        statusText.setLayoutY( contactText.getBoundsInParent().getMaxY() + statusText.getBoundsInLocal().getHeight() );
        
        truckOrdersMenu.getChildren().add( statusText );
        
//        DRAWING THE TRUCK SIDE NOW
        
//        truckXray000.png
        
        
        boolean isTruckWithCargo = Globals.missionManager.hasTruckCargo( truckInformation.getTruckId() );
        
        Image truckImage;
        
        if ( isTruckWithCargo ){
            
            truckImage = truckImageXRayFull;
            
        }else{
            
            truckImage = truckImageXRayEmpty;
            
        }
        
        ImageView truckImageView = new ImageView( truckImage );
        
        double truckPhotoRatio = 0.6;
        
        truckImageView.setPreserveRatio(true);
        truckImageView.setFitWidth( (1.0-humanTruckRatio)*truckOrdersMenuWidth*truckPhotoRatio);
              
        truckImageView.setLayoutX( menuLayoutX + humanTruckRatio*truckOrdersMenuWidth + (1.0-humanTruckRatio)*truckOrdersMenuWidth*0.5 - truckImageView.getFitWidth()/2.0 );
        truckImageView.setLayoutY( menuLayoutY + humanTruckRatio*truckOrdersMenuWidth*0.1 );
        
        truckOrdersMenu.getChildren().add( truckImageView );
        
        double truckSectionCenterX = menuLayoutX + humanTruckRatio*truckOrdersMenuWidth + (1-humanTruckRatio)*truckOrdersMenuWidth*0.5;
        
        Text truckModelText = new Text("Model: "+truckInformation.getTruckModel());
        truckModelText.setFont(new Font(20));
        truckModelText.setLayoutX( truckSectionCenterX - truckModelText.getBoundsInLocal().getWidth()/2.0 );
        truckModelText.setLayoutY( truckImageView.getBoundsInParent().getMaxY() + truckModelText.getBoundsInLocal().getHeight() );
        
        truckOrdersMenu.getChildren().add( truckModelText );
        
        Text gasText = new Text("Fuel left: "+((int)truckInformation.getTruckFuel())+ "L");
        gasText.setFont(new Font(20));
        gasText.setLayoutX( truckSectionCenterX - gasText.getBoundsInLocal().getWidth()/2.0 );
        gasText.setLayoutY( truckModelText.getBoundsInParent().getMaxY() + gasText.getBoundsInLocal().getHeight() );
        
        truckOrdersMenu.getChildren().add( gasText );

        
        //        DRAWING THE COMMANDS NOW
        
        double buttonsHeight = 100;
        int numberButtons = 1;
        double buttonsWidth = truckOrdersMenuWidth*(0.8/((double)numberButtons));
        double buttonSpacing = truckOrdersMenuWidth - ((double)numberButtons)*buttonsWidth ;
        buttonSpacing = buttonSpacing/((double)(numberButtons+1));
        
        
        
        EventHandler returnButtonEvent = new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent arg0) {
               
                removeTruckOrdersMenu();
                
                // Clear the trajectory from the screen
                Globals.drawingManager.clearTrajectory();
                
                boolean highlight = false;
                
                processStateClick(truckInformation.getTruckId(), highlight);
                
            }
        };

        
        
        
        for ( int i = 0; i < numberButtons ; i++ ){
            
            Group buttonGroup = new Group();
            
            Rectangle buttonRectangle = new Rectangle();
            buttonRectangle.setWidth(buttonsWidth);
            buttonRectangle.setHeight(buttonsHeight);
            
            buttonRectangle.setLayoutX(menuLayoutX + ((double)(i+1))*buttonSpacing + ((double)i)*buttonsWidth );
            buttonRectangle.setLayoutY(menuLayoutY + truckOrdersMenuHeight - buttonsHeight - buttonSpacing);
            
            buttonRectangle.setFill(Color.SLATEGRAY);
            buttonRectangle.setStroke(Color.DARKSLATEGRAY);
            buttonRectangle.setStrokeWidth(5);
            buttonRectangle.setArcWidth(10);
            buttonRectangle.setArcHeight(10); 
            
            buttonGroup.getChildren().add( buttonRectangle );
            
            Text buttonText = new Text();
            
            if (i == 0){
                
                buttonText.setText("GO TO LOADING ZONE");
                
                EventHandler firstButtonEvent = new EventHandler<MouseEvent>(){
                    @Override
                    public void handle(MouseEvent arg0) {

                        lastPressedTruck = truckInformation.getTruckId();
                        lastPressedDestination = "PARKING";
                                                
                        ArrayList<double[]> trajectoryPointsList = new ArrayList<>();
                        ArrayList<OsmLanelet> laneletPath = new ArrayList<>();
                        
                        int currentTruckId = truckInformation.getTruckId();

                        for ( QualisysBody qualisysBody : Globals.smlWorldInfo.getQualisysBodiesList() ){
                            if ( qualisysBody.getId() ==  currentTruckId ){
                                
                                if ( reverseGoalPose == null ){
                                    
                                    initializeReverseGoalPose();
                                    
                                }
                                        
                                double[] startPose = new double[4];
//                                [x_trailer y_trailer theta_truck theta_trailer];
                                                                
                                int trailerId = getCorrespondingTrailerId(currentTruckId);
                                
                                QualisysBody trailerBody = null;
                                
                                for ( QualisysBody tempBody : Globals.xmlProcesser.qualisysBodiesList ){
                                    
                                    if ( tempBody.getId() == trailerId ){
                                        
                                        trailerBody = tempBody;
                                        
                                    }
                                    
                                }
                                
                                if ( trailerBody == null ){
                                    
                                    startPose[0] = qualisysBody.getX();
                                    startPose[1] = qualisysBody.getY();
                                    startPose[2] = Math.toRadians( qualisysBody.getTheta() );
                                    startPose[3] = Math.toRadians( qualisysBody.getTheta() );
                                    
                                }else{
                                    
                                    startPose[0] = trailerBody.getX();
                                    startPose[1] = trailerBody.getY();
                                    startPose[2] = Math.toRadians( qualisysBody.getTheta() );
                                    startPose[3] = Math.toRadians( trailerBody.getTheta() );
                                    
                                }
                                
                                double requestTimeout = 4.0;
                                
                                ArrayList<ArrayList<double[]>> obstacleCorners = new ArrayList<>();
                                
                                String reverseRequestString = Globals.xmlProcesser.getReverseTrajectoryRequestMessageString
                                (currentTruckId, trailerId, reverseGoalPose, startPose, obstacleCorners, requestTimeout);
                                
                                Globals.smlCommunications.sendOutputMessage(reverseRequestString);
                                
                                System.out.println("sending reverseRequestString = " + reverseRequestString);
                                
                                waitingForReverseTrajectory = true;
                                
                                Globals.drawingManager.logManager.addLogEntry("Requested truck " + currentTruckId + " to find a parking plan");
                                
                                break;
                                
                            }
                        }

//                        Globals.drawingManager.drawTrajectory(trajectoryPointsList, laneletPath);

                        latestTrajectoryPointsList = trajectoryPointsList;
                                
                        removeTruckOrdersMenu();

                    }
                };   
                
                buttonGroup.setOnMouseClicked(firstButtonEvent);
                
            }
            
//            if (i == 3){
//                buttonText.setText("RETURN");
//                
//                buttonGroup.setOnMouseClicked(returnButtonEvent);
//                
//            }
            
            if (i == 0){
                
                
                Image closeButtonImage = new Image("file:closeButton.png", 100, 0, true, false);
                ImageView closeButtonImageView = new ImageView(closeButtonImage);
                 
                closeButtonImageView.setPreserveRatio(true);
                closeButtonImageView.setFitWidth(closeButtonWidth);
                
                double xPosition = menuLayoutX + truckOrdersMenuWidth - closeButtonWidth/2 - closeButtonWidth;
                double yPosition = menuLayoutY + closeButtonWidth/2;
                                
                closeButtonImageView.setLayoutX(xPosition);
                closeButtonImageView.setLayoutY(yPosition);
                 
                
                Group closeButtonGroup = new Group();
                
                closeButtonGroup.getChildren().add( closeButtonImageView );           

                truckOrdersMenu.getChildren().add( closeButtonGroup );
                
                closeButtonGroup.setOnMouseClicked(returnButtonEvent);
                
            }
            
            buttonText.setFont( Font.font("Verdana", FontWeight.BOLD, 30*standardFontReduction) );

            buttonText.setLayoutX( buttonRectangle.getBoundsInParent().getMinX() + buttonRectangle.getBoundsInParent().getWidth()/2.0 - buttonText.getBoundsInParent().getWidth()/2.0 );
            buttonText.setLayoutY( buttonRectangle.getBoundsInParent().getMinY() + buttonRectangle.getBoundsInParent().getHeight()/2.0 + buttonText.getBoundsInParent().getHeight()/2.0 );

            buttonGroup.getChildren().add( buttonText );           

            truckOrdersMenu.getChildren().add( buttonGroup );
            
        }
        
        truckOrdersMenu.toFront();
                
    }
     
    
    public void showExcavatorOrdersMenu(TruckInformation truckInformation){
    
        System.out.println("showExcavatorOrdersMenu()");
        
        actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#trajectoryButton") );
        actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#abortTrajectoryButton") );
        
        Group truckOrdersMenu = (Group) actionsManagerGroup.lookup("#truckOrdersMenu");
        
        if ( truckOrdersMenu != null ){
            
            System.out.println("CLEANING THIS");
            truckOrdersMenu.getChildren().clear();
            
        }else{
            
            truckOrdersMenu = new Group();
            truckOrdersMenu.setId("truckOrdersMenu");
            actionsManagerGroup.getChildren().add(truckOrdersMenu);
            
        }
                
        double truckOrdersMenuWidth = Globals.drawingManager.windowWidth*0.55;
        double truckOrdersMenuHeight = Globals.drawingManager.windowHeight*0.55;
        
        Rectangle truckOrdersMenuBackground = new Rectangle();
        truckOrdersMenuBackground.setWidth(truckOrdersMenuWidth);
        truckOrdersMenuBackground.setHeight(truckOrdersMenuHeight);
//        
        double menuLayoutX = ( Globals.drawingManager.windowWidth -truckOrdersMenuWidth)*0.5;
        double menuLayoutY = ( Globals.drawingManager.windowHeight -truckOrdersMenuHeight)*0.5;
        
        truckOrdersMenuBackground.setLayoutX( menuLayoutX );
        truckOrdersMenuBackground.setLayoutY( menuLayoutY );
        
        truckOrdersMenuBackground.setArcWidth(20);
        truckOrdersMenuBackground.setArcHeight(20); 
        
        truckOrdersMenuBackground.setFill(Color.SLATEGRAY);
        truckOrdersMenuBackground.setStroke(Color.DARKSLATEGRAY);
        truckOrdersMenuBackground.setStrokeWidth(15);
        
        truckOrdersMenu.getChildren().add( truckOrdersMenuBackground );
        
        double humanTruckRatio = 0.0;
        
        boolean isAvailable = isIdControllable( truckInformation.getTruckId() );
        
        boolean isDetected;
        
        if (isAvailable){
        
            List<QualisysBody> currentQualisysBodiesList = Globals.smlWorldInfo.getQualisysBodiesList();
            QualisysBody currentBody = null;
            for (QualisysBody tempBody : currentQualisysBodiesList){
                if ( tempBody.getId() == truckInformation.getTruckId() ){
                    currentBody = tempBody;
                    System.out.println("currentBody.getX() = " + currentBody.getX());
                    System.out.println("currentBody.getY() = " + currentBody.getY());
                    break;
                }
            }
            // Body is undetected by Qualisys, even though it is controllable         
            isDetected = currentBody.getX() > -9000;
            
        }else{
            
            isDetected = false;
            
        }
        
        
        
        
        
//        DRAWING THE TRUCK SIDE NOW
        
        Image truckImage = truckInformation.getTruckAndDriverImage();
                
        ImageView truckImageView = new ImageView( truckImage );
        
        double truckPhotoRatio = 0.4;
        
        truckImageView.setPreserveRatio(true);
        truckImageView.setFitWidth( (1.0-humanTruckRatio)*truckOrdersMenuWidth*truckPhotoRatio);
              
        truckImageView.setLayoutX( menuLayoutX + humanTruckRatio*truckOrdersMenuWidth + (1.0-humanTruckRatio)*truckOrdersMenuWidth*0.5 - truckImageView.getFitWidth()/2.0 );
        truckImageView.setLayoutY( menuLayoutY + truckOrdersMenuHeight*0.1 );
        
        truckOrdersMenu.getChildren().add( truckImageView );
        
        Text statusText = new Text();
        
        statusText.setText("AVAILABLE");
        statusText.setFill(Color.GREEN);
        
        statusText.setFont( Font.font("Verdana", FontWeight.BOLD, 30*standardFontReduction) );
//        Font tempFont = new Font(20);
        statusText.setLayoutX( menuLayoutX + truckOrdersMenuWidth*0.5 - statusText.getBoundsInLocal().getWidth()/2.0 );
        statusText.setLayoutY( truckImageView.getBoundsInParent().getMaxY() + statusText.getBoundsInLocal().getHeight() );
        
        truckOrdersMenu.getChildren().add( statusText );
        
        double truckSectionCenterX = menuLayoutX + humanTruckRatio*truckOrdersMenuWidth + (1-humanTruckRatio)*truckOrdersMenuWidth*0.5;
        
        Text truckModelText = new Text("Model: CAT 336E L");
        truckModelText.setFont(new Font(20));
        truckModelText.setLayoutX( truckSectionCenterX - truckModelText.getBoundsInLocal().getWidth()/2.0 );
        truckModelText.setLayoutY( statusText.getBoundsInParent().getMaxY() + truckModelText.getBoundsInLocal().getHeight() );
        
        truckOrdersMenu.getChildren().add( truckModelText );
        
        Text gasText = new Text("Fuel left: "+((int)truckInformation.getTruckFuel())+ "L");
        gasText.setFont(new Font(20));
        gasText.setLayoutX( truckSectionCenterX - gasText.getBoundsInLocal().getWidth()/2.0 );
        gasText.setLayoutY( truckModelText.getBoundsInParent().getMaxY() + gasText.getBoundsInLocal().getHeight() );
        
        truckOrdersMenu.getChildren().add( gasText );
        
        //        DRAWING THE COMMANDS NOW
        
        double buttonsHeight = 100;
        int numberButtons = 1;
        double buttonsWidth = truckOrdersMenuWidth*(0.8/((double)numberButtons));
        double buttonSpacing = truckOrdersMenuWidth - ((double)numberButtons)*buttonsWidth ;
        buttonSpacing = buttonSpacing/((double)(numberButtons+1));
        
        
        
        EventHandler returnButtonEvent = new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent arg0) {
               
                removeTruckOrdersMenu();
                
                // Clear the trajectory from the screen
                Globals.drawingManager.clearTrajectory();
                
                boolean highlight = false;
                
                processStateClick(truckInformation.getTruckId(), highlight);
                
            }
        };

        
        
        
        for ( int i = 0; i < numberButtons ; i++ ){
            
            Group buttonGroup = new Group();
            
            Rectangle buttonRectangle = new Rectangle();
            buttonRectangle.setWidth(buttonsWidth);
            buttonRectangle.setHeight(buttonsHeight);
            
            buttonRectangle.setLayoutX(menuLayoutX + ((double)(i+1))*buttonSpacing + ((double)i)*buttonsWidth );
            buttonRectangle.setLayoutY(menuLayoutY + truckOrdersMenuHeight - buttonsHeight - buttonSpacing);
            
            buttonRectangle.setFill(Color.SLATEGRAY);
            buttonRectangle.setStroke(Color.DARKSLATEGRAY);
            buttonRectangle.setStrokeWidth(5);
            buttonRectangle.setArcWidth(10);
            buttonRectangle.setArcHeight(10); 
            
            buttonGroup.getChildren().add( buttonRectangle );
            
            Text buttonText = new Text();
            
            if (i == 0){
                
                if ( truckInformation.getTruckId() == 98 ){
                    
                    buttonText.setText("LOAD CARGO");
                    
                }else{
                                        
                    buttonText.setText("UNLOAD CARGO");
                    
                }
                
                
                
                EventHandler firstButtonEvent = new EventHandler<MouseEvent>(){
                    @Override
                    public void handle(MouseEvent arg0) {
                        
                        double desiredNextYaw;
                        int bodyId;
                                                
                        if ( truckInformation.getTruckId() == 98 ){
                    
                            desiredNextYaw = -89;
                            bodyId = -200;
//                            buttonText.setText("LOAD CARGO");
                            Globals.drawingManager.logManager.addLogEntry("Excavator is loading cargo");

                        }else{
                            
                            desiredNextYaw = 179;
                            bodyId = -201;
//                            buttonText.setText("UNLOAD CARGO");
                            
                            Globals.drawingManager.logManager.addLogEntry("Excavator is unloading cargo");

                        }
                        
                        ArrayList<Double> desiredYaws = new ArrayList<>();
                        desiredYaws.add(desiredNextYaw);
                        
                        ArrayList<Integer> bodyIds = new ArrayList<>();
                        bodyIds.add(bodyId);
                        
                        String excavatorMessageString = Globals.xmlProcesser.getConstructionCommandMessageString(bodyIds, desiredYaws);
                                
                        Globals.smlCommunications.sendOutputMessage(excavatorMessageString);
//
                        System.out.println("Sending excavator command = " + excavatorMessageString);
                        
                        removeTruckOrdersMenu();

                    }
                };   
                
                buttonGroup.setOnMouseClicked(firstButtonEvent);
                
            }
            
//            if (i == 3){
//                buttonText.setText("RETURN");
//                
//                buttonGroup.setOnMouseClicked(returnButtonEvent);
//                
//            }
            
            if (i == 0){
                
                
                Image closeButtonImage = new Image("file:closeButton.png", 100, 0, true, false);
                ImageView closeButtonImageView = new ImageView(closeButtonImage);
                 
                closeButtonImageView.setPreserveRatio(true);
                closeButtonImageView.setFitWidth(closeButtonWidth);
                
                double xPosition = menuLayoutX + truckOrdersMenuWidth - closeButtonWidth/2 - closeButtonWidth;
                double yPosition = menuLayoutY + closeButtonWidth/2;
                                
                closeButtonImageView.setLayoutX(xPosition);
                closeButtonImageView.setLayoutY(yPosition);
                 
                
                Group closeButtonGroup = new Group();
                
                closeButtonGroup.getChildren().add( closeButtonImageView );           

                truckOrdersMenu.getChildren().add( closeButtonGroup );
                
                closeButtonGroup.setOnMouseClicked(returnButtonEvent);
                
            }
            
            buttonText.setFont( Font.font("Verdana", FontWeight.BOLD, 30*standardFontReduction) );

            buttonText.setLayoutX( buttonRectangle.getBoundsInParent().getMinX() + buttonRectangle.getBoundsInParent().getWidth()/2.0 - buttonText.getBoundsInParent().getWidth()/2.0 );
            buttonText.setLayoutY( buttonRectangle.getBoundsInParent().getMinY() + buttonRectangle.getBoundsInParent().getHeight()/2.0 + buttonText.getBoundsInParent().getHeight()/2.0 );

            buttonGroup.getChildren().add( buttonText );           

            truckOrdersMenu.getChildren().add( buttonGroup );
            
        }
        
        truckOrdersMenu.toFront();
                
    }
     
    
    public void showTruckOrdersMenuFreeAreaParking(int bodyId){
        
        for ( TruckInformation truckInformation : truckInformationList ){
        
            if ( truckInformation.getTruckId() == bodyId ){
            
                showTruckOrdersMenuFreeAreaParking(truckInformation);
            
            }
        
        }
        
    }
    
    public void showTruckOrdersMenuFreeAreaParking(TruckInformation truckInformation){
    
        System.out.println("showTruckOrdersMenuFreeAreaEntrance()");
        
        actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#trajectoryButton") );
        actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#abortTrajectoryButton") );
        
        Group truckOrdersMenu = (Group) actionsManagerGroup.lookup("#truckOrdersMenu");
        
        if ( truckOrdersMenu != null ){
            
            System.out.println("CLEANING THIS");
            truckOrdersMenu.getChildren().clear();
            
        }else{
            
            truckOrdersMenu = new Group();
            truckOrdersMenu.setId("truckOrdersMenu");
            actionsManagerGroup.getChildren().add(truckOrdersMenu);
            
        }
                
        double truckOrdersMenuWidth = Globals.drawingManager.windowWidth*0.55;
        double truckOrdersMenuHeight = Globals.drawingManager.windowHeight*0.55;
        
        Rectangle truckOrdersMenuBackground = new Rectangle();
        truckOrdersMenuBackground.setWidth(truckOrdersMenuWidth);
        truckOrdersMenuBackground.setHeight(truckOrdersMenuHeight);
//        
        double menuLayoutX = ( Globals.drawingManager.windowWidth -truckOrdersMenuWidth)*0.5;
        double menuLayoutY = ( Globals.drawingManager.windowHeight -truckOrdersMenuHeight)*0.5;
        
        truckOrdersMenuBackground.setLayoutX( menuLayoutX );
        truckOrdersMenuBackground.setLayoutY( menuLayoutY );
        
        truckOrdersMenuBackground.setArcWidth(20);
        truckOrdersMenuBackground.setArcHeight(20); 
        
        truckOrdersMenuBackground.setFill(Color.SLATEGRAY);
        truckOrdersMenuBackground.setStroke(Color.DARKSLATEGRAY);
        truckOrdersMenuBackground.setStrokeWidth(15);
        
        truckOrdersMenu.getChildren().add( truckOrdersMenuBackground );
        
        double humanTruckRatio = 0.3;
        double photoRatio = 0.8;
        
        Image truckDriverImage = truckInformation.getTruckDriverImage();
        ImageView truckDriverImageView = new ImageView( truckDriverImage );
        
        truckDriverImageView.setPreserveRatio(true);
        truckDriverImageView.setFitWidth(humanTruckRatio*truckOrdersMenuWidth*photoRatio);
              
        truckDriverImageView.setLayoutX( menuLayoutX + humanTruckRatio*truckOrdersMenuWidth*0.5 - truckDriverImageView.getFitWidth()/2.0 );
        truckDriverImageView.setLayoutY( menuLayoutY + humanTruckRatio*truckOrdersMenuWidth*0.5 - truckDriverImageView.getFitWidth()/2.0 );
        
        truckOrdersMenu.getChildren().add( truckDriverImageView );
               
        Text nameText = new Text("Responsible: "+truckInformation.getTruckDriverName());
        nameText.setFont(new Font(20));
        nameText.setLayoutX( menuLayoutX + humanTruckRatio*truckOrdersMenuWidth*0.5 - nameText.getBoundsInLocal().getWidth()/2.0 );
        nameText.setLayoutY( truckDriverImageView.getBoundsInParent().getMaxY() + nameText.getBoundsInLocal().getHeight() );
        
        truckOrdersMenu.getChildren().add( nameText );
        
        Text contactText = new Text("Contact: "+truckInformation.getTruckDriverContact());
        contactText.setFont(new Font(20));
        contactText.setLayoutX( menuLayoutX + humanTruckRatio*truckOrdersMenuWidth*0.5 - contactText.getBoundsInLocal().getWidth()/2.0 );
        contactText.setLayoutY( nameText.getBoundsInParent().getMaxY() + contactText.getBoundsInLocal().getHeight() );
        
        truckOrdersMenu.getChildren().add( contactText );
        
        boolean isAvailable = isIdControllable( truckInformation.getTruckId() );
        
        boolean isDetected;
        
        if (isAvailable){
        
            List<QualisysBody> currentQualisysBodiesList = Globals.smlWorldInfo.getQualisysBodiesList();
            QualisysBody currentBody = null;
            for (QualisysBody tempBody : currentQualisysBodiesList){
                if ( tempBody.getId() == truckInformation.getTruckId() ){
                    currentBody = tempBody;
                    System.out.println("currentBody.getX() = " + currentBody.getX());
                    System.out.println("currentBody.getY() = " + currentBody.getY());
                    break;
                }
            }
            // Body is undetected by Qualisys, even though it is controllable         
            isDetected = currentBody.getX() > -9000;
            
        }else{
            
            isDetected = false;
            
        }
        
        
        
        Text statusText = new Text();
        
        if ( isAvailable ){
                        
            if ( isDetected ){
                       
                statusText.setText("AVAILABLE");
                statusText.setFill(Color.GREEN);
                
            }else{
                
                statusText.setText("DISCONNECTED");
                statusText.setFill(Color.YELLOW);
                            
            }
            
        }else{
            
            statusText.setText("UNAVAILABLE");
            statusText.setFill(Color.RED);
            
        }
        
        statusText.setFont( Font.font("Verdana", FontWeight.BOLD, 30*standardFontReduction) );
//        Font tempFont = new Font(20);
        statusText.setLayoutX( menuLayoutX + humanTruckRatio*truckOrdersMenuWidth*0.5 - statusText.getBoundsInLocal().getWidth()/2.0 );
        statusText.setLayoutY( contactText.getBoundsInParent().getMaxY() + statusText.getBoundsInLocal().getHeight() );
        
        truckOrdersMenu.getChildren().add( statusText );
        
//        DRAWING THE TRUCK SIDE NOW
        
//        Image truckImage = truckInformation.getTruckAndDriverImage();
        
        boolean isTruckWithCargo = Globals.missionManager.hasTruckCargo( truckInformation.getTruckId() );
        
        Image truckImage;
        
        if ( isTruckWithCargo ){
            
            truckImage = truckImageXRayFull;
            
        }else{
            
            truckImage = truckImageXRayEmpty;
            
        }
        
        
        ImageView truckImageView = new ImageView( truckImage );
        
        double truckPhotoRatio = 0.6;
        
        truckImageView.setPreserveRatio(true);
        truckImageView.setFitWidth( (1.0-humanTruckRatio)*truckOrdersMenuWidth*truckPhotoRatio);
              
        truckImageView.setLayoutX( menuLayoutX + humanTruckRatio*truckOrdersMenuWidth + (1.0-humanTruckRatio)*truckOrdersMenuWidth*0.5 - truckImageView.getFitWidth()/2.0 );
        truckImageView.setLayoutY( menuLayoutY + humanTruckRatio*truckOrdersMenuWidth*0.1 );
        
        truckOrdersMenu.getChildren().add( truckImageView );
        
        double truckSectionCenterX = menuLayoutX + humanTruckRatio*truckOrdersMenuWidth + (1-humanTruckRatio)*truckOrdersMenuWidth*0.5;
        
        Text truckModelText = new Text("Model: "+truckInformation.getTruckModel());
        truckModelText.setFont(new Font(20));
        truckModelText.setLayoutX( truckSectionCenterX - truckModelText.getBoundsInLocal().getWidth()/2.0 );
        truckModelText.setLayoutY( truckImageView.getBoundsInParent().getMaxY() + truckModelText.getBoundsInLocal().getHeight() );
        
        truckOrdersMenu.getChildren().add( truckModelText );
        
        Text gasText = new Text("Fuel left: "+((int)truckInformation.getTruckFuel())+ "L");
        gasText.setFont(new Font(20));
        gasText.setLayoutX( truckSectionCenterX - gasText.getBoundsInLocal().getWidth()/2.0 );
        gasText.setLayoutY( truckModelText.getBoundsInParent().getMaxY() + gasText.getBoundsInLocal().getHeight() );
        
        truckOrdersMenu.getChildren().add( gasText );

        
        //        DRAWING THE COMMANDS NOW
        
        double buttonsHeight = 100;
        int numberButtons = 1;
        double buttonsWidth = truckOrdersMenuWidth*(0.8/((double)numberButtons));
        double buttonSpacing = truckOrdersMenuWidth - ((double)numberButtons)*buttonsWidth ;
        buttonSpacing = buttonSpacing/((double)(numberButtons+1));
        
        
        
        EventHandler returnButtonEvent = new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent arg0) {
               
                removeTruckOrdersMenu();
                
                // Clear the trajectory from the screen
                Globals.drawingManager.clearTrajectory();
                
                boolean highlight = false;
                
                processStateClick(truckInformation.getTruckId(), highlight);
                
            }
        };

        
        
        
        for ( int i = 0; i < numberButtons ; i++ ){
            
            Group buttonGroup = new Group();
            
            Rectangle buttonRectangle = new Rectangle();
            buttonRectangle.setWidth(buttonsWidth);
            buttonRectangle.setHeight(buttonsHeight);
            
            buttonRectangle.setLayoutX(menuLayoutX + ((double)(i+1))*buttonSpacing + ((double)i)*buttonsWidth );
            buttonRectangle.setLayoutY(menuLayoutY + truckOrdersMenuHeight - buttonsHeight - buttonSpacing);
            
            buttonRectangle.setFill(Color.SLATEGRAY);
            buttonRectangle.setStroke(Color.DARKSLATEGRAY);
            buttonRectangle.setStrokeWidth(5);
            buttonRectangle.setArcWidth(10);
            buttonRectangle.setArcHeight(10); 
            
            buttonGroup.getChildren().add( buttonRectangle );
            
            Text buttonText = new Text();
            
            if (i == 0){
                
                buttonText.setText("EXIT LOADING ZONE");
                
                EventHandler firstButtonEvent = new EventHandler<MouseEvent>(){
                    @Override
                    public void handle(MouseEvent arg0) {

                        lastPressedTruck = truckInformation.getTruckId();
                        lastPressedDestination = "EXIT";
                        
//                        int destinationId = Globals.missionManager.getAvailableDestinationId("deliver");
                        
                        ArrayList<double[]> trajectoryPointsList = new ArrayList<>();
                        ArrayList<OsmLanelet> laneletPath = new ArrayList<>();
                        
                        int currentTruckId = truckInformation.getTruckId();
//                        
                        removeTruckOrdersMenu();
                        
                        System.out.println("Will do: promptRrtToExitAreaCommand(currentTruckId)");
//                        promptRrtCommand(currentTruckId);
                        promptRrtToExitAreaCommand(currentTruckId);
                        
//                        Globals.drawingManager.drawTrajectory(trajectoryPointsList, laneletPath);

                        latestTrajectoryPointsList = trajectoryPointsList;
                                

                    }
                };   
                
                buttonGroup.setOnMouseClicked(firstButtonEvent);
                
            }
            
//            if (i == 3){
//                buttonText.setText("RETURN");
//                
//                buttonGroup.setOnMouseClicked(returnButtonEvent);
//                
//            }
            
            if (i == 0){
                
                
                Image closeButtonImage = new Image("file:closeButton.png", 100, 0, true, false);
                ImageView closeButtonImageView = new ImageView(closeButtonImage);
                
                closeButtonImageView.setPreserveRatio(true);
                closeButtonImageView.setFitWidth(closeButtonWidth);
                
                double xPosition = menuLayoutX + truckOrdersMenuWidth - closeButtonWidth/2 - closeButtonWidth;
                double yPosition = menuLayoutY + closeButtonWidth/2;
                                
                closeButtonImageView.setLayoutX(xPosition);
                closeButtonImageView.setLayoutY(yPosition);
                 
                
                Group closeButtonGroup = new Group();
                
                closeButtonGroup.getChildren().add( closeButtonImageView );           

                truckOrdersMenu.getChildren().add( closeButtonGroup );
                
                closeButtonGroup.setOnMouseClicked(returnButtonEvent);
                
            }
            
            buttonText.setFont( Font.font("Verdana", FontWeight.BOLD, 30*standardFontReduction) );

            buttonText.setLayoutX( buttonRectangle.getBoundsInParent().getMinX() + buttonRectangle.getBoundsInParent().getWidth()/2.0 - buttonText.getBoundsInParent().getWidth()/2.0 );
            buttonText.setLayoutY( buttonRectangle.getBoundsInParent().getMinY() + buttonRectangle.getBoundsInParent().getHeight()/2.0 + buttonText.getBoundsInParent().getHeight()/2.0 );

            buttonGroup.getChildren().add( buttonText );           

            truckOrdersMenu.getChildren().add( buttonGroup );
            
        }
        
        truckOrdersMenu.toFront();
                
    }
    
    public void removeTruckOrdersMenu(){
        
        Group truckOrdersMenu = (Group) actionsManagerGroup.lookup("#truckOrdersMenu");
        
        if ( truckOrdersMenu != null ){
            
//            System.out.println("CLEANING THIS");
            truckOrdersMenu.getChildren().clear();
            
        }        
        
        
    }
    
    public void processStateClick(int stateId, boolean highlight){
                
        String messageToSend = Globals.xmlProcesser.getHighlightRequestMessageString(stateId, highlight);
        
//        messageToSend = messageToSend + messageToSend + messageToSend + messageToSend + messageToSend;
        
        Globals.smlCommunications.sendOutputMessage(messageToSend);      
        
        System.out.println("Sent message: " + messageToSend);
                        
    }
        
    public void popUpBodyCompletion() throws TransformerException, ParserConfigurationException{
                
        
        
        int durationAnimationMillis = 3000;
        
        while( completedBodies.size() > 0 ){
            
            System.out.println("completedBodies = " + completedBodies.toString());
            
            System.out.println("POP UP!!!");
            
            int bodyCompleted = completedBodies.get(0);
            Globals.missionManager.setTruckArrival(bodyCompleted);
            
            if ( Globals.missionManager.isTruckOnLoadEntrance(bodyCompleted) ){
                
//                promptRrtCommand(bodyCompleted);
                
            }
            if ( Globals.missionManager.isTruckOnLoadParking(bodyCompleted) ){
                
//                promptRrtFromOpenAreaCommand(bodyCompleted);
                
            }
            if ( Globals.missionManager.isTruckOnLoadExit(bodyCompleted) ){
                
//                promptExitOpenAreaCommand(bodyCompleted);
                
            }
                    
            Globals.drawingManager.addLogEntry("Truck " + bodyCompleted + " finished its task.", Color.GREEN);
            
            completedBodies.remove(0);
            
            Text completedText = new Text("Body " + bodyCompleted + " has finished its task!");
            completedText.setFont( new Font(50) );
            completedText.setFill( Color.PAPAYAWHIP );
            
            completedText.setLayoutX(200);
            completedText.setLayoutY(200);
            
            actionsManagerGroup.getChildren().add(completedText);
            completedText.toFront();
            
            
            FadeTransition ft = new FadeTransition(Duration.millis(durationAnimationMillis), completedText);
            ft.setFromValue(1.0);
            ft.setToValue(0.0);
            ft.setCycleCount(1);
            ft.setAutoReverse(false);
            
            ScaleTransition st = new ScaleTransition(Duration.millis(durationAnimationMillis), completedText);
            st.setFromX(1.0);
            st.setFromY(1.0);
            st.setToX(.1);
            st.setToY(.1);
            st.setCycleCount(1);
            st.setAutoReverse(false);
            
            ParallelTransition pt = new ParallelTransition();
            pt.getChildren().addAll(ft, st);
            
            pt.play();
            
            pt.setOnFinished(new EventHandler<ActionEvent>(){
 
                @Override
                public void handle(ActionEvent arg0) {
                    
                    System.out.println("Cleaning POP UP");
                    actionsManagerGroup.getChildren().remove(completedText);
                    
                }

            });
            
            
        }
        
    }
    
    public void createTruckInformationList(){
        
        int[] truckIds = new int[5];
        
        truckInformationList = new ArrayList<>();
        
        TruckInformation truckInfoRui = new TruckInformation("Rui Oliveira", Globals.body1QualisysId, "scaniaLogoCropped.png", "matteoTruck.png", "ruiAndTruck.png", "+46720752943");
        truckInfoRui.setTruckFuel(180);
        truckInfoRui.setTruckModel("Scania R260");
        truckInformationList.add(truckInfoRui);
        truckIds[0] = Globals.body1QualisysId;
        
        TruckInformation truckInfoAlvito = new TruckInformation("Pedro Alvito", Globals.body2QualisysId, "scaniaLogoCropped.png", "alvitoTruck.png", "alvitoAndTruck.png", "+46720842651");
        truckInfoAlvito.setTruckFuel(220);
        truckInfoAlvito.setTruckModel("Scania R260");
        truckInformationList.add(truckInfoAlvito);
        truckIds[1] = Globals.body2QualisysId;
                
        TruckInformation truckInfoMatteo = new TruckInformation("Matteo Vanin", Globals.body3QualisysId, "scaniaLogoCropped.png", "matteoTruck.png", "matteoAndTruck.png", "+46720574526");
        truckInfoMatteo.setTruckFuel(240);
        truckInfoMatteo.setTruckModel("Scania R260");
        truckInformationList.add(truckInfoMatteo);
        truckIds[2] = Globals.body3QualisysId;
        
//        TruckInformation truckInfoLima = new TruckInformation("Pedro Lima", 98, "lima.png", "limaTruck.png", "limaAndTruck.png", "+46720775142");
        TruckInformation truckInfoLima = new TruckInformation("Pedro Lima", 98, "excavatorFlat.png", "excavatorFlat.png", "excavatorFlat.png", "+46720775142");
        truckInfoLima.setTruckFuel(120);
        truckInfoLima.setTruckModel("excavator");
        truckInformationList.add(truckInfoLima);
        truckIds[3] = 98;
        
//        TruckInformation truckInfoJonas = new TruckInformation("Jonas Mårtensson", 99, "jonas.png", "jonasTruck.png", "jonasAndTruck.png", "+46720985635");
        TruckInformation truckInfoJonas = new TruckInformation("Jonas Mårtensson", 99, "excavatorFlat.png", "excavatorFlat.png", "excavatorFlat.png", "+46720985635");
        truckInfoJonas.setTruckFuel(60);
        truckInfoJonas.setTruckModel("excavator");
        truckInformationList.add(truckInfoJonas);
        truckIds[4] = 99;
        
        Globals.missionManager = new MissionManager(truckIds);
        
        
    }
       
    public Group getTrucksMenu(){
        
        trucksMenuGroup = new Group();
        
        createTruckInformationList();
        
        Rectangle menuBackground = new Rectangle();
//        menuBackground.setX(windowWidth*Globals.mapXRatio);
//        menuBackground.setY(0);
        menuBackground.setWidth( actionsManagerWidth );
        menuBackground.setHeight(actionsManagerHeight );
        menuBackground.setFill(Color.DARKSLATEGREY);

        trucksMenuGroup.getChildren().add(menuBackground);
        
        Text text = new Text();
//        text.setFont(new Font(30));
        text.setFont( Font.font("Verdana", FontWeight.BOLD, 30*standardFontReduction) );
//        text.setWrappingWidth( windowWidth*(1.0-Globals.mapXRatio) );
//        text.setTextAlignment(TextAlignment.JUSTIFY);
        text.setText("VEHICLES");
        
        trucksMenuGroup.getChildren().add(text);
        
        System.out.println("text.getBoundsInLocal().getWidth()/2.0 = " + text.getBoundsInLocal().getWidth()/2.0);
        
//        text.setBoundsType(TextBoundsType.VISUAL);
        
        text.setLayoutX( actionsManagerWidth*0.5 - 0.5*text.getBoundsInParent().getWidth() );
        text.setLayoutY( text.getBoundsInParent().getHeight()*(3.0/2.0) );
        
        text.setFill(Color.ALICEBLUE);
        text.setId("availableText");
        
        double elementWidth = actionsManagerWidth;
        double elementHeight = (actionsManagerHeight - 2*text.getBoundsInLocal().getHeight() )/truckInformationList.size();
        int truckCounter = 1;
        
        for (TruckInformation truckInformation : truckInformationList){
        
            Group currentGroup = getTruckMenuElement(truckInformation, 0.9*elementWidth, 0.9*elementHeight);
            currentGroup.setLayoutX( actionsManagerWidth*0.5 - currentGroup.getBoundsInLocal().getWidth()/2.0 );
            currentGroup.setLayoutY( ((double)(truckCounter-1))*elementHeight + 2*text.getBoundsInLocal().getHeight() );
            trucksMenuGroup.getChildren().add(currentGroup);
            truckCounter++;           
            
        }
        
        System.out.println("updateTrucksMenu(truckMenuRoot)");
        updateTrucksMenu();
        
        return trucksMenuGroup;
        
    }
//    
//    public Group getTruckMenuElementWithPhoto(TruckInformation truckInformation, double elementWidth, double elementHeight){
//        
//                
//        Group truckMenuElementGroup = new Group();
//        truckMenuElementGroup.setId( "truckId"+truckInformation.getTruckId() );
//        
//        Rectangle backgroundRectangle = new Rectangle();
//        backgroundRectangle.setWidth(elementWidth);
//        backgroundRectangle.setHeight(elementHeight);
//        backgroundRectangle.setArcWidth(20);
//        backgroundRectangle.setArcHeight(20);
//        backgroundRectangle.setFill(Color.SLATEGRAY);
//        
//        truckMenuElementGroup.getChildren().add(backgroundRectangle);
//        
//        Image truckDriverImage = truckInformation.getTruckDriverImage();
//        ImageView truckDriverImageView = new ImageView(truckDriverImage);
//        
//        double photoWidthRatio = 0.2;
//        
//        truckDriverImageView.setPreserveRatio(true);
//        truckDriverImageView.setFitWidth(elementWidth*photoWidthRatio);
//        
//        Rectangle statusRectangle = new Rectangle();
//        statusRectangle.setWidth(elementWidth*photoWidthRatio);
//        statusRectangle.setHeight(elementWidth*photoWidthRatio);
//        statusRectangle.setFill(Color.RED);
//        statusRectangle.setOpacity(0.6);
//        
//        statusRectangle.setId("statusRectangle");
//        
//        double imageLayoutX = 0.25*photoWidthRatio*elementWidth;
//        double imageLayoutY = elementHeight/2.0 - truckDriverImageView.getFitWidth()/2.;
//        
//        truckDriverImageView.setLayoutX( imageLayoutX );
//        truckDriverImageView.setLayoutY( imageLayoutY );
//        
//        statusRectangle.setLayoutX( imageLayoutX );
//        statusRectangle.setLayoutY( imageLayoutY );
//               
//        truckMenuElementGroup.getChildren().add(statusRectangle);
//        truckMenuElementGroup.getChildren().add(truckDriverImageView);
//        
//        Image truckImage = truckInformation.getTruckImage();
//        double truckImageRatio = truckImage.getHeight()/truckImage.getWidth();
//        
//        ImageView truckImageView = new ImageView(truckImage);
//        
//        double truckWidthRatio = 1.0 - photoWidthRatio - 3.0*(photoWidthRatio/4.0);
//        
//        truckImageView.setPreserveRatio(true);
//        truckImageView.setFitWidth(elementWidth*truckWidthRatio);
//        
//        truckImageView.setLayoutX( photoWidthRatio*elementWidth + 0.5*photoWidthRatio*elementWidth );
//        truckImageView.setLayoutY( elementHeight/2.0 - truckImageView.getFitWidth()*truckImageRatio/2.0 );
//           
//        truckMenuElementGroup.getChildren().add(truckImageView);
//        
//        
//        
//        String statusTextString = "AAAAOFFLINE";
//        Text statusText = new Text(statusTextString);
//        statusText.setFill(Color.RED);
//        statusText.setTextAlignment(TextAlignment.CENTER);
//        
//        if ( truckInformation.getTruckModel().equals("excavator") ){
//            
//            statusTextString = "AVAILABLE";
//            statusText.setText( statusTextString);
//            statusText.setFill(Color.GREEN);
//            
//        }
//        
//        double maxTextWidth = elementWidth*(2.0/3.0);
//        double maxTextHeight = elementHeight/5.0;
//        
//        Font textFont = new Font(50);
//        double adjustedFontSize = DrawingBaseFunctions.getMaximumFittingFontSize(textFont, statusTextString, maxTextWidth, maxTextHeight);
//        
//        statusText.setFont(new Font(adjustedFontSize));
//                                
//        statusText.setTextAlignment(TextAlignment.CENTER);
//                
//        statusText.setId("statusText");
//        
//        truckMenuElementGroup.getChildren().add(statusText);
//        
//        String cargoTextString = "EMPTY CARGO";
//        Text cargoText = new Text(cargoTextString);
//        cargoText.setFill(Color.BLACK);
//        cargoText.setTextAlignment(TextAlignment.CENTER);
//        
//        double maxCargoTextWidth = elementWidth*(1.0/3.0);
//        maxTextHeight = elementHeight/5.0;
//        
//        textFont = new Font(50);
//        adjustedFontSize = DrawingBaseFunctions.getMaximumFittingFontSize(textFont, cargoTextString, maxCargoTextWidth, maxTextHeight);
//        
//        cargoText.setFont(new Font(adjustedFontSize));
//        
//        cargoText = DrawingBaseFunctions.centerTextAroundOffset(maxTextWidth + maxCargoTextWidth*.5, elementHeight*(4.0/5.0), cargoText);
//        cargoText.setTextAlignment(TextAlignment.CENTER);
//                
//        cargoText.setId("cargoText");
//        
//        if ( !truckInformation.getTruckModel().equals("excavator") ){
//                
//            truckMenuElementGroup.getChildren().add(cargoText);
//        
//        }
//        
//        EventHandler truckHighlightEvent = new EventHandler<MouseEvent>(){
//            @Override
//            public void handle(MouseEvent arg0) {
//
//                if ( waitingForReverseTrajectory ){
//                    
//                    return;
//                    
//                }      
//                    
//                int highlightedId = truckInformation.getTruckId(); 
//                Globals.drawingManager.makeRipple(highlightedId);
//               
//
//            }
//            
//        };
//        
//        
//        EventHandler truckClickEvent = new EventHandler<InputEvent>(){
//            @Override
//            public void handle(InputEvent arg0) {
//
////                MouseButton pressedButton = arg0.getButton();
//                
//                if ( waitingForReverseTrajectory ){
//                    
//                    return;
//                    
//                }
//                
//                Globals.drawingManager.clearTrajectory();
//                Globals.drawingManager.clearRipple();
//                cleanUpAfterRRT();
//
//                boolean highlight = true;
//                String messageToSend = null;
//
//                messageToSend = Globals.xmlProcesser.getHighlightRequestMessageString(truckInformation.getTruckId(), highlight);
//
//                if (messageToSend != null){
//
//                    Globals.smlCommunications.sendOutputMessage(messageToSend);
//
//                    System.out.println("Sent highlight request = " + messageToSend);
//                }
//
//                int currentTruckId = truckInformation.getTruckId();
//                
////                System.out.println("currentTruckId = " + currentTruckId);
////                Globals.missionManager.availableString
////                ArrayList<String> truckPositions = Globals.missionManager.getTruckPositions();
////                for ( String truckPosition : truckPositions ){
////                    System.out.println("truckPosition = " + truckPosition);
////                }
////                ArrayList<String> truckStates = Globals.missionManager.getTruckStates();
////                for ( String truckState : truckStates ){
////                    System.out.println("truckState = " + truckState);
////                }
//                
//                if ( Globals.missionManager.isTruckOnLoadEntrance(currentTruckId) ){
//                    
//                    showTruckOrdersMenuFreeAreaEntrance(truckInformation);
//                    
//                }else if ( Globals.missionManager.isTruckOnLoadParking(currentTruckId) ){
//                    
//                    showTruckOrdersMenuFreeAreaParking(truckInformation);
//                    
//                }else{
//                
//                    showTruckOrdersMenu(truckInformation);
//                    
//                }
//                
//
//            }
//            
//        };
//        
//        
//        if ( Globals.touchScreenMode ){
//            
//            truckMenuElementGroup.setOnMouseClicked(truckHighlightEvent);
//            truckMenuElementGroup.setOnSwipeRight(truckClickEvent); 
//            truckMenuElementGroup.setOnSwipeLeft(truckClickEvent); 
//            
//        }else{
//            
//            truckMenuElementGroup.setOnMouseClicked(truckClickEvent); 
//            
//        }
//        
//        EventHandler swipeEventHandler = new EventHandler<SwipeEvent>() {
//            @Override public void handle(SwipeEvent event) {
//
//                System.out.println("SWIPPED 111");
//                
//                Globals.drawingManager.clearRipple();
//                
////                System.out.println("TO REPLACE");
//////                highlightedId = truckInformation.getTruckId();   
////                System.out.println("TO REPLACE");
//                boolean highlight = true;
//                String messageToSend = null;
//                
//                messageToSend = Globals.xmlProcesser.getHighlightRequestMessageString(truckInformation.getTruckId(), highlight);
//                
//                if (messageToSend != null){
//
//                    Globals.smlCommunications.sendOutputMessage(messageToSend);     
//                    System.out.println("Sent highlight request = " + messageToSend);
//                }
////                System.out.println("TO REPLACE");   
//////                showTruckOrdersMenu(truckMenuRoot, trajectoryRoot, truckInformation);
////                System.out.println("TO REPLACE");
//                event.consume();
//            }
//        };
//        
//        
//        EventHandler enterTruckerEvent = new EventHandler<MouseEvent>(){
//            @Override
//            public void handle(MouseEvent arg0) {
//                
////                System.out.println("TO REPLACE");
//////                highlightedId = truckInformation.getTruckId();   
////                System.out.println("TO REPLACE");                
//
//            }
//        };
//                
////        truckMenuElementGroup.setOnMouseEntered(enterTruckerEvent);    
//        
//        EventHandler exitTruckerEvent = new EventHandler<MouseEvent>(){
//            @Override
//            public void handle(MouseEvent arg0) {
////                System.out.println("TO REPLACE");
//////                highlightedId = -1000;                    
////                System.out.println("TO REPLACE");
//            }
//        };
//        
////        truckMenuElementGroup.setOnMouseExited(exitTruckerEvent);        
//        
//        return truckMenuElementGroup;
//        
//    }
//       
    public Group getTruckMenuElement(TruckInformation truckInformation, double elementWidth, double elementHeight){
        
        Group truckMenuElementGroup = new Group();
        truckMenuElementGroup.setId( "truckId"+truckInformation.getTruckId() );
        
        Rectangle backgroundRectangle = new Rectangle();
        backgroundRectangle.setWidth(elementWidth);
        backgroundRectangle.setHeight(elementHeight);
        backgroundRectangle.setArcWidth(20);
        backgroundRectangle.setArcHeight(20);
        backgroundRectangle.setFill(Color.SLATEGRAY);
        
        truckMenuElementGroup.getChildren().add(backgroundRectangle);
        
        double offsetRatioHeight = 0.7;
        
        double truckWidthRatio = 0.6;
        
        Rectangle statusRectangle = new Rectangle();
        
        statusRectangle.setFill(Color.RED);
        statusRectangle.setOpacity(0.6);
        
        statusRectangle.setId("statusRectangle");
               
        truckMenuElementGroup.getChildren().add(statusRectangle);
        
        Image truckImage = truckInformation.getTruckImage();
        double truckImageRatio = truckImage.getHeight()/truckImage.getWidth();
        
        ImageView truckImageView = new ImageView(truckImage);
        
        
        truckImageView.setPreserveRatio(true);
        truckImageView.setFitWidth(elementWidth*truckWidthRatio);
                
        statusRectangle.setWidth(elementWidth*truckWidthRatio);
        statusRectangle.setHeight(elementWidth*truckWidthRatio*truckImageRatio);
               
        
        statusRectangle.setLayoutX( elementWidth*0.5 - .5*elementWidth*truckWidthRatio );
        statusRectangle.setLayoutY( (elementHeight/2.0 - truckImageView.getFitWidth()*truckImageRatio/2.0)*offsetRatioHeight );
        
        truckImageView.setLayoutX( elementWidth*0.5 - .5*elementWidth*truckWidthRatio );
        truckImageView.setLayoutY( (elementHeight/2.0 - truckImageView.getFitWidth()*truckImageRatio/2.0)*offsetRatioHeight );
           
        truckMenuElementGroup.getChildren().add(truckImageView);
        
        String statusTextString = "AAAAOFFLINE";
        Text statusText = new Text(statusTextString);
        statusText.setFill(Color.RED);
        statusText.setTextAlignment(TextAlignment.CENTER);
        
        double maxTextWidth = elementWidth*(2.0/3.0);
        double maxTextHeight = elementHeight/5.0;
        
        Font textFont = new Font(50);
        double adjustedFontSize = DrawingBaseFunctions.getMaximumFittingFontSize(textFont, statusTextString, maxTextWidth, maxTextHeight);
        
        statusText.setFont(new Font(adjustedFontSize));
        
        if ( truckInformation.getTruckModel().equals("excavator") ){
            
            statusText = DrawingBaseFunctions.centerTextAroundOffset(elementWidth/2.0, elementHeight*(4.0/5.0), statusText);
            
        }else{
            
            statusText = DrawingBaseFunctions.centerTextAroundOffset(maxTextWidth/2.0, elementHeight*(4.0/5.0), statusText);
            
        }
        
        statusText.setTextAlignment(TextAlignment.CENTER);
        statusText.setId("statusText");
        
        truckMenuElementGroup.getChildren().add(statusText);
        
        String cargoTextString = "EMPTY CARGO";
        Text cargoText = new Text(cargoTextString);
        
        if ( truckInformation.getTruckModel().equals("excavator") ){
            
            cargoText.setText("");
            
        }
        
        cargoText.setFill(Color.BLACK);
        cargoText.setTextAlignment(TextAlignment.CENTER);
        
        double maxCargoTextWidth = elementWidth*(1.0/3.0);
        maxTextHeight = elementHeight/5.0;
        
        textFont = new Font(50);
        adjustedFontSize = DrawingBaseFunctions.getMaximumFittingFontSize(textFont, cargoTextString, maxCargoTextWidth, maxTextHeight);
        
        cargoText.setFont(new Font(adjustedFontSize));
        
        cargoText = DrawingBaseFunctions.centerTextAroundOffset(maxTextWidth + maxCargoTextWidth*.5, elementHeight*(4.0/5.0), cargoText);
        cargoText.setTextAlignment(TextAlignment.CENTER);
                
        cargoText.setId("cargoText");
        
        truckMenuElementGroup.getChildren().add(cargoText);
        
        
        EventHandler truckHighlightEvent = new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent arg0) {

                if ( waitingForReverseTrajectory ){
                    
                    return;
                    
                }      
                    
                int highlightedId = truckInformation.getTruckId(); 
                Globals.drawingManager.makeRipple(highlightedId);
               

            }
            
        };
        
        
        EventHandler truckClickEvent = new EventHandler<InputEvent>(){
            @Override
            public void handle(InputEvent arg0) {

//                MouseButton pressedButton = arg0.getButton();
                
                if ( waitingForReverseTrajectory ){
                    
                    return;
                    
                }
                
                Globals.drawingManager.clearTrajectory();
                Globals.drawingManager.clearRipple();
                cleanUpAfterRRT();

                boolean highlight = true;
                String messageToSend = null;

                messageToSend = Globals.xmlProcesser.getHighlightRequestMessageString(truckInformation.getTruckId(), highlight);

                if (messageToSend != null){

                    Globals.smlCommunications.sendOutputMessage(messageToSend);

                    System.out.println("Sent highlight request = " + messageToSend);
                }

                int currentTruckId = truckInformation.getTruckId();
                
//                System.out.println("currentTruckId = " + currentTruckId);
//                Globals.missionManager.availableString
//                ArrayList<String> truckPositions = Globals.missionManager.getTruckPositions();
//                for ( String truckPosition : truckPositions ){
//                    System.out.println("truckPosition = " + truckPosition);
//                }
//                ArrayList<String> truckStates = Globals.missionManager.getTruckStates();
//                for ( String truckState : truckStates ){
//                    System.out.println("truckState = " + truckState);
//                }
                
                if ( currentTruckId == 98 || currentTruckId == 99 ){
                    
                    showExcavatorOrdersMenu(truckInformation);
                    
                }else{
                    
                    if ( Globals.missionManager.isTruckOnLoadEntrance(currentTruckId) ){
                    
                        showTruckOrdersMenuFreeAreaEntrance(truckInformation);

                    }else if ( Globals.missionManager.isTruckOnLoadParking(currentTruckId) ){

                        showTruckOrdersMenuFreeAreaParking(truckInformation);

                    }else{

                        showTruckOrdersMenu(truckInformation);

                    }
                    
                }
                
                
                

            }
            
        };
        
        
        if ( Globals.touchScreenMode ){
            
            truckMenuElementGroup.setOnMouseClicked(truckHighlightEvent);
            truckMenuElementGroup.setOnSwipeRight(truckClickEvent); 
            truckMenuElementGroup.setOnSwipeLeft(truckClickEvent); 
            
        }else{
            
            truckMenuElementGroup.setOnMouseClicked(truckClickEvent); 
            
        }
        
        EventHandler swipeEventHandler = new EventHandler<SwipeEvent>() {
            @Override public void handle(SwipeEvent event) {

                System.out.println("SWIPPED 111");
                
                Globals.drawingManager.clearRipple();
                
//                System.out.println("TO REPLACE");
////                highlightedId = truckInformation.getTruckId();   
//                System.out.println("TO REPLACE");
                boolean highlight = true;
                String messageToSend = null;
                
                messageToSend = Globals.xmlProcesser.getHighlightRequestMessageString(truckInformation.getTruckId(), highlight);
                
                if (messageToSend != null){

                    Globals.smlCommunications.sendOutputMessage(messageToSend);     
                    System.out.println("Sent highlight request = " + messageToSend);
                }
//                System.out.println("TO REPLACE");   
////                showTruckOrdersMenu(truckMenuRoot, trajectoryRoot, truckInformation);
//                System.out.println("TO REPLACE");
                event.consume();
            }
        };
        
        
        EventHandler enterTruckerEvent = new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent arg0) {
                
//                System.out.println("TO REPLACE");
////                highlightedId = truckInformation.getTruckId();   
//                System.out.println("TO REPLACE");                

            }
        };
                
        truckMenuElementGroup.setOnMouseEntered(enterTruckerEvent);    
        
        EventHandler exitTruckerEvent = new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent arg0) {
//                System.out.println("TO REPLACE");
////                highlightedId = -1000;                    
//                System.out.println("TO REPLACE");
            }
        };
        
        truckMenuElementGroup.setOnMouseExited(exitTruckerEvent);        
        
        return truckMenuElementGroup;
        
    }
           
    public boolean getBodyStateFromId(int argBodyId, double[] state){
        
        List<QualisysBody> currentQualisysBodiesList = Globals.smlWorldInfo.getQualisysBodiesList();
        int desiredBodyId = argBodyId;
        QualisysBody desiredBody = null;
        for ( QualisysBody tempBody : currentQualisysBodiesList ){
            if ( tempBody.getId() == desiredBodyId ){
                desiredBody = tempBody;
            }
        }
        
        boolean success = true;
        
        if ( desiredBody == null ){
            
            success = false;
            
        }else{
        
            if (state.length != 4){
                
                System.out.println("State length must be 4!");
                success = false;
                
            }else{
                
                state[0] = desiredBody.getX();
                state[1] = desiredBody.getY();
                double thetaInRadians = desiredBody.getTheta();
                thetaInRadians = thetaInRadians*(Math.PI/180.0);

                state[2] = thetaInRadians;
                state[3] = thetaInRadians;
            
            }
        
        }
        
        return success;
        
    }
        
    public void promptRrtToExitAreaCommand(int argBodyId){
        
        System.out.println("promptRrtToExitAreaCommand()");
        
        double[] state = new double[4];
        getBodyStateFromId(argBodyId, state);
        
        int freeSpaceExitId = Globals.missionManager.getAvailableDestinationId("Load Exit");
        
//                freeSpaceEntranceId
        List<OsmNode> osmNodes = Globals.xmlProcesser.getNodeList();

        double xPos = 0;
        double yPos = 0;

        for ( OsmNode tempNode : osmNodes ){

            if ( tempNode.id == freeSpaceExitId ){

                xPos = tempNode.X/32.;
                yPos = tempNode.Y/32.;

            }

        }
        
        if ( xPos == 0 && yPos == 0 ){
            
            System.err.println("promptRrtToExitAreaCommand() did not find the node for exiting the load area!");
            
        }

        double[] goalPosition = new double[2];

        goalPosition[0] = xPos;
        goalPosition[1] = yPos;

        String rrtRequestString = null;

        rrtRequestString = Globals.xmlProcesser.getRrtTrajectoryRequestMessageString(state, goalPosition, argBodyId);
        
        System.out.println("promptRrtToExitAreaCommand() Sending rrtRequestString = " + rrtRequestString);

        Globals.smlCommunications.sendOutputMessage(rrtRequestString);
                
//        Text promptText = new Text("Trying to exit area!\n  Is this trajectory good?");
        Text promptText = new Text("");
        promptText.setFont( Font.font("Verdana", FontWeight.BOLD, 40) );
        promptText.setFill(Color.WHITE);
        promptText.setId("promptText");
        promptText.toFront();
        actionsManagerGroup.getChildren().add(promptText);
//        promptText.setLayoutX(Globals.drawingManager.windowWidth/2. - promptText.getBoundsInParent().getWidth()/2.0);
        promptText.setLayoutX(Globals.drawingManager.worldViewManager.worldViewWidth/2. - promptText.getBoundsInParent().getWidth()/2.0);
        promptText.setLayoutY(100);
                
        removeHighlight();
        
        double buttonWidth = 200;
        double buttonHeight = standardButtonHeight;
        
        Group newRrtButtonGroup = (Group) actionsManagerGroup.lookup("#newTrajectoryButton");
                
        if ( newRrtButtonGroup == null ){
            
            String textButton = "REPLAN";

            newRrtButtonGroup = createConfirmRrtTrajectoryButton(textButton, buttonWidth, buttonHeight);
            newRrtButtonGroup.setLayoutX( Globals.drawingManager.windowWidth/2.0 - buttonWidth*(3.0/2.0) );
//            newRrtButtonGroup.setLayoutX( Globals.drawingManager.worldViewManager.worldViewWidth/2.0 + leftButtonPostTrajectoryOffsetX );
            
//            newRrtButtonGroup.setLayoutY( Globals.drawingManager.windowHeight - 1.5*buttonHeight );
            newRrtButtonGroup.setLayoutY( Globals.drawingManager.worldViewManager.worldViewHeight - 1.5*buttonHeight );
            
            System.out.println("newRrtButtonGroup.getBoundsInLocal() = " + newRrtButtonGroup.getBoundsInLocal());
            
            actionsManagerGroup.getChildren().add(newRrtButtonGroup);
            newRrtButtonGroup.setId("newTrajectoryButton");
            newRrtButtonGroup.toFront();

            
        }else{

           newRrtButtonGroup.setOnMouseClicked(null);

        }
        
        newRrtButtonGroup.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                
                // Ask for a new RRT                
                
                
                actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#newTrajectoryButton") );          
                actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#confirmButton") );   
                actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#promptText") ); 
                actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#rrtRegionGroup") );   
                
                promptRrtToExitAreaCommand(argBodyId);
                
                        
            }
        });
        
        Group confirmButtonGroup = (Group) actionsManagerGroup.lookup("#confirmButton");
                
        if ( confirmButtonGroup == null ){
            
            String textButton = "CONFIRM";

            confirmButtonGroup = createConfirmRrtTrajectoryButton(textButton, buttonWidth, buttonHeight);
            confirmButtonGroup.setLayoutX( Globals.drawingManager.windowWidth/2.0 + buttonWidth*(1.0/2.0) );
//            confirmButtonGroup.setLayoutX( Globals.drawingManager.worldViewManager.worldViewWidth/2.0 + rightButtonPostTrajectoryOffsetX );
            
//            confirmButtonGroup.setLayoutY( Globals.drawingManager.windowHeight - 1.5*buttonHeight );
//            Globals.drawingManager.worldViewManager.worldViewHeight;
            confirmButtonGroup.setLayoutY( Globals.drawingManager.worldViewManager.worldViewHeight - 1.5*buttonHeight );
            
            System.out.println("confirmButtonGroup.getBoundsInLocal() = " + confirmButtonGroup.getBoundsInLocal());
            
            actionsManagerGroup.getChildren().add(confirmButtonGroup);
            confirmButtonGroup.setId("confirmButton");
            confirmButtonGroup.toFront();

            
//            confirmButtonGroup.setLayoutX(Globals.drawingManager.windowWidth/2.0 + 1.5*buttonWidth);
            confirmButtonGroup.setLayoutX(Globals.drawingManager.worldViewManager.worldViewWidth/2.0 + 1.5*buttonWidth);
            
        }else{

           confirmButtonGroup.setOnMouseClicked(null);

        }
        
        confirmButtonGroup.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                
                me.consume();
                
//                Globals.missionManager.sendTruckToFreeArea( argBodyId );
                Globals.missionManager.sendTruckToLoadExit(argBodyId);

                sendRrtTrajectoryCommand(argBodyId);

                actionsManagerGroup.setOnMouseClicked(null);
                
                
                Globals.drawingManager.addLogEntry("Sent truck " + argBodyId + " to exit loading area.");
                
                removeHighlight();
                
//                screenRoot.lookup("#confirmRrtButton").setOnMouseClicked(null);
                cleanUpAfterRRT();
                                       
            }
        });
        
                
    }
    
    public void cleanUpAfterRRT(){
        
        actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#newTrajectoryButton") );
        actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#confirmButton") );   
        actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#promptText") );                
        actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#rrtRegionGroup") );

        Globals.drawingManager.clearRrtSolution();
        
    }
    
    public void removeHighlight(){
        
        boolean highlight = false;
        
        String messageToSend = Globals.xmlProcesser.getHighlightRequestMessageString(-1, highlight);
        
        Globals.smlCommunications.sendOutputMessage(messageToSend);      
        
        System.out.println("Sent message: " + messageToSend);
        
    }
    
    public Group createConfirmRrtTrajectoryButton(String textButton, double buttonWidth, double buttonHeight){
                
        Group performTrajectoryButtonGroup = new Group();
        
        performTrajectoryButtonGroup.setId("trajectoryButton");
        
        Rectangle buttonRectangle = new Rectangle();
        buttonRectangle.setWidth(buttonWidth);
        buttonRectangle.setHeight(buttonHeight);
            
//        buttonRectangle.setLayoutX(mapWidth/2.0 - buttonWidth/2.0);
//        buttonRectangle.setLayoutY( windowHeight - 1.5*buttonHeight );

        buttonRectangle.setFill(Color.SLATEGRAY);
        buttonRectangle.setStroke(Color.DARKSLATEGRAY);
        buttonRectangle.setStrokeWidth(5);
        buttonRectangle.setArcWidth(10);
        buttonRectangle.setArcHeight(10); 

        performTrajectoryButtonGroup.getChildren().add( buttonRectangle );

        Text buttonText = new Text(textButton);
        buttonText.setFont( Font.font("Verdana", FontWeight.BOLD, 30*standardFontReduction) );

        buttonText.setLayoutX( buttonRectangle.getBoundsInParent().getWidth()/2.0 - buttonText.getBoundsInParent().getWidth()/2.0 );
        buttonText.setLayoutY( buttonRectangle.getBoundsInParent().getHeight()/2.0 + buttonText.getBoundsInParent().getHeight()/2.0 );

        performTrajectoryButtonGroup.getChildren().add( buttonText );
        
        return performTrajectoryButtonGroup;       
        
    }
    
    public void createTrajectoryButton(ArrayList<OsmLanelet> laneletPath){
        
        Group performTrajectoryButtonGroup = new Group();
        
        performTrajectoryButtonGroup.setId("trajectoryButton");
        
        double buttonWidth = 200;
        double buttonHeight = standardButtonHeight;
        double buttonOffsetX = 150;
        
        Rectangle buttonRectangle = new Rectangle();
        buttonRectangle.setWidth(buttonWidth);
        buttonRectangle.setHeight(buttonHeight);
            
        buttonRectangle.setLayoutX( Globals.drawingManager.worldViewManager.worldViewWidth/2.0 - buttonOffsetX - buttonWidth/2.0);
//        buttonRectangle.setLayoutX( Globals.drawingManager.worldViewManager.worldViewWidth/2.0 + rightButtonPostTrajectoryOffsetX);
        
        
//        buttonRectangle.setLayoutX( Globals.drawingManager.windowWidth/2.0 - buttonOffsetX - buttonWidth/2.0);
        
        buttonRectangle.setLayoutY( Globals.drawingManager.worldViewManager.worldViewHeight - 1.5*buttonHeight );
//        buttonRectangle.setLayoutY( Globals.drawingManager.windowHeight - 1.5*buttonHeight );
        
        buttonRectangle.setFill(Color.SLATEGRAY);
        buttonRectangle.setStroke(Color.DARKSLATEGRAY);
        buttonRectangle.setStrokeWidth(5);
        buttonRectangle.setArcWidth(10);
        buttonRectangle.setArcHeight(10); 

        performTrajectoryButtonGroup.getChildren().add( buttonRectangle );

        Text buttonText = new Text("CONFIRM");
        buttonText.setFont( Font.font("Verdana", FontWeight.BOLD, 30*standardFontReduction) );

        buttonText.setLayoutX( buttonRectangle.getBoundsInParent().getMinX() + buttonRectangle.getBoundsInParent().getWidth()/2.0 - buttonText.getBoundsInParent().getWidth()/2.0 );
        buttonText.setLayoutY( buttonRectangle.getBoundsInParent().getMinY() + buttonRectangle.getBoundsInParent().getHeight()/2.0 + buttonText.getBoundsInParent().getHeight()/2.0 );
        
        performTrajectoryButtonGroup.getChildren().add( buttonText );
        
        performTrajectoryButtonGroup.toFront();
        
        EventHandler performTrajectoryClickEvent = new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent arg0) {

                int bodyId = lastPressedTruck;
                
                int bodyNum = 0;
                if ( bodyId == Globals.body1QualisysId ){
                    bodyNum = 0;
                }
                if ( bodyId == Globals.body2QualisysId ){
                    bodyNum = 1;                       
                }
                if ( bodyId == Globals.body3QualisysId ){
                    bodyNum = 2;                          
                }              
                
                truckGoBBoolean.remove(bodyNum);
                truckGoBBoolean.add(bodyNum, Boolean.TRUE);
                
                System.out.println("Confirm button press: lastPressedDestination = " + lastPressedDestination);
                
                if ( lastPressedDestination.equals("Park") ){
                    
                    Globals.missionManager.sendTruckToPark( bodyId );
                    Globals.drawingManager.addLogEntry("Sent truck " + bodyId + " to park.");
                    
                }else if ( lastPressedDestination.equals("Unload") ){
                    
                    Globals.missionManager.sendTruckToUnload( bodyId );
                    Globals.drawingManager.addLogEntry("Sent truck " + bodyId + " to unload.");
                    
                }else if ( lastPressedDestination.equals("Load Entrance") ){
                    
                    Globals.missionManager.sendTruckToLoadEntrance(bodyId );
                    Globals.drawingManager.addLogEntry("Sent truck " + bodyId + " to loading area entrance.");
                    
                }else{
                    
                    if ( lastPressedDestination.equals("PARKING") ){
                    
                        String messageToSend = Globals.xmlProcesser.getReverseTrajectoryCommandMessageString();
                        
                        Globals.smlCommunications.sendOutputMessage(messageToSend);
                        System.out.println("sent reverse_trajectory_command: messageToSend = " + messageToSend);
                        
                       
                        
                        Globals.missionManager.sendTruckToLoadParking(bodyId );
                        Globals.drawingManager.addLogEntry("Sent truck " + bodyId + " to perform reverse parking.");

                        boolean highlight = false;
                        messageToSend = Globals.xmlProcesser.getHighlightRequestMessageString(-1, highlight);

                        if (messageToSend != null){

        //                    System.out.println("SHOULD HAVE SENT AN HIGHLIGH -1 MESSAGE, NOT SENDING IT BECAUSE IT MESSES UP THE TRAJECTORY REROUTING TO MATLAB");
                            Globals.smlCommunications.sendOutputMessage(messageToSend);
                            System.out.println("Sent a highlight removal: " + messageToSend);

                        }  

                        Globals.drawingManager.clearTrajectory();
                        
                        actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#trajectoryButton") );
                        actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#abortTrajectoryButton") );

                        waitingForReverseTrajectory = false;
                        
                        return;
                    
                    
                    }
                    
                }
                    
                
                
                Globals.missionManager.setTruckLaneletPath(bodyId, laneletPath);
                                
                sendTrajectoryCommand(bodyId);
                                
                boolean highlight = false;
                
                String messageToSend = Globals.xmlProcesser.getHighlightRequestMessageString(-1, highlight);
                
                if (messageToSend != null){
                    
//                    System.out.println("SHOULD HAVE SENT AN HIGHLIGH -1 MESSAGE, NOT SENDING IT BECAUSE IT MESSES UP THE TRAJECTORY REROUTING TO MATLAB");
                    Globals.smlCommunications.sendOutputMessage(messageToSend);
                    
                }  
                
                actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#trajectoryButton") );
                actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#abortTrajectoryButton") );

                Globals.drawingManager.clearTrajectory();
                
            }
        };
        
        performTrajectoryButtonGroup.setOnMouseClicked(performTrajectoryClickEvent);
        
        actionsManagerGroup.getChildren().add(performTrajectoryButtonGroup);
        
        Group abortTrajectoryButtonGroup = new Group();
        
        abortTrajectoryButtonGroup.setId("abortTrajectoryButton");
        
//        double buttonWidth = 200;
//        double buttonHeight = 100;
        
        Rectangle abortButtonRectangle = new Rectangle();
        abortButtonRectangle.setWidth(buttonWidth);
        abortButtonRectangle.setHeight(buttonHeight);
          
        abortButtonRectangle.setLayoutX(Globals.drawingManager.worldViewManager.worldViewWidth/2.0 + buttonOffsetX - buttonWidth/2.0);
//        abortButtonRectangle.setLayoutX(Globals.drawingManager.windowWidth/2.0 + buttonOffsetX - buttonWidth/2.0);
//        abortButtonRectangle.setLayoutX(Globals.drawingManager.worldViewManager.worldViewWidth/2.0 + rightButtonPostTrajectoryOffsetX);
        
        abortButtonRectangle.setLayoutY( Globals.drawingManager.worldViewManager.worldViewHeight - 1.5*buttonHeight );
//        abortButtonRectangle.setLayoutY( Globals.drawingManager.windowHeight - 1.5*buttonHeight );
        
        abortButtonRectangle.setFill(Color.SLATEGRAY);
        abortButtonRectangle.setStroke(Color.DARKSLATEGRAY);
        abortButtonRectangle.setStrokeWidth(5);
        abortButtonRectangle.setArcWidth(10);
        abortButtonRectangle.setArcHeight(10); 

        abortTrajectoryButtonGroup.getChildren().add( abortButtonRectangle );

        Text abortButtonText = new Text("CANCEL");
        abortButtonText.setFont( Font.font("Verdana", FontWeight.BOLD, 30*standardFontReduction) );

        abortButtonText.setLayoutX( abortButtonRectangle.getBoundsInParent().getMinX() + abortButtonRectangle.getBoundsInParent().getWidth()/2.0 - abortButtonText.getBoundsInParent().getWidth()/2.0 );
        abortButtonText.setLayoutY( abortButtonRectangle.getBoundsInParent().getMinY() + abortButtonRectangle.getBoundsInParent().getHeight()/2.0 + abortButtonText.getBoundsInParent().getHeight()/2.0 );
        
        abortTrajectoryButtonGroup.getChildren().add( abortButtonText );
        
        EventHandler abortTrajectoryClickEvent = new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent arg0) {

                
                boolean highlight = false;
                String messageToSend = null;
                
                messageToSend = Globals.xmlProcesser.getHighlightRequestMessageString(-1, highlight);
                
                if (messageToSend != null){
                    
                    Globals.smlCommunications.sendOutputMessage(messageToSend);     
                    
                }                
                                        
//                        isTruckAvailable( truckInformation.getTruckId() 
                
                Globals.drawingManager.clearTrajectory();
                
                actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#trajectoryButton") );
                actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#abortTrajectoryButton") );
//                actionsManagerGroup.getChildren().clear();
                
                waitingForReverseTrajectory = false;

            }
        };
        
        abortTrajectoryButtonGroup.setOnMouseClicked(abortTrajectoryClickEvent);
        
        actionsManagerGroup.getChildren().add(abortTrajectoryButtonGroup);
        
    }
    
    public void sendTrajectoryCommand(int bodyId){
        
        System.out.println("sendTrajectoryCommand()");

//        List<TrajectoryPoint> trajectoryPointList = Globals.xmlProcesser.getTrajectoryPointList(); 
//        List<TrajectoryPoint> trajectoryPointList = Globals.xmlProcesser.getTrajectoryPointList(); 
        
        List<TrajectoryPoint> trajectoryPointList = new ArrayList<>();
        
        int cnt = 0;
        
        for ( double[] tempTrajectoryPoint : latestTrajectoryPointsList ){
            
            double argTime = (double) cnt;
            double argX = tempTrajectoryPoint[0];
            double argY = tempTrajectoryPoint[1];
            
            TrajectoryPoint trajectoryPoint = new TrajectoryPoint(argTime, argX, argY);
            
            trajectoryPointList.add(trajectoryPoint);
            
            cnt ++;
            
//            trajectoryPointList.add( new TrajectoryPoint(tabHeight, tabHeight, tabHeight))
            
        }
        
//        trajectoryPointList.add(new TrajectoryPoint(tabHeight, tabHeight, tabHeight));
                
        String messageToSend = Globals.xmlProcesser.getTrajectoryCommandMessageString(bodyId, trajectoryPointList);
        
        Globals.smlCommunications.sendOutputMessage(messageToSend);      
        
//        Thread.sleep(afterTrajectoryMessageStop);
        
        System.out.println("Sent message: " + messageToSend);

    }
        
    public OsmNode getNodeById(int nodeId, List<OsmNode> nodeList){
        
        OsmNode tempNode;
        
        for (int i = 0; i < nodeList.size(); i++){
            
            tempNode = nodeList.get(i);
            
            if (tempNode.id == nodeId ){
                
                return tempNode;
                
            }
            
        }
        
        return null;
        
    }
    
    public Group getWayPolyline(OsmWay way, List<OsmNode> nodeList){
        
        Polyline polyline = new Polyline();
        
        for ( int i = 0; i < way.nodeIds.size(); i++){
            
            int nodeId = way.nodeIds.get(i);
            
            OsmNode node = getNodeById(nodeId, nodeList);
            
            polyline.getPoints().add(node.X);
            polyline.getPoints().add(node.Y);
            
        }
        
        Group wayGroup = new Group(polyline);
        
        return wayGroup;
        
    }
    
    public Group getWayCircles(OsmWay way, List<OsmNode> nodeList){
        
        Group wayGroup = new Group();
                
        for ( int i = 0; i < way.nodeIds.size(); i++){
            
            int nodeId = way.nodeIds.get(i);
            
            OsmNode node = getNodeById(nodeId, nodeList);
            
            Circle tempCircle = new Circle(node.X, node.Y, 1.0);
            wayGroup.getChildren().add(tempCircle);
           
        }
                
        return wayGroup;
        
    }
    
    public Group getLineBetweenNodes(OsmNode startNode, OsmNode endNode){
        
        
        double x1 = 0, x2 = 0, y1 = 0, y2 = 0;

        x1 = startNode.X;
        y1 = startNode.Y;
        
        x2 = endNode.X;
        y2 = endNode.Y;
        
        System.out.println( "(" + x1 + "," + y1 + ")-(" + x2 + "," + y2 + ")" );
        
        Line line = new Line();
        line.setStartX(x1);
        line.setStartY(y1);
        line.setEndX(x2);
        line.setEndY(y2);
                
        Group objectToDraw = new Group(line);
        
        return objectToDraw;
        
    }
    
    public Group getRrtWayPolyline(){
        
        
////        getLaneletPolygon(int laneletId){
//        
//        Group laneletGroup = new Group();
//        
        OsmWay way = null;
        
        for( OsmWay tempWay : Globals.xmlProcesser.wayList ){
            
            if ( tempWay.wayType.equals( "free_space" ) ){
                
                way = tempWay;
                break;
                
            }
            
        }
//        
        if ( way == null ){
            
            System.out.println("ERROR: WAY FREE_SPACE NOT FOUND");
            
        }
        
        return Globals.drawingManager.getWayPolygon(way.id);
        
    }
    
    public void promptRrtFromOpenAreaCommand(int argBodyId) throws TransformerException, ParserConfigurationException{
        
        Text promptText = new Text("Truck is on open space area!\n  Please select new destination");
        promptText.setFont( Font.font("Verdana", FontWeight.BOLD, 40) );
        promptText.setFill(Color.WHITE);
        promptText.setId("promptText");
        promptText.toFront();
        actionsManagerGroup.getChildren().add(promptText);
        promptText.setLayoutX(Globals.drawingManager.worldViewManager.worldViewWidth/2. - promptText.getBoundsInParent().getWidth()/2.0);
//        promptText.setLayoutX(Globals.drawingManager.windowWidth/2. - promptText.getBoundsInParent().getWidth()/2.0);
//        
        promptText.setLayoutY(100);
        
        Group rrtRegionGroup = getRrtWayPolyline();
        rrtRegionGroup.setId("rrtRegionGroup");
        actionsManagerGroup.getChildren().add( rrtRegionGroup );
        
        List<QualisysBody> currentQualisysBodiesList = Globals.smlWorldInfo.getQualisysBodiesList();
        int desiredBodyId = argBodyId;
        QualisysBody desiredBody = null;
        for ( QualisysBody tempBody : currentQualisysBodiesList ){
            if ( tempBody.getId() == desiredBodyId ){
                desiredBody = tempBody;
            }
        }

        double[] state = new double[4];
        state[0] = desiredBody.getX();
        state[1] = desiredBody.getY();
        double thetaInRadians = desiredBody.getTheta();
        thetaInRadians = thetaInRadians*(Math.PI/180.0);

        state[2] = thetaInRadians;
        state[3] = thetaInRadians;


        double[] goal_position = new double[2];
        goal_position[0] = -0.5;
        goal_position[1] = -0.5;
        
        removeHighlight();
        
        
        double buttonWidth = 200;
        double buttonHeight = standardButtonHeight;
        
        Group newDestinationButtonGroup = (Group) actionsManagerGroup.lookup("#newDestinationButton");
                
        if ( newDestinationButtonGroup == null ){
            
            String textButton = "New Dest";

            newDestinationButtonGroup = createConfirmRrtTrajectoryButton(textButton, buttonWidth, buttonHeight);
//            newDestinationButtonGroup.setLayoutX(Globals.drawingManager.windowWidth/2.0 - buttonWidth*(3.0/2.0));
            newDestinationButtonGroup.setLayoutX(Globals.drawingManager.worldViewManager.worldViewWidth/2.0 - buttonWidth*(3.0/2.0));
            
            newDestinationButtonGroup.setLayoutY( Globals.drawingManager.worldViewManager.worldViewHeight - 1.5*buttonHeight );
//            newDestinationButtonGroup.setLayoutY( Globals.drawingManager.windowHeight - 1.5*buttonHeight );
            
            System.out.println("newDestinationButtonGroup.getBoundsInLocal() = " + newDestinationButtonGroup.getBoundsInLocal());
            
            actionsManagerGroup.getChildren().add(newDestinationButtonGroup);
            newDestinationButtonGroup.setId("newDestinationButton");
            newDestinationButtonGroup.toFront();

//            newDestinationButtonGroup.setLayoutX(Globals.drawingManager.windowWidth/2.0 - 1.5*buttonWidth);
            newDestinationButtonGroup.setLayoutX(Globals.drawingManager.worldViewManager.worldViewWidth/2.0 - 1.5*buttonWidth);
            
            
        }else{

           newDestinationButtonGroup.setOnMouseClicked(null);

        }
        
        newDestinationButtonGroup.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                
                me.consume();
                
                promptRrtCommand(argBodyId);
                
                actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#newDestinationButton") );          
                actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#exitAreaButton") );      
                actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#promptText") );   
                actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#rrtRegionGroup") );   

                }
            });
        
        Group exitAreaButtonGroup = (Group) actionsManagerGroup.lookup("#exitAreaButton");
                
        if ( exitAreaButtonGroup == null ){
            
            String textButton = "Exit Area";

            exitAreaButtonGroup = createConfirmRrtTrajectoryButton(textButton, buttonWidth, buttonHeight);
            exitAreaButtonGroup.setLayoutX(Globals.drawingManager.worldViewManager.worldViewWidth/2.0 + buttonWidth/2.0);
//            exitAreaButtonGroup.setLayoutX(Globals.drawingManager.windowWidth/2.0 + buttonWidth/2.0);
            
            exitAreaButtonGroup.setLayoutY( Globals.drawingManager.worldViewManager.worldViewHeight - 1.5*buttonHeight );
//            exitAreaButtonGroup.setLayoutY( Globals.drawingManager.windowHeight - 1.5*buttonHeight );
            
            System.out.println("exitAreaButtonGroup.getBoundsInLocal() = " + exitAreaButtonGroup.getBoundsInLocal());
            
            actionsManagerGroup.getChildren().add(exitAreaButtonGroup);
            exitAreaButtonGroup.setId("exitAreaButton");
            exitAreaButtonGroup.toFront();

            exitAreaButtonGroup.setLayoutX(Globals.drawingManager.worldViewManager.worldViewWidth/2.0 + 1.5*buttonWidth);
//            exitAreaButtonGroup.setLayoutX(Globals.drawingManager.windowWidth/2.0 + 1.5*buttonWidth);
            
            
        }else{

           exitAreaButtonGroup.setOnMouseClicked(null);

        }
        
        exitAreaButtonGroup.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                
                promptRrtToExitAreaCommand(argBodyId);

                actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#newDestinationButton") );          
                actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#exitAreaButton") );    
                actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#promptText") );   
                actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#rrtRegionGroup") );   
                        
            }
        });
        
                
    }
    
    public void promptRrtCommand(int argBodyId){
        
        Text promptText = new Text("Truck has arrived at open space!\n  Please select new destination");
        promptText.setFont( Font.font("Verdana", FontWeight.BOLD, 40) );
        promptText.setFill(Color.WHITE);
        promptText.setId("promptText");
        promptText.toFront();
        actionsManagerGroup.getChildren().add(promptText);
        promptText.setLayoutX(Globals.drawingManager.worldViewManager.worldViewWidth/2. - promptText.getBoundsInParent().getWidth()/2.0);
//        promptText.setLayoutX(Globals.drawingManager.windowWidth/2. - promptText.getBoundsInParent().getWidth()/2.0);
//        
        promptText.setLayoutY(100);
                
        Group rrtRegionGroup = getRrtWayPolyline();
        rrtRegionGroup.setId("rrtRegionGroup");
        actionsManagerGroup.getChildren().add( rrtRegionGroup );
        
        List<QualisysBody> currentQualisysBodiesList = Globals.smlWorldInfo.getQualisysBodiesList();
        int desiredBodyId = argBodyId;
        QualisysBody desiredBody = null;
        for ( QualisysBody tempBody : currentQualisysBodiesList ){
            if ( tempBody.getId() == desiredBodyId ){
                desiredBody = tempBody;
            }
        }

        double[] state = new double[4];
        state[0] = desiredBody.getX();
        state[1] = desiredBody.getY();
        double thetaInRadians = desiredBody.getTheta();
        thetaInRadians = thetaInRadians*(Math.PI/180.0);

        state[2] = thetaInRadians;
        state[3] = thetaInRadians;


        double[] goal_position = new double[2];
        goal_position[0] = -0.5;
        goal_position[1] = -0.5;
        
        removeHighlight();

        actionsManagerGroup.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                
                double screenGoalX = me.getX();
                double screenGoalY = me.getY();
                
                
                double goalX = Globals.drawingManager.convertToX(screenGoalX);
                double goalY = Globals.drawingManager.convertToY(screenGoalY);
                                
//                double[] start_state = new double[4];
//                double thetaInRadians = desiredBody.getTheta();
//                thetaInRadians = thetaInRadians*(Math.PI/180.0);
                
                
                System.out.println("screenGoalX = " + screenGoalX);
                System.out.println("screenGoalY = " + screenGoalY);
               
                System.out.println("goalX = " + goalX);
                System.out.println("goalY = " + goalY);
                
                goal_position[0] = goalX;
                goal_position[1] = goalY;
                
                String rrtRequestString = null;
                
                rrtRequestString = Globals.xmlProcesser.getRrtTrajectoryRequestMessageString(state, goal_position, desiredBodyId);
                
                System.out.println("promptRrtCommand() - Sending rrtRequestString = " + rrtRequestString);

                Globals.smlCommunications.sendOutputMessage(rrtRequestString);
                                
                String textButton = "CONFIRM";
                
                double buttonWidth = 200;
                double buttonHeight = standardButtonHeight;       
                
                Group confirmButtonGroup = (Group) actionsManagerGroup.lookup("#confirmRrtButton");
                
                if ( confirmButtonGroup == null ){
                    
                    confirmButtonGroup = createConfirmRrtTrajectoryButton(textButton, buttonWidth, buttonHeight);
                    confirmButtonGroup.setLayoutX( Globals.drawingManager.worldViewManager.worldViewWidth/2.0 + buttonWidth/2.0);
//                    confirmButtonGroup.setLayoutX( Globals.drawingManager.worldViewManager.worldViewWidth/2.0 + rightButtonPostTrajectoryOffsetX);
//                    confirmButtonGroup.setLayoutX( Globals.drawingManager.windowWidth/2.0 - buttonWidth/2.0);
                    
                    confirmButtonGroup.setLayoutY( Globals.drawingManager.worldViewManager.worldViewHeight - 1.5*buttonHeight );
//                    confirmButtonGroup.setLayoutY( Globals.drawingManager.windowHeight - 1.5*buttonHeight );
                    
                    System.out.println("confirmButtonGroup.getBoundsInLocal() = " + confirmButtonGroup.getBoundsInLocal());
                    
                    actionsManagerGroup.getChildren().add(confirmButtonGroup);
                    confirmButtonGroup.setId("confirmRrtButton");
                    confirmButtonGroup.toFront();
                    
                }else{
                    
                   confirmButtonGroup.setOnMouseClicked(null);
                    
                }
                
                confirmButtonGroup.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent me) {
                                                                    
                        me.consume();
                        
                        Globals.missionManager.sendTruckToLoadExit(argBodyId );
                        
                        sendRrtTrajectoryCommand(argBodyId);
                        
//                        confirmButtonGroup.setOnMouseClicked(null);
//                        screenRoot.getChildren().remove(confirmButtonGroup);
                        actionsManagerGroup.setOnMouseClicked(null);
                        
                        actionsManagerGroup.lookup("#confirmRrtButton").setOnMouseClicked(null);
                        actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#confirmRrtButton") );
                        actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#promptText") );
                        actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#rrtRegionGroup") );   
//                        screenRoot.getChildren().remove( screenRoot.lookup("#goalCircle") );
                        
                        Globals.drawingManager.clearRrtSolution();
                                               
                        
                    }
                });
                                
                }
            
        });
                
    }
    
    public void sendRrtTrajectoryCommand(int argBodyId){
        
        System.out.println("sendRrtTrajectoryCommand()");

        List<TrajectoryPoint> trajectoryPointList = new ArrayList<>();
        
        ArrayList<RrtNode> rrtTree = Globals.currentRrtSolution.getTreeNodes();
        ArrayList<Integer> rrtSolution = Globals.currentRrtSolution.getSolutionNodes();
                
        // The solution comes ordered from the leaf node to the root node
        for ( int i = rrtSolution.size()-1 ; i >= 0 ; i-- ){
            
            RrtNode currentNode = rrtTree.get( rrtSolution.get( i ) );
            TrajectoryPoint currentPoint = new TrajectoryPoint( (double)i, currentNode.x , currentNode.y );
                        
            trajectoryPointList.add(currentPoint);
            
        }
                
        String messageToSend = Globals.xmlProcesser.getTrajectoryCommandMessageString(argBodyId, trajectoryPointList);
        
        Globals.smlCommunications.sendOutputMessage(messageToSend);    
        
        try {
            Thread.sleep(afterTrajectoryMessageStop);
        } catch (InterruptedException ex) {
            Logger.getLogger(ActionsManager.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("sendRrtTrajectoryCommand: failed to sleep");
        }
        
        System.out.println("Sent message: " + messageToSend);
        
        
    }
    
    public void updateTrucksMenu(){
                                        
        List<QualisysBody> currentQualisysBodiesList = Globals.smlWorldInfo.getQualisysBodiesList();
        List<QualisysBody> availableTruckBodiesList = new ArrayList<>();
       
        for ( int i = 0 ; i < trucksMenuGroup.getChildren().size() ; i++ ){
            
            Node currentNode = trucksMenuGroup.getChildren().get(i);
           
            if ( currentNode.getId() == null ){
                continue;
            }
            
            if ( !currentNode.getId().contains("truckId") ){
                continue;
            }
            
            Group currentGroup = (Group) currentNode;
            
            Rectangle statusRectangle = (Rectangle) currentGroup.lookup("#statusRectangle");
            Text statusText = (Text) currentGroup.lookup("#statusText");
            Text cargoText = (Text) currentGroup.lookup("#cargoText");
                        
            if ( statusRectangle != null ){
                
                for (QualisysBody body : currentQualisysBodiesList){
                    
                    if ( currentGroup.getId().equals("truckId"+98) || currentGroup.getId().equals("truckId"+99) ) {
                        
                        statusRectangle.setFill(Color.GREEN);
                        statusText.setFill(Color.GREEN);
                        statusText.setText("AVAILABLE");
                        
                        break;
                        
                    }                  
                   
                    if ( currentGroup.getId().equals("truckId"+body.getId()) ){
                        
//                        System.out.println("Setting cargo to false");
//                        Globals.missionManager.setTruckCargo( body.getId(), false);
//                        System.out.println("Setting cargo to true");
//                        Globals.missionManager.setTruckCargo( body.getId(), true);
                        
                        boolean isTruckAvailable = Globals.missionManager.isTruckAvailable(body.getId());
                        
                        
//                        System.out.println("body.isControllable() = " + body.isControllable() );
//                        System.out.println("isTruckAvailable = " + isTruckAvailable );
                        boolean isTruckCargoed = false;  
                        
                        
                        if ( body.isControllable() ){
                                                                    
                            isTruckCargoed = Globals.missionManager.hasTruckCargo(body.getId());
                            
                            if ( isTruckAvailable ){

                                if ( body.getX() < -9000.0 ){
                                    
                                    statusRectangle.setFill(Color.YELLOW);
                                    statusText.setFill(Color.YELLOW);
                                    statusText.setText("UNAVAILABLE");
                                            
                                }else{
                                    statusRectangle.setFill(Color.GREEN);
                                    statusText.setFill(Color.GREEN);
                                    statusText.setText("AVAILABLE");
                                }

                            }else{

                                statusRectangle.setFill(Color.CYAN);
                                statusText.setFill(Color.CYAN);
                                statusText.setText("BUSY");

                            }
                        
                        }else{
                            
                            statusRectangle.setFill(Color.RED);
                            statusText.setFill(Color.RED);
                            statusText.setText("OFFLINE");
                            
                        }
                        
                        if (isTruckCargoed){
                            
//                            cargoText.setText("FULL CARGO");
                            cargoText.setText("12 Ton Iron");
                            
                        }else{
                            
                            cargoText.setText("EMPTY CARGO");                            
                            
                        }
                        
//                        Bounds bounds = statusText.getBoundsInParent();
                        
                                                
                        break;
                                                        
                    }else{
                        
                        // If the group corresponds to a ID not detected:
                                                
                        statusRectangle.setFill(Color.RED);
                        statusText.setFill(Color.RED);
                        statusText.setText("OFFLINE");
                        
                    }
                                        
                }    
                
            }
            
            Bounds bounds = currentGroup.getBoundsInLocal();
                        
            double centerXStatusText = ( bounds.getMinX() + bounds.getMaxX() )*0.5*(2.0/3.0);
            double centerYStatusText = ( bounds.getMinY() + bounds.getMaxY() )*(9.0/10.0);

            if ( currentGroup.getId().equals("truckId"+98) || currentGroup.getId().equals("truckId"+99) ){
                statusText = DrawingBaseFunctions.centerTextAroundOffset(( bounds.getMinX() + bounds.getMaxX() )*0.5, centerYStatusText, statusText);
            }else{
                statusText = DrawingBaseFunctions.centerTextAroundOffset(centerXStatusText, centerYStatusText, statusText);
            }
            
            
            statusText.setTextAlignment(TextAlignment.CENTER);
            
            double centerXCargoText = ( bounds.getMinX() + bounds.getMaxX() )*0.5*(1.0/3.0) + 2*centerXStatusText;
            
            cargoText = DrawingBaseFunctions.centerTextAroundOffset(centerXCargoText, centerYStatusText, cargoText);
            cargoText.setTextAlignment(TextAlignment.CENTER);
            
        }
        
                
        List<Integer> onlineBodiesList = new ArrayList<Integer>();
        List<Integer> offlineBodiesList = new ArrayList<>();
        
        
        for (QualisysBody body : availableTruckBodiesList){
            if ( body.isControllable() ){
                
                onlineBodiesList.add(body.getId());
                                
            }else{
                
                offlineBodiesList.add(body.getId());
                
            }
        }
        
        Collections.sort( onlineBodiesList );
        Collections.sort( offlineBodiesList );
        
        
        int availableTrucksCounter = 0;
        
        for (QualisysBody body : availableTruckBodiesList){
                   
//        for(int i = 0 ; i < 4 ; i++){
            
            Group currentTruckGroup = (Group) trucksMenuGroup.lookup( "#truck"+body.getId() );
            
            int truckCounter = 1;
            
            if ( !body.isControllable() ){
                truckCounter = onlineBodiesList.size();
//                truckCounter = truckCounter +
                truckCounter = truckCounter + offlineBodiesList.indexOf(body.getId());
            }else{
                
                truckCounter = onlineBodiesList.indexOf(body.getId());
            }
            
            System.out.println("truckCounter = " + truckCounter);
            
            Text tempText = (Text) currentTruckGroup.lookup("#availableText");
        
//        double elementWidth = windowWidth*(1.0-Globals.mapXRatio);
            double elementHeight = (actionsManagerHeight - 2*tempText.getBoundsInLocal().getHeight() )/truckInformationList.size();
            
//            currentTruckGroup.setLayoutX( windowWidth*Globals.mapXRatio + windowWidth*(1.0-Globals.mapXRatio)*0.5 - currentTruckGroup.getBoundsInLocal().getWidth()/2.0 );
            currentTruckGroup.setLayoutY( ((double)(truckCounter-1))*elementHeight + 2*tempText.getBoundsInLocal().getHeight() );
            
            if (currentTruckGroup == null){
                
                System.out.println("CREATING");
            
                currentTruckGroup = new Group();
                
                currentTruckGroup.setId( "#truck"+body.getId() );
                
                Image image = new Image("file:truck.png");

                if ( image.getWidth() < 1.0 ){

                    System.err.println("Image not found! (truck.png)");

                }
                
                ImageView imageView = new ImageView(image);

                double imageRatio = image.getWidth()/image.getHeight();

                imageView.setPreserveRatio(true);

                double truckMargin = 0.1*actionsManagerWidth;
                imageView.setFitWidth( actionsManagerWidth - 2*truckMargin);

                double imageHeight = imageView.getFitWidth()/imageRatio;

//                imageView.setLayoutX( windowWidth*Globals.mapXRatio + truckMargin);
                imageView.setLayoutY( ( (double)(availableTrucksCounter+1) )*(1.2)*imageHeight );
                availableTrucksCounter++;
    //            System.out.println("1080*Globals.mapXRatio = " + 1080*Globals.mapXRatio);
                
                currentTruckGroup.getChildren().add(imageView);
                
                trucksMenuGroup.getChildren().add(currentTruckGroup);
                
            }
            
        }
        
        
        
    }
    
    
    
}
