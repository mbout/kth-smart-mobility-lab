/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testingcommunications;

import javafx.scene.Group;
import javafx.scene.control.ScrollPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextBoundsType;

/**
 *
 * MissionGuiManager class is in charge of the display and interactions of the 
 * user with the Drivers tab
 * 
 * @author rui
 */
public class MissionGuiManager {
    
    private double missionGuiWidth;
    private double missionGuiHeight;
    
    private Group missionGuiGroup;
    
    ScrollPane missionQueueScrollPane;
        
    
    
    public MissionGuiManager(double argWidth, double argHeight){
        
        missionGuiWidth = argWidth;
        missionGuiHeight = argHeight;
        
        missionGuiGroup = null;
                
    }
    
    public Group getMissionGuiGroup(){
        
        if ( missionGuiGroup == null ){
        
            createMissionGuiGroup();
         
        }
        
        return missionGuiGroup;
        
    }
    
    private void createMissionGuiGroup(){
        
        missionGuiGroup = new Group();
        
        Rectangle rect = new Rectangle(missionGuiWidth, missionGuiHeight);    
        rect.setFill(Color.CADETBLUE);
        
        missionGuiGroup.getChildren().add(rect);
        
        double missionTitleXRatio = 0.9;
        double missionTitleYRatio = 0.1;
        
        double missionQueueXRatio = 0.9;
        double missionQueueYRatio = 0.6;
        
        double queueOptionsXRatio = 0.9;
        double queueOptionsYRatio = 0.1;
        
        double newMissionXRatio = 0.9;
        double newMissionYRatio = 0.2;
        
        // Setting title group
        
        Group titleGroup = getTitleGroup(missionTitleXRatio*missionGuiWidth, missionTitleYRatio*missionGuiHeight);
        
        Text titleText = getTextWithinBounds("Mission Queue", missionTitleXRatio*missionGuiWidth, missionTitleYRatio*missionGuiHeight);
        
//        titleGroup.getChildren().add(titleText);
        
        titleGroup.setLayoutX( .5*(1.-missionTitleXRatio)*missionGuiWidth );
        titleGroup.setLayoutY(0);
        
        missionGuiGroup.getChildren().add( titleGroup );
        
        // Setting Queue
        
        setMissionQueue(missionQueueXRatio*missionGuiWidth, missionQueueYRatio*missionGuiHeight);
        
        missionQueueScrollPane.setLayoutX( .5*(1.-missionTitleXRatio)*missionGuiWidth );
        missionQueueScrollPane.setLayoutY(missionTitleYRatio*missionGuiHeight);
                
        missionGuiGroup.getChildren().add(missionQueueScrollPane);
        
        // Setting queueOptions group
        
        Group queueOptionsGroup = getQueueOptionsGroup(queueOptionsXRatio*missionGuiWidth, queueOptionsYRatio*missionGuiHeight);
        
        queueOptionsGroup.setLayoutX( .5*(1.-missionTitleXRatio)*missionGuiWidth );
        queueOptionsGroup.setLayoutY( (missionTitleYRatio+missionQueueYRatio)*missionGuiHeight);
        
        missionGuiGroup.getChildren().add( queueOptionsGroup );
        
        // Setting new mission group
        
        Group newMissionGroup = getNewMissionGroup(newMissionXRatio*missionGuiWidth, newMissionYRatio*missionGuiHeight);
        
        newMissionGroup.setLayoutX( .5*(1.-missionTitleXRatio)*missionGuiWidth );
        newMissionGroup.setLayoutY( (missionTitleYRatio+missionQueueYRatio+queueOptionsYRatio)*missionGuiHeight);
        
        missionGuiGroup.getChildren().add( newMissionGroup );
       
        
        
                
    }
    
    private Group getTitleGroup(double argWidth, double argHeight){
        
        Group titleGroup = new Group();
        
        Rectangle titleRectangle = new Rectangle(argWidth, argHeight);
        titleRectangle.setFill(Color.BLANCHEDALMOND);
        
        titleGroup.getChildren().add(titleRectangle);
        
        return titleGroup;
        
    }
    
    private Text getTextWithinBounds(String textString, double maxWidth, double maxHeight){
        
        Text text = new Text(textString);        
       
        text.setTextAlignment(TextAlignment.CENTER);
        text.setFill(Color.WHITE);
        
        text.setBoundsType(TextBoundsType.VISUAL);
        
        double safetyWidth = 0.95*maxWidth;
        double safetyHeight = 0.95*maxHeight;
        
        int fontSize = 1;
        double lineHeight = 0;
        for ( int i = 1 ; i < 100; i++){
            
            text.setFont( Font.font("Verdana", FontWeight.BOLD, i) );
            
            if ( text.getBoundsInLocal().getWidth() > safetyWidth || text.getBoundsInLocal().getHeight() > safetyHeight ){
                
                break;
                
            }
                
            lineHeight = text.getBoundsInLocal().getHeight();
            fontSize = i;
                        
        }
        
        text.setFont( Font.font("Verdana", FontWeight.BOLD, fontSize) );
        text.setFill(Color.BLACK);
        
        return text;
        
    }
    
    private Group getQueueOptionsGroup(double argWidth, double argHeight){
        
        Group titleGroup = new Group();
        
        Rectangle titleRectangle = new Rectangle(argWidth, argHeight);
        titleRectangle.setFill(Color.CHARTREUSE);
        
        titleGroup.getChildren().add(titleRectangle);
        
        return titleGroup;
        
    }
    
    private Group getNewMissionGroup(double argWidth, double argHeight){
        
        Group titleGroup = new Group();
        
        Rectangle titleRectangle = new Rectangle(argWidth, argHeight);
        titleRectangle.setFill(Color.DARKSEAGREEN);
        
        titleGroup.getChildren().add(titleRectangle);
        
        return titleGroup;
        
    }
    
    private void setMissionQueue(double argWidth, double argHeight){
                
        missionQueueScrollPane = new ScrollPane();
        missionQueueScrollPane.setPrefSize(argWidth, argHeight);
        missionQueueScrollPane.setStyle("-fx-background: rgb(0,0,0);");
        
        missionQueueScrollPane.setContent(new Group());
        
    }
    
    public void updateMissionQueue(MissionInformation mission){
        
        double missionQueueSpacing = 50.;
        
        String groupIdentifier = "" +mission.missionId;
        String missionName = mission.missionName;
        String missionState = mission.missionState;
        
        Group currentContent = (Group) missionQueueScrollPane.getContent();
        
        Group missionGroup = (Group) currentContent.lookup("#" + groupIdentifier);
        
        if ( missionGroup == null ){
            
            missionGroup = createMissionQueueElement(missionName, missionState);
            missionGroup.setId(groupIdentifier);
            
            missionGroup.setLayoutY(currentContent.getBoundsInLocal().getMaxY() + missionQueueSpacing);
            
            currentContent.getChildren().add(missionGroup);
            
        }else{
            
            updateMissionQueueElement(missionGroup, missionName, missionState);
            
        }
               
    }
    
    private Group createMissionQueueElement(String argMissionName, String argMissionState){
        
        Group missionQueueElementGroup = new Group();
        
        double fontSize = 20;
        
//        Text text = getTextWithinBounds(textString, double maxWidth, double maxHeight);
        Text missionNameText = new Text(argMissionName);
        missionNameText.setFont(new Font(fontSize));
        missionNameText.setFill(Color.ALICEBLUE);
        missionNameText.setId("missionName");
        
        Text missionStateText = new Text(argMissionState);
        missionStateText.setFont(new Font(fontSize));
        missionStateText.setFill(Color.YELLOW);
        missionNameText.setId("missionState");
        
        missionStateText.setLayoutX(missionNameText.getBoundsInLocal().getMaxX());
        
        missionQueueElementGroup.getChildren().addAll(missionNameText, missionStateText);
        
        return missionQueueElementGroup;
        
    }
    
    private void updateMissionQueueElement(Group missionQueueElementGroup, String argMissionName, String argMissionState){
        
        Text missionNameText = (Text) missionQueueElementGroup.lookup("#missionName");
        Text missionStateText = (Text) missionQueueElementGroup.lookup("#missionState");
        
        missionNameText.setText(argMissionName);
        missionStateText.setText(argMissionState);
        
        missionStateText.setLayoutX(missionNameText.getBoundsInLocal().getMaxX());
                
    }
    
    public void addGroupToMissionQueue(Group groupToAdd){
        
        Group currentContent = (Group) missionQueueScrollPane.getContent();
        
        groupToAdd.setLayoutX(currentContent.getBoundsInLocal().getMinX());
        groupToAdd.setLayoutY(currentContent.getBoundsInLocal().getMaxY());
        
        currentContent.getChildren().add(groupToAdd);        
        
    }
    
    public boolean removeGroupFromMissionQueue(String identifier){
        
        boolean success = false;
        
        if ( !identifier.contains("#") ){
            
            identifier = "#" + identifier;
            
        }
        
        Group currentContent = (Group) missionQueueScrollPane.getContent();
        
        Group groupToRemove = (Group) currentContent.lookup(identifier);
        
        if ( groupToRemove == null ){
            
            success = false;
            return success;
            
        }
        
        currentContent.getChildren().remove(groupToRemove);
        
        reorganizeMissionQueue();
        
        success = true;
        return success;
        
    }
    
    private void reorganizeMissionQueue(){
        
        // This function should reoganize the Mission Queue after an element is removed
        
        
    }
    
    
    
}
