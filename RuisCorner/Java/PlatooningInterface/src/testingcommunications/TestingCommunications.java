/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testingcommunications;

import java.io.IOException;
import java.lang.Math;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.ObjectBinding;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.effect.Lighting;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javafx.util.Duration;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
/**
 *
 * @author rui
 */
public class TestingCommunications extends Application {
    
    StackPane stackPane;
    Group screenRoot;
    Group trajectoryRoot;
    Group statesRoot;
    Group backgroundRoot;
    Group truckMenuRoot;
    
    boolean mapDrawn = false;
    boolean receivedRoadInfo = false;
    boolean receivedTrajectoryReply = false;
    
//    SMLCommunications task; 
//    XmlProcesser xmlProcesser;
//    SMLWorldInformation smlWorldInfo;    
    @Override
    public void start(Stage primaryStage) throws InterruptedException {
        
        stackPane = new StackPane();
        
        screenRoot = new Group();
                
        backgroundRoot = new Group();
        screenRoot.getChildren().add(backgroundRoot);
                
        trajectoryRoot = new Group();
        trajectoryRoot.setId("trajectoryRoot");
        screenRoot.getChildren().add(trajectoryRoot);
        
        statesRoot = new Group();
        statesRoot.setId("statesRootGroup");
        screenRoot.getChildren().add(statesRoot);
        
        truckMenuRoot = new Group();
        truckMenuRoot.setId("truckMenuRoot");
        screenRoot.getChildren().add(truckMenuRoot);
        
        Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
        Globals.screenWidth = primaryScreenBounds.getWidth();
        Globals.screenHeight = primaryScreenBounds.getHeight();
        
        Globals.drawingManager = new DrawingManager(screenRoot);
        
        Scene scene = new Scene(screenRoot, Globals.screenWidth, Globals.screenHeight);
                
//        topLabel.textProperty().bind(Bindings.createStringBinding(() -> "Top: "+insets.get().getTop(), insets));
//        leftLabel.textProperty().bind(Bindings.createStringBinding(() -> "Left: "+insets.get().getLeft(), insets));
//        rightLabel.textProperty().bind(Bindings.createStringBinding(() -> "Right: "+insets.get().getRight(), insets));
//        bottomLabel.textProperty().bind(Bindings.createStringBinding(() -> "Bottom: "+insets.get().getBottom(), insets));
        
        primaryStage.setX(primaryScreenBounds.getMinX());
        primaryStage.setY(primaryScreenBounds.getMinY());
        primaryStage.setWidth(primaryScreenBounds.getWidth());
        primaryStage.setHeight(primaryScreenBounds.getHeight());
        
        System.out.println("bounds.getWidth() = " + primaryScreenBounds.getWidth());
        System.out.println("bounds.getHeight() = " + primaryScreenBounds.getHeight());
                
        if (Globals.touchScreenMode){
        
            primaryStage.initStyle(StageStyle.UNDECORATED);
        
        }
        primaryStage.setTitle("Scania Command Central");
        primaryStage.getIcons().add(new Image("file:truckIcon.png"));
        primaryStage.setScene(scene);
        
        primaryStage.show();
        
        
        // Cool solution found in: http://stackoverflow.com/questions/26711474/javafx-8-stage-insets-window-decoration-thickness
        
        ObjectBinding<Insets> insets = Bindings.createObjectBinding(() -> 
        new Insets(scene.getY(), 
            primaryStage.getWidth()-scene.getWidth() - scene.getX(), 
            primaryStage.getHeight()-scene.getHeight() - scene.getY(), 
            scene.getX()),
            scene.xProperty(),
            scene.yProperty(),
            scene.widthProperty(),
            scene.heightProperty(),
            primaryStage.widthProperty(),
            primaryStage.heightProperty()
        );
        
        
        Font desiredFont = new Font(69);
        String desiredText = "Here comes text";
        double maxWidth = 500;
        double maxHeight = 100;
        double maxSupported = DrawingBaseFunctions.getMaximumFittingFontSize(desiredFont, desiredText, maxWidth, maxHeight);
        
        System.err.println("maxSupported = " + maxSupported);
        
        
        Globals.drawingManager.leftInset = insets.get().getLeft();
        Globals.drawingManager.topInset = insets.get().getTop();
                                
        Globals.drawingManager.windowWidth = primaryScreenBounds.getWidth() - insets.get().getLeft() - insets.get().getRight();
        Globals.drawingManager.windowHeight = primaryScreenBounds.getHeight() - insets.get().getTop() - insets.get().getBottom();
        
//        Globals.drawingManager.postInitializeStuff();
        Globals.drawingManager.drawLoadingScreen();
        
        Globals.smlCommunications = new SMLCommunications();
        Globals.xmlProcesser = new XmlProcesser();
                
        Globals.smlWorldInfo = new SMLWorldInformation();
        
        Thread th = new Thread( Globals.smlCommunications );
        th.setDaemon(true);
        
        if (Globals.passwordRequired){
        
            Group textFieldGroup = Globals.drawingManager.showTextField();

            Button connectButton = new Button("CONNECT");
            
            Group passwordFieldGroup = Globals.drawingManager.showPasswordField(connectButton);
            
            Group buttonGroup = new Group(connectButton);
            Globals.drawingManager.screenRoot.getChildren().add(buttonGroup);

            System.out.println(textFieldGroup.getBoundsInLocal().toString());
            System.out.println(buttonGroup.getBoundsInLocal().toString());

            double spacingWidth = 200.0;

            textFieldGroup.setLayoutX(Globals.drawingManager.windowWidth/2.0 - 2.0*spacingWidth);
            textFieldGroup.setLayoutY(Globals.drawingManager.windowHeight*(0.95));

            passwordFieldGroup.setLayoutX(Globals.drawingManager.windowWidth/2.0 - 1.0*spacingWidth);
            passwordFieldGroup.setLayoutY(Globals.drawingManager.windowHeight*(0.95));

            buttonGroup.setLayoutX(Globals.drawingManager.windowWidth/2.0 + 0.0*spacingWidth);
            buttonGroup.setLayoutY(Globals.drawingManager.windowHeight*(0.95));

    //        buttonGroup.setLayoutX(textFieldGroup.getBoundsInLocal().getMinX() + 
    //                textFieldGroup.getBoundsInLocal().getWidth()/2.0 - buttonGroup.getBoundsInLocal().getWidth()/2.0 );
    //        
    //        buttonGroup.setLayoutY(textFieldGroup.getBoundsInLocal().getMaxY() + 
    //                textFieldGroup.getBoundsInLocal().getHeight()/2.0);

            System.out.println("HEREC");

            EventHandler<ActionEvent> buttonPressedAction = new EventHandler<ActionEvent>() {

                @Override
                public void handle(ActionEvent event) {

                    startConnection(th, primaryStage, textFieldGroup, buttonGroup, passwordFieldGroup);

                }
            };

            connectButton.setOnAction(buttonPressedAction);
        
        }else{
                    
            startConnection(th, primaryStage, null, null, null);
                    
        }
        
        
//        createSmallWindow();
//        textPositioningTest();
        
        
                
    }
    
    
    private void textPositioningTest(){
        
        
        
        Text textTest = new Text("Testing");
        textTest.setFont(new Font(40));
        textTest.setFill(Color.RED);
        
        double centerTextX = 400;
        double centerTextY = 400;
        
        textTest = DrawingBaseFunctions.centerTextAroundOffset(centerTextX, centerTextY, textTest);
        
        Rectangle rectangle = new Rectangle(textTest.getBoundsInParent().getMinX(), textTest.getBoundsInParent().getMinY(),
                textTest.getBoundsInParent().getWidth(), textTest.getBoundsInParent().getHeight() );
        rectangle.setFill(Color.FUCHSIA);
        
        Globals.drawingManager.screenRoot.getChildren().add(rectangle);
        
        Globals.drawingManager.screenRoot.getChildren().add(textTest);
        
        Circle testCircle = new Circle(centerTextX, centerTextY, 2.0, Color.AQUA);
        
        Globals.drawingManager.screenRoot.getChildren().add(testCircle);
        
        
    }
    
    private void createSmallWindow(){
        
        final Stage stage = new Stage();
                //create root node of scene, i.e. group
        Group rootGroup = new Group();
        //create scene with set width, height and color
        Scene scene = new Scene(rootGroup, 200, 200, Color.WHITESMOKE);
        //set scene to stage
        stage.setScene(scene);
        //center stage on screen
        stage.centerOnScreen();
        
        stage.setResizable(false);
//        stage.initStyle(StageStyle.DECORATED); // Normal window
//        stage.initStyle(StageStyle.TRANSPARENT); // You dont see any borders
//        stage.initStyle(StageStyle.UNDECORATED); // You dont see any borders
//        stage.initStyle(StageStyle.UNIFIED); // Normal window without an outline
        stage.initStyle(StageStyle.UTILITY);
        //show the stage
        stage.show();
        //add some node to scene
        Text text = new Text(20, 110, "JavaFX");
        text.setFill(Color.DODGERBLUE);
        text.setEffect(new Lighting());
        text.setFont(Font.font(Font.getDefault().getFamily(), 50));
        //add text to the main root group
        rootGroup.getChildren().add(text);
               
        
    }


    private void startConnection(Thread th, Stage primaryStage, Group textFieldGroup, Group buttonGroup, Group passwordFieldGroup){
        
        Globals.drawingManager.screenRoot.getChildren().remove(textFieldGroup);
        Globals.drawingManager.screenRoot.getChildren().remove(buttonGroup);
        Globals.drawingManager.screenRoot.getChildren().remove(passwordFieldGroup);
    
        th.start();
        
        double desiredRate = 24;
        
        Rectangle rect = new Rectangle(1, 1);
        rect.setOpacity(0.0);
        screenRoot.getChildren().add(rect);
        
        TranslateTransition tt = new TranslateTransition(Duration.millis((1.0/desiredRate)*1000.), rect);
        tt.setByX(100f);
        tt.setCycleCount(1);
        
        Globals.commsStateMachine = new CommunicationStateMachine(screenRoot, backgroundRoot, statesRoot, trajectoryRoot, truckMenuRoot);
        
        tt.setOnFinished(new EventHandler<ActionEvent>(){
 
            @Override
            public void handle(ActionEvent arg0) {
                
                tt.play();
                
                try {
                    Globals.commsStateMachine.step();
                } catch (SocketException ex) {
                    Logger.getLogger(TestingCommunications.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(TestingCommunications.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
            
        });
        
        tt.play();
        
        primaryStage.setOnCloseRequest((WindowEvent we) -> {
            
            if ( Globals.smlCommunications.getMessage().equals("SERVER FOUND") || Globals.smlCommunications.getMessage().equals("WAITING") ){
                
                System.out.println("Sending CLOSE message to SML World");
                Globals.smlCommunications.sendOutputMessage("CLOSE");
                try {
                    Globals.smlCommunications.closeSocket();
                    System.out.println("Closed socket");
                } catch (IOException ex) {
                    Logger.getLogger(TestingCommunications.class.getName()).log(Level.SEVERE, null, ex);
                }
                System.out.println("Sent CLOSE message to SML World");
                                
            }  
            
        });
    
    
    }


    
   
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args){
        launch(args);
    }
    
}
