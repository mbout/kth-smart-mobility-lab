/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testingcommunications;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.PauseTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.SequentialTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.input.SwipeEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextBoundsType;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Scale;
import javafx.util.Duration;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

/**
 *
 * @author rui
 */
public class DrawingManager {
    

    
    
    
//    double windowWidth = 1920;
//    double windowHeight = 1080;
    // SCREENCHANGES
    double windowWidth = Globals.screenWidth;
    double windowHeight = Globals.screenHeight;
    double mapWidth = windowWidth*Globals.mapXRatio;
    
    double leftInset = 0;
    double topInset = 0;
    
    double newImageWidth = 0;
    double newImageHeight = 0;
    
    int numNodesClicked = 0;
    OsmNode startNode;
    OsmNode endNode;
    
    int highlightedId = -1000;
    
    boolean initialized = false;
    
    
    CreditsManager creditsManager;
    LogManager logManager;
    WorldViewManager worldViewManager;
    ActionsManager actionsManager;
        
    boolean truckLocationsInitializedToNearest = false;
    
    int debuggingCounter = 0;
    
    int num_dots = 0;
    
    public Group screenRoot;
    
//    Image carImageRed, carImageBlue, carImageGreen, carImageYellow, carImageWhite, waterImage, truckTopImage;
    Image waterImage, truckTopImage;
    
    public DrawingManager(Group argScreenRoot){
        
        screenRoot = argScreenRoot;
        
        System.out.println("CONSTRUCTOR OF DRAWING MANAGER");
//        initializeCarImages();
        
        
        
        
    }
    
    
    
    public void postInitializeStuff() throws InterruptedException{
        
        creditsManager = new CreditsManager(0.25*mapWidth, 0.2*windowHeight);
        
        creditsManager.creditsGroup.setLayoutX( 0 );
        creditsManager.creditsGroup.setLayoutY( windowHeight - creditsManager.creditsGroup.getBoundsInLocal().getHeight() );
        screenRoot.getChildren().add( creditsManager.creditsGroup );
        
        logManager = new LogManager(0.75*mapWidth, 0.2*windowHeight);
        
        logManager.logGroup.setLayoutX( creditsManager.creditsGroup.getBoundsInLocal().getWidth() );
        logManager.logGroup.setLayoutY( windowHeight - creditsManager.creditsGroup.getBoundsInLocal().getHeight() );
        screenRoot.getChildren().add( logManager.logGroup );
        
        addLogEntry("Log Started");
                
        double mapHeight = 0.8*windowHeight;        
        worldViewManager = new WorldViewManager(mapWidth, mapHeight);
        
        screenRoot.getChildren().add( worldViewManager.worldViewGroup );
        worldViewManager.worldViewGroup.toBack();
        
        actionsManager = new ActionsManager(windowWidth - mapWidth, windowHeight);
        actionsManager.setMenuLayout(mapWidth, 0);
                
        screenRoot.getChildren().add( actionsManager.actionsManagerGroup );
//        worldViewManager.worldViewGroup.toFront();
        
        
    }
    
    public Group showTextField(){
        
        TextField textField = new TextField(Globals.smlCommunications.hostName);
        
        System.out.println("windowHeight = " + windowHeight);
        
//        textField.setLayoutX(windowWidth/2.0);
//        textField.setLayoutY(windowHeight*(0.9) );
                
        EventHandler<ActionEvent> textFieldChangedAction = new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                
                System.out.println("CHANGING host string");
                
                CharSequence characters = textField.getCharacters();
                Globals.smlCommunications.hostName = characters.toString();
                
//                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        };
        
        textField.setOnAction(textFieldChangedAction);
                
//        loadingScreenGroup.getChildren().add(truckIconBigLightsImageView);
        
        Group textFieldGroup = new Group(textField);
        
        screenRoot.getChildren().add(textFieldGroup);
        
        return textFieldGroup;
        
    }
    
    public Group showPasswordField(Button connectButton){
        
        PasswordField passwordField = new PasswordField();
        
        System.out.println("windowHeight = " + windowHeight);
        
//        textField.setLayoutX(windowWidth/2.0);
//        textField.setLayoutY(windowHeight*(0.9) );
                
        EventHandler<ActionEvent> passwordFieldChangedAction = new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                
                System.out.println("CHANGING password string");
                
                CharSequence characters = passwordField.getCharacters();
                Globals.passwordString = characters.toString();
                
                connectButton.fire();
                
//                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        };
        
        passwordField.setOnAction(passwordFieldChangedAction);
                
//        loadingScreenGroup.getChildren().add(truckIconBigLightsImageView);
        
        Group passwordFieldGroup = new Group(passwordField);
        
        screenRoot.getChildren().add(passwordFieldGroup);
        
        return passwordFieldGroup;
        
    }
        
    public void drawLoadingScreen(){
        
        Group loadingScreenGroup = new Group();
        
        Rectangle backgroundColor = new Rectangle(windowWidth, windowHeight);
        
//        Color bgColor = Color.rgb(82,80,81);
//        backgroundColor.setFill(bgColor);
        backgroundColor.setFill(Color.BLACK);
        
        loadingScreenGroup.getChildren().add(backgroundColor);
        
        Image truckBackgroundImage = new Image("file:background.jpg");
//        Image truckBackgroundImage = new Image("file:steelback.jpg");
//        Image truckBackgroundImage = new Image("file:steelbackground.jpg");
        ImageView truckBackgroundImageView = new ImageView(truckBackgroundImage);
        
        truckBackgroundImageView.setPreserveRatio(false);
        truckBackgroundImageView.setFitWidth(windowWidth);
        System.out.println("windowWidth = " + windowWidth);
        truckBackgroundImageView.setFitHeight(windowHeight);
        System.out.println("windowHeight = " + windowHeight);
        truckBackgroundImageView.setOpacity(0.4);
        
        loadingScreenGroup.getChildren().add(truckBackgroundImageView);
        
        Image truckIconBigImage = new Image("file:truckIconBigNoLights.png");
        ImageView truckIconBigImageView = new ImageView(truckIconBigImage);
        
        double truckImageWidth = truckIconBigImage.getWidth();
        double truckImageHeight = truckIconBigImage.getHeight();
        
        truckIconBigImageView.setPreserveRatio(true);
        truckIconBigImageView.setFitWidth(windowWidth/3.0);
        
        double truckImageViewWidth = truckIconBigImageView.getFitWidth();
        double truckImageViewHeight = truckImageHeight*(truckImageViewWidth/truckImageWidth);
        
        double truckLayoutX = windowWidth/2.0 - truckImageViewWidth/2.0;
        double truckLayoutY = windowHeight/2.0 - truckImageViewHeight/2.0;
        
        truckIconBigImageView.setLayoutX(truckLayoutX);
        truckIconBigImageView.setLayoutY(truckLayoutY);
        
        loadingScreenGroup.getChildren().add(truckIconBigImageView);
        
        Image truckIconBigLightsImage = new Image("file:truckIconBigOnlyLights.png");
        ImageView truckIconBigLightsImageView = new ImageView(truckIconBigLightsImage);
        
//        double truckImageWidth = truckIconBigImage.getWidth();
//        double truckImageHeight = truckIconBigImage.getHeight();
        
        truckIconBigLightsImageView.setPreserveRatio(true);
        truckIconBigLightsImageView.setFitWidth(windowWidth/3.0);
        
//        double truckImageViewWidth = truckIconBigImageView.getFitWidth();
//        double truckImageViewHeight = truckImageHeight*(truckImageViewWidth/truckImageWidth);
        
//        double truckLayoutX = windowWidth/2.0 - truckImageViewWidth/2.0;
//        double truckLayoutY = windowHeight/2.0 - truckImageViewHeight/2.0;
        
        truckIconBigLightsImageView.setLayoutX(truckLayoutX);
        truckIconBigLightsImageView.setLayoutY(truckLayoutY);
        
        loadingScreenGroup.getChildren().add(truckIconBigLightsImageView);
        
        FadeTransition ft = new FadeTransition(Duration.millis(500), truckIconBigLightsImageView);
        ft.setFromValue(1.0);
        ft.setToValue(0.7);
        ft.setCycleCount(10000);
        ft.setAutoReverse(true);
        
        ft.play();
        
        
        Text titleText = new Text();
        titleText.setText("COMMAND CENTRAL");
        titleText.setFont( Font.font("Verdana", FontWeight.BOLD, 100) );
//        titleText.setFont( Font.font("Stencil", 100) );
//        titleText.setFont( Font.font("Arial Black", FontWeight.BOLD, 70) );
//        titleText.setFont( Font.font("Impact", FontWeight.BOLD, 70) );
        titleText.setLayoutX(windowWidth/2.0 - titleText.getBoundsInLocal().getWidth()/2.0);
        titleText.setLayoutY(truckLayoutY/2.0 + titleText.getBoundsInLocal().getHeight()/2.0);
        titleText.setFill(Color.SILVER);
        titleText.setStrokeWidth(5.0);
        titleText.setStroke(Color.BLACK);
        
        loadingScreenGroup.getChildren().add(titleText);
        
        String loadingString = "Searching for world server";
        
        Text infoText = new Text();
        infoText.setText(loadingString);
        infoText.setFont( Font.font("Verdana", FontWeight.BOLD, 60) );
        infoText.setLayoutX(windowWidth/2.0 - infoText.getBoundsInLocal().getWidth()/2.0);
        infoText.setLayoutY(windowHeight - truckLayoutY/2.0  );
        infoText.setFill(Color.LIGHTGRAY);     
        infoText.setStrokeWidth(3.0);
        infoText.setStroke(Color.BLACK);
        
        loadingScreenGroup.getChildren().add(infoText);
        
        PauseTransition pt = new PauseTransition(Duration.millis(.4*1000.));
//        tt.setByX(100f);
//        tt.setCycleCount(1);
                
//        Globals.commsStateMachine = new CommunicationStateMachine(screenRoot, backgroundRoot, statesRoot, trajectoryRoot, truckMenuRoot);
        
        
        
        pt.setOnFinished(new EventHandler<ActionEvent>(){
 
            @Override
            public void handle(ActionEvent arg0) {
                                
                Group loadingScreenGroup = (Group) screenRoot.lookup("#loadingScreenGroup");
                                
                if ( loadingScreenGroup != null ){
                    
                    String dotsString = "";
                    
                    switch (num_dots) {
                        case 0:
                            dotsString = ".";
                            break;
                        case 1:
                            dotsString = "..";
                            break;
                        case 2:
                            dotsString = "...";
                            break;
                        case 3:
                            dotsString = "";
                            break;
                    }
                                        
                    if ( num_dots == 3 ){
                        num_dots = 0;
                    }else{
                        num_dots++;
                    }
                    
                    infoText.setText(loadingString + dotsString);
                    
                    pt.play();
                    
                }                                               
            }
            
        });
                        
        loadingScreenGroup.setId("loadingScreenGroup");
        screenRoot.getChildren().add(loadingScreenGroup);
        
        pt.play();
        
        loadingScreenGroup.toBack();
        
    }
    
    public void clearLoadingScreen() throws InterruptedException{
        
        Group loadingScreenGroup = (Group) screenRoot.lookup("#loadingScreenGroup");
        
        if ( loadingScreenGroup== null ){
            
            System.out.println("In clearLoadingScreen() - Trying to erase something that was already erased!");
            
        }else{
            
            loadingScreenGroup.getChildren().clear();
            screenRoot.getChildren().remove(loadingScreenGroup);
            
        }
        
        postInitializeStuff();
        initialized = true;
        
    }
    

      
    public void promptMissionReplan(int[] bodiesToRecheckArray ){
        
//        asdasd
        System.out.println("promptMissionReplan = " + bodiesToRecheckArray.toString());
        
        int bodyId = bodiesToRecheckArray[0];
        
        System.out.println("TO REPLACE");
//        for ( TruckInformation tempTruckInformation : truckInformationList ){
//            
//            if( tempTruckInformation.getTruckId() == bodyId ){
//                
//                Group truckMenuRoot = (Group) screenRoot.lookup("#truckMenuRoot");
//                Group trajectoryRoot = (Group) screenRoot.lookup("#trajectoryRoot");
//                showTruckOrdersMenu(truckMenuRoot, trajectoryRoot, tempTruckInformation);
//                
//            }            
//            
//        }
        System.out.println("TO REPLACE");
        
    }
        
    public void drawMissionManager(Group statesRoot){
        
        Group missionManagerGroup = (Group) statesRoot.lookup("#missionManagerGroup");
        
        if ( missionManagerGroup == null ){
            
            missionManagerGroup = new Group();
            missionManagerGroup.setId("missionManagerGroup");
            statesRoot.getChildren().add(missionManagerGroup);
            
        }else{
            
            missionManagerGroup.getChildren().clear();
            
            ArrayList<String> truckStates = Globals.missionManager.getTruckStates();
            
            for ( int i = 0; i < truckStates.size(); i++ ){
                
                Text tempText = new Text(truckStates.get(i));
                tempText.setFill(Color.WHITE);
                tempText.setFont(new Font(30));
                tempText.setLayoutX( 50 );
                tempText.setLayoutY( (double)(i+1)*50 );
                missionManagerGroup.getChildren().add(tempText);
                
            } 
            
            ArrayList<String> truckPositions = Globals.missionManager.getTruckPositions();
            
            for ( int i = 0; i < truckPositions.size(); i++ ){
                
                Text tempText = new Text(truckPositions.get(i));
                tempText.setFill(Color.WHITE);
                tempText.setFont(new Font(30));
                tempText.setLayoutX( 250 );
                tempText.setLayoutY( (double)(i+1)*50 );
                missionManagerGroup.getChildren().add(tempText);
                
            } 
            
            
//            ArrayList<Integer> truckOccupancyIds = Globals.missionManager.getTruckOccupancyAsIds();
//            
//            for ( int i = 0; i < truckOccupancyIds.size(); i++ ){
//                
//                Text tempText = new Text(""+truckOccupancyIds.get(i));
//                tempText.setFill(Color.WHITE);
//                tempText.setFont(new Font(30));
//                tempText.setLayoutX( 50 );
//                tempText.setLayoutY( (double)(i+8)*50 );
//                missionManagerGroup.getChildren().add(tempText);
//                
//            } 
                        
                        
//            ArrayList<Integer> truckIds = Globals.missionManager.truckIds;
            ArrayList<Integer> truckIds = new ArrayList<>();
            for ( TruckMissionInformation truckMissionInformation : Globals.missionManager.truckInformationList ){
                truckIds.add(truckMissionInformation.truckId);
            }
            
            
            
            for ( int i = 0; i < truckIds.size(); i++ ){
                                
                Text tempText1 = new Text();
                
                ArrayList<OsmLanelet> trajLanelets = Globals.missionManager.getTruckLaneletPath( truckIds.get(i) );
                ArrayList<Integer> trajLaneletIds = new ArrayList<>();
                
                for (OsmLanelet tempLanelet : trajLanelets){
                    trajLaneletIds.add(tempLanelet.id);
                }
                
                tempText1.setText( trajLaneletIds.toString() );
                tempText1.setFill(Color.WHITE);
                tempText1.setFont(new Font(30));
                tempText1.setLayoutX( 250 );
                tempText1.setLayoutY( (double)(i+8)*50 );
                missionManagerGroup.getChildren().add(tempText1);
                
            } 
            
            Text obstructedLaneletsText = new Text();
            obstructedLaneletsText.setText( "Obstructed Lanelets\n" +Globals.xmlProcesser.laneletsObstructedList.toString() );
            obstructedLaneletsText.setFill(Color.WHITE);
            obstructedLaneletsText.setFont(new Font(30));
            obstructedLaneletsText.setLayoutX( 500 );
            obstructedLaneletsText.setLayoutY( (double)50 );
            missionManagerGroup.getChildren().add(obstructedLaneletsText);
            
            
            
        }
        
        
        
        
        
    }
    
    public void updateLaneletObstructions(){
        
//        Globals.missionManager.newlyObstructedLaneletIds
        
//        System.out.println("");
//        asdasd
        
        //System.out.println(Globals.xmlProcesser.laneletsObstructedList.toString());
        
        Group statesRootGroup = (Group) screenRoot.lookup("#statesRootGroup");
        
        Group laneletsGroup = (Group) statesRootGroup.lookup("#laneletsGroup");
        
        if ( laneletsGroup == null){
            
            System.out.println("Creating a new lanelets group");
            laneletsGroup = new Group();
            statesRootGroup.getChildren().add(laneletsGroup);
            laneletsGroup.setId("laneletsGroup");
            laneletsGroup.toFront();
            
        }
        
        ArrayList<Integer> obstructedLaneletIds = Globals.xmlProcesser.laneletsObstructedList;
        
//        System.out.println("obstructedLaneletIds.toString() = " + obstructedLaneletIds.toString());
        
        for ( int laneletId : obstructedLaneletIds ){
            
            Group currentLaneletGroup = (Group) laneletsGroup.lookup("#"+laneletId);
            
            if ( currentLaneletGroup == null ){
                
                currentLaneletGroup = new Group();
                laneletsGroup.getChildren().add(currentLaneletGroup);
//                currentLaneletGroup.toFront();
                currentLaneletGroup.setId(""+laneletId);
                
//                System.out.println("ADDING A NEW LANELET ID: " + laneletId);
                currentLaneletGroup.getChildren().add( getLaneletPolygon(laneletId) );
                
            }
                                    
        }
        
        ArrayList<Group> groupsToRemove = new ArrayList<>();
        
        for ( Node tempLaneletNode : laneletsGroup.getChildren() ){
            
            int tempLaneletId = Integer.parseInt( tempLaneletNode.getId() );
            
            if ( !obstructedLaneletIds.contains(tempLaneletId) ){
                
                groupsToRemove.add( (Group) tempLaneletNode);
//                laneletsGroup.getChildren().remove(tempLaneletNode);
                
            }
            
        }
        
        for ( Group tempGroupToRemove : groupsToRemove ){
            
            laneletsGroup.getChildren().remove(tempGroupToRemove);
            
        }
              
        
        
    }    
    
    public Group getLaneletPolygon(int laneletId){
        
        Group laneletGroup = new Group();
        
        OsmLanelet lanelet = null;
        
        for( OsmLanelet tempLanelet : Globals.xmlProcesser.laneletList ){
            
            if ( tempLanelet.id == laneletId){
                
                lanelet = tempLanelet;
                break;
                
            }
            
        }
        
        if ( lanelet == null ){
            
            System.err.print("getLaneletPolygon() : Lanelet not found");
            return laneletGroup;
            
        }
        
        OsmWay leftWay = null;
        OsmWay rightWay = null;
        
        for( OsmWay tempway : Globals.xmlProcesser.wayList ){
            
            if ( tempway.id == lanelet.leftWayId){
                
                leftWay = tempway;
                
            }
            if ( tempway.id == lanelet.rightWayId){
                
                rightWay = tempway;
                
            }
                        
        }
                
        if ( leftWay == null || rightWay == null ){
            
            System.err.print("getLaneletPolygon() : Way not found");
            return laneletGroup;
            
        }
        
        ArrayList<OsmNode> leftWayNodes = new ArrayList<>();
        
        for ( int nodeId : leftWay.nodeIds ){
            
            for ( OsmNode node : Globals.xmlProcesser.nodeList ){
                
                if ( node.id == nodeId ){
                    
                    leftWayNodes.add(node);
                    
                }
                
            }
            
        }
        
        ArrayList<OsmNode> rightWayNodes = new ArrayList<>();
        
        for ( int nodeId : rightWay.nodeIds ){
            
            for ( OsmNode node : Globals.xmlProcesser.nodeList ){
                
                if ( node.id == nodeId ){
                    
                    rightWayNodes.add(node);
                                        
                }
                
            }
            
        }
        
        ArrayList<double[]> polygonPoints = new ArrayList<>();
        
        for ( OsmNode node : leftWayNodes ){
            
            double[] nodePosition = new double[2];
            nodePosition[0] = worldViewManager.convertToPixelX(node.X/32.);
            nodePosition[1] = worldViewManager.convertToPixelY(node.Y/32.);
            
            polygonPoints.add(nodePosition);
            
        }
        
        double dist1 = Math.sqrt( Math.pow( leftWayNodes.get(0).X - rightWayNodes.get(0).X , 2 ) + Math.pow( leftWayNodes.get(0).Y - rightWayNodes.get(0).Y , 2 ) );
        double dist2 = Math.sqrt( Math.pow( leftWayNodes.get( leftWayNodes.size()-1 ).X - rightWayNodes.get(0).X , 2 ) + Math.pow( leftWayNodes.get( leftWayNodes.size()-1 ).Y - rightWayNodes.get(0).Y , 2 ) );
        
        if ( dist1 > dist2 ){
            
            for ( OsmNode node : rightWayNodes ){
            
                double[] nodePosition = new double[2];
                nodePosition[0] = worldViewManager.convertToPixelX(node.X/32.);
                nodePosition[1] = worldViewManager.convertToPixelY(node.Y/32.);

                polygonPoints.add(nodePosition);
            
            }
            
        }else{
                
            
            for ( int nodeIdx = rightWayNodes.size() - 1 ; nodeIdx > -1 ; nodeIdx-- ){
            
                OsmNode node = rightWayNodes.get(nodeIdx);
                
                double[] nodePosition = new double[2];
                nodePosition[0] = worldViewManager.convertToPixelX(node.X/32.);
                nodePosition[1] = worldViewManager.convertToPixelY(node.Y/32.);

                polygonPoints.add(nodePosition);
            
            }
                
                
        }
            
        
        
        
        
        
        double[] allPoints = new double[2*polygonPoints.size()];
        
        Polygon polygon = new Polygon();
        polygon.setFill(Color.RED);
//        polygon.setOpacity(0.7);
        
        FadeTransition ft = new FadeTransition(Duration.millis(1500), polygon);
        ft.setFromValue(0.7);
        ft.setToValue(0.3);
        ft.setCycleCount( Animation.INDEFINITE );
        ft.setAutoReverse(true);
        
        ft.play();
        
        for ( int i = 0 ; i < polygonPoints.size() ; i++ ){
            
            double[] currentPoint = new double[2];
            currentPoint = polygonPoints.get(i);
            
            allPoints[2*i] = currentPoint[0];
            allPoints[2*i+1] = currentPoint[1];
            
            polygon.getPoints().add(currentPoint[0]);
            polygon.getPoints().add(currentPoint[1]);
            
            
        }
        
//        System.out.println("polygon.getPoints().toString() = " + polygon.getPoints().toString());
        
        laneletGroup.getChildren().add(polygon);
              
        return laneletGroup;
        
    }
    
    
    
    public void drawSMLSquare(Group backgroundRoot){
        
        double topLeftX = Globals.smlWorldInfo.centerPixelX - Globals.smlWorldInfo.pixelPerMeterX*32.*3.0;
        double topLeftY = Globals.smlWorldInfo.centerPixelY - Globals.smlWorldInfo.pixelPerMeterY*32.*3.0;
        
        Polyline polyline = new Polyline();
        
        polyline.getPoints().addAll(new Double[]{ topLeftX*(windowWidth/1920.0), topLeftY });
        polyline.getPoints().addAll(new Double[]{ ( topLeftX + 6.0*32.*Globals.smlWorldInfo.pixelPerMeterX)*(windowWidth/1920.0), topLeftY });
        polyline.getPoints().addAll(new Double[]{ ( topLeftX + 6.0*32.*Globals.smlWorldInfo.pixelPerMeterX)*(windowWidth/1920.0), topLeftY + 6.0*32.*Globals.smlWorldInfo.pixelPerMeterY });
        polyline.getPoints().addAll(new Double[]{ topLeftX*(windowWidth/1920.0) , topLeftY + 6.0*32.*Globals.smlWorldInfo.pixelPerMeterY });
        polyline.getPoints().addAll(new Double[]{ topLeftX*(windowWidth/1920.0), topLeftY });
        
        backgroundRoot.getChildren().add(polyline);
        
    }
        
    public void previewTruckTrajectory(int destinationId, int truckId ) throws InterruptedException{

        
        boolean bodyTrajectory = true;

//        int endNodeId = 100;
        
        String messageToSend = null;
        
        messageToSend = Globals.xmlProcesser.getTrajectoryRequestMessageString(truckId, destinationId, bodyTrajectory);
        
        
        Globals.commsStateMachine.transitionToTrajectoryWaiting();
        
        System.out.println("Will sleep 0.25 seconds");
        Thread.sleep(250); 

        Globals.smlCommunications.sendOutputMessage(messageToSend);      
        

        System.out.println("Sent message: " + messageToSend);
        
    }
          
    public OsmNode getClosestNode(double x, double y, List<OsmNode> nodeList){
        
        double distance = 1000000;
        int bestId = -1;
        
        for ( int i = 0; i < nodeList.size(); i++){
            
            
            double currentDistance = Math.sqrt( Math.pow( nodeList.get(i).X - x , 2 ) + Math.pow( nodeList.get(i).Y - y , 2 ) );
            
            if (currentDistance < distance){
                
                distance = currentDistance;
                bestId = i;
                
            }
            
        }
        
        return nodeList.get(bestId);
        
    }
    
    
    
    // Actions Manager
    
    
    
    
    
    public int inspectNumberNodesInGroup(Group argGroup){
        
        int numChildren = argGroup.getChildren().size();
        
        int numNodes = numChildren;
        
        for ( int i = 0 ; i < numChildren ; i++ ){
            
            try{
                Group tempGroup = (Group) argGroup.getChildren().get(i);

                numNodes = numNodes + inspectNumberNodesInGroup(tempGroup);
                
            }catch(ClassCastException e){
                
            }
            
        }        
        
        return numNodes;
        
    }
    
    
    // Handlers
    
    public void addLogEntry(String logEntry){
        
        logManager.addLogEntry(logEntry);
        
    }
    
    public void addLogEntry(String logEntry, Paint color){
        
        logManager.addLogEntry(logEntry, color);
        
    }
    
    public void createTrajectoryButton(ArrayList<OsmLanelet> laneletPathIds){
        
        actionsManager.createTrajectoryButton(laneletPathIds);
        
    }
    
    public void updateTrucksMenu(){
        
        if ( actionsManager != null ){
            
            actionsManager.updateTrucksMenu();
            
        }
       
        
    }
    
    public void clearTrajectory(){
        
        worldViewManager.clearTrajectory();
        
    }
    
    public void popUpBodyCompletion() throws TransformerException, ParserConfigurationException{
        
        actionsManager.popUpBodyCompletion();
        
    }
    
    public double convertToPixelX(double argX){
        
        return worldViewManager.convertToPixelX(argX);
        
    }
    
    public double convertToPixelY(double argY){
        
        return worldViewManager.convertToPixelY(argY);
        
    }
    
    public double convertToX(double argPixelX){
        
        return worldViewManager.convertToX(argPixelX);
        
    }
    
    public double convertToY(double argPixelY){
        
        return worldViewManager.convertToY(argPixelY);
        
    }
    
    public void clearRrtSolution(){
        
        worldViewManager.clearRrtSolution();
        
    }
    
    public Group getWayPolygon(int wayId){
        
        return worldViewManager.getWayPolygon(wayId);
        
    }
    
    public void drawRrtSolution() throws InterruptedException{
        
        worldViewManager.drawRrtSolution();
        
    }
     
    public void drawWorldView(){
        
        worldViewManager.drawImage(Color.BLACK);
        
    }
      
    public void drawTrajectory( ArrayList<double[]> trajectoryPointsList , ArrayList<OsmLanelet> laneletPath){
        
        worldViewManager.drawTrajectory(trajectoryPointsList, laneletPath);
        
    }
    
    public void drawReverseTrajectory(  ){
        
        worldViewManager.drawReverseTrajectory();
        
    }
    
    public void drawTrajectory(){
        
        worldViewManager.drawTrajectory();
        
    }
    
    public void drawStates(boolean drawEverything) throws TransformerException, ParserConfigurationException, InterruptedException{
          
        if ( initialized ){
        
            worldViewManager.drawStates(drawEverything);
            updateLaneletObstructions();
            
            if ( !truckLocationsInitializedToNearest ){
                
                Globals.missionManager.initializeTruckLocationsToNearest();
                truckLocationsInitializedToNearest = true;
                
            }
        
        }
        
    }
    
    public void makeRipple(int highlightedId){
        
        worldViewManager.makeRipple(highlightedId);
        
    }
    
    public void clearRipple(){
        
        worldViewManager.clearRipple();
        
    }
       
    
    
            
}
