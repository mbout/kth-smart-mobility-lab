/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testingcommunications;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.Array;
import java.net.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.paint.Color;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author rui
 */
public class XmlProcesser {
    
    List<OsmNode> nodeList;
    List<OsmWay> wayList;
    List<OsmLanelet> laneletList;
    
    List<TrajectoryPoint> trajectoryPointList;
    ArrayList<OsmLanelet> trajectoryLaneletList;
    List<QualisysBody> qualisysBodiesList;
    List<Body> bodiesList;
    
    ArrayList<Integer> laneletsObstructedList;
    
    double centerPixelX;
    double centerPixelY;
    double pixelPerMeterX;
    double pixelPerMeterY;
    
    String hardCodedHostAddress;
    
    public XmlProcesser() {
        
        laneletsObstructedList = new ArrayList<>();
        
    }
    
    public static Document loadXMLFromString(String xmlString) throws Exception
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        InputSource is = new InputSource(new StringReader(xmlString));
        return builder.parse(is);
    }
    
    public String processIncomingMessage(String xmlString){
         
        if (xmlString == null){
            
            System.out.println("Received message is null!");
//            return null;
        }
        
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        Document dom = null;
        
        try {
            //Using factory get an instance of document builder
            DocumentBuilder db = dbf.newDocumentBuilder();
        }catch(ParserConfigurationException pce) {
                pce.printStackTrace();
        }
        
        try {
            //parse using builder to get DOM representation of the XML file
            dom = loadXMLFromString(xmlString);
        } catch (Exception ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Element docEle = dom.getDocumentElement();
        
        
        
        if ( docEle.getTagName().equals("message") ){
            
//            System.out.println("Received a .xml message");
            
            String typeString = docEle.getAttribute("type");
            
            if ( typeString.equals("road_info") ){
                
                System.out.println("Received a road_info message");
                processRoadInfoMessage(xmlString);
                return typeString;
                
            }
                                    
            if ( typeString.equals("trajectory_reply") ){
                
                System.out.println("Received a trajectory_reply message");
                processTrajectoryReplyMessage(xmlString);
                return typeString;
                
            }
            
            if ( typeString.equals("image_properties_reply") ){
                
//                System.out.println("Received a image_properties_reply message");
                processImagePropertiesReplyMessage(xmlString);
                return typeString;
                
            }
            
            if ( typeString.equals("vehicle_states_reply") ){
                
//                System.out.println("Received a vehicle_states_reply message");
                processVehicleStatesReplyMessage(xmlString);
                return typeString;
                
            }
            
            if ( typeString.equals("task_completed") ){
                
//                System.out.println("Received a task_completed message");
//                processTaskCompletedMessage(xmlString);
                return typeString;
                
            }
                       
            if ( typeString.equals("rrt_solution") ){
                
//                System.out.println("Received a task_completed message");
//                processTaskCompletedMessage(xmlString);
                return typeString;
                
            }
            
            if ( typeString.equals("lanelet_obstruction") ){
                
//                System.out.println("Received a task_completed message");
//                processTaskCompletedMessage(xmlString);
//                processLaneletObstructionMessage(xmlString);
                return typeString;
                
            }
            
            if ( typeString.equals("reverse_trajectory_solution") ){
                
//                System.out.println("Received a trajectory_reverse_solution message");
//                processRoadInfoMessage(xmlString);
                return typeString;
                
            }
            
            if ( typeString.equals("construction_finished") ){
                                
                return typeString;
                
            }
                        
        }
                
        return null;
        
    }
    
    public String getImageRequestMessageString(boolean reallySend){
        
        String trajectoryRequestMessage = null;
        
//        System.out.println("InetAddress.getLocalHost() = " + InetAddress.getLocalHost());
//        System.out.println("InetAddress.getLocalHost().getHostAddress() = " + InetAddress.getLocalHost().getHostAddress());
//        System.out.println("InetAddress.getLocalHost().getHostName() = " + InetAddress.getLocalHost().getHostName());

        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder;
        try {
            docBuilder = docFactory.newDocumentBuilder();
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        
        // root elements
        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElement("message");
        
        Attr attr = doc.createAttribute("type");
        attr.setValue("image_request");
        rootElement.setAttributeNode(attr);
        
        attr = doc.createAttribute("really_send");
        
        if ( reallySend ){
            attr.setValue("true");
        }else{
            attr.setValue("false");
        }
        
        rootElement.setAttributeNode(attr);        
//        rootElement.setAttribute(hardCodedHostAddress, hardCodedHostAddress);
        
        doc.appendChild(rootElement);

        // This will find my IP, in order to be able to create a socket for the image
        findAndSetMyIp();     

        Element socket = doc.createElement("socket");
        Attr ipAttr = doc.createAttribute("ip");
        ipAttr.setValue(hardCodedHostAddress);
//        ipAttr.setValue( InetAddress.getLocalHost().getHostAddress() );
        socket.setAttributeNode(ipAttr);
        
        Attr portAttr = doc.createAttribute("port");
        portAttr.setValue( Integer.toString(8090) );
        socket.setAttributeNode(portAttr);
        
        rootElement.appendChild(socket);
        
        // write the content into xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer;
        try {
            transformer = transformerFactory.newTransformer();
        } catch (TransformerConfigurationException ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("getImageRequestMessageString() error");
            return null;
        }
        
        
        
        
        StringWriter writer = new StringWriter();
        try {
            transformer.transform(new DOMSource(doc), new StreamResult(writer));
        } catch (TransformerException ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("getImageRequestMessageString() error");
            return null;
        }
        String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
                
        return output;
        
    }
    
    private void findAndSetMyIp(){
        
        Enumeration networkInterfaces;
        try {
            networkInterfaces = NetworkInterface.getNetworkInterfaces();
        } catch (SocketException ex) {
            System.err.println("findAndSetMyIp() function failed. Closing program!");
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(0);
            return;
        }
        
        while(networkInterfaces.hasMoreElements())
        {
            
//            System.out.println("ONE INTERFACE:");
            NetworkInterface n = (NetworkInterface) networkInterfaces.nextElement();
            Enumeration ee = n.getInetAddresses();
            while (ee.hasMoreElements())
            {
                InetAddress i = (InetAddress) ee.nextElement();
                String ipString = i.getHostAddress();
                String ourDesiredIp = "130.237";
                if ( ipString.contains(ourDesiredIp) ){
                    
                    hardCodedHostAddress = ipString;
                    System.out.println("Decided that my IP is: "+ hardCodedHostAddress);
                    
                }
//                System.out.println(i.getHostAddress());
            }
        }
        
    }
    
    public String getVehicleStatesRequestMessageString(){
        
        String trajectoryRequestMessage = null;
        
//        System.out.println("InetAddress.getLocalHost() = " + InetAddress.getLocalHost());
//        System.out.println("InetAddress.getLocalHost().getHostAddress() = " + InetAddress.getLocalHost().getHostAddress());
//        System.out.println("InetAddress.getLocalHost().getHostName() = " + InetAddress.getLocalHost().getHostName());

        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder;
        try {
            docBuilder = docFactory.newDocumentBuilder();
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("getVehicleStatesRequestMessageString() error");
            return null;
        }
        
        // root elements
        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElement("message");
        
        Attr attr = doc.createAttribute("type");
        attr.setValue("vehicle_states_request");
        rootElement.setAttributeNode(attr);
        
        doc.appendChild(rootElement);
                       
        // write the content into xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer;
        try {
            transformer = transformerFactory.newTransformer();
        } catch (TransformerConfigurationException ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("getVehicleStatesRequestMessageString() error");
            return null;
        }
        
        StringWriter writer = new StringWriter();
        try {
            transformer.transform(new DOMSource(doc), new StreamResult(writer));
        } catch (TransformerException ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("getVehicleStatesRequestMessageString() error");
            return null;
        }
        String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
                        
        return output;
        
    }
    
    public String getImagePropertiesRequestMessageString(){
        
        String trajectoryRequestMessage = null;
        
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder;
        try {
            docBuilder = docFactory.newDocumentBuilder();
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("getImagePropertiesRequestMessageString() error");
            return null;
        }
        
        // root elements
        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElement("message");
        
        Attr attr = doc.createAttribute("type");
        attr.setValue("image_properties_request");
        rootElement.setAttributeNode(attr);
        
        doc.appendChild(rootElement);
        
        // write the content into xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer;
        try {
            transformer = transformerFactory.newTransformer();
        } catch (TransformerConfigurationException ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("getImagePropertiesRequestMessageString() error");
            return null;
        }
        
        StringWriter writer = new StringWriter();
        try {
            transformer.transform(new DOMSource(doc), new StreamResult(writer));
        } catch (TransformerException ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("getImagePropertiesRequestMessageString() error");
            return null;
        }
        String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
                
        return output;
        
    }
    
    public String getHighlightRequestMessageString(int stateId, boolean highlight){
        
//          <message type="highlight_request">
//            <vehicle id="4" highlight = "0" />    highlight is either 0 or 1
//          </message>
                        
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder;
        try {
            docBuilder = docFactory.newDocumentBuilder();
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("getHighlightRequestMessageString() error");
            return null;
        }
        
        
        
        
        
        // root elements
        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElement("message");
        
        Attr attr = doc.createAttribute("type");
        attr.setValue("highlight_request");
        rootElement.setAttributeNode(attr);
        
        doc.appendChild(rootElement);
        
        Element vehicle = doc.createElement("vehicle");
        Attr idAttr = doc.createAttribute("id");
        idAttr.setValue( Integer.toString(stateId) );
        vehicle.setAttributeNode(idAttr);
        
        Attr highlightAttr = doc.createAttribute("highlight");
        if ( highlight ){
            highlightAttr.setValue( Integer.toString(1) );
        }else{
            highlightAttr.setValue( Integer.toString(0) );
        }
        vehicle.setAttributeNode(highlightAttr);
        
        rootElement.appendChild(vehicle);
        
        // write the content into xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer;
        try {
            transformer = transformerFactory.newTransformer();
        } catch (TransformerConfigurationException ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("getHighlightRequestMessageString() error");
            return null;
        }
        
        StringWriter writer = new StringWriter();
        try {
            transformer.transform(new DOMSource(doc), new StreamResult(writer));
        } catch (TransformerException ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("getHighlightRequestMessageString() error");
            return null;
        }
        String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
        
        return output;
        
    }
    
    public String getTrajectoryRequestMessageString(int startId, int endId, boolean bodyRequest){
        
//          <message id="5" type="trajectory_request">
//            <start id="4"/>
//            <end id="3"/>
//          </message>
        
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder;
        try {
            docBuilder = docFactory.newDocumentBuilder();
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("getTrajectoryRequestMessageString() error");
            return null;
        }
        
        // root elements
        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElement("message");
        
        Attr attr = doc.createAttribute("type");
        
        if ( bodyRequest ){
            attr.setValue("body_trajectory_request");
        } else{
            attr.setValue("trajectory_request");
        }
                
        rootElement.setAttributeNode(attr);
        
        attr = doc.createAttribute("id");
        attr.setValue("5");
        rootElement.setAttributeNode(attr);
        
        doc.appendChild(rootElement);
        
        Element start = null;
        
        if ( bodyRequest ){
            start = doc.createElement("body");
        } else{
            start = doc.createElement("start");
        }
        Attr idAttr = doc.createAttribute("id");
        idAttr.setValue( Integer.toString(startId) );
        start.setAttributeNode(idAttr);
        
        rootElement.appendChild(start);
        
        Element end = doc.createElement("end");
        idAttr = doc.createAttribute("id");
        idAttr.setValue( Integer.toString(endId) );
        end.setAttributeNode(idAttr);
        
        rootElement.appendChild(end);
        
        // write the content into xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer;
        try {
            transformer = transformerFactory.newTransformer();
//        DOMSource source = new DOMSource(doc);
//        StreamResult result = new StreamResult(new File("C:\\file.xml"));
        } catch (TransformerConfigurationException ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("getTrajectoryRequestMessageString() error");
            return null;
        }
        
        StringWriter writer = new StringWriter();
        try {
            transformer.transform(new DOMSource(doc), new StreamResult(writer));
        } catch (TransformerException ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("getTrajectoryRequestMessageString() error");
            return null;
        }
        String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
        
        return output;
    }
    
    public String getTrajectoryCommandMessageString(int bodyId, List<TrajectoryPoint> trajectoryPointList){
        
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder;
        try {
            docBuilder = docFactory.newDocumentBuilder();
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("getTrajectoryCommandMessageString() error");
            return null;
        }
        
        // root elements
        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElement("message");
        
        Attr attr = doc.createAttribute("type");
        attr.setValue("body_trajectory_command");
        rootElement.setAttributeNode(attr);
        
        attr = doc.createAttribute("body_id");
        attr.setValue(Integer.toString(bodyId));
        rootElement.setAttributeNode(attr);
        
        doc.appendChild(rootElement);
        
        Element trajectory = null;
        
        trajectory = doc.createElement("trajectory");
        
        for ( int i = 0 ; i < trajectoryPointList.size() ; i++ ){
            
            Element trajPoint = doc.createElement("point");
            
            Attr seqId = doc.createAttribute("seq");
            seqId.setValue( Integer.toString(i) );
            trajPoint.setAttributeNode(seqId);
            
            Attr xCoord = doc.createAttribute("x");
            xCoord.setValue( Double.toString( trajectoryPointList.get(i).X ) );
            trajPoint.setAttributeNode(xCoord);
            
            Attr yCoord = doc.createAttribute("y");
            yCoord.setValue( Double.toString( trajectoryPointList.get(i).Y ) );
            trajPoint.setAttributeNode(yCoord);

            trajectory.appendChild(trajPoint);
            
        }
        
        rootElement.appendChild(trajectory);
        
        // write the content into xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer;
        try {
            transformer = transformerFactory.newTransformer();
//        DOMSource source = new DOMSource(doc);
//        StreamResult result = new StreamResult(new File("C:\\file.xml"));
        } catch (TransformerConfigurationException ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("getTrajectoryCommandMessageString() error");
            return null;
        }
        
        StringWriter writer = new StringWriter();
        try {
            transformer.transform(new DOMSource(doc), new StreamResult(writer));
        } catch (TransformerException ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("getTrajectoryCommandMessageString() error");
            return null;
        }
        String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
        
        return output;
        
    }
    
    public String getVehicleStopCommandMessageString(int[] bodyIds){
        
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder;
        try {
            docBuilder = docFactory.newDocumentBuilder();
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("getVehicleStopCommandMessageString() error");
            return null;
        }
        
        // root elements
        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElement("message");
        
        Attr attr = doc.createAttribute("type");
        attr.setValue("vehicle_stop_command");
        rootElement.setAttributeNode(attr);
        
        doc.appendChild(rootElement);
        
//        attr = doc.createAttribute("body_id");
//        attr.setValue(Integer.toString(bodyId));
//        rootElement.setAttributeNode(attr);
//        
        
//        
//        Element trajectory = null;
//        
//        trajectory = doc.createElement("trajectory");
        
        for ( int i = 0 ; i < bodyIds.length ; i++ ){
            
            Element bodyEl = doc.createElement("body");
            
            Attr idAttr = doc.createAttribute("id");
            idAttr.setValue( Integer.toString( bodyIds[i] ) );
            bodyEl.setAttributeNode(idAttr);
  
            rootElement.appendChild(bodyEl);
            
        }
        
        
        // write the content into xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer;
        try {
            transformer = transformerFactory.newTransformer();
//        DOMSource source = new DOMSource(doc);
//        StreamResult result = new StreamResult(new File("C:\\file.xml"));
        } catch (TransformerConfigurationException ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("getVehicleStopCommandMessageString() error");
            return null;
        }
        
        StringWriter writer = new StringWriter();
        try {
            transformer.transform(new DOMSource(doc), new StreamResult(writer));
        } catch (TransformerException ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("getVehicleStopCommandMessageString() error");
            return null;
        }
        String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
        
        return output;
        
    }
        
    public String getObstacleRequestMessageString(int obstacleId, double obstacleRadius, double argX, double argY){
        
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder;
        try {
            docBuilder = docFactory.newDocumentBuilder();
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("getReverseTrajectoryCommandMessageString() error");
            return null;
        }
        
        // root elements
        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElement("message");
        
        Attr attr = doc.createAttribute("type");
        attr.setValue("obstacle_command");
        rootElement.setAttributeNode(attr);
        
        doc.appendChild(rootElement);
        
        // Create obstacle Element
        Element obstacleElement = null;
        
        obstacleElement = doc.createElement("vehicle");
        
        obstacleElement.setAttribute("id", ""+obstacleId);
        obstacleElement.setAttribute("type", "obstacle");
        obstacleElement.setAttribute("radius", ""+obstacleRadius);
        obstacleElement.setAttribute("obstacle_type", "circle");
        
        obstacleElement.setAttribute("x", ""+argX);
        obstacleElement.setAttribute("y", ""+argY);
               
        rootElement.appendChild(obstacleElement);
        
        // write the content into xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer;
        try {
            transformer = transformerFactory.newTransformer();
//        DOMSource source = new DOMSource(doc);
//        StreamResult result = new StreamResult(new File("C:\\file.xml"));
        } catch (TransformerConfigurationException ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("getObstacleRequestMessageString() error");
            return null;
        }
        
        StringWriter writer = new StringWriter();
        try {
            transformer.transform(new DOMSource(doc), new StreamResult(writer));
        } catch (TransformerException ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("getObstacleRequestMessageString() error");
            return null;
        }
        String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
        
        return output;   
        
    }
    
    public String getReverseTrajectoryCommandMessageString(){
        
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder;
        try {
            docBuilder = docFactory.newDocumentBuilder();
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("getReverseTrajectoryCommandMessageString() error");
            return null;
        }
        
        // root elements
        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElement("message");
        
        Attr attr = doc.createAttribute("type");
        attr.setValue("reverse_trajectory_command");
        rootElement.setAttributeNode(attr);
        
        doc.appendChild(rootElement);
        
        // write the content into xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer;
        try {
            transformer = transformerFactory.newTransformer();
//        DOMSource source = new DOMSource(doc);
//        StreamResult result = new StreamResult(new File("C:\\file.xml"));
        } catch (TransformerConfigurationException ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("getReverseTrajectoryCommandMessageString() error");
            return null;
        }
        
        StringWriter writer = new StringWriter();
        try {
            transformer.transform(new DOMSource(doc), new StreamResult(writer));
        } catch (TransformerException ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("getReverseTrajectoryCommandMessageString() error");
            return null;
        }
        String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
        
        return output;   
        
    }
    
    public String getReverseTrajectoryRequestMessageString(int bodyId, int trailerId, double[] goal_pose, double[] start_pose, ArrayList<ArrayList<double[]>> obstaclesCornersList, double timeOutSeconds){
        
        if ( goal_pose.length != 4 ){
            System.err.print("getReverseTrajectoryRequestMessageString() -> goal_pose with dimension different than 4");
        }
        if ( start_pose.length != 4 ){
            System.err.print("getReverseTrajectoryRequestMessageString() -> start_pose with dimension different than 4");
        }
        
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder;
        try {
            docBuilder = docFactory.newDocumentBuilder();
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("getReverseTrajectoryRequestMessageString() error");
            return null;
        }
        
        // root elements
        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElement("message");
        
        Attr attr = doc.createAttribute("type");
        attr.setValue("reverse_trajectory_request");
        rootElement.setAttributeNode(attr);
        
        doc.appendChild(rootElement);
        
        // Create the Body Element
        Element bodyElement = doc.createElement("body");
                
        Attr idAttr = doc.createAttribute("id");
        idAttr.setValue( Integer.toString( bodyId ) );
        bodyElement.setAttributeNode(idAttr);
        
        rootElement.appendChild(bodyElement);
        
         // Create the Trailer Element
        Element trailerElement = doc.createElement("trailer");
                
        Attr trailerIdAttr = doc.createAttribute("id");
        trailerIdAttr.setValue( Integer.toString( trailerId ) );
        trailerElement.setAttributeNode(trailerIdAttr);
        
        rootElement.appendChild(trailerElement);
            
        // Create the Start Pose Element
        Element startPoseElement = doc.createElement("start_pose");
                
        Attr xAttr = doc.createAttribute("x");
        xAttr.setValue( Double.toString( start_pose[0] ) );
        startPoseElement.setAttributeNode(xAttr);

        Attr yAttr = doc.createAttribute("y");
        yAttr.setValue( Double.toString( start_pose[1] ) );
        startPoseElement.setAttributeNode(yAttr);
        
        Attr theta0Attr = doc.createAttribute("theta_0");
        theta0Attr.setValue( Double.toString( start_pose[2] ) );
        startPoseElement.setAttributeNode(theta0Attr);
        
        Attr theta1Attr = doc.createAttribute("theta_1");
        theta1Attr.setValue( Double.toString( start_pose[3] ) );
        startPoseElement.setAttributeNode(theta1Attr);
        
        rootElement.appendChild(startPoseElement);
        
        // Create the Goal Pose Element
        Element goalPoseElement = doc.createElement("goal_pose");

        xAttr = doc.createAttribute("x");
        xAttr.setValue( Double.toString( goal_pose[0] ) );
        goalPoseElement.setAttributeNode(xAttr);

        yAttr = doc.createAttribute("y");
        yAttr.setValue( Double.toString( goal_pose[1] ) );
        goalPoseElement.setAttributeNode(yAttr);
        
        theta0Attr = doc.createAttribute("theta_0");
        theta0Attr.setValue( Double.toString( goal_pose[2] ) );
        goalPoseElement.setAttributeNode(theta0Attr);
        
        theta1Attr = doc.createAttribute("theta_1");
        theta1Attr.setValue( Double.toString( goal_pose[3] ) );
        goalPoseElement.setAttributeNode(theta1Attr);
        
        rootElement.appendChild(goalPoseElement);
        
        
        // Create the timeout element
        Element timeOutElement = doc.createElement("timeout");

        xAttr = doc.createAttribute("seconds");
        xAttr.setValue( Double.toString( timeOutSeconds ) );
        timeOutElement.setAttributeNode(xAttr);
        
        rootElement.appendChild(timeOutElement);
        
        for ( ArrayList<double[]> obstacle : obstaclesCornersList ){
            
            Element obstacleElement = doc.createElement("obstacle");
            
            for ( double[] corner : obstacle ){
            
                Element cornerElement = doc.createElement("corner");
                
                xAttr = doc.createAttribute("x");
                xAttr.setValue( Double.toString( corner[0] ) );
                cornerElement.setAttributeNode(xAttr);

                yAttr = doc.createAttribute("y");
                yAttr.setValue( Double.toString( corner[1] ) );
                cornerElement.setAttributeNode(yAttr);
                
                obstacleElement.appendChild(cornerElement);
                
            }
            
            rootElement.appendChild(obstacleElement);
            
        }
        
        
        // write the content into xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer;
        try {
            transformer = transformerFactory.newTransformer();
//        DOMSource source = new DOMSource(doc);
//        StreamResult result = new StreamResult(new File("C:\\file.xml"));
        } catch (TransformerConfigurationException ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("getReverseTrajectoryRequestMessageString() error");
            return null;
        }
        
        StringWriter writer = new StringWriter();
        try {
            transformer.transform(new DOMSource(doc), new StreamResult(writer));
        } catch (TransformerException ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("getReverseTrajectoryRequestMessageString() error");
            return null;
        }
        String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
        
        return output;
        
    }
        
    public String getRrtTrajectoryRequestMessageString(double[] initialState, double[] goalPosition, int bodyId){
        
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder;
        try {
            docBuilder = docFactory.newDocumentBuilder();
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("getRrtTrajectoryRequestMessageString() error");
            return null;
        }
        
        // root elements
        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElement("message");
        
        Attr attr = doc.createAttribute("type");
        attr.setValue("rrt_trajectory_request");
        rootElement.setAttributeNode(attr);
                
        doc.appendChild(rootElement);
        
        Element initialStateElement = null;
        
        initialStateElement = doc.createElement("initial_state");
        
        initialStateElement.setAttribute("x", ""+initialState[0]);
        initialStateElement.setAttribute("y", ""+initialState[1]);
        initialStateElement.setAttribute("theta_0", ""+initialState[2]);
        initialStateElement.setAttribute("theta_1", ""+initialState[3]);
        initialStateElement.setAttribute("body_id", ""+bodyId);
       
        
        rootElement.appendChild(initialStateElement);
        
        
        Element goalPositionElement = null;
        
        goalPositionElement = doc.createElement("goal_position");
        
        goalPositionElement.setAttribute("x", ""+goalPosition[0]);
        goalPositionElement.setAttribute("y", ""+goalPosition[1]);
               
        rootElement.appendChild(goalPositionElement);
        
        // write the content into xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer;
        try {
            transformer = transformerFactory.newTransformer();
//        DOMSource source = new DOMSource(doc);
//        StreamResult result = new StreamResult(new File("C:\\file.xml"));
        } catch (TransformerConfigurationException ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("getRrtTrajectoryRequestMessageString() error");
            return null;
        }
        
        StringWriter writer = new StringWriter();
        try {
            transformer.transform(new DOMSource(doc), new StreamResult(writer));
        } catch (TransformerException ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("getRrtTrajectoryRequestMessageString() error");
            return null;
        }
        String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
        
        System.out.println("RRT_request_xml: " + output);
        
        return output;
        
    }
    
    public String getConstructionCommandMessageString(ArrayList<Integer> bodyIds, ArrayList<Double> desiredYaws){
        
        if ( bodyIds.size() != desiredYaws.size() ){
            System.err.print("getConstructionCommandMessageString() -> bodyIds and desiredYaws with different size");
        }
        
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder;
        try {
            docBuilder = docFactory.newDocumentBuilder();
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("getConstructionCommandMessageString() failed in the XML API! docBuilder");
            return null;
        }
        
        // root elements
        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElement("message");
        
        Attr attr = doc.createAttribute("type");
        attr.setValue("construction_command");
        rootElement.setAttributeNode(attr);
        
        doc.appendChild(rootElement);
            
        for ( int i = 0 ; i < bodyIds.size() ; i++ ){
            
            // Create the body command Element
            Element bodyCommandElement = doc.createElement("order");

            int bodyId = bodyIds.get(i);
            
            Attr idAttr = doc.createAttribute("id");
            idAttr.setValue( Integer.toString( bodyId ) );
            bodyCommandElement.setAttributeNode(idAttr);
            
            double yawCommand = desiredYaws.get(i);
            
            Attr yawAttr = doc.createAttribute("yaw");
            yawAttr.setValue( Double.toString( yawCommand ) );
            bodyCommandElement.setAttributeNode(yawAttr);

            rootElement.appendChild(bodyCommandElement);
            
        }
        
        // write the content into xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer;
        try {
            transformer = transformerFactory.newTransformer();
//        DOMSource source = new DOMSource(doc);
//        StreamResult result = new StreamResult(new File("C:\\file.xml"));
        } catch (TransformerConfigurationException ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("getConstructionCommandMessageString() failed in the XML API! transformer");
            return null;
        }
        
        StringWriter writer = new StringWriter();
        try {
            transformer.transform(new DOMSource(doc), new StreamResult(writer));
        } catch (TransformerException ex) {
            
           
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
            
            System.err.println("getConstructionCommandMessageString() failed in the XML API! transformer.transform");
            System.err.println( ex.toString() );
            return null;
            
        }
        String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
        
        return output;
        
    }
        
    public void processImagePropertiesReplyMessage(String xmlString){
        
//        parseXmlFile();
        
        
        //get the factory
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        Document dom = null;
        
        try {

                //Using factory get an instance of document builder
            DocumentBuilder db = dbf.newDocumentBuilder();

        }catch(ParserConfigurationException pce) {
                pce.printStackTrace();
        }
        
        try {
                //parse using builder to get DOM representation of the XML file
            dom = loadXMLFromString(xmlString);
        } catch (Exception ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
        }
        
            //get each employee element and create a Employee object
//        parseDocument();
        
        //get the root elememt
        Element docEle = dom.getDocumentElement();
        
        //get a nodelist of <employee> elements
        NodeList nl = docEle.getElementsByTagName("image_info");
//        System.out.println("There are " + nl.getLength() + " nodes in the xml.");
                        
        centerPixelX = 0;
        centerPixelY = 0;
        pixelPerMeterX = 0;
        pixelPerMeterY = 0;
        
        if(nl != null && nl.getLength() > 0) {
                    
            for(int i = 0 ; i < nl.getLength();i++) {
                
                Element el = (Element)nl.item(i);
                
                centerPixelX = Double.parseDouble( el.getAttribute("center_pixel_x") );
                centerPixelY = Double.parseDouble( el.getAttribute("center_pixel_y") );
                pixelPerMeterX = Double.parseDouble( el.getAttribute("pixel_per_meter") );
                pixelPerMeterY = Double.parseDouble( el.getAttribute("pixel_per_meter") );
                
                System.out.println("centerPixelX = " + centerPixelX);
                System.out.println("centerPixelY = " + centerPixelY);
                System.out.println("pixelPerMeterX = " + pixelPerMeterX);
                System.out.println("pixelPerMeterY = " + pixelPerMeterY);
                
                Globals.smlWorldInfo.centerPixelX = centerPixelX;
                Globals.smlWorldInfo.centerPixelY = centerPixelY;
                Globals.smlWorldInfo.pixelPerMeterX = pixelPerMeterX;
                Globals.smlWorldInfo.pixelPerMeterY = pixelPerMeterY;
            }
        }
                
        
    }
    
    public void processVehicleStatesReplyMessage(String xmlString){
               
//        System.out.println("processVehicleStatesReplyMessage = " + xmlString);
        
        //get the factory
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        Document dom = null;
        
        try {
                //Using factory get an instance of document builder
            DocumentBuilder db = dbf.newDocumentBuilder();
        }catch(ParserConfigurationException pce) {
                pce.printStackTrace();
        }
        
        try {
            //parse using builder to get DOM representation of the XML file
            dom = loadXMLFromString(xmlString);
        } catch (Exception ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
        }
        
            //get each employee element and create a Employee object
//        parseDocument();
        qualisysBodiesList = new ArrayList();
        bodiesList = new ArrayList<>();
        
        //get the root elememt
        Element docEle = dom.getDocumentElement();
        
        //get a nodelist of <employee> elements
        NodeList nl = docEle.getElementsByTagName("body_info");
//        System.out.println("There are " + nl.getLength() + " nodes in the xml.");
                        
//        System.out.println("STATES ACQUIRED:");
        
        if(nl != null && nl.getLength() > 0) {
                    
            for(int i = 0 ; i < nl.getLength();i++) {
                
                Element el = (Element)nl.item(i);
                
                double bodyX = Double.parseDouble( el.getAttribute("x") );
                double bodyY = Double.parseDouble( el.getAttribute("y") );
                double bodyTheta = Double.parseDouble( el.getAttribute("yaw") );
                int bodyId = Integer.parseInt( el.getAttribute("id") );
                String stringControllable = el.getAttribute("controllable");
                double networkConnectivity = 0.0;
                
                if ( el.hasAttribute("signal_strength") ){
                    
                    networkConnectivity = Double.parseDouble( el.getAttribute("signal_strength") );
                    
                }
                                
                boolean booleanControllable = false;
                
//                System.out.println("stringControllable = " + stringControllable);
                
                if ( stringControllable.equals("True") ){
                    booleanControllable = true;
                }else{
                    booleanControllable = false;
                }
                    
                QualisysBody tempBody = new QualisysBody(bodyX, bodyY, bodyTheta, bodyId, booleanControllable, networkConnectivity);
               
                if ( el.hasAttribute("obstacle") ){
                
                    if ( el.getAttribute("obstacle").equals("True") ){
                     
                        if ( el.hasAttribute("obstacle_type") ){
                            
                            String obstacleTypeString;
                            obstacleTypeString = el.getAttribute("obstacle_type");
                            tempBody.setObstacleType( obstacleTypeString );
                            
//                            System.out.println("Received a obstacleTypeString = " + obstacleTypeString + " with id = " + bodyId);
                            
                            if ( obstacleTypeString.equals("rectangle") || obstacleTypeString.equals("rrt") ){

                                if ( el.hasAttribute("width") ){
                                    tempBody.setWidth( Double.parseDouble( el.getAttribute("width") ) );   
                                }else{
                                    System.err.println("Obstacle width not found!");
                                }
                                if ( el.hasAttribute("height") ){
                                    tempBody.setHeight( Double.parseDouble( el.getAttribute("height") ) );   
                                }else{
                                    System.err.println("Obstacle height not found!");
                                }                       

                            }else{
                                
                                if ( el.hasAttribute("radius") ){
                                    tempBody.setRadius( Double.parseDouble( el.getAttribute("radius") ) );   
                                }else{
                                    System.err.println("Obstacle radius not found!");
                                }
                                
                            }
                            
                        }
                        
                    }
                
                }
                
                if ( el.hasAttribute("body_type") ){
                                          
                    String bodyTypeString = el.getAttribute("body_type");
                    
//                    System.out.println("bodyTypeString = " + bodyTypeString);
                    
                    tempBody.setBodyType( bodyTypeString );
                    
                }
                
                
                if ( el.hasAttribute("trailer") ){
                    
                    String trailerString = el.getAttribute("trailer");
                    
                    if ( trailerString.toLowerCase().equals("true") ){
                        
                        tempBody.setTrailer(true);
                        
                    }
                    
                }
                                
                qualisysBodiesList.add(tempBody);
                
                Body currentBody;
                
                if ( el.hasAttribute("body_type") ){
                                          
                    String bodyTypeString = el.getAttribute("body_type");
                                                            
                    if ( bodyTypeString.equals("smart_vehicle") ) {

                        double axleDistance = 2.8;
                        
                        currentBody = new SmartVehicle(axleDistance);
                        
                    } else if ( bodyTypeString.equals("dummy_vehicle") ){

                        double axleDistance = 2.8;
                        
                        currentBody = new DummyVehicle(axleDistance);
                        
                    } else {
                        
                        currentBody = new Body();
                        
                    }
                    
                    currentBody.id = bodyId;
                    
                    currentBody.x = bodyX;
                    currentBody.y = bodyY;
                    currentBody.yaw = bodyTheta;
                    
                    currentBody.controllable = booleanControllable;                    
                    currentBody.networkConnectivity = networkConnectivity;
                                        
                    bodiesList.add(currentBody);
                                    
                }
                
                                                        
            }
        }
                
        
    }
        
    public void processTaskCompletedMessage(String xmlString){
        
        //get the factory
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        Document dom = null;
        
        try {
                //Using factory get an instance of document builder
            DocumentBuilder db = dbf.newDocumentBuilder();
        }catch(ParserConfigurationException pce) {
                pce.printStackTrace();
        }
        
        try {
            //parse using builder to get DOM representation of the XML file
            dom = loadXMLFromString(xmlString);
        } catch (Exception ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        //get the root elememt
        Element docEle = dom.getDocumentElement();
        
        //get a nodelist of <employee> elements
        NodeList nl = docEle.getElementsByTagName("body");
//        System.out.println("There are " + nl.getLength() + " nodes in the xml.");
                        
//        System.out.println("STATES ACQUIRED:");
        
        int bodyId = -1;
        
        if(nl != null && nl.getLength() > 0) {
                    
            for(int i = 0 ; i < nl.getLength();i++) {
                
                Element el = (Element)nl.item(i);
                
//                double bodyX = Double.parseDouble( el.getAttribute("x") );
//                double bodyY = Double.parseDouble( el.getAttribute("y") );
//                double bodyTheta = Double.parseDouble( el.getAttribute("theta") );
                 bodyId = Integer.parseInt( el.getAttribute("id") );
//                String stringControllable = el.getAttribute("controllable");
                
                                                        
            }
        }
             
        Globals.drawingManager.actionsManager.completedBodies.add(bodyId);
        
    }
        
    public void processRrtSolutionMessage(String xmlString){
        
        //get the factory
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        Document dom = null;
        
        try {
                //Using factory get an instance of document builder
            DocumentBuilder db = dbf.newDocumentBuilder();
        }catch(ParserConfigurationException pce) {
                pce.printStackTrace();
        }
        
        try {
            //parse using builder to get DOM representation of the XML file
            dom = loadXMLFromString(xmlString);
        } catch (Exception ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
        }
                
        //get the root elememt
        Element docEle = dom.getDocumentElement();
        
        //get a nodelist of <employee> elements
        NodeList nl = docEle.getElementsByTagName("tree_node");
        System.out.println("There are " + nl.getLength() + " tree nodes in the RRT solution.");
        
        ArrayList<RrtNode> newRrtNodes = new ArrayList<>();
                        
        if(nl != null && nl.getLength() > 0) {
                    
            for(int i = 0 ; i < nl.getLength();i++) {
                
                Element el = (Element)nl.item(i);
                
                int nodeId = Integer.parseInt( el.getAttribute("id") );
                int parentId = Integer.parseInt( el.getAttribute("parent_id") ); 
                double nodeX = Double.parseDouble( el.getAttribute("x") );
                double nodeY = Double.parseDouble( el.getAttribute("y") );
                
                
                RrtNode tempNode = new RrtNode(nodeId, parentId, nodeX, nodeY);
                
                newRrtNodes.add(tempNode);               
                                                        
            }
        }
        
        
        Globals.currentRrtSolution.setTreeNodes(newRrtNodes);
        
//        System.out.println("STATES ACQUIRED:");
        
        NodeList nli = docEle.getElementsByTagName("node");
        System.out.println("There are " + nli.getLength() + " solution nodes in the RRT solution.");
        
        
        ArrayList<Integer> nodeIds = new ArrayList<>();
        ArrayList<Integer> nodeOrder = new ArrayList<>();
//        int bodyId = -1;
//        
        if(nli != null && nl.getLength() > 0) {
                    
            for(int i = 0 ; i < nli.getLength();i++) {
                
                Element el = (Element)nli.item(i);
                
                int nodeId = Integer.parseInt( el.getAttribute("id") );
                int order = Integer.parseInt( el.getAttribute("order") );
                
                nodeIds.add(nodeId);
                nodeOrder.add(order);                
                                                        
            }
        }
        
        Globals.currentRrtSolution.setSolutionNodes(nodeIds);
        
        for ( int i = 1 ; i < nodeOrder.size() ; i ++){
            
            if ( nodeOrder.get(i-1) > nodeOrder.get(i) ){
                
                System.out.println("PARSING OF RRT NODE SOLUTION COMING IN WRONG ORDER!!!");
                
            }
            
        }
        
    }
    
    public void processTrajectoryReplyMessage(String xmlString){
        
//        parseXmlFile();
        
        
        //get the factory
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        Document dom = null;
        
        try {
            //Using factory get an instance of document builder
            DocumentBuilder db = dbf.newDocumentBuilder();
        }catch(ParserConfigurationException pce) {
            pce.printStackTrace();
        }
        
        try {
            //parse using builder to get DOM representation of the XML file
            dom = loadXMLFromString(xmlString);
        } catch (Exception ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
        }
        
            //get each employee element and create a Employee object
//        parseDocument();
        
        //get the root elememt
        Element docEle = dom.getDocumentElement();
        
        //get a nodelist of <employee> elements
        NodeList nl = docEle.getElementsByTagName("point");
//        System.out.println("There are " + nl.getLength() + " nodes in the xml.");
        
        trajectoryPointList = new ArrayList();
                
        if(nl != null && nl.getLength() > 0) {
                    
            for(int i = 0 ; i < nl.getLength();i++) {
                
                Element el = (Element)nl.item(i);
                
                double time = Double.parseDouble( el.getAttribute("time") );
                double xPos = Double.parseDouble( el.getAttribute("x") );
                double yPos = Double.parseDouble( el.getAttribute("y") );
                                
                trajectoryPointList.add( new TrajectoryPoint(time, xPos, yPos) );
                        
            }
        }
        
        
        
        nl = docEle.getElementsByTagName("lanelet");
        
        trajectoryLaneletList = new ArrayList();
                
        if(nl != null && nl.getLength() > 0) {
                    
            for(int i = 0 ; i < nl.getLength();i++) {
                
                Element el = (Element)nl.item(i);
                
                double time = Double.parseDouble( el.getAttribute("time") );
                int laneletId = Integer.parseInt( el.getAttribute("id") );
                                
//                Globals.smlWorldInfo.
                        
                for ( OsmLanelet tempLanelet : laneletList ){
                    
                    if ( tempLanelet.id == laneletId ){
                        
                        trajectoryLaneletList.add( tempLanelet );
                        continue;
                        
                    }
                    
                }
                        
                
                        
            }
        }
        
        
        
        if ( trajectoryLaneletList.size() != nl.getLength() ){
            
            System.err.print("processTrajectoryReplyMessage has a wrong number of lanelets!");
                        
        }
        
        
        
    }
    
    public void processLaneletObstructionMessage(String xmlString){
        
        //System.out.println("################processLaneletObstructionMessage################");
        System.out.println("Received lanelet obstruction:\n" + xmlString);
        laneletsObstructedList = new ArrayList<>();
        
        //get the factory
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        Document dom = null;
        
        try {
            //Using factory get an instance of document builder
            DocumentBuilder db = dbf.newDocumentBuilder();
        }catch(ParserConfigurationException pce) {
            pce.printStackTrace();
        }
        
        try {
            //parse using builder to get DOM representation of the XML file
            dom = loadXMLFromString(xmlString);
        } catch (Exception ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
        }
        
            //get each employee element and create a Employee object
//        parseDocument();
        
        //get the root elememt
        Element docEle = dom.getDocumentElement();
        
        //get a nodelist of <employee> elements
        NodeList nl = docEle.getElementsByTagName("lanelet");
//        System.out.println("There are " + nl.getLength() + " nodes in the xml.");
                        
        if(nl != null && nl.getLength() > 0) {
                    
            for(int i = 0 ; i < nl.getLength();i++) {
                
                Element el = (Element)nl.item(i);
                
                int laneletId = Integer.parseInt( el.getAttribute("id") );
                
                laneletsObstructedList.add( laneletId );
                        
            }
        }
                
    }
    
    public void processRoadInfoMessage(String xmlString){
        
        Globals.routePlanner = new RoutePlanner();
        
//        parseXmlFile();
        System.out.println("processRoadInfoMessage()");
        
        System.out.println("xmlString = " + xmlString);
        
        //get the factory
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        Document dom = null;
        
//        System.out.println("processRoadInfoMessage: xmlString = " + xmlString);
        
        try {
            //Using factory get an instance of document builder
            DocumentBuilder db = dbf.newDocumentBuilder();
        }catch(ParserConfigurationException pce) {
            pce.printStackTrace();
        }
        
        try {
            //parse using builder to get DOM representation of the XML file
            dom = loadXMLFromString(xmlString);
        } catch (Exception ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
        }
        
            //get each employee element and create a Employee object
//        parseDocument();
        
        //get the root elememt
        Element docEle = dom.getDocumentElement();
        
        //get a nodelist of <employee> elements
        NodeList nl = docEle.getElementsByTagName("node");
//        System.out.println("There are " + nl.getLength() + " nodes in the xml.");
        
        nodeList = new ArrayList();
                
        if(nl != null && nl.getLength() > 0) {
                    
            for(int i = 0 ; i < nl.getLength();i++) {
                
                Element el = (Element)nl.item(i);
                
                int nodeId = Integer.parseInt( el.getAttribute("id") );
                double xPos = Double.parseDouble( el.getAttribute("x") );
                double yPos = Double.parseDouble( el.getAttribute("y") );
                
                if ( el.hasAttribute("destination") ){
                    
                    System.err.println( el.getAttribute("destination") );
                    OsmNode tempOsmNode = new OsmNode(nodeId, xPos, yPos);
                    tempOsmNode.setDestination( el.getAttribute("destination") );
                    nodeList.add( tempOsmNode );
                    
                }else{
                    
                    nodeList.add( new OsmNode(nodeId, xPos, yPos) );
                    
                }
                
                
                
                
//                //get the Employee object
                
//                nodeList.add(node);
//                //add it to list
//
                        
                        
                        
            }
        }
        
        ArrayList<OsmNode> nodeArrayList = new ArrayList<>();
        for ( OsmNode osmNode : nodeList ){
            nodeArrayList.add(osmNode);
        }
        Globals.routePlanner.setOsmNodeList(nodeArrayList);
        
        //get a nodelist of <employee> elements
        nl = docEle.getElementsByTagName("way");
        
        wayList = new ArrayList();
        
        if(nl != null && nl.getLength() > 0) {
        
            for(int i = 0 ; i < nl.getLength();i++) {

                        //get the employee element
                        Element el = (Element)nl.item(i);
                   
                        int wayId = Integer.parseInt( el.getAttribute("id") );
                        
                        OsmWay newWay = new OsmWay( wayId );
                                                
                        NodeList nodeIds = el.getElementsByTagName("nd");
                        
                        if(nodeIds != null && nodeIds.getLength() > 0) {
        
                            for(int temp = 0 ; temp < nodeIds.getLength();temp++) {
                        
                                Element nodeEl = (Element)nodeIds.item(temp);
                                                                
                                int tempNodeId = Integer.parseInt( nodeEl.getAttribute("id") );
                                                               
                                newWay.addNodeId(tempNodeId);
                                
                            }
                            
                        }
                        
                        if ( el.hasAttribute("line_type") ){
                                    
                            newWay.setWayType( el.getAttribute("line_type") );
                            
                            for ( OsmNode tempNode : nodeList ){
                                
                                if ( newWay.nodeIds.contains( tempNode.id ) ){
                                    
                                    tempNode.ignore = true;
                                    
                                }
                                
                                
                            }
                            

                        }
                        
                        wayList.add(newWay);

                        
                        
                }
        }

        ArrayList<OsmWay> wayArrayList = new ArrayList<>();
        for ( OsmWay osmWay : wayList ){
            wayArrayList.add(osmWay);
        }
        Globals.routePlanner.setOsmWayList(wayArrayList);
        
        nl = docEle.getElementsByTagName("lanelet");
        
        laneletList = new ArrayList();
        
        if(nl != null && nl.getLength() > 0) {
        
            for(int i = 0 ; i < nl.getLength();i++) {

                        //get the employee element
                        Element el = (Element)nl.item(i);
                   
                        int laneletId = Integer.parseInt( el.getAttribute("id") );
                        int leftWayId = Integer.parseInt( el.getAttribute("left_way") );
                        int rightWayId = Integer.parseInt( el.getAttribute("right_way") );
                        
                        OsmLanelet newLanelet = new OsmLanelet(laneletId, leftWayId, rightWayId);
                                                                        
                        laneletList.add(newLanelet);
                        
                }
        }
        
        ArrayList<OsmLanelet> laneletArrayList = new ArrayList<>();
        for ( OsmLanelet osmLanelet : laneletList ){
            laneletArrayList.add(osmLanelet);
        }
        Globals.routePlanner.setOsmLaneletList(laneletArrayList);
        
        Globals.routePlanner.createAdjacencyMatrix();
       
        
    }
    
    public void processReverseTrajectorySolutionMessage(String xmlString){
                
        System.out.println("processReverseTrajectorySolutionMessage()");
                
        trajectoryPointList = new ArrayList<TrajectoryPoint>();
        
        //get the factory
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        Document dom = null;
        
//        System.out.println("processRoadInfoMessage: xmlString = " + xmlString);
        
        try {
            //Using factory get an instance of document builder
            DocumentBuilder db = dbf.newDocumentBuilder();
        }catch(ParserConfigurationException pce) {
            pce.printStackTrace();
        }
        
        try {
            //parse using builder to get DOM representation of the XML file
            dom = loadXMLFromString(xmlString);
        } catch (Exception ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
        }
        
            //get each employee element and create a Employee object
//        parseDocument();
        
        //get the root elememt
        Element docEle = dom.getDocumentElement();
        
        //get a nodelist of <employee> elements
        NodeList solutionElements = docEle.getElementsByTagName("solution");
//        System.out.println("There are " + nl.getLength() + " nodes in the xml.");
        
        String status = null;
                
        if(solutionElements != null && solutionElements.getLength() > 0) {
                    
            for(int i = 0 ; i < solutionElements.getLength();i++) {
                
                Element el = (Element)solutionElements.item(i);
                
                status = el.getAttribute("status");
                        
            }
            
        }
        
        NodeList pointElements = docEle.getElementsByTagName("point");
        
        ArrayList<double[]> trajectoryPoints = new ArrayList<>();
        
        int prevSeqCount = -1;
        
        if(pointElements != null && pointElements.getLength() > 0) {
                    
            for(int i = 0 ; i < pointElements.getLength();i++) {
                
                Element el = (Element)pointElements.item(i);
                
                double x = Double.parseDouble( el.getAttribute("x") );
                double y = Double.parseDouble( el.getAttribute("y") );
                
                int currentSeqNumber = Integer.parseInt(el.getAttribute("seq") );
                
                if ( currentSeqNumber < prevSeqCount){
                    
                    System.err.println("processReverseTrajectorySolutionMessage() : Trajectory Point with wrong sequence number ");
                    
                }
                
                TrajectoryPoint trajPoint = new TrajectoryPoint((double) currentSeqNumber, x, y);
                
                trajectoryPointList.add(trajPoint);
                
                prevSeqCount = currentSeqNumber;
                             
                
            }
            
        }
        
        if ( status != null ){
        
            if ( status.equals("success") ){

                Globals.drawingManager.logManager.addLogEntry("Reverse parking plan found");

            }else if( status.equals("timeout") ){
                
                Globals.drawingManager.logManager.addLogEntry("Reverse parking plan not found in allocated time", Color.RED);
                Globals.drawingManager.actionsManager.waitingForReverseTrajectory = false;
                
            }else if( status.equals("busy") ){
                
                Globals.drawingManager.logManager.addLogEntry("Truck Monitoring control is busy, try again once all trucks are stopped", Color.RED);
                
            }
        
        }
        
        Globals.drawingManager.drawReverseTrajectory();
        
    }
        
    public void processConstructionFinishedMessage(String xmlString){
                
        System.out.println("processConstructionFinishedMessage()");
                
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        Document dom = null;
        
        try {
            //Using factory get an instance of document builder
            DocumentBuilder db = dbf.newDocumentBuilder();
        }catch(ParserConfigurationException pce) {
            pce.printStackTrace();
        }
        
        try {
            //parse using builder to get DOM representation of the XML file
            dom = loadXMLFromString(xmlString);
        } catch (Exception ex) {
            Logger.getLogger(XmlProcesser.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //get the root elememt
        Element docEle = dom.getDocumentElement();
        
        NodeList completedBodyElements = docEle.getElementsByTagName("body");

        ArrayList<Integer> completedBodies = new ArrayList<>();
                
        if(completedBodyElements != null && completedBodyElements.getLength() > 0) {
                    
            for(int i = 0 ; i < completedBodyElements.getLength();i++) {
                
                Element el = (Element)completedBodyElements.item(i);
                
                completedBodies.add( Integer.parseInt( el.getAttribute("id") ) );
                        
            }
            
        }
        
        Globals.missionManager.processExcavatorCompletion(completedBodies);
        
        
    }
   
    
    public void saveRoadInfo(SMLWorldInformation smlWorldInfo){
               
        
        for ( int i = 0; i < nodeList.size(); i++ ){
            
            smlWorldInfo.nodeList.add(nodeList.get(i));
            
        }
        
        for ( int i = 0; i < wayList.size(); i++ ){
            
            smlWorldInfo.wayList.add(wayList.get(i));
            
        }
                
        
    }
    
    public void saveImageProperties(SMLWorldInformation smlWorldInfo){

        Globals.smlWorldInfo.centerPixelX = centerPixelX;
        Globals.smlWorldInfo.centerPixelY = centerPixelY;
        Globals.smlWorldInfo.pixelPerMeterX = pixelPerMeterX;
        Globals.smlWorldInfo.pixelPerMeterY = pixelPerMeterY;
        
    }
         
    public void saveTrajectory(SMLWorldInformation smlWorldInfo){
        
        smlWorldInfo.trajectoryPointList.clear();

        for ( int i = 0; i < trajectoryPointList.size(); i++ ){
            
            smlWorldInfo.trajectoryPointList.add(trajectoryPointList.get(i));
            
        }
                       
    }
    
    public void saveBodiesList(SMLWorldInformation smlWorldInfo){

        smlWorldInfo.qualisysBodiesList.clear();        
        
        for ( int i = 0; i < qualisysBodiesList.size(); i++ ){
            
            smlWorldInfo.qualisysBodiesList.add(qualisysBodiesList.get(i));
            
        }
                       
    }
    
    public List<OsmNode> getNodeList(){
        
        return nodeList;
        
    }
    public List<OsmWay> getWayList(){
        
        return wayList;
        
    }
    public List<TrajectoryPoint> getTrajectoryPointList(){
        
        return trajectoryPointList;
        
    }
    
    public void processIncomingTrajectory(String xmlString){
        
        
    }
    
    /**
     * I take a xml element and the tag name, look for the tag and get
     * the text content 
     * i.e for <employee><name>John</name></employee> xml snippet if
     * the Element points to employee node and tagName is name I will return John  
     * @param ele
     * @param tagName
     * @return
     */
    private String getTextValue(Element ele, String tagName) {
//        System.out.println("getTextValueA");
        String textVal = null;
//        System.out.println("getTextValueB");
        NodeList nl = ele.getElementsByTagName(tagName);
//        System.out.println("getTextValueC");
        if(nl != null && nl.getLength() > 0) {
//            System.out.println("getTextValueD");
            Element el = (Element)nl.item(0);
            textVal = el.getFirstChild().getNodeValue();
        }
//        System.out.println("getTextValueE");
        return textVal;
    }


    /**
     * Calls getTextValue and returns a int value
     * @param ele
     * @param tagName
     * @return
     */
    private int getIntValue(Element ele, String tagName) {
            //in production application you would catch the exception
        return Integer.parseInt(getTextValue(ele,tagName));
    }
    
    private double getDoubleValue(Element ele, String tagName) {
            //in production application you would catch the exception
//        System.out.println("getDoubleValue");
        return Double.parseDouble(getTextValue(ele,tagName));
    }
    
    
}
