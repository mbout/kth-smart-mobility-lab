/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testingcommunications;

/**
 * 
 * Container for OsmNodes.
 * Note: OsmNode is expected to be in Real World dimensions: x and y in the
 * range of hundreds of meters 
 * 
 * @author rui
 */
public class OsmNode {

    int id;
    
    /**
    * The X position of the OsmNode.
    * (Note: Expected to be in Real World dimensions: somewhere in the range of
    * hundreds of meters)
    */
    double X;
    
    /**
    * The Y position of the OsmNode.
    * (Note: Expected to be in Real World dimensions: somewhere in the range of
    * hundreds of meters)
    */
    double Y;
    String destination = null;
    boolean ignore = false;
            
    /**
     * 
     * Creates an OsmNode with the given parameters.
     * 
     * Note: OsmNode is expected to be in Real World dimensions:
     * x and y in the range of hundreds of meters 
     * @param argId
     * @param argX
     * @param argY
     */
    public OsmNode(int argId, double argX, double argY){
        
        id = argId;
        X = argX;
        Y = argY;
                
    }
    
    /**
     *
     * @param argDestination
     */
    public void setDestination(String argDestination){
        
//        System.out.println("argDestination = " + argDestination);
        destination = argDestination;
        
    }
    
    /**
     *
     * @return
     */
    public boolean isDestination(){
        
        return (destination != null);
        
    }
    
            
    @Override public String toString() {
    
        String toString = "Node with id=" + id + " X=" + X + " Y=" + Y;

        return toString;
    }
    
}
