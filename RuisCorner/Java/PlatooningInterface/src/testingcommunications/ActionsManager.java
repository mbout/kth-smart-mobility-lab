/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testingcommunications;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.InputEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.SwipeEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.util.Duration;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

/**
 *
 * @author rui
 */
public class ActionsManager {
    
    double actionsManagerWidth;
    double actionsManagerHeight;
    double tabHeight;
    
    // SCREENCHANGES
    double standardFontReduction = Globals.screenWidth/1920.0;
    
    Group actionsManagerGroup;
    Group trucksMenuGroup;
    
    double menuLayoutX = 0;
    double menuLayoutY = 0;
    
    double standardButtonHeight = 100;
    
    long afterTrajectoryMessageStop = 1000;
    
    BorderPane borderPane;
    
    List<TruckInformation> truckInformationList;
    
    List<Boolean> truckGoABoolean;
    List<Boolean> truckGoBBoolean;
        
    int lastPressedTruck = -1000;
    String lastPressedDestination = null;
    
    List<Integer> completedBodies;
    
    ArrayList<double[]> latestTrajectoryPointsList;
    
    double closeButtonWidth = 50;
        
//    ArrayList<>
    
    MissionGuiManager missionGuiManager;
    
    boolean waitingForReverseTrajectory = false;
    
    double leftButtonPostTrajectoryOffsetX = -800;
    double rightButtonPostTrajectoryOffsetX = 0;
    
    double[] reverseGoalPose = null;
    
    Image truckImageXRayEmpty, truckImageXRayFull;
    
    public ActionsManager(double argWidth, double argHeight){
               
//        truckImageXRayEmpty = new Image("file:truckXray000.png", 100, 0, true, false);
//        truckImageXRayFull = new Image("file:truckXray075.png", 100, 0, true, false);
        truckImageXRayEmpty = new Image("file:truckXray000.png");
        truckImageXRayFull = new Image("file:truckXray075.png");
                
        completedBodies = new ArrayList<>();
        
        truckGoABoolean = new ArrayList<>();
        
        truckGoABoolean.add(false);
        truckGoABoolean.add(false);
        truckGoABoolean.add(false);
        
        truckGoBBoolean = new ArrayList<>();
        
        truckGoBBoolean.add(false);
        truckGoBBoolean.add(false);
        truckGoBBoolean.add(false);
        
        actionsManagerWidth = argWidth;
        actionsManagerHeight = argHeight;
        
        actionsManagerGroup = new Group();
        
        TabPane tabPane = new TabPane();
        
//        tabPane.setMinWidth(500);

        borderPane = new BorderPane();
//        for (int i = 0; i < 5; i++) {
//            Tab tab = new Tab();
//            tab.setText("Tab" + i);
//            HBox hbox = new HBox();
//            hbox.getChildren().add(new Label("Tab" + i));
//            hbox.setAlignment(Pos.CENTER);
//            tab.setContent(hbox);
//            tabPane.getTabs().add(tab);
//        }
        
        Tab tab = new Tab();
        tab.setStyle("-fx-font-size: 15px;-fx-alignment: CENTER;");
        tab.setText("Missions");
        tab.setClosable(false);
//        http://stackoverflow.com/questions/16922138/how-to-set-tab-name-size-in-javafx
        
//        ;
//        HBox hbox = new HBox();
//        hbox.getChildren().add(new Label("Missions"));
//        hbox.setAlignment(Pos.CENTER);
//        tab.setContent(hbox);
//        
        
        tabPane.getTabs().add(tab);
//        tabHeight = tab.getTabPane().getBoundsInLocal().getHeight();
        
        missionGuiManager = new MissionGuiManager(argWidth, argHeight);
                
        tab.setContent( missionGuiManager.getMissionGuiGroup() );
        
//        System.out.println("tab.getProperties().toString() = " + tab.getProperties().toString());
        
        tabHeight = missionGuiManager.getMissionGuiGroup().getBoundsInParent().getMinY();
        
        for ( int i = 0; i < 4; i++ ){
        
            Group groupToAdd = new Group();
            
            Rectangle rectBlue = new Rectangle(actionsManagerWidth, 200);
            rectBlue.setFill(Color.ALICEBLUE);
            
            Rectangle rectRed = new Rectangle(actionsManagerWidth, 200);
            rectRed.setFill(Color.RED);
            rectRed.setLayoutY(200);
            
            groupToAdd.getChildren().add(rectBlue);
            groupToAdd.getChildren().add(rectRed);
            
//            missionGuiManager.addGroupToMissionQueue(groupToAdd);            
        }
        
        
        
        
        tab = new Tab();
        tab.setStyle("-fx-font-size: 15px;-fx-alignment: CENTER;");
        tab.setText("Drivers");
        tab.setClosable(false);
//        tabHeight = tab.getTabPane().getBoundsInLocal().getHeight();
//        tab.
//        hbox = new HBox();
//        hbox.getChildren().add(new Label("Drivers"));
//        hbox.setAlignment(Pos.CENTER);
        createTruckInformationList();
//        tab.setContent( getTrucksMenu() );
        tabPane.getTabs().add(tab);
        
        SingleSelectionModel<Tab> selectionModel = tabPane.getSelectionModel();
        selectionModel.select(1); //select by index starting with 0
        
        // bind to take available space
        borderPane.setPrefWidth(actionsManagerWidth); 
        borderPane.setPrefHeight(actionsManagerHeight);
//        borderPane.prefHeightProperty().bind(scene.heightProperty());
//        borderPane.prefWidthProperty().bind(scene.widthProperty());
        
        borderPane.setCenter(tabPane);
        
//        actionsManagerGroup.getChildren().add(borderPane);
        
        Group trucksMenuTempGroup = getTrucksMenu();
        
        trucksMenuTempGroup.setLayoutX(Globals.screenWidth*Globals.mapXRatio);
        
        actionsManagerGroup.getChildren().add(trucksMenuTempGroup);
        
        
        
        
        
    }
    
    Group getDriversGroup(){
        
        Group driversGroup = new Group();
        
        Rectangle rect = new Rectangle(actionsManagerWidth, actionsManagerHeight);    
        rect.setFill(Color.BLUE);
        
        driversGroup.getChildren().add(rect);
        
        return driversGroup;
        
    }
    
    public void setMenuLayout(double argMenuLayoutX, double argMenuLayoutY){
        
        menuLayoutX = argMenuLayoutX;
        menuLayoutY = argMenuLayoutY;       
        
        borderPane.setLayoutX(menuLayoutX);
        borderPane.setLayoutY(menuLayoutY);
        
    }
   
        
    public boolean isIdControllable(int argId){
        
        boolean controllable = false;
        List<QualisysBody> currentQualisysBodiesList = Globals.smlWorldInfo.getQualisysBodiesList();
        
        for (QualisysBody body : currentQualisysBodiesList){
                   
                if ( body.isControllable() && body.getId() == argId ){

                    controllable = true;
                    return controllable;
                }

            }
                
        return controllable;        
        
    }
      
         
   
    public void initializeReverseGoalPose(){
        
        reverseGoalPose = new double[4];
        
        OsmWay parkingOsmWay = null;
        
        for ( OsmWay tempOsmWay : Globals.xmlProcesser.wayList ){
            
            if ( tempOsmWay.wayType.equals("parking_lot") ){
                
                parkingOsmWay = tempOsmWay;
                
            }
            
        }
        
        if ( parkingOsmWay == null ){
        
            System.err.println("initializeReverseGoalPose() : did not find the parking_spot way");
            
            reverseGoalPose[0] = 0.0;
            reverseGoalPose[1] = 0.0;
        
        }else{
            
            ArrayList<OsmNode> parkingNodes = new ArrayList<>();
            
            for ( OsmNode tempOsmNode : Globals.xmlProcesser.nodeList ){
                
                if ( parkingOsmWay.nodeIds.contains(tempOsmNode.id) ){
                    
                    parkingNodes.add(tempOsmNode);
                    
                }
                
            }
            
            for ( OsmNode tempParkingNode : parkingNodes ){
                
                reverseGoalPose[0] = reverseGoalPose[0] + (tempParkingNode.X/32.)/((double)parkingNodes.size());
                reverseGoalPose[1] = reverseGoalPose[1] + (tempParkingNode.Y/32.)/((double)parkingNodes.size());
                
            }
                        
        }
        
        
        reverseGoalPose[2] = 0.0;
        reverseGoalPose[3] = 0.0;
        
        System.out.println("reverseGoalPose[0] = " + reverseGoalPose[0]);
        System.out.println("reverseGoalPose[1] = " + reverseGoalPose[1]);
        System.out.println("reverseGoalPose[2] = " + reverseGoalPose[2]);
        System.out.println("reverseGoalPose[3] = " + reverseGoalPose[3]);
        
    }
    
    public int getCorrespondingTrailerId(int currentTruckId){
        
        int trailerId;
        
        if ( currentTruckId == Globals.body1QualisysId ){
                                    
            trailerId = Globals.trailer1QualisysId;

        }else{

            if ( currentTruckId == Globals.body2QualisysId ){

                trailerId = Globals.trailer2QualisysId;

            }else{

                if ( currentTruckId == Globals.body3QualisysId ){

                    trailerId = Globals.trailer3QualisysId;

                }else{

                    System.err.println("showTruckOrdersMenuFreeAreaEntrance() : Did not find corresponding trailer ID!");
                    trailerId = 0;

                }

            }

        }
        
        return trailerId;
        
    }
   
           
    public void processStateClick(int stateId, boolean highlight){
                
        String messageToSend = Globals.xmlProcesser.getHighlightRequestMessageString(stateId, highlight);
        
//        messageToSend = messageToSend + messageToSend + messageToSend + messageToSend + messageToSend;
        
        Globals.smlCommunications.sendOutputMessage(messageToSend);      
        
        System.out.println("Sent message: " + messageToSend);
                        
    }
        
    public void popUpBodyCompletion() throws TransformerException, ParserConfigurationException{
                
        
        
        int durationAnimationMillis = 3000;
        
        while( completedBodies.size() > 0 ){
            
            System.out.println("completedBodies = " + completedBodies.toString());
            
            System.out.println("POP UP!!!");
            
            int bodyCompleted = completedBodies.get(0);
            Globals.missionManager.setTruckArrival(bodyCompleted);
            
            if ( Globals.missionManager.isTruckOnLoadEntrance(bodyCompleted) ){
                
//                promptRrtCommand(bodyCompleted);
                
            }
            if ( Globals.missionManager.isTruckOnLoadParking(bodyCompleted) ){
                
//                promptRrtFromOpenAreaCommand(bodyCompleted);
                
            }
            if ( Globals.missionManager.isTruckOnLoadExit(bodyCompleted) ){
                
//                promptExitOpenAreaCommand(bodyCompleted);
                
            }
                    
            Globals.drawingManager.addLogEntry("Truck " + bodyCompleted + " finished its task.", Color.GREEN);
            
            completedBodies.remove(0);
            
            Text completedText = new Text("Body " + bodyCompleted + " has finished its task!");
            completedText.setFont( new Font(50) );
            completedText.setFill( Color.PAPAYAWHIP );
            
            completedText.setLayoutX(200);
            completedText.setLayoutY(200);
            
            actionsManagerGroup.getChildren().add(completedText);
            completedText.toFront();
            
            
            FadeTransition ft = new FadeTransition(Duration.millis(durationAnimationMillis), completedText);
            ft.setFromValue(1.0);
            ft.setToValue(0.0);
            ft.setCycleCount(1);
            ft.setAutoReverse(false);
            
            ScaleTransition st = new ScaleTransition(Duration.millis(durationAnimationMillis), completedText);
            st.setFromX(1.0);
            st.setFromY(1.0);
            st.setToX(.1);
            st.setToY(.1);
            st.setCycleCount(1);
            st.setAutoReverse(false);
            
            ParallelTransition pt = new ParallelTransition();
            pt.getChildren().addAll(ft, st);
            
            pt.play();
            
            pt.setOnFinished(new EventHandler<ActionEvent>(){
 
                @Override
                public void handle(ActionEvent arg0) {
                    
                    System.out.println("Cleaning POP UP");
                    actionsManagerGroup.getChildren().remove(completedText);
                    
                }

            });
            
            
        }
        
    }
    
    public void createTruckInformationList(){
        
        int[] truckIds = new int[5];
        
        truckInformationList = new ArrayList<>();
        
        TruckInformation truckInfoRui = new TruckInformation("Rui Oliveira", Globals.body1QualisysId, "scaniaLogoCropped.png", "matteoTruck.png", "ruiAndTruck.png", "+46720752943");
        truckInfoRui.setTruckFuel(180);
        truckInfoRui.setTruckModel("Scania R260");
        truckInformationList.add(truckInfoRui);
        truckIds[0] = Globals.body1QualisysId;
        
        TruckInformation truckInfoAlvito = new TruckInformation("Pedro Alvito", Globals.body2QualisysId, "scaniaLogoCropped.png", "alvitoTruck.png", "alvitoAndTruck.png", "+46720842651");
        truckInfoAlvito.setTruckFuel(220);
        truckInfoAlvito.setTruckModel("Scania R260");
        truckInformationList.add(truckInfoAlvito);
        truckIds[1] = Globals.body2QualisysId;
                
        TruckInformation truckInfoMatteo = new TruckInformation("Matteo Vanin", Globals.body3QualisysId, "scaniaLogoCropped.png", "matteoTruck.png", "matteoAndTruck.png", "+46720574526");
        truckInfoMatteo.setTruckFuel(240);
        truckInfoMatteo.setTruckModel("Scania R260");
        truckInformationList.add(truckInfoMatteo);
        truckIds[2] = Globals.body3QualisysId;
        
//        TruckInformation truckInfoLima = new TruckInformation("Pedro Lima", 98, "lima.png", "limaTruck.png", "limaAndTruck.png", "+46720775142");
        TruckInformation truckInfoLima = new TruckInformation("Pedro Lima", 98, "excavatorFlat.png", "excavatorFlat.png", "excavatorFlat.png", "+46720775142");
        truckInfoLima.setTruckFuel(120);
        truckInfoLima.setTruckModel("excavator");
        truckInformationList.add(truckInfoLima);
        truckIds[3] = 98;
        
//        TruckInformation truckInfoJonas = new TruckInformation("Jonas Mårtensson", 99, "jonas.png", "jonasTruck.png", "jonasAndTruck.png", "+46720985635");
        TruckInformation truckInfoJonas = new TruckInformation("Jonas Mårtensson", 99, "excavatorFlat.png", "excavatorFlat.png", "excavatorFlat.png", "+46720985635");
        truckInfoJonas.setTruckFuel(60);
        truckInfoJonas.setTruckModel("excavator");
        truckInformationList.add(truckInfoJonas);
        truckIds[4] = 99;
        
        Globals.missionManager = new MissionManager(truckIds);
        
        
    }
       
    public Group getTrucksMenu(){
        
        trucksMenuGroup = new Group();
        
        createTruckInformationList();
        
        Rectangle menuBackground = new Rectangle();
//        menuBackground.setX(windowWidth*Globals.mapXRatio);
//        menuBackground.setY(0);
        menuBackground.setWidth( actionsManagerWidth );
        menuBackground.setHeight(actionsManagerHeight );
        menuBackground.setFill(Color.DARKSLATEGREY);

        trucksMenuGroup.getChildren().add(menuBackground);
        
        Text text = new Text();
//        text.setFont(new Font(30));
        text.setFont( Font.font("Verdana", FontWeight.BOLD, 30*standardFontReduction) );
//        text.setWrappingWidth( windowWidth*(1.0-Globals.mapXRatio) );
//        text.setTextAlignment(TextAlignment.JUSTIFY);
        text.setText("VEHICLES");
        
        trucksMenuGroup.getChildren().add(text);
        
        System.out.println("text.getBoundsInLocal().getWidth()/2.0 = " + text.getBoundsInLocal().getWidth()/2.0);
        
//        text.setBoundsType(TextBoundsType.VISUAL);
        
        text.setLayoutX( actionsManagerWidth*0.5 - 0.5*text.getBoundsInParent().getWidth() );
        text.setLayoutY( text.getBoundsInParent().getHeight()*(3.0/2.0) );
        
        text.setFill(Color.ALICEBLUE);
        text.setId("availableText");
        
        double elementWidth = actionsManagerWidth;
        double elementHeight = (actionsManagerHeight - 2*text.getBoundsInLocal().getHeight() )/truckInformationList.size();
        int truckCounter = 1;
        
        for (TruckInformation truckInformation : truckInformationList){
        
            // Place group HERE
//            Group currentGroup = getTruckMenuElement(truckInformation, 0.9*elementWidth, 0.9*elementHeight);
//            currentGroup.setLayoutX( actionsManagerWidth*0.5 - currentGroup.getBoundsInLocal().getWidth()/2.0 );
//            currentGroup.setLayoutY( ((double)(truckCounter-1))*elementHeight + 2*text.getBoundsInLocal().getHeight() );
//            trucksMenuGroup.getChildren().add(currentGroup);
//            truckCounter++;           
            
        }
        
        System.out.println("updateTrucksMenu(truckMenuRoot)");
        updateTrucksMenu();
        
        return trucksMenuGroup;
        
    }
           
    public boolean getBodyStateFromId(int argBodyId, double[] state){
        
        List<QualisysBody> currentQualisysBodiesList = Globals.smlWorldInfo.getQualisysBodiesList();
        int desiredBodyId = argBodyId;
        QualisysBody desiredBody = null;
        for ( QualisysBody tempBody : currentQualisysBodiesList ){
            if ( tempBody.getId() == desiredBodyId ){
                desiredBody = tempBody;
            }
        }
        
        boolean success = true;
        
        if ( desiredBody == null ){
            
            success = false;
            
        }else{
        
            if (state.length != 4){
                
                System.out.println("State length must be 4!");
                success = false;
                
            }else{
                
                state[0] = desiredBody.getX();
                state[1] = desiredBody.getY();
                double thetaInRadians = desiredBody.getTheta();
                thetaInRadians = thetaInRadians*(Math.PI/180.0);

                state[2] = thetaInRadians;
                state[3] = thetaInRadians;
            
            }
        
        }
        
        return success;
        
    }
    
    
    public void removeHighlight(){
        
        boolean highlight = false;
        
        String messageToSend = Globals.xmlProcesser.getHighlightRequestMessageString(-1, highlight);
        
        Globals.smlCommunications.sendOutputMessage(messageToSend);      
        
        System.out.println("Sent message: " + messageToSend);
        
    }
    
    
    public void createTrajectoryButton(ArrayList<OsmLanelet> laneletPath){
        
        Group performTrajectoryButtonGroup = new Group();
        
        performTrajectoryButtonGroup.setId("trajectoryButton");
        
        double buttonWidth = 200;
        double buttonHeight = standardButtonHeight;
        double buttonOffsetX = 150;
        
        Rectangle buttonRectangle = new Rectangle();
        buttonRectangle.setWidth(buttonWidth);
        buttonRectangle.setHeight(buttonHeight);
            
        buttonRectangle.setLayoutX( Globals.drawingManager.worldViewManager.worldViewWidth/2.0 - buttonOffsetX - buttonWidth/2.0);
//        buttonRectangle.setLayoutX( Globals.drawingManager.worldViewManager.worldViewWidth/2.0 + rightButtonPostTrajectoryOffsetX);
        
        
//        buttonRectangle.setLayoutX( Globals.drawingManager.windowWidth/2.0 - buttonOffsetX - buttonWidth/2.0);
        
        buttonRectangle.setLayoutY( Globals.drawingManager.worldViewManager.worldViewHeight - 1.5*buttonHeight );
//        buttonRectangle.setLayoutY( Globals.drawingManager.windowHeight - 1.5*buttonHeight );
        
        buttonRectangle.setFill(Color.SLATEGRAY);
        buttonRectangle.setStroke(Color.DARKSLATEGRAY);
        buttonRectangle.setStrokeWidth(5);
        buttonRectangle.setArcWidth(10);
        buttonRectangle.setArcHeight(10); 

        performTrajectoryButtonGroup.getChildren().add( buttonRectangle );

        Text buttonText = new Text("CONFIRM");
        buttonText.setFont( Font.font("Verdana", FontWeight.BOLD, 30*standardFontReduction) );

        buttonText.setLayoutX( buttonRectangle.getBoundsInParent().getMinX() + buttonRectangle.getBoundsInParent().getWidth()/2.0 - buttonText.getBoundsInParent().getWidth()/2.0 );
        buttonText.setLayoutY( buttonRectangle.getBoundsInParent().getMinY() + buttonRectangle.getBoundsInParent().getHeight()/2.0 + buttonText.getBoundsInParent().getHeight()/2.0 );
        
        performTrajectoryButtonGroup.getChildren().add( buttonText );
        
        performTrajectoryButtonGroup.toFront();
        
        EventHandler performTrajectoryClickEvent = new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent arg0) {

                int bodyId = lastPressedTruck;
                
                int bodyNum = 0;
                if ( bodyId == Globals.body1QualisysId ){
                    bodyNum = 0;
                }
                if ( bodyId == Globals.body2QualisysId ){
                    bodyNum = 1;                       
                }
                if ( bodyId == Globals.body3QualisysId ){
                    bodyNum = 2;                          
                }              
                
                truckGoBBoolean.remove(bodyNum);
                truckGoBBoolean.add(bodyNum, Boolean.TRUE);
                
                System.out.println("Confirm button press: lastPressedDestination = " + lastPressedDestination);
                
                if ( lastPressedDestination.equals("Park") ){
                    
                    Globals.missionManager.sendTruckToPark( bodyId );
                    Globals.drawingManager.addLogEntry("Sent truck " + bodyId + " to park.");
                    
                }else if ( lastPressedDestination.equals("Unload") ){
                    
                    Globals.missionManager.sendTruckToUnload( bodyId );
                    Globals.drawingManager.addLogEntry("Sent truck " + bodyId + " to unload.");
                    
                }else if ( lastPressedDestination.equals("Load Entrance") ){
                    
                    Globals.missionManager.sendTruckToLoadEntrance(bodyId );
                    Globals.drawingManager.addLogEntry("Sent truck " + bodyId + " to loading area entrance.");
                    
                }else{
                    
                    if ( lastPressedDestination.equals("PARKING") ){
                    
                        String messageToSend = Globals.xmlProcesser.getReverseTrajectoryCommandMessageString();
                        
                        Globals.smlCommunications.sendOutputMessage(messageToSend);
                        System.out.println("sent reverse_trajectory_command: messageToSend = " + messageToSend);
                        
                       
                        
                        Globals.missionManager.sendTruckToLoadParking(bodyId );
                        Globals.drawingManager.addLogEntry("Sent truck " + bodyId + " to perform reverse parking.");

                        boolean highlight = false;
                        messageToSend = Globals.xmlProcesser.getHighlightRequestMessageString(-1, highlight);

                        if (messageToSend != null){

        //                    System.out.println("SHOULD HAVE SENT AN HIGHLIGH -1 MESSAGE, NOT SENDING IT BECAUSE IT MESSES UP THE TRAJECTORY REROUTING TO MATLAB");
                            Globals.smlCommunications.sendOutputMessage(messageToSend);
                            System.out.println("Sent a highlight removal: " + messageToSend);

                        }  

                        Globals.drawingManager.clearTrajectory();
                        
                        actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#trajectoryButton") );
                        actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#abortTrajectoryButton") );

                        waitingForReverseTrajectory = false;
                        
                        return;
                    
                    
                    }
                    
                }
                    
                
                
                Globals.missionManager.setTruckLaneletPath(bodyId, laneletPath);
                                
                sendTrajectoryCommand(bodyId);
                                
                boolean highlight = false;
                
                String messageToSend = Globals.xmlProcesser.getHighlightRequestMessageString(-1, highlight);
                
                if (messageToSend != null){
                    
//                    System.out.println("SHOULD HAVE SENT AN HIGHLIGH -1 MESSAGE, NOT SENDING IT BECAUSE IT MESSES UP THE TRAJECTORY REROUTING TO MATLAB");
                    Globals.smlCommunications.sendOutputMessage(messageToSend);
                    
                }  
                
                actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#trajectoryButton") );
                actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#abortTrajectoryButton") );

                Globals.drawingManager.clearTrajectory();
                
            }
        };
        
        performTrajectoryButtonGroup.setOnMouseClicked(performTrajectoryClickEvent);
        
        actionsManagerGroup.getChildren().add(performTrajectoryButtonGroup);
        
        Group abortTrajectoryButtonGroup = new Group();
        
        abortTrajectoryButtonGroup.setId("abortTrajectoryButton");
        
//        double buttonWidth = 200;
//        double buttonHeight = 100;
        
        Rectangle abortButtonRectangle = new Rectangle();
        abortButtonRectangle.setWidth(buttonWidth);
        abortButtonRectangle.setHeight(buttonHeight);
          
        abortButtonRectangle.setLayoutX(Globals.drawingManager.worldViewManager.worldViewWidth/2.0 + buttonOffsetX - buttonWidth/2.0);
//        abortButtonRectangle.setLayoutX(Globals.drawingManager.windowWidth/2.0 + buttonOffsetX - buttonWidth/2.0);
//        abortButtonRectangle.setLayoutX(Globals.drawingManager.worldViewManager.worldViewWidth/2.0 + rightButtonPostTrajectoryOffsetX);
        
        abortButtonRectangle.setLayoutY( Globals.drawingManager.worldViewManager.worldViewHeight - 1.5*buttonHeight );
//        abortButtonRectangle.setLayoutY( Globals.drawingManager.windowHeight - 1.5*buttonHeight );
        
        abortButtonRectangle.setFill(Color.SLATEGRAY);
        abortButtonRectangle.setStroke(Color.DARKSLATEGRAY);
        abortButtonRectangle.setStrokeWidth(5);
        abortButtonRectangle.setArcWidth(10);
        abortButtonRectangle.setArcHeight(10); 

        abortTrajectoryButtonGroup.getChildren().add( abortButtonRectangle );

        Text abortButtonText = new Text("CANCEL");
        abortButtonText.setFont( Font.font("Verdana", FontWeight.BOLD, 30*standardFontReduction) );

        abortButtonText.setLayoutX( abortButtonRectangle.getBoundsInParent().getMinX() + abortButtonRectangle.getBoundsInParent().getWidth()/2.0 - abortButtonText.getBoundsInParent().getWidth()/2.0 );
        abortButtonText.setLayoutY( abortButtonRectangle.getBoundsInParent().getMinY() + abortButtonRectangle.getBoundsInParent().getHeight()/2.0 + abortButtonText.getBoundsInParent().getHeight()/2.0 );
        
        abortTrajectoryButtonGroup.getChildren().add( abortButtonText );
        
        EventHandler abortTrajectoryClickEvent = new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent arg0) {

                
                boolean highlight = false;
                String messageToSend = null;
                
                messageToSend = Globals.xmlProcesser.getHighlightRequestMessageString(-1, highlight);
                
                if (messageToSend != null){
                    
                    Globals.smlCommunications.sendOutputMessage(messageToSend);     
                    
                }                
                                        
//                        isTruckAvailable( truckInformation.getTruckId() 
                
                Globals.drawingManager.clearTrajectory();
                
                actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#trajectoryButton") );
                actionsManagerGroup.getChildren().remove( actionsManagerGroup.lookup("#abortTrajectoryButton") );
//                actionsManagerGroup.getChildren().clear();
                
                waitingForReverseTrajectory = false;

            }
        };
        
        abortTrajectoryButtonGroup.setOnMouseClicked(abortTrajectoryClickEvent);
        
        actionsManagerGroup.getChildren().add(abortTrajectoryButtonGroup);
        
    }
    
    public void sendTrajectoryCommand(int bodyId){
        
        System.out.println("sendTrajectoryCommand()");

//        List<TrajectoryPoint> trajectoryPointList = Globals.xmlProcesser.getTrajectoryPointList(); 
//        List<TrajectoryPoint> trajectoryPointList = Globals.xmlProcesser.getTrajectoryPointList(); 
        
        List<TrajectoryPoint> trajectoryPointList = new ArrayList<>();
        
        int cnt = 0;
        
        for ( double[] tempTrajectoryPoint : latestTrajectoryPointsList ){
            
            double argTime = (double) cnt;
            double argX = tempTrajectoryPoint[0];
            double argY = tempTrajectoryPoint[1];
            
            TrajectoryPoint trajectoryPoint = new TrajectoryPoint(argTime, argX, argY);
            
            trajectoryPointList.add(trajectoryPoint);
            
            cnt ++;
            
//            trajectoryPointList.add( new TrajectoryPoint(tabHeight, tabHeight, tabHeight))
            
        }
        
//        trajectoryPointList.add(new TrajectoryPoint(tabHeight, tabHeight, tabHeight));
                
        String messageToSend = Globals.xmlProcesser.getTrajectoryCommandMessageString(bodyId, trajectoryPointList);
        
        Globals.smlCommunications.sendOutputMessage(messageToSend);      
        
//        Thread.sleep(afterTrajectoryMessageStop);
        
        System.out.println("Sent message: " + messageToSend);

    }
        
    public OsmNode getNodeById(int nodeId, List<OsmNode> nodeList){
        
        OsmNode tempNode;
        
        for (int i = 0; i < nodeList.size(); i++){
            
            tempNode = nodeList.get(i);
            
            if (tempNode.id == nodeId ){
                
                return tempNode;
                
            }
            
        }
        
        return null;
        
    }
    
    public Group getWayPolyline(OsmWay way, List<OsmNode> nodeList){
        
        Polyline polyline = new Polyline();
        
        for ( int i = 0; i < way.nodeIds.size(); i++){
            
            int nodeId = way.nodeIds.get(i);
            
            OsmNode node = getNodeById(nodeId, nodeList);
            
            polyline.getPoints().add(node.X);
            polyline.getPoints().add(node.Y);
            
        }
        
        Group wayGroup = new Group(polyline);
        
        return wayGroup;
        
    }
    
    public Group getWayCircles(OsmWay way, List<OsmNode> nodeList){
        
        Group wayGroup = new Group();
                
        for ( int i = 0; i < way.nodeIds.size(); i++){
            
            int nodeId = way.nodeIds.get(i);
            
            OsmNode node = getNodeById(nodeId, nodeList);
            
            Circle tempCircle = new Circle(node.X, node.Y, 1.0);
            wayGroup.getChildren().add(tempCircle);
           
        }
                
        return wayGroup;
        
    }
    
    public Group getLineBetweenNodes(OsmNode startNode, OsmNode endNode){
        
        
        double x1 = 0, x2 = 0, y1 = 0, y2 = 0;

        x1 = startNode.X;
        y1 = startNode.Y;
        
        x2 = endNode.X;
        y2 = endNode.Y;
        
        System.out.println( "(" + x1 + "," + y1 + ")-(" + x2 + "," + y2 + ")" );
        
        Line line = new Line();
        line.setStartX(x1);
        line.setStartY(y1);
        line.setEndX(x2);
        line.setEndY(y2);
                
        Group objectToDraw = new Group(line);
        
        return objectToDraw;
        
    }
    
    public Group getRrtWayPolyline(){
        
        
////        getLaneletPolygon(int laneletId){
//        
//        Group laneletGroup = new Group();
//        
        OsmWay way = null;
        
        for( OsmWay tempWay : Globals.xmlProcesser.wayList ){
            
            if ( tempWay.wayType.equals( "free_space" ) ){
                
                way = tempWay;
                break;
                
            }
            
        }
//        
        if ( way == null ){
            
            System.out.println("ERROR: WAY FREE_SPACE NOT FOUND");
            
        }
        
        return Globals.drawingManager.getWayPolygon(way.id);
        
    }
   
    public void updateTrucksMenu(){
                                        
        List<QualisysBody> currentQualisysBodiesList = Globals.smlWorldInfo.getQualisysBodiesList();
        List<QualisysBody> availableTruckBodiesList = new ArrayList<>();
       
        for ( int i = 0 ; i < trucksMenuGroup.getChildren().size() ; i++ ){
            
            Node currentNode = trucksMenuGroup.getChildren().get(i);
           
            if ( currentNode.getId() == null ){
                continue;
            }
            
            if ( !currentNode.getId().contains("truckId") ){
                continue;
            }
            
            Group currentGroup = (Group) currentNode;
            
            Rectangle statusRectangle = (Rectangle) currentGroup.lookup("#statusRectangle");
            Text statusText = (Text) currentGroup.lookup("#statusText");
            Text cargoText = (Text) currentGroup.lookup("#cargoText");
                        
            if ( statusRectangle != null ){
                
                for (QualisysBody body : currentQualisysBodiesList){
                    
                    if ( currentGroup.getId().equals("truckId"+98) || currentGroup.getId().equals("truckId"+99) ) {
                        
                        statusRectangle.setFill(Color.GREEN);
                        statusText.setFill(Color.GREEN);
                        statusText.setText("AVAILABLE");
                        
                        break;
                        
                    }                  
                   
                    if ( currentGroup.getId().equals("truckId"+body.getId()) ){
                        
//                        System.out.println("Setting cargo to false");
//                        Globals.missionManager.setTruckCargo( body.getId(), false);
//                        System.out.println("Setting cargo to true");
//                        Globals.missionManager.setTruckCargo( body.getId(), true);
                        
                        boolean isTruckAvailable = Globals.missionManager.isTruckAvailable(body.getId());
                        
                        
//                        System.out.println("body.isControllable() = " + body.isControllable() );
//                        System.out.println("isTruckAvailable = " + isTruckAvailable );
                        boolean isTruckCargoed = false;  
                        
                        
                        if ( body.isControllable() ){
                                                                    
                            isTruckCargoed = Globals.missionManager.hasTruckCargo(body.getId());
                            
                            if ( isTruckAvailable ){

                                if ( body.getX() < -9000.0 ){
                                    
                                    statusRectangle.setFill(Color.YELLOW);
                                    statusText.setFill(Color.YELLOW);
                                    statusText.setText("UNAVAILABLE");
                                            
                                }else{
                                    statusRectangle.setFill(Color.GREEN);
                                    statusText.setFill(Color.GREEN);
                                    statusText.setText("AVAILABLE");
                                }

                            }else{

                                statusRectangle.setFill(Color.CYAN);
                                statusText.setFill(Color.CYAN);
                                statusText.setText("BUSY");

                            }
                        
                        }else{
                            
                            statusRectangle.setFill(Color.RED);
                            statusText.setFill(Color.RED);
                            statusText.setText("OFFLINE");
                            
                        }
                        
                        if (isTruckCargoed){
                            
//                            cargoText.setText("FULL CARGO");
                            cargoText.setText("12 Ton Iron");
                            
                        }else{
                            
                            cargoText.setText("EMPTY CARGO");                            
                            
                        }
                        
//                        Bounds bounds = statusText.getBoundsInParent();
                        
                                                
                        break;
                                                        
                    }else{
                        
                        // If the group corresponds to a ID not detected:
                                                
                        statusRectangle.setFill(Color.RED);
                        statusText.setFill(Color.RED);
                        statusText.setText("OFFLINE");
                        
                    }
                                        
                }    
                
            }
            
            Bounds bounds = currentGroup.getBoundsInLocal();
                        
            double centerXStatusText = ( bounds.getMinX() + bounds.getMaxX() )*0.5*(2.0/3.0);
            double centerYStatusText = ( bounds.getMinY() + bounds.getMaxY() )*(9.0/10.0);

            if ( currentGroup.getId().equals("truckId"+98) || currentGroup.getId().equals("truckId"+99) ){
                statusText = DrawingBaseFunctions.centerTextAroundOffset(( bounds.getMinX() + bounds.getMaxX() )*0.5, centerYStatusText, statusText);
            }else{
                statusText = DrawingBaseFunctions.centerTextAroundOffset(centerXStatusText, centerYStatusText, statusText);
            }
            
            
            statusText.setTextAlignment(TextAlignment.CENTER);
            
            double centerXCargoText = ( bounds.getMinX() + bounds.getMaxX() )*0.5*(1.0/3.0) + 2*centerXStatusText;
            
            cargoText = DrawingBaseFunctions.centerTextAroundOffset(centerXCargoText, centerYStatusText, cargoText);
            cargoText.setTextAlignment(TextAlignment.CENTER);
            
        }
        
                
        List<Integer> onlineBodiesList = new ArrayList<Integer>();
        List<Integer> offlineBodiesList = new ArrayList<>();
        
        
        for (QualisysBody body : availableTruckBodiesList){
            if ( body.isControllable() ){
                
                onlineBodiesList.add(body.getId());
                                
            }else{
                
                offlineBodiesList.add(body.getId());
                
            }
        }
        
        Collections.sort( onlineBodiesList );
        Collections.sort( offlineBodiesList );
        
        
        int availableTrucksCounter = 0;
        
        for (QualisysBody body : availableTruckBodiesList){
                   
//        for(int i = 0 ; i < 4 ; i++){
            
            Group currentTruckGroup = (Group) trucksMenuGroup.lookup( "#truck"+body.getId() );
            
            int truckCounter = 1;
            
            if ( !body.isControllable() ){
                truckCounter = onlineBodiesList.size();
//                truckCounter = truckCounter +
                truckCounter = truckCounter + offlineBodiesList.indexOf(body.getId());
            }else{
                
                truckCounter = onlineBodiesList.indexOf(body.getId());
            }
            
            System.out.println("truckCounter = " + truckCounter);
            
            Text tempText = (Text) currentTruckGroup.lookup("#availableText");
        
//        double elementWidth = windowWidth*(1.0-Globals.mapXRatio);
            double elementHeight = (actionsManagerHeight - 2*tempText.getBoundsInLocal().getHeight() )/truckInformationList.size();
            
//            currentTruckGroup.setLayoutX( windowWidth*Globals.mapXRatio + windowWidth*(1.0-Globals.mapXRatio)*0.5 - currentTruckGroup.getBoundsInLocal().getWidth()/2.0 );
            currentTruckGroup.setLayoutY( ((double)(truckCounter-1))*elementHeight + 2*tempText.getBoundsInLocal().getHeight() );
            
            if (currentTruckGroup == null){
                
                System.out.println("CREATING");
            
                currentTruckGroup = new Group();
                
                currentTruckGroup.setId( "#truck"+body.getId() );
                
                Image image = new Image("file:truck.png");

                if ( image.getWidth() < 1.0 ){

                    System.err.println("Image not found! (truck.png)");

                }
                
                ImageView imageView = new ImageView(image);

                double imageRatio = image.getWidth()/image.getHeight();

                imageView.setPreserveRatio(true);

                double truckMargin = 0.1*actionsManagerWidth;
                imageView.setFitWidth( actionsManagerWidth - 2*truckMargin);

                double imageHeight = imageView.getFitWidth()/imageRatio;

//                imageView.setLayoutX( windowWidth*Globals.mapXRatio + truckMargin);
                imageView.setLayoutY( ( (double)(availableTrucksCounter+1) )*(1.2)*imageHeight );
                availableTrucksCounter++;
    //            System.out.println("1080*Globals.mapXRatio = " + 1080*Globals.mapXRatio);
                
                currentTruckGroup.getChildren().add(imageView);
                
                trucksMenuGroup.getChildren().add(currentTruckGroup);
                
            }
            
        }
        
        
        
    }
    
    
    
}
