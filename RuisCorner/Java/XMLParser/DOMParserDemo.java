import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class DOMParserDemo {
    
    public static void main(String[] args) throws Exception {
        SAXParserFactory parserFactor = SAXParserFactory.newInstance();
        SAXParser parser = parserFactor.newSAXParser();
        SAXHandler handler = new SAXHandler();
//    parser.parse(ClassLoader.getSystemResourceAsStream("employees.xml"), handler);
        parser.parse(ClassLoader.getSystemResourceAsStream("myFirstCityBiSimple.xml"), handler);
        
        //Printing the list of employees obtained from XML
        for ( Employee emp : handler.empList){
            System.out.println(emp);
        }
        
        
        
        System.out.println("handler.nodeList.size() = " + handler.nodeList.size());
        System.out.println("handler.wayList.size() = " + handler.wayList.size());
        System.out.println("handler.relationList.size() = " + handler.relationList.size());
        
        for ( Way way : handler.wayList){
            System.out.println(way);
            System.out.println("");
        }
        
        for ( Relation relation : handler.relationList){
            System.out.println(relation);
            System.out.println("");
        }
        
    }
}
/**
 * The Handler for SAX Events.
 */
class SAXHandler extends DefaultHandler {
    
    boolean handlingNode = false;
    boolean handlingWay = false;
    boolean handlingRelation = false;
    
    
    List<Employee> empList = new ArrayList<>();
    List<Node> nodeList = new ArrayList<>();
    List<Way> wayList = new ArrayList<>();
    List<Relation> relationList = new ArrayList<>();
    Employee emp = null;
    Node node = null;
    Way way = null;
    Relation relation = null;
    String content = null;
    @Override
    //Triggered when the start of tag is found.
    public void startElement(String uri, String localName,
            String qName, Attributes attributes)
            throws SAXException {
        
//        System.out.println("Start of element " + qName);
//    System.out.println("attributes = " + attributes.getLength());
        switch(qName){
            //Create a new Employee object when the start tag is found
            case "node":
                handlingNode = true;
                node = new Node();
                node.id = Integer.parseInt( attributes.getValue("id") );
                node.lat = Double.parseDouble( attributes.getValue("lat") );
                node.lon = Double.parseDouble( attributes.getValue("lon") );
                break;
            case "way":
                handlingWay = true;
                way = new Way();
                way.id = Integer.parseInt( attributes.getValue("id") );
                break;
            case "relation":
                handlingRelation = true;
                relation = new Relation();
                relation.id = Integer.parseInt( attributes.getValue("id") );
                break;
            case "nd":
                if (handlingWay){
                    way.nodeIds.add( Integer.parseInt( attributes.getValue("ref") ) );
                }
                break;
            case "member":
                if (handlingRelation){
                    if ( attributes.getValue("type").equals("way") ){
                        
                        if ( attributes.getValue("role").equals("left") ){
                            relation.refLeft = Integer.parseInt( attributes.getValue("ref") );
//                            System.out.println("attributes.getValue(ref) = " + attributes.getValue("ref"));
                        }
                        if ( attributes.getValue("role").equals("right") ){
                            relation.refRight = Integer.parseInt( attributes.getValue("ref") );
//                            System.out.println("attributes.getValue(ref) = " + attributes.getValue("ref"));
                            Integer.parseInt( attributes.getValue("ref") );
                        }
                                                
                    }
//                    way.nodeIds.add( Integer.parseInt( attributes.getValue("ref") ) );
                }
                break;
            case "tag":
                if (handlingRelation){
                  
                    if (attributes.getValue("k") != null){
                    
                        if ( attributes.getValue("k").equals("bidirectional") ){
                            
                            if ( attributes.getValue("v").equals("yes") ){
                                relation.bidirectional = true;
                            }else{
                                relation.bidirectional = false;
                            }
                            
                        }
                        
                        if ( attributes.getValue("k").equals("type") ){
                            
                            if ( attributes.getValue("v").equals("lanelet") ){
                                relation.lanelet = true;
                            }else{
                                relation.lanelet = false;
                            }
                            
                        }
                    
                    }
                    
                    
//                    way.nodeIds.add( Integer.parseInt( attributes.getValue("ref") ) );
                }
                break;
            
        }
    }
    
    @Override
    public void endElement(String uri, String localName,
            String qName) throws SAXException {
//        System.out.println("End of element " + qName);
        switch(qName){
            //Add the employee to list once end tag is found
            case "nd":
//                System.out.println("attributes.getValue(ref) = " + attributes.getValue("ref"));
                break;
            case "employee":
                empList.add(emp);
                break;
                //For all other end tags the employee has to be updated.
            case "firstName":
                emp.firstName = content;
                break;
            case "lastName":
                emp.lastName = content;
                break;
            case "location":
                emp.location = content;
                break;
            case "node":
                if (handlingNode){
                    handlingNode = false;
                }
                nodeList.add(node);
                break;
            case "way":
                if (handlingWay){
                    handlingWay = false;
                }
                wayList.add(way);
                break;
            case "relation":
                if (handlingRelation){
                    handlingRelation = false;
                }
                relationList.add(relation);
                break;
//     case "firstName":
//       emp.firstName = content;
//       break;
//     case "lat":
//       node.lat = Double.parseDouble( content );
//       break;
//     case "lon":
//       node.lon = Double.parseDouble( content );
//       break;
        }
    }
    
    @Override
    public void characters(char[] ch, int start, int length)
            throws SAXException {
        content = String.copyValueOf(ch, start, length).trim();
    }
    
}

class Employee {
    
    String id;
    String firstName;
    String lastName;
    String location;
    
    @Override
    public String toString() {
        return firstName + " " + lastName + "(" + id + ")" + location;
    }
}

class Node {
    
    int id;
    double lat;
    double lon;
    
    @Override
    public String toString() {
        return "Node " + id + " lat " + lat + " lon " + lon;
    }
    
}

class Way {
    
    int id;
    List<Integer> nodeIds = new ArrayList<>();
    
    @Override
    public String toString() {
        return "Way " + id + " with elements: " + nodeIds;
    }

}

class Relation {
    
    int id;
    int refLeft;
    int refRight;
    boolean lanelet;
    boolean bidirectional;
    
    @Override
    public String toString() {
        return "Relation " + id + " with left: " + refLeft + " and right: " + refRight + ". Lanelet: " + lanelet + ". Bidirectional: " + bidirectional;
    }

}