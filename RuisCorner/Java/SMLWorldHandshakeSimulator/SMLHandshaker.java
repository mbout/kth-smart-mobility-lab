/*
 * Copyright (c) 2013, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Oracle or the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 

import java.io.*;
import java.net.*;
import java.util.Random;

public class SMLHandshaker {
    
    static boolean connection_request = false;
    
    
    
    
    
    public static void main(String[] args) throws IOException, InterruptedException {
        
        System.out.println("Starting server...");
        
        if (args.length != 1) {
            System.err.println("Usage: java SMLHandshaker <port number>");
            System.exit(1);
        }
        
        int portNumber = Integer.parseInt(args[0]);
        
        
        System.out.println("Server is now online and waiting to accept a connetion...");
        try (
            // Creates a server socket listening on the port given in the command line
            ServerSocket serverSocket =
                new ServerSocket(Integer.parseInt(args[0]));
            
            // Creates a socket accepting incoming connections
            
            Socket clientSocket = serverSocket.accept();     
                
            // Creates a printer to send information through the socket
            PrintWriter out =
                new PrintWriter(clientSocket.getOutputStream(), true);    
                
            // Creates a buffer to receive information through the socket
            BufferedReader in = new BufferedReader(
                new InputStreamReader(clientSocket.getInputStream()));
        ) {
            String inputLine;
            String outputLine;
            
            System.out.println("Connection estabilished, waiting for messages...");
            
            inputLine = in.readLine();
            
            if (inputLine == null){
                
                System.out.println("Received a terminating caracter.");
                return;
                
            }
            
            if (inputLine.equals("CONNECT")){
                
                String response = "OK";
                out.println(response);
                
            }else{
                
                System.out.println("I was waiting for a CONNECT but received something else, SHUTTING DOWN.");
                return;
                
            }
            
            Random randomGenerator = new Random();
            long statePublishingRate = 1;
            
            for ( int i = 0; i < 100000; i++){
                
                float currentState = randomGenerator.nextFloat();
                String current_state = Float.toString(currentState);
//                String current_state = randomGenerator.nextFloat().toString();
                System.out.println("Sending current state: " + current_state);
                out.println(current_state);
                
                Thread.sleep(1000/statePublishingRate);
                
            }
            

        } catch (IOException e) {
            System.out.println("Exception caught when trying to listen on port "
                + portNumber + " or listening for a connection");
            System.out.println(e.getMessage());
        }
    }
    
    static private String parseSocketInput(String socketInput){
        
        if ( socketInput.equals("CONNECT") ){
            
            if ( connection_request == false ){
                
                // We are receiving a connection request for the first time
                return "OK";
                
            }
            
        }
        
        
        return null;
    }
}