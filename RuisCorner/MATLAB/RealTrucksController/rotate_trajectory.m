function [x_r,y_r]=rotate_trajectory(x_curr,x_r,y_r)
%ROTATE_TRAJECTORY Summary of this function goes here
%   Detailed explanation goes here

theta=x_curr(3);

x_r=x_r-x_curr(1); y_r=y_r-x_curr(2);

if size(x_r,2)==1

    x_r=x_r'; y_r=y_r';
    R=[cos(theta) sin(theta);...
        sin(-theta) cos(theta)];
    
    temp=R*[x_r;y_r];
    
    x_r=temp(1,:);
    y_r=temp(2,:);
    x_r=x_r'; y_r=y_r';
    
else
    
    R=[cos(theta) sin(theta);...
        sin(-theta) cos(theta)];
    
    temp=R*[x_r;y_r];
    
    x_r=temp(1,:);
    y_r=temp(2,:);
    
end

end

