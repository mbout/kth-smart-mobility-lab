% RRT STUFF
clear
clc

% goal=[1.5 -1.5 0 0]; % Define goal position
goal=[1.5 1.5 0 0]; % Define goal position

trailer_volt=0; % Lock trailer

% if isunix
% addpath('collision_detector\','drawing\','map_creation\',...
%     'system_simulator\','nearest\','plot_graph\');
% end
addpath('collision_detector','drawing','map_creation',...
    'handle_loaders','misc','system_simulator','nearest','plot_graph');

space_width=4;
space_limits=[-space_width/2 space_width/2 -space_width/2 space_width/2];
% Obs=map_creation_lab_1(space_width,space_width/30);
% Obs={};
% width=1200
close all
% figure('Position',[2020 60 650 625])
figure(); hold on;
% set(gcf, 'position', [1921,433,1024,692])
handle_fill=fill([space_limits(1) space_limits(1) space_limits(2) space_limits(2)],...
    [space_limits(3) space_limits(4) space_limits(4) space_limits(3)],...
    [173,255,47]/255);
plot([space_limits(1) space_limits(1) space_limits(2) space_limits(2) space_limits(1)],...
    [space_limits(3) space_limits(4) space_limits(4) space_limits(3) space_limits(3)],'r--')
plot(goal(1),goal(2),'go','MarkerSize',25,'LineWidth',20)
% Obs=map_creation_boxes_1(qtm);
Obs=map_creation_boxes_fake;
% Obs={};
% axis([-2 2 -2 2])
width=6.18;
x_offset=-2.36;
height=4.4;
y_offset=-2.4;
axis([x_offset x_offset+width y_offset y_offset+height])
plot([0 0 .5],[0 .5 0],'rx')
draw_obstacle(Obs)
pause()
% return

model_load_handle=@truck_trailer_load_handle;
u_1=0.1; tolerance=u_1;
u_2=deg2rad(15)*[-1 0 1];
% [x_start,y_start,theta_start,~]=get_pose_qualisys(qtm);
x_start=-1.5; y_start=0; theta_start=0;
% x_i=[x_start y_start theta_start 0];
x_i=[x_start y_start theta_start theta_start];
if isnan(x_i(1))
    error('Invalid Qualisys')
end

time_allowed=5;

str='test';
[coord_path,best_path]=RRT(space_width,space_limits,Obs,u_1,u_2,x_i,tolerance,goal,...
    str,model_load_handle,time_allowed);
%%
% figure; hold on;
d=0.166;
car_states=[best_path(:,1)+d*cos(best_path(:,3)) best_path(:,2)+d*sin(best_path(:,3)) best_path(:,4)];

string_sample=zeros(1,length(car_states)-2);

generation_size=20;
[first_gen]=ps_first_generation(string_sample,generation_size);

for generation=1:10
    
    disp(' '); disp(strcat('Generation #',num2str(generation))); disp(' ');
    if generation==1
        current_gen=first_gen;
        continue;
    end
    
%     current_gen=ps_fitness_sort(current_gen,car_states);
    current_gen=ps_fitness_sort_collision(current_gen,car_states,x_i,Obs);
    current_gen=ps_natural_selection(current_gen);
        
end

last_gen=ps_fitness_sort_collision(current_gen,car_states,x_i,Obs);
last_score=ps_fitness_print(car_states,last_gen(1,:))

num_waypoints=100;

[xx,yy]=ps_winner_path(car_states,last_gen(1,:),num_waypoints);
%  [score] = ps_fitness_print(best_path,string_individual)

% plot(car_states(:,1),car_states(:,2),'r')
