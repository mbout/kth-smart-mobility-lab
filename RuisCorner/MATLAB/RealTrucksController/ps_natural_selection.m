function [ new_gen ] = ps_natural_selection(current_gen)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

new_gen=[];

alpha_males=round(size(current_gen,1)/5);

new_gen=current_gen(1:alpha_males,:);

beta_males=size(current_gen,1)-alpha_males;

for i=1:beta_males
    
    rand_value_1=exprnd(0.35);
    while rand_value_1>1
        rand_value_1=exprnd(0.35);
    end
    individual_1=current_gen(ceil(rand_value_1*size(current_gen,1)),:);
    
    rand_value_2=exprnd(0.35);
    while rand_value_2>1
        rand_value_2=exprnd(0.35);
    end
    individual_2=current_gen(ceil(rand_value_2*size(current_gen,1)),:);
    
    
    % Cross over avoiding repeating same individuals
    crossover_tries=0;
    while 1
        crossover_tries=crossover_tries+1;
        repeated=0;
        
        cross_over_point=rand();
        cross_over_point=round(cross_over_point*size(current_gen,2));
        cross_over_point=max(1,cross_over_point); cross_over_point=min(size(current_gen,2),cross_over_point);
        new_individual=[individual_1(1:cross_over_point-1) individual_2(cross_over_point:end)];
        
        for j=1:size(new_gen,1)
            if isequal(new_individual,new_gen(j,:))
                % Already present
                repeated=1;
            end
        end
        
        if ~repeated
            break
        end
        
        if crossover_tries>20
            % Insert Mutation
disp('M')%             disp('Mutating')
            new_individual=round(rand(size(new_individual)));
            break
        end
        
        
    end
    
    new_gen=[new_gen; new_individual];
    
    
end

end