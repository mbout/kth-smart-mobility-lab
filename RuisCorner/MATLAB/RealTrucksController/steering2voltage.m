function [w_volt]=steering2voltage(phi)
%VELOCITY2VOLTAGE Summary of this function goes here
%   Detailed explanation goes here
% Based on steering_calibtration_4.mat
%Phi comes in radians

P=[-0.403201612510870   0.667037411833784];

w_volt=(phi-P(2))/P(1);

if phi<-0.45
   w_volt=2.77;
   return;
end

if phi>0.45
   w_volt=0.54;
   return;
end

end

