function [ new_vehicle_readings, dangerTrucks, trajectoryToPerform, ...
    bodyToPerformTrajectoryId, stop_command_ids, reverse_trajectory_request ] = ...
    controlLoopIncomingMessageProcessor( socket, t_step , qualisysReadingsStructure)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

    global buttonReverseRequest
    global buttonReverseCommand

    dangerTrucks = [];
    new_states = [];
    new_vehicle_readings = {};
    timeOutMillis = 10;
    trajectoryToPerform = [];
    bodyToPerformTrajectoryId = [];
    stop_command_ids = [];
    reverse_trajectory_request = [];

    receiving_messages_timeout = t_step/2;

    receiving_messages_timer = tic;

    while true

        if toc(receiving_messages_timer) > receiving_messages_timeout

            disp('Warning: Spending too much time to receive messages.')
            break

        end

        receivedLine = 0;
        
        if (buttonReverseRequest || buttonReverseCommand)
            
            disp('HERE')
            if buttonReverseRequest
                
                load stringReverseTrajectoryRequest
%                 buttonReverseRequest = false;
                
            end
            
            if buttonReverseCommand
                                
                load stringReverseTrajectoryCommand
                buttonReverseCommand = false;
            
            end
            
            
            
        else
        
            receivedLine = javaReceiveLineOverSocket( socket, timeOutMillis );
    
        end

%         receivedLine = javaReceiveLineOverSocket( socket, timeOutMillis );
        
%         disp(receivedLine)
        
        if receivedLine == 0

            break

        end

        messageType = javaGetMessageType( receivedLine );
        
        if strcmp(messageType,'collision_warning') == 1
%                 disp('Warning received')
            dangerTrucks = javaProcessXmlCollisionWarning( receivedLine );
        end

        if strcmp(messageType,'vehicle_states_reply') == 1
            disp('Vehicle states received - NOT EXPECTED ANYMORE!!!!!!!!!!!')
            new_states = javaProcessXmlStates( receivedLine );
        end
        
        if strcmp(messageType,'vehicle_readings_reply') == 1
%             disp('Vehicle readings received')
            new_vehicle_readings = javaProcessXmlVehicleReadings( receivedLine );
        end
        
        if strcmp(messageType,'vehicle_stop_command') == 1
            disp('Vehicle stop command received')
            disp(receivedLine)
            stop_command_ids = javaProcessXmlVehicleStopCommand( receivedLine );
        end
        
        if strcmp(messageType,'reverse_trajectory_request') == 1
            
            disp('Reverse trajectory request received')
            disp(receivedLine)
            
%             save('stringReverseTrajectoryRequest','receivedLine') 
            
%             save('receivedLine','receivedLine')
            
            if buttonReverseRequest
                
                
                [ body_id, trailer_id, start_pose, goal_pose, obstacles, timeout ] = ...
                javaProcessXmlReverseTrajectoryRequest( receivedLine );
            
                [~,~,theta_truck,~] = get_pose_qualisys_by_ids(body_id, qualisysReadingsStructure);
                [x_trailer, y_trailer, theta_trailer,~] = get_pose_qualisys_by_ids(trailer_id, qualisysReadingsStructure);
            
                reverse_trajectory_request.body_id = body_id;
                reverse_trajectory_request.trailer_id = trailer_id;
                reverse_trajectory_request.start_pose = [x_trailer y_trailer theta_truck theta_trailer];
                reverse_trajectory_request.goal_pose = goal_pose;
                reverse_trajectory_request.obstacles = obstacles;
                reverse_trajectory_request.timeout = timeout;
                
                buttonReverseRequest = false;
                
            else
                
                [ body_id, trailer_id, start_pose, goal_pose, obstacles, timeout ] = ...
                javaProcessXmlReverseTrajectoryRequest( receivedLine );
                reverse_trajectory_request.body_id = body_id;
                reverse_trajectory_request.trailer_id = trailer_id;
                reverse_trajectory_request.start_pose = start_pose;
                reverse_trajectory_request.goal_pose = goal_pose;
                reverse_trajectory_request.obstacles = obstacles;
                reverse_trajectory_request.timeout = timeout;
                  
                disp('reverse_trajectory_request.start_pose')
                disp(reverse_trajectory_request.start_pose)
                
                disp('reverse_trajectory_request.body_id')
                disp(reverse_trajectory_request.body_id)
                disp('reverse_trajectory_request.trailer_id')
                disp(reverse_trajectory_request.trailer_id)
                
            end

            
            
%             save(FILENAME,VARIABLES) 
                        
        end
        
        if strcmp(messageType,'reverse_trajectory_command') == 1
            
            disp('Reverse trajectory command received')
            disp(receivedLine)
            
%             save('stringReverseTrajectoryCommand','receivedLine') 
            
            trajectoryToPerform = 'reverse';           
            
        end
        
        if strcmp(messageType, 'body_trajectory_command' ) == 1

            disp('Received a trajectory command, will break the cycle')
            
            disp('Process XML Trajectory')
            [receivedInfo, bodyToPerformTrajectoryId] = javaProcessXmlTrajectory( receivedLine );
            disp('XML Trajectory Processed')

%             trajectoryToPerform = receivedInfo(:,1:2)/32.;
            trajectoryToPerform = receivedInfo(:,1:2);
                        
            if ( isempty(trajectoryToPerform) )
               
                disp('Received an empty trajectory!')
                
            end
            
%             disp('----------trajectoryToPerform:')
            
%             save('trajectoryToPerformOriginal','trajectoryToPerform')
%             disp(trajectoryToPerform)
                        
            trajectoryToPerform = interpolateTrajectory(trajectoryToPerform);
            
%             disp('------------trajectoryToPerform:')
%             disp(trajectoryToPerform)

%             save('trajectoryToPerformInterpolated','trajectoryToPerform')
            
            % Must interpolate my trajectory

            disp( strcat( 'Will make trajectory on body  ' , num2str(bodyToPerformTrajectoryId) ) )
            
%             disp( 'Trajecoty:' )
%             disp(trajectoryToPerform)
            
            
            break
                
        end

    end


end

