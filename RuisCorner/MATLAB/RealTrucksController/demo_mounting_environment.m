% RRT STUFF
clear
clc
clf




rng(11)

qualisys_truck_id=13;
qualisys_trailer_id=14;
qualisys_goal_id=1;
qualisys_box_1_id=9;
qualisys_box_2_id=10;
qualisys_box_3_id=11;
qualisys_box_4_id=12;

% qualisys_box_1_id=1;
% qualisys_box_2_id=14;
% qualisys_box_3_id=1;
% qualisys_box_4_id=1;


% goal=[1.5 -1.5 0 0]; % Define goal position
% goal=[(-1).^round(0.5+.125*randn)*1.25 (-1).^round(0.5+.125*randn)*1.25 0 0]; % Define goal position

% INITIALIZATION STUFF
if exist('qtm','var')
    % Close connection
    QMC(qtm, 'disconnect');
    clear mex
end

if ~exist('qtm','var')
    % Open connection
    disp('Opening connection to Qualisys...')
    qtm=QMC('QMC_conf.txt');
    disp('Opened connection to Qualisys...')
    
end
% if ~exist('s','var')
%     disp('Opening connection to NI...')
%     s=NI_initialization();
%     NI_voltage_stop(s)
%     disp('Opened connection to NI...')
% end

% NI_voltage_output(s,1.7,1.6,0);

addpath('collision_detector','drawing','map_creation',...
    'handle_loaders','misc','system_simulator','nearest','plot_graph');

space_width=4;
space_limits=[-space_width/2 space_width/2 -space_width/2 space_width/2];
space_limits(1)=space_limits(1)+0.5+.125;
space_limits(3)=space_limits(3)-0.25-.125;
space_limits(4)=space_limits(4)-0.125/2;

% pause
close all
% figure('Position',[2020 60 650 625])
figure(); hold on;
set(gcf, 'position', [1921,433,1024,692])
handle_fill=fill([space_limits(1) space_limits(1) space_limits(2) space_limits(2)],...
    [space_limits(3) space_limits(4) space_limits(4) space_limits(3)],...
    [173,255,47]/255);
plot([space_limits(1) space_limits(1) space_limits(2) space_limits(2) space_limits(1)],...
    [space_limits(3) space_limits(4) space_limits(4) space_limits(3) space_limits(3)],'r--')
width=6.18;
x_offset=-2.36;
height=4.4;
y_offset=-2.4;
axis([x_offset x_offset+width y_offset y_offset+height])

for temp_it=1:10^10

    [x_goal,y_goal,~,~] = get_pose_qualisys_id(qtm,qualisys_goal_id);
    if ~isnan(x_goal)
        goal=[x_goal y_goal 0 0]; % Define goal position
    else
        goal=[inf inf 0 0];
        %         error('Goal not detected by qualisys!')
    end
    

    
    % if isunix
    % addpath('collision_detector\','drawing\','map_creation\',...
    %     'system_simulator\','nearest\','plot_graph\');
    % end
    
    % Obs=map_creation_lab_1(space_width,space_width/30);
    % Obs={};
    % width=1200
    
    
    % Obs=map_creation_boxes_1(qtm);
    % Obs=map_creation_boxes_2(qtm);
%     Obs=map_creation_boxes_3(qtm,qualisys_box_1_id,...
%     qualisys_box_2_id,qualisys_box_3_id,qualisys_box_4_id);
%     % axis([-2 2 -2 2])
% 
    [x_box_temp,y_box_temp,theta,~]=get_pose_qualisys_id(qtm,qualisys_box_1_id);
    if inside_workspace_point([x_box_temp y_box_temp],space_limits)
        draw_obstacle(map_creation_boxes_id(qtm,qualisys_box_1_id));
    end
%      disp([x_box_temp,y_box_temp,theta])
%     
    [x_box_temp,y_box_temp,theta,~]=get_pose_qualisys_id(qtm,qualisys_box_2_id);
    if inside_workspace_point([x_box_temp y_box_temp],space_limits)
        draw_obstacle(map_creation_boxes_id(qtm,qualisys_box_2_id));
    end
%     
%      disp([x_box_temp,y_box_temp,theta])
    [x_box_temp,y_box_temp,theta,~]=get_pose_qualisys_id(qtm,qualisys_box_3_id);
    if inside_workspace_point([x_box_temp y_box_temp],space_limits)
        draw_obstacle(map_creation_boxes_id(qtm,qualisys_box_3_id));
    end
%     
%      disp([x_box_temp,y_box_temp,theta])
    [x_box_temp,y_box_temp,theta,~]=get_pose_qualisys_id(qtm,qualisys_box_4_id);
    if inside_workspace_point([x_box_temp y_box_temp],space_limits)
        draw_obstacle(map_creation_boxes_id(qtm,qualisys_box_4_id));
    end
%     
%     disp([x_box_temp,y_box_temp,theta])
%     
%     model_load_handle=@truck_trailer_load_handle;
%     u_1=0.1; tolerance=2.0*u_1;
%     u_2=deg2rad(15)*[-1 0 1];
% %     [x_start,y_start,theta_start,~]=get_pose_qualisys(qtm);
    [x_start,y_start,theta_start,~] = get_pose_qualisys_id(qtm,qualisys_truck_id);
    [x_truck,y_truck,theta_truck,~] = get_pose_qualisys_id(qtm,qualisys_trailer_id);
%     % x_i=[x_start y_start theta_start 0];
% %     x_i=[x_truck y_truck theta_truck theta_start];
    x_i=[x_truck y_truck theta_truck theta_start];
    
    disp(x_i)
%     
    plot(goal(1),goal(2),'ro','MarkerSize',30,'LineWidth',3)
%     if inside_workspace_point([x_goal y_goal],space_limits)
%         goal_circle_x=1.0*tolerance*cos([0:0.1:2*pi]); goal_circle_y=1.0*tolerance*sin([0:0.1:2*pi]);
%         plot(goal_circle_x+goal(1),goal_circle_y+goal(2),'r.');
%     end

%     if inside_workspace_point(x_i(1:2),space_limits)
        draw_state_truck_trailer(x_i);
%     end
    
    
    axis([x_offset x_offset+width y_offset y_offset+height])
    pause(0.1)
    clf
    hold on;
    handle_fill=fill([space_limits(1) space_limits(1) space_limits(2) space_limits(2)],...
        [space_limits(3) space_limits(4) space_limits(4) space_limits(3)],...
        [173,255,47]/255);
    plot([space_limits(1) space_limits(1) space_limits(2) space_limits(2) space_limits(1)],...
        [space_limits(3) space_limits(4) space_limits(4) space_limits(3) space_limits(3)],'r--')
    
end