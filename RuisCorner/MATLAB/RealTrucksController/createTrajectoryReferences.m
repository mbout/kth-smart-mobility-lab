function [ PID_struct_longitudinal_vector, PID_struct_lateral_vector,...
    x_r_vector, y_r_vector, theta_hist_vector,...
    isPerformingVector, t_stopped_vector, t_init_vector, d_s, mean_velocity ]...
    = createTrajectoryReferences(...
    trajectoryToPerform, truckNumber, t_step, t_stamp, ...
    PID_struct_longitudinal_vector,PID_struct_lateral_vector,...
    x_r_vector, y_r_vector, theta_hist_vector,...
    isPerformingVector, t_stopped_vector, t_init_vector )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

    global look_ahead_distance;

        %%% START HERE

        delta_x=diff(trajectoryToPerform(:,1)); delta_y=diff(trajectoryToPerform(:,2));
        distance_to_travel=(delta_x.^2 + delta_y.^2).^.5;
        distance_to_travel=sum(distance_to_travel);
        final_time=distance_to_travel/0.19;

%         disp('final_time')
%         disp(final_time)
%         
%         figure; hold on;
%         plot(trajectoryToPerform(:,1), trajectoryToPerform(:,2),'x')
%         plot(trajectoryToPerform(end-10:end,1), trajectoryToPerform(end-10:end,2),'gx')
%         axis([-2 2 -2 2])
        
        % INTERPOLATE STUFF
        
        x=trajectoryToPerform(:,1); y=trajectoryToPerform(:,2);
%         t_step=0.1;

        t=0:t_step:final_time; t=t';
        t_old=0:final_time/(length(x)-1):final_time;
        x_r = spline(t_old,x,t); y_r = spline(t_old,y,t);
        % h_path=figure; hold on;

%         L=0.115;

        % Path creation
        x_r_d=[diff(x_r)/t_step; (x_r(end)-x_r(end-1))/t_step]; y_r_d=[diff(y_r)/t_step; (y_r(end)-y_r(end-1))/t_step];
        theta_r=unwrap(atan2(y_r_d,x_r_d));
        
        theta_hist=nan(size(x_r_d));
        time_hist=nan(size(x_r_d));
        
        length_path=(diff(x_r).^2)+(diff(y_r).^2); length_path=sum(length_path.^.5);
        mean_velocity=length_path/t(end);

%         d_s=5/32; % Original        
        d_s=look_ahead_distance/32;
        % Best configuration so far

        %Londitudinal PID
        Kp_lo=2.0; Ki_lo=0.0;Kd_lo=0.0;
        PID_struct_longitudinal_vector{truckNumber} = PID_init(Kp_lo,Ki_lo,Kd_lo);
        %Lateral PID
        Kp_la=2.0;Ki_la=0.0;Kd_la=10.0;
        PID_struct_lateral_vector{truckNumber} = PID_init(Kp_la,Ki_la,Kd_la);
        
        x_r_vector{truckNumber} = x_r;
        y_r_vector{truckNumber} = y_r;
%         theta_r_vector{truckNumber} = theta_r; % Not used in this
%         controller
        theta_hist_vector{truckNumber} = theta_hist;
        isPerformingVector(truckNumber) = true;
        t_stopped_vector(truckNumber) = 0;
        t_init_vector(truckNumber) = t_stamp;
        
end

