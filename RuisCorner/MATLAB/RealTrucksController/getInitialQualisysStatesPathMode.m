function [ x_curr ] = getInitialQualisysStatesPathMode( qtm, controlledBodyIds, qualisysVerbose )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

    num_memory_samples = 5;

    [x_q,y_q,theta_q,t_stamp]=get_pose_qualisys_by_ids(qtm,controlledBodyIds);
    
    for i = 1:length(controlledBodyIds)
        
        x_curr{i} = ones( num_memory_samples ,3)*nan;
        
    end
    
%     x_curr = zeros( length(controlledBodyIds) ,3);

    for i = 1:length(controlledBodyIds)

        if ~isnan( x_q(i) ) % Handle possible invalid values
            if theta_q(i)<0 % Put theta in range [0,2*pi]
                theta_q(i)=theta_q(i)+2*pi;
            end
            x_curr{i} = repmat([x_q(i) y_q(i) theta_q(i) t_stamp], [num_memory_samples 1]);
        else % If invalid reading, use previous reading
%             x_curr(i,:)=x_prev(i,:);
            
            error('Qualisys initial states are invalid')

            if qualisysVerbose
                disp( strcat('Invalid Qualisys, for id:', num2str(controlledBodyIds(i) ) ) ) % Issue warning to the user
            end
        end

    end

%     x_i=[x_q y_q theta_q];
%     x_curr=x_i;
    
end

