function [ messageType ] = javaGetMessageType( trajectoryString )
%CREATEXML Summary of this function goes here
%   Detailed explanation goes here

    import java.lang.String;
    import java.lang.Double;
    import java.lang.Integer;
    import java.io.File;
    import java.io.IOException;
    import java.io.StringReader;
    import java.io.StringWriter;
    import java.net.*;
    import java.net.InetAddress;
    import java.net.UnknownHostException;
    import java.util.ArrayList;
    import java.util.Collections;
    import java.util.Enumeration;
    import java.util.List;
    import javax.xml.parsers.DocumentBuilder;
    import javax.xml.parsers.DocumentBuilderFactory;
    import javax.xml.parsers.ParserConfigurationException;
    import javax.xml.transform.Transformer;
    import javax.xml.transform.TransformerException;
    import javax.xml.transform.TransformerFactory;
    import javax.xml.transform.dom.DOMSource;
    import javax.xml.transform.stream.StreamResult;
    import org.w3c.dom.Attr;
    import org.w3c.dom.Document;
    import org.w3c.dom.Element;
    import org.w3c.dom.NodeList;
    import org.xml.sax.InputSource;
    import org.xml.sax.SAXException;


    try
    
        dbf = DocumentBuilderFactory.newInstance();
    %     dom = null;

    %     try {

            %Using factory get an instance of document builder
        db = dbf.newDocumentBuilder();

    %     disp('trajectoryString')
    %     disp(trajectoryString)
    %     save('trajectoryString','trajectoryString')


            %parse using builder to get DOM representation of the XML file
    %     dom = loadXMLFromString( trajectoryString );

        factory = DocumentBuilderFactory.newInstance();
        builder = factory.newDocumentBuilder();
        is = InputSource(StringReader(trajectoryString));
        dom = builder.parse(is);


    %     }catch(ParserConfigurationException pce) {
    %             pce.printStackTrace();
    %     }catch(SAXException se) {
    %             se.printStackTrace();
    %     }catch(IOException ioe) {
    %             ioe.printStackTrace();
    %     }


        %get the root element
        docEle = dom.getDocumentElement();

    %     trajectoryElement = docEle.getElementsByTagName('trajectory');
    %     disp('trajectoryElement.getLength()')
    %     disp(trajectoryElement.getLength())

        messageElement = dom.getElementsByTagName('message');

    %     disp('messageElement.getLength()')
    %     disp(messageElement.getLength())


        message_type = messageElement.item(0).getAttribute('type');
        
%         disp( strcat( 'message_type = ', message_type) )

        messageType = '';

        if strcmp(message_type,'body_trajectory_command') == 1

            messageType = 'body_trajectory_command';

        end

        if strcmp(message_type,'collision_warning') == 1

            messageType = 'collision_warning';

        end
        
        if strcmp(message_type,'reverse_trajectory_request') == 1

            messageType = 'reverse_trajectory_request';

        end
        
        if strcmp(message_type,'reverse_trajectory_command') == 1

            messageType = 'reverse_trajectory_command';

        end        

        if strcmp(message_type,'vehicle_states_reply') == 1

            messageType = 'vehicle_states_reply';

        end
        
        if strcmp(message_type,'vehicle_readings_reply') == 1
            
            messageType = 'vehicle_readings_reply';

        end
        
        if strcmp(message_type,'vehicle_stop_command') == 1
            
            messageType = 'vehicle_stop_command';

        end
        
        if isempty(messageType)
           
            disp('javaGetMessageType')
            disp('Message type not handled yet! Message:')
            disp(trajectoryString)
                        
        end
        
        
    catch 
        
        disp('Caught an error while processing the following string:')
        disp(trajectoryString)
        messageType = 'error';
        
    end
    

end

