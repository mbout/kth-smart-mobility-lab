function [ receivedInfo ] = javaReceiveCollisionWarningOverSocket( socket )
%JAVARECEIVESTRINGOVERSOCKET Summary of this function goes here
%   Detailed explanation goes here

    import java.io.BufferedOutputStream;
    import java.io.BufferedReader;
    import java.io.FileNotFoundException;
    import java.io.FileOutputStream;
    import java.io.IOException;
    import java.io.InputStream;
    import java.io.InputStreamReader;
    import java.io.PrintWriter;
    import java.net.InetAddress;
    import java.net.InetSocketAddress;
    import java.net.ServerSocket;
    import java.net.Socket;
    import java.net.SocketAddress;
    import java.net.SocketTimeoutException;
    import java.lang.System;

    socket.setSoTimeout( 10 )
    
    try
                
        in = BufferedReader(InputStreamReader(socket.getInputStream()));
        receivedInfo = in.readLine();
        
    catch socketError
        
%         System.out.println('javaReceiveCollisionWarningOverSocket: Error in socket.readLine()');
        receivedInfo = 0;
        
    end
    
    


end

