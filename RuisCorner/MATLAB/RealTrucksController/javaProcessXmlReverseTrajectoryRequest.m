function [ body_id, trailer_id, start_pose, goal_pose, obstacles, timeout ] = ...
javaProcessXmlReverseTrajectoryRequest( collisionWarningString )
%CREATEXML Summary of this function goes here
%   Detailed explanation goes here

    import java.lang.String;
    import java.lang.Double;
    import java.lang.Integer;
    import java.io.File;
    import java.io.IOException;
    import java.io.StringReader;
    import java.io.StringWriter;
    import java.net.*;
    import java.net.InetAddress;
    import java.net.UnknownHostException;
    import java.util.ArrayList;
    import java.util.Collections;
    import java.util.Enumeration;
    import java.util.List;
    import javax.xml.parsers.DocumentBuilder;
    import javax.xml.parsers.DocumentBuilderFactory;
    import javax.xml.parsers.ParserConfigurationException;
    import javax.xml.transform.Transformer;
    import javax.xml.transform.TransformerException;
    import javax.xml.transform.TransformerFactory;
    import javax.xml.transform.dom.DOMSource;
    import javax.xml.transform.stream.StreamResult;
    import org.w3c.dom.Attr;
    import org.w3c.dom.Document;
    import org.w3c.dom.Element;
    import org.w3c.dom.NodeList;
    import org.xml.sax.InputSource;
    import org.xml.sax.SAXException;


%     dbf = DocumentBuilderFactory.newInstance();
%     dom = null;

%     try {

        %Using factory get an instance of document builder
%     db = dbf.newDocumentBuilder();
    
    factory = DocumentBuilderFactory.newInstance();
    builder = factory.newDocumentBuilder();
    is = InputSource(StringReader(collisionWarningString));
    dom = builder.parse(is);

    %get the root element
    docEle = dom.getDocumentElement();

    bodyElements = docEle.getElementsByTagName('body');

    if (bodyElements.getLength() ~= 1)
        
        disp('javaProcessXmlReverseTrajecotyRequest() Error, should only have one body element')
        
    end
    
    body_id = [];

    for i = 0:bodyElements.getLength()-1

        el = bodyElements.item(i);
        
        id = Integer.parseInt( el.getAttribute('id') );
        
        body_id = id;

    end

    trailerElements = docEle.getElementsByTagName('trailer');

    if (trailerElements.getLength() ~= 1)
        
        disp('javaProcessXmlReverseTrajecotyRequest() Error, should only have one trailer element')
        
    end
    
    trailer_id = [];

    for i = 0:trailerElements.getLength()-1

        el = trailerElements.item(i);
        
        id = Integer.parseInt( el.getAttribute('id') );
        
        trailer_id = id;

    end

    
    
    startPoseElements = docEle.getElementsByTagName('start_pose');

    if (startPoseElements.getLength() ~= 1)
        
        disp('javaProcessXmlReverseTrajecotyRequest() Error, should only have one start_pose element')
        
    end
    
    start_pose = [];

    for i = 0:startPoseElements.getLength()-1

        el = startPoseElements.item(i);
        
        x = Double.parseDouble( el.getAttribute('x') );
        y = Double.parseDouble( el.getAttribute('y') );
        theta_0 = Double.parseDouble( el.getAttribute('theta_0') );
        theta_1 = Double.parseDouble( el.getAttribute('theta_1') );
        
        start_pose = [x y theta_0 theta_1];

    end

    
    goalPoseElements = docEle.getElementsByTagName('goal_pose');

    if (goalPoseElements.getLength() ~= 1)
        
        disp('javaProcessXmlReverseTrajecotyRequest() Error, should only have one goal_pose element')
        
    end
    
    goal_pose = [];

    for i = 0:goalPoseElements.getLength()-1

        el = goalPoseElements.item(i);
        
        x = Double.parseDouble( el.getAttribute('x') );
        y = Double.parseDouble( el.getAttribute('y') );
        theta_0 = Double.parseDouble( el.getAttribute('theta_0') );
        theta_1 = Double.parseDouble( el.getAttribute('theta_1') );
        
        goal_pose = [x y theta_0 theta_1];

    end
    
    
    timeoutElements = docEle.getElementsByTagName('timeout');

    if (timeoutElements.getLength() ~= 1)
        
        disp('javaProcessXmlReverseTrajecotyRequest() Error, should only have one timeout element')
        
    end
    
    timeout = [];

    for i = 0:timeoutElements.getLength()-1

        el = timeoutElements.item(i);
        
        timeout = Double.parseDouble( el.getAttribute('seconds') );

    end
    
    
    
    obstacleCornerElements = docEle.getElementsByTagName('obstacle');

    obstacles = {};
    
    for i = 0:obstacleCornerElements.getLength()-1

        currentObstacle = obstacleCornerElements.item(i);
        
        obstacleCorners = currentObstacle.getElementsByTagName('corner');
        
        current_obstacle = zeros(obstacleCorners.getLength(), 2);
        
        for j = 0:obstacleCorners.getLength()-1
            
            currentCorner = obstacleCorners.item(j);
            
            x = Double.parseDouble( currentCorner.getAttribute('x') );
            y = Double.parseDouble( currentCorner.getAttribute('y') );
            
            current_obstacle(j+1,:) = [x y];
            
        end
                
        obstacles{i+1} = current_obstacle;

    end
    
end

