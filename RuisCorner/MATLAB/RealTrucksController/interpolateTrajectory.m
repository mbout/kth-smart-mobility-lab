function [ trajectoryToPerformInterpolated ] = interpolateTrajectory( trajectoryToPerform )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

    desired_points_per_meter = 70;
    
    delta_x = diff(trajectoryToPerform(:,1));
    delta_y = diff(trajectoryToPerform(:,2));
    
    remove_repeated = delta_x ~= 0;
%     disp(remove_repeated)
    
%     trajectoryToPerform = trajectoryToPerform( [1; remove_repeated] , : );
    delta_x = delta_x( remove_repeated );
    delta_y = delta_y( remove_repeated );    
    
    
    trajectoryToPerform = trajectoryToPerform( [true; remove_repeated] , : );
    
    distance_traveled = (delta_x.^2 + delta_y.^2).^.5;
    
    distance_traveled = [0; cumsum(distance_traveled) ];
    
%     figure; plot(distance_traveled);

    total_distance = distance_traveled(end);
    
    
%     size( trajectoryToPerform )
%     size( distance_traveled )
    
    num_points = ceil( total_distance*desired_points_per_meter ) + 1;
    
    interpolated_distance = [0: num_points-1]*(total_distance/(num_points-1));
    
    trajectoryToPerformInterpolated = [num_points, 2];
    
    for i = 1:num_points
        
        current_dist = interpolated_distance(i);
        
        [~, idx] = min( abs(distance_traveled - current_dist) );
                
        if ( current_dist >= distance_traveled(idx) )
            
            prev_idx = idx;
            next_idx = idx+1;
            
        else
            
            prev_idx = idx-1;
            next_idx = idx;
            
        end
        
        if ( next_idx > length(distance_traveled) )
            
            prev_idx = prev_idx-1;
            next_idx = next_idx - 1;
            
        end
        
        a = ( current_dist - distance_traveled(prev_idx) )/(distance_traveled(next_idx) - distance_traveled(prev_idx));
        
        current_interpolation_x = (1-a)*trajectoryToPerform(prev_idx, 1) + a*trajectoryToPerform(next_idx, 1);
        current_interpolation_y = (1-a)*trajectoryToPerform(prev_idx, 2) + a*trajectoryToPerform(next_idx, 2);
                
        trajectoryToPerformInterpolated(i, :) =[current_interpolation_x current_interpolation_y];
        
    end
    
end

