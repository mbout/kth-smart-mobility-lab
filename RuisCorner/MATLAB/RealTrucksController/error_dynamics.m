function [error_d] = error_dynamics(error,v_d,phi_d,v_r,phi_r)
%TRACKING_ERROR [error] = tracking_error(x_r,x)

l=0.115;

error_d=error;

error_d(1)=-v_d+v_r*cos(error(3))+error(2)*(v_d/l)*tan(phi_d);
error_d(2)=v_r*sin(error(3))-error(1)*(v_d/l)*tan(phi_d);
error_d(3)=(v_r/l)*tan(phi_r)-(v_d/l)*tan(phi_d);

end