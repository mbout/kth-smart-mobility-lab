
% RRT STUFF
clear
clc


% INITIALIZATION STUFF
if exist('qtm','var')
    % Close connection
    QMC(qtm, 'disconnect');
    clear mex
end

if ~exist('qtm','var')
    % Open connection
    qtm=QMC('QMC_conf.txt');
end
if ~exist('s','var')
    s=NI_initialization();
    NI_voltage_stop(s)
end


% if isunix
% addpath('collision_detector\','drawing\','map_creation\',...
%     'system_simulator\','nearest\','plot_graph\');
% end
addpath('collision_detector','drawing','map_creation',...
    'handle_loaders','misc','system_simulator','nearest','plot_graph');

space_width=5;
space_limits=[-space_width/2 space_width/2 -space_width/2 space_width/2];
Obs=map_creation_lab_1(space_width,space_width/30);
Obs={};

model_load_handle=@truck_trailer_load_handle;
u_1=0.1; tolerance=2*u_1;
u_2=deg2rad(15)*[-1 0 1];
[x_start,y_start,theta_start,~]=get_pose_qualisys(qtm);
% x_i=[x_start y_start theta_start 0];
x_i=[x_start y_start theta_start theta_start];
if isnan(x_i(1))
    error('Invalid Qualisys')
end
goal=[-2 2 0 0];

time_allowed=5;

str='test';
coord_path=RRT(space_width,space_limits,Obs,u_1,u_2,x_i,tolerance,goal,...
    str,model_load_handle,time_allowed);
    
delta_x=diff(coord_path(:,1)); delta_y=diff(coord_path(:,2));
distance_to_travel=(delta_x.^2 + delta_y.^2).^.5;
distance_to_travel=sum(distance_to_travel);
final_time=distance_to_travel/0.25;

% INTERPOLATE STUFF

x=coord_path(:,1); y=coord_path(:,2);
t_step=0.1;
t=0:t_step:final_time; t=t';
t_old=0:final_time/(length(x)-1):final_time;
x_r = spline(t_old,x,t); y_r = spline(t_old,y,t);

% CONTROLLER STUFF

h_path=figure; hold on;

L=0.115;

% SMC Parameters
k0=0.05; k1=0.25; k2=0.5;
K=[k0 k1 k2];
p1=1; p2=1;
P=[p1 p2];
q1=1; q2=1;
Q=[q1 q2];

% Battery Considerations
velocity2voltage_handle=@velocity2voltage_no_stop_higher;

battery_velocity=[-2:0.001:2];
battery_v_volt=nan(size(battery_velocity));
for i=1:length(battery_velocity)
    battery_v_volt(i)=velocity2voltage_handle(battery_velocity(i));
end

% PATH CREATION

[x_start,y_start,theta_start,~]=get_pose_qualisys(qtm);
plot(x_r,y_r); quiver(x_start,y_start,cos(theta_start),sin(theta_start));

% axis([min([x_r;x_start])-0.25 max([x_r;x_start])+0.25 min([y_r;y_start])-0.25 max([y_r;y_start])+0.25])
axis([-3 3 -3 3])

x_r_d=[diff(x_r)/t_step; (x_r(end)-x_r(end-1))/t_step]; y_r_d=[diff(y_r)/t_step; (y_r(end)-y_r(end-1))/t_step];
x_r_dd=[diff(x_r_d)/t_step; (x_r_d(end)-x_r_d(end-1))/t_step]; y_r_dd=[diff(y_r_d)/t_step; (y_r_d(end)-y_r_d(end-1))/t_step];
theta_r=unwrap(atan2(y_r_d,x_r_d));
v_r=( x_r_d.^2 + y_r_d.^2 ).^.5;
v_r_d=[diff(v_r)/t_step; (v_r(end)-v_r(end-1))/t_step];
w_r=( (x_r_d).*(y_r_dd.^2) - (y_r_d.^2).*(x_r_dd) )./ ...
    ( x_r_d.^2 + y_r_d.^2 );
w_r_d=[diff(w_r)/t_step; (w_r(end)-w_r(end-1))/t_step];
L=0.115;
phi_r=atan(L*(w_r./v_r));

if ~fancy_gui(max(v_r),rad2deg(max(w_r)),rad2deg(max(atan(0.115*(w_r./v_r)))))
    close(h_path)
    return
end

x_hist=nan(size(x_r_d)); y_hist=nan(size(x_r_d)); theta_hist=nan(size(x_r_d));
time_hist=nan(size(x_r_d));
v_hist=nan(size(x_r_d)); v_hist(1:2)=0;
v_estimate=v_hist; w_hist=v_hist; w_estimate=v_hist; phi_hist=v_hist;

[x_q,y_q,theta_q,t_init]=get_pose_qualisys(qtm); t_stamp=t_init;
if theta_q<0
    theta_q=theta_q+2*pi;
end
x_i=[x_q y_q theta_q];
x_curr=x_i;
x_prev=x_curr;

current_elapsed=0;

for i=1:size(t)
    
    tic
    
    real_i=round(((t_stamp-t_init)+t_step-current_elapsed)/t_step);
    if real_i==0
        real_i=1; disp('Rounded to zero!')
    end
    if real_i>size(t)
        real_i=size(t); disp('Rounded to end!')
        break
    end
    % Save current postion for historic purposes
    x_hist(i)=x_curr(1); y_hist(i)=x_curr(2); theta_hist(i)=x_curr(3);
    theta_temp=unwrap(theta_hist(1:i));
    x_curr(3)=theta_temp(end);
    theta_hist(i)=x_curr(3);
    time_hist(i)=t_stamp-t_init;
    
    % Estimate velocities and steering angle
    if i>1
        v_current=norm(x_curr(1:2)-x_prev(1:2)); v_current=v_current/(time_hist(i)-time_hist(i-1));
        v_estimate(i)=v_current; v_hist(i)=median(v_estimate(max(1,i-2):max(1,i)));

        w_current=x_curr(3)-x_prev(3); w_current=w_current/(time_hist(i)-time_hist(i-1));
        w_estimate(i)=w_current; w_hist(i)=median(w_estimate(max(1,i-2):max(1,i)));

        phi_hist(i)=atan(L*(w_hist(i)/v_hist(i)));
    end
        
    % Compute error and control
    v_current=v_estimate(i); phi_current=phi_hist(i);
    if i>1
        v_current_d=(v_estimate(i)-v_estimate(i-1))/(time_hist(i)-time_hist(i-1));
    else
        v_current_d=0;
    end
    error=tracking_error_SMC(x_curr,[x_r(i) y_r(i) theta_r(i)]);
    error_d=error_dynamics(error,v_r(i),phi_r(i),v_current,phi_current);
    
    s1=sliding_surface_1(error,error_d,K);
    s2=sliding_surface_2(error,error_d,K);
    
    if i>1
        u=SMC_controller(error,error_d,w_r_d(i),w_r(i),...
            v_current,v_r_d(i),v_current_d,s1,s2,K,P,Q,(time_hist(i)-time_hist(i-1)));
    else
        u=SMC_controller(error,error_d,w_r_d(i),w_r(i),...
            v_current,v_r_d(i),v_current_d,s1,s2,K,P,Q,t_step);
    end
    
%     disp([u(1) rad2deg(u(2))]);
    
%     V_volt=velocity2voltage(u(1)); w_volt=steering2voltage(u(2));
%     V_volt=velocity2voltage_no_stop(u(1)); w_volt=steering2voltage(u(2));
    V_volt=velocity2voltage_handle(u(1)); w_volt=steering2voltage(u(2));

    if u(1)<=0
        disp('No movement forward')
    end
    
    %     Send command to truck
    NI_voltage_output(s,V_volt,w_volt);
    
    x_prev=x_curr;
    %     Get current position
    [x_q,y_q,theta_q,t_stamp]=get_pose_qualisys(qtm);
    
    if ~isnan(x_q) % Handle possible invalid values
        if theta_q<0 % Put theta in range [0,2*pi]
            theta_q=theta_q+2*pi;
        end
        x_curr=[x_q y_q theta_q];
    else % If invalid reading, use previous reading
        x_curr=x_prev;
        disp('Invalid Qualisys') % Issue warning to the user
        plot(x_curr(1),x_curr(2),'kx','Linewidth',10)
    end

    % Plot stuff
    if real_i>=2
        plot([x_r(real_i-1) x_r(real_i)],[y_r(real_i-1) y_r(real_i)],'g')
        delete(h_pioneer);
        delete(h_ref);
    end
    plot([x_prev(1) x_curr(1)],[x_prev(2) x_curr(2)],'r')
    h_ref=plot(x_r(real_i),y_r(real_i),'go');
    h_pioneer=plot(x_curr(1),x_curr(2),'ro');

    % Sampling time stuff
    current_elapsed=toc;
    pause(t_step-toc)
    
end

NI_voltage_stop(s)

if exist('qtm','var')
    % Close connection
    QMC(qtm, 'disconnect');
    clear mex
end

existing_mats=dir('*.mat'); existing_mats=length(existing_mats);
save_str=strcat('SMC_parameter_config_num_',num2str(existing_mats+1),'.mat');
save(save_str);