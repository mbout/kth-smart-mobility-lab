function [ PID_struct_longitudinal_vector, PID_struct_lateral_vector,...
    x_r_vector, y_r_vector, theta_r_vector, velocity_r_vector, theta_hist_vector,...
    isPerformingVector, t_stopped_vector, t_init_vector, d_s ]...
    = createTrajectoryReferencesPathMode(...
    trajectoryToPerform, truckNumber, t_step, t_stamp, ...
    PID_struct_longitudinal_vector,PID_struct_lateral_vector,...
    x_r_vector, y_r_vector, theta_r_vector, velocity_r_vector, theta_hist_vector,...
    isPerformingVector, t_stopped_vector, t_init_vector )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

        %%% START HERE

        delta_x=diff(trajectoryToPerform(:,1)); delta_y=diff(trajectoryToPerform(:,2));

        x=trajectoryToPerform(:,1);
        y=trajectoryToPerform(:,2);
        velocity=trajectoryToPerform(:,3);

        theta = atan2(delta_y, delta_x);
        theta = [theta(1); theta];
                
        theta_hist=nan(size(x));
        time_hist=nan(size(x));
        
        d_s=5/32;
        % Best configuration so far

        %Velocity PID
        Kp_lo=2.0; Ki_lo=0.0;Kd_lo=0.0;
        PID_struct_longitudinal_vector{truckNumber} = PID_init(Kp_lo,Ki_lo,Kd_lo);
        %Lateral PID
        Kp_la=2.0;Ki_la=0.0;Kd_la=10.0;
        PID_struct_lateral_vector{truckNumber} = PID_init(Kp_la,Ki_la,Kd_la);
        
        
        x_r_vector{truckNumber} = x;
        y_r_vector{truckNumber} = y;
        theta_r_vector{truckNumber} = theta;
        velocity_r_vector{truckNumber} = velocity;
        
%         theta_r_vector{truckNumber} = theta_r; % Not used in this
%         controller
        theta_hist_vector{truckNumber} = theta_hist;
        isPerformingVector(truckNumber) = true;
        t_stopped_vector(truckNumber) = 0;
        t_init_vector(truckNumber) = t_stamp;
        

end

