function [V_volt]=velocity2voltage_no_stop_higher(vel)
%VELOCITY2VOLTAGE Summary of this function goes here
%   Detailed explanation goes here
% Based on velocity_calib_full_battery.mat

P=[0.534963996475386  -0.999061642207099];

V_volt=(vel-P(2))/P(1);

if vel<0
    V_volt=1.6;
    return
end

if vel<0.15
    V_volt=2.147923317789047;
end
if vel>0.6
    V_volt=3.0;
end

end

