function [xx,yy] = ps_winner_path(best_path,string_individual,num_waypoints)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

phi=deg2rad(25);

L=0.115;
r=L/tan(phi);
stepSize=0.01;

waypoints=[best_path(1,:);best_path(find(string_individual)+1,:);best_path(end,:)];

new_path=[];

for i=2:size(waypoints,1)
    
    current_path=dubins(waypoints(i-1,:),waypoints(i,:),r,stepSize);
    
    if size(path,2)==1
        stepSize=0.001; %o.1
        current_path=dubins(waypoints(i-1,:),waypoints(i,:),r,stepSize);
        disp('Needed to reduce Dubins stepsize')
    end
    
    new_path=[new_path;current_path'];
    
end

xx=new_path(:,1); yy=new_path(:,2);
delta_x=diff(xx); delta_y=diff(yy);
distance_traveled=(delta_x.^2 + delta_y.^2).^.5;
distance_traveled=sum(distance_traveled);

stepSize=distance_traveled/num_waypoints;

new_path=[];

for i=2:size(waypoints,1)
    
    current_path=dubins(waypoints(i-1,:),waypoints(i,:),r,stepSize);
    
    if size(path,2)==1
        stepSize=0.001; %o.1
        current_path=dubins(waypoints(i-1,:),waypoints(i,:),r,stepSize);
        disp('Needed to reduce Dubins stepsize')
    end
    
    new_path=[new_path;current_path'];
    
end

xx=new_path(:,1); yy=new_path(:,2);

% plot(xx,yy,'k','LineWidth',4)

end

