function [ velocity_profile ] = compute_velocity_profile( trajectory )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

    inverted = false;
    velocity_tuning = 15.0;
    number_start_points = 20;

    if ( size(trajectory, 1) < size(trajectory, 2) )
        
        inverted = true;
        
        trajectory = trajectory';
        
    end

    delta_x = diff( trajectory(:,1) );
    delta_y = diff( trajectory(:,2) );
    
    delta_theta = atan2(delta_y, delta_x);
    
    delta_theta = 1 + abs( delta_theta );
    
    delta_s = ( delta_x.^2 + delta_y.^2 ).^.5;
    
    delta_s = velocity_tuning * delta_s;
    
    velocity_profile = delta_s./abs(delta_theta);
        
    velocity_profile = [velocity_profile; velocity_profile(end)];
    
    accelerating_ratio = 0:1/(number_start_points-1):1;
    accelerating_ratio = accelerating_ratio';
    
    velocity_profile(1:number_start_points) = ...
        velocity_profile(1:number_start_points).*accelerating_ratio;
    
    velocity_profile(end-number_start_points+1:end) = ...
        velocity_profile(end-number_start_points+1:end).*flipud(accelerating_ratio);
    
    if ( inverted )
        
        trajectory = trajectory';
        velocity_profile = velocity_profile';
        
    end
    
end

