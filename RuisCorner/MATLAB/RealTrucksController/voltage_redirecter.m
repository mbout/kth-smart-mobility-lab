function [ redirected_voltage ] = voltage_redirecter( V_volt,w_volt,bodyId )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

    disp( strcat( 'voltage_redirecter is DEPRECATED function, you are probably ',...
    'looking for the voltage_redirecter_summation function' ) );

    redirected_voltage = [1.7 1.7 1.7 1.7 1.7 1.7];

    if bodyId == 3
        
        redirected_voltage = [V_volt w_volt 1.7 1.7 1.7 1.7];
        
    end
    
    if bodyId == 2
        
        
        redirected_voltage = [1.7 1.7 V_volt w_volt 1.7 1.7];
        
    end
    
    if bodyId == 12
        
        redirected_voltage = [1.7 1.7 1.7 1.7 V_volt w_volt];
        
    end
    
    
        
 

end

