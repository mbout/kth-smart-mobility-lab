function [ PID_struct_longitudinal_vector, PID_struct_lateral_vector, ...
    t_stopped_vector, voltage_vector, isPerformingVector] =...
    truckControlLoopPathMode...
    ( truckNumber, t_stopped_vector, t_init_vector, t_stamp, x_curr_vector,...
    x_r_vector, y_r_vector, theta_r_vector, velocity_r_vector, PID_struct_longitudinal_vector, PID_struct_lateral_vector, isPerformingVector,...
    controlledBodyIds, d_s, mean_velocity, t_step, vehicle_readings, velocity2voltage_handle, socket)
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here

    if isPerformingVector(truckNumber)
       
        t_stopped = t_stopped_vector(truckNumber);
        t_init = t_init_vector(truckNumber);
        measurements = x_curr_vector{truckNumber};
        x_curr = measurements(1, :);
        
        disp( strcat( 'x_curr = ' , num2str( x_curr ) ) )
        
        num_samples_velocity = 4;
        
        x_r = x_r_vector{truckNumber};
        y_r = y_r_vector{truckNumber};
        theta_r = theta_r_vector{truckNumber};
        velocity_r = velocity_r_vector{truckNumber};
        
        PID_struct_longitudinal = PID_struct_longitudinal_vector{truckNumber};
        PID_struct_lateral = PID_struct_lateral_vector{truckNumber};

    %     theta_hist = theta_hist_vector(:, truckNumber);
        distance_ahead = 0.05;
        reference_point = [x_curr(1) + distance_ahead*cos(x_curr(3)), x_curr(2) + distance_ahead*sin(x_curr(3))];
    
        disp( strcat( 'reference_point = ' , num2str( reference_point ) ) )
        
        delta_x = x_r - reference_point(1);
        delta_y = y_r - reference_point(2);
        distance_to_ref_point = (delta_x.^2 + delta_y.^2).^.5;
        
        [~, real_i] = min(distance_to_ref_point);
        
        try

            % Save current theta for unwrap purposes
    %         theta_hist_vector(iteration_counter)=x_curr(3);
    %         theta_temp=unwrap(theta_hist_vector(1:iteration_counter));
    %         x_curr(3)=theta_temp(end);
    %         theta_hist_vector(iteration_counter, truckNumber)=x_curr(3);
    %         time_hist_vector(iteration_counter, truckNumber)=t_stamp-t_init;
            states = [];
            
            for i = 1:length( vehicle_readings )
                
                if controlledBodyIds( truckNumber ) == vehicle_readings{i, 1}.id
                    
                    states = vehicle_readings{i, 1}.readings;
                    
                end
                
            end
            
            cars_being_seen = size(states, 1);
%             if cars_being_seen ~= 0
%                
%                 disp( strcat( 'Truck number ' , num2str(truckNumber),...
%                     ' is seeing ' , num2str(cars_being_seen), ' cars.') )
%                 
%             end
    
    
            collision_distance = get_collision_distance(states, x_curr, [x_r, y_r], real_i);

            disp(strcat('num2str(x_r(real_i) = ',num2str(x_r(real_i) )))
            disp(strcat('num2str(y_r(real_i) = ',num2str(y_r(real_i) )))
            
            [x_r_rot,y_r_rot]=rotate_trajectory(x_curr,x_r,y_r);
            [x_car_rot,y_car_rot]=rotate_trajectory(x_curr,x_curr(1),x_curr(2)); % Simply rotates the car coordinates
%             longitudinal_error=x_r_rot(real_i)-x_car_rot;
%             [u_longitudinal,PID_struct_longitudinal]=...
%                 PID_control(PID_struct_longitudinal,longitudinal_error,t_step);

            desired_velocity = velocity_r(real_i);
            
            last_xs = measurements(1:num_samples_velocity, 1);
            last_ys = measurements(1:num_samples_velocity, 2);
            last_ts = measurements(1:num_samples_velocity, 4);
            
            disp( 'measurements = ' )
            disp( measurements )
            
            velocities = ((diff(last_xs).^2 + diff(last_ys).^2).^.5)./diff(last_ts);
                        
            disp( 'velocities = ' )
            disp( velocities )
            
            current_velocity = mean(velocities);

            disp( strcat( 'current_velocity = ', current_velocity ) )
            disp( strcat( 'desired_velocity = ', desired_velocity ) )
            
            longitudinal_error = desired_velocity - current_velocity;
            
            disp( strcat( 'longitudinal_error = ', longitudinal_error ) )
            
            [u_longitudinal,PID_struct_longitudinal]=...
                PID_control(PID_struct_longitudinal,longitudinal_error,t_step);
            
            
            e_1=y_r_rot(real_i)-y_car_rot;
            x_front=0; y_front=y_car_rot;
%             i_front=round(d_s/(t_step*mean_velocity));

            if real_i<=length(y_r_rot)
                e_2=y_r_rot(real_i)-y_front;
            else
                e_2=0;
            end
            
            lateral_error=e_1+e_2;
                        
            disp(strcat('lateral_error = ',num2str(lateral_error)))
            
            [u_lateral,PID_struct_lateral]=PID_control(PID_struct_lateral,lateral_error,t_step);

            u=[u_longitudinal u_lateral];

            [u, t_stopped] = adjustOutputToCollisionDistance( u, collision_distance, t_stopped, t_step );

            u=pioneer_command_to_truck_SML_command(u); % Command conversion from unicycle to car
            V_volt=velocity2voltage_handle(u(1)); w_volt=steering2voltage(u(2));

            if u(1)<=0
                V_volt = 1.7;
                disp('No movement forward')
            end

            terminate = check_termination_condition(x_curr, x_r, y_r);

            PID_struct_longitudinal_vector{truckNumber} = PID_struct_longitudinal;
            PID_struct_lateral_vector{truckNumber} = PID_struct_lateral;
            t_stopped_vector(truckNumber) = t_stopped;
            
%             x_rot(1000049840)
            
        catch errrrror
            
            disp ('Attempted to access x_r_rot(380); index out of bounds because numel(x_r_rot)=379.');
            disp('BEING DUMB, CORRECT CATCH STATEMENT')
            
            terminate = true;
            V_volt = 1.70; w_volt = 1.70;
            
            rethrow(errrrror)
            
        end
        
        if terminate
            
           disp('Terminate condition = true') 
           isPerformingVector(truckNumber) = false;
           sendCompletionMessageToSocket( socket, controlledBodyIds(truckNumber) );
%            disp('Will create completion xml string ')
%            outputString = javaCreateCompletionMessage( controlledBodyIds(truckNumber) );
%            disp('Will send output string: ')
%            disp(outputString)
%            javaSendStringOverSocket(socket, outputString);           
            
        end
        
        
        
    else
        
        doNotTurnOffVoltage = 0.2;
        
        if rand() > 0.98
            
            doNotTurnOffVoltage = -doNotTurnOffVoltage;
            
        end
        
        V_volt = 1.70; w_volt = 1.70 + doNotTurnOffVoltage;
        
    end
        
            

%     voltage_vector = voltage_redirecter(V_volt,w_volt,bodyId);
    bodyId = controlledBodyIds(truckNumber);
    voltage_vector = voltage_redirecter_summation( V_volt, w_volt, bodyId );
    
    
    



end

