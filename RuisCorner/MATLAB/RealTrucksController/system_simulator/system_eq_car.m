function [ x_dot ] = system_eq_car( x , u )
% SYSTEM_EQ Page 724 of LaValle

if length(u)~=2
    error('Input command with wrong dimmention')
end
if length(x)~=3
    error('Input state with wrong dimmention')
end

L=3.875;

x_dot=x;
phi=u(2);

x_dot(1)=cos(x(3))*u(1);
x_dot(2)=sin(x(3))*u(1);
x_dot(3)=tan(phi)*(u(1)/L);

end