function [ x ] = system_sim_car( x_i , u , t )
%SYSTEM_SIM Summary of this function goes here
%   Detailed explanation goes here

%     t_step=t/10;
%     t_lapse=0:t_step:t;
%     x=repmat(x_i,length(t_lapse),1);
%     
%     
%     for i=2:length(t_lapse)
%         
%         x_dot=system_eq(x(i-1,:),u);
%         x(i,:)=euler_integration(x(i-1,:),x_dot,t_step);
%         x(i,3)=rem(x(i,3),2*pi);

%     end
t_step=t;
x_dot=system_eq_car(x_i,u);
x=euler_integration(x_i,x_dot,t_step);
x(3)=rem(x(3),2*pi);

end

