function [ in_obstacle ] = point_in_obstacle( Obs, alpha )
%POINT_IN_OBSTACLE Summary of this function goes here
%   Detailed explanation goes here

in_obstacle=0;

for obstacle_id=1:length(Obs)
    
    points_obstacle=Obs{obstacle_id};
    
    inpolygon(alpha(1),alpha(2),...
        points_obstacle(:,1), points_obstacle(:,2));
    
    if inpolygon(alpha(1),alpha(2),...
            points_obstacle(:,1), points_obstacle(:,2));
        
        in_obstacle=1;
        break
        
    end
    
end

end