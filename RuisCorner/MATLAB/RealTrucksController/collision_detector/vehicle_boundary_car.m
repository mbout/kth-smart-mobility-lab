function [ Vehicle ] = vehicle_boundary_car( x )
%VEHICLE_BOUNDARY Summary of this function goes here
%   Detailed explanation goes here

Vehicle={};

L=2;

car_pos=[x(1);x(2)];

car_bounds=[-0.5 0.5+L 0.5+L -0.5; 0.5 0.5 -0.5 -0.5];

R=[cos(x(3)) -sin(x(3));...
    sin(x(3)) cos(x(3))];

car_bounds=R*car_bounds;

car_bounds=car_bounds+repmat(car_pos,[1 4]);

Vehicle{1}=car_bounds;

end

