function targetStartPointIndex = newTargetPointIndex(path,targetStartPointIndex,nrPointsInSearch, searchPoint)

% --- determining the path segment that will be used in the search for a
% new target point
maxPathIndex = size(path,1);
if(targetStartPointIndex <= maxPathIndex - nrPointsInSearch + 1)
    pathSegment = path(targetStartPointIndex: targetStartPointIndex + nrPointsInSearch - 1, :);
else
    pathSegment = path(targetStartPointIndex:maxPathIndex, :);
end

% --- computing the new target start point based on the euclidean distance
dist2SearchPoint = zeros(size(pathSegment(:,1)'));
for i = 1:size(pathSegment,1)
    xDist = abs(searchPoint(1) - pathSegment(i,1));
    yDist = abs(searchPoint(2) - pathSegment(i,2));
    dist2SearchPoint(i) = sqrt(xDist^2 + yDist^2);
end

[~, minIndex] = min(dist2SearchPoint);
targetStartPointIndex = targetStartPointIndex + minIndex - 1;
if(targetStartPointIndex == maxPathIndex)
    targetStartPointIndex = maxPathIndex - 1;
end

end