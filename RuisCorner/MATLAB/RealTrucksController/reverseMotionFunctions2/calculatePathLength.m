function pathLength = calculatePathLength(path)

nrPathPoints = size(path,1);
pathLength = 0;
for i = 1:nrPathPoints-1
    p1 = path(i,1:2);
    p2 = path(i+1,1:2);    
    pointDist = pdist([p1;p2],'euclidean');
    pathLength = pathLength + pointDist;       
end
