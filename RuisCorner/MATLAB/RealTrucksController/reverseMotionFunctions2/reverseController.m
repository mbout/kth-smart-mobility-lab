% The controller function
% Input: paramStruct - a struct containing the parameters needed to run the
%        controller.
%        tractorPose = [x y orientation] (1x4) 
%        trailerPose = [x y orientation] (1x4)  
%        path = [x y orientation curvature] (4xN)
%        obstacles = [C1; C2; C3;... CN] (4x2N) {C = [x y] (4x2)}
%        tStamps = [tStamp1 tStamp2] (1x2)
%        verbose - 0 to not show anything and 1 to show the control
%        procedure (1x1)

% output: steeringVoltage (1x1)
%         velocityVoltage (1x1)
%         reachedGoal - Stating whether the tractor-trailer has
%         reached the goal point or not (1x1)
%         paramStruct - a struct containing all the parameters that are
%         needed for the coming call of the function

function [steeringVoltage, velocityVoltage, reachedGoal, paramStruct] = reverseController...
    (paramStruct, tractorPose, trailerPose, path, obstacles, tStamps, verbose)

reachedGoal = 0;
% ------ Tractor trailer specific paramteres
L1 = 0.098;
L2 = 0.170;
steeringMax = deg2rad(22);

% --- Computing maximum hitching angle for avoiding jackknife effect
epsilonAngle = deg2rad(9); % Small value to assist the computations
if steeringMax <= atan(L1/L2)  % if tere is a critical hitch angle
    hitchAngleCritical = asin(L2*tan(steeringMax)/L1);
    hitchAngleMax = hitchAngleCritical - epsilonAngle; % Just below the critical angle to be able to straighten the vehicle at maximum hitch angle
else
    hitchAngleMax = deg2rad(90); % Never a need to go beyond 90 degrees
end

% ------ sample time
sampleTime = (tStamps(2)-tStamps(1));
% sampleTime = 0.1;

% ----- Types driving voltages
voltageMax = 3.2;
voltageMin = 0;
velocityStillVoltage = 1.85;
velocityBackVoltage = paramStruct.velocityBackVoltage;
steeringStraightVoltage = paramStruct.steeringStraightVoltage;
initialVelocityVoltage = velocityBackVoltage;
velocityVoltage = paramStruct.velocityVoltage;

% ----- Control Parameters
Kp_steering = 2;
Ki_steering = .5;
% Kp_steering = 3.1;
% Ki_steering = 5.5;
integralErrorSteering = paramStruct.integralErrorSteering;

Kp_hitchAngle = 2;
Ki_hitchAngle = .05;
integralErrorHitchAngle = paramStruct.integralErrorHitchAngle;
integralErrorHitchAngleMax = 0.1;

% Ky = 3.77;
% Kth = 1.90; 
% Kc = .20;
% maxCurveError = .9;
Ky = 3.5;
Kth = 1.88; 
Kc = .135;
maxCurveError = 1;


% ----- space and margins
qSide = 2;
space = [-qSide qSide -qSide qSide];
obstacleMargin = 0.07;

% --- Path related matters
targetStartPointIndex = paramStruct.targetStartPointIndex;
nrPathPoints = size(path,1);
pathEndPointIndex = nrPathPoints ;
nrPointsInSearch = round(nrPathPoints/10);

searchPointDist = 0.1;
brakingDist = 0.1;
inRangeDist = 0.2;

% --- Look ahead number of points
point1 = path(1,1:2);
point2 = path(2,1:2);
pointDist = pdist([point1;point2],'euclidean');
lookAheadTime = 2.2;
velocityRef = -0.1;
lookAheadNrPoints = round(abs(velocityRef)*lookAheadTime/pointDist);


% ----- Filtering parameters
nrPrevData = paramStruct.nrPrevData;
nrPrevErrors = paramStruct.nrPrevErrors;

% ----- Running the tractor
xt = tractorPose(1);
yt = tractorPose(2);
theta1 = tractorPose(3);
xc = trailerPose(1);
yc = trailerPose(2);
theta2 = trailerPose(3);

% --- Retrieving information from the struct
xtPrev = paramStruct.xtPrev;
ytPrev = paramStruct.ytPrev;
theta1Prev = paramStruct.theta1Prev;
xcPrev = paramStruct.xcPrev;
ycPrev = paramStruct.ycPrev;
theta2Prev = paramStruct.theta2Prev;

velocityVectorPrev = paramStruct.velocityVectorPrev;
angularVelocityVectorPrev = paramStruct.angularVelocityVectorPrev;
steeringVectorPrev = paramStruct.steeringVectorPrev;

error_yTildeVectorPrev = paramStruct.error_yTildeVectorPrev;
error_theta2VectorPrev = paramStruct.error_theta2VectorPrev;
error_curveVectorPrev = paramStruct.error_curveVectorPrev;

% --- Checking sensor errors
if ((xt == 0)&&(yt==0)&&(theta1==0))||(isnan(xt) && isnan(yt) && isnan(theta1))
    disp ('Sensor Reading Error - Truck')
    xt = xtPrev;
    yt = ytPrev;
    theta1 = theta1Prev;
end

if ((xc==0)&&(yc==0)&&(theta2==0))||(isnan(xc) && isnan(yc) && isnan(theta2))
    disp ('Sensor Reading Error - Trailer')
    xc = xcPrev;
    yc = ycPrev;
    theta2 = theta2Prev;
end

searchPoint = [xc - searchPointDist*cos(theta2), yc - searchPointDist*sin(theta2)];

% ----- Updating image tractor-trailer and path
if verbose > 0
    clf, hold on  
    obstacleRegions = getObstacleRegions(obstacles, obstacleMargin);
    drawObstacles(obstacleRegions, 'y')
    drawObstacles(obstacles,'k')
    drawTractorTrailer([xc,yc,theta1,theta2],space,path,searchPoint);
    drawnow
end
% ----- computing hitchangle
hitchAngle = theta1-theta2;
if (abs(hitchAngle) > pi)
    hitchAngle = hitchAngle - sign(theta1)*2*pi;
end

% ----- Computing the velocity of the tractor
xDelta = xt - xtPrev;
yDelta = yt - ytPrev;
speed = sqrt(xDelta^2 + yDelta^2)/sampleTime;

disp( strcat( 'speed = ' ,num2str( speed ) ) )

if(velocityVoltage > velocityStillVoltage)
    velocity = speed;
elseif(velocityVoltage < velocityStillVoltage)
    velocity = -speed;
else
    velocity = 0;
end

% ----- Computing angular velocity
thetaDelta = theta1 - theta1Prev;
if(abs(thetaDelta)> pi)  % Assuming orientation doesn't change pi over one sample
    if(theta1Prev < 0)
        theta1Prev = theta1Prev + 2*pi;
    else
        theta1 = theta1 + 2*pi;
    end
    thetaDelta = theta1 - theta1Prev;
end
angularVelocity  = thetaDelta/sampleTime;

% ----- Computing steering angle
if (angularVelocity==0 && velocity==0)
    steering = 0;
else
    steering = atan(angularVelocity*L1/velocity);
end

% ----- Filtering velocity, angular velocity and steering
steeringFiltered = median([steering,steeringVectorPrev]);

% --- computing the target start and end point
targetStartPointIndex = newTargetPointIndex(path,targetStartPointIndex,nrPointsInSearch, searchPoint);
targetEndPoint = path(targetStartPointIndex+1,:);
lookAheadPointIndex = min(targetStartPointIndex + lookAheadNrPoints, pathEndPointIndex);
lookAheadPoint = path(lookAheadPointIndex,:);

% --- Computing trajectory errors
error_x = targetEndPoint(1) - xc;
error_y = targetEndPoint(2) - yc;
error_xTilde = error_x*cos(theta2) + error_y*sin(theta2);
error_yTilde = -error_x*sin(theta2) + error_y*cos(theta2);

%     theta2Ref = atan((targetEndPoint(2) - targetStartPoint(2))/(targetEndPoint(1)- targetStartPoint(1)));
theta2Ref = targetEndPoint(3);
error_theta2 = theta2Ref - theta2;
if abs(error_theta2) > pi  % the orientation error should be computed in the direction in which it is smaller, it should be within [-pi, pi]
    error_theta2 = error_theta2 - sign(error_theta2)*2*pi;
end

error_curve = (lookAheadPoint(4) - tan(hitchAngle)/L2);
if abs(error_curve) > maxCurveError
    error_curve = sign(error_curve)*maxCurveError;
end

% ----- Filtering the errors
error_yTilde = mean([error_yTilde error_yTildeVectorPrev]);
error_theta2 = mean([error_theta2 error_theta2VectorPrev]);
error_curve = mean([error_curve error_curveVectorPrev]);

% --- Check if close enough to endpoint to start braking
if(targetStartPointIndex == pathEndPointIndex - 1)
    if(abs(error_xTilde) <= brakingDist && abs(error_yTilde) <= inRangeDist)  % decrease velocity when closing into the endpoint of the path.
        velocityVoltage = velocityStillVoltage -(velocityStillVoltage - initialVelocityVoltage)*abs(error_xTilde)/brakingDist;
    end
end

% ----- Trajectory stablization controller
hitchAngleRef = Ky*error_yTilde - Kth*error_theta2 + Kc*error_curve;
if abs(hitchAngleRef) > hitchAngleMax
    hitchAngleRef = sign(hitchAngleRef)*hitchAngleMax;
end

% ----- hitching angle controller
hitchAngleError = hitchAngleRef - hitchAngle;
hitchAngleErrorMod = (Kp_hitchAngle*L2 - L1)/(Kp_hitchAngle*L2)*hitchAngleRef - hitchAngle;
integralErrorHitchAngle = integralErrorHitchAngle + hitchAngleError*sampleTime;
if abs(integralErrorHitchAngle) > integralErrorHitchAngleMax
    integralErrorHitchAngle = sign(integralErrorHitchAngle)*integralErrorHitchAngleMax;
end
steeringRef = -(Kp_hitchAngle*hitchAngleErrorMod + Ki_hitchAngle*integralErrorHitchAngle);

% --- Jackknife constraint
if(hitchAngle >= hitchAngleMax - epsilonAngle && steeringRef < atan(L1*sin(hitchAngleMax)/L2))
    steeringRef = atan(L1*sin(hitchAngleMax)/L2);
elseif (hitchAngle <= -hitchAngleMax + epsilonAngle && steeringRef > -atan(L1*sin(hitchAngleMax)/L2))
    steeringRef = -atan(L1*sin(hitchAngleMax)/L2);
end

% ----- Steering constraints
if abs(steeringRef) > steeringMax
    steeringRef = sign(steeringRef)*steeringMax;
end

% -----  steering controller
errorSteering = steeringRef - steeringFiltered;
integralErrorSteering = integralErrorSteering + errorSteering;
%     if abs(integralErrorSteering) > integralErrorMax
%         integralErrorSteering = sign(integralErrorSteering)*integralErrorMax;
%     end
steeringVoltage = steeringStraightVoltage - (Kp_steering*errorSteering + Ki_steering*integralErrorSteering);
steeringVoltage = min(voltageMax,steeringVoltage);
steeringVoltage = max(voltageMin,steeringVoltage);

% --- Stop simulation if reached goal or pressed stop button
if(abs(velocityStillVoltage-velocityVoltage) < 0.4)
    reachedGoal = 1;
end

% ----- Storing values in paramStruct for next run
paramStruct.integralErrorSteering = integralErrorSteering;
paramStruct.integralErrorHitchAngle = integralErrorHitchAngle;
paramStruct.targetStartPointIndex = targetStartPointIndex;
paramStruct.xtPrev = xt;
paramStruct.ytPrev = yt;
paramStruct.theta1Prev = theta1;
paramStruct.xcPrev = xc;
paramStruct.ycPrev = yc;
paramStruct.theta2Prev = theta2;
paramStruct.velocityVectorPrev = [velocityVectorPrev(2:nrPrevData) velocity];
paramStruct.angularVelocityVectorPrev = [angularVelocityVectorPrev(2:nrPrevData) angularVelocity];
paramStruct.steeringVectorPrev = [steeringVectorPrev(2:nrPrevData) steering];
paramStruct.velocityVoltage = velocityVoltage;
paramStruct.error_yTildeVectorPrev = [error_yTildeVectorPrev(2:nrPrevErrors)  error_yTilde];
paramStruct.error_theta2VectorPrev = [error_theta2VectorPrev(2:nrPrevErrors) error_theta2];
paramStruct.error_curveVectorPrev = [error_curveVectorPrev(2:nrPrevErrors) error_curve];

end