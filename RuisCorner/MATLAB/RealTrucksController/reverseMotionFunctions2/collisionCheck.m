function collision = collisionCheck(pose, obstacles, collisionMargin)

if isempty(obstacles)
    collision = 0;
   return 
end
[tractorCorners, trailerCorners, ~]  = tractorTrailerPoints(pose,collisionMargin);
nrObstacles = size(obstacles,2)/2;

for i=1:nrObstacles
    obsIndex = 2*i-1;
    obstacle = obstacles(:,obsIndex:obsIndex+1);
    [xTractor, ~] = polybool('and', tractorCorners(:,1), tractorCorners(:,2),obstacle(:,1),obstacle(:,2));
    [xTrailer, ~] = polybool('and', trailerCorners(:,1), trailerCorners(:,2),obstacle(:,1),obstacle(:,2));
    
    if (isempty(xTractor) && isempty(xTrailer))
        collision = 0;
    else
        collision =1;
        return
    end
end