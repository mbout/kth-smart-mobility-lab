function intersectionPoint = pathObstacleIntersection(path, obstacles)

if isempty(obstacles)
    intersectionPoint = [];
    return
end

nrObstacles = size(obstacles,2)/2;

for i = 1:nrObstacles
    obsIndex = 2*i-1;
    obstacle = obstacles(:,obsIndex:obsIndex+1);
    obstaclePoly = [obstacle; obstacle(1,:)];
    [X, Y] = intersections(path(:,1),path(:,2),obstaclePoly(:,1),obstaclePoly(:,2),1);
    intersectionPoint = [X, Y];
    if ~isempty(intersectionPoint)        
        return
    end
end
