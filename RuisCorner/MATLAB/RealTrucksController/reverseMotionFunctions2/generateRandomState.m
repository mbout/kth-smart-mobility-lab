function randState = generateRandomState(space, obstacles)

minX = space(1);
maxX = space(2);
minY = space(3);
maxY = space(4);
collisionMargin = 0;
nrMaxIterations = 100;

for i = 1:nrMaxIterations
    randX = minX + (maxX-minX)*rand;
    randY = minY + (maxY-minY)*rand;
    randTheta = 2*pi*rand - pi;
    randState = [randX randY randTheta randTheta];
    collision = collisionCheck(randState, obstacles,collisionMargin);
    if ~collision 
       return 
    end
end

disp('Could not generate a random state. Work space might be too crowded with obstacles!')

end