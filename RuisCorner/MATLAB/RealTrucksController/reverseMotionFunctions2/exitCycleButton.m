function varargout = exitCycleButton(varargin)
% EXITCYCLEBUTTON MATLAB code for exitCycleButton.fig
%      EXITCYCLEBUTTON, by itself, creates a new EXITCYCLEBUTTON or raises the existing
%      singleton*.
%
%      H = EXITCYCLEBUTTON returns the handle to a new EXITCYCLEBUTTON or the handle to
%      the existing singleton*.
%
%      EXITCYCLEBUTTON('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in EXITCYCLEBUTTON.M with the given input arguments.
%
%      EXITCYCLEBUTTON('Property','Value',...) creates a new EXITCYCLEBUTTON or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before exitCycleButton_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to exitCycleButton_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help exitCycleButton

% Last Modified by GUIDE v2.5 09-Mar-2015 16:21:03

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @exitCycleButton_OpeningFcn, ...
                   'gui_OutputFcn',  @exitCycleButton_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before exitCycleButton is made visible.
function exitCycleButton_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to exitCycleButton (see VARARGIN)

% Choose default command line output for exitCycleButton
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes exitCycleButton wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = exitCycleButton_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global exitCycleButtonPushed
exitCycleButtonPushed = true;
