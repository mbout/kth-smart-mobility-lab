function [path, nrPathPoints, tree] = RRTpathPlanner(startPose, goalPose, obstacles, obstacleMargin, space, minTurningRadius,stepSize,parkingSpaceLength, maxPlanningTime)

obstacleRegions = getObstacleRegions(obstacles, obstacleMargin);
maxGrowth = 0.4;
greediness = 0;

figure, hold on
drawObstacles(obstacleRegions, 'y')
drawObstacles(obstacles, 'k')
drawTractorTrailer(startPose,space)
drawTractorTrailer(goalPose,space,[],[],1)

% --- Try first with pure dubin parking
[path, nrPathPoints] = pathPlannerDubinPark(startPose, goalPose, minTurningRadius, stepSize, parkingSpaceLength);
obstacleIntersection = pathObstacleIntersection(path, obstacleRegions);
withinBoundary = boundaryCheck(path,space, 0);
if isempty(obstacleIntersection) && withinBoundary
    plot(path(:,1),path(:,2),'r')    
    tree = [[startPose 0]; [goalPose 1]];
    return
end

% --- RRT
maxTreeNodes = 200;
tree = inf*ones(maxTreeNodes,5);
tree(1,:) = [startPose 0];

tic
for i=2:maxTreeNodes-1
    passedTime = toc;
    if passedTime >= maxPlanningTime
        break
    end
    findNewNode = 1;    
    while findNewNode
        if greediness >= rand
            randPose = goalPose;
        else
            randPose = generateRandomState(space,obstacleRegions);
        end
        [closestNode, indexClosestNode] = getClosestNode(tree, randPose, obstacleRegions, minTurningRadius, stepSize, maxGrowth, space,obstacleMargin);
        [newNode, path2NewNode] = growTreeNode(closestNode, indexClosestNode, randPose, obstacleRegions, minTurningRadius, stepSize, maxGrowth,space,obstacleMargin);
        if ~isempty(newNode)
            findNewNode = 0;
        end
    end
    tree(i,:) = newNode;
    plot(path2NewNode(:,1),path2NewNode(:,2))
    plot(newNode(1),newNode(2),'b*')
    drawnow
    [parkingPath, ~] = pathPlannerDubinPark(newNode, goalPose, minTurningRadius, stepSize, parkingSpaceLength);
    obstacleIntersection = pathObstacleIntersection(parkingPath, obstacleRegions);
    withinBoundary = boundaryCheck(parkingPath,space, 0);
    if isempty(obstacleIntersection) && withinBoundary
        tree(i+1,:) = [goalPose i];        
        plot(parkingPath(:,1),parkingPath(:,2))
        [path, pathNodes] = getPath2LastNode(tree, minTurningRadius, stepSize,parkingSpaceLength);
        plot(path(:,1),path(:,2),'r')
        [path, pathNodes] = optimizePath(pathNodes, obstacleRegions, space, minTurningRadius, stepSize,parkingSpaceLength, obstacleMargin);
        plot(path(:,1),path(:,2),'g')
        nrPathPoints = size(path,1);
        tree = cropMatrix(tree);
        return
    end
end

    path = [];
    nrPathPoints=0;
    disp('No path found!');

tree = cropMatrix(tree);