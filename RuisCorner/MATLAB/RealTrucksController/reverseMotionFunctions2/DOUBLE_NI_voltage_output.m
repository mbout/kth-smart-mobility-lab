function [v1,v2]=NI_voltage_output(s,v1,v2,vHitchLock)
%NI_VOLTAGE_OUTPUT Send voltage to NI outputs
%   It will limit the voltage to [0,3.2]
% Must limit the voltage to [0,3.2], to not fry the Scania Truck Remote
% Control. v1 = steering, v2 = velocity.
    v1=min(3.2,v1);
    v1=max(0,v1);
    v2=min(3.2,v2);
    v2=max(0,v2); 
    vHitchLock=min(3.2,vHitchLock);
    vHitchLock=max(0,vHitchLock);
    
    s.outputSingleScan([vHitchLock v1 v2 ]);    
end

