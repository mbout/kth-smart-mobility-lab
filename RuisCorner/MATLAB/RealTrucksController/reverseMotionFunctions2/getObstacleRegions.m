function obstacleRegions = getObstacleRegions(obstacles, margin)
if isempty(obstacles)
    obstacleRegions = [];
    return
end

diagMargin = sqrt(2)*margin;
nrObstacles = size(obstacles,2)/2;
obstacleRegions = zeros(size(obstacles));

for i=1:nrObstacles
    obsIndex = i*2-1;
    A = obstacles(1,obsIndex:obsIndex+1);
    B = obstacles(2,obsIndex:obsIndex+1);
    C = obstacles(3,obsIndex:obsIndex+1);
    D = obstacles(4,obsIndex:obsIndex+1);
    
    deltaX = D(1) - A(1);
    deltaY = D(2)- A(2);
    gamma = atan2(deltaY,deltaX);
    alpha = gamma + pi/4;
    
    A2 = A + diagMargin*[-cos(alpha) -sin(alpha)];
    B2 = B + diagMargin*[-sin(alpha) cos(alpha)];
    C2 = C + diagMargin*[cos(alpha) sin(alpha)];
    D2 = D + diagMargin*[sin(alpha) -cos(alpha)];
        
    obstacleRegions(:,obsIndex:obsIndex+1) = [A2; B2; C2; D2];
    
    end
