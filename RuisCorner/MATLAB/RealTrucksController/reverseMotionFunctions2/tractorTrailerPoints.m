function [tractorCorners, trailerCorners, hitchPoint]  = tractorTrailerPoints(states, margin)
% states
xc = states(1);
yc = states(2);
theta1 = states(3);
theta2 = states(4);

% Dimensions of tractor-trailer in [m]
trailerWidth = 0.088;
trailerLength = 0.280;
trailerBackPart = 0.04;
trailerHitchDistance = 0.028;

tractorWidth = 0.088;
tractorLength = 0.187;
tractorHitchDistance = 0.045;

% converting state points to endpoint
xe = xc - trailerBackPart*cos(theta2);
ye = yc - trailerBackPart*sin(theta2);

% trailer rectangle points
A = [xe + trailerWidth/2*sin(theta2), ye - trailerWidth/2*cos(theta2)];
B = [A(1) - trailerWidth*sin(theta2), A(2) + trailerWidth*cos(theta2)];
C = [B(1) + trailerLength*cos(theta2), B(2) + trailerLength*sin(theta2)];
D = [A(1) + trailerLength*cos(theta2), A(2) + trailerLength*sin(theta2)];

hitchPoint = (C+D)/2 - [trailerHitchDistance*cos(theta2) trailerHitchDistance*sin(theta2)];

% tractor rectangle points
A2 = [hitchPoint(1) - tractorHitchDistance*cos(theta1) + tractorWidth/2*sin(theta1),...
    hitchPoint(2) - tractorHitchDistance*sin(theta1) - tractorWidth/2*cos(theta1)];
B2 = [A2(1) - tractorWidth*sin(theta1), A2(2) + tractorWidth*cos(theta1)];
C2 = [A2(1) - tractorWidth*sin(theta1) + tractorLength*cos(theta1),...
    A2(2) + tractorWidth * cos(theta1) + tractorLength*sin(theta1)];
D2 = [A2(1) + tractorLength*cos(theta1), A2(2) + tractorLength*sin(theta1)];

if nargin > 1 && margin > 0
    marginDiag = sqrt(2)*margin;
    gamma = pi/4 + theta2;
    A = A + [-marginDiag*cos(gamma),-marginDiag*sin(gamma)];
    B = B + [-marginDiag*sin(gamma), marginDiag*cos(gamma)];
    C = C + [marginDiag*cos(gamma), marginDiag*sin(gamma)];
    D = D + [marginDiag*sin(gamma), -marginDiag*cos(gamma)];
    
    
    A2 = A2 + [-marginDiag*cos(gamma),-marginDiag*sin(gamma)];
    B2 = B2 + [-marginDiag*sin(gamma), marginDiag*cos(gamma)];
    C2 = C2 + [marginDiag*cos(gamma), marginDiag*sin(gamma)];
    D2 = D2 + [marginDiag*sin(gamma), -marginDiag*cos(gamma)];
end

trailerCorners = [A; B; C; D];
tractorCorners = [A2; B2; C2; D2];
end