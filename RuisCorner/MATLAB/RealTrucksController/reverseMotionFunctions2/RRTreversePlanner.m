% --- RRT wrapper function
% startPose = [xTrailerStart, yTrailerStart, thetaTruckStart, thetaTrailerStart]
% goalPose = [xTrailerGoal, yTrailerGoal, thetaTruckGoal, thetaTrailerGoal]
% obstacle = [C1; C2; C3; C4], C = [x y]. Give obstacle corners in clockwise order

function [path, nrPathPoints, tree] = RRTreversePlanner(startPose, goalPose, obstacles, maxPlanningTime)

obstacleMargin = 0.07;
qside = 2.5;
parkingSpaceLength = .5;
space = [-qside qside -qside qside];
stepSize = .01;
minTurningRadius = .3;
[path, nrPathPoints, tree] = RRTpathPlanner(startPose, goalPose, obstacles, obstacleMargin, space, minTurningRadius,stepSize,parkingSpaceLength, maxPlanningTime);

