function [closestNode, indexClosestNode] = getClosestNode(tree, randPose, obstacles, minTurningRadius, stepSize, maxGrowth,space, margin)

tree = cropMatrix(tree);
nrTreeNodes = size(tree,1);
nodeDist = zeros(nrTreeNodes, 1);
poseIndex = 1:4;
parentIndex = 5;
minDist2ChildNodes = 0.1;

for i = 1:nrTreeNodes
    currentNode = tree(i,:);
    [newNode, ~] = growTreeNode(currentNode(poseIndex), i,randPose, obstacles, minTurningRadius, stepSize, maxGrowth,space, margin);
    if isempty(newNode)
        nodeDist(i) = inf;            
    else
        % check if close to child nodes 
        childNodes = tree(tree(:,parentIndex) == i,:);
        close2ChildNodes = checkCloseness2Nodes(newNode, childNodes, minDist2ChildNodes);       
        if close2ChildNodes
            nodeDist(i) = inf;
        else
            dubinPath = pathPlannerDubin(currentNode(poseIndex), randPose, minTurningRadius, stepSize);
            nodeDist(i) = calculatePathLength(dubinPath);
        end
    end
end

[~, indexClosestNode] = min(nodeDist);

if nodeDist(indexClosestNode)== inf
    closestNode = [];
else
    closestNode = tree(indexClosestNode,:);
end             



