% This is a demonstration of the three functions used for the automated reverse
% driving system. 1. RRTreversePlanner(), 2. reverseControllerInit() 3.
% reverseController(). The first section of the file
% "experimentInitialization.m" must be run first before running this
% file. 
clc, close all
clear

qtm=QMC('QMC_conf_2.txt');

qualisys_truck_id = 22;
qualisys_trailer_id = 25;
box_1_id = 8; box_2_id = 9; box_3_id = 10; box_4_id = 11; box_M_id = 17;

% --- Obtaining the position of the tractor trailer and obstacles, and
% determining the goal pose.
[xt,yt,theta1,~] = get_pose_qualisys_id(qtm,qualisys_truck_id);
[xc,yc,theta2,~] = get_pose_qualisys_id(qtm,qualisys_trailer_id);
tractorPose = [xt,yt,theta1];
trailerPose = [xc,yc,theta2];
obstacles = getObstacles(qtm, box_1_id, box_2_id, box_3_id, box_4_id, box_M_id);

startPose = [xc yc theta1 theta2];
goalPose = [0 0 0 0];

% --- Planning the path
maxPlanningTime = 10;
path = RRTreversePlanner(startPose, goalPose, obstacles,maxPlanningTime);

return

%%
% --- Initializing the controller (obtaining the initial parameter struct)
paramStruct = reverseControllerInit(tractorPose, trailerPose);

global exitCycleButtonPushed   % Button for stopping the tractor
exitCycleButtonPushed = false;          
exitCycleButton   

reachedGoal = 0;
sampleTime = 0.1;

[xc,yc,theta2,tStamp] = get_pose_qualisys_id(qtm,qualisys_trailer_id);
tStamps = [0 tStamp];
pause(sampleTime)

% --- Running the tractor-trailer
while true
    tic
    [xt,yt,theta1,~] = get_pose_qualisys_id(qtm,qualisys_truck_id);
    [xc,yc,theta2,tStamp] = get_pose_qualisys_id(qtm,qualisys_trailer_id);
    tStamps = [tStamps(2) tStamp];
    tractorPose = [xt,yt,theta1];
    trailerPose = [xc,yc,theta2];
    
    [steeringVoltage, velocityVoltage, reachedGoal, paramStruct] = reverseController(paramStruct, tractorPose, trailerPose, path, obstacles, tStamps, 1);
    NI_voltage_output(s, steeringVoltage, velocityVoltage, 0);
    
    if reachedGoal||exitCycleButtonPushed  % --- Stopping when reaching the goal or when the stop button has been pushed
        NI_voltage_stop(s);
        break
    end
    computationTime = toc;
    pause(sampleTime-computationTime)
end