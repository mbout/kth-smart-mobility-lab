 function [path2LastNode, pathNodes]= getPath2LastNode(tree, minTurningRadius, stepSize,parkingSpaceLength)
tree = cropMatrix(tree);
pathNodes = inf*ones(size(tree));

nrTreeNodes = size(tree,1);
nodeIndex = nrTreeNodes;
i = 1;
while true 
    nodeInPath = tree(nodeIndex,:);
    pathNodes(i,:) = nodeInPath;
    
    nodeIndex = nodeInPath(5);
    if nodeIndex == 0
        break
    end
    i = i+1;
end

pathNodes = cropMatrix(pathNodes);
nrPathNodes = size(pathNodes,1);
pathNodes = flipud(pathNodes(1:nrPathNodes,:));

path2LastNode = inf*ones(10000,4);
minTurningRadius = minTurningRadius - 0.001;

for i = 1:nrPathNodes-2
    [pathBetweenNodes, nrPathPoints] = pathPlannerDubin(pathNodes(i,1:4),pathNodes(i+1,1:4), minTurningRadius, stepSize);        
    connectionIndex = find(path2LastNode(:,1) == inf,1);    
    path2LastNode(connectionIndex: connectionIndex + nrPathPoints - 1,:) = pathBetweenNodes;
end

[finalPathSegment, nrPathPoints] = pathPlannerDubinPark(pathNodes(end-1,1:4),pathNodes(end,1:4), minTurningRadius, stepSize,parkingSpaceLength);        
connectionIndex = find(path2LastNode(:,1) == inf,1);    
path2LastNode(connectionIndex: connectionIndex + nrPathPoints - 1,:) = finalPathSegment;
path2LastNode = cropMatrix(path2LastNode);



