function [s] = NI_initialization()
%NI_INITIALIZATION Summary of this function goes here
%   Detailed explanation goes here

devices=daq.getDevices;
% Create the session
s=daq.createSession('ni');
% Adding output channels

s.addAnalogOutputChannel(devices(1).ID,0,'Voltage');
s.addAnalogOutputChannel(devices(1).ID,2,'Voltage');
s.addAnalogOutputChannel(devices(1).ID,3,'Voltage');
end

