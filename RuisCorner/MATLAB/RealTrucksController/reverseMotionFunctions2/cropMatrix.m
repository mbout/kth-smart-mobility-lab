function croppedMatrix = cropMatrix(matrix)

nrRows = find(matrix == inf, 1) - 1;
if isempty(nrRows)
    croppedMatrix = matrix;
else
    croppedMatrix = matrix(1:nrRows,:);    
end
