function obstacles = getObstacles(obstacle_ids, qualisysReadingsStructure)

box_1_id = obstacle_ids.box_1_id;
box_2_id = obstacle_ids.box_2_id;
box_3_id = obstacle_ids.box_3_id;
box_4_id = obstacle_ids.box_4_id;
box_M_id = obstacle_ids.box_M_id;

% --- dimension of obstacles
boxLength = 0.6;
boxWidth = 0.4;
miniBoxLength = 0.4;
miniBoxWidth = 0.3;
miniBoxHalfDiag = sqrt(miniBoxLength^2 + miniBoxWidth^2)/2;
gamma = atan2(miniBoxWidth,miniBoxLength);


% --- get position of obstacles 
[xB1,yB1,thetaB1,~] = get_pose_qualisys_id(box_1_id, qualisysReadingsStructure); 
[xB2,yB2,thetaB2,~] = get_pose_qualisys_id(box_2_id, qualisysReadingsStructure);
[xB3,yB3,thetaB3,~] = get_pose_qualisys_id(box_3_id, qualisysReadingsStructure); 
[xB4,yB4,thetaB4,~] = get_pose_qualisys_id(box_4_id, qualisysReadingsStructure);
[xBM,yBM,thetaBM,~] = get_pose_qualisys_id(box_M_id, qualisysReadingsStructure);
beta = gamma + thetaBM;
xBM = xBM - miniBoxHalfDiag*cos(beta);
yBM = yBM - miniBoxHalfDiag*sin(beta);

% --- create obstacles
obs1 = createRectangleObstacle([xB1 yB1],boxLength,boxWidth,thetaB1);
obs2 = createRectangleObstacle([xB2 yB2],boxLength,boxWidth,thetaB2);
obs3 = createRectangleObstacle([xB3 yB3],boxLength,boxWidth,thetaB3);
obs4 = createRectangleObstacle([xB4 yB4],boxLength,boxWidth,thetaB4);
obs5 = createRectangleObstacle([xBM yBM],miniBoxLength,miniBoxWidth,thetaBM);
obstacles = [obs1 obs2 obs3 obs4 obs5];
