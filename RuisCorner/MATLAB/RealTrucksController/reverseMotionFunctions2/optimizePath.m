function [optimizedPath, pathNodesOpt] = optimizePath(pathNodes, obstacles, space, minTurningRadius, stepSize,parkingSpaceLength,obstacleMargin)

pathNodesOpt = pathNodes;
nrNodes = size(pathNodesOpt,1);
currentNodeIndex = 1;
destinationNodeIndex = nrNodes -1;
currentNode = pathNodesOpt(currentNodeIndex,:);
destinationNode = pathNodesOpt(destinationNodeIndex,:);

while currentNode ~= destinationNode
    while destinationNodeIndex >= currentNodeIndex + 2
        [path, ~] = pathPlannerDubin(currentNode, destinationNode, minTurningRadius, stepSize);
        intersectionPoint = pathObstacleIntersection(path, obstacles);
        withinSpace = boundaryCheck(path, space, obstacleMargin);
        if isempty(intersectionPoint) && withinSpace
            pathNodesOpt = pathNodesOpt([1:currentNodeIndex destinationNodeIndex:end],:);
            break;
        else
            destinationNodeIndex = destinationNodeIndex - 1;
            destinationNode = pathNodesOpt(destinationNodeIndex,:);
        end
    end     
    
    currentNodeIndex = currentNodeIndex + 1;    
    nrNodes = size(pathNodesOpt,1);
    destinationNodeIndex = nrNodes - 1;
    
    currentNode = pathNodesOpt(currentNodeIndex,:);
    destinationNode = pathNodesOpt(destinationNodeIndex,:);    
end

for i = 1:nrNodes
    pathNodesOpt(i,5) = i -1;    
end

[optimizedPath, ~]= getPath2LastNode(pathNodesOpt, minTurningRadius, stepSize,parkingSpaceLength);


% figure, hold on
% plot(optimizedPath(:,1), optimizedPath(:,2))
% for i = 1:nrNodes
%     plot(pathNodesOpt(i,1),pathNodesOpt(i,2),'*')
% end
% drawObstacles(obstacles)
% axis(space)
% 
