function withinSpace = boundaryCheck(path, space, margin)

xMin = space(1)+ margin;
xMax = space(2) - margin;
yMin = space(3) + margin;
yMax = space(4) - margin;

boundaryLine = [xMin yMin;xMin yMax;xMax yMax;xMax yMin;xMin yMin];
[X, Y] =intersections(path(:,1),path(:,2),boundaryLine(:,1),boundaryLine(:,2),1); 
intersectionPoint = [X, Y];
withinSpace = isempty(intersectionPoint);
