clear
close all

ipAddress = '130.237.50.246'; % Mine
% ipAddress = '130.237.43.135'; % Pedro's
% ipAddress = '130.237.43.176'; % Matteo's
port = 8000;

% Correspondence:
controlledBodyIds = [2, 3, 12];

realRun = true;

% % INITIALIZATION STUFF
% if exist('qtm','var')
%     % Close connection
%     QMC(qtm, 'disconnect');
%     clear mex
% end
% 
% if ~exist('qtm','var')
%     % Open connection
%     disp('Opening connection to Qualisys...')
%     qtm=QMC('QMC_conf.txt');
% end

clear mex
disp('Opening connection to Qualisys...')
qtm=QMC('QMC_conf.txt');


if realRun

    if ~exist('s','var')
        s=NI_initialization();
        NI_voltage_stop(s)
    end

else

    s = -1;

end

states_figure_handler = figure;
hold on; axis(3*[-1 1 -1 1]);

while 1

    [trajectory, bodyId, socket] = connectAndGetTrajectory( ipAddress, port , s, controlledBodyIds, realRun);
    
    
    disp( strcat( 'Will make trajectory on ' , num2str(bodyId) ) )
    % disp(bodyId)

    qualisys_truck_id = bodyId;

    % return
    if realRun
        NI_voltage_stop(s)
    end

    % % disp('Received info:')
    % % disp(trajectory)
    % 
    % plot(trajectory(:,1), trajectory(:,2)) 

    % load sml_trajectory

    sml_trajectory = trajectory(:,1:2)/32.;

    addpath('collision_detector','drawing','map_creation',...
        'handle_loaders','misc','system_simulator','nearest','plot_graph');
    
    %%% START HERE
    
    delta_x=diff(sml_trajectory(:,1)); delta_y=diff(sml_trajectory(:,2));
    distance_to_travel=(delta_x.^2 + delta_y.^2).^.5;
    distance_to_travel=sum(distance_to_travel);
    final_time=distance_to_travel/0.15;

    % INTERPOLATE STUFF

    x=sml_trajectory(:,1); y=sml_trajectory(:,2);
    t_step=0.1;
    t=0:t_step:final_time; t=t';
    t_old=0:final_time/(length(x)-1):final_time;
    x_r = spline(t_old,x,t); y_r = spline(t_old,y,t);
    % h_path=figure; hold on;

    L=0.115;

    % Non Linear Parameters
    csi=0.7; g=60;

    % Gain tuning
    % gain_reduction_k1=0.25;
    % gain_reduction_k2=0.125;
    % gain_reduction_k3=0.125/2;
    gain_reduction_k1=0.5;
    gain_reduction_k2=0.25;
    gain_reduction_k3=0.25;

    % PATH CREATION
    x_r_d=[diff(x_r)/t_step; (x_r(end)-x_r(end-1))/t_step]; y_r_d=[diff(y_r)/t_step; (y_r(end)-y_r(end-1))/t_step];
    x_r_dd=[diff(x_r_d)/t_step; (x_r_d(end)-x_r_d(end-1))/t_step]; y_r_dd=[diff(y_r_d)/t_step; (y_r_d(end)-y_r_d(end-1))/t_step];
    theta_r=unwrap(atan2(y_r_d,x_r_d));
    v_r=( x_r_d.^2 + y_r_d.^2 ).^.5;
    w_r=( (x_r_d).*(y_r_dd.^2) - (y_r_d.^2).*(x_r_dd) )./ ...
        ( x_r_d.^2 + y_r_d.^2 );

    x_hist=nan(size(x_r_d)); y_hist=nan(size(x_r_d)); theta_hist=nan(size(x_r_d));
    time_hist=nan(size(x_r_d));
    v_hist=nan(size(x_r_d)); v_hist(1:2)=0;
    v_estimate=v_hist; w_hist=v_hist; w_estimate=v_hist; phi_hist=v_hist;

    [x_q,y_q,theta_q,t_init]=get_pose_qualisys_id(qtm,qualisys_truck_id); t_stamp=t_init;
    if theta_q<0
        theta_q=theta_q+2*pi;
    end
    x_i=[x_q y_q theta_q];
    x_curr=x_i;
    x_prev=x_curr;

    trailer_volt=0; % Lock trailer
    current_elapsed=0;
    velocity2voltage_handle=@velocity2voltage_no_stop_higher;
    % velocity2voltage_handle=@velocity2voltage_no_stop_higher;

    length_path=(diff(x_r).^2)+(diff(y_r).^2); length_path=sum(length_path.^.5);
    mean_velocity=length_path/t(end);

    d_s=5/32;
    % Best configuration so far
    %Londitudinal PID
    Kp_lo=3.0; Ki_lo=0.0;Kd_lo=0.0;
    PID_struct_longitudinal=PID_init(Kp_lo,Ki_lo,Kd_lo);
    %Lateral PID
    Kp_la=2.0;Ki_la=0.0;Kd_la=10.0;
    PID_struct_lateral=PID_init(Kp_la,Ki_la,Kd_la);

    %Velocity PID
    Kp_vel=1.0; Ki_vel=4.0;Kd_vel=99;
    PID_struct_velocity=PID_init(Kp_vel,Ki_vel,Kd_vel);
    
    desired_velocity = 0.20;
    
    prev_error=inf;
    
    
%     axis(states_figure_handler, 3*[-1 1 -1 1]);
    
    hold on;
    plot(x_r, y_r, 'b--');
    
    elapased_time_vector = zeros(size(t));
    iteration_counter = 1;
    
    states = [];
    

    while true

        tic

%         real_i=round(((t_stamp-t_init)+t_step-current_elapsed)/t_step);
        
        real_i = closest_reference_finder(x_curr, [x_r, y_r]);
        
        if real_i==0
            real_i=1; disp('Rounded to zero!')
        end
        if real_i>size(t,1)
            real_i=size(t,1); disp('Rounded to end!')
            disp('Should break, did not')
            error_test=tracking_error([x_r(real_i) y_r(real_i) theta_r(real_i)],x_curr);
            if norm(error_test(1:2))<0.05 || prev_error<norm(error_test(1:2))
                break
            end
            prev_error=norm(error_test(1:2));
            %         break
        end
        % Save current postion for historic purposes
        x_hist(iteration_counter)=x_curr(1); y_hist(iteration_counter)=x_curr(2); theta_hist(iteration_counter)=x_curr(3);
        theta_temp=unwrap(theta_hist(1:iteration_counter));
        x_curr(3)=theta_temp(end);
        theta_hist(iteration_counter)=x_curr(3);
        time_hist(iteration_counter)=t_stamp-t_init;

        % Estimate velocities and steering angle
        if iteration_counter>1
            v_current=norm(x_curr(1:2)-x_prev(1:2)); v_current=v_current/(time_hist(iteration_counter)-time_hist(iteration_counter-1));
            v_estimate(iteration_counter)=v_current; v_hist(iteration_counter)=median(v_estimate(max(1,iteration_counter-2):max(1,iteration_counter)));

            w_current=x_curr(3)-x_prev(3); w_current=w_current/(time_hist(iteration_counter)-time_hist(iteration_counter-1));
            w_estimate(iteration_counter)=w_current; w_hist(iteration_counter)=median(w_estimate(max(1,iteration_counter-2):max(1,iteration_counter)));

            phi_hist(iteration_counter)=atan(L*(w_hist(iteration_counter)/v_hist(iteration_counter)));
        end
        
        
        dangerTrucks = [];
        new_states = [];
        timeOutMillis = 10;
        
        receiving_messages_timeout = t_step/2;
        
        receiving_messages_timer = tic;
        
        while true
            
            if toc(receiving_messages_timer) > receiving_messages_timeout
               
                disp('Warning: Spending too much time to receive messages.')
                break
                
            end
           
            receivedLine = javaReceiveLineOverSocket( socket, timeOutMillis );
            
            if receivedLine == 0
               
                break
                
            end
            
            messageType = javaGetMessageType( receivedLine );
            
            if strcmp(messageType,'collision_warning') == 1
%                 disp('Warning received')
                dangerTrucks = javaProcessXmlCollisionWarning( receivedLine );
            end
            
            if strcmp(messageType,'vehicle_states_reply') == 1
%                 disp('Vehicle states received')
                new_states = javaProcessXmlStates( receivedLine );
            end
                                    
        end
        
        if ~isempty(new_states)
            
            states = new_states;
            states_handler = draw_all_states(states, states_figure_handler);
            plot(x_r, y_r, 'b--');
        end
        
%         receivedInfo = javaReceiveCollisionWarningOverSocket( socket );
        
        collision_distance = get_collision_distance(states, x_curr, [x_r, y_r], real_i);
        
        collision_distance = 0;

        warningFlag = false;
        
%         if ~isempty(dangerTrucks)
        if collision_distance
                       
            warningFlag = true;
            
        end

        [x_r_rot,y_r_rot]=rotate_trajectory(x_curr,x_r,y_r);
        [x_car_rot,y_car_rot]=rotate_trajectory(x_curr,x_curr(1),x_curr(2)); % Simply rotates the car coordinates
        longitudinal_error=x_r_rot(real_i)-x_car_rot;
        [u_longitudinal,PID_struct_longitudinal]=...
            PID_control(PID_struct_longitudinal,longitudinal_error,t_step);
        
        velocity_error = desired_velocity - v_hist(iteration_counter);
        [u_longitudinal,PID_struct_velocity]=...
            PID_control(PID_struct_velocity,velocity_error,t_step);
        
        
        disp(strcat('velocity_error = ', num2str(velocity_error)))
%         disp(strcat('longitudinal_error = ', num2str(longitudinal_error)))

        e_l_hist(iteration_counter)=longitudinal_error;

        e_1=y_r_rot(real_i)-y_car_rot;
        x_front=0; y_front=y_car_rot;
        i_front=round(d_s/(t_step*mean_velocity));

        if real_i+i_front<=length(t)
            e_2=y_r_rot(real_i+i_front)-y_front;
        else
            e_2=0;
        end

        error=e_1+e_2;
        e_1_hist(iteration_counter)=e_1; e_2_hist(iteration_counter)=e_2;
        [u_lateral,PID_struct_lateral]=PID_control(PID_struct_lateral,error,t_step);

        error=tracking_error([x_r(real_i) y_r(real_i) theta_r(real_i)],x_curr); % Compute error

        u=[u_longitudinal u_lateral];
        
        disp(strcat('u_longitudinal = ', num2str(u_longitudinal)))
        
        safety_follow_margin = 0.3;
        
        if collision_distance
            
            if collision_distance < safety_follow_margin
                
                disp( strcat( 'Full stop, collision distance: ', num2str(collision_distance) ) )
%                 disp('Full stop due to possible collision');
                u(1) = 0;
                
            else
                
                disp( strcat( 'Braking, collision distance: ', num2str(collision_distance) ) )
%                 disp('Braking due to possible collision');
                u(1) = u(1) * ( collision_distance / safety_follow_margin);
                
            end
            
        end
        
        u=pioneer_command_to_truck_SML_command(u); % Command conversion from unicycle to car
        %     V_volt=velocity2voltage(u(1)); w_volt=steering2voltage(u(2));
        %     V_volt=velocity2voltage_no_stop(u(1)); w_volt=steering2voltage(u(2));
        V_volt=velocity2voltage_handle(u(1)); w_volt=steering2voltage(u(2));
        
%         if warningFlag
%             u(1) = 0;
%         end

        if u(1)<=0
            V_volt = 1.7;
            disp('No movement forward')
        end

        %     Send command to truck
        disp( strcat('collision_distance = ', num2str(collision_distance)) )
        disp(strcat('V_volt = ', num2str(V_volt)))
        
%         V_volt = 2.5;
        w_volt = 2.0;
        
        voltage_vector = voltage_redirecter(V_volt,w_volt,bodyId);
    %     V_volt,w_volt,1.7,1.7,1.7,1.7
    
    if realRun
        NI_voltage_output(s,voltage_vector(1),voltage_vector(2),voltage_vector(3),voltage_vector(4),...
            voltage_vector(5),voltage_vector(6),trailer_volt);
    end
    %     NI_voltage_output(s,V_volt,w_volt,1.7,1.7,1.7,1.7,trailer_volt);

        x_prev = x_curr;
        %     Get current position
        [x_q,y_q,theta_q,t_stamp]=get_pose_qualisys_id(qtm,qualisys_truck_id);

        if ~isnan(x_q) % Handle possible invalid values
            if theta_q<0 % Put theta in range [0,2*pi]
                theta_q=theta_q+2*pi;
            end
            x_curr=[x_q y_q theta_q];
        else % If invalid reading, use previous reading
            x_curr=x_prev;
            disp('Invalid Qualisys') % Issue warning to the user
        end

        % Plot stuff
        if real_i>=2 && realRun
            plot([x_r(real_i-1) x_r(real_i)],[y_r(real_i-1) y_r(real_i)],'g')
%             delete(h_pioneer);
%             delete(h_ref);
        end
        %     plot([x_prev(1) x_curr(1)],[x_prev(2) x_curr(2)],'r')
        h_ref=plot(x_r(real_i),y_r(real_i),'go');
        h_pioneer=plot(x_curr(1),x_curr(2),'ro');

        % Sampling time stuff
        current_elapsed=toc;
        
        elapased_time_vector(iteration_counter) = current_elapsed;
        iteration_counter = iteration_counter + 1;
        
        memory = [];
        [terminate, memory] = check_termination_condition(memory, x_curr, x_r, y_r);
        
        if terminate
            
            break
            
        end
        
        pause(t_step-toc)
        
%         if exist('states_handler')
%            
%             delete(states_handler)
%             
%         end

    end

    if realRun
        NI_voltage_stop(s)
    end

end
    
if exist('qtm','var')
    % Close connection
    QMC(qtm, 'disconnect');
    clear mex
end

