function [ u, t_stopped  ] = adjustOutputToCollisionDistance( u, collision_distance, t_stopped, t_step )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

    safety_follow_margin = 0.3;

    if collision_distance

        if collision_distance < safety_follow_margin

            disp( strcat( 'Full stop, collision distance: ', num2str(collision_distance) ) )
            u(1) = 0;
            t_stopped = t_stopped + t_step;

        else

            disp( strcat( 'Braking, collision distance: ', num2str(collision_distance) ) )
            u(1) = u(1) * ( collision_distance / safety_follow_margin);
            t_stopped = t_stopped + ( collision_distance / safety_follow_margin)*t_step;

        end

    else

%          disp( strcat( 'Safe, collision distance: ', num2str(collision_distance) ) )

    end
            
end