clear
close all


% Adding Amro's folder
addpath('reverseMotionFunctions2');

% Choose which computer to connect to
ip_address = '130.237.50.246'; % Mine
% ip_address = '130.237.43.135'; % Pedro's
% ip_address = '130.237.43.176'; % Matteo's
% ip_address = '130.237.43.200'; % Internet

port = 8000;

% Correspondence between body ids and the NI voltage outputs, the first id
% will correspond to the first pair of initialized NI outputs;
% controlled_body_ids = [2, 3, 12];
controlled_body_ids = [16, 17, 10];

maximumForwardSpeedVoltage = 2.6;

truck1SpeedOffsetVoltage = 0.32;
truck2SpeedOffsetVoltage = 0.0;
truck3SpeedOffsetVoltage = 0.0;

global look_ahead_distance;
% look_ahead_distance = 5; % Original
look_ahead_distance = 8;

total_number_trucks = length(controlled_body_ids);

% If false, it will interface with the SML World, however without using the
% NI daq
real_run = true;

% Initializing stuff for the Qualisys
clear mex
disp('Opening connection to Qualisys...')
qtm_handler=QMC('QMC_conf_2.txt');
qualisys_verbose = false;

% Initialize the NI daq or not
if real_run
    if ~exist('s','var')
        s=NI_initialization(total_number_trucks);
        NI_voltage_stop(s, total_number_trucks)
        disp('NI DAQ Initialized')
    end
else
    s = -1;
end


% Global variables associated with the GUI
% When closedButtonPushed is true the program will try to exit cleanly
% When truckNOk, truck N will send a completed task message if it is
% performing
global closedButtonPushed
closedButtonPushed = false;
global truck1Ok
truck1Ok = false;
global truck2Ok
truck2Ok = false;
global truck3Ok
truck3Ok = false;
global buttonReverseRequest
buttonReverseRequest = false;
global buttonReverseCommand
buttonReverseCommand = false;

    
obstacle_ids.box_1_id = 1;    obstacle_ids.box_2_id = 2;
obstacle_ids.box_3_id = 3;    obstacle_ids.box_4_id = 4;
obstacle_ids.box_M_id = 5;

qualisysReadingsStructure = [];

try

    
    socket = connectToSmlWorld( ip_address, port , s, controlled_body_ids, real_run);
           
    % This line will open the TruckOkButtons
    truckOKButtons
    
    % For safety set the NI voltages to neutral
    if real_run
        NI_voltage_stop(s, total_number_trucks)
    end

    [ x_curr_vector, x_prev_vector, qualisysReadingsStructure ] = getInitialQualisysStates...
        ( qtm_handler, controlled_body_ids, qualisys_verbose, qualisysReadingsStructure );

    trailer_volt=0; % This voltage closes the trailer hitch lock
    current_elapsed=0;
    velocity2voltage_handle=@velocity2voltage_no_stop_higher;

    vehicle_readings = {};

    t_stopped = 0;

    % Initializing common variables
    isPerformingVector = 0*zeros(1,total_number_trucks);
    isInReverseVector = 0*zeros(1,total_number_trucks);
    t_stopped_vector = isPerformingVector;
    t_init_vector = isPerformingVector;
    theta_hist_vector = cell(1, total_number_trucks);
    time_hist_vector = cell(1, total_number_trucks);
    x_r_vector = cell(1, total_number_trucks);
    y_r_vector = cell(1, total_number_trucks);
    PID_struct_longitudinal_vector = cell(1, total_number_trucks);
    PID_struct_lateral_vector = cell(1, total_number_trucks);
    d_s =[]; 
    mean_velocity = [];
    
    last_reverse_path = [];

    % Desired sampling time
    t_step = 0.1;
    
    tStamps = [];

    reverseParamStruct = [];

    
    disp('Started Control Loop')
    
    while true
        
        % Start loop timer
        loop_timer = tic;

        % Initialize the voltage vector that will be sent to the NI daq
        voltage_vector = zeros(1,6);
        
        % Checking if user decided to stop the algorithm
        if closedButtonPushed
            error('Close Button Pushed');
        end
        
        % Process messages that we might receive
        [ new_vehicle_readings, dangerTrucks, trajectoryToPerform,...
            bodyToPerformTrajectoryId, stop_command_ids, reverse_trajectory_request  ] = ...
                controlLoopIncomingMessageProcessor( socket, t_step , qualisysReadingsStructure );

        % Check if any of the performing trucks should be stopped            
        isPerformingVector = processVehicleStop( stop_command_ids, controlled_body_ids, isPerformingVector );
                    
        % Update the vehicle readings to the most recent ones
        if ~isempty(new_vehicle_readings)
            vehicle_readings = new_vehicle_readings;
        end
        
        plannerOutputString = '';
        
        % Received a reverse_trajectory_request, must process it
        if ~isempty( reverse_trajectory_request )
            
%             % If any truck is performing, don't run the planning algorithm
%             if sum(isPerformingVector) == 0
                                
                
%                 reverse_trajectory_request.body_id = body_id;
%             reverse_trajectory_request.start_pose = start_pose;
%             reverse_trajectory_request.goal_pose = goal_pose;
%             reverse_trajectory_request.obstacles = obstacles;
%             reverse_trajectory_request.timeout = timeout;
                
            % AMRO STUFF
            % --- Obtaining the position of the tractor trailer and obstacles, and
            % determining the goal pose.
            obstacles = getObstacles(obstacle_ids, qualisysReadingsStructure);

            startPose = reverse_trajectory_request.start_pose;
            goalPose = reverse_trajectory_request.goal_pose;
            
            NI_voltage_stop(s, total_number_trucks);
            
            % --- Planning the path
            maxPlanningTime = reverse_trajectory_request.timeout;
            path = RRTreversePlanner(startPose, goalPose, obstacles, maxPlanningTime);
            % Path is an Nx4 matrix, first and second rows are X and Y
            % coordinates
            % !AMRO STUFF

            if isempty(path)
                % Reverse path planning timed out
                plannerOutputString = javaCreateReverseTrajectorySolutionMessage( [0 0] , 'timeout');
                last_reverse_path.path = [];
                disp('Planner went over timeout time')
                disp('Ignoring it/ sending empty trajectory')

            else
                % Reverse path planning was a success
                plannerOutputString = javaCreateReverseTrajectorySolutionMessage( path(:, 1:2) , 'success');
                last_reverse_path.path = path;
                last_reverse_path.body_id = reverse_trajectory_request.body_id;
                last_reverse_path.trailer_id = reverse_trajectory_request.trailer_id;
                last_reverse_path.obstacles = obstacles;
                disp('Planner found a solution')
                disp('Sending trajectory back')

                % --- Initializing the controller (obtaining the initial parameter struct)
                [xt,yt,theta1,~] = get_pose_qualisys_id(last_reverse_path.body_id, qualisysReadingsStructure);
                [xc,yc,theta2,t_stamp] = get_pose_qualisys_id(last_reverse_path.trailer_id, qualisysReadingsStructure);
                tStamps = [t_stamp-t_step t_stamp];
                tractorPose = [xt,yt,theta1];
                trailerPose = [xc,yc,theta2];
                reverseParamStruct = reverseControllerInit(tractorPose, trailerPose);

            end
                
%             else
%                 % Send busy, because we are performing, and cannot run the
%                 % planning algorithm now
%                 plannerOutputString = javaCreateReverseTrajectorySolutionMessage( [0 0] , 'busy');
%                 last_reverse_path.path = [];
%                 disp('Received a reverse planner request while controlling other trucks')
%                 disp('Ignoring it/ sending empty trajectory')
%                 
%             end
            
            success = sendMessageToSocket( socket, plannerOutputString );
                
            if ~success

                disp('Failed to send reverse planner reply to command central' );
                error('Failed to send reverse planner reply to command central' );
                
            else
                
                disp( 'Sent to world/command central: ' )
                disp( plannerOutputString )

            end 
                        
        end
       
%         A new trajectory command appeared, must handle it
        if ~isempty(trajectoryToPerform)
            
            if isstr(trajectoryToPerform)
                % Its a reverse trajectory
               disp('Its a reverse trajectory I need to perform')
               
               % Get truck number and set Performing and Reverse properties
               % in accordance
               truckNumber = find(controlled_body_ids == last_reverse_path.body_id);
               if ~isPerformingVector(truckNumber)
                    isInReverseVector(truckNumber) = 1;
                    isPerformingVector(truckNumber) = 1;
               else
%                  Repeating the warning that we are busy and cannot plan
                    disp('REVERSE TRAJECTORY REJECTED: Trajectory request rejected, truck is already in movement')
               end
                
            else
%                 Its a forward trajectory

            % Get truck number and set Performing property in accordance
                truckNumber = find(controlled_body_ids == bodyToPerformTrajectoryId);
                
                if ~isPerformingVector (truckNumber)
%                   Body is free, initialize trajectory and whant nots
                    isInReverseVector(truckNumber) = 0;
                   
                    [ PID_struct_longitudinal_vector, PID_struct_lateral_vector,...
                    x_r_vector, y_r_vector, theta_hist_vector,...
                    isPerformingVector, t_stopped_vector, t_init_vector, d_s, mean_velocity ]...
                    = createTrajectoryReferences(...
                    trajectoryToPerform, truckNumber, t_step, t_stamp, ...
                    PID_struct_longitudinal_vector,PID_struct_lateral_vector,...
                    x_r_vector, y_r_vector, theta_hist_vector,...
                    isPerformingVector, t_stopped_vector, t_init_vector );

                else
%                   Truck was requested to perform a trajectory when it was
%                   already performing something else
                    disp('TRAJECTORY REJECTED: Trajectory request rejected, truck is already in movement')

                end
                
            end

        end

        qualisys_timer = tic;
        
        qualisysReadingsStructure = updateQualisysReadingsStructure(qtm_handler, qualisysReadingsStructure);
        
        [ x_prev_vector, x_curr_vector, t_stamp ]...
            = updateQualisysStates...
            ( qtm_handler, controlled_body_ids, x_curr_vector, qualisys_verbose, qualisysReadingsStructure );
        
        qualisys_time = toc(qualisys_timer);
        if (qualisys_time > 0.2)
            disp(strcat('\n\n WARNING \nQualisys taking a lot of time: ',num2str(qualisys_time),'\n WARNING \n\n'))
        end
        
        performingTrucks = [];
          
        % Iterate over all the trucks
        for truckNumber = 1:total_number_trucks

            if isPerformingVector(truckNumber)
                performingTrucks(length(performingTrucks)+1) = controlled_body_ids(truckNumber);
            end
            
            truck_voltage_vector = zeros(1, 2*total_number_trucks);
            
            if ( isInReverseVector( truckNumber ) && isPerformingVector(truckNumber) )
                
%                 Amro's reverse controller iteration function should appear here
                disp( strcat( 'Performing in reverse for truck ', num2str( controlled_body_ids(truckNumber) ) ) );
                
                
                [xt,yt,theta1,~] = get_pose_qualisys_id(last_reverse_path.body_id , qualisysReadingsStructure);
                [xc,yc,theta2,tStamp] = get_pose_qualisys_id(last_reverse_path.trailer_id , qualisysReadingsStructure);
                tStamps = [tStamps(2) tStamp];
                tractorPose = [xt,yt,theta1];
                trailerPose = [xc,yc,theta2];

                [steeringVoltage, velocityVoltage, reachedGoal, reverseParamStruct] = ...
                reverseController(reverseParamStruct, tractorPose, trailerPose,...
                last_reverse_path.path, last_reverse_path.obstacles, tStamps, 0);
                
                disp( strcat('tractorPose : ', num2str( tractorPose ) ) )
                disp( strcat('trailerPose : ', num2str( trailerPose ) ) )
                disp( strcat('inputs : ', num2str( [velocityVoltage steeringVoltage] ) ) )
                velocityVoltage = max(velocityVoltage, 0.1);
                velocityVoltage = min(velocityVoltage, 3.1);
                steeringVoltage = max(steeringVoltage, 0.1);
                steeringVoltage = min(steeringVoltage, 3.1);
                
                disp( strcat('inputsClamped : ', num2str( [velocityVoltage steeringVoltage] ) ) )
            
                truck_voltage_vector = voltage_redirecter_summation...
                    ( velocityVoltage, steeringVoltage, last_reverse_path.body_id, controlled_body_ids );
                
                if reachedGoal
                   
                    disp('Reverse maneuver reached the goal!')
                    
                    disp('Terminate condition = true') 
                    isPerformingVector( truckNumber ) = false;
                    isInReverseVector( truckNumber ) = false;
                    sendCompletionMessageToSocket( socket, controlled_body_ids(truckNumber) );
                    disp('Sent completion message')
                    
                end
            
            else
                
                if ( isPerformingVector(truckNumber) )
%                     Controller iteration function
                    [ PID_struct_longitudinal_vector, PID_struct_lateral_vector, ...
                    t_stopped_vector, truck_voltage_vector, isPerformingVector] =...
                    truckControlLoop...
                    ( truckNumber, t_stopped_vector, t_init_vector, t_stamp, x_curr_vector,...
                    x_r_vector, y_r_vector, PID_struct_longitudinal_vector, PID_struct_lateral_vector, isPerformingVector,...
                    controlled_body_ids, d_s, mean_velocity, t_step, vehicle_readings, velocity2voltage_handle, socket);
                end    
            end
                  
%              Put the current voltage control outputs in the combined
%              voltages vector to be fed to NI daq. (This is done by
%              function voltage_redirecter_summation, which is called in
%              truckControlLoop
            voltage_vector = voltage_vector + truck_voltage_vector;
%             disp(strcat('voltage_vector = ', num2str(voltage_vector)))
%             disp(strcat('truck_voltage_vector = ', num2str(truck_voltage_vector)))
            

        end
        
        % Some helpful displays
        if length( performingTrucks ) == 1
            disp(strcat('Truck number ', num2str(performingTrucks) , ' is performing') )
        end
        if length( performingTrucks ) > 1
            disp(strcat('Truck numbers ', num2str(performingTrucks) , ' are performing') )
        end

        % Output the voltages if it is a real run
        if real_run
            
            voltage_vector = voltage_vector + 1.6*(voltage_vector==0.0);
            
            voltage_vector(voltage_vector > 3.1) = 3.1;
            voltage_vector(voltage_vector < 0.1) = 0.1;            
%             disp([voltage_vector trailer_volt])

            voltage_vector(1) = voltage_vector(1) + truck1SpeedOffsetVoltage;
            voltage_vector(3) = voltage_vector(3) + truck2SpeedOffsetVoltage;
            voltage_vector(5) = voltage_vector(5) + truck3SpeedOffsetVoltage;

            voltage_vector(1) = min( voltage_vector(1), maximumForwardSpeedVoltage );
            voltage_vector(3) = min( voltage_vector(3), maximumForwardSpeedVoltage );
            voltage_vector(5) = min( voltage_vector(5), maximumForwardSpeedVoltage );
            
            NI_voltage_output(s,[voltage_vector trailer_volt]);
        end
        
        % Handling button press for truck 1
        if truck1Ok && isPerformingVector(1)
            disp(strcat('Sending truck completion message for body ',num2str( controlled_body_ids(1) ) ) )
            success = sendCompletionMessageToSocket( socket, controlled_body_ids(1) );
            if ~success
                disp(strcat('Failed to send truck completion message for body ',num2str( controlled_body_ids(1) ) ) )
            else
                truck1Ok = false;
                isPerformingVector(1) = 0;
            end            
        end
        % Handling button press for truck 2
        if truck2Ok && isPerformingVector(2)
            disp(strcat('Sending truck completion message for body ',num2str( controlled_body_ids(2) ) ) )
            success = sendCompletionMessageToSocket( socket, controlled_body_ids(2) );
            if ~success
                disp(strcat('Failed to send truck completion message for body ',num2str( controlled_body_ids(2) ) ) )
            else
                truck2Ok = false;
                isPerformingVector(2) = 0;
            end            
        end
        % Handling button press for truck 3
        if truck3Ok && isPerformingVector(3)
            disp(strcat('Sending truck completion message for body ',num2str( controlled_body_ids(3) ) ) )
            success = sendCompletionMessageToSocket( socket, controlled_body_ids(3) );
            if ~success
                disp(strcat('Failed to send truck completion message for body ',num2str( controlled_body_ids(3) ) ) )
            else
                truck3Ok = false;
                isPerformingVector(3) = 0;
            end            
        end
        
        
        % Measuring the elapsed time in the current cycle
        current_elapsed=toc(loop_timer);

        % Warn the user that the sampling time was not respected
        if current_elapsed > t_step
            disp(strcat('Overtime!!! Cycle time: ' , num2str(current_elapsed) ) )
        end

        % Pause the cycle so that it tries to spend a total of t_step
        % running
        pause(t_step-current_elapsed)


    end

    % Stop everything for safety
    if real_run
        NI_voltage_stop(s, total_number_trucks)
    end

catch err

    
    if real_run
        NI_voltage_stop(s, total_number_trucks)
    end
    % If anything goes wrong, try to close the socket
    disp('Will close socket, and send CLOSE message.')
    
%     if exist('socket','var')
    
    javaCloseSocket( socket )
    
    
    
%     else
        
%         disp('Socket does not exist.')
%         
%     end

    % Rethrow the error, so that we know what happened
    rethrow(err);

end


    


