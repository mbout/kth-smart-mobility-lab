function [ trajectory, bodyId ] = javaProcessXmlTrajectory( trajectoryString )
%CREATEXML Summary of this function goes here
%   Detailed explanation goes here

    import java.lang.String;
    import java.lang.Double;
    import java.lang.Integer;
    import java.io.File;
    import java.io.IOException;
    import java.io.StringReader;
    import java.io.StringWriter;
    import java.net.*;
    import java.net.InetAddress;
    import java.net.UnknownHostException;
    import java.util.ArrayList;
    import java.util.Collections;
    import java.util.Enumeration;
    import java.util.List;
    import javax.xml.parsers.DocumentBuilder;
    import javax.xml.parsers.DocumentBuilderFactory;
    import javax.xml.parsers.ParserConfigurationException;
    import javax.xml.transform.Transformer;
    import javax.xml.transform.TransformerException;
    import javax.xml.transform.TransformerFactory;
    import javax.xml.transform.dom.DOMSource;
    import javax.xml.transform.stream.StreamResult;
    import org.w3c.dom.Attr;
    import org.w3c.dom.Document;
    import org.w3c.dom.Element;
    import org.w3c.dom.NodeList;
    import org.xml.sax.InputSource;
    import org.xml.sax.SAXException;


    dbf = DocumentBuilderFactory.newInstance();
%     dom = null;

%     try {

        %Using factory get an instance of document builder
    db = dbf.newDocumentBuilder();
    
%     disp('trajectoryString')
%     disp(trajectoryString)
%     save('trajectoryString','trajectoryString')


        %parse using builder to get DOM representation of the XML file
%     dom = loadXMLFromString( trajectoryString );
    
    factory = DocumentBuilderFactory.newInstance();
    builder = factory.newDocumentBuilder();
    is = InputSource(StringReader(trajectoryString));
    dom = builder.parse(is);


%     }catch(ParserConfigurationException pce) {
%             pce.printStackTrace();
%     }catch(SAXException se) {
%             se.printStackTrace();
%     }catch(IOException ioe) {
%             ioe.printStackTrace();
%     }


    %get the root element
    docEle = dom.getDocumentElement();
    
%     trajectoryElement = docEle.getElementsByTagName('trajectory');
%     disp('trajectoryElement.getLength()')
%     disp(trajectoryElement.getLength())
    
    messageElement = dom.getElementsByTagName('message');
    
    disp('messageElement.getLength()')
    disp(messageElement.getLength())
    
    
    messageType = messageElement.item(0).getAttribute('type');
    
    disp('messageType')
    disp(messageType)
    
%     pointElement = docEle.getElementsByTagName('point');
%     disp('pointElement.getLength()')
%     disp(pointElement.getLength())
    
%     disp('docEle.toString()')
%     disp(docEle.toString())
    
   
    
    bodyId = 0;
    
%     disp( 'messageElement.getAttribute(body_id)' )
%     disp( messageElement.getAttribute('body_id') )
    
    
    
    for i = 0:messageElement.getLength()-1
        
        el = messageElement.item(i);
        bodyId = Integer.parseInt( el.getAttribute('body_id') );
        
    end
    
%     pointElements = docEle.getElementsByTagName('point');

    %get a nodelist of <employee> elements
    pointElements = docEle.getElementsByTagName('point');
%     messageElements = docEle.getElementsByTagName('message');
    
    
%        System.out.println("There are " + nl.getLength() + " nodes in the xml.");

%     trajectoryPointList = ArrayList();
% 
%     if(nl != null && nl.getLength() > 0) {
% 
    trajectory = zeros(pointElements.getLength(), 3);

    for i = 0:pointElements.getLength()-1
%     for( i = 0 ; i < nl.getLength();i++) {

        el = pointElements.item(i);
%         el.getAttribute('time')
        seq = Double.parseDouble( el.getAttribute('seq') );
%         Double.parseDouble( el.getAttribute('time') )
        
        xPos = Double.parseDouble( el.getAttribute('x') );
        yPos = Double.parseDouble( el.getAttribute('y') );

        
        trajectory(i+1,:) = [xPos, yPos, seq]; 
%         trajectoryPointList.add( new TrajectoryPoint(time, xPos, yPos) );

%     }
    end
%     }


end

