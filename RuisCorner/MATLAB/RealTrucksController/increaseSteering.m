function [ newVoltages , direction] = increaseSteering( previousVoltages, s , direction)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

    increaseStep = 0.05;

    if direction
        
        voltageIncrease = increaseStep;
        
    else
        
        voltageIncrease = -increaseStep;
                
    end

    steeringWheelSelector = zeros(size(previousVoltages));
    
    for i = 1:length(steeringWheelSelector)/2
       
        steeringWheelSelector(2*i) = 1;
        
    end
    
    newVoltages = previousVoltages + voltageIncrease*steeringWheelSelector;
    
    prohibitedMargin = 0.7;
    
    if newVoltages(2) < prohibitedMargin || newVoltages(2) > 3.2-prohibitedMargin
       
        direction = ~direction;
        
    end
    
%     v1 = newVoltages(1);
%     v2 = newVoltages(2);
%     v3 = newVoltages(3);
%     v4 = newVoltages(4);
%     v5 = newVoltages(5);
%     v6 = newVoltages(6);
%     v7 = newVoltages(7);
    
    NI_voltage_output(s,newVoltages)
    
end

