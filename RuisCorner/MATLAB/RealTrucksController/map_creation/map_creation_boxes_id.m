function [ Obs ] = map_creation_boxes_id( qtm,id_box )
%MAP_CREATION Summary of this function goes here
%   Detailed explanation goes here



[x,y,theta,~]=get_pose_qualisys_id(qtm,id_box);

sf_mg=0.125; % safe margin 
box_length=0.6+2*sf_mg; box_width=0.4+2*sf_mg;

Obs={};
obstacle=repmat([x-sf_mg*cos(theta)-sf_mg*sin(-theta) y-sf_mg*sin(theta)-sf_mg*cos(theta)],[4 1])+...    
   [[cos(theta) -sin(theta);sin(theta) cos(theta)]*[0 0;0 box_width; box_length box_width; box_length 0]']';     
Obs{1}=obstacle;

end