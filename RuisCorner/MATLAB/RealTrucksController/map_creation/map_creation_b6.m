function [ Obs ] = map_creation_b6( space_width , d )
%MAP_CREATION Summary of this function goes here
%   Detailed explanation goes here

Obs={};
point=[(1/8)*(space_width/2) (6/8)*(space_width/2)];
obstacle=[point;point;point;point]+[0 0;-d 0;-d d;0 d];
Obs{1}=obstacle;
point=[-(5/8)*(space_width/2) (3/8)*(space_width/2)];
obstacle=[point;point;point;point]+[0 0;-d 0;-d d;0 d];
Obs{2}=obstacle;
point=[-(4/8)*(space_width/2) -(4/8)*(space_width/2)];
obstacle=[point;point;point;point]+[0 0;-d 0;-d d;0 d];
Obs{3}=obstacle;
point=[(6/8)*(space_width/2) (0/8)*(space_width/2)];
obstacle=[point;point;point;point]+[0 0;-d 0;-d d;0 d];
Obs{4}=obstacle;



end

