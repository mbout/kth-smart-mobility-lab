function [ Obs ] = map_creation_grid( space_width , w , d)
%MAP_CREATION space_width , w , d, w=distance between blocks, d=size block

Obs={};
it=1;

for i=w:w:space_width-w
    
    for j=w:w:space_width-w
        
        point=-0.5*[space_width space_width]+[i j];
        obstacle=[point;point;point;point]+...
        [0 0;d 0;d -d;0 -d];
        Obs{it}=obstacle;
        it=it+1;
        
    end
    
end

end

