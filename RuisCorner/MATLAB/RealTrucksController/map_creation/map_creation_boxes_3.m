function [ Obs ] = map_creation_boxes_3(qtm,id_box_1,id_box_2,id_box_3,id_box_4)
%MAP_CREATION Summary of this function goes here
%   Detailed explanation goes here

[x,y,theta,~]=get_pose_qualisys_id(qtm,id_box_1);

sf_mg=0.125; % safe margin 
box_length=0.6+2*sf_mg; box_width=0.4+2*sf_mg;

Obs={};

if ~isnan(x)

    obstacle=repmat([x-sf_mg*cos(theta)-sf_mg*sin(-theta) y-sf_mg*sin(theta)-sf_mg*cos(theta)],[4 1])+...    
       [[cos(theta) -sin(theta);sin(theta) cos(theta)]*[0 0;0 box_width; box_length box_width; box_length 0]']';     
    Obs{length(Obs)+1}=obstacle;

end

[x,y,theta,~]=get_pose_qualisys_id(qtm,id_box_2);

if ~isnan(x)
    
obstacle=repmat([x-sf_mg*cos(theta)-sf_mg*sin(-theta) y-sf_mg*sin(theta)-sf_mg*cos(theta)],[4 1])+...    
   [[cos(theta) -sin(theta);sin(theta) cos(theta)]*[0 0;0 box_width; box_length box_width; box_length 0]']';     
    Obs{length(Obs)+1}=obstacle;

end

[x,y,theta,~]=get_pose_qualisys_id(qtm,id_box_3);

if ~isnan(x)
    
obstacle=repmat([x-sf_mg*cos(theta)-sf_mg*sin(-theta) y-sf_mg*sin(theta)-sf_mg*cos(theta)],[4 1])+...    
   [[cos(theta) -sin(theta);sin(theta) cos(theta)]*[0 0;0 box_width; box_length box_width; box_length 0]']';     
    Obs{length(Obs)+1}=obstacle;

end

[x,y,theta,~]=get_pose_qualisys_id(qtm,id_box_4);

if ~isnan(x)
    
obstacle=repmat([x-sf_mg*cos(theta)-sf_mg*sin(-theta) y-sf_mg*sin(theta)-sf_mg*cos(theta)],[4 1])+...    
   [[cos(theta) -sin(theta);sin(theta) cos(theta)]*[0 0;0 box_width; box_length box_width; box_length 0]']';     
    Obs{length(Obs)+1}=obstacle;

end


end