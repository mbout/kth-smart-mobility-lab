function [path] = optimal_path_print_car_up_the_tree( graph , node_id , t)
%OPTIMAL_PATH Summary of this function goes here
%   Detailed explanation goes here

path=[];

while 1
    
    path=[path; node_id];
    node=graph.get(node_id);
    
    if node.prev==0
        break
    else
        node_id=node.prev;
    end
    
end

path=fliplr(path');

node=graph.get(path(1));

hold on

x_continuous=node.x;

for i=2:length(path)
    
    node=graph.get(path(i));
    
    x_continuous=continuous_sim_car(x_continuous(end,:),node.u,t);
       
    %        plot(x_position(:,1),x_position(:,2),'r--',...
    %            'LineWidth',2)
    plot(x_continuous(:,1),x_continuous(:,2),'r')
    
end

end

