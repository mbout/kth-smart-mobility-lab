function [path] = optimal_path_print( graph , node_id , t)
%OPTIMAL_PATH Summary of this function goes here
%   Detailed explanation goes here

path=graph.findpath(1,node_id);

prev_node=graph.get(path(1));

hold on

x_continuous=prev_node.x;

for i=2:length(path)
    
    node=graph.get(path(i));
    
    x_continuous=continuous_sim( x_continuous(end,:) , node.u , 1 );
    
    x_position=zeros(size(x_continuous,1),2);
    for j=1:size(x_continuous,1)
        x_position(j,:)=car_position_rear(x_continuous(j,:));
    end
    
    plot(x_position(:,1),x_position(:,2))
    prev_node=node;
    
end



end

