function [ ] = plot_occupancy( occupancy_grid , ...
    space_width , space_res )
%UNTITLED8 Summary of this function goes here
%   Detailed explanation goes here

for i=1:size(occupancy_grid,1)
    for j=1:size(occupancy_grid,2)
        if occupancy_grid(i,j)
            rectangle('Position',[-0.5*space_width+(i-1)*space_res,...
                -0.5*space_width+(j-1)*space_res, space_res,space_res],...
                'FaceColor','k')
        end
    end
end

end

