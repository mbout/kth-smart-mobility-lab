function [ ] = plot_connection_car( new_node , graph)
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here

%     x_i=new_node.x;
parent=graph.get(new_node.parent);
%     x_f=parent.x;
car_pos_f=car_position_front_car_model(new_node.x);
car_pos_i=car_position_front_car_model(parent.x);


%     plot([car_pos_i(1) car_pos_f(1)],[car_pos_i(2) car_pos_f(2)])
x_continuous=continuous_sim_car( parent.x , new_node.u , 1 );

x_position=zeros(size(x_continuous,1),2);
for j=1:size(x_continuous,1)
    x_position(j,:)=car_position_front_car_model(x_continuous(j,:));
end

plot(x_position(:,1),x_position(:,2))


end

