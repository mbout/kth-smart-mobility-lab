clear
close all

ipAddress = '130.237.50.246'; % Mine
% ipAddress = '130.237.43.135'; % Pedro's
% ipAddress = '130.237.43.176'; % Matteo's
port = 8000;

% Correspondence:
controlledBodyIds = [2, 3, 12];

realRun = false;

% % INITIALIZATION STUFF
% if exist('qtm','var')
%     % Close connection
%     QMC(qtm, 'disconnect');
%     clear mex
% end
% 
% if ~exist('qtm','var')
%     % Open connection
%     disp('Opening connection to Qualisys...')
%     qtm=QMC('QMC_conf.txt');
% end

clear mex
disp('Opening connection to Qualisys...')
qtm=QMC('QMC_conf.txt');

qualisysVerbose = false;

if realRun

    if ~exist('s','var')
        s=NI_initialization();
        NI_voltage_stop(s)
    end

else

    s = -1;

end

global closedButtonPushed
closedButtonPushed = false;
global truck1Ok
truck1Ok = false;
global truck2Ok
truck2Ok = false;
global truck3Ok
truck3Ok = false;

try

%         [trajectory, bodyId, socket] = connectAndGetTrajectory( ipAddress, port , s, controlledBodyIds, realRun);
    socket = connectToSmlWorld( ipAddress, port , s, controlledBodyIds, realRun);
   
    closeButton
    truckOKButtons
        
    if realRun
        NI_voltage_stop(s)
    end

    addpath('collision_detector','drawing','map_creation',...
        'handle_loaders','misc','system_simulator','nearest','plot_graph');

    [ x_curr_vector, x_prev_vector ] = getInitialQualisysStates( qtm, controlledBodyIds, qualisysVerbose );

    trailer_volt=0; % Lock trailer
    current_elapsed=0;
    velocity2voltage_handle=@velocity2voltage_no_stop_higher;
    % velocity2voltage_handle=@velocity2voltage_no_stop_higher;

    states = [];

    t_stopped = 0;

    % Initializing common variables
    isPerformingVector = 0*zeros(1,3);
    t_stopped_vector = isPerformingVector;
    t_init_vector = isPerformingVector;
    theta_hist_vector = cell(1, 3);
    time_hist_vector = cell(1, 3);
    x_r_vector = cell(1, 3);
    y_r_vector = cell(1, 3);
    PID_struct_longitudinal_vector = cell(1, 3);
    PID_struct_lateral_vector = cell(1, 3);
    d_s =[]; 
    mean_velocity = [];

    t_step = 0.1;

    while true

        tic

        voltage_vector = zeros(1,6);
        
        if closedButtonPushed
            error('Close Button Pushed');
        end
        
%         disp('Control LOOP')
        
        [ new_states, dangerTrucks, trajectoryToPerform, bodyToPerformTrajectoryId  ] = ...
                controlLoopIncomingMessageProcessor( socket, t_step );

        disp(isPerformingVector)
            
        if ~isempty(new_states)
            states = new_states;
%                 states_handler = draw_all_states(states, states_figure_handler);
%                 plot(x_r, y_r, 'b--');                
        end

        if ~isempty(trajectoryToPerform)

            truckNumber = find(controlledBodyIds == bodyToPerformTrajectoryId);

            if ~isPerformingVector(truckNumber)

                % Body is free, make it perform the trajectory
%                     initializeTrajectoryForControlLoop(trajectoryToPerform, truckNumber)

                [ PID_struct_longitudinal_vector, PID_struct_lateral_vector,...
                x_r_vector, y_r_vector, theta_hist_vector,...
                isPerformingVector, t_stopped_vector, t_init_vector, d_s, mean_velocity ]...
                = createTrajectoryReferences(...
                trajectoryToPerform, truckNumber, t_step, t_stamp, ...
                PID_struct_longitudinal_vector,PID_struct_lateral_vector,...
                x_r_vector, y_r_vector, theta_hist_vector,...
                isPerformingVector, t_stopped_vector, t_init_vector );

            else

                disp('TRAJECTORY REJECTED: Trajectory request rejected, truck is already in movement')

            end

        end


        
        [ x_prev_vector, x_curr_vector, t_stamp ] = updateQualisysStates( qtm, controlledBodyIds, x_curr_vector, qualisysVerbose );
        
        for truckNumber = 1:3


            [ PID_struct_longitudinal_vector, PID_struct_lateral_vector, ...
            t_stopped_vector, truck_voltage_vector, isPerformingVector] =...
            truckControlLoop...
            ( truckNumber, t_stopped_vector, t_init_vector, t_stamp, x_curr_vector,...
            x_r_vector, y_r_vector, PID_struct_longitudinal_vector, PID_struct_lateral_vector, isPerformingVector,...
            controlledBodyIds, d_s, mean_velocity, t_step, states, velocity2voltage_handle, socket);

            voltage_vector = voltage_vector + truck_voltage_vector;

        end

        if realRun
            NI_voltage_output(s,voltage_vector(1),voltage_vector(2),voltage_vector(3),voltage_vector(4),...
                voltage_vector(5),voltage_vector(6),trailer_volt);
        end

        % Sampling time stuff
        current_elapsed=toc;
        
        
%         if truck

        if truck1Ok
            disp(strcat('Sending truck completion message for body ',num2str( controlledBodyIds(1) ) ) )
            success = sendCompletionMessageToSocket( socket, controlledBodyIds(1) );
            if ~success
                disp(strcat('Failed to send truck completion message for body ',num2str( controlledBodyIds(1) ) ) )
            else
                truck1Ok = false;
            end            
        end

        if truck2Ok
            disp(strcat('Sending truck completion message for body ',num2str( controlledBodyIds(2) ) ) )
            success = sendCompletionMessageToSocket( socket, controlledBodyIds(2) );
            if ~success
                disp(strcat('Failed to send truck completion message for body ',num2str( controlledBodyIds(2) ) ) )
            else
                truck2Ok = false;
            end            
        end
    
        if truck3Ok
            disp(strcat('Sending truck completion message for body ',num2str( controlledBodyIds(3) ) ) )
            success = sendCompletionMessageToSocket( socket, controlledBodyIds(3) );
            if ~success
                disp(strcat('Failed to send truck completion message for body ',num2str( controlledBodyIds(3) ) ) )
            else
                truck3Ok = false;
            end            
        end

        
        

        if current_elapsed > t_step

            disp(strcat('Overtime!!! Cycle time: ' , num2str(current_elapsed) ) )

        end

        pause(t_step-current_elapsed)


    end

    if realRun
        NI_voltage_stop(s)
    end

catch err

%     rethrow(err);

    disp('Will close socket, and send CLOSE message.')
    javaCloseSocket( socket )

end


    
if exist('qtm','var')
    % Close connection
    QMC(qtm, 'disconnect');
    clear mex
end

