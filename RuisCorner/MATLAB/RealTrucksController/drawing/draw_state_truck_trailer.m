function [  ] = draw_state_truck_trailer( x )
%DRAW_STATE Summary of this function goes here
%   Detailed explanation goes here

if length(x)~=4
    error('Input state with wrong dimmention')
end

% d=0.166;
d=0.150;
L=0.115;

trailer_pos=[x(1);x(2)];

% E=0.166-0.025;
E=0.19;
axle=0.045;

car_pos=[x(1)+d*cos(x(3));x(2)+d*sin(x(3))];

car_bounds=[-0.025 L+0.04 L+0.04 -0.025; axle axle -axle -axle];

R=[cos(x(4)) -sin(x(4));...
    sin(x(4)) cos(x(4))];

car_bounds=R*car_bounds;

car_bounds=car_bounds+repmat(car_pos,[1 4]);

fill(car_bounds(1,:),car_bounds(2,:),[0.5,0.5,0.5])

% trailer_bounds=[-0.025 E E -0.025; axle axle -axle -axle];
trailer_bounds=[-0.08 E E -0.08; axle axle -axle -axle];

R=[cos(x(3)) -sin(x(3));...
    sin(x(3)) cos(x(3))];

trailer_bounds=R*trailer_bounds;

trailer_bounds=trailer_bounds+repmat(trailer_pos,[1 4]);

fill(trailer_bounds(1,:),trailer_bounds(2,:),'k')

end