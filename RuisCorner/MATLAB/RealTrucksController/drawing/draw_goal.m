function [ ] = draw_goal( goal , tolerance )
%DRAW_GOAL Summary of this function goes here
%   Detailed explanation goes here

plot(goal(1),goal(2), 'go')

    for i=0:0.05:2*pi

        plot(goal(1)+tolerance*cos(i),goal(2)+tolerance*sin(i), 'g');

    end

end

