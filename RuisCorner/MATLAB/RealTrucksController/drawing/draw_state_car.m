function [  ] = draw_state_car( x )
%DRAW_STATE Summary of this function goes here
%   Detailed explanation goes here

if length(x)~=3
    error('Input state with wrong dimmention')
end

L=3.875;
axle=3.1/2;

car_pos=[x(1);x(2)];

car_bounds=[-1.635 L+1.46 L+1.46 -1.635; axle axle -axle -axle];

R=[cos(x(3)) -sin(x(3));...
    sin(x(3)) cos(x(3))];

car_bounds=R*car_bounds;

car_bounds=car_bounds+repmat(car_pos,[1 4]);

fill(car_bounds(1,:),car_bounds(2,:),[.5 .5 .5])

end