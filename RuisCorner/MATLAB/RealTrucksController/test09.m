clear
close all

ipAddress = '130.237.50.246'; % Mine
% ipAddress = '130.237.43.135'; % Pedro's
% ipAddress = '130.237.43.176'; % Matteo's
port = 8000;

% Correspondence:
controlledBodyIds = [2, 3, 12];

realRun = false;

% % INITIALIZATION STUFF
% if exist('qtm','var')
%     % Close connection
%     QMC(qtm, 'disconnect');
%     clear mex
% end
% 
% if ~exist('qtm','var')
%     % Open connection
%     disp('Opening connection to Qualisys...')
%     qtm=QMC('QMC_conf.txt');
% end

clear mex
disp('Opening connection to Qualisys...')
qtm=QMC('QMC_conf.txt');


if realRun

    if ~exist('s','var')
        s=NI_initialization();
        NI_voltage_stop(s)
    end

else

    s = -1;

end

states_figure_handler = figure;
hold on; axis(3*[-1 1 -1 1]);



while 1
    
    try

        [trajectory, bodyId, socket] = connectAndGetTrajectory( ipAddress, port , s, controlledBodyIds, realRun);

        qualisys_truck_id = bodyId;

        if realRun
            NI_voltage_stop(s)
        end

        addpath('collision_detector','drawing','map_creation',...
            'handle_loaders','misc','system_simulator','nearest','plot_graph');

        %%% START HERE

        delta_x=diff(trajectory(:,1)); delta_y=diff(trajectory(:,2));
        distance_to_travel=(delta_x.^2 + delta_y.^2).^.5;
        distance_to_travel=sum(distance_to_travel);
        final_time=distance_to_travel/0.15;

        % INTERPOLATE STUFF

        x=trajectory(:,1); y=trajectory(:,2);
        t_step=0.1;
        t=0:t_step:final_time; t=t';
        t_old=0:final_time/(length(x)-1):final_time;
        x_r = spline(t_old,x,t); y_r = spline(t_old,y,t);
        % h_path=figure; hold on;

        L=0.115;

        % Gain tuning
        % gain_reduction_k1=0.25; gain_reduction_k2=0.125; gain_reduction_k3=0.125/2;
        gain_reduction_k1=0.5; gain_reduction_k2=0.25; gain_reduction_k3=0.25;

        % Path creation
        x_r_d=[diff(x_r)/t_step; (x_r(end)-x_r(end-1))/t_step]; y_r_d=[diff(y_r)/t_step; (y_r(end)-y_r(end-1))/t_step];
        theta_r=unwrap(atan2(y_r_d,x_r_d));
        
        theta_hist=nan(size(x_r_d));
        time_hist=nan(size(x_r_d));

        [x_q,y_q,theta_q,t_init]=get_pose_qualisys_id(qtm,qualisys_truck_id); t_stamp=t_init;
        if theta_q<0
            theta_q=theta_q+2*pi;
        end
        x_i=[x_q y_q theta_q];
        x_curr=x_i;
        x_prev=x_curr;

        trailer_volt=0; % Lock trailer
        current_elapsed=0;
        velocity2voltage_handle=@velocity2voltage_no_stop_higher;
        % velocity2voltage_handle=@velocity2voltage_no_stop_higher;

        length_path=(diff(x_r).^2)+(diff(y_r).^2); length_path=sum(length_path.^.5);
        mean_velocity=length_path/t(end);

        d_s=5/32;
        % Best configuration so far

        %Londitudinal PID
        Kp_lo=2.0; Ki_lo=0.0;Kd_lo=0.0;
        PID_struct_longitudinal=PID_init(Kp_lo,Ki_lo,Kd_lo);
        %Lateral PID
        Kp_la=2.0;Ki_la=0.0;Kd_la=10.0;
        PID_struct_lateral=PID_init(Kp_la,Ki_la,Kd_la);


        prev_error=inf;


    %     axis(states_figure_handler, 3*[-1 1 -1 1]);

        hold on;
        plot(x_r, y_r, 'b--');

        elapased_time_vector = zeros(size(t));
        iteration_counter = 1;

        states = [];

        t_stopped = 0;

        while true

            tic

            real_i=round(((t_stamp-t_init-t_stopped)+t_step-current_elapsed)/t_step);

            if real_i==0
                real_i=1; disp('Zero index, rounded to one!')
            end
            
            % Save current theta for unwrap purposes
            theta_hist(iteration_counter)=x_curr(3);
            theta_temp=unwrap(theta_hist(1:iteration_counter));
            x_curr(3)=theta_temp(end);
            theta_hist(iteration_counter)=x_curr(3);
            time_hist(iteration_counter)=t_stamp-t_init;


            [ new_states, dangerTrucks ] = controlLoopIncomingMessageProcessor( socket, t_step );

            if ~isempty(new_states)

                states = new_states;
                states_handler = draw_all_states(states, states_figure_handler);
                plot(x_r, y_r, 'b--');
                
            end

            collision_distance = get_collision_distance(states, x_curr, [x_r, y_r], real_i);

            warningFlag = false;

            if collision_distance

                warningFlag = true;

            end

            [x_r_rot,y_r_rot]=rotate_trajectory(x_curr,x_r,y_r);
            [x_car_rot,y_car_rot]=rotate_trajectory(x_curr,x_curr(1),x_curr(2)); % Simply rotates the car coordinates
            longitudinal_error=x_r_rot(real_i)-x_car_rot;
            [u_longitudinal,PID_struct_longitudinal]=...
                PID_control(PID_struct_longitudinal,longitudinal_error,t_step);

            e_1=y_r_rot(real_i)-y_car_rot;
            x_front=0; y_front=y_car_rot;
            i_front=round(d_s/(t_step*mean_velocity));

            if real_i+i_front<=length(t)
                e_2=y_r_rot(real_i+i_front)-y_front;
            else
                e_2=0;
            end

            lateral_error=e_1+e_2;
            [u_lateral,PID_struct_lateral]=PID_control(PID_struct_lateral,lateral_error,t_step);

            u=[u_longitudinal u_lateral];

            [u, t_stopped] = adjustOutputToCollisionDistance( u, collision_distance, t_stopped, t_step );
            
            u=pioneer_command_to_truck_SML_command(u); % Command conversion from unicycle to car
            V_volt=velocity2voltage_handle(u(1)); w_volt=steering2voltage(u(2));

            if u(1)<=0
                V_volt = 1.7;
                disp('No movement forward')
            end

            voltage_vector = voltage_redirecter(V_volt,w_volt,bodyId);

            if realRun
                NI_voltage_output(s,voltage_vector(1),voltage_vector(2),voltage_vector(3),voltage_vector(4),...
                    voltage_vector(5),voltage_vector(6),trailer_volt);
            end

            x_prev = x_curr;
            % Get current position
            [x_q,y_q,theta_q,t_stamp]=get_pose_qualisys_id(qtm,qualisys_truck_id);

            if ~isnan(x_q) % Handle possible invalid values
                if theta_q<0 % Put theta in range [0,2*pi]
                    theta_q=theta_q+2*pi;
                end
                x_curr=[x_q y_q theta_q];
            else % If invalid reading, use previous reading
                x_curr=x_prev;
                disp('Invalid Qualisys') % Issue warning to the user
            end

            % Plot stuff
            if real_i>=2 && realRun
                plot([x_r(real_i-1) x_r(real_i)],[y_r(real_i-1) y_r(real_i)],'g')
    %             delete(h_pioneer);
    %             delete(h_ref);
            end
            %     plot([x_prev(1) x_curr(1)],[x_prev(2) x_curr(2)],'r')
            h_ref=plot(x_r(real_i),y_r(real_i),'go');
            h_pioneer=plot(x_curr(1),x_curr(2),'ro');

            % Sampling time stuff
            current_elapsed=toc;

            elapased_time_vector(iteration_counter) = current_elapsed;
            iteration_counter = iteration_counter + 1;

            memory = [];
            [terminate, memory] = check_termination_condition(memory, x_curr, x_r, y_r);

            if terminate

                break

            end

            pause(t_step-toc)

    %         if exist('states_handler')
    %            
    %             delete(states_handler)
    %             
    %         end

        end

        if realRun
            NI_voltage_stop(s)
        end
        
    catch err
        
        rethrow(err);
        
        
        javaCloseSocket( socket )
        
    end

end
    
if exist('qtm','var')
    % Close connection
    QMC(qtm, 'disconnect');
    clear mex
end

