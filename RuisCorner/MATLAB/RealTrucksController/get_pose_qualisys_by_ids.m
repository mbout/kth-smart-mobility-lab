function [x,y,theta,t_stamp] = get_pose_qualisys_by_ids(ids, qualisysReadingsStructure)
%GET_POSE_QUALISYS Get x,y,theta from qualisys, they come in meters and
%radians
%   Detailed explanation goes here



%     the6DOF=QMC(qtm);
%     frameinfo=QMC(qtm,'frameinfo');
    
%     qualisysReadingsStructure.frameinfo = frameinfo;
    
    t_stamp=qualisysReadingsStructure.frameinfo(2)/(10^6);
%     frameinfo(2)/(10^6)
%     disp(frameinfo(2)/(10^8))
    x=qualisysReadingsStructure.the6DOF(1,ids)/1000;
    y=qualisysReadingsStructure.the6DOF(2,ids)/1000;
    theta=deg2rad(qualisysReadingsStructure.the6DOF(6,ids));
    

    x=x';
    y=y';
    theta=theta';
    
end

