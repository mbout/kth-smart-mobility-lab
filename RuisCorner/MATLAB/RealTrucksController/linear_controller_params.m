function [u] = linear_controller_params(error,v_r,w_r,K)
%LINEAR_CONTROLLER [u] = linear_controller(error,v_r,w_r)
%   Detailed explanation goes here

u_F=[v_r*cos(error(3));w_r];

k1=K(1); k2=K(2); k3=K(3);

K_s=[k1 0 0;...
    0 sign(v_r)*k2 k3];

u_B=K_s*error;

u=[u_B+u_F]';

end

