function [ system_sim_handle,collision_checker_handle,inside_workspace_handle,...
    state_position_handle,optimal_path_print_handle,metric_distance_handle,...
    plot_connection_handle,draw_state_handle,random_point_handle,nearest_handle]...
    = car_load_handle()
%CAR_HANDLE Summary of this function goes here
%   Detailed explanation goes here

system_sim_handle=@continuous_sim_car;
collision_checker_handle=@collision_checker_car;
inside_workspace_handle=@inside_workspace_car;
state_position_handle=@car_position_front_car_model; %Irrelevant
metric_distance_handle=@metric_distance_car;
optimal_path_print_handle=@optimal_path_print_car_up_the_tree;
plot_connection_handle=@plot_connection_car;
draw_state_handle=@draw_state_car;
random_point_handle=@get_x_rand_car_model;
nearest_handle=@nearest_car_model;

end

