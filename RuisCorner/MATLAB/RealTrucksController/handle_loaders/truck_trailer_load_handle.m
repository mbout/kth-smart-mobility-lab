function [ system_sim_handle,collision_checker_handle,inside_workspace_handle,...
    state_position_handle,optimal_path_print_handle,metric_distance_handle,...
    plot_connection_handle,draw_state_handle,random_point_handle,nearest_handle]...
    = truck_trailer_load_handle()
%TRUCK_TRAILER_HANDLE Summary of this function goes here
%   Detailed explanation goes here

system_sim_handle=@continuous_sim_truck_trailer;
collision_checker_handle=@collision_checker_truck_trailer;
inside_workspace_handle=@inside_workspace_truck_trailer;
state_position_handle=@car_position_front_truck_trailer_model;
% metric_distance_handle=@metric_distance_truck_trailer_theta_cropped;
% metric_distance_handle=@metric_distance_truck_trailer_car_front;
metric_distance_handle=@metric_distance_truck_trailer_car_axle;
optimal_path_print_handle=@optimal_path_print_truck_trailer_up_the_tree;
plot_connection_handle=@plot_connection_truck_trailer;
draw_state_handle=@draw_state_truck_trailer;
random_point_handle=@get_x_rand_truck_trailer_model;
nearest_handle=@nearest_truck_trailer_model;

end

