function [ system_sim_handle,collision_checker_handle,inside_workspace_handle,...
    state_position_handle,optimal_path_print_handle,metric_distance_handle,...
    plot_connection_handle,draw_state_handle,random_point_handle,nearest_handle]...
    = point_load_handle()
%CAR_HANDLE Summary of this function goes here
%   Detailed explanation goes here

system_sim_handle=@continuous_sim_point; %
collision_checker_handle=@collision_checker_point;
inside_workspace_handle=@inside_workspace_point;
state_position_handle=@point_position;
metric_distance_handle=@metric_distance_point;
optimal_path_print_handle=@optimal_path_print_point;
plot_connection_handle=@plot_connection_point;
draw_state_handle=@draw_state_point;
random_point_handle=@get_x_rand_point_model;
nearest_handle=@nearest_point;

end

