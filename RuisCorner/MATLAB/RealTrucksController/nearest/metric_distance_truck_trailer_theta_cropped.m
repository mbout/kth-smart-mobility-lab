function [ dist ] = metric_distance_truck_trailer_theta_cropped( x , alpha )
%METRIC_DISTANCE Summary of this function goes here
%   Detailed explanation goes here

dist=x-alpha;

dist(:,3:4)=dist(:,3:4)-(dist(:,3:4)>pi)*pi;
dist(:,3:4)=dist(:,3:4)+(dist(:,3:4)<-pi)*pi;

dist=(dist(:,1).^2+dist(:,2).^2+dist(:,3).^2+dist(:,4).^2).^.5;

end

