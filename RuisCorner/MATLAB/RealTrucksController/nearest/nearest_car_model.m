function [ nearest_id ] = nearest_car_model(graph_pos,x_rand,k)
%UNTITLED p=euclidean distance of trailer in the xy-plane. d_theta is the
%difference of trailer angles. dist=p*(1+abs(sin(d_theta)))

% distances=metric_distance_car(graph_pos,repmat(x_rand,[size(graph_pos,1) 1]));
dist=graph_pos-repmat(x_rand,[size(graph_pos,1) 1]);

dist=(dist(:,1).^2 + dist(:,2).^2 + dist(:,3).^2).^.5;

[~,nearest_id]=sort(dist);

nearest_id=nearest_id(1:k);

end