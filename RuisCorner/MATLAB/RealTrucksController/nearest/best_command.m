function [ u ] = best_command( x_rand , nearest_node , t , U_space, ...
    system_sim_handle , metric_distance_handle)

best_dist=inf;
best_u=zeros(1,ndims(U_space));

for i=1:size(U_space,1)
    
    for j=1:size(U_space,2)
        
        x=system_sim_handle(nearest_node.x,U_space(i,j,:),t);
        x_new=x(end,:);
        
        dist=metric_distance_handle(x_new,x_rand);
        
        if dist<best_dist
            best_dist=dist;
            best_u=[U_space(i,j,1) U_space(i,j,2)];
        end
        
    end
    
end

u=best_u;

end

