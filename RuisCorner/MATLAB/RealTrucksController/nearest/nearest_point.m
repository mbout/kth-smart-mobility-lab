function [ nearest_id ] = nearest_point(graph_pos,x_rand,k)
%UNTITLED p=euclidean distance of trailer in the xy-plane. d_theta is the
%difference of trailer angles. dist=p*(1+abs(sin(d_theta)))

distances=metric_distance_point(graph_pos,repmat(x_rand,[size(graph_pos,1) 1]));

[~,nearest_id]=sort(distances);

nearest_id=nearest_id(1:k);

end