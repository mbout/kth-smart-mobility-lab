function [ min_id ] = find_nearest( alpha , graph )
%FIND_NEAREST Summary of this function goes here
%   Detailed explanation goes here

%     it=graph.depthfirstiterator;
it=1:size(graph.Node);

min_id=0;
min_dist=10^10;

for node_id=it
    
    node=graph.get(node_id);
    
    distance=metric_distance(node,alpha);
    
    if distance<min_dist
        
        min_id=node_id;
        min_dist=distance;
        
    end
    
    
end

end

