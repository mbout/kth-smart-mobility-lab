function [ real_i ] = closest_reference_finder( x_curr, traj )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

%  = closest_reference_finder(x_curr, [x_r, y_r]);
    x_r = traj(:,1);
    y_r = traj(:,2);
    
    
    ahead_distance = 2.0/32;
    target_point = [x_curr(1)+ahead_distance*cos(x_curr(3)), x_curr(2)+ahead_distance*sin(x_curr(3))];
    
    target_point_repmat = repmat(target_point, [size(traj,1) 1]);

    dist = traj - target_point_repmat;
    dist = ( dist(:,1).^2 + dist(:,2).^2 ).^5;
    
    [~, real_i] = min(dist); 

end

