function [u,PID_struct] = PID_control(PID_struct,error,t_step)
%PID_CONTROL Summary of this function goes here
%   Detailed explanation goes here

    PID_struct.error_i=PID_struct.error_i+t_step*error;

    if PID_struct.Kd == 99
        
        u=PID_struct.Kp*error+...
        PID_struct.Ki*PID_struct.error_i;
    
        disp( strcat( 'PID_struct.error_i = ', num2str(PID_struct.error_i) ) )
        
    else
        
        u=PID_struct.Kp*error+...
        PID_struct.Ki*PID_struct.error_i+...
        PID_struct.Kd*(error-PID_struct.error_prev);
        
    end
    
    


    PID_struct.error_prev=error;
    
end

