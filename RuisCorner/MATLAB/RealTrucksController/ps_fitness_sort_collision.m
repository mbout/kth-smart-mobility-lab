function [ current_gen ] = ps_fitness_sort_collision(current_gen,waypoints,x_i,Obs)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

score=zeros(size(current_gen,1),1);

for i=1:size(current_gen,1)
%     disp(strcat('Individual #',num2str(i)))
    score(i)=ps_fitness_function_collision(waypoints,current_gen(i,:),x_i,Obs);
end

[score,I_sorted]=sort(score,'descend');
current_gen=current_gen(I_sorted,:);

end