function [score] = ps_fitness_function_collision(best_path,string_individual,x_i,Obs)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

phi=deg2rad(25);

L=0.115;
r=L/tan(phi);
stepSize=0.01;

waypoints=[best_path(1,:);best_path(find(string_individual)+1,:);best_path(end,:)];

new_path=[];

for i=2:size(waypoints,1)
    
    current_path=dubins(waypoints(i-1,:),waypoints(i,:),r,stepSize);
    
    if size(path,2)==1
        stepSize=0.001; %o.1
        current_path=dubins(waypoints(i-1,:),waypoints(i,:),r,stepSize);
        disp('Needed to reduce Dubins stepsize')
    end
    
    new_path=[new_path;current_path'];
    
end

xx=new_path(:,1); yy=new_path(:,2);
delta_x=diff(xx); delta_y=diff(yy);
distance_traveled=(delta_x.^2 + delta_y.^2).^.5;
v=distance_traveled;
distance_traveled=sum(distance_traveled);

w=diff(new_path(:,3));
phi=atan(L*(w./v));

x_curr=x_i;
t=1;
time_to_check=0;
for i=1:length(phi)
    
    x_curr=system_sim_truck_trailer(x_curr,[v(i) phi(i)],t);
    
    time_to_check=time_to_check+1;
    
    if time_to_check==10
        
        if collision_checker_truck_trailer(x_curr,Obs)
            score=-inf;
            temp_handle=plot(xx,yy,'r','LineWidth',2);
            drawnow
            pause(0.1)
            delete(temp_handle)
            return
        end
        
        time_to_check=0;
    end
    
end

d=0.166;
car_final_pos=[x_curr(:,1)+d*cos(x_curr(:,3)) x_curr(:,2)+d*sin(x_curr(:,3)) x_curr(:,4)];

score=-distance_traveled;

temp_handle=plot(xx,yy,'b','LineWidth',2);

drawnow
pause(0.01)
delete(temp_handle)

% plot(xx,yy)

end

