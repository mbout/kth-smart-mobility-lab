% RRT STUFF
clear
clc 

% goal=[1.5 -1.5 0 0]; % Define goal position
goal=[(-1).^round(0.5+.125*randn)*1.25 (-1).^round(0.5+.125*randn)*1.25 0 0]; % Define goal position

% INITIALIZATION STUFF
if exist('qtm','var')
    % Close connection
    QMC(qtm, 'disconnect');
    clear mex
end

if ~exist('qtm','var')
    % Open connection
    qtm=QMC('QMC_conf.txt');
end
if ~exist('s','var')
    s=NI_initialization();
    NI_voltage_stop(s)
end

[x_goal,y_goal,~,~] = get_pose_qualisys_id(qtm,16);
if ~isnan(x_goal)
    goal=[x_goal y_goal 0 0]; % Define goal position
end

trailer_volt=0; % Lock trailer

% if isunix
% addpath('collision_detector\','drawing\','map_creation\',...
%     'system_simulator\','nearest\','plot_graph\');
% end
addpath('collision_detector','drawing','map_creation',...
    'handle_loaders','misc','system_simulator','nearest','plot_graph');

space_width=4;
space_limits=[-space_width/2 space_width/2 -space_width/2 space_width/2];
% Obs=map_creation_lab_1(space_width,space_width/30);
% Obs={};
% width=1200
close all
% figure('Position',[2020 60 650 625])
figure(); hold on;
set(gcf, 'position', [1921,433,1024,692])
handle_fill=fill([space_limits(1) space_limits(1) space_limits(2) space_limits(2)],...
    [space_limits(3) space_limits(4) space_limits(4) space_limits(3)],...
    [173,255,47]/255);
plot([space_limits(1) space_limits(1) space_limits(2) space_limits(2) space_limits(1)],...
    [space_limits(3) space_limits(4) space_limits(4) space_limits(3) space_limits(3)],'r--')



Obs=map_creation_boxes_1(qtm);
% axis([-2 2 -2 2])
width=6.18;
x_offset=-2.36;
height=4.4;
y_offset=-2.4;
axis([x_offset x_offset+width y_offset y_offset+height])
draw_obstacle(Obs)

model_load_handle=@truck_trailer_load_handle;
u_1=0.1; tolerance=2.0*u_1;
u_2=deg2rad(15)*[-1 0 1];
[x_start,y_start,theta_start,~]=get_pose_qualisys(qtm);
[x_truck,y_truck,theta_truck,~] = get_pose_qualisys_id(qtm,15);
% x_i=[x_start y_start theta_start 0];
x_i=[x_truck y_truck theta_truck theta_start];

% plot(goal(1),goal(2),'go','MarkerSize',25,'LineWidth',20)
goal_circle_x=1.5*tolerance*cos([0:0.1:2*pi]); goal_circle_y=1.5*tolerance*sin([0:0.1:2*pi]);
plot(goal_circle_x+goal(1),goal_circle_y+goal(2),'r.');
draw_state_truck_trailer(x_i);

pause()

if isnan(x_i(1)) || isnan(x_i(4)) 
    error('Invalid Qualisys, check if truck is being detected!')
end

time_allowed=5;

str='test';
[coord_path,best_path]=RRT(space_width,space_limits,Obs,u_1,u_2,x_i,tolerance,goal,...
    str,model_load_handle,time_allowed);

d=0.15;
car_states=[best_path(:,1)+d*cos(best_path(:,3)) best_path(:,2)+d*sin(best_path(:,3)) best_path(:,4)];

plot(car_states(:,1),car_states(:,2),'r')
pause()

clf; hold on
handle_fill=fill([space_limits(1) space_limits(1) space_limits(2) space_limits(2)],...
    [space_limits(3) space_limits(4) space_limits(4) space_limits(3)],...
    [173,255,47]/255);
plot([space_limits(1) space_limits(1) space_limits(2) space_limits(2) space_limits(1)],...
    [space_limits(3) space_limits(4) space_limits(4) space_limits(3) space_limits(3)],'r--')
axis([x_offset x_offset+width y_offset y_offset+height])
draw_obstacle(Obs)
plot(goal_circle_x+goal(1),goal_circle_y+goal(2),'r.');
draw_state_truck_trailer(x_i);


d=0.15;
car_states=[best_path(:,1)+d*cos(best_path(:,3)) best_path(:,2)+d*sin(best_path(:,3)) best_path(:,4)];

string_sample=zeros(1,length(car_states)-2);

generation_size=20;
[first_gen]=ps_first_generation(string_sample,generation_size);

for generation=1:2
    
disp(generation);%     disp(' '); disp(strcat('Generation #',num2str(generation))); disp(' ');
    if generation==1
        current_gen=first_gen;
        continue;
    end
    
%     current_gen=ps_fitness_sort(current_gen,car_states);
    current_gen=ps_fitness_sort_collision(current_gen,car_states,x_i,Obs);
    current_gen=ps_natural_selection(current_gen);
        
end

last_gen=ps_fitness_sort_collision(current_gen,car_states,x_i,Obs);
last_score=ps_fitness_print(car_states,last_gen(1,:))

num_waypoints=100;

[xx,yy]=ps_winner_path(car_states,last_gen(1,:),num_waypoints);

coord_path=[xx yy];




delta_x=diff(coord_path(:,1)); delta_y=diff(coord_path(:,2));
distance_to_travel=(delta_x.^2 + delta_y.^2).^.5;
distance_to_travel=sum(distance_to_travel);
final_time=distance_to_travel/0.15;

% INTERPOLATE STUFF

x=coord_path(:,1); y=coord_path(:,2);
t_step=0.1;
t=0:t_step:final_time; t=t';
t_old=0:final_time/(length(x)-1):final_time;
x_r = spline(t_old,x,t); y_r = spline(t_old,y,t);
% h_path=figure; hold on;

L=0.115;

% Non Linear Parameters
csi=0.7; g=60;

% Gain tuning
gain_reduction_k1=0.25;
gain_reduction_k2=0.125;
gain_reduction_k3=0.125/2;

% PATH CREATION

% clf
hold on
delete(handle_fill)
axis([x_offset x_offset+width y_offset y_offset+height])
draw_obstacle(Obs); plot(goal_circle_x+goal(1),goal_circle_y+goal(2),'r.');
plot([space_limits(1) space_limits(1) space_limits(2) space_limits(2) space_limits(1)],...
    [space_limits(3) space_limits(4) space_limits(4) space_limits(3) space_limits(3)],'r--')

[x_start,y_start,theta_start,~]=get_pose_qualisys(qtm);
plot(x_r,y_r,'b'); % quiver(x_start,y_start,0.25*cos(theta_start),0.25*sin(theta_start))

x_r_d=[diff(x_r)/t_step; (x_r(end)-x_r(end-1))/t_step]; y_r_d=[diff(y_r)/t_step; (y_r(end)-y_r(end-1))/t_step];
x_r_dd=[diff(x_r_d)/t_step; (x_r_d(end)-x_r_d(end-1))/t_step]; y_r_dd=[diff(y_r_d)/t_step; (y_r_d(end)-y_r_d(end-1))/t_step];
theta_r=unwrap(atan2(y_r_d,x_r_d));
v_r=( x_r_d.^2 + y_r_d.^2 ).^.5;
w_r=( (x_r_d).*(y_r_dd.^2) - (y_r_d.^2).*(x_r_dd) )./ ...
    ( x_r_d.^2 + y_r_d.^2 );

if ~fancy_gui(max(v_r),rad2deg(max(w_r)),rad2deg(max(atan(0.115*(w_r./v_r)))))
%     close(h_path)
    return
end

x_hist=nan(size(x_r_d)); y_hist=nan(size(x_r_d)); theta_hist=nan(size(x_r_d));
time_hist=nan(size(x_r_d));
v_hist=nan(size(x_r_d)); v_hist(1:2)=0;
v_estimate=v_hist; w_hist=v_hist; w_estimate=v_hist; phi_hist=v_hist;

[x_q,y_q,theta_q,t_init]=get_pose_qualisys(qtm); t_stamp=t_init;
if theta_q<0
    theta_q=theta_q+2*pi;
end
x_i=[x_q y_q theta_q];
x_curr=x_i;
x_prev=x_curr;

% axis([min(x_r)-0.25 max(x_r)+0.25 min(y_r)-0.25 max(y_r)+0.25])
% axis(.5*space_width*[-1 1 -1 1])

current_elapsed=0;
velocity2voltage_handle=@velocity2voltage_no_stop_higher;

prev_error=inf;
for i=1:size(1.5*t)
    
    tic
    
    real_i=round(((t_stamp-t_init)+t_step-current_elapsed)/t_step);
    if real_i==0
        real_i=1; disp('Rounded to zero!')
    end
    if real_i>size(t,1)
        real_i=size(t,1); disp('Rounded to end!')
        disp('Should break, did not')
        error_test=tracking_error([x_r(real_i) y_r(real_i) theta_r(real_i)],x_curr);
        if norm(error_test(1:2))<0.05 || prev_error<norm(error_test(1:2))
            break
        end
        prev_error=norm(error_test(1:2));
%         break
    end
    % Save current postion for historic purposes
    x_hist(i)=x_curr(1); y_hist(i)=x_curr(2); theta_hist(i)=x_curr(3);
    theta_temp=unwrap(theta_hist(1:i));
    x_curr(3)=theta_temp(end);
    theta_hist(i)=x_curr(3);
    time_hist(i)=t_stamp-t_init;
    
    % Estimate velocities and steering angle
    if i>1
        v_current=norm(x_curr(1:2)-x_prev(1:2)); v_current=v_current/(time_hist(i)-time_hist(i-1));
        v_estimate(i)=v_current; v_hist(i)=median(v_estimate(max(1,i-2):max(1,i)));

        w_current=x_curr(3)-x_prev(3); w_current=w_current/(time_hist(i)-time_hist(i-1));
        w_estimate(i)=w_current; w_hist(i)=median(w_estimate(max(1,i-2):max(1,i)));

        phi_hist(i)=atan(L*(w_hist(i)/v_hist(i)));
    end
        
    error=tracking_error([x_r(real_i) y_r(real_i) theta_r(real_i)],x_curr); % Compute error

    %     Compute command
    %     u=linear_controller(error,v_r(i),w_r(i));
    k1=2*csi*sqrt(w_r(real_i)^2 + g*(v_r(real_i)^2));
    k3=k1;
    k2=g;
    k1=gain_reduction_k1*k1;
    k2=gain_reduction_k2*k2;
    k3=gain_reduction_k3*k1;
    
    K=[k1 k2 k3];

    u=non_linear_controller_params(error,v_r(real_i),w_r(real_i),K);
    u=pioneer_command_to_truck_SML_command(u); % Command conversion from unicycle to car
%     V_volt=velocity2voltage(u(1)); w_volt=steering2voltage(u(2));
%     V_volt=velocity2voltage_no_stop(u(1)); w_volt=steering2voltage(u(2));
    V_volt=velocity2voltage_handle(u(1)); w_volt=steering2voltage(u(2));

    if u(1)<=0
        disp('No movement forward')
    end
    
    %     Send command to truck
    NI_voltage_output(s,V_volt,w_volt,trailer_volt);
    
    x_prev=x_curr;
    %     Get current position
    [x_q,y_q,theta_q,t_stamp]=get_pose_qualisys(qtm);
    
    if ~isnan(x_q) % Handle possible invalid values
        if theta_q<0 % Put theta in range [0,2*pi]
            theta_q=theta_q+2*pi;
        end
        x_curr=[x_q y_q theta_q];
    else % If invalid reading, use previous reading
        x_curr=x_prev;
        disp('Invalid Qualisys') % Issue warning to the user
    end

    % Plot stuff
    if real_i>=2
        plot([x_r(real_i-1) x_r(real_i)],[y_r(real_i-1) y_r(real_i)],'g')
        delete(h_pioneer);
        delete(h_ref);
    end
    plot([x_prev(1) x_curr(1)],[x_prev(2) x_curr(2)],'r')
    h_ref=plot(x_r(real_i),y_r(real_i),'go');
    h_pioneer=plot(x_curr(1),x_curr(2),'ro');

    % Sampling time stuff
    current_elapsed=toc;
    pause(t_step-toc)
    
end

NI_voltage_stop(s)

if exist('qtm','var')
    % Close connection
    QMC(qtm, 'disconnect');
    clear mex
end

existing_mats=dir('*.mat'); existing_mats=length(existing_mats);
save_str=strcat('non_linear_parameter_config_num_',num2str(existing_mats+1),'.mat');
save(save_str);