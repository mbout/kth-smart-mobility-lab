function [x_rand] = get_x_rand_point_model(goal,space_width,collision_checker_handle, ...
    Obs)
%GET_X_RAND Summary of this function goes here
%   Detailed explanation goes here

    if (rand<1/20)
        x_rand=goal;
    else
        x_rand=[(rand(1,2)-0.5)*space_width];
        while collision_checker_handle(x_rand,Obs)
            x_rand=[(rand(1,2)-0.5)*space_width];
        end
    end


end

