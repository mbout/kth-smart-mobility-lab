function [ outputString ] = javaCreateCommandMessage( throttle, steering )
%CREATEXML Summary of this function goes here
%   Detailed explanation goes here

    import java.io.File;
    import java.io.IOException;
    import java.io.StringReader;
    import java.io.StringWriter;
    import java.net.*;
    import java.util.ArrayList;
    import java.util.Collections;
    import java.util.Enumeration;
    import java.util.List;
    import javax.xml.parsers.DocumentBuilder;
    import javax.xml.parsers.DocumentBuilderFactory;
    import javax.xml.parsers.ParserConfigurationException;
    import javax.xml.transform.Transformer;
    import javax.xml.transform.TransformerException;
    import javax.xml.transform.TransformerFactory;
    import javax.xml.transform.dom.DOMSource;
    import javax.xml.transform.stream.StreamResult;
    import org.w3c.dom.Attr;
    import org.w3c.dom.Document;
    import org.w3c.dom.Element;
    import org.w3c.dom.NodeList;
    import org.xml.sax.InputSource;
    import org.xml.sax.SAXException;
   
    docFactory = DocumentBuilderFactory.newInstance();
    docBuilder = docFactory.newDocumentBuilder();

    % root elements
    doc = docBuilder.newDocument();
    rootElement = doc.createElement('message');

%     attr = doc.createAttribute('type');
%     attr.setValue('task_completed');
%     rootElement.setAttributeNode(attr);

    doc.appendChild(rootElement);

    
    start = doc.createElement('commands');
    
    throttleAttr = doc.createAttribute('throttle');
    throttleAttr.setValue( num2str(throttle) );
    start.setAttributeNode(throttleAttr);
    
    steeringAttr = doc.createAttribute('steering');
    steeringAttr.setValue( num2str(steering) );
    start.setAttributeNode(steeringAttr);
    
    rootElement.appendChild(start);

    % write the content into xml file
    transformerFactory = TransformerFactory.newInstance();
    transformer = transformerFactory.newTransformer();
%        DOMSource source = new DOMSource(doc);
%        StreamResult result = new StreamResult(new File('C:\\file.xml'));

    writer = StringWriter();
    transformer.transform(DOMSource(doc), StreamResult(writer));
    outputString = writer.getBuffer().toString().replaceAll('\n|\r', '');


end

