function [ last_line, num_lines ] = getLastLine( line )
%GETMOSTRECENTCOMMANDS Summary of this function goes here
%   Detailed explanation goes here

    line_str = char(line)';

    num_lines = sum(line==10);
    
%     commands = zeros(num_lines, 3);

    for i = 1:num_lines-1

        [~, line_str]=strtok(line_str, char(10));
    
    end

    last_line = line_str;
    
    if ( sum(int8(last_line)==10) > 1 )
       
        % There is more than one line
        [before_last_line, ~]=strtok(last_line, char(10));
        last_line = before_last_line;
        
    end
       

end

