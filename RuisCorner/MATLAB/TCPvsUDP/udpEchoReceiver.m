clear; close all;

% my_ip = '130.237.50.246'; % Mine
my_ip = '130.237.43.135'; % Pedro's
% my_ip = '130.237.43.176'; % Matteo's
% my_ip = '130.237.43.200'; % Internet
host_port = 10000;

desired_rate = 200;

t = udp(my_ip, host_port, 'LocalPort', 9090);

t.Timeout = 1/(2*desired_rate);

global exitButtonPressed
exitButtonPressed = false;
% exitGui

fopen(t);

time_stamp_timer = tic;

iterations = 0;
total_time = 0;

delay_sum = 0;
num_iterations = 0;

while 1

    timer = tic;
    
%     disp('Cycle')
    
    fprintf(t, strcat(num2str(toc(time_stamp_timer)), '\n' ));
    
%     try
    available_bytes = t.BytesAvailable;

%     disp(available_bytes)
    
    if ( available_bytes ~= 0 )
%         fscanf(t);
%         line = fgetl(t);

        line = fread(t, available_bytes);
        line_num = str2num(char(line)');
        delay = toc(time_stamp_timer) - line_num;
        disp(available_bytes)
%         disp(delay)
        
        delay_sum = delay_sum + sum(delay);
        num_iterations = num_iterations + length(delay);
        disp(delay_sum/num_iterations)
        
%         disp(line_num)
%         disp('line_num')
%         
%         disp(delay)
%         total_time = total_time + delay;
%         iterations = iterations + 1;
%         disp('line_num = ')
%         disp(line_num)
%         disp('toc(time_stamp_timer) = ')
%         disp(toc(time_stamp_timer))
    end
      
    
%     disp('Cycle')

    time_elapsed = double(toc(timer));
    if time_elapsed > 1/desired_rate
        disp('Overtime')
    else 
        pause(1/desired_rate - time_elapsed)
    end
    
end

fclose(t);