clear

load line

line_str = char(line)'

num_lines = sum(line==10);

commands = zeros(num_lines, 3);

for i = 1:num_lines

[command, line_str]=strtok(line_str, char(10));
% 
% disp('command')
% disp(command)

[time, rem]=strtok(command, 'time:');
[speed, rem]=strtok(rem, ':v:');
[phi, rem]=strtok(rem, ':phi:');

commands(i, 1) = str2num(time);
commands(i, 2) = str2num(speed);
commands(i, 3) = str2num(phi);

end

[~, i] = max(commands(:, 1));

speed = commands(i, 2)
phi = commands(i, 3)

