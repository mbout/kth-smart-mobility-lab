# Echo server program
import socket
import time
import random


HOST = ''                 # Symbolic name meaning all available interfaces
PORT = 8000              # Arbitrary non-privileged port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

print "Started! now"

s.bind((HOST, PORT))
s.listen(1)
conn, addr = s.accept()
print 'Connected by', addr

desired_output_rate = 200.0


data = conn.recv(1024)

print "data = " + str(data)

initial_time = time.time()

conn.settimeout((1./desired_output_rate)/2.)

while 1:

	# print "Cycle"
	start_cycle_time = time.time()

	# data = conn.recv(1024)
	# if not data: break


	data = str( time.time() - initial_time ) + '\n'

	try:

		conn.sendall(data)

	except:
		
		break

	print "Sent: " + data

	cycle_time = time.time() - start_cycle_time

	if cycle_time > 0.0: 
		print "1/cycle_time: " + str(1/cycle_time)

	time_to_sleep = 1./desired_output_rate - cycle_time
	print "time_to_sleep: " + str(time_to_sleep)

	if time_to_sleep > 0.0:
		time.sleep(time_to_sleep)


conn.close()
print "Socket closed"