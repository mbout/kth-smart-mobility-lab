clear; close all;

ip_address = '130.237.50.246'; % Mine
% ip_address = '130.237.43.135'; % Pedro's
% ip_address = '130.237.43.176'; % Matteo's
% ip_address = '130.237.43.200'; % Internet
host_port = 10000;

desired_rate = 50;

t = udp(ip_address, host_port);

t.Timeout = 1/(2*desired_rate);

input_buffer_size = 4*1024;

t.InputBufferSize = input_buffer_size;

global exitButtonPressed
exitButtonPressed = false;
% exitGui

fopen(t);

fwrite(t, 'HERE I AM');

time_stamp_timer = tic;

iterations = 0;
total_time = 0;

delay_sum = 0;
num_iterations = 0;

while 1

    timer = tic;
        
%     try
    

%     disp(available_bytes)
    
    
    while 1
        
        available_bytes = t.BytesAvailable;
        
        if ( available_bytes ~= 0 )
    %         fscanf(t);
    %         line = fgetl(t);

            if ( available_bytes == input_buffer_size )

                disp('Buffer full!')

            else

%                 disp(available_bytes)

            end

            line = fread(t, available_bytes);
            line_num = str2num(char(line)');

        else
            
            break
            
        end

    end
      
    
%     disp('Cycle')

    time_elapsed = double(toc(timer));
    if time_elapsed > 1/desired_rate
        disp('Overtime')
    else 
        pause(1/desired_rate - time_elapsed)
    end
    
end

fclose(t);