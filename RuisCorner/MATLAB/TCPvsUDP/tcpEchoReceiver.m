if exist('t', 'var')
    fclose(t);
end

clear; close all;

my_ip = '130.237.50.246'; % Mine
% my_ip = '130.237.43.135'; % Pedro's
% my_ip = '130.237.43.176'; % Matteo's
% my_ip = '130.237.43.200'; % Internet

host_port = 50007;

desired_rate = 200;

t = tcpip(my_ip, host_port);

t.Timeout = 1/(desired_rate*2);

global exitButtonPressed
exitButtonPressed = false;
% exitGui

fopen(t);

time_stamp_timer = tic;

delay_sum = 0;
num_received_packets = 0;
num_sent_packets = 0;
num_iterations = 0;
num_buffer_reads = 0;


while 1
        
    timer = tic;
        
    current_time = toc(time_stamp_timer);
    
    num_iterations = num_iterations + 1;
    
%     fprintf(t, strcat(num2str(current_time), '\n' ));
    fprintf(t, num2str(current_time) );
%     fprintf(t, strcat(num2str(current_time,'% 10.4f'), '\n') );
%     fprintf(t, strcat('0.001', '\n') );
    num_sent_packets = num_sent_packets + 1;

    available_bytes = t.BytesAvailable;
    
    if ( available_bytes ~= 0 )
        
        num_buffer_reads = num_buffer_reads + 1;

        line = fread(t, available_bytes);
%         line_num = str2double(char(line)');
        [ commands, num_lines ] = getAllLines( line );
        delay = toc(time_stamp_timer) - commands;
                
        if sum(isnan(delay)) == 0
                                   
            delay_sum = delay_sum + sum(delay);
            num_received_packets = num_received_packets + length(delay);

            disp(['Average delay: ',num2str(delay_sum/num_received_packets) ])
            disp(['Average Received Packet Frequency: ',num2str(num_received_packets/toc(time_stamp_timer)) ])
            disp(['Average Sent Packet Frequency: ',num2str(num_sent_packets/toc(time_stamp_timer)) ])
            disp(['Average Iteration Frequency: ',num2str(num_iterations/toc(time_stamp_timer)) ])
            disp(['Average Packets Per Read: ',num2str(num_received_packets/num_buffer_reads) ])
           
        else
            
            disp('Isnan')
            disp(delay)
%             error()
                        
        end
        
        
        
    end
       

   
    time_elapsed = double(toc(timer));
    
    if time_elapsed > 1/desired_rate
        disp('Overtime')
    else 
        pause(1/desired_rate - time_elapsed)
    end
    
end

fclose(t);