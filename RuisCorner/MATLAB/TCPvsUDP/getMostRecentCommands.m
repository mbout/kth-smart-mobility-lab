function [ speed, phi, num_lines ] = getMostRecentCommands( line )
%GETMOSTRECENTCOMMANDS Summary of this function goes here
%   Detailed explanation goes here

    line_str = char(line)';

    num_lines = sum(line==10);
    
    commands = zeros(num_lines, 3);

    for i = 1:num_lines

    [command, line_str]=strtok(line_str, char(10));
    % 
    % disp('command')
    % disp(command)

    [time, rem]=strtok(command, 'time:');
    [speed, rem]=strtok(rem, ':v:');
    [phi, ~]=strtok(rem, ':phi:');

    commands(i, 1) = str2double(time);
    commands(i, 2) = str2double(speed);
    commands(i, 3) = str2double(phi);

    end

    [~, i] = max(commands(:, 1));

    speed = commands(i, 2);
    phi = commands(i, 3);


end

