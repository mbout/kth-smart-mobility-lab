

desired_rate = 10;
num_iterations = 0;

initial_timer = tic;
prev_time = 0;

while 1
    
    
%     disp(['Loop start: ',num2str(toc(initial_timer))]);
    current_time = toc(initial_timer);
%     disp(['Current frequency: ',num2str( 1/(current_time-prev_time) ) ] )
    prev_time = current_time;

    iteration_timer = tic;
    
    num_iterations = num_iterations + 1;
    
%     disp(['Average frequency: ',num2str(num_iterations/toc(initial_timer) ) ] )
    
    average_frequency = num_iterations/toc(initial_timer);

    elapsed_time = double(toc(iteration_timer));
    
    if elapsed_time > (1/desired_rate)
       disp('Overtime')        
    else
        pause((1/desired_rate) - elapsed_time)        
    end
        
%     disp(['Loop end: ',num2str(toc(initial_timer))]);
    
end