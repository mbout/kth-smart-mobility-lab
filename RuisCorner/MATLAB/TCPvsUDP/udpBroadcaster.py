import socket
import sys
import time

print "Started!"

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# Bind the socket to the port
server_address = ('', 10000)
# server_address = ('localhost', 10000)
# print >>sys.stderr, 'starting up on %s port %s' % server_address
sock.bind(server_address)
# Messages are read from the socket using recvfrom(), which returns the data as well as the address of the client from which it was sent.

print "Waiting for an initial message from a client"

data, address = sock.recvfrom(4096)

print "Client connected"

# UDP_IP = "127.0.0.1"
# UDP_PORT = 5005

initial_time = time.time()
packets_received = 0

broadcast_rate = 200.

while True:
	

	start_cycle_time = time.time()

	current_time = start_cycle_time - initial_time
	data = str(current_time) + '\n'

	sent = sock.sendto(data, address)

	print "Sent " + data

	cycle_time = time.time() - start_cycle_time

	if cycle_time == 0:

		time.sleep(1/broadcast_rate)
		continue

	if cycle_time < (1/broadcast_rate): 

		time.sleep(1/broadcast_rate - cycle_time)
		continue

	print "Overtime"