# Echo server program
import socket
import time
import random

HOST = ''                 # Symbolic name meaning all available interfaces
PORT = 50007              # Arbitrary non-privileged port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(1)
conn, addr = s.accept()
print 'Connected by', addr

desired_output_rate = 1000.0

initial_time = time.time()
packets_sent = 0

try:

	while 1:

		# print "Cycle"
		start_cycle_time = time.time()

		# data = conn.recv(1024)
		# # if not data: break
		# # conn.sendall(data)
		# # print "Sent: " + data

		v_command = random.random()
		phi_command = random.random()
		time_stamp = time.time() - initial_time

		data = "time:"+str(time_stamp)+":v:"+str(v_command)+":phi:"+str(phi_command)
		conn.sendall(data+'\n')

		cycle_time = time.time() - start_cycle_time

		time_to_sleep = 1./desired_output_rate - cycle_time

		packets_sent = packets_sent + 1

		if packets_sent%1000 == 0:

			print "Sent " + str(packets_sent) + " packets."


		if time_to_sleep > 0.0:
			time.sleep(time_to_sleep)
		else:
			print "overtime"



except:

	print "Sent " + str(packets_sent) + " packets."
	print "Error caugh and connection closed"
	conn.close()

print "Sent " + str(packets_sent) + " packets."
conn.close()