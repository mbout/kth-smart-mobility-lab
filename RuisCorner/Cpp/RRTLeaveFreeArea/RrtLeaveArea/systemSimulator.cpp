#include "systemSimulator.h"
#include "distanceComputer.h"
#include "collision.h"
#include <cmath>        // std::abs
#include <stdlib.h>
#include <iostream>

// MISCELLANEOUS


float get_random_float_between(float a, float b) {
    
    float random = ((float) rand()) / (float) RAND_MAX;
    float diff = b - a;
    float r = random * diff;
    return a + r;
    
}

void get_random_state(float *random_state, float *goal_state){
    
    do
    {
        if ( get_random_float_between(0, 20) < 1.0 ){
        
            random_state[0] = goal_state[0];
            random_state[1] = goal_state[1];
            random_state[2] = goal_state[2];
            random_state[3] = goal_state[3];
        
        }else{

            random_state[0] = get_random_float_between(GLOBAL_MIN_X, GLOBAL_MAX_X);
            random_state[1] = get_random_float_between(GLOBAL_MIN_Y, GLOBAL_MAX_Y);
            random_state[2] = get_random_float_between(0, 2*M_PI);
            random_state[3] = get_random_float_between(0, 2*M_PI);
        //    random_state[3] = get_random_float_between(0,2*M_PI);

        }
        
    } while ( is_in_collision(random_state) );
    
    
    
       
}




// SYSTEM SIMULATOR
void system_equation(float *initial_state, float *final_state, float *inputs, float time_step){
    
//    final_state[0] = initial_state[0] + cos(initial_state[2])*inputs[0]*time_step;
//    final_state[1] = initial_state[1] + sin(initial_state[2])*inputs[0]*time_step;
//    final_state[2] = initial_state[2] + inputs[1]*time_step;
    float truck_speed = inputs[0];
    float trailer_speed = cos(initial_state[3] - initial_state[2])*truck_speed;
    float phi = inputs[1];
    
    float truck_length = TRUCK_LENGTH;
    float trailer_length = TRAILER_LENGTH;
        
    final_state[0] = initial_state[0] + cos(initial_state[2])*trailer_speed*time_step;
    final_state[1] = initial_state[1] + sin(initial_state[2])*trailer_speed*time_step;
    final_state[2] = initial_state[2] + (1/trailer_length)*sin(initial_state[3] - initial_state[2])*truck_speed*time_step;
    final_state[3] = initial_state[3] + tan(phi)*(truck_speed/truck_length)*time_step;
        
}

void euler_integration(float *initial_state, float *final_state, float *inputs){
    
    float time_step = EULER_SIMULATION_TIME/((float) EULER_NUMBER_STEPS);
    
//    float intermediate_state[STATE_DIMENSION];
//    for (int j = 0; j < STATE_DIMENSION; j++ ){
    float intermediate_state[4];
    for (int j = 0; j < 4; j++ ){
            
        intermediate_state[j] = initial_state[j];
            
    }
    
    for (int i = 0; i < EULER_NUMBER_STEPS; i++ ){
        
        system_equation(intermediate_state, final_state, inputs, time_step);
//        std::cout << final_state[0] << " " << final_state[1] << " " << final_state[2] << " " << final_state[3] << " " << std::endl;  
//        std::cout << "inputs[1]" << inputs[1] << std::endl; 
//        std::cout << intermediate_state[0] << " " << intermediate_state[1] << " " << intermediate_state[2] << std::endl;  
        
//        for (int j = 0; j < STATE_DIMENSION; j++ ){
        for (int j = 0; j < 4; j++ ){
            
            intermediate_state[j] = final_state[j];
            
        }
                
    }  
    
    if (final_state[2] > 2*M_PI){
        final_state[2] = final_state[2] - 2*M_PI;
        return;
    }
    
    if (final_state[2] < 0){
        final_state[2] = final_state[2] + 2*M_PI;
        return;
    }
    
}



bool find_best_input(float *initial_state, float *goal_state, float *inputs){
    
    int number_steps = 10;
    float simulation_time = EULER_SIMULATION_TIME;
    
    float steering_inputs[5];
    
    steering_inputs[0] = -MAX_STEERING_ANGLE; steering_inputs[1] = -MAX_STEERING_ANGLE/2;
    steering_inputs[2] = 0;
    steering_inputs[3] = MAX_STEERING_ANGLE/2; steering_inputs[4] = MAX_STEERING_ANGLE;
    
//    float velocity_inputs[4];
//    
//    velocity_inputs[0] = -1; velocity_inputs[1] = -0.5;
//    velocity_inputs[2] = 0.5; velocity_inputs[3] = 1;
    
    float velocity_inputs[4];
    
    velocity_inputs[0] = 0.25; velocity_inputs[1] = 0.5;
    velocity_inputs[2] = 0.75; velocity_inputs[3] = 1;
    
    
    
//    float final_state[STATE_DIMENSION];
    float final_state[4];
    
    float min_distance = 1000;
    float best_input[2];
    
    bool possible_movement = false;
    
//    std::cout << "goal_state: " << goal_state[0] << " " << goal_state[1] << " " << goal_state[2] << " " << std::endl;
    for ( int j = 0; j < 4; j++ ){
        
        inputs[0] = velocity_inputs[j];
    
        for ( int i = 0; i < 5; i++ ){

            inputs[1] = steering_inputs[i];
            euler_integration(initial_state, final_state, inputs);

            float distance;

    //        std::cout << "initial_state: " << initial_state[0] << " " << initial_state[1]  << " " << initial_state[2] << std::endl;
    //        std::cout << "testing inputs: " << inputs[0] << " " << inputs[1] << std::endl;
    //        std::cout << "final_state: " << final_state[0] << " " << final_state[1]  << " " << final_state[2] << std::endl;

            distance = get_distance(final_state, goal_state);

            if (distance < min_distance && !is_in_collision(final_state) ){

    //            std::cout << "FOUND A BETTER CHOICE " << steering_inputs[i] << std::endl;
                min_distance = distance;
                best_input[0] = velocity_inputs[j];
                best_input[1] = steering_inputs[i];
                possible_movement = true;

            }


        }
    
    }
//    std::cout << "------>best_input " << best_input << std::endl;
    
    inputs[0] = best_input[0];
    inputs[1] = best_input[1]; 
    
    return possible_movement;
    
}