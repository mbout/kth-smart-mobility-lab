#include "distanceComputer.h"
//#include <math.h>
//#include <iostream>
#include <cmath>     


float get_metric_distance(float *state_a, float *state_b){
    
    // Normal euclidian distance
    
    // CORRECT THIS!!!!
//    std::cout << STATE_DIMENSION << std::endl;
    
    float distance = 0;
    
    distance = distance + std::abs( state_a[0] - state_b[0] );
    distance = distance + std::abs( state_a[1] - state_b[1] );
    
    float angle_difference = std::abs( state_a[2] - state_b[2] );
    while (angle_difference > 2*M_PI){
        angle_difference = angle_difference - 2*M_PI;
    }
    distance = distance + angle_difference;
    
    angle_difference = std::abs( state_a[3] - state_b[3] );
    while (angle_difference > 2*M_PI){
        angle_difference = angle_difference - 2*M_PI;
    }
    distance = distance + angle_difference;
    
    return distance;
        
}


float get_two_point_metric_xy_distance(float *state_a, float *state_b){
    
    // Normal euclidian distance
    
    // CORRECT THIS!!!!
//    std::cout << STATE_DIMENSION << std::endl;
    
    float state_a_front_pos[2];
    state_a_front_pos[0] = state_a[0] + TRAILER_LENGTH*cos(state_a[2]) + TRUCK_LENGTH*cos(state_a[3]);
    state_a_front_pos[1] = state_a[1] + TRAILER_LENGTH*sin(state_a[2]) + TRUCK_LENGTH*sin(state_a[3]);
    
    float state_b_front_pos[2];
    state_b_front_pos[0] = state_b[0] + TRAILER_LENGTH*cos(state_b[2]) + TRUCK_LENGTH*cos(state_b[3]);
    state_b_front_pos[1] = state_b[1] + TRAILER_LENGTH*sin(state_b[2]) + TRUCK_LENGTH*sin(state_b[3]);
    
    float distance = 0;
    
    for (int i = 0; i < 2; i++){
        
        distance = distance + std::abs( state_a_front_pos[i] - state_b_front_pos[i] );
    
    }
    
    for (int i = 0; i < 2; i++){
        
        distance = distance + std::abs( state_a[i] - state_b[i] );
    
    }
    
    return distance;
        
}

float get_xy_distance(float *state_a, float *state_b){
    
    // Normal euclidian distance
    
    float distance = 0;
    
    for (int i = 0; i < 2; i++){
        
        distance = distance + std::abs( state_a[i] - state_b[i] );
    
    }
    
    return distance;
        
}

float get_car_front_distance(float *state_truck, float *xy){
    
    // Normal euclidian distance
    
    float distance = 0;
    
    float car_front_pos[2];
    
    car_front_pos[0] = state_truck[0] + TRAILER_LENGTH*cos(state_truck[2]) + TRUCK_LENGTH*cos(state_truck[3]);
    car_front_pos[1] = state_truck[1] + TRAILER_LENGTH*sin(state_truck[2]) + TRUCK_LENGTH*sin(state_truck[3]);
    
    
    for (int i = 0; i < 2; i++){
        
        distance = distance + std::abs( car_front_pos[i] - xy[i] );
    
    }
    
    return distance;
        
}

float get_tractor_pose_distance(float *state_a, float *state_b){
    
    // Normal euclidian distance
    
    float state_a_front_pos[3];
    state_a_front_pos[0] = state_a[0] + TRAILER_LENGTH*cos(state_a[2]) + TRUCK_LENGTH*cos(state_a[3]);
    state_a_front_pos[1] = state_a[1] + TRAILER_LENGTH*sin(state_a[2]) + TRUCK_LENGTH*sin(state_a[3]);
    state_a_front_pos[2] = state_a[3];
    
    float state_b_front_pos[2];
    state_b_front_pos[0] = state_b[0] + TRAILER_LENGTH*cos(state_b[2]);
    state_b_front_pos[1] = state_b[1] + TRAILER_LENGTH*sin(state_b[2]);
    state_b_front_pos[2] = state_b[3];
    
    float distance = 0;
    
    // angles are in radians
            
    distance = distance + std::abs( state_a_front_pos[0] - state_b_front_pos[0] );
    distance = distance + std::abs( state_a_front_pos[1] - state_b_front_pos[1] );
    distance = distance + 0.1*std::abs( state_a_front_pos[2] - state_b_front_pos[2] );
       
    
    return distance;
        
}



float get_distance(float *state_a, float *state_b){
    
    // In order to quickly change distance heuristics
    // This function is always called for computing the distance
        
    // Change the heuristic by changing the following function
//    return get_metric_distance(state_a, state_b);
//    return get_two_point_metric_xy_distance(state_a, state_b);
    return get_tractor_pose_distance(state_a, state_b);
//    return get_xy_distance(state_a, state_b);
    
//    float xy_point[2];
//    xy_point[0] = state_b[0];
//    xy_point[1] = state_b[1];
        
//    return get_car_front_distance(state_a, xy_point);
            
}


bool goal_check(float *state_a, float *state_b){
    
    if ( get_distance(state_a, state_b) < GOAL_TOLERANCE ){
        
        return true;
        
    }
    
    return false;
    
}