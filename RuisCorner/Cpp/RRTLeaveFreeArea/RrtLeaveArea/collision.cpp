#include "collision.h"
#include <math.h>

#include <iostream>

bool is_inside_square_check(float *current_state, float x, float y, float w, float h){
    
    if ( current_state[0] > x && current_state[0] < x + w && current_state[1] > y && current_state[1] < y + h ){
        
        return true;
        
    }
    
    return false;  
    
}

bool is_in_collision_1_square(float *current_state){
    
    std::cout << "MAX_STEERING_ANGLE: " << MAX_STEERING_ANGLE << std::endl;
    
    float x_square = 10;
    float y_square = 10;
    float w_square = 70;
    float h_square = 70;
    
    return is_inside_square_check(current_state, x_square, y_square, w_square, h_square);
    
}

bool is_in_collision_4_squares(float *current_state){
    
    float x_square = -80;
    float y_square = -70;
    float w_square = 30;
    float h_square = 30;
    if ( is_inside_square_check(current_state, x_square, y_square, w_square, h_square) ){
        return true;
    }
    
    x_square = 50;
    y_square = 40;
//    w_square = 70; 
//    h_square = 70;
    if ( is_inside_square_check(current_state, x_square, y_square, w_square, h_square) ){
        return true;
    }
    
    x_square = -15;
    y_square = -15;
//    w_square = 70; 
//    h_square = 70;
    if ( is_inside_square_check(current_state, x_square, y_square, w_square, h_square) ){
        return true;
    }
    
    x_square = 50;
    y_square = 80;
//    w_square = 70; 
//    h_square = 70;
    if ( is_inside_square_check(current_state, x_square, y_square, w_square, h_square) ){
        return true;
    }
    
    return false;
    
}


bool is_in_collision_square_obstacles_list(float *current_state){
    
    SquareObstacle temp_obstacle;
    
    for (int i = 0; i < obstacles_list_global.size(); i++){
        
        temp_obstacle = obstacles_list_global.at(i);
        if ( is_inside_square_check(current_state, temp_obstacle.x, temp_obstacle.y, temp_obstacle.w, temp_obstacle.h) ){
            return true;
        }
        
    }
    
    return false;
    
}

bool is_in_collision_boxes_vector(float *current_state){
    
    float xy_point[2];
    
    xy_point[0] = current_state[0];
    xy_point[1] = current_state[1];
    
    bool is_inside = 0;
    bool inside_one_obstacle = 0;
    
    for ( int i = 0; i < box_obstacles_vector_global.size(); i++ ){
        
        BoxObstacle temp_box_obstacle = box_obstacles_vector_global.at(i);
        bool is_inside = 1;
                
        for ( int j = 0; j < 4; j++ ){
            
            if ( temp_box_obstacle.bigger[j] == 1 ){
                
                is_inside = xy_point[1] > temp_box_obstacle.m[j]*xy_point[0] + temp_box_obstacle.b[j];
                
            }else{
                
                is_inside = xy_point[1] < temp_box_obstacle.m[j]*xy_point[0] + temp_box_obstacle.b[j];
                
            }
            
            if ( is_inside == 0 ){
                break;
            }else{
                
            }
                       
            
        }
        
        if ( is_inside == 1){
//            std::cout << "Inside obstacle number " << i << std::endl;
            return 1;
        }
        
        
    }
    
    return 0;
    
}

bool is_in_collision_square_obstacles_list_truck_and_trailer(float *current_state){
    
    if ( is_in_collision_boxes_vector(current_state) ){
        return 1;
    }
    
    float car_front_pos[2];
    
    car_front_pos[0] = current_state[0] + TRAILER_LENGTH*cos(current_state[2]) + TRUCK_LENGTH*cos(current_state[3]);
    car_front_pos[1] = current_state[1] + TRAILER_LENGTH*sin(current_state[2]) + TRUCK_LENGTH*sin(current_state[3]);
    
    if ( is_in_collision_boxes_vector(car_front_pos) ){
        return 1;
    }
    
    return 0;
        
}



bool is_in_collision(float *current_state){
    
//    return is_in_collision_1_square(current_state);
//    return is_in_collision_4_squares(current_state);
//    if ( abs(current_state[2] - current_state[3]) > M_PI/4. ){
//        return true;
//    }
    
    
    return is_in_collision_square_obstacles_list_truck_and_trailer(current_state);
    
}