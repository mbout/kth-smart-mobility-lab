import sys, string, os, math, time
import numpy
import pygame
import xml.etree.ElementTree as ET
from xml.dom import minidom

FILEPATH = "RRTLeaveArea\dist\Debug\Cygwin_4.x-Windows\\"


def make_file_empty(filename):

	f = open(filename, 'w')

	f.write("")

	f.close()


def make_empty_file(filename):

	f = open(filename, 'w')

	f.write("")

	f.close()


def read_tree_nodes_file(filename):

	f = open(filename, 'r')

	tree_nodes = []

	for line in f:

		current_node = []

		numbersList = line.split()

		current_node.append( float(numbersList[0]) ) # the x-position 
		current_node.append( float(numbersList[1]) ) # the y-position 
		current_node.append( int(numbersList[2]) ) # the parent id

		tree_nodes.append( current_node )

	# print tree_nodes

	return tree_nodes

def read_tree_solution_file(filename):

	f = open(filename, 'r')

	tree_solution = []

	for line in f:
		
		tree_solution.append( int( line ) ) # the parent id

	# print tree_nodes

	return tree_solution

def get_rrt_arguments(initial_pose=[1.,1.,1.,1.], goal_coord=[-1.,-1.], space_limits=[-2.,2.,-2.,2.], goal_tolerance=.1, euler_sim_time=.1, max_steering_angle=.35):

	x = initial_pose[0]
	y = initial_pose[1]
	theta_0 = initial_pose[2]
	theta_1 = initial_pose[3]

	goal_x = goal_coord[0]
	goal_y = goal_coord[1]

	space_lim_min_x = space_limits[0]
	space_lim_max_x = space_limits[1]
	space_lim_min_y = space_limits[2]
	space_lim_max_y = space_limits[3]

	goal_tolerance = 0.1
	euler_sim_time = 0.1
	max_steering_angle = math.radians(20)

	args_string = str(x) + str(' ') + str(y) + str(' ') + str(theta_0) + str(' ') +	str(theta_1)
	args_string = args_string + str(' ') + str(goal_x) + str(' ') + str(goal_y)
	args_string = args_string + str(' ') + str(space_lim_min_x) + str(' ') + str(space_lim_max_x) + str(' ') + str(space_lim_min_y) + str(' ') + str(space_lim_max_y)	
	args_string = args_string + str(' ') + str(goal_tolerance) + str(' ') + str(euler_sim_time) + str(' ') + str(max_steering_angle)

	return args_string


def get_tree_surface(canvas_width, canvas_height, initial_pose, goal_position, tree_nodes, solution_nodes, space_limits, obstacles_list):

	resized_tree_nodes = []

	rescale_x = canvas_width/( space_limits[1] - space_limits[0] )
	rescale_y = canvas_height/( space_limits[3] - space_limits[2] )

	for tree_node in tree_nodes:

		resized_tree_nodes.append( [ (tree_node[0]-space_limits[0])*rescale_x, (-tree_node[1]-space_limits[2])*rescale_y, tree_node[2] ] )


	world_surface = pygame.Surface((canvas_width, canvas_height), 0, 32)

	fill_color = (0, 0, 0)
	world_surface.fill(fill_color)



	for obstacle in obstacles_list:

		pointlist = []

		# [a, b, c, d] = obstacle[2]


		print "obstacle[2] = " + str( obstacle[2] )
		resized_obstacle_points = []
		for temp_point in obstacle[2]:

			resized_obstacle_points.append( ( (temp_point[0]-space_limits[0])*rescale_x, (-temp_point[1]-space_limits[2])*rescale_y ) )


		# pointlist.append( ( pos_x - cos(theta)*margin , pos_x - sin(theta)*margin ) ) 
		# pointlist.append( ( pos_x - cos(theta)*margin , pos_x - sin(theta)*margin ) ) 

		pygame.draw.polygon(world_surface, (100, 100, 100), resized_obstacle_points)

	tree_color = (255, 255, 255)
	branch_color = (255, 0, 0)
	line_closed = False
	line_thickness = int(2.0)

	goal_position_resized = ( int( (goal_position[0]-space_limits[0])*rescale_x ) , int( (-goal_position[1]-space_limits[2])*rescale_y ) )
	pygame.draw.circle(world_surface, (0,255,0), goal_position_resized, 10)

	start_position_resized = ( int( (initial_pose[0]-space_limits[0])*rescale_x ) , int( (-initial_pose[1]-space_limits[2])*rescale_y ) )
	pygame.draw.circle(world_surface, (0,0,255), start_position_resized, 10)

	for current_tree_node_id in range(1, len(tree_nodes) ):

		current_tree_node = resized_tree_nodes[current_tree_node_id]
		current_parent_tree_node = resized_tree_nodes[ current_tree_node[2] ]

		line_tuple_list = []
		line_tuple_list.append( ( int(current_tree_node[0]),int(current_tree_node[1]) ) )
		line_tuple_list.append( ( int(current_parent_tree_node[0]),int(current_parent_tree_node[1]) ) )

		pygame.draw.lines(world_surface, tree_color, line_closed, tuple( line_tuple_list ), line_thickness)

	for solution_nodes_it in range(1, len(solution_nodes) ):

		solution_node_id = solution_nodes[solution_nodes_it]
		solution_node_parent_id = solution_nodes[solution_nodes_it-1]

		current_tree_node = resized_tree_nodes[solution_node_id]
		current_parent_tree_node = resized_tree_nodes[ solution_node_parent_id ]

		line_tuple_list = []
		line_tuple_list.append( ( int(current_tree_node[0]),int(current_tree_node[1]) ) )
		line_tuple_list.append( ( int(current_parent_tree_node[0]),int(current_parent_tree_node[1]) ) )

		pygame.draw.lines(world_surface, branch_color, line_closed, tuple( line_tuple_list ), line_thickness)

	return world_surface


def call_rrt(obstacles_list, initial_pose=[1.,1.,1.,1.], goal_coord=[-1.,-1.], space_limits=[-2.,2.,-2.,2.], goal_tolerance=.1, euler_sim_time=.1,	max_steering_angle=.35):

	obstacles_filename = "cppInputObstacles.txt"

	write_obstacles_to_file(obstacles_list, obstacles_filename)

	#make_empty_file(FILEPATH + obstacles_filename)
	# make_empty_file(obstacles_filename)

	args_string = get_rrt_arguments(initial_pose, goal_coord, space_limits, goal_tolerance, euler_sim_time, max_steering_angle)

	os.system(FILEPATH + "rrtleavearea.exe" + ' ' + args_string + ' ' + obstacles_filename)

	tree_nodes =  read_tree_nodes_file("cppOutputTreeNodes.txt")

	solution_nodes = read_tree_solution_file("cppOutputTreeSolution.txt")

	return [tree_nodes, solution_nodes]




def draw_rrt(initial_pose, goal_coord, tree_nodes, solution_nodes, space_limits, obstacles_list):

	image_width = 900
	image_height = 900

	rrt_tree_surface = get_tree_surface(image_width, image_height, initial_pose, goal_coord, tree_nodes, solution_nodes, space_limits, obstacles_list)

	size = width, height = image_width, image_height

	screen = pygame.display.set_mode(size)

	# screen.fill(BLACK)

	screen.blit(rrt_tree_surface, (0,0) )

	pygame.display.flip()

	print "DOOOOOOOOOOOOOOOOOOOOOONNNNNNNNNNNNNNNNNNNNNNNNNNNNEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE"

	time.sleep(10.0)


def prettify(elem):
    """Return a pretty-printed XML string for the Element.
    """
    rough_string = ET.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")

def write_obstacles_to_file(obstacles_list, filename):

	f = open(filename, 'w')

	print "filename" + filename

	for obstacle in obstacles_list:

		[obstacle_string, obstacle_points] = get_obstacle_string(obstacle[0], obstacle[1])
		print "obstacle_string = " + obstacle_string
		f.write(obstacle_string)
		obstacle.append(obstacle_points)

	f.close()




def get_obstacle_string(obstacle_pose, obstacle_dimensions):

	obstacle_string = ""

	box_width = obstacle_dimensions[0]
	box_height = obstacle_dimensions[1]
	box_margin = obstacle_dimensions[2]

	R = numpy.matrix([ [math.cos( obstacle_pose[2] ), -math.sin( obstacle_pose[2] )], [math.sin( obstacle_pose[2] ), math.cos( obstacle_pose[2] )] ])
	t = numpy.matrix([ [ obstacle_pose[0] ], [ obstacle_pose[1] ] ])

	T = numpy.concatenate( (R, t), axis=1)
	T = numpy.concatenate( (T, numpy.matrix( [0, 0, 1 ] ) ), axis=0)

	trans1 = numpy.dot(T,  numpy.matrix( [ [-box_margin], [-box_margin], [1] ] ) ).tolist()
	trans2 = numpy.dot(T,  numpy.matrix( [ [box_width + box_margin], [-box_margin], [1] ] ) ).tolist()
	trans3 = numpy.dot(T,  numpy.matrix( [ [box_width + box_margin], [box_height + box_margin], [1] ] ) ).tolist()
	trans4 = numpy.dot(T,  numpy.matrix( [ [-box_margin], [box_height + box_margin], [1] ] ) ).tolist()

	# X = numpy.array( [ [trans1[0]], [trans2[0]], [trans3[0]], [trans4[0]] ] )
	X = [ trans1[0][0], trans2[0][0], trans3[0][0], trans4[0][0] ]
	Y = [ trans1[1][0], trans2[1][0], trans3[1][0], trans4[1][0] ]
	# Y = numpy.array( [ [trans1[1]], [trans2[1]], [trans3[1]], [trans4[1]] ] )

	# Y = numpy.array( [ [trans1[1]], [trans2[1]], [trans3[1]], [trans4[1]] ] )

	while obstacle_pose[2] > 2*math.pi:
		obstacle_pose[2] = obstacle_pose[2] - 2*math.pi;

	while obstacle_pose[2] < 0 :
		obstacle_pose[2] = obstacle_pose[2] + 2*math.pi;

	str1 = ''
	str2 = ''
	str3 = ''
	str4 = ''

	obstacle_points = [ ( X[0] , Y[0] ) , ( X[1] , Y[1] ) , ( X[2] , Y[2] ) , ( X[3] , Y[3] ) ]

	if ( (obstacle_pose[2] > math.pi/2) and (obstacle_pose[2] < (3*math.pi)/2) ):

		B = numpy.array( [[X[0], 1], [X[1], 1] ] )
		b = numpy.array( [[Y[0]], [Y[1]] ] )
		
		mb,resid,rank,s  = numpy.linalg.lstsq(B,b)
		mb = mb.tolist()

		str1 = str(0) +  ' ' + str( mb[0] ) +  ' ' + str( mb[1] );
		B = numpy.array( [[X[2], 1], [X[3], 1] ] )
		b = numpy.array( [[Y[2]], [Y[3]] ] )
		
		mb,resid,rank,s  = numpy.linalg.lstsq(B,b)
		mb = mb.tolist()

		str2 = str(1) +  ' ' + str( mb[0] ) +  ' ' + str( mb[1] );

	else:

		B = numpy.array( [[X[0], 1], [X[1], 1] ] )
		b = numpy.array( [[Y[0]], [Y[1]] ] )

		mb,resid,rank,s  = numpy.linalg.lstsq(B,b)
		mb = mb.tolist()

		str1 = str(1) +  ' ' + str( mb[0][0] ) +  ' ' + str( mb[1][0] );
		B = numpy.array( [[X[2], 1], [X[3], 1] ] )
		b = numpy.array( [[Y[2]], [Y[3]] ] )
		
		mb,resid,rank,s  = numpy.linalg.lstsq(B,b)
		mb = mb.tolist()

		str2 = str(0) + ' ' + str( mb[0][0] ) + ' ' + str( mb[1][0] );

		


	if obstacle_pose[2] < math.pi:

		B = numpy.array( [[X[1], 1], [X[2], 1] ] )
		b = numpy.array( [[Y[1]], [Y[2]] ] )
		
		mb,resid,rank,s  = numpy.linalg.lstsq(B,b)
		mb = mb.tolist()

		str3 = str(0) +  ' ' + str( mb[0][0] ) +  ' ' + str( mb[1][0] );
		B = numpy.array( [[X[3], 1], [X[0], 1] ] )
		b = numpy.array( [[Y[3]], [Y[0]] ] )
		
		mb,resid,rank,s  = numpy.linalg.lstsq(B,b)
		mb = mb.tolist()

		str4 = str(1) +  ' ' + str( mb[0][0] ) +  ' ' + str( mb[1][0] );


	else:

		B = numpy.array( [[X[1], 1], [X[2], 1] ] )
		b = numpy.array( [[Y[1]], [Y[2]] ] )
		
		mb,resid,rank,s  = numpy.linalg.lstsq(B,b)
		mb = mb.tolist()

		str3 = str(1) +  ' ' + str( mb[0][0] ) +  ' ' + str( mb[1][0] );
		B = numpy.array( [[X[3], 1], [X[0], 1] ] )
		b = numpy.array( [[Y[3]], [Y[0]] ] )
		
		mb,resid,rank,s  = numpy.linalg.lstsq(B,b)
		mb = mb.tolist()

		str4 = str(0) +  ' ' + str( mb[0][0] ) +  ' ' + str( mb[1][0] );

	
	str1 = str1 + '\n'
	str2 = str2 + '\n'
	str3 = str3 + '\n'
	str4 = str4 + '\n'

	obstacle_string = "Obstacle\n" + str1 + str2 + str3 + str4

	return [obstacle_string, obstacle_points]

def get_rrt_solution_message(rrt_nodes, solution_ids):
	"Process an incoming controlled_body_info message"
	
	root = ET.Element('message')

	root.set("type", "rrt_solution")

	tree_root = ET.SubElement(root, "tree")

	# for tree_node in rrt_nodes:
	for it in range( len( rrt_nodes ) ):

		tree_node = rrt_nodes[it]
		ET.SubElement(tree_root, "tree_node", id = str(it), x = str(tree_node[0]), y = str(tree_node[1]), parent_id = str(tree_node[2]) )

	solution_root = ET.SubElement(root, "solution")

	# for tree_node in rrt_nodes:
	for it in range( len( solution_ids ) ):

		solution_id = solution_ids[it]
		ET.SubElement(solution_root, "node", id = str(solution_id), order = str(it) )

	# print "tree:"
	xml_string = ET.tostring(root)
	# print xml_string
	# print prettify(root)

	return xml_string



print "MAIN"

# initial_pose=[1.,1.,1.,1.]
# goal_coord=[-1.,-1.]
truck_angle = 110.
initial_pose=[-0.7,0.,math.radians(truck_angle),math.radians(truck_angle)]
goal_coord=[0.8,0.]
space_limits=[-1.,1.,-1.,1.]
goal_tolerance=.02
euler_sim_time=.1
max_steering_angle=.35

# obstacle_pose = [-2, 3, -4.01]
# get_obstacle_string(obstacle_pose)

obstacles_list = []

obstacle_1 = []
obstacle_1_pose = [-0.3, -0.3, 0.01]
obstacle_1_dimensions = [0.6, 0.6, 0.0]

obstacle_1.append(obstacle_1_pose)
obstacle_1.append(obstacle_1_dimensions)

obstacles_list.append(obstacle_1)
obstacles_list.append(obstacle_1)

[tree_nodes, solution_nodes] = call_rrt(obstacles_list, initial_pose, goal_coord, space_limits, goal_tolerance, euler_sim_time,	max_steering_angle)
# [tree_nodes, solution_nodes] = call_rrt(space_limits = space_limits)

# get_rrt_solution_message(tree_nodes, solution_nodes)

draw_rrt(initial_pose, goal_coord, tree_nodes, solution_nodes, space_limits, obstacles_list)
