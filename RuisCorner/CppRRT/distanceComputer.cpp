#include "distanceComputer.h"
//#include <math.h>
//#include <iostream>
#include <cmath>     


float get_metric_distance(float *state_a, float *state_b){
    
    // Normal euclidian distance
    
    // CORRECT THIS!!!!
//    std::cout << STATE_DIMENSION << std::endl;
    
    float distance = 0;
    
    for (int i = 0; i < 4; i++){
        
        distance = distance + std::abs( state_a[i] - state_b[i] );
    
    }
    
    return distance;
        
}

float get_xy_distance(float *state_a, float *state_b){
    
    // Normal euclidian distance
    
    float distance = 0;
    
    for (int i = 0; i < 2; i++){
        
        distance = distance + std::abs( state_a[i] - state_b[i] );
    
    }
    
    return distance;
        
}

float get_car_front_distance(float *state_truck, float *xy){
    
    // Normal euclidian distance
    
    float distance = 0;
    
    float car_front_pos[2];
    
    car_front_pos[0] = state_truck[0] + TRAILER_LENGTH*cos(state_truck[2]) + TRUCK_LENGTH*cos(state_truck[3]);
    car_front_pos[1] = state_truck[1] + TRAILER_LENGTH*sin(state_truck[2]) + TRUCK_LENGTH*sin(state_truck[3]);
    
    
    for (int i = 0; i < 2; i++){
        
        distance = distance + std::abs( car_front_pos[i] - xy[i] );
    
    }
    
    return distance;
        
}

float get_distance(float *state_a, float *state_b){
    
    // In order to quickly change distance heuristics
    // This function is always called for computing the distance
        
    // Change the heuristic by changing the following function
//    return get_metric_distance(state_a, state_b);
//    return get_xy_distance(state_a, state_b);
    
    float xy_point[2];
    xy_point[0] = state_b[0];
    xy_point[1] = state_b[1];
        
    return get_car_front_distance(state_a, xy_point);
            
}


bool goal_check(float *state_a, float *state_b){
    
    if ( get_distance(state_a, state_b) < GOAL_TOLERANCE ){
        
        return true;
        
    }
    
    return false;
    
}