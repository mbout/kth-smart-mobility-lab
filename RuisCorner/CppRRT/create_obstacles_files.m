% INITIALIZATION STUFF
% if exist('qtm','var')
%     % Close connection
%     QMC(qtm, 'disconnect');
%     clear mex
% end
% 
% if ~exist('qtm','var')
%     % Open connection
%     disp('Opening connection to Qualisys...')
%     qtm=QMC('QMC_conf.txt');
%     disp('Opened connection to Qualisys...')
%     
% end

disp('Opening connection to Qualisys...')
    qtm=QMC('QMC_conf.txt');
    disp('Opened connection to Qualisys...')

fileId = fopen('obstacles.txt', 'w');

close all
figure; hold on;

axis(2*[-1 1 -1 1])

% qualisys_obstacle_id = 1;
write_obstacle_to_file(qtm, 10, fileId );
write_obstacle_to_file(qtm, 11, fileId );
write_obstacle_to_file(qtm, 12, fileId );
write_obstacle_to_file(qtm, 13, fileId );
% write_obstacle_to_file(qtm, qualisys_obstacle_id, fileId );

fclose(fileId);

type obstacles.txt