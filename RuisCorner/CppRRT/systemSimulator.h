/* 
 * File:   systemSimulator.h
 * Author: rui
 *
 * Created on den 15 december 2014, 12:16
 */

#ifndef SYSTEMSIMULATOR_H
#define	SYSTEMSIMULATOR_H

extern float GLOBAL_MIN_X;
extern float GLOBAL_MAX_X;
extern float GLOBAL_MIN_Y;
extern float GLOBAL_MAX_Y;
extern float const TRUCK_LENGTH;
extern float const TRAILER_LENGTH;
extern int const STATE_DIMENSION;
extern float EULER_SIMULATION_TIME;
extern int const EULER_NUMBER_STEPS;
extern float const SPACE_WIDTH;
extern float GOAL_TOLERANCE;
extern float MAX_STEERING_ANGLE;

float get_random_float_between(float a, float b);

void get_random_state(float *random_state, float *goal_state);

void system_equation(float *initial_state, float *final_state, float *inputs, float time_step);

void euler_integration(float *initial_state, float *final_state, float *inputs);

bool find_best_input(float *initial_state, float *goal_state, float *inputs);

#endif	/* SYSTEMSIMULATOR_H */

