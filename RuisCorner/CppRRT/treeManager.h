/* 
 * File:   treeManager.h
 * Author: rui
 *
 * Created on den 15 december 2014, 12:41
 */

#ifndef TREEMANAGER_H
#define	TREEMANAGER_H

#include <vector>
#include "TreeNode.h"

//extern const int STATE_DIMENSION;
extern const float SPACE_WIDTH;
extern float GLOBAL_MIN_X;
extern float GLOBAL_MAX_X;
extern float GLOBAL_MIN_Y;
extern float GLOBAL_MAX_Y;
extern float EULER_SIMULATION_TIME;
extern float GOAL_TOLERANCE;
extern float MAX_STEERING_ANGLE;


void initialize_tree_list(int argc, char**argv, std::vector<TreeNode> &rrt_tree_vector, float *goal_xy);

int find_closest_node(std::vector<TreeNode> &rrt_tree_vector, float *goal_state);

void add_tree_node(std::vector<TreeNode> &rrt_tree_vector, float *new_state,int parent_index);

void print_tree_nodes(std::vector<TreeNode> &rrt_tree_vector);

#endif	/* TREEMANAGER_H */

