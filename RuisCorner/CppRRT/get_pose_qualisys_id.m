function [x,y,theta,t_stamp] = get_pose_qualisys_id(qtm,id)
%GET_POSE_QUALISYS Get x,y,theta from qualisys, they come in meters and
%radians
%   Detailed explanation goes here

    the6DOF=QMC(qtm);
    frameinfo=QMC(qtm,'frameinfo');
    t_stamp=frameinfo(2)/(10^6);
%     frameinfo(2)/(10^6)
%     disp(frameinfo(2)/(10^8))
    x=the6DOF(1,id)/1000;
    y=the6DOF(2,id)/1000;
    theta=deg2rad(the6DOF(6,id));
    
    
end

