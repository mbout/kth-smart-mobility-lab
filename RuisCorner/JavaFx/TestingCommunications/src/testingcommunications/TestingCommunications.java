/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testingcommunications;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;
import java.lang.Math;

/**
 *
 * @author rui
 */
public class TestingCommunications extends Application {
    
    StackPane stackPane;
    Group screenRoot;
    
    List<Node> resizedNodeList;
    
    double windowWidth = 1000;
    double windowHeight = 1000;
    
    boolean mapDrawn = false;
    
    int numNodesClicked = 0;
    Node startNode;
    Node endNode;
    
    @Override
    public void start(Stage primaryStage) throws InterruptedException {
        Button btn = new Button();
        btn.setText("Say 'Hello World'");
        
        
        stackPane = new StackPane();
//        root.getChildren().add(btn);
        
        screenRoot = new Group();
        
        Scene scene = new Scene(screenRoot, windowWidth, windowHeight);
        
        
        
        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
        
//        ConnectToSML task = new ConnectToSML();
        ProcessXml xmlProcesser = new ProcessXml();
        ReceiveInitialXml task = new ReceiveInitialXml(xmlProcesser);
        
        
        task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent t) {
                
                btn.setText("Login done");

            }
        });
        
        Thread th = new Thread(task);
        th.setDaemon(true);
        th.start();
                
        
        btn.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Hello World!");
                System.out.println("task.workDoneProperty() = " + task.workDoneProperty());
                System.out.println("task.getWorkDone() = " + task.getWorkDone());
                System.out.println("task.getMessage() = " + task.getMessage());
            }
        });
        
        
        Rectangle rect = new Rectangle(1, 1);
        rect.setOpacity(0.0);
        screenRoot.getChildren().add(rect);
        
        
        Rectangle r = new Rectangle();
        r.setX(0);
        r.setY(0);
        r.setWidth(1000);
        r.setHeight(1000);
        r.setFill(Color.GREEN);
        screenRoot.getChildren().add(r);
        
        TranslateTransition tt = new TranslateTransition(Duration.millis(100), rect);
        tt.setByX(100f);
        tt.setCycleCount(1);
        
        tt.setOnFinished(new EventHandler<ActionEvent>(){
 
            @Override
            public void handle(ActionEvent arg0) {
                
//                System.out.println("FINISHED ANIMATION");
                tt.play();
                btn.setText( task.getMessage() );
                
                if ( task.getMessage().equals("RECEIVED1")){
                    
                    btn.setText( "YUSSSSSSSSSSSS" );
                    
                    if (!mapDrawn){
                    
                        drawMap(xmlProcesser);
                        mapDrawn = true;
                        
                        screenRoot.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            public void handle(MouseEvent me) {

                                System.out.println("me.getX() = " + me.getX());
                                System.out.println("me.getY() = " + me.getY());
                                
                                processMouseClick(me.getX(), me.getY());
                                
//                                showPlatoonSearch();

                            }

                        });
                        
                    }
                    
                }
                
                if ( task.getMessage().equals("RECEIVED2")){
                    
                    btn.setText( "YUSSSSSSSSSSSS" );
//                    drawTraj();
                    
                }
                
                
            }
        });
        
        tt.play();
        
    }
    
    public void processMouseClick(double xMouse, double yMouse){
        
        Node tempNode = getClosestNode(xMouse, yMouse, resizedNodeList);
        
        if (numNodesClicked == 0){
            
            startNode = tempNode;
            numNodesClicked++;
            
        }
        
        if (numNodesClicked == 1){
            
            endNode = tempNode;
            numNodesClicked++;
            
            sendNodesToSmlWorld();
            
        }
                        
    }
    
    public void sendNodesToSmlWorld(){
        
        
        
        
    }
    
    public Node getClosestNode(double x, double y, List<Node> nodeList){
        
        double distance = 1000000;
        int bestId = -1;
        
        for ( int i = 0; i < nodeList.size(); i++){
            
            
            double currentDistance = Math.sqrt( Math.pow( nodeList.get(i).pixelX - x , 2 ) + Math.pow( nodeList.get(i).pixelY - y , 2 ) );
            
            if (currentDistance < distance){
                
                distance = currentDistance;
                bestId = i;
                
            }
            
        }
        
        return nodeList.get(bestId);
        
    }
    
    
    
    
    public void create_resized_nodes(List<Node> nodeList){
        
//        resizedNodeList
        
        double minX = 100000;
        double minY = 100000;
        double maxX = -100000;
        double maxY = -100000;
        
        for ( int i = 0; i < nodeList.size(); i++ ){
            
            double currentX = nodeList.get(i).pixelX;
            double currentY = nodeList.get(i).pixelY;
            
            if (currentX < minX){
                minX = currentX;
            }
            if (currentY < minY){
                minY = currentY;
            }
            if (currentX > minX){
                maxX = currentX;
            }
            if (currentY > minY){
                maxY = currentY;
            }
            
            
        }
        
        double shrinkingX = windowWidth/(maxX-minX);
        double shrinkingY = windowHeight/(maxY-minY);
        
        double shriking;
        
        if (shrinkingX < shrinkingY){
            shriking = shrinkingX;
        }else{            
            shriking = shrinkingY;
        }
        
        
        resizedNodeList = new ArrayList();
        
        for ( int i = 0; i < nodeList.size() ; i++ ){
            
            Node tempNode = nodeList.get(i);
                        
            Node newNode = new Node(tempNode.id, (tempNode.pixelX - minX)*shriking, (tempNode.pixelY - minY)*shriking);
            
            resizedNodeList.add(newNode);
            
        }
        
    }
    
    
    
    public void drawMap(ProcessXml argXmlProcesser){
        
//        root.getChildren().
        
        
        
        
        List<Way> wayList = argXmlProcesser.getWayList();
        List<Node> nodeList = argXmlProcesser.getNodeList();
        
        
        create_resized_nodes(nodeList);
        
        
        
//        for ( int i = 0; i < 10; i++){
//            
//            System.out.println("nodeList.get(i) = " + nodeList.get(i));
//            
//        }
        
        int test_stuff = 20;
        
        for ( int i = 0; i < wayList.size(); i++ ){
//        for ( int i = test_stuff; i < test_stuff+1; i++ ){
        
            Way tempWay = wayList.get(i);
            
            System.out.println("tempWay = " + tempWay);
            System.out.println("tempWay.nodeIds.size() = " + tempWay.nodeIds.size());
            
            screenRoot.getChildren().add( getWayPolyline(tempWay, resizedNodeList) );
//            screenRoot.getChildren().add( getWayCircles(tempWay, nodeList) );
            
            for ( int nodeId = 0; nodeId < tempWay.nodeIds.size(); nodeId++){
                
//                root.getChildren().add( getLineBetweenNodes(tempWay.nodeIds.get(nodeId-1), tempWay.nodeIds.get(nodeId), nodeList) );
//                System.out.println("");
                
//                Node startNode = getNodeById(tempWay.nodeIds.get(nodeId-1), nodeList);
                Node endNode = getNodeById(tempWay.nodeIds.get(nodeId), nodeList);
                System.out.println("endNode = " + endNode);
                
                
//                root.getChildren().add( getLineBetweenNodes(startNode, endNode) );
                
                
            }
           
                
        }
        
        
    }
    
    public Group getWayPolyline(Way way, List<Node> nodeList){
        
        
        
//        Polyline polyline = new Polyline();
//polyline.getPoints().addAll(new Double[]{
//    0.0, 0.0,
//    20.0, 10.0,
//    10.0, 20.0 });
        
        Polyline polyline = new Polyline();
        
        for ( int i = 0; i < way.nodeIds.size(); i++){
            
            int nodeId = way.nodeIds.get(i);
            
            Node node = getNodeById(nodeId, nodeList);
            
            polyline.getPoints().add(node.pixelX);
            polyline.getPoints().add(node.pixelY);
            
            
            
        }
        
        
//        Node getNodeById(int nodeId, List<Node> nodeList);
        
        Group wayGroup = new Group(polyline);
        
        return wayGroup;
        
    }
    
    public Group getWayCircles(Way way, List<Node> nodeList){
        
        Group wayGroup = new Group();
                
        for ( int i = 0; i < way.nodeIds.size(); i++){
            
            int nodeId = way.nodeIds.get(i);
            
            Node node = getNodeById(nodeId, nodeList);
            
            Circle tempCircle = new Circle(node.pixelX, node.pixelY, 1.0);
            wayGroup.getChildren().add(tempCircle);
           
        }
                
        return wayGroup;
        
    }
    
    public Group getLineBetweenNodes(Node startNode, Node endNode){
        
        
        double x1 = 0, x2 = 0, y1 = 0, y2 = 0;

        x1 = startNode.pixelX;
        y1 = startNode.pixelY;
        
        x2 = endNode.pixelX;
        y2 = endNode.pixelY;
        
        System.out.println( "(" + x1 + "," + y1 + ")-(" + x2 + "," + y2 + ")" );
        
        Line line = new Line();
        line.setStartX(x1);
        line.setStartY(y1);
        line.setEndX(x2);
        line.setEndY(y2);
//        line.setStartX(0);
//        line.setStartY(0);
//        line.setEndX(150);
//        line.setEndY(150);
                
//        Circle circle = new Circle(x1, y1, 1.);
        
        Group objectToDraw = new Group(line);
        
        return objectToDraw;
        
        
    }
    
    public Node getNodeById(int nodeId, List<Node> nodeList){
        
        Node tempNode;
        
        for (int i = 0; i < nodeList.size(); i++){
            
            tempNode = nodeList.get(i);
            
            if (tempNode.id == nodeId ){
                
                return tempNode;
                
            }
            
        }
        
        return null;
        
    }
    
//    public Group getLineBetweenNodes(int node1Id, int node2Id, List<Node> nodeList){
//        
//        double x1 = 0, x2 = 0, y1 = 0, y2 = 0;
//        
////        System.out.println("Printing a line for nodes:");
////        System.out.println("node1Id = " + node1Id);
////        System.out.println("node2Id = " + node2Id);
//        
//        for ( int i = 0; i < nodeList.size(); i++){
//            
//            Node tempNode = nodeList.get(i);
//            
//            if (tempNode.id == node1Id){
////                System.out.println("found");
//                x1 = tempNode.pixelX;
//                y1 = tempNode.pixelY;
//            }
//            if (tempNode.id == node2Id){
////                System.out.println("FOUND");
//                x2 = tempNode.pixelX;
//                y2 = tempNode.pixelY;
//            }
//                        
//        }
//        
//        System.out.println("x1 = " + x1 + " y1 = " + y1 + " x2 = " + x2 + " y2 = " + y2);
//        
//        Line line = new Line();
//        line.setStartX(x1);
//        line.setStartY(y1);
//        line.setEndX(x2);
//        line.setEndY(y2);
//                
//        Circle circle = new Circle(x1, y1, 1.);
//        
//        Group objectToDraw = new Group(line);
//        
//        return objectToDraw;
//                
//    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
