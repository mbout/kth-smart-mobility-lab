# Implements list search functions
import customclasses as cc
import webbrowser
import copy
import math

def create_osm_node_list(xml_root):

	osm_node_list = []
	origin_node = []

	for node in xml_root.findall('node'):

		add_flag = True

		for tag in node.findall('tag'):
			if tag.get('k') == "origin":
				if tag.get('v') == "true":
					
					id = int( node.get('id') )
					origin_lat = float( node.get('lat') )
					origin_lon = float( node.get('lon') )

					origin_node = cc.OSMNode(id, origin_lat, origin_lon)
					add_flag = False
					continue

		if not add_flag:
			continue

		id = int( node.get('id') )
		lat = float( node.get('lat') )
		lon = float( node.get('lon') )

		osm_node_list.append( cc.OSMNode(id, lat, lon) )

	return [osm_node_list, origin_node]

def create_osm_way_list(xml_root):

	osm_way_list = []

	for way in xml_root.findall('way'):

		id = int( way.get('id') )

		current_way = cc.OSMWay( id )

		for tag in way.findall('tag'):
			# print "Found a tag"
			if tag.get('k') == "line_type":

				# print str ( type ( tag.get('v') ) )
				current_way.set_line_type( tag.get('v') )
				# print "Setting line type as " + tag.get('v')

			else:

				print "Unrecognized tag in way xml"

		for node in way.findall('nd'):
			ref_id = int( node.get('ref') )
			current_way.add_node_id( ref_id )

		osm_way_list.append( current_way )

	return osm_way_list

def create_osm_lanelet_list(osm_node_list, osm_way_list, xml_root):

	osm_lanelet_list = []

	for relation in xml_root.findall('relation'):
		
		lanelet = False
		bidirectional = False

		for tag in relation.findall('tag'):

			if tag.get('k') == 'type' and tag.get('v') == 'lanelet':

				lanelet = True

			if tag.get('k') == 'bidirectional' and tag.get('v') == 'yes':

				bidirectional = True

		if lanelet:

			temp_lanelet = lanelet
			lanelet_id = int( relation.get('id') )

			left_osm_way = []
			right_osm_way = []

			for member in relation.findall('member'):

				if member.get('type') == 'way':

					way_id = int( member.get('ref') )
					tempOSMWay = get_osm_way_by_id(osm_way_list, way_id)

					if member.get('role') == 'left':

						left_osm_way = copy.deepcopy( tempOSMWay )

					else:

						right_osm_way = copy.deepcopy( tempOSMWay )

			if not left_osm_way:
				raise NameError('left_osm_way is empty, lanelet creation aborted')
			if not right_osm_way:
				raise NameError('right_osm_way is empty, lanelet creation aborted')

			starting_right_node = get_osm_way_by_id(osm_node_list, right_osm_way.node_ids[0] )

			starting_left_node = get_osm_way_by_id(osm_node_list, left_osm_way.node_ids[0] )
			ending_left_node = get_osm_way_by_id(osm_node_list, left_osm_way.node_ids[-1] )

			if ( get_distance_between_nodes(starting_right_node, starting_left_node) > get_distance_between_nodes(starting_right_node, ending_left_node) ):

				# Need to flip the left way, because it is in the wrong direction!
				left_osm_way.node_ids.reverse()

			osm_lanelet_list.append( cc.OSMLanelet(lanelet_id, left_osm_way, right_osm_way) )

			inverted_left_osm_way = copy.deepcopy( left_osm_way )
			inverted_right_osm_way = copy.deepcopy( right_osm_way )

			if len(left_osm_way.node_ids) != len(right_osm_way.node_ids):
				raise NameError('Lanelet ways have a different number of nodes')

			inverted_left_osm_way.node_ids.reverse()
			inverted_right_osm_way.node_ids.reverse()

			if bidirectional:

				"WHICH ID TO PUT????, IT SHOULD NOT BE REPEATED"
				osm_lanelet_list.append( cc.OSMLanelet(lanelet_id, inverted_right_osm_way, inverted_left_osm_way) )

	return osm_lanelet_list

def normalize_coordinates(osm_node_list, origin):
	"To center all the coordinates around origin or (0,0)"

	if origin[0] == -1:

		x_average = 0
		y_average = 0
		number_nodes = len( osm_node_list )

		for node in osm_node_list:

			x_average = x_average + node.x/number_nodes
			y_average = y_average + node.y/number_nodes

	else:

		x_average = origin[0]
		y_average = origin[1]

	for node in osm_node_list:

		node.x = node.x - x_average
		node.y = node.y - y_average

	return

def get_osm_node_by_id(osm_node_list, node_id):
	"function used to get an OSMNode from osm_node_list by its OSMId (node_id)"

	for temp_node in osm_node_list:

		if temp_node.id == node_id:

			return temp_node

	raise NameError('Node not found by its OSM Id')

	return

def get_osm_way_by_id(osm_way_list, way_id):
	"function used to get an OSMWay from osm_way_list by its OSMId (way_id)"

	for temp_way in osm_way_list:

		if temp_way.id == way_id:

			return temp_way

	raise NameError('Way not found by its OSM Id')

	return

def get_osm_lanelet_by_id(osm_lanelet_list, lanelet_id):
	"function used to get an OSMWay from osm_way_list by its OSMId (way_id)"

	for temp_lanelet in osm_lanelet_list:

		if temp_lanelet.id == lanelet_id:

			return temp_lanelet

	raise NameError('Lanelet not found by its OSM Id')

	return

def create_pixel_values_for_nodes(osm_node_list, osm_way_list, canvas_width = 1800, canvas_height = 1000, viewing_gap = 0, pixel_per_meter = -1,
	origin_x = -1, origin_y = -1):
	"A function for creating pixel values for the nodes"

	min_x = 10e+10
	max_x = - 10e+10
	min_y = 10e+10
	max_y = - 10e+10

	avg_x = 0
	avg_y = 0

	for node in osm_node_list:

		avg_x = avg_x + node.x
		avg_y = avg_y + node.y

		if ( min_x > node.x ):
			min_x = node.x
		if ( max_x < node.x ):
			max_x = node.x
		if ( min_y > node.y ):
			min_y = node.y
		if ( max_y < node.y ):
			max_y = node.y

	avg_x = avg_x/len( osm_node_list )
	avg_y = avg_y/len( osm_node_list )

	if origin_x != -1:

		avg_x = origin_x
		avg_y = origin_y

	world_width = max_x - min_x;
	world_height = max_y - min_y;

	
	# if pixel_per_meter == -1:

	# 	if (world_width > world_height):

	# 		pixel_per_meter = (canvas_width-2*viewing_gap)/world_width

	# 	else:

	# 		pixel_per_meter = (canvas_height-2*viewing_gap)/world_height

	if pixel_per_meter == -1:

		# if (world_width > world_height):

		pixel_per_meter_x = (canvas_width-2*viewing_gap)/world_width

		# else:

		pixel_per_meter_y = (canvas_height-2*viewing_gap)/world_height

		if pixel_per_meter_x < pixel_per_meter_y:

			pixel_per_meter = pixel_per_meter_x

		else:

			pixel_per_meter = pixel_per_meter_y

	else:

		pixel_per_meter = pixel_per_meter/32.

	# print "pixel_per_meter"
	# print pixel_per_meter

	for way in osm_way_list:

			for idx in xrange(0, len(way.node_ids)):

				OSMNode = get_osm_node_by_id(osm_node_list, way.node_ids[idx])
				# set_node_pixel_values(OSMNode, min_x, min_y, canvas_height, pixel_per_meter, viewing_gap)
				set_node_pixel_values_centered(OSMNode, avg_x, avg_y, canvas_width, canvas_height, pixel_per_meter, viewing_gap)

	center_pixel_x = canvas_width/2 + -avg_x*pixel_per_meter + viewing_gap
	center_pixel_y = canvas_height/2 - -avg_y*pixel_per_meter + viewing_gap

	return [center_pixel_x, center_pixel_y, pixel_per_meter]

def create_lanelet_adjacency_matrix(osm_lanelet_list, osm_way_list, osm_node_list):

	adjacency_matrix = [[0 for x in range( len(osm_lanelet_list) )] for x in range ( len(osm_lanelet_list) )]

	for lanelet_index_a in range( len(osm_lanelet_list) ):

		for lanelet_index_b in range( len(osm_lanelet_list) ):

			if lanelet_index_a == lanelet_index_b:

				continue

			"Check if Lanelet_A connects to Lanelet_B (one directional only)"
			is_adjacent = check_Lanelet_Adjacency(osm_lanelet_list[lanelet_index_a], osm_lanelet_list[lanelet_index_b], osm_way_list)
			
			if is_adjacent:

				lanelet_length = get_lanelet_length( osm_lanelet_list[lanelet_index_a], osm_node_list )
				adjacency_matrix[lanelet_index_a][lanelet_index_b] = lanelet_length

	return adjacency_matrix

def check_Lanelet_Adjacency(lanelet_start, lanelet_end, osm_way_list):

	if ( lanelet_start.left_osm_way.node_ids[-1] == lanelet_end.left_osm_way.node_ids[0] ) and ( lanelet_start.right_osm_way.node_ids[-1] == lanelet_end.right_osm_way.node_ids[0] ) :

		return True

	return False

def get_lanelet_length(osm_lanelet, osm_node_list):

	left_lane_length = 0
	right_lane_length = 0

	left_osm_way = osm_lanelet.left_osm_way
	right_osm_way = osm_lanelet.right_osm_way


	for way_node_id in range( len(left_osm_way.node_ids) - 1 ):

		prev_node_left = get_osm_node_by_id( osm_node_list, left_osm_way.node_ids[way_node_id] )
		prev_node_right = get_osm_node_by_id( osm_node_list, right_osm_way.node_ids[way_node_id] )

		next_node_left = get_osm_node_by_id( osm_node_list, left_osm_way.node_ids[way_node_id+1] )
		next_node_right = get_osm_node_by_id( osm_node_list, right_osm_way.node_ids[way_node_id+1] )

		current_left_lane_length = get_distance_between_nodes(prev_node_left, next_node_left)
		current_right_lane_length = get_distance_between_nodes(prev_node_right, next_node_right)

		left_lane_length = left_lane_length + current_left_lane_length
		right_lane_length = right_lane_length + current_right_lane_length

	average_length = ( left_lane_length + right_lane_length )/2

	return average_length

def get_distance_between_nodes(osm_node_start, osm_node_end):

	distance = ( ( osm_node_end.x - osm_node_start.x )**2 + ( osm_node_end.y - osm_node_start.y )**2 )**0.5

	return distance

def get_pixel_distance_to_node(pixel, osm_node):

	distance = ( ( osm_node.pixel_x - pixel[0] )**2 + ( osm_node.pixel_y - pixel[1] )**2 )**0.5

	return distance

def get_center_between_nodes(osm_node_start, osm_node_end):

	x = ( osm_node_start.x + osm_node_end.x )/2.
	y = ( osm_node_start.y + osm_node_end.y )/2.

	return [x, y]

def dijkstra_algorithm(adjacency_matrix, source_id):
	"Receives an adjacency_matrix, which is not necessarily symmetric."
	"A value of 0 means that in adjacency_matrix[i][j] means that there is no connection from node i to node j"

	num_nodes = len(adjacency_matrix)
	distances = [ 10e10 for i in range(num_nodes) ]
	distances[source_id] = 0
	previous_node = [ -1 for i in range(num_nodes) ]

	nodes_to_visit = [ source_id ]

	while nodes_to_visit:

		current_node_to_visit = nodes_to_visit.pop(0)

		current_adjacencies = adjacency_matrix[current_node_to_visit]

		for i in range( len(current_adjacencies) ):

			if i == current_node_to_visit:

				"Same node"
				continue

			if current_adjacencies[i] == 0:

				"No connection"
				continue

			if distances[i] >  distances[current_node_to_visit] + current_adjacencies[i]:

				distances[i] = distances[current_node_to_visit] + current_adjacencies[i]
				previous_node[i] = current_node_to_visit
				nodes_to_visit.append(i)

	return distances, previous_node

def get_shortest_path(previous_node, destination_id):
	"Simply receives Dijkstra outputs and converts it to the shortes path"
	
	shortest_path = []

	while destination_id != -1:

		shortest_path.append(destination_id)
		destination_id = previous_node[destination_id]

	shortest_path.reverse()
	
	return shortest_path	

def get_shortest_path_distance(adjacency_matrix, previous_node, destination_id):
	"Simply receives Dijkstra outputs and sums the distances composing the shortest path"

	distance = 0

	while destination_id != -1:

		distance = distance + adjacency_matrix[ previous_node[destination_id] ][ destination_id ]
		destination_id = previous_node[destination_id]

	
	return distance	

def convert_to_lanelet_id(shortest_path, osm_lanelet_list):

	lanelet_path = []

	for i in shortest_path:

		lanelet_path.append(osm_lanelet_list[i].id)

	return lanelet_path

def get_trajectory(lanelet_path, osm_lanelet_list, osm_node_list):

	x_traj = []
	y_traj = []

	for lanelet_id in lanelet_path:

		temp_lanelet = get_osm_lanelet_by_id(osm_lanelet_list, lanelet_id)

		[x_traj_temp, y_traj_temp] = convert_lanelet_to_trajectory(temp_lanelet, osm_node_list)

		if x_traj_temp != None:

			# print "type( x_traj_temp )"
			# print type( x_traj_temp )
			# print "x_traj_temp"
			# print x_traj_temp

			# x_traj_temp.reverse()
			# y_traj_temp.reverse()

			x_traj.extend(x_traj_temp)
			y_traj.extend(y_traj_temp)

		else:

			print "x_traj" + str( x_traj )

	return [x_traj, y_traj]

def crop_trajectory_to_node_ids(traj_x, traj_y, osm_node_list, start_id, end_id):

	
	start_node = get_osm_node_by_id(osm_node_list, start_id) 
	end_node = get_osm_node_by_id(osm_node_list, end_id) 

	best_start_distance = 10e10
	best_end_distance = 10e10

	best_start_id = -1
	best_end_id = -1

	for idx in range( len( traj_x ) ):

		current_distance = ( ( start_node.x - traj_x[idx] )**2. + ( start_node.y - traj_y[idx] )**2 )**0.5

		if current_distance < best_start_distance:

			best_start_distance = current_distance
			best_start_id = idx

		current_distance = ( ( end_node.x - traj_x[idx] )**2. + ( end_node.y - traj_y[idx] )**2 )**0.5

		if current_distance < best_end_distance:

			best_end_distance = current_distance
			best_end_id = idx

	if best_start_id == -1 or best_end_id == -1:

		return [traj_x, traj_y]

	else:

		return [traj_x[best_start_id:best_end_id], traj_y[best_start_id:best_end_id]]

def convert_lanelet_to_trajectory(osm_lanelet, osm_node_list):

	left_osm_way = osm_lanelet.left_osm_way 
	right_osm_way = osm_lanelet.right_osm_way

	center_points_x = []
	center_points_y = []

	left_nodes = []
	
	for left_id in left_osm_way.node_ids:

		left_nodes.append( get_osm_node_by_id(osm_node_list, left_id) )

	right_nodes = []
	
	for right_id in right_osm_way.node_ids:

		right_nodes.append( get_osm_node_by_id(osm_node_list, right_id) )

	for idx in range( len( left_nodes ) ):

		[x, y] = get_center_between_nodes(left_nodes[idx], right_nodes[idx])
		center_points_x.append(x)
		center_points_y.append(y)
	
	cumulative_length_center = [0]
	current_cumulative_length_center = 0

	cumulative_length_left = [0]
	current_cumulative_length_left = 0

	cumulative_length_right = [0]
	current_cumulative_length_right = 0

	for idx in range(1, len( left_nodes ) ):

		prev_x = center_points_x[idx-1]
		new_x = center_points_x[idx]

		prev_y = center_points_y[idx-1]
		new_y = center_points_y[idx]

		current_distance = ( ( new_x - prev_x )**2. + ( new_y - prev_y )**2 )**0.5

		current_cumulative_length_center = current_cumulative_length_center + current_distance
		cumulative_length_center.append(current_cumulative_length_center)

		prev_node = left_nodes[idx-1]
		new_node = left_nodes[idx]

	 	current_cumulative_length_left = current_cumulative_length_left + get_distance_between_nodes(prev_node, new_node)
	 	cumulative_length_left.append(current_cumulative_length_left)

	 	prev_node = right_nodes[idx-1]
		new_node = right_nodes[idx]

	 	current_cumulative_length_right = current_cumulative_length_right + get_distance_between_nodes(prev_node, new_node)
		cumulative_length_right.append(current_cumulative_length_right)

	cumulative_length_desired = []

	velocity = 5. * (1000./3600.)

	center_length = cumulative_length_center[-1]

	number_interpolation_points = int( math.ceil( center_length/velocity ) )

	for idx in range(number_interpolation_points):

		cumulative_length_desired.append( (float(idx)/number_interpolation_points) * center_length )

	interpolated_center_points_x = []
	interpolated_center_points_x.append( center_points_x[0] )
	interpolated_center_points_y = []
	interpolated_center_points_y.append( center_points_y[0] )

	for idx in range(1, number_interpolation_points):

		for search_idx in range( len(cumulative_length_center)-1 ):

			if cumulative_length_desired[idx] > cumulative_length_center[search_idx] and cumulative_length_desired[idx] < cumulative_length_center[search_idx+1]:

				distance_a = cumulative_length_desired[idx] - cumulative_length_center[search_idx]
				distance_b = cumulative_length_center[search_idx+1] - cumulative_length_desired[idx]

				interpolated_x = ( distance_b*center_points_x[search_idx] + distance_a*center_points_x[search_idx+1] )/( distance_a + distance_b )
				interpolated_y = ( distance_b*center_points_y[search_idx] + distance_a*center_points_y[search_idx+1] )/( distance_a + distance_b )

				interpolated_center_points_x.append(interpolated_x)
				interpolated_center_points_y.append(interpolated_y)


	new_interpolation_distance = 0
	new_interpolation_cumulative_distance = [new_interpolation_distance]

	for i in range( len( center_points_x ) - 1 ):

		current_distance =  ( ( center_points_x[i+1] - center_points_x[i] )**2 + ( center_points_y[i+1] - center_points_y[i] )**2 )**0.5
		new_interpolation_distance = new_interpolation_distance + current_distance
		new_interpolation_cumulative_distance.append(new_interpolation_distance)

	return [interpolated_center_points_x, interpolated_center_points_y]

def find_closest_lanelets(click_position, osm_lanelet_list, osm_node_list):

	best_distance = 10e10
	best_id = -1


	for lanelet in osm_lanelet_list:

		left_way = lanelet.left_osm_way

		for node_id in left_way.node_ids:

			OSM_Node = get_osm_node_by_id(osm_node_list, node_id)

			distance = get_pixel_distance_to_node(click_position, OSM_Node)
			if distance < best_distance:
				best_distance = distance
				best_id = node_id

		right_way = lanelet.right_osm_way

		for node_id in right_way.node_ids:

			OSM_Node = get_osm_node_by_id(osm_node_list, node_id)

			distance = get_pixel_distance_to_node(click_position, OSM_Node)
			if distance < best_distance:
				best_distance = distance
				best_id = node_id

	# return [best_id, best_distance]

	closest_lanelets = []

	for lanelet_id in range( len( osm_lanelet_list ) ):

		for node_id in osm_lanelet_list[lanelet_id].left_osm_way.node_ids:

			if node_id == best_id:

				closest_lanelets.append( lanelet_id )

		for node_id in osm_lanelet_list[lanelet_id].right_osm_way.node_ids:

			if node_id == best_id:

				closest_lanelets.append( lanelet_id )	

	return [closest_lanelets, best_distance]

def find_possible_paths_between_lanelets(closest_lanelets_start, closest_lanelets_end, adjacency_matrix):

	best_path_distance = 10e10
	best_shortest_path = []

	for source_id in closest_lanelets_start:

		distances, previous_node = dijkstra_algorithm(adjacency_matrix, source_id)

		for destination_id in closest_lanelets_end:

			distance = get_shortest_path_distance(adjacency_matrix, previous_node, destination_id)

			if distance < best_path_distance:

				shortest_path = get_shortest_path(previous_node, destination_id)
				# print len(shortest_path)

				if len( shortest_path ) > 1:

					best_path_distance = distance
					best_shortest_path = shortest_path

	return best_shortest_path

def get_trajectory_from_node_ids(start_id, end_id, osm_lanelet_list, osm_way_list, osm_node_list):

	start_lanelets = []
	end_lanelets = []

	for lanelet_index in range( len( osm_lanelet_list ) ):

		for temp_id in osm_lanelet_list[lanelet_index].left_osm_way.node_ids:

			if start_id == temp_id:

				start_lanelets.append( lanelet_index )

			if end_id == temp_id:

				end_lanelets.append( lanelet_index )

		for temp_id in osm_lanelet_list[lanelet_index].right_osm_way.node_ids:

			if start_id == temp_id:

				start_lanelets.append( lanelet_index )

			if end_id == temp_id:

				end_lanelets.append( lanelet_index )

	adjacency_matrix = create_lanelet_adjacency_matrix(osm_lanelet_list, osm_way_list, osm_node_list)

	best_distance = 10e10
	best_start = -1
	best_end = -1
	
	for start_lanelet_id in start_lanelets:

		for end_lanelet_id in end_lanelets:

			distances, previous_node = dijkstra_algorithm(adjacency_matrix, start_lanelet_id)
			# shortest_path = get_shortest_path(previous_node, destination_id)
			current_distance = get_shortest_path_distance(adjacency_matrix, previous_node, end_lanelet_id)
			
			if current_distance < best_distance and current_distance > 0.1:

				# print "current_distance"
				# print current_distance

				best_distance = current_distance
				best_start = start_lanelet_id
				best_end = end_lanelet_id

	if len( start_lanelets ) == len( end_lanelets ) and len( start_lanelets ) == 1 and start_lanelets[0] == end_lanelets[0]:

		lanelet_path = convert_to_lanelet_id([start_lanelet_id], osm_lanelet_list) 

	else:

		distances, previous_node = dijkstra_algorithm(adjacency_matrix, best_start)	

		shortest_path = get_shortest_path(previous_node, best_end)

		lanelet_path = convert_to_lanelet_id(shortest_path, osm_lanelet_list) 

	[traj_x, traj_y] = get_trajectory(lanelet_path, osm_lanelet_list, osm_node_list)

	[new_traj_x, new_traj_y] = crop_trajectory_to_node_ids(traj_x, traj_y, osm_node_list, start_id, end_id)

	# new_traj_x = traj_x
	# new_traj_y = traj_y

	# [new_traj_x, new_traj_y] = convert_points_to_pixel(osm_node_list, traj_x, traj_y)

	return [new_traj_x, new_traj_y]

def get_closed_trajectory_from_node_id(start_id, osm_lanelet_list, osm_way_list, osm_node_list):

	start_lanelets = []
	end_lanelets = []

	for lanelet_index in range( len( osm_lanelet_list ) ):

		for temp_id in osm_lanelet_list[lanelet_index].left_osm_way.node_ids:

			if start_id == temp_id:

				start_lanelets.append( lanelet_index )

		for temp_id in osm_lanelet_list[lanelet_index].right_osm_way.node_ids:

			if start_id == temp_id:

				start_lanelets.append( lanelet_index )

	adjacency_matrix = create_lanelet_adjacency_matrix(osm_lanelet_list, osm_way_list, osm_node_list)

	best_distance = 10e10
	best_start = -1
	best_end = -1
	
	for start_lanelet_id in start_lanelets:

		for end_lanelet_id in range( len( osm_lanelet_list ) ):

			if (start_lanelet_id == end_lanelet_id ):
				continue

			distances, previous_node = dijkstra_algorithm(adjacency_matrix, start_lanelet_id)
			# shortest_path = get_shortest_path(previous_node, destination_id)
			current_distance = get_shortest_path_distance(adjacency_matrix, previous_node, end_lanelet_id)
			
			if current_distance < best_distance and current_distance > 0.1:

				# print "current_distance"
				# print current_distance

				best_distance = current_distance
				best_start = start_lanelet_id
				best_end = end_lanelet_id

	if len( start_lanelets ) == len( end_lanelets ) and len( start_lanelets ) == 1 and start_lanelets[0] == end_lanelets[0]:

		lanelet_path = convert_to_lanelet_id([start_lanelet_id], osm_lanelet_list) 

	else:

		distances, previous_node = dijkstra_algorithm(adjacency_matrix, best_start)	

		shortest_path = get_shortest_path(previous_node, best_end)

		lanelet_path = convert_to_lanelet_id(shortest_path, osm_lanelet_list) 

	# print "lanelet_path"
	# print lanelet_path

	lanelet_path_start = lanelet_path

	distances, previous_node = dijkstra_algorithm(adjacency_matrix, best_end)	

	shortest_path = get_shortest_path(previous_node, best_start)

	lanelet_path_end = convert_to_lanelet_id(shortest_path, osm_lanelet_list) 

	# print "lanelet_path_end"
	# print lanelet_path_end
	# print "lanelet_path_end[1:-1]"
	# print lanelet_path_end[1:-1]


	# print "lanelet_path_closed"
	# print lanelet_path_start.extend(lanelet_path_end[1:-1])
	# print lanelet_path_start

	[traj_x, traj_y] = get_trajectory(lanelet_path_start, osm_lanelet_list, osm_node_list)

	# [new_traj_x, new_traj_y] = crop_trajectory_to_node_ids(traj_x, traj_y, osm_node_list, start_id, end_id)

	new_traj_x = traj_x
	new_traj_y = traj_y

	# [new_traj_x, new_traj_y] = convert_points_to_pixel(osm_node_list, traj_x, traj_y)

	return [new_traj_x, new_traj_y]

def set_node_pixel_values(OSMNode, min_x, min_y, canvas_height, pixel_per_meter, viewing_gap):

	OSMNode.pixel_x = (OSMNode.x - min_x)*pixel_per_meter + viewing_gap
	OSMNode.pixel_y = canvas_height - (OSMNode.y - min_y)*pixel_per_meter - viewing_gap

def set_node_pixel_values_centered(OSMNode, avg_x, avg_y, canvas_width, canvas_height, pixel_per_meter, viewing_gap):

	OSMNode.pixel_x = canvas_width/2 + (OSMNode.x - avg_x)*pixel_per_meter + viewing_gap
	OSMNode.pixel_y = canvas_height/2 - (OSMNode.y - avg_y)*pixel_per_meter + viewing_gap

