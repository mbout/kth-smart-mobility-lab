import laneletmodule
import pygame

xml_file_location = 'myFifthCity.xml'

lanelet_module = laneletmodule.LaneletModule()

lanelet_module.load_xml_world_info(xml_file_location)

# lanelet_module.set_surface_properties(canvas_width = 1280, canvas_height = 720, viewing_gap = 0, pixel_per_meter = 720/6)
# lanelet_module.set_surface_properties(canvas_width = 1280, canvas_height = 720, viewing_gap = 50, pixel_per_meter = -1)
lanelet_module.set_surface_properties(canvas_width = 1920, canvas_height = 1080, viewing_gap = 0, pixel_per_meter = 720/6)

world_surface = lanelet_module.get_world_surface()

# pygame.image.save(world_surface, 'world_surface.bmp') 