import laneletlibrary
import pygame


def draw_lanelet(lanelet, osm_node_list, world_surface):

	node_tuple_list = []

	for node_id in lanelet.left_osm_way.node_ids:

		current_node = laneletlibrary.get_osm_node_by_id(osm_node_list, node_id)

		current_tuple = ( int( current_node.pixel_x ) , int( current_node.pixel_y ) )
		node_tuple_list.append( current_tuple )

	for idx in range( len ( lanelet.right_osm_way.node_ids ) ):

		current_node = laneletlibrary.get_osm_node_by_id(osm_node_list, lanelet.right_osm_way.node_ids[-1 - idx])

		current_tuple = ( int( current_node.pixel_x ) , int( current_node.pixel_y ) )
		node_tuple_list.append( current_tuple )

	COLOR = (77, 76, 69) # Asphalt black

	pygame.draw.polygon(world_surface, COLOR, tuple( node_tuple_list ) )

def draw_way(way, osm_node_list, world_surface):

	if way.line_type == None:
		return

	node_tuple_list = []

	for node_id in way.node_ids:

		current_node = laneletlibrary.get_osm_node_by_id(osm_node_list, node_id)

		current_tuple = ( int( current_node.pixel_x ) , int( current_node.pixel_y ) )
		node_tuple_list.append( current_tuple )

	COLOR = ()
	line_thickness = 0

	if way.line_type == 'exterior':
		COLOR = (191, 179, 8) # Dirty yellow
		line_thickness = 3

	if way.line_type == 'interior':
		COLOR = (245, 243, 223) # Dirty white 
		line_thickness = 2

	# print node_tuple_list
	line_closed = False
	

	pygame.draw.lines(world_surface, COLOR, line_closed, tuple( node_tuple_list ), line_thickness)


def draw_all_lanelets(osm_info, world_surface):

	# draw_lanelet(osm_info.osm_lanelet_list[10], osm_info.osm_node_list, world_surface)

	for lanelet in osm_info.osm_lanelet_list:

		draw_lanelet(lanelet, osm_info.osm_node_list, world_surface)


def draw_all_ways(osm_info, world_surface):

	for way in osm_info.osm_way_list:

		draw_way(way, osm_info.osm_node_list, world_surface)
