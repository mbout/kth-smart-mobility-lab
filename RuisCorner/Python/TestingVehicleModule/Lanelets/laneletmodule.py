import xmlcomms
import laneletlibrary
import laneletvisualisation
import customclasses
import pygame
import socket, sys
import xml.etree.ElementTree as ET

sys.path.append('../')

from world import timed_print

class LaneletModule:
	"This class implements the Lanelet Module to be running in the SML World"


	def load_xml_world_info(self, xml_file_location):
		"This method loads the information contained in the .xml created by JOSM"
		"This method has to be run before doing anything with the class"
		"TO DO: Implement this method in the constructor"

		XML_file = xml_file_location

		tree = ET.parse(XML_file)
		xml_root = tree.getroot()

		[osm_node_list, origin_node] = laneletlibrary.create_osm_node_list(xml_root)

		if origin_node:

			self.origin = [origin_node.x, origin_node.y]

		else:

			self.origin = [-1, -1]

		laneletlibrary.normalize_coordinates(osm_node_list, self.origin)

		osm_way_list = laneletlibrary.create_osm_way_list(xml_root)

		osm_lanelet_list = laneletlibrary.create_osm_lanelet_list(osm_node_list, osm_way_list, xml_root)

		adjacency_matrix = laneletlibrary.create_lanelet_adjacency_matrix(osm_lanelet_list, osm_way_list, osm_node_list)

		laneletlibrary.create_pixel_values_for_nodes(osm_node_list, osm_way_list)

		self.osm_info = customclasses.OSMInfo(osm_node_list, osm_way_list, osm_lanelet_list, adjacency_matrix)
		
		return

	def set_surface_properties(self, canvas_width = 1800, canvas_height = 1000, viewing_gap = 0, pixel_per_meter = -1):

		self.canvas_width = canvas_width
		self.canvas_height = canvas_height
		# self.viewing_gap = viewing_gap
		# self.pixel_per_meter = pixel_per_meter
		# center_pixel_x = 0
		# center_pixel_y = 0
		# pixel_per_meter = 0


		if self.origin[0] == -1:

			[self.center_pixel_x, self.center_pixel_y, self.pixel_per_meter] = laneletlibrary.create_pixel_values_for_nodes(self.osm_info.osm_node_list, self.osm_info.osm_way_list, canvas_width, canvas_height, viewing_gap, pixel_per_meter)

		else:

			[self.center_pixel_x, self.center_pixel_y, self.pixel_per_meter] = laneletlibrary.create_pixel_values_for_nodes(self.osm_info.osm_node_list, self.osm_info.osm_way_list, canvas_width, canvas_height, viewing_gap, pixel_per_meter, 
				origin_x = 0, origin_y = 0)

		return

	def get_road_info_message_string(self):
		"This method creates a message string to be sent to the Command Central"
		"This string contains the required information about the roads, and is according to protocol"

		road_info_message_string = xmlcomms.make_road_info_xml_message_string(self.osm_info.osm_node_list, self.osm_info.osm_way_list)
		road_info_message_string = road_info_message_string + "\n"

		return road_info_message_string

	def process_and_reply(self, request_string, bodies_list):
		"Roughly process an incoming message, and call appropriate parsing functions"
		"Output otions guide:"
		"If message is invalid return None"
		"If message is valid and you want to send a string as reponse, return string response"
		"If its a special request (e.g.: Image request), deal with communications internally and return True"

		xml_root = []

		try:

			xml_root = ET.fromstring(request_string)

		except (ET.ParseError):

			#print ET.ParseError
			return None

		if xml_root.tag == None or xml_root.tag != "message":

			return None

		message_type = xml_root.get("type")


		if message_type == "trajectory_request":
			timed_print('Received trajectory request from command central',parent = 'CMD')
			lanelet =  self.process_and_reply_to_trajectory_request_string(request_string)
			timed_print('Sent trajectory reply to command central',parent = 'CMD')
			return lanelet

		elif message_type == "image_request":

			timed_print('Received background image request from command central',parent = 'CMD')
			reply = self.process_image_request(request_string)
			if reply:
				timed_print('Sent background image reply to command central',parent = 'CMD')
			elif reply == None:
				timed_print('Error while sending background image to command central','FAIL',parent='CMD')

			# When returning true, we are not replying to command central
			return True

		elif message_type == "image_properties_request":
			timed_print('Received background image properties request from command central',parent = 'CMD')
			lanelet = self.process_and_reply_to_image_properties_request_string(request_string)
			timed_print('Sent background image properties reply to command central',parent = 'CMD')
			return lanelet

		elif message_type == "highlight_request":

			self.process_and_reply_to_highlight_request_string(request_string)

			# When returning true, we are not replying to command central
			return True

		elif message_type == "vehicle_states_request":

			return self.process_and_reply_to_vehicle_states_request_string(request_string, bodies_list)
			
		else:

			print "Message type not handled yet: " + message_type
			return None


		return reply_string

	def process_image_request(self, request_string):

		xml_root = ET.fromstring(request_string)

		xml_socket_info = xml_root.find('socket')

		if xml_socket_info == None:

			print "Error in xml string"
			return

		ip_string = xml_socket_info.get("ip")
		port_number = int( xml_socket_info.get("port") )

		#print "ip_string"
		#print ip_string

		#print "port_number"
		#print port_number

		# return
		sock = socket.socket()

		#print "Will connect"
		# sock.connect(("130.237.50.246", 8050))
		try:
			sock.connect( (ip_string, port_number) )
		except:
			return None
		#print "Connected"
		
		file = open("resources/world_surface.bmp", "rb")
		# chunks = 0
		total_bytes = 0
		while True:
			chunk = file.read(65536)
			# current_bytes = sys.getsizeof(chunk)
			# total_bytes = total_bytes + current_bytes
			# print total_bytes
			if not chunk:
				break  # EOF
			sock.sendall(chunk)
			# chunks = chunks + 1

		sock.close()

		return True

	def process_and_reply_to_trajectory_request_string(self, trajectory_request_string):
		"This method parses a trajectory request string received from the Command Central, following the protocol"
		"It then produces a reply string to be sent, that is also following the protocol"

		[start_id, end_id] = xmlcomms.process_trajectory_request_string(trajectory_request_string, self.osm_info.osm_node_list, self.osm_info.osm_way_list)

		trajectory_reply_message_string = xmlcomms.get_trajectory_reply_message_string(start_id, end_id, self.osm_info.osm_lanelet_list, self.osm_info.osm_way_list, self.osm_info.osm_node_list)

		trajectory_reply_message_string = trajectory_reply_message_string + '\n' 

		return trajectory_reply_message_string

	def process_and_reply_to_vehicle_states_request_string(self, vehicle_states_request_string, bodies_list):
		"This method parses a vehicle states request string received from the Command Central, following the protocol"
		"It then produces a reply string to be sent, that is also following the protocol"

		# [start_id, end_id] = xmlcomms.process_trajectory_request_string(trajectory_request_string, self.osm_info.osm_node_list, self.osm_info.osm_way_list)

		#print "Received a vehicle states request"

		# for body in bodies_list:

			# print body
			# print [ body['x'], body['y'], body['a3'] ]
			# print body['x']
			# print body['y']
			# print body['a3']
			# print [ body['x'], body['y'], body['a3'] ]


		# trajectory_reply_message_string = xmlcomms.get_trajectory_reply_message_string(start_id, end_id, self.osm_info.osm_lanelet_list, self.osm_info.osm_way_list, self.osm_info.osm_node_list)

		# trajectory_reply_message_string = trajectory_reply_message_string + '\n' 

		vehicle_states_reply_message_string = xmlcomms.get_vehicle_states_reply_message_string(bodies_list)

		vehicle_states_reply_message_string = vehicle_states_reply_message_string + '\n'

		return vehicle_states_reply_message_string

	def process_and_reply_to_image_properties_request_string(self, image_properties_request_string):
		"This method parses a image properties request string string received from the Command Central"
		"It then produces a reply string to be sent"

		image_properties_reply_message_string = xmlcomms.get_image_properties_reply_message_string(self.center_pixel_x, self.center_pixel_y, self.pixel_per_meter)

		image_properties_reply_message_string = image_properties_reply_message_string + '\n' 

		return image_properties_reply_message_string

	def process_highlight_request_string(self, image_properties_request_string):
		"This method parses a image properties request string string received from the Command Central"
		"It then produces a reply string to be sent"

		[] = xmlcomms.process_highlight_message_string()

		return image_properties_reply_message_string

	def get_world_surface(self):
		"This method returns a pygame.Surface that corresponds to the image of the world"

		world_surface = pygame.Surface((self.canvas_width, self.canvas_height), 0, 32)

		FOREST_GREEN = (80, 163, 77)
		world_surface.fill(FOREST_GREEN)

		laneletvisualisation.draw_all_lanelets(self.osm_info, world_surface)
		laneletvisualisation.draw_all_ways(self.osm_info, world_surface)

		# For debugging purposes
		# pygame.image.save(world_surface, 'world_surface.bmp')

		return world_surface
