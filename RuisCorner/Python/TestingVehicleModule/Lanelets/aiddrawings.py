from Tkinter import *
from laneletlibrary import *

GLOBAL_click_one = []
GLOBAL_click_two = []
w_canvas = []

def draw_circle(x, y, min_x, min_y, canvas_handler, canvas_height, shrinking_ratio, fill_color = "black"):
	"function used to draw a line between two OSMNodes"

	x1 = x
	y1 = y

	circle_radius = .01
	top_left_x = x1 - circle_radius
	top_left_y = y1 + circle_radius
	bottom_right_x = x1 + circle_radius
	bottom_right_y = y1 - circle_radius

	canvas_handler.create_oval( (top_left_x - min_x)*shrinking_ratio, canvas_height - (top_left_y - min_y)*shrinking_ratio, (bottom_right_x - min_x)*shrinking_ratio, canvas_height - (bottom_right_y - min_y)*shrinking_ratio, outline = "red", fill = "red", width = 2)

	return

def draw_line_between_nodes(OSMNode1, OSMNode2, min_x, min_y, canvas_handler, canvas_height, shrinking_ratio, fill_color = "black"):
	"function used to draw a line between two OSMNodes"

	x1 = OSMNode1.x
	y1 = OSMNode1.y

	x2 = OSMNode2.x
	y2 = OSMNode2.y

	canvas_handler.create_line( (x1 - min_x)*shrinking_ratio, canvas_height - (y1 - min_y)*shrinking_ratio, (x2 - min_x)*shrinking_ratio, canvas_height - (y2 - min_y)*shrinking_ratio , fill = fill_color)

	return

def set_node_pixel_values(OSMNode, min_x, min_y, canvas_height, shrinking_ratio):

	OSMNode.pixel_x = (OSMNode.x - min_x)*shrinking_ratio
	OSMNode.pixel_y = canvas_height - (OSMNode.y - min_y)*shrinking_ratio


def visualise_map_shortest_path_and_trajectory(osm_node_list, osm_way_list, osm_lanelet_list, shortest_path, traj_x, traj_y, source_id, destination_id):
	"Draw a lot of stuff for visualisation/debugging purposes"

	min_x = 10e+10; max_x = - 10e+10;
	min_y = 10e+10; max_y = - 10e+10;

	for node in osm_node_list:

		if ( min_x > node.x ):
			min_x = node.x
		if ( max_x < node.x ):
			max_x = node.x
		if ( min_y > node.y ):
			min_y = node.y
		if ( max_y < node.y ):
			max_y = node.y

	viewing_gap = 0
	min_x = min_x - viewing_gap
	min_y = min_y - viewing_gap
	max_x = max_x + viewing_gap
	max_y = max_y + viewing_gap

	world_width = max_x - min_x;
	world_height = max_y - min_y;

	canvas_width = 1800;
	canvas_height = 1000;

	if (world_width > world_height):

		shrinking_ratio = canvas_width/world_width

	else:

		shrinking_ratio = canvas_height/world_height

	for way in osm_way_list:

			for idx in xrange(0, len(way.node_ids)):

				OSMNode = get_osm_node_by_id(osm_node_list, way.node_ids[idx])
				set_node_pixel_values(OSMNode, min_x, min_y, canvas_height, shrinking_ratio)

	draw = False
	if draw:

		master = Tk()
		w = Canvas(master, width=canvas_width, height=canvas_height)
		w.pack()

		for way in osm_way_list:

			for idx in xrange(0, len(way.node_ids)-1):

				OSMNode1 = get_osm_node_by_id(osm_node_list, way.node_ids[idx])
				OSMNode2 = get_osm_node_by_id(osm_node_list, way.node_ids[idx+1])

				
				draw_line_between_nodes(OSMNode1, OSMNode2, min_x, min_y, w, canvas_height, shrinking_ratio*(8/6))

		color_list = ['red', 'yellow', 'blue', 'green', 'pink', 'yellow']

		color_id = 0
		lanelet_id = 0

		for traj_index in range( len( traj_x ) ):


			circle_radius = 1.0
			top_left_x = traj_x[traj_index] - circle_radius
			top_left_y = traj_y[traj_index] + circle_radius
			bottom_right_x = traj_x[traj_index] + circle_radius
			bottom_right_y = traj_y[traj_index] - circle_radius
			draw_circle(traj_x[traj_index], traj_y[traj_index], min_x, min_y, w, canvas_height, shrinking_ratio*(8/6))

		for lanelet in osm_lanelet_list:

			current_color = 'black'
			if lanelet_id in shortest_path:
				current_color = 'blue'
			if lanelet_id == source_id:
				current_color = 'red'
			if lanelet_id == destination_id:
				current_color = 'green'
			if current_color == 'black':
				lanelet_id = lanelet_id + 1
				continue

			lanelet_id = lanelet_id + 1	

			for idx in xrange(0, len(lanelet.left_osm_way.node_ids)-1):

				OSMNode1 = get_osm_node_by_id(osm_node_list, lanelet.left_osm_way.node_ids[idx])
				OSMNode2 = get_osm_node_by_id(osm_node_list, lanelet.left_osm_way.node_ids[idx+1])

				draw_line_between_nodes(OSMNode1, OSMNode2, min_x, min_y, w, canvas_height, shrinking_ratio*(8/6), current_color)

			for idx in xrange(0, len(lanelet.right_osm_way.node_ids)-1):

				OSMNode1 = get_osm_node_by_id(osm_node_list, lanelet.right_osm_way.node_ids[idx])
				OSMNode2 = get_osm_node_by_id(osm_node_list, lanelet.right_osm_way.node_ids[idx+1])

				draw_line_between_nodes(OSMNode1, OSMNode2, min_x, min_y, w, canvas_height, shrinking_ratio*(8/6), current_color)

			color_id = color_id + 1

		mainloop()


def mouse_left_click(event):

	global GLOBAL_click_one
	global GLOBAL_click_two
	global w_canvas

	if not GLOBAL_click_one:
		GLOBAL_click_one.append(event.x)
		GLOBAL_click_one.append(event.y)
		return

	if not GLOBAL_click_two:
		GLOBAL_click_two.append(event.x)
		GLOBAL_click_two.append(event.y)
		w_canvas.quit()
		return

	
	# print("Mouse position: (%s %s)" % (event.x, event.y))
	return


def draw_lanelets_and_get_input(osm_node_list, osm_way_list, osm_lanelet_list):
	"Draw a lot of stuff for visualisation/debugging purposes"

	min_x = 10e+10; max_x = - 10e+10;
	min_y = 10e+10; max_y = - 10e+10;

	for node in osm_node_list:

		if ( min_x > node.x ):
			min_x = node.x
		if ( max_x < node.x ):
			max_x = node.x
		if ( min_y > node.y ):
			min_y = node.y
		if ( max_y < node.y ):
			max_y = node.y

	viewing_gap = 0
	min_x = min_x - viewing_gap
	min_y = min_y - viewing_gap
	max_x = max_x + viewing_gap
	max_y = max_y + viewing_gap

	world_width = max_x - min_x;
	world_height = max_y - min_y;

	canvas_width = 1800;
	canvas_height = 1000;

	if (world_width > world_height):

		shrinking_ratio = canvas_width/world_width

	else:

		shrinking_ratio = canvas_height/world_height

	draw = True
	if draw:

		master = Tk()
		global w_canvas
		w_canvas = Canvas(master, width=canvas_width, height=canvas_height)
		w_canvas.pack()

		for way in osm_way_list:

			for idx in xrange(0, len(way.node_ids)):

				OSMNode = get_osm_node_by_id(osm_node_list, way.node_ids[idx])
				set_node_pixel_values(OSMNode, min_x, min_y, canvas_height, shrinking_ratio)



		color_id = 0
		lanelet_id = 0

		for lanelet in osm_lanelet_list:

			current_color = 'black'
			lanelet_id = lanelet_id + 1	

			for idx in xrange(0, len(lanelet.left_osm_way.node_ids)-1):

				OSMNode1 = get_osm_node_by_id(osm_node_list, lanelet.left_osm_way.node_ids[idx])
				OSMNode2 = get_osm_node_by_id(osm_node_list, lanelet.left_osm_way.node_ids[idx+1])

				# draw_line_between_nodes(OSMNode1, OSMNode2, min_x, min_y, w_canvas, canvas_height, shrinking_ratio*(8/6), current_color)
				w_canvas.create_line( OSMNode1.pixel_x , OSMNode1.pixel_y, OSMNode2.pixel_x , OSMNode2.pixel_y , fill = current_color)

			for idx in xrange(0, len(lanelet.right_osm_way.node_ids)-1):

				OSMNode1 = get_osm_node_by_id(osm_node_list, lanelet.right_osm_way.node_ids[idx])
				OSMNode2 = get_osm_node_by_id(osm_node_list, lanelet.right_osm_way.node_ids[idx+1])

				# draw_line_between_nodes(OSMNode1, OSMNode2, min_x, min_y, w_canvas, canvas_height, shrinking_ratio*(8/6), current_color)
				w_canvas.create_line( OSMNode1.pixel_x , OSMNode1.pixel_y, OSMNode2.pixel_x , OSMNode2.pixel_y , fill = current_color)

			color_id = color_id + 1

		# button = Button(w, text='Mouse Clicks')
		# button.pack()
		w_canvas.bind('<Button-1>', mouse_left_click)
		# widget.bind('<Double-1>', quit) 
		# print "w_canvas.mainloop() started"
		w_canvas.mainloop()
		# print "w_canvas.mainloop() ended"

	return [GLOBAL_click_one, GLOBAL_click_two]
