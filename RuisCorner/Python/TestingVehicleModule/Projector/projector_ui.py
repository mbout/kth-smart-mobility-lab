# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'start.ui'
#
# Created: Thu Jan 22 14:19:33 2015
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_projector(object):
    def setupUi(self, projector):
        projector.setObjectName(_fromUtf8("projector"))
        projector.setWindowModality(QtCore.Qt.WindowModal)
        projector.resize(816, 198)
        self.run_projector = QtGui.QPushButton(projector)
        self.run_projector.setGeometry(QtCore.QRect(10, 130, 791, 51))
        self.run_projector.setObjectName(_fromUtf8("run_projector"))
        self.label = QtGui.QLabel(projector)
        self.label.setGeometry(QtCore.QRect(60, 80, 66, 17))
        self.label.setText(_fromUtf8(""))
        self.label.setObjectName(_fromUtf8("label"))
        self.groupBox_4 = QtGui.QGroupBox(projector)
        self.groupBox_4.setGeometry(QtCore.QRect(620, 20, 151, 81))
        self.groupBox_4.setObjectName(_fromUtf8("groupBox_4"))
        self.check_origin = QtGui.QCheckBox(self.groupBox_4)
        self.check_origin.setGeometry(QtCore.QRect(10, 30, 111, 22))
        self.check_origin.setObjectName(_fromUtf8("check_origin"))
        self.groupBox = QtGui.QGroupBox(projector)
        self.groupBox.setGeometry(QtCore.QRect(20, 20, 261, 111))
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.min_x = QtGui.QTextEdit(self.groupBox)
        self.min_x.setGeometry(QtCore.QRect(60, 30, 61, 31))
        self.min_x.setObjectName(_fromUtf8("min_x"))
        self.label_2 = QtGui.QLabel(self.groupBox)
        self.label_2.setGeometry(QtCore.QRect(10, 30, 66, 31))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.max_x = QtGui.QTextEdit(self.groupBox)
        self.max_x.setGeometry(QtCore.QRect(180, 30, 61, 31))
        self.max_x.setObjectName(_fromUtf8("max_x"))
        self.label_3 = QtGui.QLabel(self.groupBox)
        self.label_3.setGeometry(QtCore.QRect(130, 30, 66, 31))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.label_5 = QtGui.QLabel(self.groupBox)
        self.label_5.setGeometry(QtCore.QRect(130, 70, 66, 31))
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.min_y = QtGui.QTextEdit(self.groupBox)
        self.min_y.setGeometry(QtCore.QRect(60, 70, 61, 31))
        self.min_y.setObjectName(_fromUtf8("min_y"))
        self.label_4 = QtGui.QLabel(self.groupBox)
        self.label_4.setGeometry(QtCore.QRect(10, 70, 66, 31))
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.max_y = QtGui.QTextEdit(self.groupBox)
        self.max_y.setGeometry(QtCore.QRect(180, 70, 61, 31))
        self.max_y.setObjectName(_fromUtf8("max_y"))
        self.groupBox_3 = QtGui.QGroupBox(projector)
        self.groupBox_3.setGeometry(QtCore.QRect(290, 20, 311, 111))
        self.groupBox_3.setObjectName(_fromUtf8("groupBox_3"))
        self.projector_host = QtGui.QTextEdit(self.groupBox_3)
        self.projector_host.setGeometry(QtCore.QRect(50, 50, 151, 31))
        self.projector_host.setObjectName(_fromUtf8("projector_host"))
        self.label_10 = QtGui.QLabel(self.groupBox_3)
        self.label_10.setGeometry(QtCore.QRect(10, 50, 41, 31))
        self.label_10.setObjectName(_fromUtf8("label_10"))
        self.projector_port = QtGui.QTextEdit(self.groupBox_3)
        self.projector_port.setGeometry(QtCore.QRect(250, 50, 61, 31))
        self.projector_port.setObjectName(_fromUtf8("projector_port"))
        self.label_11 = QtGui.QLabel(self.groupBox_3)
        self.label_11.setGeometry(QtCore.QRect(210, 50, 31, 31))
        self.label_11.setObjectName(_fromUtf8("label_11"))
        self.stop_projector = QtGui.QPushButton(projector)
        self.stop_projector.setGeometry(QtCore.QRect(10, 130, 791, 51))
        self.stop_projector.setObjectName(_fromUtf8("stop_projector"))

        self.retranslateUi(projector)
        QtCore.QMetaObject.connectSlotsByName(projector)

    def retranslateUi(self, projector):
        projector.setWindowTitle(_translate("projector", "Projector", None))
        self.run_projector.setText(_translate("projector", "Start Projector", None))
        self.groupBox_4.setTitle(_translate("projector", "Main Settings", None))
        self.check_origin.setText(_translate("projector", "Print Origin", None))
        self.groupBox.setTitle(_translate("projector", "Space Area", None))
        self.min_x.setHtml(_translate("projector", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Ubuntu\'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">-3000</p></body></html>", None))
        self.label_2.setText(_translate("projector", "Min X", None))
        self.max_x.setHtml(_translate("projector", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Ubuntu\'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">3000</p></body></html>", None))
        self.label_3.setText(_translate("projector", "Max X", None))
        self.label_5.setText(_translate("projector", "Max Y", None))
        self.min_y.setHtml(_translate("projector", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Ubuntu\'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">-3000</p></body></html>", None))
        self.label_4.setText(_translate("projector", "Min Y", None))
        self.max_y.setHtml(_translate("projector", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Ubuntu\'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">3000</p></body></html>", None))
        self.groupBox_3.setTitle(_translate("projector", "Remote Projector", None))
        self.projector_host.setHtml(_translate("projector", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Ubuntu\'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">localhost</p></body></html>", None))
        self.label_10.setText(_translate("projector", "Host", None))
        self.projector_port.setHtml(_translate("projector", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Ubuntu\'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">8000</p></body></html>", None))
        self.label_11.setText(_translate("projector", "Port", None))
        self.stop_projector.setText(_translate("projector", "Stop Projector", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    projector = QtGui.QWidget()
    ui = Ui_projector()
    ui.setupUi(projector)
    projector.show()
    sys.exit(app.exec_())

