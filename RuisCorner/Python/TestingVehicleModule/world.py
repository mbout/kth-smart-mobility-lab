#!/usr/bin/python

import threading, sys, errno
import datetime, time
import pygame
from socket import *
from socket import error as SocketError
from xml.dom import minidom
from xml.etree import ElementTree as ET

sys.path.append('Lanelets')
import laneletmodule

import vehiclesimulator

import laneletmodule
import xmlcomms
from mocap import Mocap

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    WORLD = '\033[1;97m'
    PROJECTOR = '\033[1;32m'
    CMD = '\033[1;93m'
    QUALISYS = '\033[1;96m'

def timed_print(message,color = None,parent= None):

	try:
		color = getattr(bcolors,color)
	except:
		color = ''
	try:
		parent = getattr(bcolors,parent)
	except:
		parent = ''
	print parent + get_current_time() + bcolors.ENDC + ' ' + color + message + bcolors.ENDC


def get_current_time():
	return datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S ")


def start_mocap_socket(qualisys_host,qualisys_port,mocap_on):
	while 1:
		Qs = Mocap(qualisys_host,qualisys_port)
		if Qs.socket == None:
			if mocap_on:
				timed_print('Qualisys computer is down please turn it on','FAIL')
				mocap_on = False
		else:
			break
	timed_print('Connected to Qualisys at ' + qualisys_host + ' in port:' + str(qualisys_port),'OKBLUE',parent = 'QUALISYS')
	timed_print('Qualisys computer is ON','OKGREEN',parent = 'QUALISYS')	
	return Qs


def mocap_handler_handler(qualisys_host,qualisys_port):
	global bodies_list_xml
	global bodies_list
	mocap_on = True

	Qs = start_mocap_socket(qualisys_host,qualisys_port,mocap_on)

	while 1:

		bodies_list = Qs.get_updated_bodies()

		if bodies_list == 'off':
			timed_print('Qualisys computer is down please turn it on','FAIL',parent = 'QUALISYS')
			Qs.socket.close()
			time.sleep(1)
			Qs = start_mocap_socket(qualisys_host,qualisys_port,mocap_on)

		try:
			#bodies_list_xml = create_bodies_xml(bodies_list)
			try:
				bodies_list_xml = xmlcomms.get_vehicle_states_reply_message_string(bodies_list)
			except:
				pass
			if not mocap_on and (bodies_list != []):
				timed_print('Qualisys computer is ON','OKGREEN',parent = 'QUALISYS')
				mocap_on = True
		except TypeError:
			#Qs._stop_measurement()
			if mocap_on and (bodies_list != []):
				mocap_on = False
				bodies_list_xml = ''

			#Qs = Mocap(qualisys_host,qualisys_port)
			#print Qs

			#return


def projector_manager_handler(conn,BUFSIZE):

	global bodies_list_xml

	timed_print('Waiting for projector requests',parent = 'PROJECTOR')

	while 1:
		try:
			request = conn.recv(BUFSIZE)
		except:
			conn.close()
			break
		if request == 'BACKGROUND':

			timed_print('Request for background image from projector',parent = 'PROJECTOR')

			image = open('resources/world_surface.bmp','rb')
			image_data = image.read()
			image.close()

			conn.send(image_data)


		elif request == 'STATES':

			#print get_current_time() + ' Request for vehicles states from projector'

			# bodies_list = Qs.get_updated_bodies()

			# bodies_list_xml = create_bodies_xml(bodies_list)

			if bodies_list_xml is not None:
				conn.send(bodies_list_xml)


		elif request == 'TRAFFIC LIGHTS':

			timed_print('Request for traffic lights from projector',parent = 'PROJECTOR')

		elif request == 'HIGHLIGHTS':

			timed_print('Request for highlights from projector',parent = 'PROJECTOR')

		elif request == 'CLOSE':

			timed_print('Request for close from projector',parent = 'PROJECTOR')
			conn.close()
			timed_print('Projector Manager disconnected','WARNING',parent = 'PROJECTOR')
			return

def command_central_handler(conn,BUFSIZE,lanelet_module):

	global bodies_list

	timed_print('Waiting for trajectory request from command central',parent = 'CMD')

	trajectory_info = lanelet_module.get_road_info_message_string()
	conn.send(trajectory_info)
	timed_print('Trajectory info sent to command central',parent = 'CMD')

	while 1:

		try:
			request_string = conn.recv(BUFSIZE)
		except SocketError as e:
			if e.errno == errno.ECONNRESET:
				timed_print('Command Central disconnected','WARNING',parent = 'CMD')
				conn.close()
				return

		if request_string == 'CLOSE\r\n':
			conn.close()
			timed_print('Command Central disconnected','WARNING',parent = 'CMD')
			return

		#timed_print('Request received from command central')

		# command_central_reply = lanelet_module.process_and_reply_to_trajectory_request_string(request_string)
		command_central_reply = lanelet_module.process_and_reply(request_string, bodies_list)
		
		# timed_print('Request processed and sent to command central')

		if command_central_reply == None:
			timed_print('Invalid message received, do nothing','FAIL',parent = 'CMD')
			# Invalid message received, do nothing
			
		elif isinstance( command_central_reply, str ):			
			conn.send(command_central_reply)


		elif command_central_reply == True:
			pass
			#print "Special request handled internally"
			
		else:
			print "Invalid message, do not even know how to handle"
			print "Message: " + request_string


def vehicle_central_handler(conn,BUFSIZE,lanelet_module):
	pass

def v2v_v2i_handler(conn,BUFSIZE,lanelet_module):
	pass

def create_connections_server(PORT = "8000"):
	HOST = ''
	ADDR = (HOST,PORT)
	BUFSIZE = 4096

	serv = socket( AF_INET,SOCK_STREAM)

	serv.setsockopt(SOL_SOCKET,SO_REUSEADDR, 1)

	serv.bind((ADDR))

	return serv

def start_connections_server(serv,lanelet_module):
	global thread_list

	BUFSIZE = 4096


	lanelet_module.set_surface_properties(canvas_width = 1920, canvas_height = 1080, viewing_gap = 0, pixel_per_meter = 720/6)
	background_roads = lanelet_module.get_world_surface()

	pygame.image.save(background_roads,'resources/world_surface.bmp')

	while 1:

		try:
			timed_print('Ready for new connections',parent = 'WORLD')
			serv.listen(1)

			conn,addr = serv.accept()

			timed_print('IP: '+ str(addr[0]) + ' Port: ' + str(addr[1]) + ' ...connected!','OKBLUE',parent = 'WORLD')

			conn.send('OK\n')

			data = conn.recv(BUFSIZE)

			#print repr(data)

			if data == 'projector-main' or data == 'projector-states':

				conn.send('OK')

				if data == 'projector-main':
					timed_print('Projector Manager connected','OKGREEN',parent='PROJECTOR')
				elif data == 'projector-states':
					timed_print('Projector receiving states ftom SML world','OKGREEN',parent='PROJECTOR')

				t = threading.Thread(target=projector_manager_handler,args=(conn,BUFSIZE))
				t.daemon = True
				t.start()



			elif data == 'command\r\n':

				timed_print('Command central connected','OKGREEN',parent = 'CMD')

				t = threading.Thread(target=command_central_handler,args=(conn,BUFSIZE,lanelet_module))
				t.daemon = True
				t.start()

			elif data == 'vehicle\r\n':

				timed_print('Vehicle connected')

				t = threading.Thread(target=vehicle_handler,args=(conn,BUFSIZE,lanelet_module))
				t.daemon = True
				t.start()

			elif data == 'communication\r\n':

				timed_print('V2V/V2I connected')

				t = threading.Thread(target=v2v_v2i_handler,args=(conn,BUFSIZE,lanelet_module))
				t.daemon = True
				t.start()



		except KeyboardInterrupt:
			serv.close()
			break

def vehicle_simulator_handler():

	global bodies_list

	vehicle_simulator = vehiclesimulator.VehicleSimulator()

	while True:

		print "vehicle_simulator_handler LOOP"

		vehicle_simulator.step()

		print bodies_list
		time.sleep(0.05)





if __name__ == "__main__":

	thread_list = []

	xml_file_location = 'Lanelets/myFifthCity.xml'

	timed_print('Parsing ' + xml_file_location,parent = 'WORLD')
	
	lanelet_module = laneletmodule.LaneletModule()
	lanelet_module.load_xml_world_info(xml_file_location)

	# Globals declaration
	bodies_list_xml = ''
	bodies_list = []

	qualisys_host = 'smlremote.no-ip.biz'
	qualisys_port = 22224 
	t = threading.Thread(target=mocap_handler_handler,args=(qualisys_host,qualisys_port))
	t.daemon = True
	t.start()

	t = threading.Thread(target=vehicle_simulator_handler )
	t.daemon = True
	t.start()

	PORT = 8000
	timed_print('Creating Connections Server in Port:' + str(PORT),parent = 'WORLD')
	serv = create_connections_server(PORT)

	timed_print('Starting Connections Server',parent = 'WORLD')

	start_connections_server(serv,lanelet_module)
