import laneletmodule

xml_file_location = 'myFirstCity.xml'

lanelet_module = laneletmodule.LaneletModule()

lanelet_module.load_xml_world_info(xml_file_location)

print lanelet_module.get_road_info_message_string()

# trajectory_request_string = "<message id=\"5\" type=\"trajectory_request\"><start id=\"2\"/><end id=\"42\"/></message>"
trajectory_request_string = "<message id=\"5\" type=\"trajectory_request\"><start id=\"-1808\"/><end id=\"-186\"/></message>"

# print trajectory_request_string

print lanelet_module.process_and_reply_to_trajectory_request_string(trajectory_request_string)