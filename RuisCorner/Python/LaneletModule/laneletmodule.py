import xmlcomms
import laneletlibrary
import customclasses
import xml.etree.ElementTree as ET

class LaneletModule:
	"This class implements the Lanelet Module to be running in the SML World"


	def load_xml_world_info(self, xml_file_location):

		XML_file = xml_file_location

		tree = ET.parse(XML_file)
		xml_root = tree.getroot()

		osm_node_list = laneletlibrary.create_osm_node_list(xml_root)

		osm_way_list = laneletlibrary.create_osm_way_list(xml_root)

		osm_lanelet_list = laneletlibrary.create_osm_lanelet_list(osm_way_list, xml_root)

		adjacency_matrix = laneletlibrary.create_lanelet_adjacency_matrix(osm_lanelet_list, osm_way_list, osm_node_list)

		laneletlibrary.create_pixel_values_for_nodes(osm_node_list, osm_way_list)

		self.osm_info = customclasses.OSMInfo(osm_node_list, osm_way_list, osm_lanelet_list, adjacency_matrix)

		return

	def get_road_info_message_string(self):

		road_info_message_string = xmlcomms.make_road_info_xml_message_string(self.osm_info.osm_node_list, self.osm_info.osm_way_list)
		road_info_message_string = road_info_message_string + "\n"

		return road_info_message_string

	def process_and_reply_to_trajectory_request_string(self, trajectory_request_string):

		print trajectory_request_string

		[start_id, end_id] = xmlcomms.process_trajectory_request_string(trajectory_request_string, self.osm_info.osm_node_list, self.osm_info.osm_way_list)

		trajectory_reply_message_string = xmlcomms.get_trajectory_reply_message_string(start_id, end_id, self.osm_info.osm_lanelet_list, self.osm_info.osm_way_list, self.osm_info.osm_node_list)

		return trajectory_reply_message_string