# Implements list search functions
import customClasses as cc
import webbrowser
import copy
import math

def create_OSMNodeList(xml_root):

	# print len(xml_root.findall('node'))
	# print len(xml_root.findall('way'))

	OSMNodeList = []

	for node in xml_root.findall('node'):

		id = int( node.get('id') )
		lat = float( node.get('lat') )
		lon = float( node.get('lon') )

		OSMNodeList.append( cc.OSMNode(id, lat, lon) )

	# print len(OSMNodeList)

	return OSMNodeList


def create_OSMWayList(xml_root):

	OSMWayList = []

	for way in xml_root.findall('way'):

		id = int( way.get('id') )

		currentWay = cc.OSMWay( id )

		for node in way.findall('nd'):

			ref_id = int( node.get('ref') )
			currentWay.add_node_id( ref_id )

		OSMWayList.append( currentWay )

	return OSMWayList

# def create_OSMLaneletList(OSMWayList, xml_root):

# 	OSMLaneletList = []

# 	for relation in xml_root.findall('relation'):
		
# 		lanelet = False

# 		for tag in relation.findall('tag'):

# 			if tag.get('k') == 'type' and tag.get('v') == 'lanelet':

# 				lanelet = True

# 		if lanelet:

# 			temp_lanelet = lanelet
# 			lanelet_id = int( relation.get('id') )

# 			leftOSMWay = []
# 			rightOSMWay = []

# 			for member in relation.findall('member'):

# 				if member.get('type') == 'way':

# 					way_id = int( member.get('ref') )
# 					tempOSMWay = get_OSMWay_by_id(OSMWayList, way_id)

# 					if member.get('role') == 'left':

# 						leftOSMWay = tempOSMWay

# 					else:

# 						rightOSMWay = tempOSMWay

# 			if not leftOSMWay:
# 				raise NameError('leftOSMWay is empty, lanelet creation aborted')
# 			if not rightOSMWay:
# 				raise NameError('rightOSMWay is empty, lanelet creation aborted')

# 			OSMLaneletList.append( cc.OSMLanelet(lanelet_id, leftOSMWay, rightOSMWay) )

# 	return OSMLaneletList


def create_OSMLaneletListBi(OSMWayList, xml_root):

	OSMLaneletList = []

	for relation in xml_root.findall('relation'):
		
		lanelet = False
		bidirectional = False

		for tag in relation.findall('tag'):

			if tag.get('k') == 'type' and tag.get('v') == 'lanelet':

				lanelet = True

			if tag.get('k') == 'bidirectional' and tag.get('v') == 'yes':

				bidirectional = True

		if lanelet:

			temp_lanelet = lanelet
			lanelet_id = int( relation.get('id') )

			leftOSMWay = []
			rightOSMWay = []

			for member in relation.findall('member'):

				if member.get('type') == 'way':

					way_id = int( member.get('ref') )
					tempOSMWay = get_OSMWay_by_id(OSMWayList, way_id)

					if member.get('role') == 'left':

						leftOSMWay = tempOSMWay

					else:

						rightOSMWay = tempOSMWay

			if not leftOSMWay:
				raise NameError('leftOSMWay is empty, lanelet creation aborted')
			if not rightOSMWay:
				raise NameError('rightOSMWay is empty, lanelet creation aborted')

			OSMLaneletList.append( cc.OSMLanelet(lanelet_id, leftOSMWay, rightOSMWay) )

			inverted_leftOSMWay = copy.deepcopy( leftOSMWay )
			inverted_rightOSMWay = copy.deepcopy( rightOSMWay )

			if len(leftOSMWay.node_ids) != len(rightOSMWay.node_ids):
				raise NameError('Lanelet ways have a different number of nodes')

			inverted_leftOSMWay.node_ids.reverse()
			inverted_rightOSMWay.node_ids.reverse()

			if bidirectional:

				"WHICH ID TO PUT????, IT SHOULD NOT BE REPEATED"
				OSMLaneletList.append( cc.OSMLanelet(lanelet_id, inverted_rightOSMWay, inverted_leftOSMWay) )

				# print str(inverted_rightOSMWay.node_ids[0]) + '==' + str(rightOSMWay.node_ids[-1])
				# print str(inverted_leftOSMWay.node_ids[0]) + '==' + str(leftOSMWay.node_ids[-1])

	return OSMLaneletList

def get_OSMNode_by_id(OSMNodeList, node_id):
	"function used to get an OSMNode from OSMNodeList by its OSMId (node_id)"

	for temp_node in OSMNodeList:

		if temp_node.id == node_id:

			return temp_node

	raise NameError('Node not found by its OSM Id')

	return

def get_OSMWay_by_id(OSMWayList, way_id):
	"function used to get an OSMWay from OSMWayList by its OSMId (way_id)"

	for temp_way in OSMWayList:

		if temp_way.id == way_id:

			return temp_way

	raise NameError('Way not found by its OSM Id')

	return

def get_OSMLanelet_by_id(OSMLaneletList, lanelet_id):
	"function used to get an OSMWay from OSMWayList by its OSMId (way_id)"

	for temp_lanelet in OSMLaneletList:

		if temp_lanelet.id == lanelet_id:

			return temp_lanelet

	raise NameError('Lanelet not found by its OSM Id')

	return


def create_Lanelet_Adjacency_Matrix(OSMLaneletList, OSMWayList, OSMNodeList):

	adjacency_matrix = [[0 for x in range( len(OSMLaneletList) )] for x in range ( len(OSMLaneletList) )]

	for lanelet_index_a in range( len(OSMLaneletList) ):

		for lanelet_index_b in range( len(OSMLaneletList) ):

			if lanelet_index_a == lanelet_index_b:

				continue

			# Check is Lanelet_A connect to Lanelet_B (one directional only)
			is_adjacent = check_Lanelet_Adjacency(OSMLaneletList[lanelet_index_a], OSMLaneletList[lanelet_index_b], OSMWayList)
			# Redundant check, when a lanelet is checked with itself!

			if is_adjacent:

				lanelet_length = get_lanelet_length( OSMLaneletList[lanelet_index_a], OSMNodeList )
				adjacency_matrix[lanelet_index_a][lanelet_index_b] = lanelet_length

	return adjacency_matrix


def check_Lanelet_Adjacency(Lanelet_start, Lanelet_end, OSMWayList):

	if ( Lanelet_start.left_OSMWay.node_ids[-1] == Lanelet_end.left_OSMWay.node_ids[0] ) and ( Lanelet_start.right_OSMWay.node_ids[-1] == Lanelet_end.right_OSMWay.node_ids[0] ) :

		return True

	return False

def get_lanelet_length(OSMLanelet, OSMNodeList):

	left_lane_length = 0
	right_lane_length = 0

	left_OSMWay = OSMLanelet.left_OSMWay
	right_OSMWay = OSMLanelet.right_OSMWay


	for way_node_id in range( len(left_OSMWay.node_ids) - 1 ):

		prev_node_left = get_OSMNode_by_id( OSMNodeList, left_OSMWay.node_ids[way_node_id] )
		prev_node_right = get_OSMNode_by_id( OSMNodeList, right_OSMWay.node_ids[way_node_id] )

		next_node_left = get_OSMNode_by_id( OSMNodeList, left_OSMWay.node_ids[way_node_id+1] )
		next_node_right = get_OSMNode_by_id( OSMNodeList, right_OSMWay.node_ids[way_node_id+1] )

		current_left_lane_length = get_distance_between_nodes(prev_node_left, next_node_left)
		current_right_lane_length = get_distance_between_nodes(prev_node_right, next_node_right)

		left_lane_length = left_lane_length + current_left_lane_length
		right_lane_length = right_lane_length + current_right_lane_length

	average_length = ( left_lane_length + right_lane_length )/2

	return average_length

def get_distance_between_nodes(OSMNodeStart, OSMNodeEnd):

	distance = ( ( OSMNodeEnd.x - OSMNodeStart.x )**2 + ( OSMNodeEnd.y - OSMNodeStart.y )**2 )**0.5

	return distance

def get_center_between_nodes(OSMNodeStart, OSMNodeEnd):

	x = ( OSMNodeStart.x + OSMNodeEnd.x )/2.
	y = ( OSMNodeStart.y + OSMNodeEnd.y )/2.

	return [x, y]

def dijkstra_algorithm(adjacency_matrix, source_id):
	"Receives an adjacency_matrix, which is not necessarily symmetric."
	"A value of 0 means that in adjacency_matrix[i][j] means that there is no connection from node i to node j"

	num_nodes = len(adjacency_matrix)
	distances = [ 10e10 for i in range(num_nodes) ]
	distances[source_id] = 0
	previous_node = [ -1 for i in range(num_nodes) ]

	nodes_to_visit = [ source_id ]

	while nodes_to_visit:

		current_node_to_visit = nodes_to_visit.pop(0)

		current_adjacencies = adjacency_matrix[current_node_to_visit]

		for i in range( len(current_adjacencies) ):

			# print 'Checking adjacency from ' + str(current_node_to_visit) + ' to ' + str(i)

			if i == current_node_to_visit:

				# print 'Same node'
				continue

			if current_adjacencies[i] == 0:

				# print 'No connection'
				continue

			if distances[i] >  distances[current_node_to_visit] + current_adjacencies[i]:

				# print 'Current distance = ' + str( distances[i] )
				# print 'distances[current_node_to_visit] + current_adjacencies[i] = ' + str( distances[current_node_to_visit] + current_adjacencies[i] )
				# print 'Shorter distance'

				distances[i] = distances[current_node_to_visit] + current_adjacencies[i]
				previous_node[i] = current_node_to_visit
				nodes_to_visit.append(i)


	return distances, previous_node


def get_shortest_path(previous_node, destination_id):
	"Simply receives Dijkstra outputs and converts it to the shortes path"
	
	shortest_path = []

	while destination_id != -1:

		shortest_path.append(destination_id)
		destination_id = previous_node[destination_id]

	shortest_path.reverse()
	
	return shortest_path	

def convert_to_lanelet_id(shortest_path, OSMLaneletList):

	lanelet_path = []

	for i in shortest_path:

		lanelet_path.append(OSMLaneletList[i].id)

	return lanelet_path


def get_trajectory(lanelet_path, OSMLaneletList, OSMNodeList):

	x_traj = []
	y_traj = []

	for lanelet_id in lanelet_path:

		temp_lanelet = get_OSMLanelet_by_id(OSMLaneletList, lanelet_id)

		[x_traj_temp, y_traj_temp] = convert_lanelet_to_trajectory(temp_lanelet, OSMNodeList)

		x_traj.extend(x_traj_temp)
		y_traj.extend(y_traj_temp)

	# trajectory = RoadTrajectory(x_road, y_road, t_road)

	# return trajectory
	return [x_traj, y_traj]

def convert_lanelet_to_trajectory(OSMLanelet, OSMNodeList):

	left_OSMWay = OSMLanelet.left_OSMWay 
	right_OSMWay = OSMLanelet.right_OSMWay

	center_points_x = []
	center_points_y = []

	left_nodes = []
	
	for left_id in left_OSMWay.node_ids:

		left_nodes.append( get_OSMNode_by_id(OSMNodeList, left_id) )

	right_nodes = []
	
	for right_id in right_OSMWay.node_ids:

		right_nodes.append( get_OSMNode_by_id(OSMNodeList, right_id) )

	for idx in range( len( left_nodes ) ):

		[x, y] = get_center_between_nodes(left_nodes[idx], right_nodes[idx])
		center_points_x.append(x)
		center_points_y.append(y)
	
	cumulative_length_center = [0]
	current_cumulative_length_center = 0

	cumulative_length_left = [0]
	current_cumulative_length_left = 0

	cumulative_length_right = [0]
	current_cumulative_length_right = 0

	for idx in range(1, len( left_nodes ) ):

		prev_x = center_points_x[idx-1]
		new_x = center_points_x[idx]

		prev_y = center_points_y[idx-1]
		new_y = center_points_y[idx]

		current_distance = ( ( new_x - prev_x )**2. + ( new_y - prev_y )**2 )**0.5

		current_cumulative_length_center = current_cumulative_length_center + current_distance
		cumulative_length_center.append(current_cumulative_length_center)

		prev_node = left_nodes[idx-1]
		new_node = left_nodes[idx]

	 	current_cumulative_length_left = current_cumulative_length_left + get_distance_between_nodes(prev_node, new_node)
	 	cumulative_length_left.append(current_cumulative_length_left)

	 	prev_node = right_nodes[idx-1]
		new_node = right_nodes[idx]

	 	current_cumulative_length_right = current_cumulative_length_right + get_distance_between_nodes(prev_node, new_node)
		cumulative_length_right.append(current_cumulative_length_right)

	# print cumulative_length_left
	# print cumulative_length_right
	# print cumulative_length_center

	cumulative_length_desired = []

	velocity = 5. * (1000./3600.)
	# print "Velocity " + str( velocity )

	center_length = cumulative_length_center[-1]

	number_interpolation_points = int( math.ceil( center_length/velocity ) )

	for idx in range(number_interpolation_points):

		cumulative_length_desired.append( (float(idx)/number_interpolation_points) * center_length )

	interpolated_center_points_x = []
	interpolated_center_points_x.append( center_points_x[0] )
	interpolated_center_points_y = []
	interpolated_center_points_y.append( center_points_y[0] )

	for idx in range(1, number_interpolation_points):

		for search_idx in range( len(cumulative_length_center)-1 ):

			if cumulative_length_desired[idx] > cumulative_length_center[search_idx] and cumulative_length_desired[idx] < cumulative_length_center[search_idx+1]:

				distance_a = cumulative_length_desired[idx] - cumulative_length_center[search_idx]
				distance_b = cumulative_length_center[search_idx+1] - cumulative_length_desired[idx]

				interpolated_x = ( distance_b*center_points_x[search_idx] + distance_a*center_points_x[search_idx+1] )/( distance_a + distance_b )
				interpolated_y = ( distance_b*center_points_y[search_idx] + distance_a*center_points_y[search_idx+1] )/( distance_a + distance_b )

				interpolated_center_points_x.append(interpolated_x)
				interpolated_center_points_y.append(interpolated_y)
				


	# raise NameError('Debug...')
	# print number_interpolation_points
	# print cumulative_length_desired


	offset_x_removal = 651500
	offset_y_removal = 6560540


	new_interpolation_distance = 0
	new_interpolation_cumulative_distance = [new_interpolation_distance]

	for i in range( len( center_points_x ) - 1 ):

		current_distance =  ( ( center_points_x[i+1] - center_points_x[i] )**2 + ( center_points_y[i+1] - center_points_y[i] )**2 )**0.5
		# print "current_distance:"
		# print current_distance
		new_interpolation_distance = new_interpolation_distance + current_distance
		new_interpolation_cumulative_distance.append(new_interpolation_distance)


	# for i in range( len( center_points_x ) ):
	# 	center_points_x[i] = center_points_x[i] - offset_x_removal

	# for i in range( len( interpolated_center_points_x ) ):
	# 	interpolated_center_points_x[i] = interpolated_center_points_x[i] - offset_x_removal

	# for i in range( len( center_points_y ) ):
	# 	center_points_y[i] = center_points_y[i] - offset_y_removal

	# for i in range( len( interpolated_center_points_y ) ):
	# 	interpolated_center_points_y[i] = interpolated_center_points_y[i] - offset_y_removal

	# print "\n\n\n\n\n\n\nCHEEEEEECK HEEEEEEERREEEEEEEEEE:::::\n\n\n\n\n\n"
	# print "Center points X: "
	# print center_points_x
	# print "Interpolated center points X: "
	# print interpolated_center_points_x
	# print "Center points Y: "
	# print center_points_y
	# print "Interpolated center points Y: "
	# print interpolated_center_points_y
	# print "New interpolated distance"
	# print new_interpolation_cumulative_distance

	return [interpolated_center_points_x, interpolated_center_points_y]
