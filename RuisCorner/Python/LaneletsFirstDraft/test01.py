import utm
import xml.etree.ElementTree as ET
import customClasses as cc
from Tkinter import *

cc = reload(cc)

print "Goodbye, World!"




utm_tuple = utm.from_latlon(51.2, 7.5)
print utm_tuple

utm_tuple = utm.from_latlon(59.16705640735, 17.63029077908)
print utm_tuple


# print EASTING, NORTHING, ZONENUMBER, ZONELETTER


tree = ET.parse('scaniaNewLayer.xml')
root = tree.getroot()

print root.tag
print root.attrib

OSMNodeList = []

print len(root.findall('node'))
print len(root.findall('way'))

for node in root.findall('node'):

	OSMNodeList.append( cc.OSMNode(node.get('id'), node.get('lat'), node.get('lon')) )

print len(OSMNodeList)

OSMWayList = []

for way in root.findall('way'):

	currentWay = cc.OSMWay( way.get('id') )

	# currentWay = cc.OSMWay( 4 )

	for node in way.findall('nd'):

		currentWay.add_node_id( node.get('ref') )

	OSMWayList.append( currentWay )


min_x = 10e+10; max_x = - min_x;
min_y = 10e+10; max_y = - 10e+10;

for node in OSMNodeList:

	if ( min_x > node.x ):
		min_x = node.x
	if ( max_x < node.x ):
		max_x = node.x
	if ( min_y > node.y ):
		min_y = node.y
	if ( max_y < node.y ):
		max_y = node.y

viewing_gap = 50
min_x = min_x - viewing_gap
min_y = min_y - viewing_gap
max_x = max_x + viewing_gap
max_y = max_y + viewing_gap

world_width = max_x - min_x;
world_height = max_y - min_y;

canvas_width = 1000;
canvas_height = 1000;

if (world_width > world_height):

	shrinking_ratio = canvas_width/world_width

else:

	shrinking_ratio = canvas_height/world_height



def draw_line_between_nodes(OSMNode1, OSMNode2, min_x, min_y, canvas_handler, canvas_height, shrinking_ratio):
	"function used to draw a line between two OSMNodes"

	x1 = OSMNode1.x
	y1 = OSMNode1.y

	x2 = OSMNode2.x
	y2 = OSMNode2.y

	canvas_handler.create_line( (x1 - min_x)*shrinking_ratio, canvas_height - (y1 - min_y)*shrinking_ratio, (x2 - min_x)*shrinking_ratio, canvas_height - (y2 - min_y)*shrinking_ratio )

	return

def get_OSMNode_by_id(OSMNodeList, node_id):
	"function used to get an OSMNode from OSMNodeList by its OSMId (node_id)"

	for OSMNode in OSMNodeList:

		if OSMNode.id == node_id:

			return OSMNode

	raise NameError('Node not found by its OSM Id')

	return


master = Tk()
w = Canvas(master, width=canvas_width, height=canvas_height)
w.pack()

# coordinate system origin: upper-left corner




for way in OSMWayList:

	print 'len(way.node_ids) =', len(way.node_ids)

	for idx in xrange(0, len(way.node_ids)-1):

		# print 'way.node_ids[idx]' , way.node_ids[idx]
		# print 'type(way.node_ids[idx])' , type(way.node_ids[idx])

		OSMNode1 = get_OSMNode_by_id(OSMNodeList, way.node_ids[idx])
		OSMNode2 = get_OSMNode_by_id(OSMNodeList, way.node_ids[idx+1])


		draw_line_between_nodes(OSMNode1, OSMNode2, min_x, min_y, w, canvas_height, shrinking_ratio*(8/6))

# w.create_line( (x1 - min_x)*shrinking_ratio, canvas_height - (y1 - min_y)*shrinking_ratio, 500, 500)

# w.create_line(0, 100, 200, 0, fill="red", dash=(4, 4))
# w.create_rectangle(50, 25, 150, 75, fill="blue")

# w.create_line(min_x-min_x, min_)

mainloop()