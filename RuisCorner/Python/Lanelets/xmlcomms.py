import xml.etree.ElementTree as ET
import customclasses as cc
import xml.dom.minidom
import socket
from xml.dom import minidom
import time
from laneletlibrary import *

BUFSIZE = 2**15

def prettify(elem):
    """Return a pretty-printed XML string for the Element.
    """
    rough_string = ET.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")

def make_road_info_xml_message_string(osm_node_list, osm_way_list):

	root = ET.Element('message')

	root.set("type", "road_info")

	for node in osm_node_list:

		ET.SubElement(root, "node", id = str(node.id), pixel_x = str(node.pixel_x), pixel_y = str(node.pixel_y) )

	for way in osm_way_list:

		current_way_et = ET.SubElement(root, "way", id = str(way.id) )

		for node_id in way.node_ids:

			ET.SubElement(current_way_et, "nd", id = str(node_id) )


	# tree = ET.ElementTree(root)
	# tree.write("send_initial_xml.xml")

	# print "tree:"
	xml_string = ET.tostring(root)
	# print prettify(root)

	return xml_string

def get_trajectory_reply_message_string(start_id, end_id, osm_lanelet_list, osm_way_list, osm_node_list):

	root = ET.Element('message')

	print ["start_id", "end_id"]
	print [start_id, end_id]

	[traj_x, traj_y] = get_trajectory_from_node_ids(start_id, end_id, osm_lanelet_list, osm_way_list, osm_node_list);

	print "traj_x"
	print traj_x

	root.set("type", "trajectory_reply")
	root.set("id", "5")

	for i in range( len( traj_x ) ):

		current_node = ET.SubElement(root, 'point')
		current_node.set("pixel_x", str( traj_x[i] ) )
		current_node.set("pixel_y", str( traj_y[i] ) )
		current_node.set("time", str(i) )
	
	return ET.tostring(root);

def process_trajectory_request_string(trajectory_request_message_string, osm_node_list, osm_way_list):

	root = ET.fromstring(trajectory_request_message_string)

	# root = ET.Element('message')

	start_id = int( root.find("start").get("id") )

	end_id = int( root.find("end").get("id") )

	# print "start_id, end_id"
	# print [start_id, end_id]

	return [start_id, end_id]
	

def create_server(osm_lanelet_list, osm_node_list, osm_way_list):

	#create an INET, STREAMing socket
	serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	#bind the socket to a public host,
	# and a well-known port

	print "Creating a PUBLIC socket"
	serversocket.bind((socket.gethostname(), 6557))
	# print "Creating a PRIVATE INTERNAL socket"
	# serversocket.bind(('localhost', 5555))
	
	print "Waiting for a connection"
	serversocket.settimeout(10)
	#become a server socket
	serversocket.listen(1)
	#accept connections from outside
	(clientsocket, address) = serversocket.accept()

	# print "Pause one second"
	# time.sleep() 

	print "Sending road info"
	# Get road info string and send it
	road_info_message_string = make_road_info_xml_message_string(osm_node_list, osm_way_list)
	road_info_message_string = road_info_message_string + "\n"
	# road_info_message_string = "lalalalalalalala \n"
	sent = clientsocket.send(road_info_message_string)
	if sent == 0:
		raise RuntimeError("socket connection broken")

	print "Waiting for trajectory request"
	# Wait for trajectory request and process it
	clientsocket.setblocking(1)
	reply_string = clientsocket.recv(BUFSIZE);
	print "Received trajectory reply: " + reply_string
	[start_id, end_id] = process_trajectory_request_string(reply_string, osm_node_list, osm_way_list)

	print "Sending trajectory reply"
	# Get trajectory string and send it
	trajectory_reply_message_string = get_trajectory_reply_message_string(start_id, end_id, osm_lanelet_list, osm_way_list, osm_node_list)
	print "trajectory_reply_message_string: "
	print trajectory_reply_message_string
	clientsocket.send(trajectory_reply_message_string)

	return 0


def send_initial_xml(osm_node_list, osm_way_list):


	root = ET.Element('message')

	root.set("type", "road_info")

	for node in osm_node_list:

		ET.SubElement(root, "node", id = str(node.id), pixel_x = str(node.pixel_x), pixel_y = str(node.pixel_y) )


	for way in osm_way_list:

		current_way_et = ET.SubElement(root, "way", id = str(way.id) )

		for node_id in way.node_ids:

			ET.SubElement(current_way_et, "nd", id = str(node_id) )


	tree = ET.ElementTree(root)
	tree.write("send_initial_xml.xml")

	# print "tree:"
	xml_string = ET.tostring(root)
	print xml_string
	print prettify(root)



	print "Creating the socket"
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	host = "130.237.50.246"
	port = 5555

	print "Connecting the socket"
	s.connect( (host, port) )

	print "Sending information over the socket"
	s.send( xml_string )




