// author: Haukur Heidarsson

#include <MotorWheel.h>
#include <R2WD.h>
#include <PID_Beta6.h>
#include <PinChangeInt.h>
#include <PinChangeIntConfig>
#define _USE_MATH_DEFINES
#include <cmath>

enum buffer_state {
  BUFFER_EMPTY,
  STATE_QUERY,
  CONTROL_COMMAND,
  BUFFER_INVALID
};

const int buffer_size = 100;
char buffer[buffer_size];
char junk[buffer_size];

const int maxc = 500;
const int minc = -500;

/* Motor setup */
irqISR(irq1,isr1); // Intterrupt function.on the basis of the pulse, work for wheel1
MotorWheel lwheel(9,8,4,5,&irq1,REDUCTION_RATIO,int(144*PI));
irqISR(irq2,isr2);
MotorWheel rwheel(10,11,6,7,&irq2,REDUCTION_RATIO,int(144*PI));
R2WD drivetrain(&lwheel,&rwheel,WHEELSPAN);

/* Loop variables */
int left = 0;
int right = 0;
boolean ldir = DIR_ADVANCE;
boolean rdir = DIR_BACKOFF;

boolean is_control() {
  // control signal
  // u +010 +005
  if (strncmp(buffer, "u", 1) != 0) {
    return false;
  }
  int left;
  int right;
  sscanf(buffer, "u %d %d", &left, &right);
  if (left < minc || left > maxc || right < minc || right > maxc) {
    return false;
  }
  return true;
}

boolean is_state_query() {
  return (strncmp(buffer, "state?", 6) == 0);
}


int read_buffer() {
  send_debug("reading buffer");
  int junk_bytes_read = Serial.readBytesUntil(':', junk, buffer_size);
  int bytes_read = Serial.readBytesUntil(';', buffer, buffer_size);
  
  //if (junk_bytes_read > 0) {
  //  Serial.print("junk=");
  //  Serial.print(junk);
  //}
  
  if (bytes_read > 0) {
    Serial.print("buffer=");
    Serial.print(buffer);
  }
  
  if (bytes_read == 0) {
    // zero bytes were read
    return BUFFER_EMPTY;
  } else if (is_control()) {
    return CONTROL_COMMAND;
  } else if (is_state_query()) {
    return STATE_QUERY;
  } else {
    return BUFFER_INVALID;
  }
}

void send_state(int left, int right) {
  Serial.print(":state ");
  Serial.print(left, DEC);
  Serial.print(" ");
  Serial.print(right, DEC);
  Serial.print(";"); 
}

void send_debug(const char* msg) {
  Serial.print(":!");
  Serial.print(msg);
  Serial.print(";");
}

void setup() {
  //TCCR0B=TCCR0B&0xf8|0x01; 
  TCCR1B=TCCR1B&0xf8|0x01;    // Pin9,Pin10 PWM 31250Hz
  //TCCR2B=TCCR2B&0xf8|0x01;  // Pin3,Pin11 PWM 31250Hz
  Serial.begin(9600);
  drivetrain.PIDEnable(0.26,0.02,0,10);
  send_debug("ready");
}

void loop() {  
  while (Serial.available()) {
    switch (read_buffer()) {
      case BUFFER_EMPTY:
        break;
      case CONTROL_COMMAND:
        sscanf(buffer, "u %d %d", &left, &right);
        if (left <= 0) {
          ldir = DIR_BACKOFF;
        } else {
          ldir = DIR_ADVANCE;
        }
        if (right <= 0) {
          rdir = DIR_ADVANCE;
        } else {
          rdir = DIR_BACKOFF;
        }
        Serial.print(":!set left=");
        Serial.print(left, DEC);
        Serial.print(" right=");
        Serial.print(right, DEC);
        Serial.print(";");
        break;
      case STATE_QUERY:
        send_state(
          drivetrain.wheelLeftGetSpeedMMPS(), 
          drivetrain.wheelRightGetSpeedMMPS());
        // Asking for the state resets it >-(
        drivetrain.wheelLeftSetSpeedMMPS(abs(left), ldir);
        drivetrain.wheelRightSetSpeedMMPS(abs(right), rdir);
        break;
      case BUFFER_INVALID:
        left = 0;
        right = 0;
        send_debug("got an invalid command");
        break;
    }
    drivetrain.wheelLeftSetSpeedMMPS(abs(left), ldir);
    drivetrain.wheelRightSetSpeedMMPS(abs(right), rdir);
  }
  drivetrain.PIDRegulate();
}
