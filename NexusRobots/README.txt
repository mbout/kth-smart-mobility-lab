The APC220 quick guide explains how to setup the radios and perform simple tests to check the radios' and Nexus robots' behavior.

The manual folders gather some manuals and datasheets found online, for the radios and the robots

The Test programs folder contains the programs used for the tests described in the quick quide

The lib folder contains the Arduino libraries for the Nexus robots