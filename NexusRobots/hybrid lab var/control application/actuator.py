# -*- coding: utf-8 -*-
from math import sqrt, atan2, cos, sin
from numpy import clip
from serial import Serial
from time import sleep

import numpy as np

import logging
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
logging.basicConfig(level=logging.DEBUG)


def distance(pose1, pose2):
    return sqrt((pose1[0]-pose2[0])**2+(pose1[1]-pose2[1])**2)

def angle_ref(next_pose, cur_pose):
	dy = next_pose[1] - cur_pose[1]
	dx = next_pose[0] - cur_pose[0]
	return atan2(dy, dx)

def update_pose(cur_pose, angle, speed, time_step):
	x = cur_pose[0] + speed*cos(angle)*time_step
	y = cur_pose[1] + speed*sin(angle)*time_step
	return (x, y)

def nexus_move(cur_pose, next_pose):
	U_CONST = 300;
	K_omega = 5;
	theta_ref = angle_ref(next_pose, cur_pose)
	print "theta_ref: ", theta_ref
	theta_curr = cur_pose[2]
	omega = clip(K_omega*(theta_ref-theta_curr),-300,300)
	Uleft = -omega + U_CONST
	Uright = omega + U_CONST
	Uleft = Uright = 100
	return Uleft, Uright

class actuator(object):
	def __init__(self, port):
		self.inbox = {'next_pose':None, 'cur_pose':None}
		self.subs_topic = ('next_pose','cur_pose')
		self.cur_pose = None
		self.next_pose = None
		self.id = 'actuator'
		self.ubot = Ubot(port)

	def execute(self, ros):
		#print "----"
		#print self.cur_pose
		#print self.next_pose
		if self.inbox['next_pose']:
			self.next_pose = self.inbox['next_pose']
			self.inbox['next_pose'] = None
		if self.inbox['cur_pose']:
			self.cur_pose = self.inbox['cur_pose']
			self.inbox['cur_pose'] = None
		#print "----"
		#print self.cur_pose
		#print self.next_pose
		if self.next_pose:
			(Uleft, Uright) = nexus_move(self.cur_pose, self.next_pose)
			self.ubot.control(Uleft, Uright)


###########################
##########################
# serial communication to nexus
def mk_expectation_exception(wanted, got):
	return Exception(
		"Expected {0} but got {1}".format(
			str(wanted),
			str(got)))

class Ubot:
	def __init__(self, port):
		self.serial = Serial(port, 9600, timeout=5)
		log.info("Giving serial port 2 seconds to wake up")
		sleep(2)
		log.info("Serial ready")
		log.info("Reading motor set points")
		l, r = self.read_state()
		log.info("motor set to {0:d} {1:d}".format(l, r))
		log.info("Robot ready")

	def _writemsg(self, msg):
		b = msg.encode("ascii")
		log.info("sending {0}".format(str(b)))
		self.serial.write(b)

	def _readmsg(self):
		while True:
			b = b""
			while b != b":":
				b = self.serial.read()
			b = self.serial.read()
			msg = b""
			while b != b";":
				msg += b
				b = self.serial.read()
			log.info("read {0}".format(str(msg)))
			if msg.startswith(b"!"):
				log.debug("robot says: {0!s}".format(msg))
			else:
				break
		return msg

	def control(self, uleft, uright):
		assert uleft >= -999 and uleft <= 999
		assert uright >= -999 and uright <= 999
		uleft = int(uleft)
		uright = int(uright)
		msg = ":u {0:=+04d} {1:=+04d};".format(uleft, uright)
		self._writemsg(msg)

	def read_state(self):
		self._writemsg(":state?;")
		#sleep(1) ##########################
		resp = self._readmsg()
		if not resp.startswith(b"state"):
			self.control(0,0)
			raise mk_expectation_exception(b"state", resp)
		_, left, right = resp.decode("ascii").split(" ")
		return int(left), int(right)

	def buffer_input(self):
		self.serial.read

	def close_connection(self):
		self.serial.close()